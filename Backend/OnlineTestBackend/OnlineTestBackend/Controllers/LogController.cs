﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LogController : BaseController
    {
        public LogController(OnlineTestBackendDBContext db) : base(db) { }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Logs.Include(x => x.Users).OrderByDescending(x => x.Id).AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            Log log = await _context.Logs.
                Include(x => x.Users).ThenInclude(x => x.Employees).
                Include(x => x.Users).ThenInclude(x => x.Students).Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
            if (log == null)
                return new BaseResponse
                {
                    errorCode = 1,
                    message = "Không tìm thấy dữ liệu!"
                };
            if (log.Users.Employees != null)
                log.Users.Employees.ToList()[0].EMP_AVATAR = Utils.Helper.GetBaseUrl(Request) + log.Users.Employees.ToList()[0].EMP_AVATAR;
            else
                log.Users.Students.ToList()[0].STU_AVATAR = Utils.Helper.GetBaseUrl(Request) + log.Users.Students.ToList()[0].STU_AVATAR;
            return new BaseResponse
            {
                data = log
            };
        }
    }
}