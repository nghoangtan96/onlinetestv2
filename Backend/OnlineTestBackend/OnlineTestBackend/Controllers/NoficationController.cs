﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NoficationController : BaseController
    {
        public NoficationController(OnlineTestBackendDBContext db) : base(db) { }
        //get all nofication by user id
        [HttpGet("getAllNoficationByUserId/{useid}")]
        public async Task<ActionResult<BaseResponse>> getAllNoficationByUserId(long useid)
        {
            List<Nofication> listNofication = await _context.Nofications.Where(x => x.NOF_USEID == useid).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).OrderByDescending(x => x.Id).AsNoTracking().ToListAsync();
            for (int i = 0; i < listNofication.Count; i++)
            {
                if (listNofication.ElementAt(i).UserCreated != null)
                {
                    if (listNofication.ElementAt(i).UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                    {
                        listNofication.ElementAt(i).UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                    }
                    else
                    {
                        listNofication.ElementAt(i).UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + listNofication.ElementAt(i).UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                    }
                }
                TimeSpan temp = DateTime.Now.Subtract(listNofication.ElementAt(i).NOF_CREATEDDATE.GetValueOrDefault());
                listNofication.ElementAt(i).timeSpan = (temp.Days != 0 ? temp.Days + " ngày trước" : (temp.Hours != 0 ? temp.Hours + " giờ trước" : (temp.Minutes != 0 ? temp.Minutes + " phút trước" : "Vừa xong")));
            }
            return new BaseResponse
            {
                data = listNofication
            };
        }
        [HttpGet("getTop5NoficationNewestOrAllNotReadByUserId/{useid}")]
        public async Task<ActionResult<BaseResponse>> getTop5NoficationNewestOrAllNotReadByUserId(long useid)
        {
            int countNotRead = _context.Nofications.Where(x => x.NOF_USEID == useid && x.NOF_STATUS == 1).ToList().Count;

            if (countNotRead == 0 || countNotRead < 5)
            {
                List<Nofication> listNoficationNewest = await _context.Nofications.Where(x => x.NOF_USEID == useid).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).OrderByDescending(x => x.Id).Take(5).AsNoTracking().ToListAsync();
                for (int i = 0; i < listNoficationNewest.Count; i++)
                {
                    if (listNoficationNewest.ElementAt(i).UserCreated != null)
                    {
                        if (listNoficationNewest.ElementAt(i).UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                        {
                            listNoficationNewest.ElementAt(i).UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                        }
                        else
                        {
                            listNoficationNewest.ElementAt(i).UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + listNoficationNewest.ElementAt(i).UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                        }
                    }
                    TimeSpan temp = DateTime.Now.Subtract(listNoficationNewest.ElementAt(i).NOF_CREATEDDATE.GetValueOrDefault());
                    listNoficationNewest.ElementAt(i).timeSpan = (temp.Days != 0 ? temp.Days + " ngày trước" : (temp.Hours != 0 ? temp.Hours + " giờ trước" : (temp.Minutes != 0 ? temp.Hours + " phút trước" : "Vừa xong")));
                }
                return new BaseResponse
                {
                    data = listNoficationNewest
                };
            }
            else
            {
                List<Nofication> listNofication = await _context.Nofications.Where(x => x.NOF_USEID == useid && x.NOF_STATUS == 1).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).OrderByDescending(x => x.Id).ToListAsync();
                for (int i = 0; i < listNofication.Count; i++)
                {
                    if (listNofication.ElementAt(i).UserCreated != null)
                    {
                        if (listNofication.ElementAt(i).UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                        {
                            listNofication.ElementAt(i).UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                        }
                        else
                        {
                            listNofication.ElementAt(i).UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + listNofication.ElementAt(i).UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                        }
                    }
                    TimeSpan temp = DateTime.Now.Subtract(listNofication.ElementAt(i).NOF_CREATEDDATE.GetValueOrDefault());
                    listNofication.ElementAt(i).timeSpan = (temp.Days != 0 ? temp.Days + " ngày trước" : (temp.Hours != 0 ? temp.Hours + " giờ trước" : (temp.Minutes != 0 ? temp.Minutes + " phút trước" : "Vừa xong")));
                }
                return new BaseResponse
                {
                    data = listNofication
                };
            }
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var nofication = await _context.Nofications.Where(x => x.Id == id).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                AsNoTracking().
                FirstOrDefaultAsync();
            if (nofication == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (nofication.UserCreated != null)
            {
                if (nofication.UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    nofication.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    nofication.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + nofication.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
            }

            TimeSpan temp = DateTime.Now.Subtract(nofication.NOF_CREATEDDATE.GetValueOrDefault());
            nofication.timeSpan = (temp.Days != 0 ? temp.Days + " ngày trước" : (temp.Hours != 0 ? temp.Hours + " giờ trước" : (temp.Minutes != 0 ? temp.Minutes + " phút trước" : "Vừa xong")));

            return new BaseResponse
            {
                data = nofication
            };
        }
        [HttpGet("messageRead/{userId}")]
        public async Task<ActionResult<BaseResponse>> messageRead(long userId)
        {
            var listNofication = await _context.Nofications.Where(x => x.NOF_USEID == userId && x.NOF_STATUS == 1).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                ToListAsync();

            if (listNofication.Count == 0)
            {
                return new BaseResponse
                {
                    errorCode = 1,
                    message = "Không có thông báo mới!"
                };
            }

            for (int i = 0; i < listNofication.Count; i++)
            {
                listNofication.ElementAt(i).NOF_STATUS = 0;
                await _context.SaveChangesAsync();
            }

            return new BaseResponse
            {
                data = listNofication
            };
        }
    }
}