﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FacultyController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public FacultyController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Faculties.Include(x => x.Branches).Where(x => x.FAC_ISDELETED == false).AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(int id)
        {
            var faculty = await _context.Faculties.
                Include(x => x.Employees).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.UserModified).ThenInclude(x => x.Employees).
                Where(x => x.FAC_ISDELETED == false && x.Id == id).AsNoTracking().FirstOrDefaultAsync();
            if (faculty == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (faculty.UserCreated != null)
            {
                if (faculty.UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    faculty.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    faculty.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + faculty.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
            }
            //if (faculty.UserModified != null && faculty.FAC_CREATEDBY != faculty.FAC_MODIFIEDBY)
            if (faculty.UserModified != null)
            {
                if (faculty.UserModified.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    faculty.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    faculty.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + faculty.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
            }

            return new BaseResponse
            {
                data = faculty
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(Faculty faculty)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = faculty.FAC_CREATEDBY.GetValueOrDefault();
            log.LOG_MODULE = "Thêm khoa";
            log.LOG_ACTID = 1;
            log.LOG_TABID = 7;
            string title = "<b>Tên khoa:</b><title>" +
                "<b>Địa chỉ:</b><title>" +
                "<b>Số điện thoại:</b><title>" +
                "<b>Email:</b><title>" +
                "<b>Website:</b><title>" +
                "<b>Thông tin khoa:</b><title>" +
                "<b>Trạng thái:</b>";

            string insertLog = faculty.FAC_NAME + "<item>"
                + (faculty.FAC_ADDRESS == null ? "" : faculty.FAC_ADDRESS) + "<item>"
                + (faculty.FAC_PHONE == null ? "" : faculty.FAC_PHONE) + "<item>"
                + (faculty.FAC_EMAIL == null ? "" : faculty.FAC_EMAIL) + "<item>"
                + (faculty.FAC_WEBSITE == null ? "" : faculty.FAC_WEBSITE) + "<item>"
                + faculty.FAC_INFORMATION + "<item>"
                + (faculty.FAC_STATUS == 1 ? "Sử dụng" : "Loại bỏ");

            faculty.FAC_CREATEDDATE = DateTime.Now;
            faculty.FAC_ISDELETED = false;

            try
            {
                _context.Faculties.Add(faculty);
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + insertLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetFaculty", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = faculty
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(int id, Faculty faculty)
        {
            Faculty facultyDB = await _context.Faculties.FindAsync(id);

            if (facultyDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = faculty.FAC_MODIFIEDBY.GetValueOrDefault();
            log.LOG_ACTID = 2;
            log.LOG_TABID = 7;
            log.LOG_MODULE = "Sửa khoa";

            string title = "<b>Tên khoa:</b><title>" +
                "<b>Địa chỉ:</b><title>" +
                "<b>Số điện thoại:</b><title>" +
                "<b>Email:</b><title>" +
                "<b>Website:</b><title>" +
                "<b>Thông tin khoa:</b><title>" +
                "<b>Trạng thái:</b>";

            string updateLog = facultyDB.FAC_NAME + "<to>" + faculty.FAC_NAME + "<item>"
                + (facultyDB.FAC_ADDRESS == null ? "" : facultyDB.FAC_ADDRESS) + "<to>" + (faculty.FAC_ADDRESS == null ? "" : faculty.FAC_ADDRESS) + "<item>"
                + (facultyDB.FAC_PHONE == null ? "" : facultyDB.FAC_PHONE) + "<to>" + (faculty.FAC_PHONE == null ? "" : faculty.FAC_PHONE) + "<item>"
                + (facultyDB.FAC_EMAIL == null ? "" : facultyDB.FAC_EMAIL) + "<to>" + (faculty.FAC_EMAIL == null ? "" : faculty.FAC_EMAIL) + "<item>"
                + (facultyDB.FAC_WEBSITE == null ? "" : facultyDB.FAC_WEBSITE) + "<to>" + (faculty.FAC_WEBSITE == null ? "" : faculty.FAC_WEBSITE) + "<item>"
                + facultyDB.FAC_INFORMATION + "<to>" + faculty.FAC_INFORMATION + "<item>"
                + (facultyDB.FAC_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<to>" + (faculty.FAC_STATUS == 1 ? "Sử dụng" : "Loại bỏ");

            try
            {
                facultyDB.FAC_NAME = faculty.FAC_NAME;
                facultyDB.FAC_ADDRESS = faculty.FAC_ADDRESS;
                facultyDB.FAC_PHONE = faculty.FAC_PHONE;
                facultyDB.FAC_EMAIL = faculty.FAC_EMAIL;
                facultyDB.FAC_WEBSITE = faculty.FAC_WEBSITE;
                facultyDB.FAC_INFORMATION = faculty.FAC_INFORMATION;
                facultyDB.FAC_STATUS = faculty.FAC_STATUS;
                facultyDB.FAC_MODIFIEDBY = faculty.FAC_MODIFIEDBY;
                facultyDB.FAC_MODIFIEDDATE = DateTime.Now;
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + updateLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetFaculty", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = facultyDB
            };
        }
        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(int id, long user)
        {
            Faculty facultyDB = await _context.Faculties.Include(x => x.Employees).FirstOrDefaultAsync(x => x.Id == id);

            if (facultyDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }
            if (facultyDB.Employees.Where(x => x.EMP_ISDELETED == false).ToList().Count != 0)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Không thể xóa. Vui lòng xóa tất cả nhân viên, ngành học, môn học thuộc khoa trước!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_MODULE = "Xóa khoa";
            log.LOG_ACTID = 3;
            log.LOG_TABID = 7;

            string title = "<b>Tên khoa:</b><title>" +
                "<b>Địa chỉ:</b><title>" +
                "<b>Số điện thoại:</b><title>" +
                "<b>Email:</b><title>" +
                "<b>Website:</b><title>" +
                "<b>Thông tin khoa:</b><title>" +
                "<b>Trạng thái:</b>";

            string deleteLog = facultyDB.FAC_NAME + "<item>"
                + (facultyDB.FAC_ADDRESS == null ? "" : facultyDB.FAC_ADDRESS) + "<item>"
                + (facultyDB.FAC_PHONE == null ? "" : facultyDB.FAC_PHONE) + "<item>"
                + (facultyDB.FAC_EMAIL == null ? "" : facultyDB.FAC_EMAIL) + "<item>"
                + (facultyDB.FAC_WEBSITE == null ? "" : facultyDB.FAC_WEBSITE) + "<item>"
                + facultyDB.FAC_INFORMATION + "<item>"
                + (facultyDB.FAC_STATUS == 1 ? "Sử dụng" : "Loại bỏ");

            try
            {
                facultyDB.FAC_ISDELETED = true;
                facultyDB.FAC_MODIFIEDBY = user;
                facultyDB.FAC_MODIFIEDDATE = DateTime.Now;

                log.LOG_CONTENT = title + "<divider>" + deleteLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetFaculty", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = facultyDB
            };
        }
        [HttpPost("getListFacultyMultiDel")]
        public async Task<ActionResult<BaseResponse>> getListFacultyMultiDel(int[] arrDel)
        {
            List<Faculty> listFac = new List<Faculty>();
            for (int i = 0; i < arrDel.Length; i++)
            {
                Faculty fac = await _context.Faculties.Where(x => x.Id == arrDel[i]).AsNoTracking().FirstOrDefaultAsync();
                if (fac != null)
                {
                    listFac.Add(fac);
                }
            }
            return new BaseResponse
            {
                data = listFac
            };
        }
        [HttpPost("multipleDelete/{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long user, Faculty[] arrDel)
        {

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 7;
            log.LOG_MODULE = "Xóa danh sách khoa";

            string title = "<b>Tên khoa:</b><title>" +
                "<b>Địa chỉ:</b><title>" +
                "<b>Số điện thoại:</b><title>" +
                "<b>Email:</b><title>" +
                "<b>Website:</b><title>" +
                "<b>Thông tin khoa:</b><title>" +
                "<b>Trạng thái:</b>";
            string multiDeleteLog = "";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i = 0; i < arrDel.Length; i++)
                    {
                        var facultyDB = _context.Faculties.
                            Include(x => x.Employees).
                            Where(x => x.Id == arrDel[i].Id).FirstOrDefault();
                        if (facultyDB.Employees.Where(x => x.EMP_ISDELETED == false).ToList().Count != 0)
                        {
                            return new BaseResponse
                            {
                                errorCode = 405,
                                message = "Không thể xóa. Vui lòng xóa tất cả nhân viên, ngành học, môn học thuộc khoa " + facultyDB.FAC_NAME + " trước!"
                            };
                        }
                        if (facultyDB != null)
                        {
                            multiDeleteLog += facultyDB.FAC_NAME + "<item>"
                                + (facultyDB.FAC_ADDRESS == null ? "" : facultyDB.FAC_ADDRESS) + "<item>"
                                + (facultyDB.FAC_PHONE == null ? "" : facultyDB.FAC_PHONE) + "<item>"
                                + (facultyDB.FAC_EMAIL == null ? "" : facultyDB.FAC_EMAIL) + "<item>"
                                + (facultyDB.FAC_WEBSITE == null ? "" : facultyDB.FAC_WEBSITE) + "<item>"
                                + facultyDB.FAC_INFORMATION + "<item>"
                                + (facultyDB.FAC_STATUS == 1 ? "Sử dụng" : "Loại bỏ");
                            if (i != arrDel.Length - 1)
                                multiDeleteLog += "<delete>";

                            facultyDB.FAC_MODIFIEDBY = user;
                            facultyDB.FAC_MODIFIEDDATE = DateTime.Now;
                            facultyDB.FAC_ISDELETED = true;
                            await _context.SaveChangesAsync();
                        }
                    }

                    log.LOG_CONTENT = title + "<divider>" + multiDeleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetFaculty", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse { };
        }
    }
}