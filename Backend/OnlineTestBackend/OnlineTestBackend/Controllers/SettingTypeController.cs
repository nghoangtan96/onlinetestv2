﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SettingTypeController : BaseController
    {
        public SettingTypeController(OnlineTestBackendDBContext db) : base(db) { }
        //get all setting type & setting list by setting type
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.SettingTypes.Include(x => x.Settings).AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(int id)
        {
            var settingType = await _context.SettingTypes.Where(x => x.Id == id).Include(x => x.Settings).AsNoTracking().FirstOrDefaultAsync();
            if(settingType == null)
            {
                return new BaseResponse
                {
                    errorCode = 1,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = settingType
            };
        }
    }
}