﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using OnlineTestBackend.Models;
using OnlineTestBackend.Models.ExportData;
using OnlineTestBackend.Models.Request;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : BaseController
    {
        private IHostingEnvironment _hostingEnv;
        private readonly IHubContext<SignalHub> _hubContext;
        public StudentController(OnlineTestBackendDBContext db, IHostingEnvironment env, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hostingEnv = env;
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            List<Student> listStudent = await _context.Students.
                Include(x => x.Users).
                Include(x => x.Classes).Include(x => x.SchoolYears).
                Include(x => x.Branches).ThenInclude(x => x.Faculties).
                Where(x => x.STU_ISDELETED == false).AsNoTracking().ToListAsync();
            for (int i = 0; i < listStudent.Count; i++)
            {
                listStudent.ElementAt(i).Users.User_Permissions = await _context.User_Permissions.
                    Include(x => x.Permissions).Where(x => x.USPE_USEID == listStudent.ElementAt(i).Users.Id).AsNoTracking().ToListAsync();
            }
            return new BaseResponse
            {
                data = listStudent
            };
        }
        [HttpGet("getListByClassId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListByClassId(long id)
        {
            List<Student> listStudent = await _context.Students.
                Include(x => x.Users).
                Include(x => x.SchoolYears).Include(x => x.Classes).Include(x => x.Branches).
                Where(x => x.STU_ISDELETED == false && x.STU_CLAID == id).AsNoTracking().ToListAsync();
            for (int i = 0; i < listStudent.Count; i++)
            {
                listStudent.ElementAt(i).Users.User_Permissions = await _context.User_Permissions.
                    Include(x => x.Permissions).Where(x => x.USPE_USEID == listStudent.ElementAt(i).Users.Id).AsNoTracking().ToListAsync();
            }
            return new BaseResponse
            {
                data = listStudent
            };
        }
        [HttpGet("getListByScheduleTestId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListByScheduleTestId(long id)
        {
            Schedule_Test scheduleTest = await _context.Schedule_Tests.
                Include(x => x.Student_Tests).AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            List<long> listStudentId = new List<long>();

            for (int i = 0; i < scheduleTest.Student_Tests.Count; i++)
            {
                listStudentId.Add(scheduleTest.Student_Tests.ElementAt(i).STTE_STUID.GetValueOrDefault());
            }

            List<Student> listStudent = await _context.Students.
                Include(x => x.Users).
                Include(x => x.SchoolYears).Include(x => x.Classes).Include(x => x.Branches).
                Where(x => x.STU_ISDELETED == false && listStudentId.Contains(x.Id)).AsNoTracking().ToListAsync();
            for (int i = 0; i < listStudent.Count; i++)
            {
                listStudent.ElementAt(i).Users.User_Permissions = await _context.User_Permissions.
                    Include(x => x.Permissions).Where(x => x.USPE_USEID == listStudent.ElementAt(i).Users.Id).AsNoTracking().ToListAsync();
            }
            return new BaseResponse
            {
                data = listStudent
            };
        }
        [HttpGet("getListBySchoolYearId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListBySchoolYearId(int id)
        {
            List<Student> listStudent = await _context.Students.
                   Include(x => x.Users).
                   Include(x => x.SchoolYears).Include(x => x.Classes).Include(x => x.Branches).
                   Where(x => x.STU_ISDELETED == false && x.STU_SCYEID == id).AsNoTracking().ToListAsync();
            for (int i = 0; i < listStudent.Count; i++)
            {
                listStudent.ElementAt(i).Users.User_Permissions = await _context.User_Permissions.
                    Include(x => x.Permissions).Where(x => x.USPE_USEID == listStudent.ElementAt(i).Users.Id).AsNoTracking().ToListAsync();
            }
            return new BaseResponse
            {
                data = listStudent
            };
        }
        [HttpGet("getListBySubjectId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListBySubjectId(long id)
        {
            Subject subject = await _context.Subjects.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            if (subject.SUB_ISALL == true)
            {
                return new BaseResponse
                {
                    data = await _context.Students.
                        Include(x => x.Users).
                        Include(x => x.SchoolYears).Include(x => x.Classes).Include(x => x.Branches).
                        Where(x => x.STU_ISDELETED == false).AsNoTracking().ToListAsync()
                };
            }
            else
            {
                List<Subject_Branch> listSubjectBranch = await _context.Subject_Branches.Where(x => x.SUBR_SUBID == id).AsNoTracking().ToListAsync();
                int[] arrBranch = new int[listSubjectBranch.Count];
                for (int i = 0; i < listSubjectBranch.Count; i++)
                {
                    arrBranch[i] = listSubjectBranch.ElementAt(i).SUBR_BRAID;
                }

                return new BaseResponse
                {
                    data = await _context.Students.
                    Include(x => x.Users).
                    Include(x => x.SchoolYears).Include(x => x.Classes).Include(x => x.Branches).
                    Where(x => arrBranch.Contains(x.STU_BRAID)).AsNoTracking().ToListAsync()
                };
            }
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var student = await _context.Students.
                Include(x => x.Users).
                Include(x => x.Classes).Include(x => x.Branches).ThenInclude(x => x.Faculties).
                Include(x => x.SchoolYears).AsNoTracking().
                FirstOrDefaultAsync(x => x.STU_ISDELETED == false && x.Id == id);
            if (student == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            student.Users.User_Permissions = await _context.User_Permissions.
                    Include(x => x.Permissions).Where(x => x.USPE_USEID == student.Users.Id).AsNoTracking().ToListAsync();

            if (student.STU_AVATAR != null)
            {
                student.FileUrl = Utils.Helper.GetBaseUrl(Request) + student.STU_AVATAR;
            }
            else
            {
                student.FileUrl = "assets/img/default_avatar.png";
            }

            if (student.STU_CREATEDBY != null)
            {
                student.UserCreated = await _context.Users.Include(x => x.Employees).AsNoTracking().FirstOrDefaultAsync(x => x.Id == student.STU_CREATEDBY);
                if (student.UserCreated.Employees.ElementAt(0).EMP_AVATAR != null)
                {
                    student.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + student.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
                else
                {
                    student.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
            }

            if (student.STU_MODIFIEDBY != null)
            {
                student.UserModified = await _context.Users.Include(x => x.Employees).AsNoTracking().FirstOrDefaultAsync(x => x.Id == student.STU_MODIFIEDBY);
                if (student.UserModified.Employees.ElementAt(0).EMP_AVATAR != null)
                {
                    student.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + student.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
                else
                {
                    student.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
            }

            return new BaseResponse
            {
                data = student
            };
        }
        [HttpGet("getSampleFileLink")]
        public async Task<ActionResult<BaseResponse>> getSampleFileLink()
        {
            return new BaseResponse
            {
                data = Utils.Helper.GetBaseUrl(Request) + "/Data/Excels/DSSV.xlsx"
            };
        }
        [HttpPost("ImportExcel/{user}&{autogencode}&{autogenaccount}")]
        public async Task<ActionResult<BaseResponse>> Import(long user, bool autogencode, bool autogenaccount, [FromForm]IFormFile file, CancellationToken cancellationToken)
        {
            if (file == null || file.Length <= 0)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy file tải lên!"
                };
            }

            if (!Path.GetExtension(file.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Định dạng file không hợp lệ!"
                };
            }

            List<Student> listStudent = await _context.Students.Where(x => x.STU_ISDELETED == false).ToListAsync();
            List<SchoolYear> listSchoolYear = await _context.SchoolYears.Where(x => x.SCYE_ISDELETED == false).ToListAsync();
            List<Branch> listBranch = await _context.Branches.Where(x => x.BRA_ISDELETED == false).ToListAsync();
            List<Class> listClass = await _context.Classes.Where(x => x.CLA_ISDELETED == false).ToListAsync();
            Setting usernameSetting = await _context.Settings.Where(x => x.Id == 1).FirstOrDefaultAsync();
            Setting passwordSetting = await _context.Settings.Where(x => x.Id == 2).FirstOrDefaultAsync();

            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream, cancellationToken);

                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;

                    using (var transaction = _context.Database.BeginTransaction())
                    {
                        try
                        {
                            //check valid
                            string errorLog = "";
                            bool hasError = false;
                            for (int row = 2; row <= rowCount; row++)
                            {
                                bool isCorrect = true;
                                string[] arrTitleSampleFile = new string[] { "Mã sinh viên", "CMND", "Họ", "Tên", "Giới tính", "Ngày sinh", "Số điện thoại", "Email", "Địa chỉ", "Niên khóa", "Ngành", "Lớp", "Tên tài khoản", "Mật khẩu" };
                                //check column is correct
                                for (int i = 1; i <= 14; i++)
                                {
                                    if (!(worksheet.Cells[1, i].Value != null && worksheet.Cells[1, i].Value.ToString().Trim().Contains(arrTitleSampleFile[i - 1])))
                                    {
                                        isCorrect = false;
                                        break;
                                    }
                                }
                                if (isCorrect == false)
                                {
                                    return new BaseResponse
                                    {
                                        errorCode = 405,
                                        message = "Dữ liệu không hợp lệ!<divider>Các cột dữ liệu không khớp theo file mẫu!"
                                    };
                                }
                                //valid column
                                string validateLog = "<b>Dòng " + (row - 1) + ":</b><br>";
                                //code
                                if (autogencode == false)
                                {
                                    if (worksheet.Cells[row, 1].Value == null)
                                    {
                                        validateLog += "&emsp;- Chưa nhập mã sinh viên.<br>";
                                        hasError = true;
                                    }
                                    else if (worksheet.Cells[row, 1].Value.ToString().Trim().Length != 9)
                                    {
                                        validateLog += "&emsp;- Mã sinh viên phải đủ 9 ký tự.<br>";
                                        hasError = true;
                                    }
                                    else if (!worksheet.Cells[row, 1].Value.ToString().Trim().All(char.IsDigit))
                                    {
                                        validateLog += "&emsp;- Mã sinh viên sai định dạng.<br>";
                                        hasError = true;
                                    }
                                    else if (listStudent.Where(x => x.STU_CODE == worksheet.Cells[row, 1].Value.ToString().Trim()).FirstOrDefault() != null)
                                    {
                                        validateLog += "&emsp;- Mã sinh viên đã tồn tại.<br>";
                                        hasError = true;
                                    }
                                }
                                //identity
                                if (worksheet.Cells[row, 2].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập CMND.<br>";
                                    hasError = true;
                                }
                                else if (worksheet.Cells[row, 2].Value.ToString().Trim().Length != 9 && worksheet.Cells[row, 2].Value.ToString().Trim().Length != 12)
                                {
                                    validateLog += "&emsp;- CMND phải đủ 9 hoặc 12 ký tự.<br>";
                                    hasError = true;
                                }
                                else if (!worksheet.Cells[row, 2].Value.ToString().Trim().All(char.IsDigit))
                                {
                                    validateLog += "&emsp;- CMND sai định dạng.<br>";
                                    hasError = true;
                                }
                                else if (listStudent.Where(x => x.STU_IDENTITY == worksheet.Cells[row, 2].Value.ToString().Trim()).FirstOrDefault() != null)
                                {
                                    validateLog += "&emsp;- CMND đã tồn tại.<br>";
                                    hasError = true;
                                }
                                //lastname
                                if (worksheet.Cells[row, 3].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập họ sinh viên.<br>";
                                    hasError = true;
                                }
                                //firstname
                                if (worksheet.Cells[row, 4].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập tên sinh viên.<br>";
                                    hasError = true;
                                }
                                //gender
                                if (worksheet.Cells[row, 5].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập giới tính.<br>";
                                    hasError = true;
                                }
                                else if (worksheet.Cells[row, 5].Value.ToString().Trim() != "Nam" && worksheet.Cells[row, 5].Value.ToString().Trim() != "Nữ")
                                {
                                    validateLog += "&emsp;- Giới tính chỉ chấp nhận 'Nam' hoặc 'Nữ'.<br>";
                                    hasError = true;
                                }
                                //dob
                                if (worksheet.Cells[row, 6].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập ngày sinh.<br>";
                                    hasError = true;
                                }
                                else if (worksheet.Cells[row, 6].Value.ToString().Trim().Split("/").Length != 3)
                                {
                                    validateLog += "&emsp;- Ngày sinh sai định dạng.<br>";
                                    hasError = true;
                                }
                                else if ((DateTime.Now.Year - int.Parse(worksheet.Cells[row, 6].Value.ToString().Trim().Split("/")[2])) < 18)
                                {
                                    validateLog += "&emsp;- Ngày sinh chưa chính xác, sinh viên chưa đủ 18 tuổi.<br>";
                                    hasError = true;
                                }
                                //phone
                                if (worksheet.Cells[row, 7].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập số điện thoại.<br>";
                                    hasError = true;
                                }
                                else if (!Utils.Validate.customizePhoneNumber(worksheet.Cells[row, 7].Value.ToString().Trim()).All(char.IsDigit))
                                {
                                    validateLog += "&emsp;- Số điện thoại sai định dạng.<br>";
                                    hasError = true;
                                }
                                else if (!Utils.Validate.validPhone(worksheet.Cells[row, 7].Value.ToString().Trim()))
                                {
                                    validateLog += "&emsp;- Số điện thoại không chính xác.<br>";
                                    hasError = true;
                                }
                                else if (listStudent.Where(x => x.STU_PHONE == Utils.Validate.customizePhoneNumber(worksheet.Cells[row, 7].Value.ToString().Trim())).FirstOrDefault() != null)
                                {
                                    validateLog += "&emsp;- Số điện thoại đã tồn tại.<br>";
                                    hasError = true;
                                }
                                //email
                                if (worksheet.Cells[row, 8].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập email.<br>";
                                    hasError = true;
                                }
                                else if (!Utils.Validate.validEmail(worksheet.Cells[row, 8].Value.ToString().Trim()))
                                {
                                    validateLog += "&emsp;- Email sai định dạng.<br>";
                                    hasError = true;
                                }
                                else if (listStudent.Where(x => x.STU_EMAIL == worksheet.Cells[row, 8].Value.ToString().Trim()).FirstOrDefault() != null)
                                {
                                    validateLog += "&emsp;- Email đã tồn tại.<br>";
                                    hasError = true;
                                }
                                //schoolyear
                                if (worksheet.Cells[row, 10].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập niên khóa.<br>";
                                    hasError = true;
                                }
                                else if (listSchoolYear.Where(x => x.SCYE_CODE == worksheet.Cells[row, 10].Value.ToString().Trim()).FirstOrDefault() == null)
                                {
                                    validateLog += "&emsp;- Niên khóa không tồn tại.<br>";
                                    hasError = true;
                                }
                                //branch
                                if (worksheet.Cells[row, 11].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập ngành học.<br>";
                                    hasError = true;
                                }
                                else if (listBranch.Where(x => x.BRA_NAME == worksheet.Cells[row, 11].Value.ToString().Trim()).FirstOrDefault() == null)
                                {
                                    validateLog += "&emsp;- Ngành học không tồn tại.<br>";
                                    hasError = true;
                                }
                                //class
                                if (worksheet.Cells[row, 12].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập lớp học.<br>";
                                    hasError = true;
                                }
                                else if (listClass.Where(x => x.CLA_CODE == worksheet.Cells[row, 12].Value.ToString().Trim()).FirstOrDefault() == null)
                                {
                                    validateLog += "&emsp;- Lớp học không tồn tại.<br>";
                                    hasError = true;
                                }
                                if (autogenaccount == false)
                                {
                                    //username
                                    if (worksheet.Cells[row, 13].Value == null)
                                    {
                                        validateLog += "&emsp;- Chưa nhập tên tài khoản.<br>";
                                        hasError = true;
                                    }
                                    else if (worksheet.Cells[row, 13].Value.ToString().Trim().Length < usernameSetting.SET_VALUE)
                                    {
                                        validateLog += "&emsp;- Tên tài khoản phải từ " + usernameSetting.SET_VALUE + " ký tự trở lên.<br>";
                                        hasError = true;
                                    }
                                    //password
                                    if (worksheet.Cells[row, 14].Value == null)
                                    {
                                        validateLog += "&emsp;- Chưa nhập mật khẩu.<br>";
                                        hasError = true;
                                    }
                                    else if (worksheet.Cells[row, 14].Value.ToString().Trim().Length < passwordSetting.SET_VALUE)
                                    {
                                        validateLog += "&emsp;- Mật khẩu phải từ " + usernameSetting.SET_VALUE + " ký tự trở lên.<br>";
                                        hasError = true;
                                    }
                                }
                                if (hasError == true)
                                    errorLog += validateLog;
                            }

                            //log
                            string title = "<b>Mã sinh viên:</b><title>" +
                                "<b>CMND:</b><title>" +
                                "<b>Họ:</b><title>" +
                                "<b>Tên:</b><title>" +
                                "<b>Ngày sinh:</b><title>" +
                                "<b>Giới tính:</b><title>" +
                                "<b>Số điện thoại:</b><title>" +
                                "<b>Địa chỉ:</b><title>" +
                                "<b>Email:</b><title>" +
                                "<b>Niên khóa:<b><title>" +
                                "<b>Ngành:</b><title>" +
                                "<b>Lớp:</b><title>" +
                                "<b>Tên tài khoản:</b><title>" +
                                "<b>Trạng thái tài khoản:</b><title>" +
                                "<b>Vai trò:</b>";
                            string multiInsertLog = "";


                            if (hasError == false)
                            {
                                //add data
                                for (int row = 2; row <= rowCount; row++)
                                {
                                    string rawPassword = "";
                                    //insert data to DB
                                    //user
                                    User userInsert = new User();
                                    if (autogenaccount == true)
                                    {
                                        string username = worksheet.Cells[row, 1].Value.ToString().Trim();
                                        if (username.Length < usernameSetting.SET_VALUE)
                                            username = (username + worksheet.Cells[row, 3].Value.ToString().Trim() + worksheet.Cells[row, 4].Value.ToString().Trim()).ToLower();
                                        string password = worksheet.Cells[row, 6].Value.ToString().Trim();
                                        if (password.Length < passwordSetting.SET_VALUE)
                                            password = Utils.Helper.RandomString(passwordSetting.SET_VALUE.GetValueOrDefault());
                                        userInsert.USE_USERNAME = username;
                                        userInsert.USE_PASSWORD = password;
                                        rawPassword = password;
                                    }
                                    else
                                    {
                                        userInsert.USE_USERNAME = worksheet.Cells[row, 13].Value.ToString().Trim();
                                        userInsert.USE_PASSWORD = Utils.Helper.GenHash(worksheet.Cells[row, 14].Value.ToString().Trim());
                                        rawPassword = worksheet.Cells[row, 14].Value.ToString().Trim();
                                    }
                                    userInsert.USE_CREATEDBY = user;
                                    userInsert.USE_CREATEDDATE = DateTime.Now;
                                    userInsert.USE_ISACTIVE = true;
                                    userInsert.USE_ISDELETED = false;
                                    userInsert.USE_STATUS = 1;
                                    _context.Users.Add(userInsert);
                                    await _context.SaveChangesAsync();

                                    //student
                                    Student studentInsert = new Student();
                                    if (autogencode == true)
                                    {
                                        int countStudentByClassId = _context.Students.
                                            Where(x => x.STU_CLAID == listClass.Where(y => y.CLA_CODE == worksheet.Cells[row, 12].Value.ToString().Trim()).
                                            FirstOrDefault().Id).ToList().Count + 1;
                                        studentInsert.STU_CODE = worksheet.Cells[row, 10].Value.ToString().Trim() +
                                            listBranch.Where(x => x.BRA_NAME == worksheet.Cells[row, 11].Value.ToString().Trim()).FirstOrDefault().BRA_CODE +
                                            (countStudentByClassId < 9 ? "000" + countStudentByClassId : (countStudentByClassId < 99 ? "00" + countStudentByClassId : (countStudentByClassId < 999 ? "0" + countStudentByClassId : countStudentByClassId.ToString())));
                                    }
                                    else
                                        studentInsert.STU_CODE = worksheet.Cells[row, 1].Value.ToString().Trim();
                                    studentInsert.STU_IDENTITY = worksheet.Cells[row, 2].Value.ToString().Trim();
                                    studentInsert.STU_LASTNAME = worksheet.Cells[row, 3].Value.ToString().Trim();
                                    studentInsert.STU_FIRSTNAME = worksheet.Cells[row, 4].Value.ToString().Trim();
                                    studentInsert.STU_GENDER = (worksheet.Cells[row, 5].Value.ToString().Trim() == "Nam" ? true : false);
                                    studentInsert.STU_DATEOFBIRTH = new DateTime(int.Parse(worksheet.Cells[row, 6].Value.ToString().Trim().Split("/")[2]),
                                        int.Parse(worksheet.Cells[row, 6].Value.ToString().Trim().Split("/")[1]),
                                        int.Parse(worksheet.Cells[row, 6].Value.ToString().Trim().Split("/")[0]));
                                    studentInsert.STU_PHONE = worksheet.Cells[row, 7].Value.ToString().Trim();
                                    studentInsert.STU_EMAIL = worksheet.Cells[row, 8].Value.ToString().Trim();
                                    studentInsert.STU_ADDRESS = (worksheet.Cells[row, 9].Value == null ? null : worksheet.Cells[row, 9].ToString().Trim());
                                    studentInsert.STU_SCYEID = listSchoolYear.Where(x => x.SCYE_CODE == worksheet.Cells[row, 10].Value.ToString().Trim()).FirstOrDefault().Id;
                                    studentInsert.STU_BRAID = listBranch.Where(x => x.BRA_NAME == worksheet.Cells[row, 11].Value.ToString().Trim()).FirstOrDefault().Id;
                                    studentInsert.STU_CLAID = listClass.Where(x => x.CLA_CODE == worksheet.Cells[row, 12].Value.ToString().Trim()).FirstOrDefault().Id;
                                    studentInsert.STU_CREATEDBY = user;
                                    studentInsert.STU_CREATEDDATE = DateTime.Now;
                                    studentInsert.STU_ISDELETED = false;
                                    studentInsert.STU_STATUS = 1;
                                    studentInsert.STU_USEID = userInsert.Id;
                                    _context.Students.Add(studentInsert);
                                    await _context.SaveChangesAsync();

                                    //user_permission
                                    User_Permission userPermission = new User_Permission();
                                    userPermission.USPE_CREATEDBY = user;
                                    userPermission.USPE_CREATEDDATE = DateTime.Now;
                                    userPermission.USPE_USEID = userInsert.Id;
                                    userPermission.USPE_STATUS = 1;
                                    userPermission.USPE_PERID = 8;

                                    _context.User_Permissions.Add(userPermission);
                                    await _context.SaveChangesAsync();

                                    //nofication
                                    Nofication nofication = new Nofication();
                                    nofication.NOF_USEID = userInsert.Id;
                                    nofication.NOF_CREATEDBY = user;
                                    nofication.NOF_LINK = "/info_user";
                                    nofication.NOF_CREATEDDATE = DateTime.Now;
                                    nofication.NOF_STATUS = 1;
                                    nofication.NOF_TITLE = "Thông báo bảo mật tài khoản";
                                    nofication.NOF_CONTENT = "Tài khoản của bạn vừa được tạo, vui lòng đổi mật khẩu để đảm bảo tài khoản an toàn!";
                                    _context.Nofications.Add(nofication);
                                    await _context.SaveChangesAsync();

                                    //log
                                    multiInsertLog += studentInsert.STU_CODE + "<item>"
                                    + studentInsert.STU_IDENTITY + "<item>"
                                    + studentInsert.STU_LASTNAME + "<item>"
                                    + studentInsert.STU_FIRSTNAME + "<item>"
                                    + studentInsert.STU_DATEOFBIRTH.GetValueOrDefault().Day + "/" + studentInsert.STU_DATEOFBIRTH.GetValueOrDefault().Month + "/" + studentInsert.STU_DATEOFBIRTH.GetValueOrDefault().Year + "<item>"
                                    + (studentInsert.STU_GENDER == true ? "Nam" : "Nữ") + "<item>"
                                    + studentInsert.STU_PHONE + "<item>"
                                    + studentInsert.STU_ADDRESS + "<item>"
                                    + studentInsert.STU_EMAIL + "<item>"
                                    + listSchoolYear.Where(x => x.Id == studentInsert.STU_SCYEID).FirstOrDefault().SCYE_CODE + "<item>"
                                    + listBranch.Where(x => x.Id == studentInsert.STU_BRAID).FirstOrDefault().BRA_NAME + "<item>"
                                    + listClass.Where(x => x.Id == studentInsert.STU_CLAID).FirstOrDefault().CLA_CODE + "<item>"
                                    + userInsert.USE_USERNAME + "<item>"
                                    + (userInsert.USE_STATUS == 1 ? "Sử dụng" : "Khóa") + "<item>"
                                    + "Sinh viên";

                                    if (row != rowCount)
                                        multiInsertLog += "<delete>";

                                    //send mail
                                    if (studentInsert.STU_EMAIL != null)
                                    {
                                        List<Information> listInformation = await _context.Informations.
                                            Where(x => x.INF_ISMULTI == true && x.INF_ISDELETED == false).ToListAsync();

                                        MailAddress to = new MailAddress(studentInsert.STU_EMAIL);
                                        MailAddress from = new MailAddress("hcmunre.fake@gmail.com");

                                        MailMessage message = new MailMessage(from, to);
                                        message.Subject = "Cấp tài khoản cho sinh viên " + studentInsert.STU_LASTNAME + " " + studentInsert.STU_FIRSTNAME;
                                        message.Body = "Kính gửi " + (studentInsert.STU_GENDER == true ? "anh" : "chị") + " <span class='text-capitalize'>" +
                                            studentInsert.STU_LASTNAME + " " + studentInsert.STU_FIRSTNAME + "</span>,<br>" +
                                            listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + " thông báo: <br> " +
                                            "Bạn đã được cấp tài khoản sử dụng phần mềm thi trắc nghiệm online, " +
                                            "vui lòng truy cập và sử dụng tại địa chỉ " +
                                            "<a href='" + Utils.Helper.Issuer + "'>" + Utils.Helper.Issuer + "</a> với tài khoản:<br>" +
                                            "<span class='pl-4'><b>Tên tài khoản:</b> " + userInsert.USE_USERNAME + "</span><br>" +
                                            "<span class='pl-4'><b>Mật khẩu:</b> " + rawPassword + "</span><br>" +
                                            "<br>Mọi chi tiết thắc mắc vui lòng liên hệ:<br>" + listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + "<br>" +
                                            "Số điện thoại: " + listInformation.Where(x => x.INF_KEYNAME == "Số điện thoại").FirstOrDefault().INF_VALUE + "<br>" +
                                            "Email: " + listInformation.Where(x => x.INF_KEYNAME == "Email").FirstOrDefault().INF_VALUE + "<br>" +
                                            "Địa chỉ: " + listInformation.Where(x => x.INF_KEYNAME == "Địa chỉ").FirstOrDefault().INF_VALUE;
                                        message.IsBodyHtml = true;
                                        //message.Attachments.Add(new Attachment(_hostingEnv.ContentRootPath + "\\Data\\Users\\default_avatar.png"));

                                        SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
                                        {
                                            Credentials = new NetworkCredential("hcmunre.fake@gmail.com", "tnmt@12345"),
                                            EnableSsl = true
                                        };

                                        try
                                        {
                                            await client.SendMailAsync(message);
                                        }
                                        catch (SmtpException ex) { Console.WriteLine(ex.ToString()); }
                                    }
                                }

                                Log log = new Log();
                                log.LOG_ACTID = 1;
                                log.LOG_TABID = 14;
                                log.LOG_MODULE = "Thêm danh sách sinh viên";
                                log.LOG_USEID = user;
                                log.LOG_DATE = DateTime.Now;
                                log.LOG_CONTENT = title + "<divider>" + multiInsertLog;
                                _context.Logs.Add(log);
                                await _context.SaveChangesAsync();

                                transaction.Commit();

                                //real time reload
                                await _hubContext.Clients.All.SendAsync("GetLog", true);
                                await _hubContext.Clients.All.SendAsync("GetStudent", true);
                            }
                            else
                                return new BaseResponse
                                {
                                    errorCode = 405,
                                    message = "Dữ liệu không hợp lệ!<divider>" + errorLog
                                };
                        }
                        catch (Exception)
                        {
                            return new BaseResponse
                            {
                                errorCode = 500,
                                message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                            };
                        }
                    }
                }
            }

            return new BaseResponse { };
        }
        [HttpPost("exportExcelLinkDownload")]
        public ActionResult<BaseResponse> Export(CancellationToken cancellationToken, Student[] listStudentRequest)
        {
            string excelFileName = $"{DateTime.Now.ToString("yyyyMMddHHmmssfff")}_Danh sách sinh viên.xlsx";
            string physicalPath = Path.Combine(_hostingEnv.ContentRootPath, "Data\\Excels\\", excelFileName);

            if (!Directory.Exists(Path.Combine(_hostingEnv.ContentRootPath, "Data\\Excels", "")))
                Directory.CreateDirectory(Path.Combine(_hostingEnv.ContentRootPath, "Data\\Excels", ""));

            DirectoryInfo dir = new DirectoryInfo(Path.Combine(_hostingEnv.ContentRootPath, "Data\\Excels", ""));
            FileInfo[] files = dir.GetFiles("*Danh sách sinh viên*", SearchOption.TopDirectoryOnly);
            foreach (var item in files)
            {
                if (item.Exists)
                {
                    item.Delete();
                }
            }

            FileInfo file = new FileInfo(physicalPath);

            string downloadUrl = string.Format("{0}/{1}/{2}", Utils.Helper.GetBaseUrl(Request), "Data/Excels", excelFileName);

            // format data
            List<StudentData> listStudentResult = new List<StudentData>();

            if (listStudentRequest.Length == 0)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            for (int i = 0; i < listStudentRequest.Length; i++)
            {
                StudentData stuData = new StudentData();
                stuData.stt = (i + 1);
                stuData.code = listStudentRequest[i].STU_CODE;
                stuData.identity = listStudentRequest[i].STU_IDENTITY;
                stuData.lastname = listStudentRequest[i].STU_LASTNAME;
                stuData.firstname = listStudentRequest[i].STU_FIRSTNAME;
                stuData.gender = (listStudentRequest[i].STU_GENDER == true ? "Nam" : "Nữ");
                stuData.dob = (listStudentRequest[i].STU_DATEOFBIRTH.GetValueOrDefault().Day + "/" +
                    listStudentRequest[i].STU_DATEOFBIRTH.GetValueOrDefault().Month + "/" +
                    listStudentRequest[i].STU_DATEOFBIRTH.GetValueOrDefault().Year);
                stuData.phone = listStudentRequest[i].STU_PHONE;
                stuData.email = listStudentRequest[i].STU_EMAIL;
                stuData.address = listStudentRequest[i].STU_ADDRESS;
                stuData.schoolyear = listStudentRequest[i].SchoolYears.SCYE_CODE;
                stuData.branch = listStudentRequest[i].Branches.BRA_NAME;
                stuData.classs = listStudentRequest[i].Classes.CLA_CODE;
                stuData.username = listStudentRequest[i].Users.USE_USERNAME;
                stuData.accountstatus = (listStudentRequest[i].Users.USE_STATUS == 1 ? "Sử dụng" : "Khóa");
                for (int j = 0; j < listStudentRequest[i].Users.User_Permissions.Count; j++)
                {
                    stuData.permission += listStudentRequest[i].Users.User_Permissions.ElementAt(j).Permissions.PER_NAME;
                    if (j != listStudentRequest[i].Users.User_Permissions.Count - 1)
                        stuData.permission += ", ";
                }
                listStudentResult.Add(stuData);
            }
            using (var package = new ExcelPackage(file))
            {
                var worksheet = package.Workbook.Worksheets.Add("Sheet1");
                worksheet.Cells.LoadFromCollection(listStudentResult, true);

                var allCells = worksheet.Cells[worksheet.Dimension.Address];
                allCells.AutoFilter = true;

                // Auto-fit all the columns
                allCells.AutoFitColumns();

                var headerCells = worksheet.Cells[1, 1, 1, worksheet.Dimension.Columns];
                headerCells.Style.Font.Bold = true;
                headerCells.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                headerCells.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);

                package.Save();
            }

            return new BaseResponse
            {
                data = downloadUrl
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(StudentRequest studentRequest)
        {
            string rawPassword = studentRequest.user.USE_PASSWORD;

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = studentRequest.student.STU_CREATEDBY.GetValueOrDefault();
            log.LOG_ACTID = 1;
            log.LOG_TABID = 14;
            log.LOG_MODULE = "Thêm sinh viên";
            string title = "<b>Mã sinh viên:</b><title>" +
                "<b>CMND:</b><title>" +
                "<b>Họ:</b><title>" +
                "<b>Tên:</b><title>" +
                "<b>Ngày sinh:</b><title>" +
                "<b>Giới tính:</b><title>" +
                "<b>Số điện thoại:</b><title>" +
                "<b>Địa chỉ:</b><title>" +
                "<b>Email:</b><title>" +
                "<b>Niên khóa:<b><title>" +
                "<b>Ngành:</b><title>" +
                "<b>Lớp:</b><title>" +
                "<b>Tên tài khoản:</b><title>" +
                "<b>Trạng thái tài khoản:</b><title>" +
                "<b>Vai trò:</b>";
            string insertLog = studentRequest.student.STU_CODE + "<item>"
                + studentRequest.student.STU_IDENTITY + "<item>"
                + studentRequest.student.STU_LASTNAME + "<item>"
                + studentRequest.student.STU_FIRSTNAME + "<item>"
                + studentRequest.student.STU_DATEOFBIRTH.GetValueOrDefault().Day + "/" + studentRequest.student.STU_DATEOFBIRTH.GetValueOrDefault().Month + "/" + studentRequest.student.STU_DATEOFBIRTH.GetValueOrDefault().Year + "<item>"
                + (studentRequest.student.STU_GENDER == true ? "Nam" : "Nữ") + "<item>"
                + studentRequest.student.STU_PHONE + "<item>"
                + studentRequest.student.STU_ADDRESS + "<item>"
                + studentRequest.student.STU_EMAIL + "<item>"
                + _context.SchoolYears.FirstOrDefault(x => x.Id == studentRequest.student.STU_SCYEID).SCYE_CODE + "<item>"
                + _context.Branches.FirstOrDefault(x => x.Id == studentRequest.student.STU_BRAID).BRA_NAME + "<item>"
                + _context.Classes.FirstOrDefault(x => x.Id == studentRequest.student.STU_CLAID).CLA_CODE + "<item>"
                + studentRequest.user.USE_USERNAME + "<item>"
                + (studentRequest.user.USE_STATUS == 1 ? "Sử dụng" : "Khóa") + "<item>";

            for (int i = 0; i < studentRequest.user_permissions.Length; i++)
            {
                insertLog += _context.Permissions.FirstOrDefault(x => x.Id == studentRequest.user_permissions[i]).PER_NAME;
                if (i != studentRequest.user_permissions.Length - 1)
                    insertLog += ", ";
            }

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //user
                    studentRequest.user.USE_PASSWORD = Utils.Helper.GenHash(studentRequest.user.USE_PASSWORD);
                    studentRequest.user.USE_CREATEDDATE = DateTime.Now;
                    studentRequest.user.USE_ISDELETED = false;

                    _context.Users.Add(studentRequest.user);
                    await _context.SaveChangesAsync();

                    //student
                    studentRequest.student.STU_CREATEDDATE = DateTime.Now;
                    studentRequest.student.STU_ISDELETED = false;
                    studentRequest.student.STU_USEID = studentRequest.user.Id;

                    _context.Students.Add(studentRequest.student);
                    await _context.SaveChangesAsync();

                    //user_permission
                    for (int i = 0; i < studentRequest.user_permissions.Length; i++)
                    {
                        User_Permission userPermission = new User_Permission();
                        userPermission.USPE_CREATEDBY = studentRequest.user.USE_CREATEDBY;
                        userPermission.USPE_CREATEDDATE = DateTime.Now;
                        userPermission.USPE_USEID = studentRequest.user.Id;
                        userPermission.USPE_STATUS = 1;
                        userPermission.USPE_PERID = studentRequest.user_permissions[i];

                        _context.User_Permissions.Add(userPermission);
                        await _context.SaveChangesAsync();
                    }

                    //log
                    log.LOG_CONTENT = title + "<divider>" + insertLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    //nofication
                    Nofication nofication = new Nofication();
                    nofication.NOF_USEID = studentRequest.user.Id;
                    nofication.NOF_CREATEDBY = studentRequest.student.STU_CREATEDBY;
                    nofication.NOF_LINK = "/info_user";
                    nofication.NOF_CREATEDDATE = DateTime.Now;
                    nofication.NOF_STATUS = 1;
                    nofication.NOF_TITLE = "Thông báo bảo mật tài khoản";
                    nofication.NOF_CONTENT = "Tài khoản của bạn vừa được tạo, vui lòng đổi mật khẩu để đảm bảo tài khoản an toàn!";
                    _context.Nofications.Add(nofication);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetStudent", true);

                    if (studentRequest.student.STU_EMAIL != null)
                    {
                        List<Information> listInformation = await _context.Informations.
                            Where(x => x.INF_ISMULTI == true && x.INF_ISDELETED == false).ToListAsync();

                        MailAddress to = new MailAddress(studentRequest.student.STU_EMAIL);
                        MailAddress from = new MailAddress("hcmunre.fake@gmail.com");

                        MailMessage message = new MailMessage(from, to);
                        message.Subject = "Cấp tài khoản cho sinh viên " + studentRequest.student.STU_LASTNAME + " " + studentRequest.student.STU_FIRSTNAME;
                        message.Body = "Kính gửi " + (studentRequest.student.STU_GENDER == true ? "anh" : "chị") + " <span class='text-capitalize'>" +
                            studentRequest.student.STU_LASTNAME + " " + studentRequest.student.STU_FIRSTNAME + "</span>,<br>" +
                            listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + " thông báo: <br> " +
                            "Bạn đã được cấp tài khoản sử dụng phần mềm thi trắc nghiệm online, " +
                            "vui lòng truy cập và sử dụng tại địa chỉ " +
                            "<a href='" + Utils.Helper.Issuer + "'>" + Utils.Helper.Issuer + "</a> với tài khoản:<br>" +
                            "<span class='pl-4'><b>Tên tài khoản:</b> " + studentRequest.user.USE_USERNAME + "</span><br>" +
                            "<span class='pl-4'><b>Mật khẩu:</b> " + rawPassword + "</span><br>" +
                            "<br>Mọi chi tiết thắc mắc vui lòng liên hệ:<br>" + listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + "<br>" +
                            "Số điện thoại: " + listInformation.Where(x => x.INF_KEYNAME == "Số điện thoại").FirstOrDefault().INF_VALUE + "<br>" +
                            "Email: " + listInformation.Where(x => x.INF_KEYNAME == "Email").FirstOrDefault().INF_VALUE + "<br>" +
                            "Địa chỉ: " + listInformation.Where(x => x.INF_KEYNAME == "Địa chỉ").FirstOrDefault().INF_VALUE;
                        message.IsBodyHtml = true;
                        //message.Attachments.Add(new Attachment(_hostingEnv.ContentRootPath + "\\Data\\Users\\default_avatar.png"));

                        SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
                        {
                            Credentials = new NetworkCredential("hcmunre.fake@gmail.com", "tnmt@12345"),
                            EnableSsl = true
                        };

                        try
                        {
                            await client.SendMailAsync(message);
                        }
                        catch (SmtpException ex) { Console.WriteLine(ex.ToString()); }
                    }
                }
                catch (Exception) { }
            };

            //speedsms
            //if (employeeRequest.employee.EMP_PHONE != null)
            //{
            //    SpeedSMSAPI api = new SpeedSMSAPI();

            //    String[] phones = new String[] { employeeRequest.employee.EMP_PHONE };
            //    String str = "Bạn đã được cấp tài khoản sử dụng phần mềm quản lý thi trắc nghiệm online " +
            //        "(truy cập " + employeeRequest.employee.origin + " để sử dụng) bởi Trường Đại học Tài nguyên & Môi trường" +
            //        "tp Hồ Chí Minh. Tên tài khoản: " + employeeRequest.user.USE_USERNAME + ", Mật khẩu: " + rawPassword;
            //    String response = api.sendSMS(phones, str, 3, "HCMUNRE");
            //    Console.WriteLine(response);
            //    Console.ReadLine();
            //}

            return new BaseResponse
            {
                data = studentRequest.student
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(long id, StudentRequest studentRequest)
        {
            Student studentDB = await _context.Students.Where(x => x.Id == id && x.STU_ISDELETED == false).
                Include(x => x.Branches).Include(x => x.Classes).Include(x => x.SchoolYears).
                Include(x => x.Users).FirstOrDefaultAsync();
            if (studentDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = studentRequest.student.STU_MODIFIEDBY.GetValueOrDefault();
            log.LOG_ACTID = 2;
            log.LOG_TABID = 14;
            log.LOG_MODULE = "Sửa thông tin sinh viên";

            string title = "<b>Mã sinh viên:</b><title>" +
                "<b>CMND:</b><title>" +
                "<b>Họ:</b><title>" +
                "<b>Tên:</b><title>" +
                "<b>Ngày sinh:</b><title>" +
                "<b>Giới tính:</b><title>" +
                "<b>Số điện thoại:</b><title>" +
                "<b>Địa chỉ:</b><title>" +
                "<b>Email:</b><title>" +
                "<b>Niên khóa:<b><title>" +
                "<b>Ngành:</b><title>" +
                "<b>Lớp:</b><title>" +
                "<b>Tên tài khoản:</b><title>" +
                "<b>Trạng thái tài khoản:</b><title>" +
                "<b>Vai trò:</b>";

            string updateLog = studentDB.STU_CODE + "<to>" + studentRequest.student.STU_CODE + "<item>" +
                studentDB.STU_IDENTITY + "<to>" + studentRequest.student.STU_IDENTITY + "<item>" +
                studentDB.STU_LASTNAME + "<to>" + studentRequest.student.STU_LASTNAME + "<item>" +
                studentDB.STU_FIRSTNAME + "<to>" + studentRequest.student.STU_FIRSTNAME + "<item>" +
                studentDB.STU_DATEOFBIRTH.GetValueOrDefault().Day + "/" + studentDB.STU_DATEOFBIRTH.GetValueOrDefault().Month + "/" + studentDB.STU_DATEOFBIRTH.GetValueOrDefault().Year + "<to>" +
                studentRequest.student.STU_DATEOFBIRTH.GetValueOrDefault().Day + "/" + studentRequest.student.STU_DATEOFBIRTH.GetValueOrDefault().Month + "/" + studentRequest.student.STU_DATEOFBIRTH.GetValueOrDefault().Year + "<item>" +
                (studentDB.STU_GENDER == true ? "Nam" : "Nữ") + "<to>" + (studentRequest.student.STU_GENDER == true ? "Nam" : "Nữ") + "<item>" +
                studentDB.STU_PHONE + "<to>" + studentRequest.student.STU_PHONE + "<item>" +
                studentDB.STU_ADDRESS + "<to>" + studentRequest.student.STU_ADDRESS + "<item>" +
                studentDB.STU_EMAIL + "<to>" + studentRequest.student.STU_EMAIL + "<item>" +
                studentDB.SchoolYears.SCYE_CODE + "<to>" + _context.SchoolYears.FirstOrDefault(x => x.Id == studentRequest.student.STU_SCYEID).SCYE_CODE + "<item>" +
                studentDB.Branches.BRA_NAME + "<to>" + _context.Branches.FirstOrDefault(x => x.Id == studentRequest.student.STU_BRAID).BRA_NAME + "<item>" +
                studentDB.Classes.CLA_CODE + "<to>" + _context.Classes.FirstOrDefault(x => x.Id == studentRequest.student.STU_CLAID).CLA_CODE + "<item>" +
                studentDB.Users.USE_USERNAME + "<to>" + studentRequest.user.USE_USERNAME + "<item>" +
                (studentDB.Users.USE_STATUS == 1 ? "Sử dụng" : "Khóa") + "<to>" + (studentRequest.user.USE_STATUS == 1 ? "Sử dụng" : "Khóa") + "<item>";

            var listUserPermission = await _context.User_Permissions.
                Where(x => x.USPE_USEID == studentDB.Users.Id).Include(x => x.Permissions).ToListAsync();
            for (int i = 0; i < listUserPermission.Count; i++)
            {
                updateLog += listUserPermission.ElementAt(i).Permissions.PER_NAME;
                if (i != listUserPermission.Count - 1)
                    updateLog += ", ";
            }
            updateLog += "<to>";
            for (int i = 0; i < studentRequest.user_permissions.Length; i++)
            {
                var permission = await _context.Permissions.FirstOrDefaultAsync(x => x.Id == studentRequest.user_permissions[i]);
                updateLog += permission.PER_NAME;
                if (i != studentRequest.user_permissions.Length - 1)
                    updateLog += ", ";
            }

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //user
                    studentDB.Users.USE_STATUS = studentRequest.user.USE_STATUS;
                    studentDB.Users.USE_MODIFIEDBY = studentRequest.user.USE_MODIFIEDBY;
                    studentDB.Users.USE_MODIFIEDDATE = DateTime.Now;
                    await _context.SaveChangesAsync();

                    //student
                    studentDB.STU_FIRSTNAME = studentRequest.student.STU_FIRSTNAME;
                    studentDB.STU_LASTNAME = studentRequest.student.STU_LASTNAME;
                    studentDB.STU_DATEOFBIRTH = studentRequest.student.STU_DATEOFBIRTH;
                    studentDB.STU_GENDER = studentRequest.student.STU_GENDER;
                    studentDB.STU_EMAIL = studentRequest.student.STU_EMAIL;
                    studentDB.STU_PHONE = studentRequest.student.STU_PHONE;
                    studentDB.STU_IDENTITY = studentRequest.student.STU_IDENTITY;
                    studentDB.STU_ADDRESS = studentRequest.student.STU_ADDRESS;
                    studentDB.STU_AVATAR = studentRequest.student.STU_AVATAR;
                    studentDB.STU_MODIFIEDBY = studentRequest.student.STU_MODIFIEDBY;
                    studentDB.STU_MODIFIEDDATE = DateTime.Now;
                    studentDB.STU_CLAID = studentRequest.student.STU_CLAID;
                    await _context.SaveChangesAsync();

                    //user permission
                    if (listUserPermission.Count == studentRequest.user_permissions.Length)
                    {
                        for (int i = 0; i < listUserPermission.Count; i++)
                        {
                            listUserPermission.ElementAt(i).USPE_PERID = studentRequest.user_permissions[i];
                            listUserPermission.ElementAt(i).USPE_MODIFIEDBY = studentRequest.student.STU_MODIFIEDBY;
                            listUserPermission.ElementAt(i).USPE_MODIFIEDDATE = DateTime.Now;
                            await _context.SaveChangesAsync();
                        }
                    }
                    else
                    {
                        if (listUserPermission.Count > studentRequest.user_permissions.Length)
                        {
                            for (int i = 0; i < listUserPermission.Count; i++)
                            {
                                if (i <= studentRequest.user_permissions.Length - 1)
                                {
                                    listUserPermission.ElementAt(i).USPE_PERID = studentRequest.user_permissions[i];
                                    listUserPermission.ElementAt(i).USPE_MODIFIEDBY = studentRequest.student.STU_MODIFIEDBY;
                                    listUserPermission.ElementAt(i).USPE_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                                else
                                {
                                    _context.User_Permissions.Remove(listUserPermission.ElementAt(i));
                                    await _context.SaveChangesAsync();
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < studentRequest.user_permissions.Length; i++)
                            {
                                if (i <= listUserPermission.Count - 1)
                                {
                                    listUserPermission.ElementAt(i).USPE_PERID = studentRequest.user_permissions[i];
                                    listUserPermission.ElementAt(i).USPE_MODIFIEDBY = studentRequest.student.STU_MODIFIEDBY;
                                    listUserPermission.ElementAt(i).USPE_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                                else
                                {
                                    User_Permission userPermission = new User_Permission();
                                    userPermission.USPE_USEID = studentRequest.user.Id;
                                    userPermission.USPE_PERID = studentRequest.user_permissions[i];
                                    userPermission.USPE_CREATEDBY = studentRequest.student.STU_MODIFIEDBY;
                                    userPermission.USPE_CREATEDDATE = DateTime.Now;
                                    userPermission.USPE_STATUS = 1;
                                    _context.User_Permissions.Add(userPermission);
                                    await _context.SaveChangesAsync();
                                }
                            }
                        }
                    }

                    //log
                    log.LOG_CONTENT = title + "<divider>" + updateLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    //nofication
                    Nofication nofication = new Nofication();
                    nofication.NOF_USEID = studentRequest.user.Id;
                    nofication.NOF_CREATEDBY = studentRequest.student.STU_MODIFIEDBY;
                    nofication.NOF_CREATEDDATE = DateTime.Now;
                    nofication.NOF_STATUS = 1;
                    nofication.NOF_LINK = "/info_user";
                    nofication.NOF_TITLE = "Thông báo thay đổi thông tin tài khoản";
                    if (studentDB.Users.USE_STATUS != studentRequest.user.USE_STATUS)
                    {
                        if (studentRequest.user.USE_STATUS == 1)
                        {
                            nofication.NOF_CONTENT = "Tài khoản của bạn vừa được mở khóa bởi tài khoản <b>" +
                                _context.Users.FirstOrDefault(x => x.Id == studentRequest.user.USE_MODIFIEDBY).USE_USERNAME +
                                "</b> - Nhân viên <b>" + _context.Employees.FirstOrDefault(x => x.EMP_USEID == studentRequest.user.USE_MODIFIEDBY).EMP_LASTNAME +
                                " " + _context.Employees.FirstOrDefault(x => x.EMP_USEID == studentRequest.user.USE_MODIFIEDBY).EMP_FIRSTNAME +
                                "</b>, vui lòng kiểm tra lại thông tin tài khoản (Icon tài khoản -> Thông tin tài khoản)!";

                            _context.Nofications.Add(nofication);
                            await _context.SaveChangesAsync();
                        }
                    }
                    else
                    {
                        nofication.NOF_CONTENT = "Tài khoản của bạn vừa được chỉnh sửa bởi tài khoản <b>" +
                            _context.Users.FirstOrDefault(x => x.Id == studentRequest.user.USE_MODIFIEDBY).USE_USERNAME +
                            "</b> - Nhân viên <b>" + _context.Employees.FirstOrDefault(x => x.EMP_USEID == studentRequest.user.USE_MODIFIEDBY).EMP_LASTNAME +
                            " " + _context.Employees.FirstOrDefault(x => x.EMP_USEID == studentRequest.user.USE_MODIFIEDBY).EMP_FIRSTNAME +
                            "</b>, vui lòng kiểm tra lại thông tin tài khoản (Icon tài khoản -> Thông tin tài khoản)!";

                        _context.Nofications.Add(nofication);
                        await _context.SaveChangesAsync();
                    }

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetStudent", true);
                    await _hubContext.Clients.All.SendAsync("GetNofication", true);

                    if (studentRequest.student.STU_EMAIL != null)
                    {
                        List<Information> listInformation = await _context.Informations.
                            Where(x => x.INF_ISMULTI == true && x.INF_ISDELETED == false).ToListAsync();

                        MailAddress to = new MailAddress(studentRequest.student.STU_EMAIL);
                        MailAddress from = new MailAddress("hcmunre.fake@gmail.com");

                        MailMessage message = new MailMessage(from, to);
                        message.Subject = "Thông báo chỉnh sửa thông tin tài khoản sinh viên " + studentRequest.student.STU_LASTNAME + " " + studentRequest.student.STU_FIRSTNAME;
                        if (studentDB.Users.USE_STATUS != studentRequest.user.USE_STATUS)
                        {
                            if (studentRequest.user.USE_STATUS == 1)
                            {
                                message.Body = "Kính gửi " + (studentRequest.student.STU_GENDER == true ? "anh" : "chị") + " <span class='text-capitalize'>" +
                                    studentRequest.student.STU_LASTNAME + " " + studentRequest.student.STU_FIRSTNAME + "</span>,<br>" +
                                    listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + " thông báo: <br> " +
                                    "Tài khoản của bạn đã được mở khóa bởi tài khoản <b>" + _context.Users.FirstOrDefault(x => x.Id == studentRequest.user.USE_MODIFIEDBY).USE_USERNAME +
                                    "</b> - Nhân viên <b>" + _context.Employees.FirstOrDefault(x => x.EMP_USEID == studentRequest.user.USE_MODIFIEDBY).EMP_LASTNAME +
                                    " " + _context.Employees.FirstOrDefault(x => x.EMP_USEID == studentRequest.user.USE_MODIFIEDBY).EMP_FIRSTNAME +
                                    "</b>, vui lòng truy cập địa chỉ " +
                                    "<a href='" + Utils.Helper.Issuer + "'>" + Utils.Helper.Issuer + "</a> để kiểm tra thông tin tài khoản<br>" +
                                    "<br>Mọi chi tiết thắc mắc vui lòng liên hệ:<br>" + listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + "<br>" +
                                    "Số điện thoại: " + listInformation.Where(x => x.INF_KEYNAME == "Số điện thoại").FirstOrDefault().INF_VALUE + "<br>" +
                                    "Email: " + listInformation.Where(x => x.INF_KEYNAME == "Email").FirstOrDefault().INF_VALUE + "<br>" +
                                    "Địa chỉ: " + listInformation.Where(x => x.INF_KEYNAME == "Địa chỉ").FirstOrDefault().INF_VALUE;
                            }
                            else
                            {
                                message.Body = "Kính gửi " + (studentRequest.student.STU_GENDER == true ? "anh" : "chị") + " <span class='text-capitalize'>" +
                                    studentRequest.student.STU_LASTNAME + " " + studentRequest.student.STU_FIRSTNAME + "</span>,<br>" +
                                    listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + " thông báo: <br> " +
                                    "Tài khoản của bạn đã bị khóa tạm thời bởi tài khoản <b>" + _context.Users.FirstOrDefault(x => x.Id == studentRequest.user.USE_MODIFIEDBY).USE_USERNAME +
                                    "</b> - Nhân viên <b>" + _context.Employees.FirstOrDefault(x => x.EMP_USEID == studentRequest.user.USE_MODIFIEDBY).EMP_LASTNAME +
                                    " " + _context.Employees.FirstOrDefault(x => x.EMP_USEID == studentRequest.user.USE_MODIFIEDBY).EMP_FIRSTNAME +
                                    "</b>, vui lòng liên hệ để biết thêm chi tiết!<br>" +
                                    "<br>Mọi chi tiết thắc mắc vui lòng liên hệ:<br>" + listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + "<br>" +
                                    "Số điện thoại: " + listInformation.Where(x => x.INF_KEYNAME == "Số điện thoại").FirstOrDefault().INF_VALUE + "<br>" +
                                    "Email: " + listInformation.Where(x => x.INF_KEYNAME == "Email").FirstOrDefault().INF_VALUE + "<br>" +
                                    "Địa chỉ: " + listInformation.Where(x => x.INF_KEYNAME == "Địa chỉ").FirstOrDefault().INF_VALUE;
                            }
                        }
                        else
                        {
                            message.Body = "Kính gửi " + (studentRequest.student.STU_GENDER == true ? "anh" : "chị") + " <span class='text-capitalize'>" +
                                studentRequest.student.STU_LASTNAME + " " + studentRequest.student.STU_FIRSTNAME + "</span>,<br>" +
                                listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + " thông báo: <br> " +
                                "Thông tin tài khoản của bạn đã được chỉnh sửa bởi tài khoản <b>" + _context.Users.FirstOrDefault(x => x.Id == studentRequest.user.USE_MODIFIEDBY).USE_USERNAME +
                                "</b> - Nhân viên <b>" + _context.Employees.FirstOrDefault(x => x.EMP_USEID == studentRequest.user.USE_MODIFIEDBY).EMP_LASTNAME +
                                " " + _context.Employees.FirstOrDefault(x => x.EMP_USEID == studentRequest.user.USE_MODIFIEDBY).EMP_FIRSTNAME +
                                "</b>, vui lòng truy cập địa chỉ " +
                                "<a href='" + Utils.Helper.Issuer + "'>" + Utils.Helper.Issuer + "</a> để kiểm tra thông tin chỉnh sửa<br>" +
                                "<br>Mọi chi tiết thắc mắc vui lòng liên hệ:<br>" + listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + "<br>" +
                                "Số điện thoại: " + listInformation.Where(x => x.INF_KEYNAME == "Số điện thoại").FirstOrDefault().INF_VALUE + "<br>" +
                                "Email: " + listInformation.Where(x => x.INF_KEYNAME == "Email").FirstOrDefault().INF_VALUE + "<br>" +
                                "Địa chỉ: " + listInformation.Where(x => x.INF_KEYNAME == "Địa chỉ").FirstOrDefault().INF_VALUE;
                        }

                        message.IsBodyHtml = true;
                        //message.Attachments.Add(new Attachment(_hostingEnv.ContentRootPath + "\\Data\\Users\\default_avatar.png"));

                        SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
                        {
                            Credentials = new NetworkCredential("hcmunre.fake@gmail.com", "tnmt@12345"),
                            EnableSsl = true
                        };

                        try
                        {
                            await client.SendMailAsync(message);
                        }
                        catch (SmtpException ex) { Console.WriteLine(ex.ToString()); }
                    }
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 1,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse
            {
                data = studentDB
            };
        }
        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long id, long user)
        {
            Student studentDB = await _context.Students.Include(x => x.Users).
                Include(x => x.Branches).Include(x => x.Classes).Include(x => x.SchoolYears).
                Include(x => x.Student_Tests).
                FirstOrDefaultAsync(x => x.Id == id);

            if (studentDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 14;
            log.LOG_MODULE = "Xóa sinh viên";

            string title = "<b>Mã sinh viên:</b><title>" +
                "<b>CMND:</b><title>" +
                "<b>Họ:</b><title>" +
                "<b>Tên:</b><title>" +
                "<b>Ngày sinh:</b><title>" +
                "<b>Giới tính:</b><title>" +
                "<b>Số điện thoại:</b><title>" +
                "<b>Địa chỉ:</b><title>" +
                "<b>Email:</b><title>" +
                "<b>Niên khóa:<b><title>" +
                "<b>Ngành:</b><title>" +
                "<b>Lớp:</b><title>" +
                "<b>Tên tài khoản:</b><title>" +
                "<b>Trạng thái tài khoản:</b><title>" +
                "<b>Vai trò:</b>";
            string deleteLog = studentDB.STU_CODE + "<item>"
                + studentDB.STU_IDENTITY + "<item>"
                + studentDB.STU_LASTNAME + "<item>"
                + studentDB.STU_FIRSTNAME + "<item>"
                + studentDB.STU_DATEOFBIRTH.GetValueOrDefault().Day + "/" + studentDB.STU_DATEOFBIRTH.GetValueOrDefault().Month + "/" + studentDB.STU_DATEOFBIRTH.GetValueOrDefault().Year + "<item>"
                + (studentDB.STU_GENDER == true ? "Nam" : "Nữ") + "<item>"
                + studentDB.STU_PHONE + "<item>"
                + studentDB.STU_ADDRESS + "<item>"
                + studentDB.STU_EMAIL + "<item>"
                + studentDB.SchoolYears.SCYE_CODE + "<item>"
                + studentDB.Branches.BRA_NAME + "<item>"
                + studentDB.Classes.CLA_CODE + "<item>"
                + studentDB.Users.USE_USERNAME + "<item>"
                + (studentDB.Users.USE_STATUS == 1 ? "Sử dụng" : "Khóa") + "<item>";

            List<User_Permission> listUserPermission = await _context.User_Permissions.
                Where(x => x.USPE_USEID == studentDB.Users.Id).Include(x => x.Permissions).ToListAsync();
            for (int i = 0; i < listUserPermission.Count; i++)
            {
                deleteLog += listUserPermission.ElementAt(i).Permissions.PER_NAME;
                if (i != listUserPermission.Count - 1)
                    deleteLog += ", ";
            }
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    studentDB.STU_MODIFIEDBY = user;
                    studentDB.STU_MODIFIEDDATE = DateTime.Now;
                    studentDB.STU_ISDELETED = true;
                    studentDB.Users.USE_MODIFIEDBY = user;
                    studentDB.Users.USE_MODIFIEDDATE = DateTime.Now;
                    studentDB.Users.USE_ISDELETED = true;
                    studentDB.Users.USE_STATUS = 2;
                    await _context.SaveChangesAsync();

                    log.LOG_CONTENT = title + "<divider>" + deleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetStudent", true);

                    transaction.Commit();

                    if (studentDB.STU_EMAIL != null)
                    {
                        List<Information> listInformation = await _context.Informations.
                            Where(x => x.INF_ISMULTI == true && x.INF_ISDELETED == false).ToListAsync();

                        MailAddress to = new MailAddress(studentDB.STU_EMAIL);
                        MailAddress from = new MailAddress("hcmunre.fake@gmail.com");

                        MailMessage message = new MailMessage(from, to);
                        message.Subject = "Khóa tài khoản nhân viên " + studentDB.STU_LASTNAME + " " + studentDB.STU_FIRSTNAME;
                        message.Body = "Kính gửi " + (studentDB.STU_GENDER == true ? "anh" : "chị") + " <span class='text-capitalize'>" +
                            studentDB.STU_LASTNAME + " " + studentDB.STU_FIRSTNAME + "</span>,<br>" +
                            listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + " thông báo: <br> " +
                            "Tài khoản <b>" + studentDB.Users.USE_USERNAME + "</b> của bạn đã bị xóa khỏi hệ thống bởi tài khoản <b>" +
                            _context.Users.FirstOrDefault(x => x.Id == user).USE_USERNAME +
                            "</b> - Nhân viên <b>" + _context.Employees.FirstOrDefault(x => x.EMP_USEID == user).EMP_LASTNAME +
                            " " + _context.Employees.FirstOrDefault(x => x.EMP_USEID == user).EMP_FIRSTNAME +
                            "</b>.<br>" +
                            "<br>Mọi chi tiết thắc mắc vui lòng liên hệ:<br>" + listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + "<br>" +
                            "Số điện thoại: " + listInformation.Where(x => x.INF_KEYNAME == "Số điện thoại").FirstOrDefault().INF_VALUE + "<br>" +
                            "Email: " + listInformation.Where(x => x.INF_KEYNAME == "Email").FirstOrDefault().INF_VALUE + "<br>" +
                            "Địa chỉ: " + listInformation.Where(x => x.INF_KEYNAME == "Địa chỉ").FirstOrDefault().INF_VALUE;
                        message.IsBodyHtml = true;
                        //message.Attachments.Add(new Attachment(_hostingEnv.ContentRootPath + "\\Data\\Users\\default_avatar.png"));

                        SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
                        {
                            Credentials = new NetworkCredential("hcmunre.fake@gmail.com", "tnmt@12345"),
                            EnableSsl = true
                        };

                        try
                        {
                            await client.SendMailAsync(message);
                        }
                        catch (SmtpException ex) { Console.WriteLine(ex.ToString()); }
                    }
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 1,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse
            {
                data = studentDB
            };
        }
        [HttpPost("getListStudentMultiDel")]
        public async Task<ActionResult<BaseResponse>> getListStudentMultiDel(int[] arrDel)
        {
            List<Student> listStu = new List<Student>();
            for (int i = 0; i < arrDel.Length; i++)
            {
                Student stu = await _context.Students.Where(x => x.Id == arrDel[i]).
                    Include(x => x.Users).
                    Include(x => x.Branches).Include(x => x.SchoolYears).Include(x => x.Classes).
                    AsNoTracking().
                    FirstOrDefaultAsync();
                if (stu != null)
                {
                    stu.Users.User_Permissions = await _context.User_Permissions.
                        Where(x => x.USPE_USEID == stu.Users.Id).Include(x => x.Permissions).AsNoTracking().ToListAsync();

                    listStu.Add(stu);
                }
            }
            return new BaseResponse
            {
                data = listStu
            };
        }
        [HttpPost("multipleDelete/{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long user, Student[] arrDel)
        {

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 14;
            log.LOG_MODULE = "Xóa danh sách sinh viên";

            string title = "<b>Mã sinh viên:</b><title>" +
                "<b>CMND:</b><title>" +
                "<b>Họ:</b><title>" +
                "<b>Tên:</b><title>" +
                "<b>Ngày sinh:</b><title>" +
                "<b>Giới tính:</b><title>" +
                "<b>Số điện thoại:</b><title>" +
                "<b>Địa chỉ:</b><title>" +
                "<b>Email:</b><title>" +
                "<b>Niên khóa:<b><title>" +
                "<b>Ngành:</b><title>" +
                "<b>Lớp:</b><title>" +
                "<b>Tên tài khoản:</b><title>" +
                "<b>Trạng thái tài khoản:</b><title>" +
                "<b>Vai trò:</b>";
            string multiDeleteLog = "";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i = 0; i < arrDel.Length; i++)
                    {
                        var studentDB = _context.Students.
                            Include(x => x.Users).
                            Include(x => x.Branches).Include(x => x.Classes).Include(x => x.SchoolYears).
                            Where(x => x.Id == arrDel[i].Id).FirstOrDefault();
                        if (studentDB != null)
                        {
                            multiDeleteLog += studentDB.STU_CODE + "<item>"
                                + studentDB.STU_IDENTITY + "<item>"
                                + studentDB.STU_LASTNAME + "<item>"
                                + studentDB.STU_FIRSTNAME + "<item>"
                                + studentDB.STU_DATEOFBIRTH.GetValueOrDefault().Day + "/" + studentDB.STU_DATEOFBIRTH.GetValueOrDefault().Month + "/" + studentDB.STU_DATEOFBIRTH.GetValueOrDefault().Year + "<item>"
                                + (studentDB.STU_GENDER == true ? "Nam" : "Nữ") + "<item>"
                                + studentDB.STU_PHONE + "<item>"
                                + studentDB.STU_ADDRESS + "<item>"
                                + studentDB.STU_EMAIL + "<item>"
                                + studentDB.SchoolYears.SCYE_CODE + "<item>"
                                + studentDB.Branches.BRA_NAME + "<item>"
                                + studentDB.Classes.CLA_CODE + "<item>"
                                + studentDB.Users.USE_USERNAME + "<item>"
                                + (studentDB.Users.USE_STATUS == 1 ? "Sử dụng" : "Khóa") + "<item>";

                            List<User_Permission> listUserPermimssion = await _context.User_Permissions.
                                Where(x => x.USPE_USEID == studentDB.Users.Id).Include(x => x.Permissions).ToListAsync();
                            for (int j = 0; j < listUserPermimssion.Count; j++)
                            {
                                multiDeleteLog += listUserPermimssion.ElementAt(j).Permissions.PER_NAME;
                                if (j != listUserPermimssion.Count - 1)
                                    multiDeleteLog += ", ";
                            }

                            if (i != arrDel.Length - 1)
                                multiDeleteLog += "<delete>";

                            studentDB.STU_MODIFIEDBY = user;
                            studentDB.STU_MODIFIEDDATE = DateTime.Now;
                            studentDB.STU_ISDELETED = true;
                            studentDB.Users.USE_MODIFIEDBY = user;
                            studentDB.Users.USE_MODIFIEDDATE = DateTime.Now;
                            studentDB.Users.USE_ISDELETED = true;
                            studentDB.Users.USE_STATUS = 2;
                            await _context.SaveChangesAsync();

                            if (studentDB.STU_EMAIL != null)
                            {
                                List<Information> listInformation = await _context.Informations.
                                    Where(x => x.INF_ISMULTI == true && x.INF_ISDELETED == false).ToListAsync();

                                MailAddress to = new MailAddress(studentDB.STU_EMAIL);
                                MailAddress from = new MailAddress("hcmunre.fake@gmail.com");

                                MailMessage message = new MailMessage(from, to);
                                message.Subject = "Khóa tài khoản nhân viên " + studentDB.STU_LASTNAME + " " + studentDB.STU_FIRSTNAME;
                                message.Body = "Kính gửi " + (studentDB.STU_GENDER == true ? "anh" : "chị") + " <span class='text-capitalize'>" +
                                    studentDB.STU_LASTNAME + " " + studentDB.STU_FIRSTNAME + "</span>,<br>" +
                                    listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + " thông báo: <br> " +
                                    "Tài khoản <b>" + studentDB.Users.USE_USERNAME + "</b> của bạn đã bị xóa khỏi hệ thống bởi tài khoản <b>" +
                                    _context.Users.FirstOrDefault(x => x.Id == user).USE_USERNAME +
                                    "</b> - Nhân viên <b>" + _context.Employees.FirstOrDefault(x => x.EMP_USEID == user).EMP_LASTNAME +
                                    " " + _context.Employees.FirstOrDefault(x => x.EMP_USEID == user).EMP_FIRSTNAME +
                                    "</b>.<br>" +
                                    "<br>Mọi chi tiết thắc mắc vui lòng liên hệ:<br>" + listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + "<br>" +
                                    "Số điện thoại: " + listInformation.Where(x => x.INF_KEYNAME == "Số điện thoại").FirstOrDefault().INF_VALUE + "<br>" +
                                    "Email: " + listInformation.Where(x => x.INF_KEYNAME == "Email").FirstOrDefault().INF_VALUE + "<br>" +
                                    "Địa chỉ: " + listInformation.Where(x => x.INF_KEYNAME == "Địa chỉ").FirstOrDefault().INF_VALUE;
                                message.IsBodyHtml = true;
                                //message.Attachments.Add(new Attachment(_hostingEnv.ContentRootPath + "\\Data\\Users\\default_avatar.png"));

                                SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
                                {
                                    Credentials = new NetworkCredential("hcmunre.fake@gmail.com", "tnmt@12345"),
                                    EnableSsl = true
                                };

                                try
                                {
                                    await client.SendMailAsync(message);
                                }
                                catch (SmtpException ex) { Console.WriteLine(ex.ToString()); }
                            }
                        }
                    }

                    log.LOG_CONTENT = title + "<divider>" + multiDeleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetStudent", true);
                }
                catch (Exception) { }
            }

            return new BaseResponse { };
        }
    }
}