﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TestDetailController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public TestDetailController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.TestDetails.Where(x => x.TEDE_ISDELETED == false).AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var testdetail = await _context.TestDetails.
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Where(x => x.TEDE_ISDELETED == false && x.Id == id).AsNoTracking().FirstOrDefaultAsync();
            if (testdetail == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (testdetail.UserCreated != null)
            {
                if (testdetail.UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    testdetail.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    testdetail.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + testdetail.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
            }

            return new BaseResponse
            {
                data = testdetail
            };
        }
        [HttpGet("getListByTestIdAndCode/{id}&{code}")]
        public async Task<ActionResult<BaseResponse>> getListByTestIdAndCode(long id, string code)
        {
            var testDetailList = await _context.TestDetails.
                Where(x => x.TEDE_ISDELETED == false && x.TEDE_TESID == id && x.TEDE_CODE == code).AsNoTracking().ToListAsync();
            if (testDetailList == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = testDetailList
            };
        }
    }
}