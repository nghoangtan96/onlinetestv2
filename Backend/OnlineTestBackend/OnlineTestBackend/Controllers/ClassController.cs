﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ClassController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public ClassController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Classes.
                Include(x => x.SchoolYears).Include(x => x.Branches).ThenInclude(x => x.Faculties).
                Where(x => x.CLA_ISDELETED == false).AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("getListByBranchIdAndSchoolYearId/{braid}&{scyeid}")]
        public async Task<ActionResult<BaseResponse>> getListByBranchIdAndSchoolYearId(int braid, int scyeid)
        {
            return new BaseResponse
            {
                data = await _context.Classes.
                Include(x => x.SchoolYears).Include(x => x.Branches).
                Where(x => x.CLA_ISDELETED == false && x.CLA_SCYEID == scyeid && x.CLA_BRAID == braid).AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var classs = await _context.Classes.Where(x => x.CLA_ISDELETED == false && x.Id == id).
                Include(x => x.Branches).ThenInclude(x => x.Faculties).Include(x => x.SchoolYears).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.UserModified).ThenInclude(x => x.Employees).
                AsNoTracking().
                FirstOrDefaultAsync();
            if (classs == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (classs.CLA_CREATEDBY != null)
            {
                if (classs.UserCreated.Employees.ElementAt(0).EMP_AVATAR != null)
                {
                    classs.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + classs.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
                else
                {
                    classs.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
            }

            if (classs.CLA_MODIFIEDBY != null)
            {
                if (classs.UserModified.Employees.ElementAt(0).EMP_AVATAR != null)
                {
                    classs.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + classs.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
                else
                {
                    classs.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
            }

            return new BaseResponse
            {
                data = classs
            };
        }
        [HttpPost("getListClassMultiDel")]
        public async Task<ActionResult<BaseResponse>> getListClassMultiDel(long[] arrDel)
        {
            List<Class> listCla = new List<Class>();
            for (int i = 0; i < arrDel.Length; i++)
            {
                Class cla = await _context.Classes.
                    Include(x => x.SchoolYears).Include(x => x.Branches).
                    Where(x => x.Id == arrDel[i]).AsNoTracking().FirstOrDefaultAsync();
                if (cla != null)
                {
                    listCla.Add(cla);
                }
            }
            return new BaseResponse
            {
                data = listCla
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(Class classs)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = classs.CLA_CREATEDBY.GetValueOrDefault();
            log.LOG_ACTID = 1;
            log.LOG_TABID = 13;
            log.LOG_MODULE = "Thêm lớp học";
            string title = "<b>Mã lớp học:</b><title>" +
                "<b>Niên khóa:</b><title>" +
                "<b>Ngành:</b>";

            string insertLog = classs.CLA_CODE + "<item>" +
                _context.SchoolYears.FirstOrDefault(x => x.Id == classs.CLA_SCYEID).SCYE_CODE + "<item>" +
                _context.Branches.FirstOrDefault(x => x.Id == classs.CLA_BRAID).BRA_NAME;

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    classs.CLA_CREATEDDATE = DateTime.Now;
                    classs.CLA_ISDELETED = false;
                    classs.CLA_STATUS = 1;

                    _context.Classes.Add(classs);
                    await _context.SaveChangesAsync();

                    log.LOG_CONTENT = title + "<divider>" + insertLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetClass", true);
                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 2,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            };

            return new BaseResponse
            {
                data = classs
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(long id, Class classs)
        {
            Class classDB = await _context.Classes.Include(x => x.Branches).Include(x => x.SchoolYears).
                Include(x => x.Students).
                FirstOrDefaultAsync(x => x.Id == id && x.CLA_ISDELETED == false);

            if (classDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };

            if (classDB.Students.Count != 0)
            {
                return new BaseResponse
                {
                    errorCode = 1,
                    message = "Không thể chỉnh sửa, lớp học đã được sử dụng!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = classs.CLA_MODIFIEDBY.GetValueOrDefault();
            log.LOG_ACTID = 2;
            log.LOG_TABID = 13;
            log.LOG_MODULE = "Sửa lớp học";
            string title = "<b>Mã lớp học:</b><title>" +
                "<b>Niên khóa:</b><title>" +
                "<b>Ngành:</b>";

            string updateLog = classDB.CLA_CODE + "<to>" + classs.CLA_CODE + "<item>" +
                classDB.SchoolYears.SCYE_CODE + "<to>" + classs.SchoolYears.SCYE_CODE + "<item>" +
                classDB.Branches.BRA_NAME + "<to>" + classs.Branches.BRA_NAME;

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    classDB.CLA_CODE = classs.CLA_CODE;
                    classDB.CLA_BRAID = classs.CLA_BRAID;
                    classDB.CLA_SCYEID = classs.CLA_SCYEID;
                    classDB.CLA_MODIFIEDBY = classs.CLA_MODIFIEDBY;
                    classDB.CLA_MODIFIEDDATE = DateTime.Now;
                    await _context.SaveChangesAsync();

                    log.LOG_CONTENT = title + "<divider>" + updateLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetClass", true);
                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 2,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            };

            return new BaseResponse
            {
                data = classs
            };
        }
        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long id, long user)
        {

            Class classDB = await _context.Classes.
                Include(x => x.SchoolYears).Include(x => x.Branches).
                Include(x => x.Students).
                FirstOrDefaultAsync(x => x.Id == id && x.CLA_ISDELETED == false);

            if (classDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 13;
            log.LOG_MODULE = "Xóa lớp học";

            string title = "<b>Mã lớp học:</b><title>" +
                "<b>Niên khóa:</b><title>" +
                "<b>Ngành:</b>";

            string deleteLog = classDB.CLA_CODE + "<item>"
                + classDB.SchoolYears.SCYE_CODE + "<item>"
                + classDB.Branches.BRA_NAME;

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    classDB.CLA_MODIFIEDBY = user;
                    classDB.CLA_MODIFIEDDATE = DateTime.Now;
                    classDB.CLA_ISDELETED = true;
                    await _context.SaveChangesAsync();

                    log.LOG_CONTENT = title + "<divider>" + deleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetClass", true);
                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 2,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            };

            return new BaseResponse
            {
                data = classDB
            };
        }
        [HttpPost("multipleDelete/{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long user, Class[] arrDel)
        {

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 13;
            log.LOG_MODULE = "Xóa danh sách lớp học";

            string title = "<b>Mã lớp học:</b><title>" +
                "<b>Niên khóa:</b><title>" +
                "<b>Ngành:</b>";

            string multiDeleteLog = "";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i = 0; i < arrDel.Length; i++)
                    {
                        var claDB = _context.Classes.
                            Include(x => x.Branches).Include(x => x.SchoolYears).Include(x => x.Students).
                            FirstOrDefault(x => x.Id == arrDel[i].Id);

                        if (claDB == null)
                        {
                            return new BaseResponse
                            {
                                errorCode = 404,
                                message = "Không tìm thấy dữ liệu!"
                            };
                        }

                        multiDeleteLog += claDB.CLA_CODE + "<item>"
                                + claDB.SchoolYears.SCYE_CODE + "<item>"
                                + claDB.Branches.BRA_NAME + "<item>";

                        claDB.CLA_MODIFIEDBY = user;
                        claDB.CLA_MODIFIEDDATE = DateTime.Now;
                        claDB.CLA_ISDELETED = true;
                        await _context.SaveChangesAsync();
                    }

                    log.LOG_CONTENT += title + "<divider>" + multiDeleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();
                }
                catch (Exception) { }
            }

            return new BaseResponse { };
        }
    }
}