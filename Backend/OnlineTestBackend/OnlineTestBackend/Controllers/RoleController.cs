﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : BaseController
    {
        public RoleController(OnlineTestBackendDBContext db) : base(db) { }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Roles.Where(x => x.ROL_ISPARENT == true).
                Include(x => x.Actionns).Include(x => x.Tables).
                Include(x => x.Roles).ThenInclude(x => x.Actionns).
                Include(x => x.Roles).ThenInclude(x => x.Tables).
                AsNoTracking().
                ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var role = await _context.Roles.
                Include(x => x.Roles).ThenInclude(x => x.Actionns).
                Include(x => x.Roles).ThenInclude(x => x.Tables).
                AsNoTracking().
                FirstOrDefaultAsync(x => x.ROL_ISPARENT == true && x.Id == id);
            if (role == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = role
            };
        }
    }
}