﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Backend.Models.Response;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Backend.Models;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using System.Net.Http;
using System.Net;
using System.Web;
using System.Web.Http;
using Microsoft.AspNetCore.Authorization;

namespace OnlineTestBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private IHostingEnvironment _hostingEnv;
        public FileController(IHostingEnvironment env)
        {
            _hostingEnv = env;
        }
        [HttpPost("{directory}"), RequestSizeLimit(500_000_000)]
        public async Task<ActionResult<string>> Post(string directory, [FromForm]IFormFile file)
        {
            //string newFileName = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + file.FileName + Path.GetExtension(file.FileName);
            string newFileName = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + file.FileName;
            string pathPhy = Path.Combine(_hostingEnv.ContentRootPath, "Data\\" + directory, newFileName);

            if (!Directory.Exists(Path.Combine(_hostingEnv.ContentRootPath, "Data\\" + directory, "")))
                Directory.CreateDirectory(Path.Combine(_hostingEnv.ContentRootPath, "Data\\" + directory, ""));

            using (var stream = new FileStream(pathPhy, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            return Ok(new BaseResponse
            {
                //data = (Utils.Helper.GetBaseUrl(Request) + "/Data/" + path + "/" + file.FileName)
                data = "/Data/" + directory + "/" + newFileName
            });
        }

        [HttpPut("{oldfile}&{directory}"), RequestSizeLimit(500_000_000)]
        public async Task<ActionResult<string>> Put(string oldfile, string directory, [FromForm]IFormFile file)
        {
            string newFileName = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + file.FileName;
            string pathPhy = Path.Combine(_hostingEnv.ContentRootPath, "Data\\" + directory, newFileName);

            if (!Directory.Exists(Path.Combine(_hostingEnv.ContentRootPath, "Data\\" + directory, "")))
                Directory.CreateDirectory(Path.Combine(_hostingEnv.ContentRootPath, "Data\\" + directory, ""));

            using (var stream = new FileStream(pathPhy, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            string pathDelete = Path.Combine(_hostingEnv.ContentRootPath, "Data\\" + directory, oldfile);
            if (System.IO.File.Exists(pathDelete))
            {
                try
                {
                    System.IO.File.Delete(pathDelete);
                }
                catch { }
            }

            return Ok(new BaseResponse
            {
                data = "/Data/" + directory + "/" + newFileName
            });
        }

        [HttpDelete("{directory}&{filename}")]
        public ActionResult<string> Delete(string directory, string filename)
        {
            string pathDelete = Path.Combine(_hostingEnv.ContentRootPath, "Data\\" + directory, filename);
            if (System.IO.File.Exists(pathDelete))
            {
                try
                {
                    System.IO.File.Delete(pathDelete);

                }
                catch { }
            }
            return Ok(new BaseResponse
            {
                data = "/Data/" + directory + "/" + filename
            });
        }

        [HttpGet("{filename}")]
        public async Task<IActionResult> DownloadFile(string filename)
        {
            try
            {
                string file = Path.Combine(_hostingEnv.ContentRootPath, "Data\\" + "File", filename);

                var memory = new MemoryStream();
                using (var stream = new FileStream(file, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }

                memory.Position = 0;

                return File(memory, GetMimeType(file), filename);
                //return Ok(new BaseResponse
                //{
                //    data = memory
                //});
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        private static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        private string GetMimeType(string file)
        {
            string extension = Path.GetExtension(file).ToLowerInvariant();
            switch (extension)
            {
                case ".txt": return "text/plain";
                case ".pdf": return "application/pdf";
                case ".doc": return "application/vnd.ms-word";
                case ".docx": return "application/vnd.ms-word";
                case ".xls": return "application/vnd.ms-excel";
                case ".png": return "image/png";
                case ".jpg": return "image/jpeg";
                case ".jpeg": return "image/jpeg";
                case ".gif": return "image/gif";
                case ".csv": return "text/csv";
                default: return "";
            }
        }
    }
}