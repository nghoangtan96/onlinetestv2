﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectBranchController : BaseController
    {
        public SubjectBranchController(OnlineTestBackendDBContext db) : base(db) { }
        [HttpGet("getListByBranchId/{id}")]
        public async Task<ActionResult<BaseResponse>> GetListByBranchId(int id)
        {
            List<Subject> subject = new List<Subject>();
            subject = await _context.Subjects.Include(x => x.Faculties).Where(x => x.SUB_ISALL == true && x.SUB_ISDELETED == false).ToListAsync();
            var subjectOfBranch = await _context.Subject_Branches.Include(x => x.Subjects).ThenInclude(x => x.Faculties).
                Where(x => x.SUBR_BRAID == id && x.Subjects.SUB_ISDELETED == false).AsNoTracking().ToListAsync();
            if (subjectOfBranch.Count != 0)
            {
                for (int i = 0; i < subjectOfBranch.Count; i++)
                {
                    subject.Add(subjectOfBranch.ElementAt(i).Subjects);
                }
            }
            if (subject.Count == 0)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            return new BaseResponse
            {
                data = subject
            };
        }
        [HttpGet("getListBySubjectId/{id}")]
        public async Task<ActionResult<BaseResponse>> GetListBySubjectId(long id)
        {
            var subjectBranch = await _context.Subject_Branches.
                Where(x => x.SUBR_SUBID == id).AsNoTracking().Select(x => x.SUBR_BRAID).ToListAsync();
            if (subjectBranch == null)
                return new BaseResponse
                {
                    errorCode = 1,
                    message = "Không tìm thấy dữ liệu!"
                };

            return new BaseResponse
            {
                data = subjectBranch
            };
        }
    }
}