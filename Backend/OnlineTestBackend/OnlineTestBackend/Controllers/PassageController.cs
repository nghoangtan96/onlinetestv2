﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PassageController : BaseController
    {
        private IHostingEnvironment _hostingEnv;
        private readonly IHubContext<SignalHub> _hubContext;
        public PassageController(OnlineTestBackendDBContext db, IHostingEnvironment env, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hostingEnv = env;
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Passages.Where(x => x.PAS_ISDELETED == false).
                Include(x => x.Parts).ThenInclude(x => x.Subjects).
                Include(x => x.AnswerTypes).AsNoTracking().
                ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var passage = await _context.Passages.
                Include(x => x.Parts).ThenInclude(x => x.Subjects).
                Include(x => x.Questions).ThenInclude(x => x.Options).
                Include(x => x.AnswerTypes).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.UserModified).ThenInclude(x => x.Employees).
                Where(x => x.PAS_ISDELETED == false && x.Id == id).
                AsNoTracking().
                FirstOrDefaultAsync();
            if (passage == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (passage.UserCreated != null)
            {
                if (passage.UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    passage.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    passage.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + passage.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
            }
            if (passage.UserModified != null)
            {
                if (passage.UserModified.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    passage.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    passage.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + passage.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
            }
            if (passage.PAS_MEDIA != null)
            {
                passage.FileUrl = Utils.Helper.GetBaseUrl(Request) + passage.PAS_MEDIA;
            }

            passage.countTest = _context.Passages.Include(x => x.Test_Questions).FirstOrDefault(x => x.Id == id).Test_Questions.Count;

            for (int i = 0; i < passage.Questions.Count; i++)
            {
                if (passage.Questions.ElementAt(i).QUE_MEDIA != null)
                {
                    passage.Questions.ElementAt(i).FileUrl = Utils.Helper.GetBaseUrl(Request) + passage.Questions.ElementAt(i).QUE_MEDIA;
                    passage.Questions.ElementAt(i).fileType = passage.Questions.ElementAt(i).QUE_MEDIA.Substring(passage.Questions.ElementAt(i).QUE_MEDIA.Length - 3, 3);
                }
            }

            return new BaseResponse
            {
                data = passage
            };
        }
        [HttpGet("getListByPartId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListByPartId(long id)
        {
            var passageList = await _context.Passages.
                Include(x => x.Questions).
                Include(x => x.Parts).ThenInclude(x => x.Subjects).
                Include(x => x.AnswerTypes).
                Where(x => x.PAS_ISDELETED == false && x.PAS_PARID == id).AsNoTracking().ToListAsync();
            if (passageList == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = passageList
            };
        }
        [HttpGet("getListBySubjectId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListBySubjectId(long id)
        {
            var passageList = await _context.Passages.
                Include(x => x.Questions).ThenInclude(x => x.Options).
                Include(x => x.Parts).ThenInclude(x => x.Subjects).
                Include(x => x.AnswerTypes).
                Where(x => x.PAS_ISDELETED == false && x.Parts.PAR_SUBID == id).AsNoTracking().ToListAsync();
            if (passageList == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            for (int i = 0; i < passageList.Count; i++)
            {
                if (passageList.ElementAt(i).PAS_MEDIA != null)
                {
                    passageList.ElementAt(i).FileUrl = Utils.Helper.GetBaseUrl(Request) + passageList.ElementAt(i).PAS_MEDIA;
                }
            }

            for (int i = 0; i < passageList.Count; i++)
            {
                for (int j = 0; j < passageList.ElementAt(i).Questions.Count; j++)
                {
                    for (int k = 0; k < passageList.ElementAt(i).Questions.ElementAt(j).Options.Count; k++)
                    {
                        passageList.ElementAt(i).Questions.ElementAt(j).Options.ElementAt(k).OPT_ISCORRECT = false;
                    }

                    if (passageList.ElementAt(i).Questions.ElementAt(j).QUE_MEDIA != null)
                    {
                        passageList.ElementAt(i).Questions.ElementAt(j).FileUrl = Utils.Helper.GetBaseUrl(Request) + passageList.ElementAt(i).Questions.ElementAt(j).QUE_MEDIA;
                    }
                }
            }

            return new BaseResponse
            {
                data = passageList
            };
        }
        [HttpGet("getListByPartIdAndAnswerTypeId/{partid}&{answertypeid}")]
        public async Task<ActionResult<BaseResponse>> getListByPartIdAndAnswerTypeId(long partid, int answertypeid)
        {
            var passageList = await _context.Passages.
                Include(x => x.Questions).
                Include(x => x.Parts).ThenInclude(x => x.Subjects).
                Include(x => x.AnswerTypes).
                Where(x => x.PAS_ISDELETED == false && x.PAS_PARID == partid && x.PAS_ANTYID == answertypeid).AsNoTracking().ToListAsync();
            if (passageList == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = passageList
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(Passage passage)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = passage.PAS_CREATEDBY.GetValueOrDefault();
            log.LOG_ACTID = 1;
            log.LOG_TABID = 2;
            log.LOG_MODULE = "Thêm đoạn văn";
            string title = "<b>Nội dung đoạn văn:</b><title>" +
                "<b>Tệp âm thanh, video:</b><title>" +
                "<b>Trộn câu hỏi:</b><title>" +
                "<b>Trạng thái:</b><title>" +
                "<b>Loại câu hỏi:</b><title>" +
                "<b>Thuộc học phần:</b>";
            string insertLog = (passage.PAS_CONTENT != null ? passage.PAS_CONTENT : "") + "<item>"
                + (passage.PAS_MEDIA != null ? passage.PAS_MEDIA : "") + "<item>"
                + (passage.PAS_ISSHUFFLE == true ? "Có" : "Không") + "<item>"
                + (passage.PAS_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>"
                + _context.AnswerTypes.FirstOrDefault(x => x.Id == passage.PAS_ANTYID).ANTY_NAME + "<item>"
                + _context.Parts.FirstOrDefault(x => x.Id == passage.PAS_PARID).PAR_NAME;

            passage.PAS_CREATEDDATE = DateTime.Now;
            passage.PAS_ISDELETED = false;
            passage.PAS_ORDER = _context.Passages.Where(x => x.PAS_PARID == passage.PAS_PARID && x.PAS_ISDELETED == false).ToList().Count + 1;

            try
            {
                _context.Passages.Add(passage);
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + insertLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetPassage", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = passage
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(long id, Passage passage)
        {
            Passage passageDB = await _context.Passages.Include(x => x.Test_Questions).FirstOrDefaultAsync(x => x.Id == id);

            if (passageDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };

            if (passageDB.Test_Questions.Count != 0)
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Không thể chỉnh sửa, đoạn văn đã được sử dụng!"
                };

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = passage.PAS_MODIFIEDBY.GetValueOrDefault();
            log.LOG_ACTID = 2;
            log.LOG_TABID = 2;
            log.LOG_MODULE = "Sửa đoạn văn";
            string title = "<b>Nội dung đoạn văn:</b><title>" +
                "<b>Tệp âm thanh, video:</b><title>" +
                "<b>Trộn câu hỏi:</b><title>" +
                "<b>Trạng thái:</b><title>" +
                "<b>Loại câu hỏi:</b><title>" +
                "<b>Thuộc học phần:</b>";
            string updateLog = (passageDB.PAS_CONTENT != null ? passageDB.PAS_CONTENT : "") + "<to>" + (passage.PAS_CONTENT != null ? passage.PAS_CONTENT : "") + "<item>"
                + (passageDB.PAS_MEDIA != null ? passageDB.PAS_MEDIA : "") + "<to>" + (passage.PAS_MEDIA != null ? passage.PAS_MEDIA : "") + "<item>"
                + (passageDB.PAS_ISSHUFFLE == true ? "Có" : "Không") + "<to>" + (passage.PAS_ISSHUFFLE == true ? "Có" : "Không") + "<item>"
                + (passageDB.PAS_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<to>" + (passage.PAS_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>"
                + _context.AnswerTypes.FirstOrDefault(x => x.Id == passageDB.PAS_ANTYID).ANTY_NAME + "<to>" + _context.AnswerTypes.FirstOrDefault(x => x.Id == passage.PAS_ANTYID).ANTY_NAME + "<item>"
                + _context.Parts.FirstOrDefault(x => x.Id == passageDB.PAS_PARID).PAR_NAME + "<to>" + _context.Parts.FirstOrDefault(x => x.Id == passage.PAS_PARID).PAR_NAME;

            try
            {
                passageDB.PAS_CONTENT = passage.PAS_CONTENT;
                passageDB.PAS_MEDIA = passage.PAS_MEDIA;
                passageDB.PAS_ISSHUFFLE = passage.PAS_ISSHUFFLE;
                passageDB.PAS_STATUS = passage.PAS_STATUS;
                passageDB.PAS_ANTYID = passage.PAS_ANTYID;
                passageDB.PAS_PARID = passage.PAS_PARID;
                passageDB.PAS_MODIFIEDBY = passage.PAS_MODIFIEDBY;
                passageDB.PAS_MODIFIEDDATE = DateTime.Now;
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + updateLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetPassage", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = passageDB
            };
        }
        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long id, long user)
        {
            Passage passageDB = await _context.Passages.Include(x => x.Test_Questions).ThenInclude(x => x.Tests).
                FirstOrDefaultAsync(x => x.Id == id);

            if (passageDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };

            if (passageDB.Test_Questions.FirstOrDefault(x => x.Tests.TES_STATUS != 4) != null)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Không thể xóa, đoạn văn đag được sử dụng!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 2;
            log.LOG_MODULE = "Xóa đoạn văn";
            string title = "<b>Nội dung đoạn văn:</b><title>" +
                "<b>Tệp âm thanh, video:</b><title>" +
                "<b>Trộn câu hỏi:</b><title>" +
                "<b>Trạng thái:</b><title>" +
                "<b>Loại câu hỏi:</b><title>" +
                "<b>Thuộc học phần:</b>";
            string deleteLog = (passageDB.PAS_CONTENT != null ? passageDB.PAS_CONTENT : "") + "<item>"
                + (passageDB.PAS_MEDIA != null ? passageDB.PAS_MEDIA : "") + "<item>"
                + (passageDB.PAS_ISSHUFFLE == true ? "Có" : "Không") + "<item>"
                + (passageDB.PAS_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>"
                + _context.AnswerTypes.FirstOrDefault(x => x.Id == passageDB.PAS_ANTYID).ANTY_NAME + "<item>"
                + _context.Parts.FirstOrDefault(x => x.Id == passageDB.PAS_PARID).PAR_NAME;

            if (passageDB.PAS_MEDIA != null && passageDB.Test_Questions.Count == 0)
            {
                new FileController(_hostingEnv).Delete("Passage", passageDB.PAS_MEDIA.Split("/")[3]);
            }

            try
            {
                passageDB.PAS_MODIFIEDBY = user;
                passageDB.PAS_MODIFIEDDATE = DateTime.Now;
                passageDB.PAS_ISDELETED = true;
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + deleteLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetPassage", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = passageDB
            };
        }
        [HttpPost("getListPassageMultiDel")]
        public async Task<ActionResult<BaseResponse>> getListPassageMultiDel(long[] arrDel)
        {
            List<Passage> listPas = new List<Passage>();
            for (int i = 0; i < arrDel.Length; i++)
            {
                Passage pas = await _context.Passages.
                    Include(x => x.AnswerTypes).
                    Include(x => x.Parts).ThenInclude(x => x.Subjects).
                    Where(x => x.Id == arrDel[i]).AsNoTracking().FirstOrDefaultAsync();
                if (pas != null)
                {
                    listPas.Add(pas);
                }
            }
            return new BaseResponse
            {
                data = listPas
            };
        }
        [HttpPost("multipleDelete/{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long user, Passage[] arrDel)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 2;
            log.LOG_MODULE = "Xóa danh sách đoạn văn";
            log.LOG_CONTENT = "";

            string title = "<b>Nội dung đoạn văn:</b><title>" +
                "<b>Tệp âm thanh, video:</b><title>" +
                "<b>Trộn câu hỏi:</b><title>" +
                "<b>Trạng thái:</b><title>" +
                "<b>Loại câu hỏi:</b><title>" +
                "<b>Thuộc học phần:</b>";
            string multiDeleteLog = "";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i = 0; i < arrDel.Length; i++)
                    {
                        var passageDB = _context.Passages.
                            Include(x => x.Questions).
                            Include(x => x.Test_Questions).ThenInclude(x => x.Tests).
                            Where(x => x.Id == arrDel[i].Id).FirstOrDefault();

                        if (passageDB == null)
                            return new BaseResponse
                            {
                                errorCode = 404,
                                message = "Không tìm thấy dữ liệu!"
                            };
                        if (passageDB.Test_Questions.FirstOrDefault(x => x.Tests.TES_STATUS != 4) != null)
                        {
                            return new BaseResponse
                            {
                                errorCode = 405,
                                message = "Không thể xóa, có đoạn văn đang được sử dụng!"
                            };
                        }

                        if (passageDB.PAS_MEDIA != null && passageDB.Test_Questions.Count == 0)
                        {
                            new FileController(_hostingEnv).Delete("Passage", passageDB.PAS_MEDIA.Split("/")[3]);
                        }

                        multiDeleteLog += (passageDB.PAS_CONTENT != null ? passageDB.PAS_CONTENT : "") + "<item>"
                            + (passageDB.PAS_MEDIA != null ? passageDB.PAS_MEDIA : "") + "<item>"
                            + (passageDB.PAS_ISSHUFFLE == true ? "Có" : "Không") + "<item>"
                            + (passageDB.PAS_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>"
                            + _context.AnswerTypes.FirstOrDefault(x => x.Id == passageDB.PAS_ANTYID).ANTY_NAME + "<item>"
                            + _context.Parts.FirstOrDefault(x => x.Id == passageDB.PAS_PARID).PAR_NAME;
                        if (i != arrDel.Length - 1)
                            multiDeleteLog += "<delete>";

                        //question delete
                        if (passageDB.Questions.Count != 0)
                        {
                            for (int j = 0; j < passageDB.Questions.Count; j++)
                            {
                                passageDB.Questions.ElementAt(i).QUE_ISDELETED = true;
                                await _context.SaveChangesAsync();
                            }
                        }

                        //passage delete
                        passageDB.PAS_MODIFIEDBY = user;
                        passageDB.PAS_MODIFIEDDATE = DateTime.Now;
                        passageDB.PAS_ISDELETED = true;
                        await _context.SaveChangesAsync();
                    }

                    log.LOG_CONTENT += title + "divider" + multiDeleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetPassage", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse { };
        }
    }
}