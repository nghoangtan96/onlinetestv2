﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LabController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public LabController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Labs.Where(x => x.LAB_ISDELETED == false).AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(int id)
        {
            var lab = await _context.Labs.
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.UserModified).ThenInclude(x => x.Employees).
                Where(x => x.LAB_ISDELETED == false && x.Id == id).AsNoTracking().FirstOrDefaultAsync();
            if (lab == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (lab.UserCreated != null)
            {
                if (lab.UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    lab.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    lab.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + lab.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
            }
            if (lab.UserModified != null)
            {
                if (lab.UserModified.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    lab.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    lab.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + lab.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
            }

            return new BaseResponse
            {
                data = lab
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(Lab lab)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = lab.LAB_CREATEDBY.GetValueOrDefault();
            log.LOG_MODULE = "Thêm phòng thi";
            log.LOG_ACTID = 1;
            log.LOG_TABID = 1015;
            string title = "<b>Tên phòng thi:</b><title>" +
                "<b>Trạng thái:</b>";

            string insertLog = lab.LAB_NAME + "<item>"
                + (lab.LAB_STATUS == 1 ? "Sử dụng" : "Không sử dụng");

            lab.LAB_CREATEDDATE = DateTime.Now;
            lab.LAB_ISDELETED = false;

            try
            {
                _context.Labs.Add(lab);
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + insertLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetLab", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = lab
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(int id, Lab lab)
        {
            Lab labDB = await _context.Labs.Include(x => x.Schedule_Tests).
                FirstOrDefaultAsync(x => x.LAB_ISDELETED == false && x.Id == id);

            if (labDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }
            if (labDB.Schedule_Tests.FirstOrDefault(x => x.SCTE_STATUS != 4) != null)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Không thể chỉnh sửa, phòng thi đang được sử dụng!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = lab.LAB_MODIFIEDBY.GetValueOrDefault();
            log.LOG_ACTID = 2;
            log.LOG_TABID = 1015;
            log.LOG_MODULE = "Sửa phòng thi";

            string title = "<b>Tên phòng thi:</b><title>" +
                "<b>Trạng thái:</b>";

            string updateLog = labDB.LAB_NAME + "<to>" + lab.LAB_NAME + "<item>"
                + (labDB.LAB_STATUS == 1 ? "Sử dụng" : "Không sử dụng") + "<to>" + (lab.LAB_STATUS == 1 ? "Sử dụng" : "Không sử dụng");

            try
            {
                labDB.LAB_NAME = lab.LAB_NAME;
                labDB.LAB_STATUS = lab.LAB_STATUS;
                labDB.LAB_MODIFIEDBY = lab.LAB_MODIFIEDBY;
                labDB.LAB_MODIFIEDDATE = DateTime.Now;
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + updateLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetLab", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = labDB
            };
        }
        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(int id, long user)
        {
            Lab labDB = await _context.Labs.Include(x => x.Schedule_Tests).
                FirstOrDefaultAsync(x => x.LAB_ISDELETED == false && x.Id == id);

            if (labDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (labDB.Schedule_Tests.FirstOrDefault(x => x.SCTE_STATUS != 4) != null)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Không thể xóa, phòng thi đang được sử dụng!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_MODULE = "Xóa phòng thi";
            log.LOG_ACTID = 3;
            log.LOG_TABID = 1015;

            string title = "<b>Tên phòng thi:</b><title>" +
                "<b>Trạng thái:</b>";

            string deleteLog = labDB.LAB_NAME + "<item>"
                + (labDB.LAB_STATUS == 1 ? "Sử dụng" : "Không sử dụng");

            try
            {
                labDB.LAB_ISDELETED = true;
                labDB.LAB_MODIFIEDBY = user;
                labDB.LAB_MODIFIEDDATE = DateTime.Now;

                log.LOG_CONTENT = title + "<divider>" + deleteLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetLab", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = labDB
            };
        }
        [HttpPost("getListLabMultiDel")]
        public async Task<ActionResult<BaseResponse>> getListLabMultiDel(int[] arrDel)
        {
            List<Lab> listLab = new List<Lab>();
            for (int i = 0; i < arrDel.Length; i++)
            {
                Lab lab = await _context.Labs.Where(x => x.Id == arrDel[i]).AsNoTracking().FirstOrDefaultAsync();
                if (lab != null)
                {
                    listLab.Add(lab);
                }
            }
            return new BaseResponse
            {
                data = listLab
            };
        }
        [HttpPost("multipleDelete/{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long user, Lab[] arrDel)
        {

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 1015;
            log.LOG_MODULE = "Xóa danh sách phòng thi";

            string title = "<b>Tên phòng thi:</b><title>" +
                "<b>Trạng thái:</b>";
            string multiDeleteLog = "";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i = 0; i < arrDel.Length; i++)
                    {
                        var labDB = _context.Labs.Include(x => x.Schedule_Tests).
                            Where(x => x.Id == arrDel[i].Id).FirstOrDefault();

                        if (labDB == null)
                        {
                            return new BaseResponse
                            {
                                errorCode = 404,
                                message = "Không tìm thấy dữ liệu!"
                            };
                        }
                        if (labDB.Schedule_Tests.FirstOrDefault(x=>x.SCTE_STATUS != 4) != null)
                        {
                            return new BaseResponse
                            {
                                errorCode = 405,
                                message = "Không thể xóa, phòng thi " + labDB.LAB_NAME + " đang được sử dụng!"
                            };
                        }

                        multiDeleteLog += labDB.LAB_NAME + "<item>"
                                + (labDB.LAB_STATUS == 1 ? "Sử dụng" : "Không sử dụng");
                        if (i != arrDel.Length - 1)
                            multiDeleteLog += "<delete>";

                        labDB.LAB_MODIFIEDBY = user;
                        labDB.LAB_MODIFIEDDATE = DateTime.Now;
                        labDB.LAB_ISDELETED = true;
                        await _context.SaveChangesAsync();
                    }

                    log.LOG_CONTENT = title + "<divider>" + multiDeleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetLab", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse { };
        }
    }
}