﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TestShiftController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public TestShiftController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.TestShifts.Where(x => x.SHI_ISDELETED == false).AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(int id)
        {
            var testshift = await _context.TestShifts.
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.UserModified).ThenInclude(x => x.Employees).
                Where(x => x.SHI_ISDELETED == false && x.Id == id).AsNoTracking().FirstOrDefaultAsync();
            if (testshift == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (testshift.UserCreated != null)
            {
                if (testshift.UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    testshift.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    testshift.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + testshift.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
            }
            if (testshift.UserModified != null)
            {
                if (testshift.UserModified.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    testshift.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    testshift.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + testshift.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
            }

            return new BaseResponse
            {
                data = testshift
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(TestShift testshift)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = testshift.SHI_CREATEDBY.GetValueOrDefault();
            log.LOG_MODULE = "Thêm ca thi";
            log.LOG_ACTID = 1;
            log.LOG_TABID = 1015;
            string title = "<b>Tên ca thi:</b><title>" +
                "<b>Thời gian bắt đầu:</b><title>" +
                "<b>Trạng thái:</b>";

            string insertLog = testshift.SHI_NAME + "<item>"
                + testshift.SHI_TIMESTART + "<item>"
                + (testshift.SHI_STATUS == 1 ? "Sử dụng" : "Không sử dụng");

            testshift.SHI_CREATEDDATE = DateTime.Now;
            testshift.SHI_ISDELETED = false;

            try
            {
                _context.TestShifts.Add(testshift);
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + insertLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetTestShift", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = testshift
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(int id, TestShift testshift)
        {
            TestShift testshiftDB = await _context.TestShifts.Include(x => x.Schedule_Tests).
                FirstOrDefaultAsync(x => x.SHI_ISDELETED == false && x.Id == id);

            if (testshiftDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (testshiftDB.Schedule_Tests.FirstOrDefault(x => x.SCTE_STATUS != 4) != null)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Không thể chỉnh sửa, ca thi đang được sử dụng!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = testshift.SHI_MODIFIEDBY.GetValueOrDefault();
            log.LOG_ACTID = 2;
            log.LOG_TABID = 1015;
            log.LOG_MODULE = "Sửa ca thi";

            string title = "<b>Tên ca thi:</b><title>" +
                "<b>Thời gian bắt đầu:</b><title>" +
                "<b>Trạng thái:</b>";

            string updateLog = testshiftDB.SHI_NAME + "<to>" + testshift.SHI_NAME + "<item>"
                + testshiftDB.SHI_TIMESTART + "<to>" + testshift.SHI_TIMESTART + "<item>"
                + (testshiftDB.SHI_STATUS == 1 ? "Sử dụng" : "Không sử dụng") + "<to>" + (testshift.SHI_STATUS == 1 ? "Sử dụng" : "Không sử dụng");

            try
            {
                testshiftDB.SHI_NAME = testshift.SHI_NAME;
                testshiftDB.SHI_TIMESTART = testshift.SHI_TIMESTART;
                testshiftDB.SHI_STATUS = testshift.SHI_STATUS;
                testshiftDB.SHI_MODIFIEDBY = testshift.SHI_MODIFIEDBY;
                testshiftDB.SHI_MODIFIEDDATE = DateTime.Now;
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + updateLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetTestShift", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = testshiftDB
            };
        }
        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(int id, long user)
        {
            TestShift testshiftDB = await _context.TestShifts.Include(x => x.Schedule_Tests).
                FirstOrDefaultAsync(x => x.SHI_ISDELETED == false && x.Id == id);

            if (testshiftDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }
            if (testshiftDB.Schedule_Tests.FirstOrDefault(x => x.SCTE_STATUS != 4) != null)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Không thể xóa, ca thi đang được sử dụng!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_MODULE = "Xóa ca thi";
            log.LOG_ACTID = 3;
            log.LOG_TABID = 1015;

            string title = "<b>Tên ca thi:</b><title>" +
                "<b>Thời gian bắt đầu:</b><title>" +
                "<b>Trạng thái:</b>";

            string deleteLog = testshiftDB.SHI_NAME + "<item>"
                + testshiftDB.SHI_TIMESTART + "<item>"
                + (testshiftDB.SHI_STATUS == 1 ? "Sử dụng" : "Không sử dụng");

            try
            {
                testshiftDB.SHI_ISDELETED = true;
                testshiftDB.SHI_MODIFIEDBY = user;
                testshiftDB.SHI_MODIFIEDDATE = DateTime.Now;

                log.LOG_CONTENT = title + "<divider>" + deleteLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetTestShift", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = testshiftDB
            };
        }
        [HttpPost("getListTestShiftMultiDel")]
        public async Task<ActionResult<BaseResponse>> getListTestShiftMultiDel(int[] arrDel)
        {
            List<TestShift> listTestshift = new List<TestShift>();
            for (int i = 0; i < arrDel.Length; i++)
            {
                TestShift testshift = await _context.TestShifts.Where(x => x.Id == arrDel[i]).AsNoTracking().FirstOrDefaultAsync();
                if (testshift != null)
                {
                    listTestshift.Add(testshift);
                }
            }
            return new BaseResponse
            {
                data = listTestshift
            };
        }
        [HttpPost("multipleDelete/{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long user, TestShift[] arrDel)
        {

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 1015;
            log.LOG_MODULE = "Xóa danh sách ca thi";

            string title = "<b>Tên ca thi:</b><title>" +
                "<b>Thời gian bắt đầu:</b><title>" +
                "<b>Trạng thái:</b>";
            string multiDeleteLog = "";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i = 0; i < arrDel.Length; i++)
                    {
                        var testshiftDB = _context.TestShifts.Include(x => x.Schedule_Tests).
                            Where(x => x.Id == arrDel[i].Id).FirstOrDefault();

                        if (testshiftDB == null)
                        {
                            return new BaseResponse
                            {
                                errorCode = 404,
                                message = "Không tìm thấy dữ liệu!"
                            };
                        }
                        if (testshiftDB.Schedule_Tests.FirstOrDefault(x=>x.SCTE_STATUS != 4) != null)
                        {
                            return new BaseResponse
                            {
                                errorCode = 405,
                                message = "Không thể xóa, ca thi '" + testshiftDB.SHI_NAME + "' đang được sử dụng!"
                            };
                        }

                        multiDeleteLog += testshiftDB.SHI_NAME + "<item>"
                                + testshiftDB.SHI_TIMESTART + "<item>"
                                + (testshiftDB.SHI_STATUS == 1 ? "Sử dụng" : "Không sử dụng");
                        if (i != arrDel.Length - 1)
                            multiDeleteLog += "<delete>";

                        testshiftDB.SHI_MODIFIEDBY = user;
                        testshiftDB.SHI_MODIFIEDDATE = DateTime.Now;
                        testshiftDB.SHI_ISDELETED = true;
                        await _context.SaveChangesAsync();
                    }

                    log.LOG_CONTENT = title + "<divider>" + multiDeleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetTestShift", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse { };
        }
    }
}