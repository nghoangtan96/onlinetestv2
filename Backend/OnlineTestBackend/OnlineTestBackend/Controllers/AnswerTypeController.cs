﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AnswerTypeController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public AnswerTypeController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            Setting setting = await _context.Settings.Where(x => x.Id == 10).AsNoTracking().FirstOrDefaultAsync();
            List<AnswerType> listAnswerTypeResult = new List<AnswerType>();
            for (int i = 0; i < setting.SET_MULTICHOICE.Split('/').Length; i++)
            {
                string name = setting.SET_MULTICHOICE.Split('/')[i];
                if (setting.SET_MULTICHOICEVALUE.Split('/')[i] == "1")
                {
                    listAnswerTypeResult.Add(await _context.AnswerTypes.Where(x => x.ANTY_NAME == name).AsNoTracking().FirstOrDefaultAsync());
                }
            }
            return new BaseResponse
            {
                data = listAnswerTypeResult
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(int id)
        {
            var ansDB = await _context.AnswerTypes.
                Include(x => x.UserModified).ThenInclude(x => x.Employees).
                AsNoTracking().
                FirstOrDefaultAsync(x => x.Id == id);
            if (ansDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };

            if (ansDB.UserModified != null)
            {
                if (ansDB.UserModified.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    ansDB.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    ansDB.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + ansDB.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
            }

            return new BaseResponse
            {
                data = ansDB
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(int id, AnswerType answertype)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = answertype.ANTY_MODIFIEDBY.GetValueOrDefault();
            log.LOG_MODULE = "Sửa loại câu hỏi";
            log.LOG_ACTID = 2;
            log.LOG_TABID = 4;
            var ansDB = await _context.AnswerTypes.FindAsync(id);
            if (ansDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };

            string title = "<b>Tên loại câu hỏi:</b><title>" +
                "<b>Mẫu câu hỏi:</b>";
            string updateLog = ansDB.ANTY_NAME + "<to>" + answertype.ANTY_NAME + "<item>" +
                ansDB.ANTY_SAMPLE + "<to>" + answertype.ANTY_SAMPLE;

            try
            {
                ansDB.ANTY_SAMPLE = answertype.ANTY_SAMPLE;
                ansDB.ANTY_MODIFIEDBY = answertype.ANTY_MODIFIEDBY;
                ansDB.ANTY_MODIFIEDDATE = DateTime.Now;
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + updateLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetAnswerType", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = ansDB
            };
        }
    }
}