﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BranchController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public BranchController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }

        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Branches.Include(x => x.Branches).Where(x => x.BRA_ISDELETED == false).AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("getAllParentBranch")]
        public async Task<ActionResult<BaseResponse>> GetAllParentBranch()
        {
            List<Branch> listBranch = await _context.Branches.
                Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == null).AsNoTracking().ToListAsync();
            for (int i = 0; i < listBranch.Count; i++)
            {
                List<Branch> listBranchChild1 = await _context.Branches.
                    Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == listBranch.ElementAt(i).Id).AsNoTracking().ToListAsync();
                if (listBranchChild1.Count != 0)
                {
                    listBranch.ElementAt(i).Branches = listBranchChild1;
                    for (int j = 0; j < listBranchChild1.Count; j++)
                    {
                        List<Branch> listBranchChild2 = await _context.Branches.
                            Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == listBranchChild1.ElementAt(j).Id).AsNoTracking().ToListAsync();
                        if (listBranchChild2.Count != 0)
                        {
                            listBranchChild1.ElementAt(j).Branches = listBranchChild2;
                            for (int k = 0; k < listBranchChild2.Count; k++)
                            {
                                List<Branch> listBranchChild3 = await _context.Branches.
                                    Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == listBranchChild2.ElementAt(k).Id).AsNoTracking().ToListAsync();
                                if (listBranchChild3.Count != 0)
                                {
                                    listBranchChild2.ElementAt(k).Branches = listBranchChild3;
                                }
                            }
                        }
                    }
                }
            }
            return new BaseResponse
            {
                data = listBranch
            };
        }
        [HttpGet("getAllAllowChildBranch")]
        public async Task<ActionResult<BaseResponse>> GetAllAllowChildBranch()
        {
            List<Branch> listBranch = await _context.Branches.
                Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == null && x.BRA_ISPARENT == true).AsNoTracking().ToListAsync();
            for (int i = 0; i < listBranch.Count; i++)
            {
                List<Branch> listBranchChild1 = await _context.Branches.
                    Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == listBranch.ElementAt(i).Id && x.BRA_ISPARENT == true).AsNoTracking().ToListAsync();
                if (listBranchChild1.Count != 0)
                {
                    listBranch.ElementAt(i).Branches = listBranchChild1;
                    for (int j = 0; j < listBranchChild1.Count; j++)
                    {
                        List<Branch> listBranchChild2 = await _context.Branches.
                            Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == listBranchChild1.ElementAt(j).Id && x.BRA_ISPARENT == true).AsNoTracking().ToListAsync();
                        if (listBranchChild2.Count != 0)
                        {
                            listBranchChild1.ElementAt(j).Branches = listBranchChild2;
                            for (int k = 0; k < listBranchChild2.Count; k++)
                            {
                                List<Branch> listBranchChild3 = await _context.Branches.
                                    Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == listBranchChild2.ElementAt(k).Id && x.BRA_ISPARENT == true).AsNoTracking().ToListAsync();
                                if (listBranchChild3.Count != 0)
                                {
                                    listBranchChild2.ElementAt(k).Branches = listBranchChild3;
                                }
                            }
                        }
                    }
                }
            }
            return new BaseResponse
            {
                data = listBranch
            };
        }
        [HttpGet("getListBranchByFacultyId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListBranchByFacultyId(int id)
        {
            return new BaseResponse
            {
                data = await _context.Branches.Include(x => x.Branches).Where(x => x.BRA_ISDELETED == false && x.BRA_FACID == id).AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("getListParentBranchByFacultyId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListParentBranchByFacultyId(int id)
        {
            List<Branch> listBranch = await _context.Branches.
                Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == null && x.BRA_FACID == id).AsNoTracking().ToListAsync();
            for (int i = 0; i < listBranch.Count; i++)
            {
                List<Branch> listBranchChild1 = await _context.Branches.
                    Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == listBranch.ElementAt(i).Id).AsNoTracking().ToListAsync();
                if (listBranchChild1.Count != 0)
                {
                    listBranch.ElementAt(i).Branches = listBranchChild1;
                    for (int j = 0; j < listBranchChild1.Count; j++)
                    {
                        List<Branch> listBranchChild2 = await _context.Branches.
                            Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == listBranchChild1.ElementAt(j).Id).AsNoTracking().ToListAsync();
                        if (listBranchChild2.Count != 0)
                        {
                            listBranchChild1.ElementAt(j).Branches = listBranchChild2;
                            for (int k = 0; k < listBranchChild2.Count; k++)
                            {
                                List<Branch> listBranchChild3 = await _context.Branches.
                                    Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == listBranchChild2.ElementAt(k).Id).AsNoTracking().ToListAsync();
                                if (listBranchChild3.Count != 0)
                                {
                                    listBranchChild2.ElementAt(k).Branches = listBranchChild3;
                                }
                            }
                        }
                    }
                }
            }
            return new BaseResponse
            {
                data = listBranch
            };
        }
        [HttpGet("getListAllowChildBranchByFacultyId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListAllowChildBranchByFacultyId(int id)
        {
            List<Branch> listBranch = await _context.Branches.
                Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == null && x.BRA_ISPARENT == true && x.BRA_FACID == id).AsNoTracking().ToListAsync();
            for (int i = 0; i < listBranch.Count; i++)
            {
                List<Branch> listBranchChild1 = await _context.Branches.
                    Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == listBranch.ElementAt(i).Id && x.BRA_ISPARENT == true).AsNoTracking().ToListAsync();
                if (listBranchChild1.Count != 0)
                {
                    listBranch.ElementAt(i).Branches = listBranchChild1;
                    for (int j = 0; j < listBranchChild1.Count; j++)
                    {
                        List<Branch> listBranchChild2 = await _context.Branches.
                            Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == listBranchChild1.ElementAt(j).Id && x.BRA_ISPARENT == true).AsNoTracking().ToListAsync();
                        if (listBranchChild2.Count != 0)
                        {
                            listBranchChild1.ElementAt(j).Branches = listBranchChild2;
                            for (int k = 0; k < listBranchChild2.Count; k++)
                            {
                                List<Branch> listBranchChild3 = await _context.Branches.
                                    Where(x => x.BRA_ISDELETED == false && x.BRA_PARENT == listBranchChild2.ElementAt(k).Id && x.BRA_ISPARENT == true).AsNoTracking().ToListAsync();
                                if (listBranchChild3.Count != 0)
                                {
                                    listBranchChild2.ElementAt(k).Branches = listBranchChild3;
                                }
                            }
                        }
                    }
                }
            }
            return new BaseResponse
            {
                data = listBranch
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(int id)
        {
            var branch = await _context.Branches.
                Where(x => x.Id == id && x.BRA_ISDELETED == false).
                Include(x => x.Faculties).
                Include(x => x.Subject_Branches).ThenInclude(x => x.Subjects).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.UserModified).ThenInclude(x => x.Employees).
                Include(x => x.Branches).Include(x => x.BranchesParent).
                AsNoTracking().
                FirstOrDefaultAsync();
            if (branch == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (branch.UserCreated != null)
            {
                if (branch.UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    branch.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    branch.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + branch.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
            }
            if (branch.UserModified != null)
            {
                if (branch.UserModified.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    branch.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    branch.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + branch.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
            }

            return new BaseResponse
            {
                data = branch
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(Branch branch)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = branch.BRA_CREATEDBY.GetValueOrDefault();
            log.LOG_ACTID = 1;
            log.LOG_TABID = 6;
            log.LOG_MODULE = "Thêm ngành học";

            string title = "<b>Tên ngành:</b>" + "<title>" +
                "<b>Mã ngành:</b>" + "<title>" +
                "<b>Thời lượng:</b>" + "<title>" +
                "<b>Thuộc ngành học:</b>" + "<title>" +
                "<b>Cho phép tạo chuyên ngành:</b>";
            string insertLog = branch.BRA_NAME + "<item>"
                + branch.BRA_CODE + "<item>"
                + branch.BRA_DURATION + "<item>"
                + (branch.BRA_PARENT != null ? _context.Branches.FirstOrDefault(x => x.Id == branch.BRA_PARENT).BRA_NAME : "") + "<item>"
                + (branch.BRA_ISPARENT == true ? "Có" : "Không");

            branch.BRA_CREATEDDATE = DateTime.Now;
            branch.BRA_ISDELETED = false;
            branch.BRA_STATUS = 1;

            //Check branch level
            if (branch.BRA_PARENT != null)
            {
                branch.BRA_LEVEL = _context.Branches.Where(x => x.Id == branch.BRA_PARENT).FirstOrDefault().BRA_LEVEL + 1;
            }
            else
                branch.BRA_LEVEL = 1;

            try
            {
                _context.Branches.Add(branch);
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + insertLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetBranch", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = branch
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(int id, Branch branch)
        {
            Branch braDB = await _context.Branches.Include(x => x.Students).
                FirstOrDefaultAsync(x => x.BRA_ISDELETED == false && x.Id == id);
            if (braDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };

            if (braDB.Students.Count > 0)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Không thể chỉnh sửa, ngành học đã được sử dụng!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = branch.BRA_MODIFIEDBY.GetValueOrDefault();
            log.LOG_ACTID = 2;
            log.LOG_TABID = 6;
            log.LOG_MODULE = "Sửa ngành học";

            string title = "<b>Tên ngành:</b>" + "<title>" +
                "<b>Mã ngành:</b>" + "<title>" +
                "<b>Thời lượng:</b>" + "<title>" +
                "<b>Thuộc ngành học:</b>" + "<title>" +
                "<b>Cho phép tạo chuyên ngành:</b>";
            string updateLog = braDB.BRA_NAME + "<to>" + branch.BRA_NAME + "<item>"
                + braDB.BRA_CODE + "<to>" + branch.BRA_CODE + "<item>"
                + braDB.BRA_DURATION + "<to>" + branch.BRA_DURATION + "<item>"
                + (braDB.BRA_PARENT != null ? _context.Branches.FirstOrDefault(x => x.Id == braDB.BRA_PARENT).BRA_NAME : "") + "<to>" + (branch.BRA_PARENT != null ? _context.Branches.FirstOrDefault(x => x.Id == branch.BRA_PARENT).BRA_NAME : "") + "<item>"
                + (braDB.BRA_ISPARENT == true ? "Có" : "Không") + "<to>" + (branch.BRA_ISPARENT == true ? "Có" : "Không");

            braDB.BRA_NAME = branch.BRA_NAME;
            braDB.BRA_FACID = branch.BRA_FACID;
            braDB.BRA_CODE = branch.BRA_CODE;
            braDB.BRA_DURATION = branch.BRA_DURATION;
            braDB.BRA_ISPARENT = branch.BRA_ISPARENT;
            braDB.BRA_PARENT = branch.BRA_PARENT;
            braDB.BRA_MODIFIEDBY = branch.BRA_MODIFIEDBY;
            braDB.BRA_MODIFIEDDATE = DateTime.Now;
            if (branch.BRA_PARENT != null)
            {
                braDB.BRA_LEVEL = _context.Branches.Where(x => x.Id == branch.BRA_PARENT).FirstOrDefault().BRA_LEVEL + 1;
            }
            else
            {
                braDB.BRA_LEVEL = 1;
            }

            try
            {
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + updateLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetBranch", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = branch
            };
        }
        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(int id, long user)
        {
            var braDB = await _context.Branches.Include(x => x.Branches).FirstOrDefaultAsync(x => x.Id == id);

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 6;
            log.LOG_MODULE = "Xóa ngành học";

            string title = "<b>Tên ngành:</b>" + "<title>" +
                "<b>Mã ngành:</b>" + "<title>" +
                "<b>Thời lượng:</b>" + "<title>" +
                "<b>Thuộc ngành học:</b>" + "<title>" +
                "<b>Cho phép tạo chuyên ngành:</b>";
            string deleteLog = braDB.BRA_NAME + "<item>"
                + braDB.BRA_CODE + "<item>"
                + braDB.BRA_DURATION + "<item>"
                + (braDB.BRA_PARENT != null ? _context.Branches.FirstOrDefault(x => x.Id == braDB.BRA_PARENT).BRA_NAME : "") + "<item>"
                + (braDB.BRA_ISPARENT == true ? "Có" : "Không");

            if (braDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (braDB.Branches.Count != 0)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Không thể xóa, ngành học được sử dụng bởi các chuyên ngành con!"
                };
            }

            try
            {
                braDB.BRA_MODIFIEDBY = user;
                braDB.BRA_MODIFIEDDATE = DateTime.Now;
                braDB.BRA_ISDELETED = true;
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + deleteLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetBranch", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = braDB
            };
        }
    }
}