﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TestTypeController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public TestTypeController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Test_Types.Where(x => x.TETY_ISDELETED == false).AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(int id)
        {
            var testtype = await _context.Test_Types.
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.UserModified).ThenInclude(x => x.Employees).
                Where(x => x.TETY_ISDELETED == false && x.Id == id).AsNoTracking().FirstOrDefaultAsync();
            if (testtype == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (testtype.UserCreated != null)
            {
                if (testtype.UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    testtype.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    testtype.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + testtype.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
            }
            if (testtype.UserModified != null)
            {
                if (testtype.UserModified.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    testtype.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    testtype.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + testtype.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
            }

            return new BaseResponse
            {
                data = testtype
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(Test_Type testtype)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = testtype.TETY_CREATEDBY.GetValueOrDefault();
            log.LOG_MODULE = "Thêm loại bài thi";
            log.LOG_ACTID = 1;
            log.LOG_TABID = 1015;
            string title = "<b>Tên loại bài thi:</b><title>" +
                "<b>Trạng thái:</b>";

            string insertLog = testtype.TETY_NAME + "<item>"
                + (testtype.TETY_STATUS == 1 ? "Sử dụng" : "Không sử dụng");

            testtype.TETY_CREATEDDATE = DateTime.Now;
            testtype.TETY_ISDELETED = false;

            try
            {
                _context.Test_Types.Add(testtype);
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + insertLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetTestType", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = testtype
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(int id, Test_Type testtype)
        {
            Test_Type testtypeDB = await _context.Test_Types.Include(x => x.Tests).
                FirstOrDefaultAsync(x => x.TETY_ISDELETED == false && x.Id == id);

            if (testtypeDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }
            if (testtypeDB.Tests.Count != 0)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Không thể chỉnh sửa, loại bài thi đã được sử dụng!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = testtype.TETY_MODIFIEDBY.GetValueOrDefault();
            log.LOG_ACTID = 2;
            log.LOG_TABID = 1015;
            log.LOG_MODULE = "Sửa loại bài thi";

            string title = "<b>Tên loại bài thi:</b><title>" +
                "<b>Trạng thái:</b>";

            string updateLog = testtypeDB.TETY_NAME + "<to>" + testtype.TETY_NAME + "<item>"
                + (testtypeDB.TETY_STATUS == 1 ? "Sử dụng" : "Không sử dụng") + "<to>" + (testtype.TETY_STATUS == 1 ? "Sử dụng" : "Không sử dụng");

            try
            {
                testtypeDB.TETY_NAME = testtype.TETY_NAME;
                testtypeDB.TETY_STATUS = testtype.TETY_STATUS;
                testtypeDB.TETY_MODIFIEDBY = testtype.TETY_MODIFIEDBY;
                testtypeDB.TETY_MODIFIEDDATE = DateTime.Now;
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + updateLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetTestType", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = testtypeDB
            };
        }
        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(int id, long user)
        {
            Test_Type testtypeDB = await _context.Test_Types.Include(x => x.Tests).
                FirstOrDefaultAsync(x => x.Id == id);

            if (testtypeDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }
            if (testtypeDB.Tests.FirstOrDefault(x => x.TES_STATUS != 4) != null)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Không thể xóa, đề thi đang được sử dụng!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_MODULE = "Xóa loại bài thi";
            log.LOG_ACTID = 3;
            log.LOG_TABID = 1015;

            string title = "<b>Tên loại bài thi:</b><title>" +
                "<b>Trạng thái:</b>";

            string deleteLog = testtypeDB.TETY_NAME + "<item>"
                + (testtypeDB.TETY_STATUS == 1 ? "Sử dụng" : "Không sử dụng");

            try
            {
                testtypeDB.TETY_ISDELETED = true;
                testtypeDB.TETY_MODIFIEDBY = user;
                testtypeDB.TETY_MODIFIEDDATE = DateTime.Now;

                log.LOG_CONTENT = title + "<divider>" + deleteLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetTestType", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = testtypeDB
            };
        }
    }
}