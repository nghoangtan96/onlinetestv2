﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;
using OnlineTestBackend.Models.Request;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class StudentTestController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public StudentTestController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Student_Tests.
                Include(x => x.Students).
                Include(x => x.Schedule_Tests).
                AsNoTracking().
                ToListAsync()
            };
        }

        [HttpGet("getAllByUserId/{id}")]
        public async Task<ActionResult<BaseResponse>> getAllByUserId(long id)
        {
            var studentTest = await _context.Student_Tests.
                Include(x => x.Students).ThenInclude(x => x.Users).
                Include(x => x.Schedule_Tests).Where(x => x.Students.Users.Id == id).
                AsNoTracking().
                ToListAsync();

            if (studentTest == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = studentTest
            };
        }
        [HttpGet("getAllByUserIdForResult/{id}")]
        public async Task<ActionResult<BaseResponse>> getAllByUserIdForResult(string id)
        {
            var studentTest = await _context.Student_Tests.
                Include(x => x.Students).ThenInclude(x => x.Users).
                Include(x => x.Schedule_Tests).ThenInclude(x => x.Tests).ThenInclude(x => x.Test_Types).
                Include(x => x.Schedule_Tests).ThenInclude(x => x.Tests).ThenInclude(x => x.Subjects).
                Include(x => x.Schedule_Tests).ThenInclude(x => x.Labs).
                Where(x => x.Students.STU_CODE == id && x.STTE_STATUS == 4).
                OrderByDescending(x => x.STTE_MODIFIEDDATE).AsNoTracking().ToListAsync();

            if (_context.Students.FirstOrDefault(x => x.STU_CODE == id) == null)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Mã sinh viên không tồn tại!"
                };
            }

            if (studentTest.Count == 0)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = studentTest
            };
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var studentTest = await _context.Student_Tests.
                Include(x => x.Students).
                Include(x => x.Schedule_Tests).
                AsNoTracking().
                FirstOrDefaultAsync(x => x.Id == id);
            if (studentTest == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = studentTest
            };
        }
        [HttpGet("getByScheduleTestIdAndStudentId/{scteid}&{useid}")]
        public async Task<ActionResult<BaseResponse>> getByScheduleTestIdAndStudentId(long scteid, long useid)
        {
            var studentTest = await _context.Student_Tests.Include(x => x.Students).ThenInclude(x => x.Users).
                AsNoTracking().
                FirstOrDefaultAsync(x => x.STTE_SCTEID == scteid && x.Students.Users.Id == useid);
            if (studentTest == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = studentTest
            };
        }
        [HttpGet("getAllByScheduleTestId/{id}")]
        public async Task<ActionResult<BaseResponse>> getAllByScheduleTestId(long id)
        {
            var studentTest = await _context.Student_Tests.
                Include(x => x.Students).
                Include(x => x.Schedule_Tests).Where(x => x.STTE_SCTEID == id).
                AsNoTracking().
                ToListAsync();
            if (studentTest == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = studentTest
            };
        }

        [HttpPut("testSubmit/{scheduleTestId}&{userId}")]
        public async Task<ActionResult<BaseResponse>> testSubmit(long scheduleTestId, long userId, ExamRequest req)
        {
            Student_Test studentTestDB = await _context.Student_Tests.
                Include(x => x.Students).
                Include(x => x.Schedule_Tests).ThenInclude(x => x.TestShifts).
                Include(x => x.Schedule_Tests).ThenInclude(x => x.Tests).
                FirstOrDefaultAsync(x => x.STTE_SCTEID == scheduleTestId && x.Students.STU_USEID == userId);

            if (studentTestDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };

            if(studentTestDB.STTE_STATUS == 3)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Bài thi đã được nộp!"
                };
            }

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //submit
                    if (req.type == 1)
                    {
                        studentTestDB.STTE_MODIFIEDDATE = DateTime.Now;
                        studentTestDB.STTE_QUESTION = req.listQuestion;
                        studentTestDB.STTE_OPTION = req.listOption;
                        studentTestDB.STTE_STATUS = 3;
                        studentTestDB.STTE_REMAININGTIME = 0;
                        await _context.SaveChangesAsync();

                        //calculate score
                        int countQuestion = req.listQuestion.Split("-").Length;
                        double pointPerQuestion = (double)10 / (double)countQuestion;
                        double score = 0;
                        for (int i = 0; i < req.listQuestion.Split("-").Length; i++)
                        {
                            long queId = long.Parse(req.listQuestion.Split("-")[i]);
                            Question question = await _context.Questions.Include(x => x.Options).FirstOrDefaultAsync(x => x.Id == queId);
                            if (question.QUE_ANTYID == 2)
                            {
                                string opt = req.listOption.Split("-")[i];
                                string[] arrOpt = opt.Split(".");
                                bool isCorrect = true;
                                for (int j = 0; j < question.Options.Count; j++)
                                {
                                    if (question.Options.ElementAt(j).OPT_ISCORRECT == true)
                                    {
                                        if (arrOpt.FirstOrDefault(x => long.Parse(x) == question.Options.ElementAt(j).Id) == null)
                                        {
                                            isCorrect = false;
                                            break;
                                        }
                                        else
                                            continue;
                                    }
                                }

                                if (isCorrect == true)
                                {
                                    score += pointPerQuestion;
                                }
                            }
                            else
                            {
                                long optId = long.Parse(req.listOption.Split("-")[i]);
                                if (question.Options.FirstOrDefault(x => x.Id == optId && x.OPT_ISCORRECT == true) != null)
                                {
                                    score += pointPerQuestion;
                                }
                            }
                        }
                        studentTestDB.STTE_SCORE = Math.Round(score, 2);

                        //nofication
                        Nofication nof = new Nofication(); //created
                        nof.NOF_USEID = req.userId;
                        nof.NOF_CREATEDBY = 16;
                        nof.NOF_CREATEDDATE = DateTime.Now;
                        nof.NOF_PARAM = req.userId;
                        nof.NOF_STATUS = 1;
                        nof.NOF_CONTENT = "Điểm thi " +
                            _context.Test_Types.FirstOrDefault(x => x.Id == studentTestDB.Schedule_Tests.Tests.TES_TETYID).TETY_NAME +
                            " môn " + _context.Subjects.FirstOrDefault(x => x.Id == studentTestDB.Schedule_Tests.Tests.TES_SUBID).SUB_NAME +
                            " thi ngày " + studentTestDB.Schedule_Tests.SCTE_DATE.Day + "/" +
                            studentTestDB.Schedule_Tests.SCTE_DATE.Month + "/" +
                            studentTestDB.Schedule_Tests.SCTE_DATE.Year +
                            " " + studentTestDB.Schedule_Tests.TestShifts.SHI_NAME.ToLower() + " (" +
                            new DateTime().Add(studentTestDB.Schedule_Tests.TestShifts.SHI_TIMESTART).ToString("hh:mm tt") +
                            ") đã hoàn tất xử lý, vui lòng kiểm tra!";
                        nof.NOF_LINK = "/result";
                        nof.NOF_TITLE = "Thông báo hoàn tất xử lý điểm!";
                        _context.Nofications.Add(nof);
                        await _context.SaveChangesAsync();
                    }

                    transaction.Commit();

                    //await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetNofication", true);
                    await _hubContext.Clients.All.SendAsync("GetStudentTest", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse
            {
                data = studentTestDB
            };
        }

        [HttpPut("doingExam/{scheduleTestId}&{userId}")]
        public async Task<ActionResult<BaseResponse>> doingExam(long scheduleTestId, long userId, ExamRequest req)
        {
            Student_Test studentTestDB = await _context.Student_Tests.
                Include(x => x.Students).
                Include(x => x.Schedule_Tests).ThenInclude(x => x.TestShifts).
                Include(x => x.Schedule_Tests).ThenInclude(x => x.Tests).
                FirstOrDefaultAsync(x => x.STTE_SCTEID == scheduleTestId && x.Students.STU_USEID == userId);

            if (studentTestDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    studentTestDB.STTE_STATUS = 2;
                    studentTestDB.STTE_MODIFIEDDATE = DateTime.Now;
                    studentTestDB.STTE_OPTION = req.listOption;
                    studentTestDB.STTE_QUESTION = req.listQuestion;
                    await _context.SaveChangesAsync();

                    transaction.Commit();
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse
            {
                data = studentTestDB
            };
        }

        [HttpPut("updateExam/{scheduleTestId}&{userId}")]
        public async Task<ActionResult<BaseResponse>> updateExam(long scheduleTestId, long userId, ExamRequest req)
        {
            Student_Test studentTestDB = await _context.Student_Tests.
                Include(x => x.Students).
                Include(x => x.Schedule_Tests).ThenInclude(x => x.TestShifts).
                Include(x => x.Schedule_Tests).ThenInclude(x => x.Tests).
                FirstOrDefaultAsync(x => x.STTE_SCTEID == scheduleTestId && x.Students.STU_USEID == userId);

            if (studentTestDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            if (studentTestDB.STTE_STATUS == 3)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Bài thi đã kết thúc!"
                };
            }

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    studentTestDB.STTE_QUESTION = req.listQuestion;
                    studentTestDB.STTE_OPTION = req.listOption;
                    studentTestDB.STTE_REMAININGTIME = req.remainingTime / 1000;
                    studentTestDB.STTE_MODIFIEDDATE = DateTime.Now;
                    studentTestDB.STTE_CURRENTPART = req.currentPart;
                    studentTestDB.STTE_CURRENTTIMEAUDIO = req.currentTimeAudio;
                    await _context.SaveChangesAsync();

                    transaction.Commit();
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse
            {
                data = studentTestDB
            };
        }
    }
}