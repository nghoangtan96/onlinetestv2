﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public SubjectController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Subjects.Where(x => x.SUB_ISDELETED == false).
                Include(x => x.Subject_Branches).ThenInclude(x => x.Branches).
                Include(x => x.Faculties).AsNoTracking().
                ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var subject = await _context.Subjects.Where(x => x.SUB_ISDELETED == false).
                Include(x => x.Subject_Branches).ThenInclude(x => x.Branches).
                Include(x => x.Faculties).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.UserModified).ThenInclude(x => x.Employees).
                Include(x => x.Parts).ThenInclude(x => x.Questions).
                Include(x => x.Parts).ThenInclude(x => x.Passages).
                Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();

            if (subject == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            for (int i = 0; i < subject.Parts.Count; i++)
            {
                if (subject.Parts.ElementAt(i).Questions != null && subject.Parts.ElementAt(i).Questions.FirstOrDefault(x => x.QUE_ISDELETED == false) != null)
                {
                    subject.Parts.ElementAt(i).numberOfQuestion = _context.Questions.
                        Where(x => x.QUE_PARID == subject.Parts.ElementAt(i).Id && x.QUE_ISDELETED == false).ToList().Count;
                    subject.Parts.ElementAt(i).countQuestion = subject.Parts.ElementAt(i).numberOfQuestion;
                }
                if (subject.Parts.ElementAt(i).Passages != null && subject.Parts.ElementAt(i).Passages.FirstOrDefault(x => x.PAS_ISDELETED == false) != null)
                {
                    long count = 0;
                    List<Passage> listPassage = await _context.Passages.Include(x => x.Questions).
                        Where(x => x.PAS_PARID == subject.Parts.ElementAt(i).Id && x.PAS_ISDELETED == false).ToListAsync();
                    for (int j = 0; j < listPassage.Count; j++)
                    {
                        if (listPassage.ElementAt(j).Questions != null && listPassage.ElementAt(j).Questions.FirstOrDefault(x => x.QUE_ISDELETED == false) != null)
                        {
                            count += _context.Questions.
                                Where(x => x.QUE_PASID == listPassage.ElementAt(j).Id && x.QUE_ISDELETED == false).ToList().Count;
                        }
                    }

                    subject.Parts.ElementAt(i).numberOfQuestion = count;
                    subject.Parts.ElementAt(i).countPassage = count;
                }
            }

            if (subject.UserCreated != null)
            {
                if (subject.UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    subject.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    subject.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + subject.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
            }
            if (subject.UserModified != null)
            {
                if (subject.UserModified.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    subject.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    subject.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + subject.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
            }

            return new BaseResponse
            {
                data = subject
            };
        }
        [HttpGet("getListSubjectByFacultyId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListSubjectByFacultyId(long id)
        {
            return new BaseResponse
            {
                data = await _context.Subjects.
                Include(x => x.Subject_Branches).ThenInclude(x => x.Branches).
                Include(x => x.Faculties).
                Where(x => x.SUB_ISDELETED == false && x.SUB_FACID == id).AsNoTracking().
                ToListAsync()
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(Subject subject)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = subject.SUB_CREATEDBY.GetValueOrDefault();
            log.LOG_ACTID = 1;
            log.LOG_TABID = 5;
            log.LOG_MODULE = "Thêm môn học";
            string title = "<b>Tên môn học:</b>" + "<title>" +
                "<b>Mã môn học:</b>" + "<title>" +
                "<b>Số tín chỉ:</b>" + "<title>" +
                "<b>Thuộc ngành:</b>";
            string insertLog = subject.SUB_NAME + "<item>"
                + subject.SUB_CODE + "<item>"
                + subject.SUB_CREDIT + "<item>";

            if (subject.listBranch != null)
            {
                for (int i = 0; i < subject.listBranch.Length; i++)
                {
                    insertLog += _context.Branches.Where(x => x.Id == subject.listBranch[i]).FirstOrDefault().BRA_NAME;
                    if (i != subject.listBranch.Length - 1)
                        insertLog += "<opt>";
                }
            }
            else
            {
                insertLog += "Tất cả";
            }

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //subject
                    subject.SUB_CREATEDDATE = DateTime.Now;
                    subject.SUB_ISDELETED = false;
                    subject.SUB_STATUS = 1;

                    _context.Subjects.Add(subject);
                    await _context.SaveChangesAsync();

                    //subject_branch
                    if (subject.listBranch != null)
                    {
                        for (int i = 0; i < subject.listBranch.Length; i++)
                        {
                            Subject_Branch subjectBranch = new Subject_Branch();
                            subjectBranch.SUBR_BRAID = subject.listBranch[i];
                            subjectBranch.SUBR_SUBID = subject.Id;
                            subjectBranch.SUBR_STATUS = 1;
                            subjectBranch.SUBR_CREATEDBY = subject.SUB_CREATEDBY;
                            subjectBranch.SUBR_CREATEDDATE = DateTime.Now;

                            _context.Subject_Branches.Add(subjectBranch);
                            await _context.SaveChangesAsync();
                        }
                    }

                    log.LOG_CONTENT = title + "<divider>" + insertLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetSubject", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            };

            return new BaseResponse
            {
                data = subject
            };

        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(long id, Subject subject)
        {
            var subjectDB = await _context.Subjects.
                Include(x => x.Subject_Branches).ThenInclude(x => x.Branches).
                Where(x => x.Id == id).FirstOrDefaultAsync();

            if (subjectDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = subject.SUB_CREATEDBY.GetValueOrDefault();
            log.LOG_ACTID = 2;
            log.LOG_TABID = 5;
            log.LOG_MODULE = "Sửa môn học";
            string title = "<b>Tên môn học:</b>" + "<title>" +
                "<b>Mã môn học:</b>" + "<title>" +
                "<b>Số tín chỉ:</b>" + "<title>" +
                "<b>Thuộc ngành:</b>";

            string updateLog = subjectDB.SUB_NAME + "<to>" + subject.SUB_NAME + "<item>"
                + subjectDB.SUB_CODE + "<to>" + subject.SUB_CODE + "<item>"
                + subjectDB.SUB_CREDIT + "<to>" + subject.SUB_CREDIT + "<item>";

            if (subject.listBranch != null)
            {
                if (subjectDB.Subject_Branches.Count != 0)
                {
                    for (int i = 0; i < subjectDB.Subject_Branches.Count; i++)
                    {
                        updateLog += subjectDB.Subject_Branches.ElementAt(i).Branches.BRA_NAME;
                        if (i != subjectDB.Subject_Branches.Count - 1)
                            updateLog += ", ";
                    }
                    updateLog += "<to>";
                    for (int i = 0; i < subject.listBranch.Length; i++)
                    {
                        updateLog += _context.Branches.FirstOrDefault(x => x.Id == subject.listBranch[i]).BRA_NAME;
                        if (i != subject.listBranch.Length - 1)
                            updateLog += ", ";
                    }
                }
                else
                {
                    subjectDB.SUB_ISALL = false;
                    updateLog += "Tất cả<to>";
                    for (int i = 0; i < subject.listBranch.Length; i++)
                    {
                        updateLog += _context.Branches.FirstOrDefault(x => x.Id == subject.listBranch[i]).BRA_NAME;
                        if (i != subject.listBranch.Length - 1)
                            updateLog += ", ";
                    }
                }
            }
            else
            {
                if (subjectDB.Subject_Branches.Count != 0)
                {
                    subjectDB.SUB_ISALL = true;
                    for (int i = 0; i < subjectDB.Subject_Branches.Count; i++)
                    {
                        updateLog += subjectDB.Subject_Branches.ElementAt(i).Branches.BRA_NAME;
                        if (i != subjectDB.Subject_Branches.Count - 1)
                            updateLog += ", ";
                    }
                    updateLog += "<to>Tất cả";
                }
                else
                {
                    updateLog += "Tất cả<to>Tất cả";
                }
            }

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //subject
                    subjectDB.SUB_NAME = subject.SUB_NAME;
                    subjectDB.SUB_CODE = subject.SUB_CODE;
                    subjectDB.SUB_FACID = subject.SUB_FACID;
                    subjectDB.SUB_CREDIT = subject.SUB_CREDIT;
                    subjectDB.SUB_MODIFIEDBY = subject.SUB_MODIFIEDBY;
                    subjectDB.SUB_MODIFIEDDATE = DateTime.Now;

                    await _context.SaveChangesAsync();

                    //subject_branch
                    //insert
                    if (subject.listBranch != null)
                    {
                        for (int i = 0; i < subject.listBranch.Length; i++)
                        {
                            bool isExist = false;
                            for (int j = 0; j < subjectDB.Subject_Branches.Count; j++)
                            {
                                if (subject.listBranch[i] == subjectDB.Subject_Branches.ElementAt(j).SUBR_BRAID)
                                {
                                    isExist = true;
                                    break;
                                }
                            }
                            if (isExist == false)
                            {
                                Subject_Branch subjectBranch = new Subject_Branch();
                                subjectBranch.SUBR_BRAID = subject.listBranch[i];
                                subjectBranch.SUBR_SUBID = subject.Id;
                                subjectBranch.SUBR_STATUS = 1;
                                subjectBranch.SUBR_CREATEDBY = subject.SUB_CREATEDBY;
                                subjectBranch.SUBR_CREATEDDATE = DateTime.Now;

                                _context.Subject_Branches.Add(subjectBranch);
                                await _context.SaveChangesAsync();
                            }
                        }
                    }
                    //delete
                    if (subjectDB.Subject_Branches.Count != 0)
                    {
                        for (int i = 0; i < subjectDB.Subject_Branches.Count; i++)
                        {
                            bool isExist = false;
                            for (int j = 0; j < subject.listBranch.Length; j++)
                            {
                                if (subjectDB.Subject_Branches.ElementAt(i).SUBR_BRAID == subject.listBranch[j])
                                {
                                    isExist = true;
                                    break;
                                }
                            }
                            if (isExist == false)
                            {
                                _context.Subject_Branches.Remove(subjectDB.Subject_Branches.ElementAt(i));
                                await _context.SaveChangesAsync();
                            }
                        }
                    }

                    log.LOG_CONTENT = title + "<divider>" + updateLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetSubject", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            };

            return new BaseResponse
            {
                data = subject
            };
        }
        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long id, long user)
        {
            var subDB = await _context.Subjects.
                Include(x => x.Subject_Branches).ThenInclude(x => x.Branches).
                Include(x => x.Parts).ThenInclude(x => x.Passages).ThenInclude(x => x.Questions).
                Include(x => x.Parts).ThenInclude(x => x.Questions).
                FirstOrDefaultAsync(x => x.Id == id);

            if (subDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 5;
            log.LOG_MODULE = "Xóa môn học";

            string title = "<b>Tên môn học:</b>" + "<title>" +
                "<b>Mã môn học:</b>" + "<title>" +
                "<b>Số tín chỉ:</b>" + "<title>" +
                "<b>Thuộc ngành:</b>";
            string deleteLog = subDB.SUB_NAME + "<item>"
                + subDB.SUB_CODE + "<item>"
                + subDB.SUB_CREDIT + "<item>";

            if (subDB.Subject_Branches.Count != 0)
            {
                for (int i = 0; i < subDB.Subject_Branches.Count; i++)
                {
                    deleteLog += subDB.Subject_Branches.ElementAt(i).Branches.BRA_NAME;
                    if (i != subDB.Subject_Branches.Count - 1)
                        deleteLog += ", ";
                }
            }
            else
            {
                deleteLog += "Tất cả";
            }

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //part delete
                    if (subDB.Parts.Where(x => x.PAR_ISDELETED == false).ToList().Count != 0)
                    {
                        List<Part> listPart = subDB.Parts.Where(x => x.PAR_ISDELETED == false).ToList();
                        for (int i = 0; i < listPart.Count; i++)
                        {
                            if (listPart.ElementAt(i).Passages.Where(x => x.PAS_ISDELETED == false).ToList().Count != 0)
                            {
                                // passage delete
                                List<Passage> listPassage = listPart.ElementAt(i).Passages.Where(x => x.PAS_ISDELETED == false).ToList();
                                for (int j = 0; j < listPassage.Count; j++)
                                {
                                    if (listPassage.ElementAt(j).Questions.Where(x => x.QUE_ISDELETED == false).ToList().Count != 0)
                                    {
                                        // question delete
                                        List<Question> listQuestion = listPassage.ElementAt(j).Questions.Where(x => x.QUE_ISDELETED == false).ToList();
                                        for (int k = 0; k < listQuestion.Count; k++)
                                        {
                                            listQuestion.ElementAt(k).QUE_ISDELETED = true;
                                            listQuestion.ElementAt(k).QUE_MODIFIEDBY = user;
                                            listQuestion.ElementAt(k).QUE_MODIFIEDDATE = DateTime.Now;
                                            await _context.SaveChangesAsync();
                                        }
                                    }

                                    listPassage.ElementAt(j).PAS_ISDELETED = true;
                                    listPassage.ElementAt(j).PAS_MODIFIEDBY = user;
                                    listPassage.ElementAt(j).PAS_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                            }

                            if (listPart.ElementAt(i).Questions.Where(x => x.QUE_ISDELETED == false).ToList().Count != 0)
                            {
                                List<Question> listQuestion = listPart.ElementAt(i).Questions.Where(x => x.QUE_ISDELETED == false).ToList();
                                for (int j = 0; j < listQuestion.Count; j++)
                                {
                                    listQuestion.ElementAt(j).QUE_ISDELETED = true;
                                    listQuestion.ElementAt(j).QUE_MODIFIEDBY = user;
                                    listQuestion.ElementAt(j).QUE_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                            }

                            listPart.ElementAt(i).PAR_ISDELETED = true;
                            listPart.ElementAt(i).PAR_MODIFIEDBY = user;
                            listPart.ElementAt(i).PAR_MODIFIEDDATE = DateTime.Now;
                            await _context.SaveChangesAsync();
                        }
                    }


                    subDB.SUB_MODIFIEDBY = user;
                    subDB.SUB_MODIFIEDDATE = DateTime.Now;
                    subDB.SUB_ISDELETED = true;
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetSubject", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            log.LOG_CONTENT = title + "<divider>" + deleteLog;
            _context.Logs.Add(log);
            await _context.SaveChangesAsync();

            return new BaseResponse
            {
                data = subDB
            };
        }

        [HttpPost("multipleDelete/{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long user, Subject[] arrDel)
        {

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 5;
            log.LOG_MODULE = "Xóa danh sách môn học";

            string title = "<b>Tên môn học:</b>" + "<title>" +
                "<b>Mã môn học:</b>" + "<title>" +
                "<b>Số tín chỉ:</b>" + "<title>" +
                "<b>Thuộc ngành:</b>";
            string multiDeleteLog = "";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i = 0; i < arrDel.Length; i++)
                    {
                        var subDB = _context.Subjects.
                            Include(x => x.Subject_Branches).ThenInclude(x => x.Branches).
                            Include(x => x.Parts).ThenInclude(x => x.Passages).ThenInclude(x => x.Questions).
                            Include(x => x.Parts).ThenInclude(x => x.Questions).
                            FirstOrDefault(x => x.Id == arrDel[i].Id);
                        if (subDB != null)
                        {
                            multiDeleteLog += subDB.SUB_NAME + "<item>"
                                + subDB.SUB_CODE + "<item>"
                                + subDB.SUB_CREDIT + "<item>";

                            if (subDB.Subject_Branches != null)
                            {
                                for (int j = 0; j < subDB.Subject_Branches.Count; j++)
                                {
                                    multiDeleteLog += subDB.Subject_Branches.ElementAt(i).Branches.BRA_NAME;
                                    if (i != subDB.Subject_Branches.Count - 1)
                                        multiDeleteLog += ", ";
                                }
                            }
                            else
                            {
                                multiDeleteLog += "Tất cả";
                            }

                            //part delete
                            if (subDB.Parts.Where(x => x.PAR_ISDELETED == false).ToList().Count != 0)
                            {
                                List<Part> listPart = subDB.Parts.Where(x => x.PAR_ISDELETED == false).ToList();
                                for (int j = 0; j < listPart.Count; j++)
                                {
                                    if (listPart.ElementAt(j).Passages.Where(x => x.PAS_ISDELETED == false).ToList().Count != 0)
                                    {
                                        // passage delete
                                        List<Passage> listPassage = listPart.ElementAt(j).Passages.Where(x => x.PAS_ISDELETED == false).ToList();
                                        for (int k = 0; k < listPassage.Count; k++)
                                        {
                                            if (listPassage.ElementAt(k).Questions.Where(x => x.QUE_ISDELETED == false).ToList().Count != 0)
                                            {
                                                // question delete
                                                List<Question> listQuestion = listPassage.ElementAt(j).Questions.Where(x => x.QUE_ISDELETED == false).ToList();
                                                for (int l = 0; l < listQuestion.Count; l++)
                                                {
                                                    listQuestion.ElementAt(l).QUE_ISDELETED = true;
                                                    listQuestion.ElementAt(l).QUE_MODIFIEDBY = user;
                                                    listQuestion.ElementAt(l).QUE_MODIFIEDDATE = DateTime.Now;
                                                    await _context.SaveChangesAsync();
                                                }
                                            }

                                            listPassage.ElementAt(k).PAS_ISDELETED = true;
                                            listPassage.ElementAt(k).PAS_MODIFIEDBY = user;
                                            listPassage.ElementAt(k).PAS_MODIFIEDDATE = DateTime.Now;
                                            await _context.SaveChangesAsync();
                                        }
                                    }

                                    if (listPart.ElementAt(j).Questions.Where(x => x.QUE_ISDELETED == false).ToList().Count != 0)
                                    {
                                        List<Question> listQuestion = listPart.ElementAt(j).Questions.Where(x => x.QUE_ISDELETED == false).ToList();
                                        for (int k = 0; k < listQuestion.Count; k++)
                                        {
                                            listQuestion.ElementAt(k).QUE_ISDELETED = true;
                                            listQuestion.ElementAt(k).QUE_MODIFIEDBY = user;
                                            listQuestion.ElementAt(k).QUE_MODIFIEDDATE = DateTime.Now;
                                            await _context.SaveChangesAsync();
                                        }
                                    }

                                    listPart.ElementAt(j).PAR_ISDELETED = true;
                                    listPart.ElementAt(j).PAR_MODIFIEDBY = user;
                                    listPart.ElementAt(j).PAR_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                            }

                            subDB.SUB_MODIFIEDBY = user;
                            subDB.SUB_MODIFIEDDATE = DateTime.Now;
                            subDB.SUB_ISDELETED = true;
                            await _context.SaveChangesAsync();

                        }
                    }

                    log.LOG_CONTENT += title + "<divider>" + multiDeleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetSubject", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse { };
        }
        [HttpPost("getListSubjectMultiDel")]
        public async Task<ActionResult<BaseResponse>> getListSubjectMultiDel(long[] arrDel)
        {
            List<Subject> listSub = new List<Subject>();
            for (int i = 0; i < arrDel.Length; i++)
            {
                Subject sub = await _context.Subjects.
                    Include(x => x.Faculties).
                    Include(x => x.Subject_Branches).ThenInclude(x => x.Branches).
                    Where(x => x.Id == arrDel[i]).AsNoTracking().FirstOrDefaultAsync();
                if (sub != null)
                {
                    listSub.Add(sub);
                }
            }
            return new BaseResponse
            {
                data = listSub
            };
        }
    }
}