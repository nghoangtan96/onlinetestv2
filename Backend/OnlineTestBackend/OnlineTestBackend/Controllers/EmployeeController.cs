﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OnlineTestBackend.Models;
using OnlineTestBackend.Models.ExportData;
using OnlineTestBackend.Models.Request;
using OnlineTestBackend.Utils;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : BaseController
    {
        private IHostingEnvironment _hostingEnv;
        private readonly IHubContext<SignalHub> _hubContext;
        public EmployeeController(OnlineTestBackendDBContext db, IHostingEnvironment env, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hostingEnv = env;
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            List<Employee> listEmployee = await _context.Employees.Where(x => x.EMP_ISDELETED == false).
                Include(x => x.Users).
                Include(x => x.Faculties).
                Include(x => x.Employee_Subjects).ThenInclude(x => x.Subjects).AsNoTracking().
                ToListAsync();
            for (int i = 0; i < listEmployee.Count; i++)
            {
                listEmployee.ElementAt(i).Users.User_Permissions = await _context.User_Permissions.
                    Include(x => x.Permissions).
                    AsNoTracking().
                    Where(x => x.USPE_USEID == listEmployee.ElementAt(i).Users.Id).ToListAsync();
            }
            return new BaseResponse
            {
                data = listEmployee
            };
        }
        [HttpGet("GetAllExceptSelf/{id}")]
        public async Task<ActionResult<BaseResponse>> GetAllExceptSelf(long id)
        {
            List<Employee> listEmployee = await _context.Employees.
                Include(x => x.Users).
                Include(x => x.Employee_Subjects).ThenInclude(x => x.Subjects).
                Where(x => x.EMP_ISDELETED == false && x.Id != id).ToListAsync();
            for (int i = 0; i < listEmployee.Count; i++)
            {
                listEmployee.ElementAt(i).Users.User_Permissions = await _context.User_Permissions.
                    Include(x => x.Permissions).Where(x => x.USPE_USEID == listEmployee.ElementAt(i).Users.Id).ToListAsync();
            }
            return new BaseResponse
            {
                data = listEmployee
            };
        }
        [HttpGet("getListByPermissionId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListByPermissionId(int id)
        {
            List<Employee> listEmployee = new List<Employee>();
            List<User_Permission> listUserPermission = await _context.User_Permissions.
                Where(x => x.USPE_PERID == id).AsNoTracking().ToListAsync();

            for (int i = 0; i < listUserPermission.Count; i++)
            {
                Employee employee = await _context.Employees.
                    Include(x => x.Users).
                    Include(x => x.Faculties).
                    Include(x => x.Employee_Subjects).ThenInclude(x => x.Subjects).
                    AsNoTracking().
                    FirstOrDefaultAsync(x => x.EMP_ISDELETED == false && x.Users.Id == listUserPermission.ElementAt(i).USPE_USEID);
                if (employee != null)
                {
                    employee.Users.User_Permissions = await _context.User_Permissions.Include(x => x.Permissions).
                    Where(x => x.USPE_USEID == employee.Users.Id).AsNoTracking().ToListAsync();
                    listEmployee.Add(employee);
                }
            }

            return new BaseResponse
            {
                data = listEmployee
            };
        }
        [HttpGet("getListByFacultyId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListByFacultyId(int id)
        {
            List<Employee> listEmployee = await _context.Employees.
                Include(x => x.Users).Include(x => x.Faculties).
                Include(x => x.Employee_Subjects).ThenInclude(x => x.Subjects).
                AsNoTracking().
                Where(x => x.EMP_ISDELETED == false && x.EMP_FACID == id).ToListAsync();

            for (int i = 0; i < listEmployee.Count; i++)
            {
                listEmployee.ElementAt(i).Users.User_Permissions = await _context.User_Permissions.
                    Include(x => x.Permissions).
                    Where(x => x.USPE_USEID == listEmployee.ElementAt(i).Users.Id).AsNoTracking().ToListAsync();
            }

            return new BaseResponse
            {
                data = listEmployee
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var employee = await _context.Employees.Where(x => x.EMP_ISDELETED == false && x.Id == id).
                Include(x => x.Users).
                Include(x => x.Employee_Subjects).ThenInclude(x => x.Subjects).
                Include(x => x.Faculties).
                AsNoTracking().
                FirstOrDefaultAsync();
            if (employee == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (employee.EMP_AVATAR != null)
            {
                employee.FileUrl = Utils.Helper.GetBaseUrl(Request) + employee.EMP_AVATAR;
            }
            else
            {
                employee.FileUrl = "assets/img/default_avatar.png";
            }

            employee.Users.User_Permissions = await _context.User_Permissions.Where(x => x.USPE_USEID == employee.Users.Id).
                Include(x => x.Permissions).AsNoTracking().ToListAsync();

            if (employee.EMP_CREATEDBY != null)
            {
                employee.UserCreated = await _context.Users.Include(x => x.Employees).AsNoTracking().FirstOrDefaultAsync(x => x.Id == employee.EMP_CREATEDBY);
                if (employee.UserCreated.Employees.ElementAt(0).EMP_AVATAR != null)
                {
                    employee.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + employee.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
                else
                {
                    employee.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
            }

            if (employee.EMP_MODIFIEDBY != null)
            {
                employee.UserModified = await _context.Users.Include(x => x.Employees).AsNoTracking().FirstOrDefaultAsync(x => x.Id == employee.EMP_MODIFIEDBY);
                if (employee.UserModified.Employees.ElementAt(0).EMP_AVATAR != null)
                {
                    employee.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + employee.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
                else
                {
                    employee.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
            }

            return new BaseResponse
            {
                data = employee
            };
        }
        [HttpPost("exportExcelLinkDownload")]
        public ActionResult<BaseResponse> Export(CancellationToken cancellationToken, Employee[] listEmployeeRequest)
        {
            string excelFileName = $"{DateTime.Now.ToString("yyyyMMddHHmmssfff")}_Danh sách nhân viên.xlsx";
            string physicalPath = Path.Combine(_hostingEnv.ContentRootPath, "Data\\Excels\\", excelFileName);

            if (!Directory.Exists(Path.Combine(_hostingEnv.ContentRootPath, "Data\\Excels", "")))
                Directory.CreateDirectory(Path.Combine(_hostingEnv.ContentRootPath, "Data\\Excels", ""));

            DirectoryInfo dir = new DirectoryInfo(Path.Combine(_hostingEnv.ContentRootPath, "Data\\Excels", ""));
            FileInfo[] files = dir.GetFiles("*Danh sách nhân viên*", SearchOption.TopDirectoryOnly);
            foreach (var item in files)
            {
                if (item.Exists)
                {
                    item.Delete();
                }
            }

            FileInfo file = new FileInfo(physicalPath);

            string downloadUrl = string.Format("{0}/{1}/{2}", Utils.Helper.GetBaseUrl(Request), "Data/Excels", excelFileName);

            // format data
            List<EmployeeData> listEmployeeResult = new List<EmployeeData>();

            if (listEmployeeRequest.Length == 0)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            for (int i = 0; i < listEmployeeRequest.Length; i++)
            {
                EmployeeData empData = new EmployeeData();
                empData.stt = (i + 1);
                empData.code = listEmployeeRequest[i].EMP_CODE;
                empData.identity = listEmployeeRequest[i].EMP_IDENTITY;
                empData.lastname = listEmployeeRequest[i].EMP_LASTNAME;
                empData.firstname = listEmployeeRequest[i].EMP_FIRSTNAME;
                empData.gender = (listEmployeeRequest[i].EMP_GENDER == true ? "Nam" : "Nữ");
                empData.dob = (listEmployeeRequest[i].EMP_DATEOFBIRTH.GetValueOrDefault().Day + "/" +
                    listEmployeeRequest[i].EMP_DATEOFBIRTH.GetValueOrDefault().Month + "/" +
                    listEmployeeRequest[i].EMP_DATEOFBIRTH.GetValueOrDefault().Year);
                empData.phone = listEmployeeRequest[i].EMP_PHONE;
                empData.email = listEmployeeRequest[i].EMP_EMAIL;
                empData.address = listEmployeeRequest[i].EMP_ADDRESS;
                empData.faculty = listEmployeeRequest[i].Faculties.FAC_NAME;
                empData.username = listEmployeeRequest[i].Users.USE_USERNAME;
                empData.accountstatus = (listEmployeeRequest[i].Users.USE_STATUS == 1 ? "Sử dụng" : "Khóa");
                for (int j = 0; j < listEmployeeRequest[i].Employee_Subjects.Count; j++)
                {
                    empData.subjects += listEmployeeRequest[i].Employee_Subjects.ElementAt(j).Subjects.SUB_NAME;
                    if (j != listEmployeeRequest[i].Employee_Subjects.Count - 1)
                        empData.subjects += ", ";
                }
                for (int j = 0; j < listEmployeeRequest[i].Users.User_Permissions.Count; j++)
                {
                    empData.permission += listEmployeeRequest[i].Users.User_Permissions.ElementAt(j).Permissions.PER_NAME;
                    if (j != listEmployeeRequest[i].Users.User_Permissions.Count - 1)
                        empData.permission += ", ";
                }
                listEmployeeResult.Add(empData);
            }
            using (var package = new ExcelPackage(file))
            {
                var worksheet = package.Workbook.Worksheets.Add("Sheet1");
                worksheet.Cells.LoadFromCollection(listEmployeeResult, true);

                var allCells = worksheet.Cells[worksheet.Dimension.Address];
                allCells.AutoFilter = true;

                // Auto-fit all the columns
                allCells.AutoFitColumns();

                var headerCells = worksheet.Cells[1, 1, 1, worksheet.Dimension.Columns];
                headerCells.Style.Font.Bold = true;
                headerCells.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                headerCells.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);

                package.Save();
            }

            return new BaseResponse
            {
                data = downloadUrl
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(EmployeeRequest employeeRequest)
        {
            string rawPassword = employeeRequest.user.USE_PASSWORD;
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = employeeRequest.employee.EMP_CREATEDBY.GetValueOrDefault();
            log.LOG_ACTID = 1;
            log.LOG_TABID = 9;
            log.LOG_MODULE = "Thêm nhân viên";
            string title = "<b>Họ:</b><title>" +
                "<b>Tên:</b><title>" +
                "<b>Mã nhân viên:</b><title>" +
                "<b>CMND:</b><title>" +
                "<b>Ngày sinh:</b><title>" +
                "<b>Giới tính:</b><title>" +
                "<b>Số điện thoại:</b><title>" +
                "<b>Địa chỉ:</b><title>" +
                "<b>Email:</b><title>" +
                "<b>Thuộc khoa:<b><title>" +
                "<b>Chuyên môn:</b><title>" +
                "<b>Tên tài khoản:</b><title>" +
                "<b>Trạng thái tài khoản:</b><title>" +
                "<b>Vai trò:</b>";
            string insertLog = employeeRequest.employee.EMP_LASTNAME + "<item>"
                + employeeRequest.employee.EMP_FIRSTNAME + "<item>"
                + employeeRequest.employee.EMP_CODE + "<item>"
                + employeeRequest.employee.EMP_IDENTITY + "<item>"
                + employeeRequest.employee.EMP_DATEOFBIRTH.GetValueOrDefault().Day + "/" + employeeRequest.employee.EMP_DATEOFBIRTH.GetValueOrDefault().Month + "/" + employeeRequest.employee.EMP_DATEOFBIRTH.GetValueOrDefault().Year + "<item>"
                + (employeeRequest.employee.EMP_GENDER == true ? "Nam" : "Nữ") + "<item>"
                + employeeRequest.employee.EMP_PHONE + "<item>"
                + employeeRequest.employee.EMP_ADDRESS + "<item>"
                + employeeRequest.employee.EMP_EMAIL + "<item>"
                + _context.Faculties.FirstOrDefault(x => x.Id == employeeRequest.employee.EMP_FACID).FAC_NAME + "<item>";

            if (employeeRequest.list_subject.Length != 0)
            {
                for (int i = 0; i < employeeRequest.list_subject.Length; i++)
                {
                    insertLog += _context.Subjects.FirstOrDefault(x => x.Id == employeeRequest.list_subject[i]).SUB_NAME;
                    if (i != employeeRequest.list_subject.Length - 1)
                        insertLog += ", ";
                }
            }
            insertLog += "<item>" + employeeRequest.user.USE_USERNAME + "<item>"
                + (employeeRequest.user.USE_STATUS == 1 ? "Sử dụng" : "Khóa") + "<item>";
            for (int i = 0; i < employeeRequest.user_permissions.Length; i++)
            {
                insertLog += _context.Permissions.FirstOrDefault(x => x.Id == employeeRequest.user_permissions[i]).PER_NAME;
                if (i != employeeRequest.user_permissions.Length - 1)
                    insertLog += ", ";
            }


            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //user
                    employeeRequest.user.USE_PASSWORD = Utils.Helper.GenHash(employeeRequest.user.USE_PASSWORD);
                    employeeRequest.user.USE_CREATEDDATE = DateTime.Now;
                    employeeRequest.user.USE_ISDELETED = false;

                    _context.Users.Add(employeeRequest.user);
                    await _context.SaveChangesAsync();

                    //employee
                    employeeRequest.employee.EMP_CREATEDDATE = DateTime.Now;
                    employeeRequest.employee.EMP_ISDELETED = false;
                    employeeRequest.employee.EMP_USEID = employeeRequest.user.Id;

                    _context.Employees.Add(employeeRequest.employee);
                    await _context.SaveChangesAsync();

                    //employee_subject
                    if (employeeRequest.list_subject.Length != 0)
                    {
                        for (int i = 0; i < employeeRequest.list_subject.Length; i++)
                        {
                            Employee_Subject employeeSubject = new Employee_Subject();
                            employeeSubject.EMSU_EMPID = employeeRequest.employee.Id;
                            employeeSubject.EMSU_SUBID = employeeRequest.list_subject[i];
                            employeeSubject.EMSU_CREATEDBY = employeeRequest.employee.EMP_CREATEDBY;
                            employeeSubject.EMSU_CREATEDDATE = DateTime.Now;

                            _context.Employee_Subjects.Add(employeeSubject);
                            await _context.SaveChangesAsync();
                        }
                    }

                    //user_permission
                    for (int i = 0; i < employeeRequest.user_permissions.Length; i++)
                    {
                        User_Permission userPermission = new User_Permission();
                        userPermission.USPE_CREATEDBY = employeeRequest.user.USE_CREATEDBY;
                        userPermission.USPE_CREATEDDATE = DateTime.Now;
                        userPermission.USPE_USEID = employeeRequest.user.Id;
                        userPermission.USPE_STATUS = 1;
                        userPermission.USPE_PERID = employeeRequest.user_permissions[i];

                        _context.User_Permissions.Add(userPermission);
                        await _context.SaveChangesAsync();
                    }

                    //log
                    log.LOG_CONTENT = title + "<divider>" + insertLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    //nofication
                    Nofication nofication = new Nofication();
                    nofication.NOF_USEID = employeeRequest.user.Id;
                    nofication.NOF_CREATEDBY = employeeRequest.employee.EMP_CREATEDBY;
                    nofication.NOF_LINK = "/info_user";
                    nofication.NOF_CREATEDDATE = DateTime.Now;
                    nofication.NOF_STATUS = 1;
                    nofication.NOF_TITLE = "Thông báo bảo mật tài khoản";
                    nofication.NOF_CONTENT = "Tài khoản của bạn vừa được tạo, vui lòng đổi mật khẩu để đảm bảo tài khoản an toàn!";
                    _context.Nofications.Add(nofication);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetEmployee", true);

                    if (employeeRequest.employee.EMP_EMAIL != null)
                    {
                        List<Information> listInformation = await _context.Informations.
                            Where(x => x.INF_ISMULTI == true && x.INF_ISDELETED == false).ToListAsync();

                        MailAddress to = new MailAddress(employeeRequest.employee.EMP_EMAIL);
                        MailAddress from = new MailAddress("hcmunre.fake@gmail.com");

                        MailMessage message = new MailMessage(from, to);
                        message.Subject = "Cấp tài khoản cho nhân viên " + employeeRequest.employee.EMP_LASTNAME + " " + employeeRequest.employee.EMP_FIRSTNAME;
                        message.Body = "Kính gửi " + (employeeRequest.employee.EMP_GENDER == true ? "ông" : "bà") + " <span class='text-capitalize'>" +
                            employeeRequest.employee.EMP_LASTNAME + " " + employeeRequest.employee.EMP_FIRSTNAME + "</span>,<br>" +
                            listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + " thông báo: <br> " +
                            "Bạn đã được cấp tài khoản sử dụng phần mềm quản lý thi trắc nghiệm online, " +
                            "vui lòng truy cập và sử dụng tại địa chỉ " +
                            "<a href='" + employeeRequest.employee.origin + "'>" + employeeRequest.employee.origin + "</a> với tài khoản:<br>" +
                            "<span class='pl-4'><b>Tên tài khoản:</b> " + employeeRequest.user.USE_USERNAME + "</span><br>" +
                            "<span class='pl-4'><b>Mật khẩu:</b> " + rawPassword + "</span><br>" +
                            "<br>Mọi chi tiết thắc mắc vui lòng liên hệ:<br>" + listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + "<br>" +
                            "Số điện thoại: " + listInformation.Where(x => x.INF_KEYNAME == "Số điện thoại").FirstOrDefault().INF_VALUE + "<br>" +
                            "Email: " + listInformation.Where(x => x.INF_KEYNAME == "Email").FirstOrDefault().INF_VALUE + "<br>" +
                            "Địa chỉ: " + listInformation.Where(x => x.INF_KEYNAME == "Địa chỉ").FirstOrDefault().INF_VALUE;
                        message.IsBodyHtml = true;
                        //message.Attachments.Add(new Attachment(_hostingEnv.ContentRootPath + "\\Data\\Users\\default_avatar.png"));

                        SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
                        {
                            Credentials = new NetworkCredential("hcmunre.fake@gmail.com", "tnmt@12345"),
                            EnableSsl = true
                        };

                        try
                        {
                            await client.SendMailAsync(message);
                        }
                        catch (SmtpException ex) { Console.WriteLine(ex.ToString()); }
                    }
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            };

            //speedsms
            //if (employeeRequest.employee.EMP_PHONE != null)
            //{
            //    SpeedSMSAPI api = new SpeedSMSAPI();

            //    String[] phones = new String[] { employeeRequest.employee.EMP_PHONE };
            //    String str = "Bạn đã được cấp tài khoản sử dụng phần mềm quản lý thi trắc nghiệm online " +
            //        "(truy cập " + employeeRequest.employee.origin + " để sử dụng) bởi Trường Đại học Tài nguyên & Môi trường" +
            //        "tp Hồ Chí Minh. Tên tài khoản: " + employeeRequest.user.USE_USERNAME + ", Mật khẩu: " + rawPassword;
            //    String response = api.sendSMS(phones, str, 3, "HCMUNRE");
            //    Console.WriteLine(response);
            //    Console.ReadLine();
            //}

            return new BaseResponse
            {
                data = employeeRequest.employee
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(long id, EmployeeRequest employeeRequest)
        {
            Employee employeeDB = await _context.Employees.Where(x => x.Id == id && x.EMP_ISDELETED == false).
                Include(x => x.Users).FirstOrDefaultAsync();
            if (employeeDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = employeeRequest.employee.EMP_MODIFIEDBY.GetValueOrDefault();
            log.LOG_ACTID = 2;
            log.LOG_TABID = 9;
            log.LOG_MODULE = "Sửa thông tin nhân viên";

            string title = "<b>Họ:</b><title>" +
                "<b>Tên:</b><title>" +
                "<b>Mã nhân viên:</b><title>" +
                "<b>CMND:</b><title>" +
                "<b>Ngày sinh:</b><title>" +
                "<b>Giới tính:</b><title>" +
                "<b>Số điện thoại:</b><title>" +
                "<b>Địa chỉ:</b><title>" +
                "<b>Email:</b><title>" +
                "<b>Thuộc khoa:<b><title>" +
                "<b>Chuyên môn:</b><title>" +
                "<b>Tên tài khoản:</b><title>" +
                "<b>Trạng thái tài khoản:</b><title>" +
                "<b>Vai trò:</b>";
            string updateLog = employeeDB.EMP_LASTNAME + "<to>" + employeeRequest.employee.EMP_LASTNAME + "<item>" +
                employeeDB.EMP_FIRSTNAME + "<to>" + employeeRequest.employee.EMP_FIRSTNAME + "<item>" +
                employeeDB.EMP_CODE + "<to>" + employeeRequest.employee.EMP_CODE + "<item>" +
                employeeDB.EMP_IDENTITY + "<to>" + employeeRequest.employee.EMP_IDENTITY + "<item>" +
                employeeDB.EMP_DATEOFBIRTH.GetValueOrDefault().Day + "/" + employeeDB.EMP_DATEOFBIRTH.GetValueOrDefault().Month + "/" + employeeDB.EMP_DATEOFBIRTH.GetValueOrDefault().Year + "<to>" +
                employeeRequest.employee.EMP_DATEOFBIRTH.GetValueOrDefault().Day + "/" + employeeRequest.employee.EMP_DATEOFBIRTH.GetValueOrDefault().Month + "/" + employeeRequest.employee.EMP_DATEOFBIRTH.GetValueOrDefault().Year + "<item>" +
                (employeeDB.EMP_GENDER == true ? "Nam" : "Nữ") + "<to>" + (employeeRequest.employee.EMP_GENDER == true ? "Nam" : "Nữ") + "<item>" +
                employeeDB.EMP_PHONE + "<to>" + employeeRequest.employee.EMP_PHONE + "<item>" +
                employeeDB.EMP_ADDRESS + "<to>" + employeeRequest.employee.EMP_ADDRESS + "<item>" +
                employeeDB.EMP_EMAIL + "<to>" + employeeRequest.employee.EMP_EMAIL + "<item>" +
                _context.Faculties.FirstOrDefault(x => x.Id == employeeDB.EMP_FACID).FAC_NAME + "<to>" + _context.Faculties.FirstOrDefault(x => x.Id == employeeRequest.employee.EMP_FACID).FAC_NAME + "<item>";

            var listEmployeeSubject = await _context.Employee_Subjects.
                Where(x => x.EMSU_EMPID == employeeDB.Id).Include(x => x.Subjects).ToListAsync();
            for (int i = 0; i < listEmployeeSubject.Count; i++)
            {
                updateLog += listEmployeeSubject.ElementAt(i).Subjects.SUB_NAME;
                if (i != listEmployeeSubject.Count - 1)
                    updateLog += ", ";
            }
            updateLog += "<to>";
            for (int i = 0; i < employeeRequest.list_subject.Length; i++)
            {
                var subject = await _context.Subjects.FirstOrDefaultAsync(x => x.Id == employeeRequest.list_subject[i]);
                updateLog += subject.SUB_NAME;
                if (i != employeeRequest.list_subject.Length - 1)
                    updateLog += ", ";
            }

            updateLog += "<item>" + employeeDB.Users.USE_USERNAME + "<to>" + employeeRequest.user.USE_USERNAME + "<item>" +
                (employeeDB.Users.USE_STATUS == 1 ? "Sử dụng" : "Khóa") + "<to>" + (employeeRequest.user.USE_STATUS == 1 ? "Sử dụng" : "Khóa") + "<item>";

            var listUserPermission = await _context.User_Permissions.
                Where(x => x.USPE_USEID == employeeDB.Users.Id).Include(x => x.Permissions).ToListAsync();
            for (int i = 0; i < listUserPermission.Count; i++)
            {
                updateLog += listUserPermission.ElementAt(i).Permissions.PER_NAME;
                if (i != listUserPermission.Count - 1)
                    updateLog += ", ";
            }
            updateLog += "<to>";
            for (int i = 0; i < employeeRequest.user_permissions.Length; i++)
            {
                var permission = await _context.Permissions.FirstOrDefaultAsync(x => x.Id == employeeRequest.user_permissions[i]);
                updateLog += permission.PER_NAME;
                if (i != employeeRequest.user_permissions.Length - 1)
                    updateLog += ", ";
            }

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //user
                    employeeDB.Users.USE_USERNAME = employeeRequest.user.USE_USERNAME;
                    if (employeeDB.Users.USE_PASSWORD != employeeRequest.user.USE_PASSWORD)
                    {
                        employeeDB.Users.USE_PASSWORD = employeeRequest.user.USE_PASSWORD;
                    }
                    employeeDB.Users.USE_STATUS = employeeRequest.user.USE_STATUS;
                    employeeDB.Users.USE_MODIFIEDBY = employeeRequest.user.USE_MODIFIEDBY;
                    employeeDB.Users.USE_MODIFIEDDATE = DateTime.Now;
                    await _context.SaveChangesAsync();

                    //employee
                    employeeDB.EMP_FIRSTNAME = employeeRequest.employee.EMP_FIRSTNAME;
                    employeeDB.EMP_LASTNAME = employeeRequest.employee.EMP_LASTNAME;
                    employeeDB.EMP_DATEOFBIRTH = employeeRequest.employee.EMP_DATEOFBIRTH;
                    employeeDB.EMP_GENDER = employeeRequest.employee.EMP_GENDER;
                    employeeDB.EMP_EMAIL = employeeRequest.employee.EMP_EMAIL;
                    employeeDB.EMP_PHONE = employeeRequest.employee.EMP_PHONE;
                    employeeDB.EMP_CODE = employeeRequest.employee.EMP_CODE;
                    employeeDB.EMP_IDENTITY = employeeRequest.employee.EMP_IDENTITY;
                    employeeDB.EMP_ADDRESS = employeeRequest.employee.EMP_ADDRESS;
                    employeeDB.EMP_FACID = employeeRequest.employee.EMP_FACID;
                    employeeDB.EMP_AVATAR = employeeRequest.employee.EMP_AVATAR;
                    employeeDB.EMP_MODIFIEDBY = employeeRequest.employee.EMP_MODIFIEDBY;
                    employeeDB.EMP_MODIFIEDDATE = DateTime.Now;
                    await _context.SaveChangesAsync();

                    //user permission
                    if (listUserPermission.Count == employeeRequest.user_permissions.Length)
                    {
                        for (int i = 0; i < listUserPermission.Count; i++)
                        {
                            listUserPermission.ElementAt(i).USPE_PERID = employeeRequest.user_permissions[i];
                            listUserPermission.ElementAt(i).USPE_MODIFIEDBY = employeeRequest.employee.EMP_MODIFIEDBY;
                            listUserPermission.ElementAt(i).USPE_MODIFIEDDATE = DateTime.Now;
                            await _context.SaveChangesAsync();
                        }
                    }
                    else
                    {
                        if (listUserPermission.Count > employeeRequest.user_permissions.Length)
                        {
                            for (int i = 0; i < listUserPermission.Count; i++)
                            {
                                if (i <= employeeRequest.user_permissions.Length - 1)
                                {
                                    listUserPermission.ElementAt(i).USPE_PERID = employeeRequest.user_permissions[i];
                                    listUserPermission.ElementAt(i).USPE_MODIFIEDBY = employeeRequest.employee.EMP_MODIFIEDBY;
                                    listUserPermission.ElementAt(i).USPE_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                                else
                                {
                                    _context.User_Permissions.Remove(listUserPermission.ElementAt(i));
                                    await _context.SaveChangesAsync();
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < employeeRequest.user_permissions.Length; i++)
                            {
                                if (i <= listUserPermission.Count - 1)
                                {
                                    listUserPermission.ElementAt(i).USPE_PERID = employeeRequest.user_permissions[i];
                                    listUserPermission.ElementAt(i).USPE_MODIFIEDBY = employeeRequest.employee.EMP_MODIFIEDBY;
                                    listUserPermission.ElementAt(i).USPE_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                                else
                                {
                                    User_Permission userPermission = new User_Permission();
                                    userPermission.USPE_USEID = employeeRequest.user.Id;
                                    userPermission.USPE_PERID = employeeRequest.user_permissions[i];
                                    userPermission.USPE_CREATEDBY = employeeRequest.employee.EMP_MODIFIEDBY;
                                    userPermission.USPE_CREATEDDATE = DateTime.Now;
                                    userPermission.USPE_STATUS = 1;
                                    _context.User_Permissions.Add(userPermission);
                                    await _context.SaveChangesAsync();
                                }
                            }
                        }
                    }

                    //employee subject
                    if (listEmployeeSubject.Count == employeeRequest.list_subject.Length)
                    {
                        for (int i = 0; i < listEmployeeSubject.Count; i++)
                        {
                            listEmployeeSubject.ElementAt(i).EMSU_SUBID = employeeRequest.list_subject[i];
                            await _context.SaveChangesAsync();
                        }
                    }
                    else
                    {
                        if (listEmployeeSubject.Count > employeeRequest.list_subject.Length)
                        {
                            for (int i = 0; i < listEmployeeSubject.Count; i++)
                            {
                                if (i <= employeeRequest.list_subject.Length - 1)
                                {
                                    listEmployeeSubject.ElementAt(i).EMSU_SUBID = employeeRequest.list_subject[i];
                                    await _context.SaveChangesAsync();
                                }
                                else
                                {
                                    _context.Employee_Subjects.Remove(listEmployeeSubject.ElementAt(i));
                                    await _context.SaveChangesAsync();
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < employeeRequest.list_subject.Length; i++)
                            {
                                if (i <= listEmployeeSubject.Count - 1)
                                {
                                    listEmployeeSubject.ElementAt(i).EMSU_SUBID = employeeRequest.list_subject[i];
                                    await _context.SaveChangesAsync();
                                }
                                else
                                {
                                    Employee_Subject employeeSubject = new Employee_Subject();
                                    employeeSubject.EMSU_EMPID = employeeRequest.employee.Id;
                                    employeeSubject.EMSU_SUBID = employeeRequest.list_subject[i];
                                    employeeSubject.EMSU_CREATEDBY = employeeRequest.employee.EMP_MODIFIEDBY;
                                    employeeSubject.EMSU_CREATEDDATE = DateTime.Now;
                                    _context.Employee_Subjects.Add(employeeSubject);
                                    await _context.SaveChangesAsync();
                                }
                            }
                        }
                    }

                    //log
                    log.LOG_CONTENT = title + "<divider>" + updateLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    //nofication
                    Nofication nofication = new Nofication();
                    nofication.NOF_USEID = employeeRequest.user.Id;
                    nofication.NOF_CREATEDBY = employeeRequest.employee.EMP_MODIFIEDBY;
                    nofication.NOF_CREATEDDATE = DateTime.Now;
                    nofication.NOF_STATUS = 1;
                    nofication.NOF_LINK = "/info_user";
                    nofication.NOF_TITLE = "Thông báo thay đổi thông tin tài khoản";
                    nofication.NOF_CONTENT = "Tài khoản của bạn vừa được chỉnh sửa bởi tài khoản <b>" +
                        _context.Users.FirstOrDefault(x => x.Id == employeeRequest.user.USE_MODIFIEDBY).USE_USERNAME +
                        "</b> - Nhân viên <b>" + _context.Employees.FirstOrDefault(x => x.EMP_USEID == employeeRequest.user.USE_MODIFIEDBY).EMP_LASTNAME +
                        " " + _context.Employees.FirstOrDefault(x => x.EMP_USEID == employeeRequest.user.USE_MODIFIEDBY).EMP_FIRSTNAME +
                        "</b>, vui lòng kiểm tra lại thông tin tài khoản (Icon tài khoản -> Thông tin tài khoản)!";
                    _context.Nofications.Add(nofication);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetEmployee", true);
                    await _hubContext.Clients.All.SendAsync("GetNofication", true);

                    if (employeeRequest.employee.EMP_EMAIL != null)
                    {
                        List<Information> listInformation = await _context.Informations.
                            Where(x => x.INF_ISMULTI == true && x.INF_ISDELETED == false).ToListAsync();

                        MailAddress to = new MailAddress(employeeRequest.employee.EMP_EMAIL);
                        MailAddress from = new MailAddress("hcmunre.fake@gmail.com");

                        MailMessage message = new MailMessage(from, to);
                        message.Subject = "Thông báo chỉnh sửa thông tin tài khoản nhân viên " + employeeRequest.employee.EMP_LASTNAME + " " + employeeRequest.employee.EMP_FIRSTNAME;
                        message.Body = "Kính gửi " + (employeeRequest.employee.EMP_GENDER == true ? "ông" : "bà") + " <span class='text-capitalize'>" +
                            employeeRequest.employee.EMP_LASTNAME + " " + employeeRequest.employee.EMP_FIRSTNAME + "</span>,<br>" +
                            listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + " thông báo: <br> " +
                            "Thông tin tài khoản của bạn đã được chỉnh sửa bởi tài khoản <b>" + _context.Users.FirstOrDefault(x => x.Id == employeeRequest.user.USE_MODIFIEDBY).USE_USERNAME +
                            "</b> - Nhân viên <b>" + _context.Employees.FirstOrDefault(x => x.EMP_USEID == employeeRequest.user.USE_MODIFIEDBY).EMP_LASTNAME +
                            " " + _context.Employees.FirstOrDefault(x => x.EMP_USEID == employeeRequest.user.USE_MODIFIEDBY).EMP_FIRSTNAME +
                            "</b>, vui lòng truy cập địa chỉ " +
                            "<a href='" + employeeRequest.employee.origin + "'>" + employeeRequest.employee.origin + "</a> để kiểm tra thông tin chỉnh sửa<br>" +
                            "<br>Mọi chi tiết thắc mắc vui lòng liên hệ:<br>" + listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + "<br>" +
                            "Số điện thoại: " + listInformation.Where(x => x.INF_KEYNAME == "Số điện thoại").FirstOrDefault().INF_VALUE + "<br>" +
                            "Email: " + listInformation.Where(x => x.INF_KEYNAME == "Email").FirstOrDefault().INF_VALUE + "<br>" +
                            "Địa chỉ: " + listInformation.Where(x => x.INF_KEYNAME == "Địa chỉ").FirstOrDefault().INF_VALUE;
                        message.IsBodyHtml = true;
                        //message.Attachments.Add(new Attachment(_hostingEnv.ContentRootPath + "\\Data\\Users\\default_avatar.png"));

                        SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
                        {
                            Credentials = new NetworkCredential("hcmunre.fake@gmail.com", "tnmt@12345"),
                            EnableSsl = true
                        };

                        try
                        {
                            await client.SendMailAsync(message);
                        }
                        catch (SmtpException ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse
            {
                data = employeeDB
            };
        }
        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long id, long user)
        {
            Employee employeeDB = await _context.Employees.Include(x => x.Users).FirstOrDefaultAsync(x => x.Id == id);

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 9;
            log.LOG_MODULE = "Xóa nhân viên";

            string title = "<b>Họ:</b><title>" +
                "<b>Tên:</b><title>" +
                "<b>Mã nhân viên:</b><title>" +
                "<b>CMND:</b><title>" +
                "<b>Ngày sinh:</b><title>" +
                "<b>Giới tính:</b><title>" +
                "<b>Số điện thoại:</b><title>" +
                "<b>Địa chỉ:</b><title>" +
                "<b>Email:</b><title>" +
                "<b>Thuộc khoa:<b><title>" +
                "<b>Chuyên môn:</b><title>" +
                "<b>Tên tài khoản:</b><title>" +
                "<b>Trạng thái tài khoản:</b><title>" +
                "<b>Vai trò:</b>";

            string deleteLog = employeeDB.EMP_LASTNAME + "<item>"
                + employeeDB.EMP_FIRSTNAME + "<item>"
                + employeeDB.EMP_CODE + "<item>"
                + employeeDB.EMP_IDENTITY + "<item>"
                + employeeDB.EMP_DATEOFBIRTH.GetValueOrDefault().Day + "/" + employeeDB.EMP_DATEOFBIRTH.GetValueOrDefault().Month + "/" + employeeDB.EMP_DATEOFBIRTH.GetValueOrDefault().Year + "<item>"
                + (employeeDB.EMP_GENDER == true ? "Nam" : "Nữ") + "<item>"
                + employeeDB.EMP_PHONE + "<item>"
                + employeeDB.EMP_ADDRESS + "<item>"
                + employeeDB.EMP_EMAIL + "<item>"
                + _context.Faculties.FirstOrDefault(x => x.Id == employeeDB.EMP_FACID).FAC_NAME + "<item>";

            List<Employee_Subject> listEmployeeSubject = await _context.Employee_Subjects.
                Where(x => x.EMSU_EMPID == employeeDB.Id).Include(x => x.Subjects).ToListAsync();
            for (int i = 0; i < listEmployeeSubject.Count; i++)
            {
                deleteLog += listEmployeeSubject.ElementAt(i).Subjects.SUB_NAME;
                if (i != listEmployeeSubject.Count - 1)
                    deleteLog += ", ";
            }
            deleteLog += "<item>" + employeeDB.Users.USE_USERNAME + "<item>"
                + (employeeDB.Users.USE_STATUS == 1 ? "Sử dụng" : "Khóa") + "<item>";
            List<User_Permission> listUserPermission = await _context.User_Permissions.
                Where(x => x.USPE_USEID == employeeDB.Users.Id).Include(x => x.Permissions).ToListAsync();
            for (int i = 0; i < listUserPermission.Count; i++)
            {
                deleteLog += listUserPermission.ElementAt(i).Permissions.PER_NAME;
                if (i != listUserPermission.Count - 1)
                    deleteLog += ", ";
            }

            if (employeeDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            try
            {
                employeeDB.EMP_MODIFIEDBY = user;
                employeeDB.EMP_MODIFIEDDATE = DateTime.Now;
                employeeDB.EMP_ISDELETED = true;
                employeeDB.Users.USE_MODIFIEDBY = user;
                employeeDB.Users.USE_MODIFIEDDATE = DateTime.Now;
                employeeDB.Users.USE_ISDELETED = true;
                employeeDB.Users.USE_STATUS = 2;
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + deleteLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetEmployee", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            if (employeeDB.EMP_EMAIL != null)
            {
                List<Information> listInformation = await _context.Informations.
                    Where(x => x.INF_ISMULTI == true && x.INF_ISDELETED == false).ToListAsync();

                MailAddress to = new MailAddress(employeeDB.EMP_EMAIL);
                MailAddress from = new MailAddress("hcmunre.fake@gmail.com");

                MailMessage message = new MailMessage(from, to);
                message.Subject = "Khóa tài khoản nhân viên " + employeeDB.EMP_LASTNAME + " " + employeeDB.EMP_FIRSTNAME;
                message.Body = "Kính gửi " + (employeeDB.EMP_GENDER == true ? "ông" : "bà") + " <span class='text-capitalize'>" +
                    employeeDB.EMP_LASTNAME + " " + employeeDB.EMP_FIRSTNAME + "</span>,<br>" +
                    listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + " thông báo: <br> " +
                    "Tài khoản của bạn đã bị khóa bởi tài khoản <b>" + _context.Users.FirstOrDefault(x => x.Id == user).USE_USERNAME +
                    "</b> - Nhân viên <b>" + _context.Employees.FirstOrDefault(x => x.EMP_USEID == user).EMP_LASTNAME +
                    " " + _context.Employees.FirstOrDefault(x => x.EMP_USEID == user).EMP_FIRSTNAME +
                    "</b>.<br>" +
                    "<br>Mọi chi tiết thắc mắc vui lòng liên hệ:<br>" + listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + "<br>" +
                    "Số điện thoại: " + listInformation.Where(x => x.INF_KEYNAME == "Số điện thoại").FirstOrDefault().INF_VALUE + "<br>" +
                    "Email: " + listInformation.Where(x => x.INF_KEYNAME == "Email").FirstOrDefault().INF_VALUE + "<br>" +
                    "Địa chỉ: " + listInformation.Where(x => x.INF_KEYNAME == "Địa chỉ").FirstOrDefault().INF_VALUE;
                message.IsBodyHtml = true;
                //message.Attachments.Add(new Attachment(_hostingEnv.ContentRootPath + "\\Data\\Users\\default_avatar.png"));

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
                {
                    Credentials = new NetworkCredential("hcmunre.fake@gmail.com", "tnmt@12345"),
                    EnableSsl = true
                };

                try
                {
                    await client.SendMailAsync(message);
                }
                catch (SmtpException ex) { Console.WriteLine(ex.ToString()); }
            }

            return new BaseResponse
            {
                data = employeeDB
            };
        }
        [HttpPost("getListEmployeeMultiDel")]
        public async Task<ActionResult<BaseResponse>> getListEmployeeMultiDel(int[] arrDel)
        {
            List<Employee> listEmp = new List<Employee>();
            for (int i = 0; i < arrDel.Length; i++)
            {
                Employee emp = await _context.Employees.Where(x => x.Id == arrDel[i]).
                    Include(x => x.Users).
                    Include(x => x.Faculties).
                    Include(x => x.Employee_Subjects).ThenInclude(x => x.Subjects).
                    AsNoTracking().
                    FirstOrDefaultAsync();
                if (emp != null)
                {
                    emp.Users.User_Permissions = await _context.User_Permissions.
                        Where(x => x.USPE_USEID == emp.Users.Id).Include(x => x.Permissions).AsNoTracking().ToListAsync();

                    listEmp.Add(emp);
                }
            }
            return new BaseResponse
            {
                data = listEmp
            };
        }
        [HttpPost("multipleDelete/{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long user, Employee[] arrDel)
        {

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 9;
            log.LOG_MODULE = "Xóa danh sách nhân viên";

            string title = "<b>Họ:</b><title>" +
                "<b>Tên:</b><title>" +
                "<b>Mã nhân viên:</b><title>" +
                "<b>CMND:</b><title>" +
                "<b>Ngày sinh:</b><title>" +
                "<b>Giới tính:</b><title>" +
                "<b>Số điện thoại:</b><title>" +
                "<b>Địa chỉ:</b><title>" +
                "<b>Email:</b><title>" +
                "<b>Thuộc khoa:<b><title>" +
                "<b>Chuyên môn:</b><title>" +
                "<b>Tên tài khoản:</b><title>" +
                "<b>Trạng thái tài khoản:</b><title>" +
                "<b>Vai trò:</b>";
            string multiDeleteLog = "";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i = 0; i < arrDel.Length; i++)
                    {
                        var employeeDB = _context.Employees.
                            Include(x => x.Users).
                            Include(x => x.Faculties).
                            Where(x => x.Id == arrDel[i].Id).FirstOrDefault();
                        if (employeeDB != null)
                        {
                            multiDeleteLog += employeeDB.EMP_LASTNAME + "<item>"
                                + employeeDB.EMP_FIRSTNAME + "<item>"
                                + employeeDB.EMP_CODE + "<item>"
                                + employeeDB.EMP_IDENTITY + "<item>"
                                + employeeDB.EMP_DATEOFBIRTH.GetValueOrDefault().Day + "/" + employeeDB.EMP_DATEOFBIRTH.GetValueOrDefault().Month + "/" + employeeDB.EMP_DATEOFBIRTH.GetValueOrDefault().Year + "<item>"
                                + (employeeDB.EMP_GENDER == true ? "Nam" : "Nữ") + "<item>"
                                + employeeDB.EMP_PHONE + "<item>"
                                + employeeDB.EMP_ADDRESS + "<item>"
                                + employeeDB.EMP_EMAIL + "<item>"
                                + _context.Faculties.FirstOrDefault(x => x.Id == employeeDB.EMP_FACID).FAC_NAME + "<item>";
                            List<Employee_Subject> listEmployeeSubject = await _context.Employee_Subjects.
                                Where(x => x.EMSU_EMPID == employeeDB.Id).Include(x => x.Subjects).ToListAsync();
                            for (int j = 0; j < listEmployeeSubject.Count; j++)
                            {
                                multiDeleteLog += listEmployeeSubject.ElementAt(j).Subjects.SUB_NAME;
                                if (j != listEmployeeSubject.Count - 1)
                                    multiDeleteLog += ", ";
                            }
                            multiDeleteLog += "<item>" + employeeDB.Users.USE_USERNAME + "<item>"
                                + (employeeDB.Users.USE_STATUS == 1 ? "Sử dụng" : "Khóa") + "<item>";
                            List<User_Permission> listUserPermimssion = await _context.User_Permissions.
                                Where(x => x.USPE_USEID == employeeDB.Users.Id).Include(x => x.Permissions).ToListAsync();
                            for (int j = 0; j < listUserPermimssion.Count; j++)
                            {
                                multiDeleteLog += listUserPermimssion.ElementAt(j).Permissions.PER_NAME;
                                if (j != listUserPermimssion.Count - 1)
                                    multiDeleteLog += ", ";
                            }

                            if (i != arrDel.Length - 1)
                                multiDeleteLog += "<delete>";

                            employeeDB.EMP_MODIFIEDBY = user;
                            employeeDB.EMP_MODIFIEDDATE = DateTime.Now;
                            employeeDB.EMP_ISDELETED = true;
                            employeeDB.Users.USE_MODIFIEDBY = user;
                            employeeDB.Users.USE_MODIFIEDDATE = DateTime.Now;
                            employeeDB.Users.USE_ISDELETED = true;
                            employeeDB.Users.USE_STATUS = 2;
                            await _context.SaveChangesAsync();

                            if (employeeDB.EMP_EMAIL != null)
                            {
                                List<Information> listInformation = await _context.Informations.
                                    Where(x => x.INF_ISMULTI == true && x.INF_ISDELETED == false).ToListAsync();

                                MailAddress to = new MailAddress(employeeDB.EMP_EMAIL);
                                MailAddress from = new MailAddress("hcmunre.fake@gmail.com");

                                MailMessage message = new MailMessage(from, to);
                                message.Subject = "Khóa tài khoản nhân viên " + employeeDB.EMP_LASTNAME + " " + employeeDB.EMP_FIRSTNAME;
                                message.Body = "Kính gửi " + (employeeDB.EMP_GENDER == true ? "ông" : "bà") + " <span class='text-capitalize'>" +
                                    employeeDB.EMP_LASTNAME + " " + employeeDB.EMP_FIRSTNAME + "</span>,<br>" +
                                    listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + " thông báo: <br> " +
                                    "Tài khoản của bạn đã bị khóa bởi tài khoản <b>" + _context.Users.FirstOrDefault(x => x.Id == user).USE_USERNAME +
                                    "</b> - Nhân viên <b>" + _context.Employees.FirstOrDefault(x => x.EMP_USEID == user).EMP_LASTNAME +
                                    " " + _context.Employees.FirstOrDefault(x => x.EMP_USEID == user).EMP_FIRSTNAME +
                                    "</b>.<br>" +
                                    "<br>Mọi chi tiết thắc mắc vui lòng liên hệ:<br>" + listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + "<br>" +
                                    "Số điện thoại: " + listInformation.Where(x => x.INF_KEYNAME == "Số điện thoại").FirstOrDefault().INF_VALUE + "<br>" +
                                    "Email: " + listInformation.Where(x => x.INF_KEYNAME == "Email").FirstOrDefault().INF_VALUE + "<br>" +
                                    "Địa chỉ: " + listInformation.Where(x => x.INF_KEYNAME == "Địa chỉ").FirstOrDefault().INF_VALUE;
                                message.IsBodyHtml = true;
                                //message.Attachments.Add(new Attachment(_hostingEnv.ContentRootPath + "\\Data\\Users\\default_avatar.png"));

                                SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
                                {
                                    Credentials = new NetworkCredential("hcmunre.fake@gmail.com", "tnmt@12345"),
                                    EnableSsl = true
                                };

                                try
                                {
                                    await client.SendMailAsync(message);
                                }
                                catch (SmtpException ex) { Console.WriteLine(ex.ToString()); }
                            }
                        }
                    }

                    log.LOG_CONTENT = title + "<divider>" + multiDeleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetEmployee", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse { };
        }
    }
}