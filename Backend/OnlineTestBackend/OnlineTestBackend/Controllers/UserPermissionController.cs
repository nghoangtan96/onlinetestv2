﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserPermissionController : BaseController
    {
        public UserPermissionController(OnlineTestBackendDBContext db) : base(db) { }
        [HttpGet("getListPermissionIdByUserId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListPermissionIdByUserId(long id)
        {
            return new BaseResponse
            {
                data = await _context.User_Permissions.Where(x => x.USPE_USEID == id).AsNoTracking().Select(x => x.USPE_PERID).ToListAsync()
            };
        }
        [HttpGet("getListPermissionByUserId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListPermissionByUserId(long id)
        {
            return new BaseResponse
            {
                data = await _context.User_Permissions.Where(x => x.USPE_USEID == id).
                Include(x => x.Permissions).ThenInclude(x => x.Role_Permissions).ThenInclude(x => x.Roles).ThenInclude(x => x.Actionns).
                Include(x => x.Permissions).ThenInclude(x => x.Role_Permissions).ThenInclude(x => x.Roles).ThenInclude(x => x.Tables).
                AsNoTracking().
                ToListAsync()
            };
        }
    }
}