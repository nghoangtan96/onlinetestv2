﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;
using OnlineTestBackend.Models.Request;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SettingController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public SettingController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Settings.AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var setting = await _context.Settings.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            if (setting == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            return new BaseResponse
            {
                data = setting
            };
        }
        [HttpPost("updatesetting")]
        public async Task<ActionResult<BaseResponse>> UpdateSetting(SettingRequest[] req)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = req[0].userId;
            log.LOG_MODULE = "Cập nhật thiết lập hệ thống";
            log.LOG_CONTENT = "";
            List<Setting> settingList = await _context.Settings.ToListAsync();
            if (settingList.Count == 0)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i = 0; i < settingList.Count; i++)
                    {
                        if (settingList[i].Id == req[i].id)
                        {
                            if (settingList[i].SET_VALUE != null)
                            {
                                if (settingList[i].SET_VALUE != req[i].value)
                                {
                                    log.LOG_CONTENT += settingList[i].SET_NAME + ": "
                                        + settingList[i].SET_VALUE + " > " + req[i].value + "<br>";

                                    settingList[i].SET_VALUE = req[i].value;
                                    settingList[i].SET_MODIFIEDBY = req[i].userId;
                                    settingList[i].SET_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                                else
                                    continue;
                            }
                            else if (settingList[i].SET_ISACTIVE != null)
                            {
                                if (settingList[i].SET_ISACTIVE != req[i].isactive)
                                {
                                    log.LOG_CONTENT += settingList[i].SET_NAME + ": "
                                        + (settingList[i].SET_ISACTIVE == true ? "Kích hoạt" : "Tắt") + " > "
                                        + (req[i].isactive == true ? "Kích hoạt<br>" : "Tắt<br>");

                                    settingList[i].SET_ISACTIVE = req[i].isactive;
                                    settingList[i].SET_MODIFIEDBY = req[i].userId;
                                    settingList[i].SET_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                                else
                                    continue;
                            }
                            else if (settingList[i].SET_CHOICE != null)
                            {
                                if (settingList[i].SET_CHOICEVALUE != req[i].choicevalue)
                                {
                                    log.LOG_CONTENT += settingList[i].SET_NAME + ": "
                                        + settingList[i].SET_CHOICEVALUE + " > "
                                        + req[i].choicevalue + "<br>";

                                    settingList[i].SET_CHOICEVALUE = req[i].choicevalue;
                                    settingList[i].SET_MODIFIEDBY = req[i].userId;
                                    settingList[i].SET_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                                else
                                    continue;
                            }
                            else if (settingList[i].SET_MULTICHOICE != null)
                            {
                                if (settingList[i].SET_MULTICHOICEVALUE != req[i].multichoicevalue)
                                {
                                    log.LOG_CONTENT += settingList[i].SET_NAME + ": ";
                                    for (int j = 0; j < settingList[i].SET_MULTICHOICE.Split("/").Length; j++)
                                    {
                                        if (settingList[i].SET_MULTICHOICEVALUE.Split("/")[j] != req[i].multichoicevalue.Split("/")[j])
                                        {
                                            if (j == 0)
                                            {
                                                log.LOG_CONTENT += "(" + settingList[i].SET_MULTICHOICE.Split("/")[j] + ") " + settingList[i].SET_MULTICHOICEVALUE.Split("/")[j] + " > " + req[i].multichoicevalue.Split("/")[j];
                                            }
                                            else
                                            {
                                                log.LOG_CONTENT += ", (" + settingList[i].SET_MULTICHOICE.Split("/")[j] + ") " + settingList[i].SET_MULTICHOICEVALUE.Split("/")[j] + " > " + req[i].multichoicevalue.Split("/")[j];
                                            }
                                        }
                                    }

                                    settingList[i].SET_MULTICHOICEVALUE = req[i].multichoicevalue;
                                    settingList[i].SET_MODIFIEDBY = req[i].userId;
                                    settingList[i].SET_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                                else
                                    continue;
                            }
                        }
                    }

                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetSetting", true);
                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse
            {
                data = await _context.SettingTypes.Include(x => x.Settings).ToListAsync()
            };
        }
    }
}