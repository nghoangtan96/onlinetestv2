﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class InformationController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public InformationController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            List<Information> listInformation = await _context.Informations.
                Where(x => x.INF_ISDELETED == false && x.INF_ISMULTI == true).
                OrderBy(x => x.INF_ORDER).AsNoTracking().ToListAsync();
            for (int i = 0; i < listInformation.Count; i++)
            {
                if (listInformation[i].INF_KEYNAME == "Logo" && listInformation[i].INF_VALUE != null)
                {
                    listInformation[i].FileUrl = Utils.Helper.GetBaseUrl(Request) + listInformation[i].INF_VALUE;
                }
                listInformation[i].Informations = await _context.Informations.
                    Where(x => x.INF_INFID == listInformation[i].Id && x.INF_ISDELETED == false).ToListAsync();
            }
            return new BaseResponse
            {
                data = listInformation
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(int id)
        {
            var information = await _context.Informations.AsNoTracking().FirstOrDefaultAsync(x=>x.Id == id);
            if (information == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = information
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(Information information)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    Log log = new Log();
                    log.LOG_DATE = DateTime.Now;
                    log.LOG_USEID = information.INF_CREATEDBY.GetValueOrDefault();
                    log.LOG_ACTID = 1;
                    log.LOG_TABID = 10;
                    log.LOG_MODULE = "Thêm thông tin";

                    string title = "<b>" + information.INF_KEYNAME + "</b>";
                    string insertLog = information.INF_VALUE;

                    information.INF_CREATEDDATE = DateTime.Now;
                    information.INF_ISDELETED = false;
                    information.INF_STATUS = 1;

                    _context.Informations.Add(information);
                    await _context.SaveChangesAsync();

                    log.LOG_CONTENT = title + "<divider>" + insertLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetInformation", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse
            {
                data = information
            };
        }
        [HttpPut("{userid}")]
        public async Task<ActionResult<BaseResponse>> Put(long userid, List<Information> listInformation)
        {
            List<Information> listInformationDB = await _context.Informations.
                Where(x => x.INF_ISMULTI == true && x.INF_ISDELETED == false).ToListAsync();

            if(listInformationDB.Count == 0)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            for (int i = 0; i < listInformationDB.Count; i++)
            {
                listInformationDB[i].Informations = await _context.Informations.
                    Where(x => x.INF_INFID == listInformationDB[i].Id && x.INF_ISDELETED == false).ToListAsync();
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = userid;
            log.LOG_ACTID = 2;
            log.LOG_TABID = 10;
            log.LOG_MODULE = "Sửa thông tin trang web";

            string title = "";
            string updateLog = "";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i = 0; i < listInformationDB.Count; i++)
                    {
                        title += "<b>" + listInformationDB[i].INF_KEYNAME + "</b>";
                        updateLog += listInformationDB[i].INF_VALUE + "<to>" + listInformation[i].INF_VALUE;

                        listInformationDB[i].INF_VALUE = listInformation[i].INF_VALUE;
                        listInformationDB[i].INF_MODIFIEDBY = userid;
                        listInformationDB[i].INF_MODIFIEDDATE = DateTime.Now;
                        await _context.SaveChangesAsync();

                        if (!(i == listInformationDB.Count - 1 && listInformationDB[i].Informations == null))
                        {
                            title += "<title>";
                            updateLog += "<item>";
                        }

                        if (listInformationDB[i].Informations != null)
                        {
                            for (int j = 0; j < listInformationDB[i].Informations.Count; j++)
                            {
                                title += "<b>" + listInformationDB[i].Informations.ElementAt(j).INF_KEYNAME + " (" + (j + 1) + ")</b>";
                                updateLog += listInformationDB[i].Informations.ElementAt(j).INF_VALUE + "<to>" + listInformation[i].Informations.ElementAt(j).INF_VALUE;

                                listInformationDB[i].Informations.ElementAt(j).INF_VALUE = listInformation[i].Informations.ElementAt(j).INF_VALUE;
                                listInformationDB[i].Informations.ElementAt(j).INF_MODIFIEDBY = userid;
                                listInformationDB[i].Informations.ElementAt(j).INF_MODIFIEDDATE = DateTime.Now;
                                await _context.SaveChangesAsync();

                                if (!(i == listInformationDB.Count - 1 && j == listInformationDB[i].Informations.Count - 1))
                                {
                                    title += "<title>";
                                    updateLog += "<item>";
                                }
                            }
                        }
                    }

                    log.LOG_CONTENT = title + "<divider>" + updateLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetInformation", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse
            {
                data = listInformationDB
            };
        }
        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(int id, long user)
        {
            var informationDB = await _context.Informations.FindAsync(id);

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 10;
            log.LOG_MODULE = "Xóa thông tin trang web";

            string title = "<b>" + informationDB.INF_KEYNAME + "</b>";
            string deleteLog = informationDB.INF_VALUE;

            if (informationDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            try
            {
                informationDB.INF_MODIFIEDBY = user;
                informationDB.INF_MODIFIEDDATE = DateTime.Now;
                informationDB.INF_ISDELETED = true;
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + deleteLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetInformation", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = informationDB
            };
        }
    }
}