﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        public readonly OnlineTestBackendDBContext _context;
        public BaseController(OnlineTestBackendDBContext context)
        {
            _context = context;
        }
    }
}