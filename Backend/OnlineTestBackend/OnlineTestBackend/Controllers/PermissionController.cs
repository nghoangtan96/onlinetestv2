﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;
using OnlineTestBackend.Models.Request;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PermissionController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public PermissionController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Permissions.AsNoTracking().Where(x => x.PER_ISDELETED == false).ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(int id)
        {
            var permission = await _context.Permissions.
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.UserModified).ThenInclude(x => x.Employees).
                Include(x => x.Role_Permissions).ThenInclude(x => x.Roles).ThenInclude(x => x.Actionns).
                Include(x => x.Role_Permissions).ThenInclude(x => x.Roles).ThenInclude(x => x.Tables).
                AsNoTracking().
                Where(x => x.PER_ISDELETED == false && x.Id == id).FirstOrDefaultAsync();
            if (permission == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (permission.UserCreated != null)
            {
                if (permission.UserCreated.Employees.ElementAt(0).EMP_AVATAR != null)
                {
                    permission.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + permission.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
                else
                {
                    permission.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
            }
            if (permission.UserModified != null && permission.PER_CREATEDBY != permission.PER_MODIFIEDBY)
            {
                if (permission.UserModified.Employees.ElementAt(0).EMP_AVATAR != null)
                {
                    permission.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + permission.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
                else
                {
                    permission.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
            }

            return new BaseResponse
            {
                data = permission
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(PermissionRequest permissionRequest)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = permissionRequest.permission.PER_CREATEDBY.GetValueOrDefault();
            log.LOG_MODULE = "Thêm vai trò";
            log.LOG_ACTID = 1;
            log.LOG_TABID = 8;
            string title = "<b>Tên vai trò:</b><title>" +
               "<b>Trạng thái:</b><title>" +
               "<b>Danh sách quyền:</b>";

            string insertLog = permissionRequest.permission.PER_NAME + "<item>"
                + (permissionRequest.permission.PER_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //permission add
                    permissionRequest.permission.PER_CREATEDDATE = DateTime.Now;
                    permissionRequest.permission.PER_ISDELETED = false;

                    _context.Permissions.Add(permissionRequest.permission);
                    await _context.SaveChangesAsync();

                    //role add
                    for (int i = 0; i < permissionRequest.listRolePermission.Length; i++)
                    {
                        if (permissionRequest.listRolePermission[i].ROPE_ISACTIVE == true)
                        {
                            var role = await _context.Roles.
                            Include(x => x.Actionns).
                            Include(x => x.Tables).
                            FirstOrDefaultAsync(x => x.Id == permissionRequest.listRolePermission[i].ROPE_ROLID);
                            insertLog += role.Actionns.ACT_NAME + " " + role.Tables.TAB_NAME;
                            if (i != permissionRequest.listRolePermission.Length - 1)
                                insertLog += "<opt>";
                        }

                        permissionRequest.listRolePermission[i].ROPE_CREATEDBY = permissionRequest.permission.PER_CREATEDBY;
                        permissionRequest.listRolePermission[i].ROPE_CREATEDDATE = DateTime.Now;
                        permissionRequest.listRolePermission[i].ROPE_PERID = permissionRequest.permission.Id;
                        permissionRequest.listRolePermission[i].ROPE_STATUS = 1;
                        _context.Role_Permissions.Add(permissionRequest.listRolePermission[i]);
                        await _context.SaveChangesAsync();
                    }

                    log.LOG_CONTENT = title + "<divider>" + insertLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetPermission", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse
            {
                data = permissionRequest.permission
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(int id, PermissionRequest permissionRequest)
        {
            Permission permissionDB = await _context.Permissions.Include(x => x.Role_Permissions).FirstOrDefaultAsync(x => x.Id == id);
            List<User_Permission> listUserPermission = await _context.User_Permissions.Where(x => x.USPE_PERID == id).ToListAsync();

            if (permissionDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = permissionRequest.permission.PER_MODIFIEDBY.GetValueOrDefault();
            log.LOG_ACTID = 2;
            log.LOG_TABID = 8;
            log.LOG_MODULE = "Sửa vai trò";

            string title = "<b>Tên vai trò:</b><title>" +
               "<b>Trạng thái:</b><title>" +
               "<b>Danh sách quyền:</b>";

            string updateLog = permissionDB.PER_NAME + "<to>" + permissionRequest.permission.PER_NAME + "<item>"
                + (permissionDB.PER_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<to>" + (permissionRequest.permission.PER_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>";

            for (int i = 0; i < permissionDB.Role_Permissions.Count; i++)
            {
                if (permissionDB.Role_Permissions.ElementAt(i).ROPE_ISACTIVE == true)
                {
                    var role = await _context.Roles.
                    Include(x => x.Actionns).Include(x => x.Tables).
                    FirstOrDefaultAsync(x => x.Id == permissionDB.Role_Permissions.ElementAt(i).ROPE_ROLID);
                    updateLog += role.Actionns.ACT_NAME + " " + role.Tables.TAB_NAME;
                    if (i != permissionDB.Role_Permissions.Count - 1)
                        updateLog += ", ";
                }
            }
            updateLog += "<to>";
            for (int i = 0; i < permissionRequest.listRolePermission.Length; i++)
            {
                if (permissionRequest.listRolePermission[i].ROPE_ISACTIVE == true)
                {
                    var role = await _context.Roles.
                    Include(x => x.Actionns).Include(x => x.Tables).
                    FirstOrDefaultAsync(x => x.Id == permissionRequest.listRolePermission[i].ROPE_ROLID);
                    updateLog += role.Actionns.ACT_NAME + " " + role.Tables.TAB_NAME;
                    if (i != permissionRequest.listRolePermission.Length - 1)
                        updateLog += ", ";
                }
            }

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //update permission
                    permissionDB.PER_NAME = permissionRequest.permission.PER_NAME;
                    permissionDB.PER_STATUS = permissionRequest.permission.PER_STATUS;
                    permissionDB.PER_MODIFIEDBY = permissionRequest.permission.PER_MODIFIEDBY;
                    permissionDB.PER_MODIFIEDDATE = DateTime.Now;
                    await _context.SaveChangesAsync();

                    //update role
                    for (int i = 0; i < permissionDB.Role_Permissions.Count; i++)
                    {
                        permissionDB.Role_Permissions.ElementAt(i).ROPE_ISACTIVE = permissionRequest.listRolePermission[i].ROPE_ISACTIVE;
                        permissionDB.Role_Permissions.ElementAt(i).ROPE_MODIFIEDBY = permissionRequest.permission.PER_MODIFIEDBY;
                        permissionDB.Role_Permissions.ElementAt(i).ROPE_MODIFIEDDATE = DateTime.Now;
                        await _context.SaveChangesAsync();
                    }

                    log.LOG_CONTENT = title + "<divider>" + updateLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetPermission", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse
            {
                data = permissionDB
            };
        }
        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(int id, long user)
        {
            Permission permissionDB = await _context.Permissions.
                Include(x => x.Role_Permissions).
                Include(x => x.User_Permissions).
                FirstOrDefaultAsync(x => x.Id == id);

            if (permissionDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (permissionDB.User_Permissions.Count != 0)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Không thể xóa, vai trò đang được sử dụng!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_MODULE = "Xóa vai trò";
            log.LOG_ACTID = 3;
            log.LOG_TABID = 8;

            string title = "<b>Tên vai trò:</b><title>" +
               "<b>Trạng thái:</b><title>" +
               "<b>Danh sách quyền:</b>";

            string deleteLog = permissionDB.PER_NAME + "<item>"
                + (permissionDB.PER_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>";
            for (int i = 0; i < permissionDB.Role_Permissions.Count; i++)
            {
                if (permissionDB.Role_Permissions.ElementAt(i).ROPE_ISACTIVE == true)
                {
                    var role = await _context.Roles.
                    Include(x => x.Actionns).Include(x => x.Tables).
                    FirstOrDefaultAsync(x => x.Id == permissionDB.Role_Permissions.ElementAt(i).ROPE_ROLID);
                    deleteLog += role.Actionns.ACT_NAME + " " + role.Tables.TAB_NAME;
                    if (i != permissionDB.Role_Permissions.Count)
                        deleteLog += "<opt>";
                }
            }

            try
            {
                permissionDB.PER_ISDELETED = true;
                permissionDB.PER_MODIFIEDBY = user;
                permissionDB.PER_MODIFIEDDATE = DateTime.Now;

                log.LOG_CONTENT = title + "<divider>" + deleteLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetPermission", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = permissionDB
            };
        }
        [HttpPost("getListPermissionMultiDel")]
        public async Task<ActionResult<BaseResponse>> getListPermissionMultiDel(int[] arrDel)
        {
            List<Permission> listPer = new List<Permission>();
            for (int i = 0; i < arrDel.Length; i++)
            {
                Permission per = await _context.Permissions.Where(x => x.Id == arrDel[i]).
                    Include(x => x.Role_Permissions).ThenInclude(x => x.Roles).ThenInclude(x => x.Actionns).
                    Include(x => x.Role_Permissions).ThenInclude(x => x.Roles).ThenInclude(x => x.Tables).
                    AsNoTracking().
                    FirstOrDefaultAsync();
                if (per != null)
                {
                    listPer.Add(per);
                }
            }
            return new BaseResponse
            {
                data = listPer
            };
        }
        [HttpPost("multipleDelete/{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long user, Permission[] arrDel)
        {

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 8;
            log.LOG_MODULE = "Xóa danh sách vai trò";

            string title = "<b>Tên vai trò:</b><title>" +
               "<b>Trạng thái:</b><title>" +
               "<b>Danh sách quyền:</b>";
            string multiDeleteLog = "";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i = 0; i < arrDel.Length; i++)
                    {
                        var permissionDB = _context.Permissions.
                            Include(x => x.Role_Permissions).
                            Include(x => x.User_Permissions).
                            Where(x => x.Id == arrDel[i].Id).FirstOrDefault();
                        if (permissionDB.User_Permissions.Count != 0)
                        {
                            return new BaseResponse
                            {
                                errorCode = 405,
                                message = "Xóa danh sách vai trò thất bại. Vui lòng xóa tất cả tài khoản sử dụng vai trò " + permissionDB.PER_NAME + " trước!"
                            };
                        }
                        if (permissionDB != null)
                        {
                            multiDeleteLog += permissionDB.PER_NAME + "<item>"
                                + (permissionDB.PER_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>";
                            for (int j = 0; j < permissionDB.Role_Permissions.Count; j++)
                            {
                                if (permissionDB.Role_Permissions.ElementAt(j).ROPE_ISACTIVE == true)
                                {
                                    var role = await _context.Roles.
                                    Include(x => x.Actionns).Include(x => x.Tables).
                                    FirstOrDefaultAsync(x => x.Id == permissionDB.Role_Permissions.ElementAt(j).ROPE_ROLID);
                                    multiDeleteLog += role.Actionns.ACT_NAME + " " + role.Tables.TAB_NAME;
                                    if (j != permissionDB.Role_Permissions.Count - 1)
                                        multiDeleteLog += "<opt>";
                                }
                            }

                            if (i != arrDel.Length - 1)
                                multiDeleteLog += "<delete>";

                            permissionDB.PER_MODIFIEDBY = user;
                            permissionDB.PER_MODIFIEDDATE = DateTime.Now;
                            permissionDB.PER_ISDELETED = true;
                            await _context.SaveChangesAsync();
                        }
                    }

                    log.LOG_CONTENT = title + "<divider>" + multiDeleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetPermission", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse { };
        }
    }
}