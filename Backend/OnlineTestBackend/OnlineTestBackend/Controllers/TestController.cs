﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;
using OnlineTestBackend.Models.Request;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public TestController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Tests.
                Include(x => x.Test_Types).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.Subjects).Include(x => x.TestingFormats).
                Where(x => x.TES_ISDELETED == false).AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("getListById/{id}")]
        public async Task<ActionResult<BaseResponse>> getListById(long id)
        {
            var test = await _context.Tests.
                Include(x => x.Test_Types).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.Subjects).Include(x => x.TestingFormats).
                Where(x => x.TES_ISDELETED == false && x.Id == id).AsNoTracking().ToListAsync();
            if (test == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = test
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var test = await _context.Tests.
                Include(x => x.TestingFormats).
                Include(x => x.Test_Details).Include(x => x.Test_Questions).
                Include(x => x.Test_Types).Include(x => x.Subjects).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.UserModified).ThenInclude(x => x.Employees).
                Where(x => x.TES_ISDELETED == false && x.Id == id).
                AsNoTracking().
                FirstOrDefaultAsync();
            if (test == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (test.Test_Questions.Count != 0)
            {
                int count = 0;
                List<Test_Question> listTestQuestion = await _context.Test_Questions
                    .Include(x => x.Questions).ThenInclude(x => x.Options)
                    .Include(x => x.Questions).ThenInclude(x => x.Parts)
                    .Include(x => x.Passages).ThenInclude(x => x.Questions).ThenInclude(x => x.Options)
                    .Include(x => x.Passages).ThenInclude(x => x.Parts)
                    .Where(x => x.TEQU_TESID == test.Id).AsNoTracking().ToListAsync();
                for (int i = 0; i < listTestQuestion.Count; i++)
                {
                    if (listTestQuestion.ElementAt(i).TEQU_QUEID != null)
                        count++;
                    else
                    {
                        count += listTestQuestion.ElementAt(i).Passages.Questions.Count;
                    }
                }
                test.countQuestion = count;
                test.Test_Questions = listTestQuestion;
            }

            for (int i = 0; i < test.Test_Questions.Count; i++)
            {
                if (test.Test_Questions.ElementAt(i).TEQU_PASID != null)
                {
                    if (test.Test_Questions.ElementAt(i).Passages.PAS_MEDIA != null)
                    {
                        test.Test_Questions.ElementAt(i).Passages.FileUrl = Utils.Helper.GetBaseUrl(Request) + test.Test_Questions.ElementAt(i).Passages.PAS_MEDIA;
                    }
                }
                else
                {
                    if (test.Test_Questions.ElementAt(i).Questions.QUE_MEDIA != null)
                    {
                        test.Test_Questions.ElementAt(i).Questions.FileUrl = Utils.Helper.GetBaseUrl(Request) + test.Test_Questions.ElementAt(i).Questions.QUE_MEDIA;
                    }
                }
            }

            if (test.UserCreated != null)
            {
                if (test.UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    test.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    test.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + test.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
            }
            if (test.UserModified != null)
            {
                if (test.UserModified.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    test.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    test.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + test.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
            }
            if (test.UserAccepted != null)
            {
                if (test.UserAccepted.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    test.UserAccepted.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    test.UserAccepted.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + test.UserAccepted.Employees.ElementAt(0).EMP_AVATAR;
                }
            }

            return new BaseResponse
            {
                data = test
            };
        }
        [HttpGet("getTestByTesttypeIdAndSubjectId/{testtypeid}&{subjectid}")]
        public async Task<ActionResult<BaseResponse>> getTestByTesttypeIdAndSubjectId(int testtypeid, long subjectid)
        {
            var test = await _context.Tests.
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Where(x => x.TES_ISDELETED == false && x.TES_TETYID == testtypeid && x.TES_SUBID == subjectid && x.TES_STATUS == 2).AsNoTracking().ToListAsync();
            if (test == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = test
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(TestRequest testreq)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //test
                    testreq.test.TES_ISSHUFFLE = (testreq.shuffle == 1 ? false : true);
                    testreq.test.TES_CREATEDDATE = DateTime.Now;
                    testreq.test.TES_ISDELETED = false;
                    testreq.test.TES_ENCPASSWORD = Utils.Helper.GenHash(testreq.test.TES_PASSWORD);
                    _context.Tests.Add(testreq.test);
                    await _context.SaveChangesAsync();

                    //test question
                    for (int i = 0; i < testreq.testQuestion.Length; i++)
                    {
                        Test_Question testQuestion = new Test_Question();
                        //testreq.testQuestion[i].Id = 1;
                        //testreq.testQuestion[i].TEQU_TESID = testreq.test.Id;
                        //testreq.testQuestion[i].TEQU_CREATEDBY = testreq.test.TES_CREATEDBY;
                        //testreq.testQuestion[i].TEQU_CREATEDDATE = DateTime.Now;
                        testQuestion.TEQU_CREATEDBY = testreq.test.TES_CREATEDBY;
                        testQuestion.TEQU_CREATEDDATE = DateTime.Now;
                        testQuestion.TEQU_TESID = testreq.test.Id;
                        testQuestion.TEQU_QUEID = testreq.testQuestion[i].TEQU_QUEID;
                        testQuestion.TEQU_PASID = testreq.testQuestion[i].TEQU_PASID;

                        _context.Test_Questions.Add(testQuestion);
                        await _context.SaveChangesAsync();
                    }

                    //testformat
                    TestingFormat testFormat = new TestingFormat();
                    testFormat.TESF_TESID = testreq.test.Id;
                    testFormat.TESF_ISSHOWPART = testreq.testFormat == 1 ? false : true;
                    testFormat.TESF_ISSHOWSCORE = testreq.showScore;
                    _context.TestingFormats.Add(testFormat);
                    await _context.SaveChangesAsync();

                    //shuffle
                    if (testreq.shuffle == 2)
                    {
                        for (int i = 0; i < testreq.numberOfTest; i++)
                        {
                            for (int j = 0; j < testreq.testShuffle[i].listQuestion.Length; j++)
                            {
                                Test_Detail testDetail = new Test_Detail();
                                testDetail.TEDE_TESID = testreq.test.Id;
                                testDetail.TEDE_CREATEDBY = testreq.test.TES_CREATEDBY;
                                testDetail.TEDE_CREATEDDATE = DateTime.Now;
                                testDetail.TEDE_ISDELETED = false;
                                testDetail.TEDE_CODE = testreq.testShuffle[i].code;

                                if (testreq.testShuffle[i].listTypeQuestion[j] == 1)
                                {
                                    testDetail.TEDE_QUEID = testreq.testShuffle[i].listQuestion[j];
                                    testDetail.TEDE_QUEOPTION = testreq.testShuffle[i].listOption[j];
                                    _context.TestDetails.Add(testDetail);
                                    await _context.SaveChangesAsync();
                                }
                                else
                                {
                                    testDetail.TEDE_PASID = testreq.testShuffle[i].listQuestion[j];
                                    string question = "";
                                    string option = "";
                                    for (int k = 0; k < testreq.testShuffle[i].listQuestionPassage[j].listQuestionOfPassage.Length; k++)
                                    {
                                        question += testreq.testShuffle[i].listQuestionPassage[j].listQuestionOfPassage[k];
                                        option += testreq.testShuffle[i].listQuestionPassage[j].listOptionOfPassage[k];
                                        if (k != testreq.testShuffle[i].listQuestionPassage[j].listQuestionOfPassage.Length - 1)
                                        {
                                            question += ".";
                                            option += ".";
                                        }
                                    }
                                    testDetail.TEDE_PASQUESTION = question;
                                    testDetail.TEDE_PASOPTION = option;
                                    _context.TestDetails.Add(testDetail);
                                    await _context.SaveChangesAsync();
                                }
                            }
                        }
                    }

                    //nofication
                    Nofication selfNof = new Nofication();
                    selfNof.NOF_USEID = testreq.test.TES_CREATEDBY;
                    selfNof.NOF_CREATEDBY = testreq.test.TES_CREATEDBY;
                    selfNof.NOF_CREATEDDATE = DateTime.Now;
                    selfNof.NOF_PARAM = testreq.test.Id;
                    selfNof.NOF_STATUS = 1;
                    selfNof.NOF_CONTENT = "Bạn đã tạo thành công đề thi môn " + _context.Subjects.FirstOrDefault(x => x.Id == testreq.test.TES_SUBID).SUB_NAME
                        + " (Mật khẩu: " + testreq.test.TES_PASSWORD + "). Đề thi đã được gửi đến quản lý khoa để xét duyệt!";
                    selfNof.NOF_LINK = "/test";
                    selfNof.NOF_TITLE = "Thông báo chờ xét duyệt đề thi!";
                    _context.Nofications.Add(selfNof);
                    await _context.SaveChangesAsync();

                    List<User_Permission> listUserPermission = await _context.User_Permissions.
                        Include(x => x.Permissions).Where(x => x.USPE_PERID == 9).ToListAsync();
                    Subject subject = await _context.Subjects.FirstOrDefaultAsync(x => x.Id == testreq.test.TES_SUBID);
                    List<Employee> listEmployee = await _context.Employees.
                        Include(x => x.Users).Where(x => x.EMP_FACID == subject.SUB_FACID).ToListAsync();
                    for (int i = 0; i < listEmployee.Count; i++)
                    {
                        if (listUserPermission.FirstOrDefault(x => x.USPE_USEID == listEmployee.ElementAt(i).Users.Id) != null)
                        {
                            Nofication nof = new Nofication();
                            nof.NOF_USEID = listEmployee.ElementAt(i).Users.Id;
                            nof.NOF_CREATEDBY = testreq.test.TES_CREATEDBY;
                            nof.NOF_CREATEDDATE = DateTime.Now;
                            nof.NOF_PARAM = testreq.test.Id;
                            nof.NOF_STATUS = 1;
                            nof.NOF_CONTENT = listUserPermission.FirstOrDefault(x => x.USPE_USEID == listEmployee.ElementAt(i).Users.Id).Permissions.PER_NAME
                                + " " + listEmployee.ElementAt(i).EMP_LASTNAME + " " + listEmployee.ElementAt(i).EMP_FIRSTNAME
                                + " đã gửi yêu cầu xét duyệt đề thi môn " + _context.Subjects.FirstOrDefault(x => x.Id == testreq.test.TES_SUBID).SUB_NAME
                                + " (Mật khẩu: " + testreq.test.TES_PASSWORD + ")!";
                            nof.NOF_LINK = "/test";
                            nof.NOF_TITLE = "Xét duyệt đề thi";
                            _context.Nofications.Add(nof);
                            await _context.SaveChangesAsync();
                        }
                    }

                    //log
                    Log log = new Log();
                    log.LOG_DATE = DateTime.Now;
                    log.LOG_USEID = testreq.test.TES_CREATEDBY.GetValueOrDefault();
                    log.LOG_MODULE = "Thêm đề thi";
                    log.LOG_ACTID = 1;
                    log.LOG_TABID = 1016;
                    string title = "<b>Loại bài thi:</b><title>" +
                        "<b>Môn học:</b><title>" +
                        "<b>Điểm tối đa:</b><title>" +
                        "<b>Định dạng bài thi:</b><title>" +
                        "<b>Số lượng câu hỏi:</b><title>" +
                        "<b>Thời gian làm bài:</b><title>" +
                        "<b>Trộn câu hỏi, đáp án:</b><title>" +
                        "<b>Số lượng mã đề:</b><title>" +
                        "<b>Trạng thái:</b><title>";

                    string insertLog = _context.Test_Types.FirstOrDefault(x => x.Id == testreq.test.TES_TETYID).TETY_NAME + "<item>"
                        + _context.Subjects.FirstOrDefault(x => x.Id == testreq.test.TES_SUBID).SUB_NAME + "<item>"
                        + testreq.test.TES_MAXSCORE + "<item>"
                        + (testreq.testFormat == 1 ? "Thường" : "Toeic") + "<item>"
                        + testreq.numberOfQuestion + "<item>"
                        + testreq.test.TES_TIME + " phút" + "<item>"
                        + (testreq.shuffle == 1 ? "Không trộn" : "Trộn theo mã đề") + "<item>"
                        + (testreq.shuffle == 1 ? "" : testreq.numberOfTest.ToString()) + "<item>"
                        + (testreq.test.TES_STATUS == 1 ? "Đang chờ duyệt" : (testreq.test.TES_STATUS == 2 ? "Đã duyệt" : (testreq.test.TES_STATUS == 3 ? "Đang thi" : "Đã kết thúc")));
                    log.LOG_CONTENT = title + "<divider>" + insertLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetNofication", true);
                    await _hubContext.Clients.All.SendAsync("GetTest", true);
                }
                catch (Exception ex)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                        //message = ex.Message
                    };
                }
            }

            return new BaseResponse
            {
                data = testreq.test
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(long id, TestRequest req)
        {
            Test testDB = await _context.Tests.
                Include(x => x.TestingFormats).Include(x => x.Test_Details).
                Include(x => x.Test_Questions).ThenInclude(x => x.Passages).ThenInclude(x => x.Questions).
                Include(x => x.Schedule_Tests).
                FirstOrDefaultAsync(x => x.TES_ISDELETED == false && x.Id == id);

            if (testDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            if (testDB.TES_STATUS == 3 || testDB.TES_STATUS == 4 || testDB.Schedule_Tests.Count != 0)
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Đề thi đang hoặc đã được sử dụng, không thể chỉnh sửa!"
                };

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    testDB.TES_ISSHUFFLE = req.shuffle == 1 ? false : true;
                    //testDB.TES_CODE = req.test.TES_CODE;
                    testDB.TES_MAXSCORE = req.test.TES_MAXSCORE;
                    testDB.TES_SUBID = req.test.TES_SUBID;
                    testDB.TES_TETYID = req.test.TES_TETYID;
                    testDB.TES_TIME = req.test.TES_TIME;
                    testDB.TES_MODIFIEDBY = req.test.TES_MODIFIEDBY;
                    testDB.TES_MODIFIEDDATE = DateTime.Now;
                    testDB.TES_PASSWORD = req.test.TES_PASSWORD;
                    testDB.TES_STATUS = 1;
                    if (testDB.TES_PASSWORD != req.test.TES_PASSWORD)
                        testDB.TES_ENCPASSWORD = Utils.Helper.GenHash(req.test.TES_PASSWORD);
                    await _context.SaveChangesAsync();

                    //test Format
                    testDB.TestingFormats.ElementAt(0).TESF_ISSHOWPART = req.testFormat == 1 ? false : true;
                    testDB.TestingFormats.ElementAt(0).TESF_ISSHOWSCORE = req.showScore;
                    await _context.SaveChangesAsync();

                    //testQuestion
                    //check
                    bool isChange_testQuestion = false;
                    if (testDB.Test_Questions.Count != req.testQuestion.Length)
                    {
                        isChange_testQuestion = true;
                    }
                    else
                    {
                        for (int i = 0; i < req.testQuestion.Length; i++)
                        {
                            if (req.testQuestion[i].TEQU_QUEID != null)
                            {
                                if (testDB.Test_Questions.FirstOrDefault(x => x.TEQU_QUEID == req.testQuestion[i].TEQU_QUEID) == null)
                                {
                                    isChange_testQuestion = true;
                                    break;
                                }
                            }
                            else
                            {
                                if (testDB.Test_Questions.FirstOrDefault(x => x.TEQU_PASID == req.testQuestion[i].TEQU_PASID) == null)
                                {
                                    isChange_testQuestion = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (isChange_testQuestion == true)
                    {
                        List<Test_Question> listTestQuestion = await _context.Test_Questions.Where(x => x.TEQU_TESID == testDB.Id).ToListAsync();
                        _context.RemoveRange(listTestQuestion);
                        await _context.SaveChangesAsync();

                        //add new testquestion
                        for (int i = 0; i < req.testQuestion.Length; i++)
                        {
                            Test_Question testQuestion = new Test_Question();
                            //testreq.testQuestion[i].Id = 1;
                            //testreq.testQuestion[i].TEQU_TESID = testreq.test.Id;
                            //testreq.testQuestion[i].TEQU_CREATEDBY = testreq.test.TES_CREATEDBY;
                            //testreq.testQuestion[i].TEQU_CREATEDDATE = DateTime.Now;
                            testQuestion.TEQU_CREATEDBY = req.test.TES_CREATEDBY;
                            testQuestion.TEQU_CREATEDDATE = DateTime.Now;
                            testQuestion.TEQU_TESID = req.test.Id;
                            testQuestion.TEQU_QUEID = req.testQuestion[i].TEQU_QUEID;
                            testQuestion.TEQU_PASID = req.testQuestion[i].TEQU_PASID;

                            _context.Test_Questions.Add(testQuestion);
                            await _context.SaveChangesAsync();
                        }
                    }

                    //testDetail (shuffle)
                    //check
                    if (testDB.Test_Details.Count == 0)
                    {
                        if (req.shuffle == 2)
                        {
                            for (int i = 0; i < req.numberOfTest; i++)
                            {
                                for (int j = 0; j < req.testShuffle[i].listQuestion.Length; j++)
                                {
                                    Test_Detail testDetail = new Test_Detail();
                                    testDetail.TEDE_TESID = req.test.Id;
                                    testDetail.TEDE_CREATEDBY = req.test.TES_CREATEDBY;
                                    testDetail.TEDE_CREATEDDATE = DateTime.Now;
                                    testDetail.TEDE_ISDELETED = false;
                                    testDetail.TEDE_CODE = req.testShuffle[i].code;

                                    if (req.testShuffle[i].listTypeQuestion[j] == 1)
                                    {
                                        testDetail.TEDE_QUEID = req.testShuffle[i].listQuestion[j];
                                        testDetail.TEDE_QUEOPTION = req.testShuffle[i].listOption[j];
                                        _context.TestDetails.Add(testDetail);
                                        await _context.SaveChangesAsync();
                                    }
                                    else
                                    {
                                        testDetail.TEDE_PASID = req.testShuffle[i].listQuestion[j];
                                        string question = "";
                                        string option = "";
                                        for (int k = 0; k < req.testShuffle[i].listQuestionPassage[j].listQuestionOfPassage.Length; k++)
                                        {
                                            question += req.testShuffle[i].listQuestionPassage[j].listQuestionOfPassage[k];
                                            option += req.testShuffle[i].listQuestionPassage[j].listOptionOfPassage[k];
                                            if (k != req.testShuffle[i].listQuestionPassage[j].listQuestionOfPassage.Length - 1)
                                            {
                                                question += ".";
                                                option += ".";
                                            }
                                        }
                                        testDetail.TEDE_PASQUESTION = question;
                                        testDetail.TEDE_PASOPTION = option;
                                        _context.TestDetails.Add(testDetail);
                                        await _context.SaveChangesAsync();
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (req.shuffle == 1)
                        {
                            List<Test_Detail> listTestDetail = await _context.TestDetails.Where(x => x.TEDE_TESID == testDB.Id).ToListAsync();
                            _context.RemoveRange(listTestDetail);
                            await _context.SaveChangesAsync();
                        }
                        else
                        {
                            bool isChange_testDetail = false;
                            for (int i = 0; i < req.testShuffle.Length; i++)
                            {
                                if (testDB.Test_Details.FirstOrDefault(x => x.TEDE_CODE == req.testShuffle[i].code) == null)
                                {
                                    isChange_testDetail = true;
                                    break;
                                }
                            }
                            if (isChange_testDetail == true)
                            {
                                List<Test_Detail> listTestDetail = await _context.TestDetails.Where(x => x.TEDE_TESID == testDB.Id).ToListAsync();
                                _context.RemoveRange(listTestDetail);
                                await _context.SaveChangesAsync();

                                //add new testDetail
                                for (int i = 0; i < req.numberOfTest; i++)
                                {
                                    for (int j = 0; j < req.testShuffle[i].listQuestion.Length; j++)
                                    {
                                        Test_Detail testDetail = new Test_Detail();
                                        testDetail.TEDE_TESID = req.test.Id;
                                        testDetail.TEDE_CREATEDBY = req.test.TES_CREATEDBY;
                                        testDetail.TEDE_CREATEDDATE = DateTime.Now;
                                        testDetail.TEDE_ISDELETED = false;
                                        testDetail.TEDE_CODE = req.testShuffle[i].code;

                                        if (req.testShuffle[i].listTypeQuestion[j] == 1)
                                        {
                                            testDetail.TEDE_QUEID = req.testShuffle[i].listQuestion[j];
                                            testDetail.TEDE_QUEOPTION = req.testShuffle[i].listOption[j];
                                            _context.TestDetails.Add(testDetail);
                                            await _context.SaveChangesAsync();
                                        }
                                        else
                                        {
                                            testDetail.TEDE_PASID = req.testShuffle[i].listQuestion[j];
                                            string question = "";
                                            string option = "";
                                            for (int k = 0; k < req.testShuffle[i].listQuestionPassage[j].listQuestionOfPassage.Length; k++)
                                            {
                                                question += req.testShuffle[i].listQuestionPassage[j].listQuestionOfPassage[k];
                                                option += req.testShuffle[i].listQuestionPassage[j].listOptionOfPassage[k];
                                                if (k != req.testShuffle[i].listQuestionPassage[j].listQuestionOfPassage.Length - 1)
                                                {
                                                    question += ".";
                                                    option += ".";
                                                }
                                            }
                                            testDetail.TEDE_PASQUESTION = question;
                                            testDetail.TEDE_PASOPTION = option;
                                            _context.TestDetails.Add(testDetail);
                                            await _context.SaveChangesAsync();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //nofication
                    Nofication selfNof = new Nofication();
                    selfNof.NOF_USEID = req.test.TES_MODIFIEDBY;
                    selfNof.NOF_CREATEDBY = req.test.TES_MODIFIEDBY;
                    selfNof.NOF_CREATEDDATE = DateTime.Now;
                    selfNof.NOF_PARAM = req.test.Id;
                    selfNof.NOF_STATUS = 1;
                    selfNof.NOF_CONTENT = "Bạn đã chỉnh sửa thành công đề thi môn " + _context.Subjects.FirstOrDefault(x => x.Id == req.test.TES_SUBID).SUB_NAME
                        + " (Mật khẩu: " + req.test.TES_PASSWORD + "). Đề thi đã được gửi đến quản lý khoa để xét duyệt!";
                    selfNof.NOF_LINK = "/test";
                    selfNof.NOF_TITLE = "Thông báo chờ xét duyệt đề thi!";
                    _context.Nofications.Add(selfNof);
                    await _context.SaveChangesAsync();

                    List<User_Permission> listUserPermission = await _context.User_Permissions.
                        Include(x => x.Permissions).Where(x => x.USPE_PERID == 9).ToListAsync();
                    Subject subject = await _context.Subjects.FirstOrDefaultAsync(x => x.Id == req.test.TES_SUBID);
                    List<Employee> listEmployee = await _context.Employees.
                        Include(x => x.Users).Where(x => x.EMP_FACID == subject.SUB_FACID).ToListAsync();
                    for (int i = 0; i < listEmployee.Count; i++)
                    {
                        if (listUserPermission.FirstOrDefault(x => x.USPE_USEID == listEmployee.ElementAt(i).Users.Id) != null)
                        {
                            Nofication nof = new Nofication();
                            nof.NOF_USEID = listEmployee.ElementAt(i).Users.Id;
                            nof.NOF_CREATEDBY = req.test.TES_MODIFIEDBY;
                            nof.NOF_CREATEDDATE = DateTime.Now;
                            nof.NOF_PARAM = req.test.Id;
                            nof.NOF_STATUS = 1;
                            nof.NOF_CONTENT = listUserPermission.FirstOrDefault(x => x.USPE_USEID == listEmployee.ElementAt(i).Users.Id).Permissions.PER_NAME
                                + " " + listEmployee.ElementAt(i).EMP_LASTNAME + " " + listEmployee.ElementAt(i).EMP_FIRSTNAME
                                + " đã chỉnh sửa và gửi yêu cầu xét duyệt lại đề thi môn " + _context.Subjects.FirstOrDefault(x => x.Id == req.test.TES_SUBID).SUB_NAME
                                + " (Mật khẩu: " + req.test.TES_PASSWORD + ")!";
                            nof.NOF_LINK = "/test";
                            nof.NOF_TITLE = "Xét duyệt đề thi";
                            _context.Nofications.Add(nof);
                            await _context.SaveChangesAsync();
                        }
                    }

                    //log
                    Log log = new Log();
                    log.LOG_DATE = DateTime.Now;
                    log.LOG_USEID = req.test.TES_MODIFIEDBY.GetValueOrDefault();
                    log.LOG_MODULE = "Sửa đề thi";
                    log.LOG_ACTID = 2;
                    log.LOG_TABID = 1016;
                    string title = "<b>Loại bài thi:</b><title>" +
                        "<b>Môn học:</b><title>" +
                        "<b>Điểm tối đa:</b><title>" +
                        "<b>Định dạng bài thi:</b><title>" +
                        "<b>Số lượng câu hỏi:</b><title>" +
                        "<b>Thời gian làm bài:</b><title>" +
                        "<b>Trộn câu hỏi, đáp án:</b><title>" +
                        "<b>Số lượng mã đề:</b><title>" +
                        "<b>Trạng thái:</b><title>";

                    int countQuestion = 0;
                    if (isChange_testQuestion == false)
                    {
                        for (int i = 0; i < testDB.Test_Questions.Count; i++)
                        {
                            if (testDB.Test_Questions.ElementAt(i).TEQU_QUEID != null)
                                countQuestion++;
                            else
                                countQuestion += testDB.Test_Questions.ElementAt(i).Passages.Questions.Count;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < testDB.Test_Questions.Count; i++)
                        {
                            if (testDB.Test_Questions.ElementAt(i).TEQU_QUEID != null)
                                countQuestion++;
                            else
                                countQuestion += _context.Passages.
                                    Include(x => x.Questions).
                                    FirstOrDefault(x => x.Id == testDB.Test_Questions.ElementAt(i).TEQU_PASID).Questions.Count;
                        }
                    }

                    int countTest = 0;
                    for (int j = 0; j < testDB.Test_Details.Count; j++)
                    {
                        string currentCode = "";
                        if (testDB.Test_Details.ElementAt(j).TEDE_CODE != currentCode && testDB.Test_Details.ElementAt(j).TEDE_TESID == testDB.Id)
                        {
                            countTest++;
                            currentCode = testDB.Test_Details.ElementAt(j).TEDE_CODE;
                        }
                    }

                    string updateLog = _context.Test_Types.FirstOrDefault(x => x.Id == testDB.TES_TETYID).TETY_NAME + "<to>" + _context.Test_Types.FirstOrDefault(x => x.Id == req.test.TES_TETYID).TETY_NAME + "<item>"
                        + _context.Subjects.FirstOrDefault(x => x.Id == testDB.TES_SUBID).SUB_NAME + "<to>" + _context.Subjects.FirstOrDefault(x => x.Id == req.test.TES_SUBID).SUB_NAME + "<item>"
                        + testDB.TES_MAXSCORE + "<to>" + req.test.TES_MAXSCORE + "<item>"
                        + (testDB.TestingFormats.ElementAt(0).TESF_ISSHOWPART == false ? "Thường" : "Toeic") + "<to>" + (req.testFormat == 1 ? "Thường" : "Toeic") + "<item>"
                        + countQuestion + "<to>" + req.numberOfQuestion + "<item>"
                        + testDB.TES_TIME + " phút" + "<to>" + req.test.TES_TIME + " phút" + "<item>"
                        + (testDB.TES_ISSHUFFLE == false ? "Không trộn" : "Trộn theo mã đề") + "<to>" + (req.shuffle == 1 ? "Không trộn" : "Trộn theo mã đề") + "<item>"
                        + (testDB.TES_ISSHUFFLE == false ? "0" : countTest.ToString()) + "<item>" + "<to>" + (req.shuffle == 1 ? "0" : req.numberOfTest.ToString()) + "<item>"
                        + (testDB.TES_STATUS == 1 ? "Đang chờ duyệt" : (testDB.TES_STATUS == 2 ? "Đã duyệt" : (testDB.TES_STATUS == 3 ? "Đang thi" : "Đã kết thúc"))) + "<to>" + (req.test.TES_STATUS == 1 ? "Đang chờ duyệt" : (req.test.TES_STATUS == 2 ? "Đã duyệt" : (req.test.TES_STATUS == 3 ? "Đang thi" : "Đã kết thúc")));
                    log.LOG_CONTENT = title + "<divider>" + updateLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetNofication", true);
                    await _hubContext.Clients.All.SendAsync("GetTest", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse
            {
                data = testDB
            };
        }
        [HttpPost("acceptTest")]
        public async Task<ActionResult<BaseResponse>> acceptTest(AcceptRequest req)
        {
            Test testDB = await _context.Tests.
                Include(x => x.Test_Questions).ThenInclude(x => x.Passages).ThenInclude(x => x.Questions).
                Include(x => x.TestingFormats).
                Include(x => x.Test_Details).
                FirstOrDefaultAsync(x => x.Id == req.id);

            if (testDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            try
            {
                testDB.TES_STATUS = 2;
                testDB.TES_ACCEPTEDBY = req.userid;
                testDB.TES_ACCEPTEDDATE = DateTime.Now;
                testDB.TES_PASSWORD = req.password;
                testDB.TES_ENCPASSWORD = Utils.Helper.GenHash(req.password);
                await _context.SaveChangesAsync();

                //nofication
                User user = await _context.Users.Include(x => x.Employees).
                    FirstOrDefaultAsync(x => x.Id == req.userid);
                User userBack = await _context.Users.Include(x => x.Employees).
                    FirstOrDefaultAsync(x => x.Id == testDB.TES_CREATEDBY);
                User_Permission userPermission = await _context.User_Permissions.
                    Include(x => x.Permissions).FirstOrDefaultAsync(x => x.USPE_USEID == req.userid);
                //self nofication
                Nofication selfNof = new Nofication();
                selfNof.NOF_CREATEDBY = req.userid;
                selfNof.NOF_CREATEDDATE = DateTime.Now;
                selfNof.NOF_LINK = "/test";
                selfNof.NOF_USEID = req.userid;
                selfNof.NOF_STATUS = 1;
                selfNof.NOF_TITLE = "Thông báo duyệt đề thi";
                selfNof.NOF_PARAM = testDB.Id;
                selfNof.NOF_CONTENT = "Bạn đã duyệt đề thi môn " + _context.Subjects.FirstOrDefault(x => x.Id == testDB.TES_SUBID).SUB_NAME +
                    " của " + _context.User_Permissions.Include(x => x.Permissions).FirstOrDefault(x => x.USPE_USEID == testDB.TES_CREATEDBY).Permissions.PER_NAME +
                    " " + userBack.Employees.ElementAt(0).EMP_LASTNAME + userBack.Employees.ElementAt(0).EMP_FIRSTNAME +
                    ", mật khẩu đề thi đã được đặt lại (Mật khẩu: " + testDB.TES_PASSWORD + ")";
                _context.Nofications.Add(selfNof);
                await _context.SaveChangesAsync();

                //nofication back
                Nofication nof = new Nofication();
                nof.NOF_USEID = testDB.TES_CREATEDBY;
                nof.NOF_STATUS = 1;
                nof.NOF_CREATEDBY = req.userid;
                nof.NOF_CREATEDDATE = DateTime.Now;
                nof.NOF_TITLE = "Xác nhận duyệt đề thi";
                nof.NOF_CONTENT = "Đề thi môn " + _context.Subjects.FirstOrDefault(x => x.Id == testDB.TES_SUBID).SUB_NAME + " của bạn đã được duyệt " +
                    "bởi " + userPermission.Permissions.PER_NAME +
                    " " + user.Employees.ElementAt(0).EMP_LASTNAME + " " + user.Employees.ElementAt(0).EMP_FIRSTNAME + ", mật khẩu đề thi đã được đặt lại." +
                    " Vui lòng liên hệ " + userPermission.Permissions.PER_NAME + " " + user.Employees.ElementAt(0).EMP_LASTNAME + " " + user.Employees.ElementAt(0).EMP_FIRSTNAME +
                    " để lấy mật khẩu cập nhật đề thi nếu cần!";
                nof.NOF_LINK = "/test";
                nof.NOF_PARAM = testDB.Id;

                _context.Nofications.Add(nof);
                await _context.SaveChangesAsync();

                //log
                Log log = new Log();
                log.LOG_DATE = DateTime.Now;
                log.LOG_USEID = req.userid;

                log.LOG_MODULE = "Duyệt đề thi";
                log.LOG_ACTID = 5;
                log.LOG_TABID = 1016;
                string title = "<b>Loại bài thi:</b><title>" +
                    "<b>Môn học:</b><title>" +
                    "<b>Điểm tối đa:</b><title>" +
                    "<b>Định dạng bài thi:</b><title>" +
                    "<b>Số lượng câu hỏi:</b><title>" +
                    "<b>Thời gian làm bài:</b><title>" +
                    "<b>Trộn câu hỏi, đáp án:</b><title>" +
                    "<b>Số lượng mã đề:</b><title>" +
                    "<b>Trạng thái:</b><title>";

                //number of question
                long count = 0;
                for (int i = 0; i < testDB.Test_Questions.Count; i++)
                {
                    if (testDB.Test_Questions.ElementAt(i).TEQU_QUEID != null)
                        count++;
                    else
                        count += testDB.Test_Questions.ElementAt(i).Passages.Questions.Count;
                }

                //number of test shuffle
                int countTest = 0;
                if (testDB.Test_Details != null && testDB.Test_Details.Count != 0)
                {
                    string currentCode = "";
                    for (int i = 0; i < testDB.Test_Details.Count; i++)
                    {
                        if (currentCode != testDB.Test_Details.ElementAt(i).TEDE_CODE && testDB.Test_Details.ElementAt(i).TEDE_TESID == testDB.Id)
                        {
                            currentCode = testDB.Test_Details.ElementAt(i).TEDE_CODE;
                            countTest++;
                        }
                    }
                }

                string acceptLog = _context.Test_Types.FirstOrDefault(x => x.Id == testDB.TES_TETYID).TETY_NAME + "<item>"
                    + _context.Subjects.FirstOrDefault(x => x.Id == testDB.TES_SUBID).SUB_NAME + "<item>"
                    + testDB.TES_MAXSCORE + "<item>"
                    + (testDB.TestingFormats.ElementAt(0).TESF_ISSHOWPART == false ? "Thường" : "Toeic") + "<item>"
                    + count + "<item>"
                    + testDB.TES_TIME + " phút" + "<item>"
                    + (testDB.TES_ISSHUFFLE == false ? "Không trộn" : "Trộn theo mã đề") + "<item>"
                    + (testDB.TES_ISSHUFFLE == false ? "0" : countTest.ToString()) + "<item>"
                    + (testDB.TES_STATUS == 1 ? "Đang chờ duyệt" : (testDB.TES_STATUS == 2 ? "Đã duyệt" : (testDB.TES_STATUS == 3 ? "Đang thi" : "Đã kết thúc")));
                log.LOG_CONTENT = title + "<divider>" + acceptLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetNofication", true);
                await _hubContext.Clients.All.SendAsync("GetTest", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = testDB
            };
        }
        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long id, long user)
        {
            Test testDB = await _context.Tests.
                Include(x => x.TestingFormats).Include(x => x.Test_Details).
                Include(x => x.Test_Questions).ThenInclude(x => x.Passages).ThenInclude(x => x.Questions).
                Include(x => x.Subjects).Include(x => x.Test_Types).
                FirstOrDefaultAsync(x => x.TES_ISDELETED == false && x.Id == id);

            if (testDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            if (testDB.TES_STATUS == 3)
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Đề thi đang sử dụng, không thể xóa!"
                };

            try
            {
                testDB.TES_ISDELETED = true;
                testDB.TES_MODIFIEDBY = user;
                testDB.TES_MODIFIEDDATE = DateTime.Now;
                await _context.SaveChangesAsync();

                //nofication
                if (testDB.TES_STATUS == 1)
                {
                    List<User_Permission> listUserPermission = await _context.User_Permissions.
                        Include(x => x.Permissions).Where(x => x.USPE_PERID == 9).ToListAsync();
                    List<Employee> listEmployee = await _context.Employees.
                        Include(x => x.Users).Where(x => x.EMP_FACID == testDB.Subjects.SUB_FACID).ToListAsync();
                    for (int i = 0; i < listEmployee.Count; i++)
                    {
                        if (listUserPermission.FirstOrDefault(x => x.USPE_USEID == listEmployee.ElementAt(i).Users.Id) != null)
                        {
                            User userDB = _context.Users.Include(x => x.Employees).FirstOrDefault(x => x.Id == user);
                            Nofication nof = new Nofication();
                            nof.NOF_USEID = listEmployee.ElementAt(i).Users.Id;
                            nof.NOF_CREATEDBY = user;
                            nof.NOF_CREATEDDATE = DateTime.Now;
                            nof.NOF_PARAM = testDB.Id;
                            nof.NOF_STATUS = 1;
                            nof.NOF_CONTENT = "Đề thi " + testDB.Test_Types.TETY_NAME + " môn " + testDB.Subjects.SUB_NAME + " được nhờ xét duyệt đã bị xóa bởi " +
                                _context.User_Permissions.Include(x => x.Permissions).FirstOrDefault(x => x.USPE_USEID == user).Permissions.PER_NAME +
                                " " + userDB.Employees.ElementAt(0).EMP_LASTNAME + " " + userDB.Employees.ElementAt(0).EMP_FIRSTNAME + "!";
                            nof.NOF_LINK = "/test";
                            nof.NOF_TITLE = "Thông báo đề thi cần xét duyệt đã bị xóa!";
                            _context.Nofications.Add(nof);
                            await _context.SaveChangesAsync();
                        }
                    }
                }
                else if (testDB.TES_STATUS == 2)
                {
                    User userDB = _context.Users.Include(x => x.Employees).FirstOrDefault(x => x.Id == user);

                    Nofication nofToAccepter = new Nofication();
                    nofToAccepter.NOF_USEID = testDB.TES_ACCEPTEDBY;
                    nofToAccepter.NOF_CREATEDBY = user;
                    nofToAccepter.NOF_CREATEDDATE = DateTime.Now;
                    nofToAccepter.NOF_PARAM = testDB.Id;
                    nofToAccepter.NOF_STATUS = 1;
                    nofToAccepter.NOF_CONTENT = "Đề thi " + testDB.Test_Types.TETY_NAME + " môn " + testDB.Subjects.SUB_NAME + " bạn đã xét duyệt đã bị xóa bởi "
                        + _context.User_Permissions.Include(x => x.Permissions).FirstOrDefault(x => x.USPE_USEID == user).Permissions.PER_NAME + " "
                        + userDB.Employees.ElementAt(0).EMP_LASTNAME + " " + userDB.Employees.ElementAt(0).EMP_FIRSTNAME;
                    nofToAccepter.NOF_LINK = "/test";
                    nofToAccepter.NOF_TITLE = "Thông báo đề thi xét duyệt đã bị xóa!";
                    _context.Nofications.Add(nofToAccepter);
                    await _context.SaveChangesAsync();
                }

                //log
                Log log = new Log();
                log.LOG_DATE = DateTime.Now;
                log.LOG_USEID = user;
                log.LOG_MODULE = "Xóa đề thi";
                log.LOG_ACTID = 3;
                log.LOG_TABID = 1016;
                string title = "<b>Loại bài thi:</b><title>" +
                    "<b>Môn học:</b><title>" +
                    "<b>Điểm tối đa:</b><title>" +
                    "<b>Định dạng bài thi:</b><title>" +
                    "<b>Số lượng câu hỏi:</b><title>" +
                    "<b>Thời gian làm bài:</b><title>" +
                    "<b>Trộn câu hỏi, đáp án:</b><title>" +
                    "<b>Số lượng mã đề:</b><title>" +
                    "<b>Trạng thái:</b><title>";

                int countQuestion = 0;
                for (int i = 0; i < testDB.Test_Questions.Count; i++)
                {
                    if (testDB.Test_Questions.ElementAt(i).TEQU_QUEID != null)
                        countQuestion++;
                    else
                        countQuestion += testDB.Test_Questions.ElementAt(i).Passages.Questions.Count;
                }

                int countTest = 0;
                for (int i = 0; i < testDB.Test_Details.Count; i++)
                {
                    string currentCode = "";
                    if (testDB.Test_Details.ElementAt(i).TEDE_CODE != currentCode && testDB.Test_Details.ElementAt(i).TEDE_TESID == testDB.Id)
                    {
                        countTest++;
                        currentCode = testDB.Test_Details.ElementAt(i).TEDE_CODE;
                    }
                }

                string deleteLog = testDB.Test_Types.TETY_NAME + "<item>"
                    + testDB.Subjects.SUB_NAME + "<item>"
                    + testDB.TES_MAXSCORE + "<item>"
                    + (testDB.TestingFormats.ElementAt(0).TESF_ISSHOWPART == false ? "Thường" : "Toeic") + "<item>"
                    + countQuestion + "<item>"
                    + testDB.TES_TIME + " phút" + "<item>"
                    + (testDB.TES_ISSHUFFLE == false ? "Không trộn" : "Trộn theo mã đề") + "<item>"
                    + (testDB.TES_ISSHUFFLE == false ? "0" : countTest.ToString()) + "<item>"
                    + (testDB.TES_STATUS == 1 ? "Đang chờ duyệt" : (testDB.TES_STATUS == 2 ? "Đã duyệt" : (testDB.TES_STATUS == 3 ? "Đang thi" : "Đã kết thúc")));
                log.LOG_CONTENT = title + "<divider>" + deleteLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetNofication", true);
                await _hubContext.Clients.All.SendAsync("GetTest", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = testDB
            };
        }

        [HttpPost("multipleDelete/{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long user, Test[] arrDel)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 1016;
            log.LOG_MODULE = "Xóa danh sách đề thi";

            string title = "<b>Loại bài thi:</b><title>" +
                        "<b>Môn học:</b><title>" +
                        "<b>Điểm tối đa:</b><title>" +
                        "<b>Định dạng bài thi:</b><title>" +
                        "<b>Số lượng câu hỏi:</b><title>" +
                        "<b>Thời gian làm bài:</b><title>" +
                        "<b>Trộn câu hỏi, đáp án:</b><title>" +
                        "<b>Số lượng mã đề:</b><title>" +
                        "<b>Trạng thái:</b><title>";
            string multiDeleteLog = "";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i = 0; i < arrDel.Length; i++)
                    {
                        Test testDB = await _context.Tests.
                            Include(x => x.TestingFormats).Include(x => x.Test_Details).
                            Include(x => x.Test_Questions).ThenInclude(x => x.Passages).ThenInclude(x => x.Questions).
                            Include(x => x.Subjects).Include(x => x.Test_Types).
                            FirstOrDefaultAsync(x => x.TES_ISDELETED == false && x.Id == arrDel[i].Id);

                        if (testDB == null)
                            return new BaseResponse
                            {
                                errorCode = 404,
                                message = "Không tìm thấy dữ liệu!"
                            };
                        if (testDB.TES_STATUS == 3 || testDB.TES_STATUS == 4)
                            return new BaseResponse
                            {
                                errorCode = 405,
                                message = "Có đề thi đang sử dụng, không thể xóa!"
                            };

                        testDB.TES_ISDELETED = true;
                        testDB.TES_MODIFIEDBY = user;
                        testDB.TES_MODIFIEDDATE = DateTime.Now;
                        await _context.SaveChangesAsync();

                        //nofication
                        if (testDB.TES_STATUS == 1)
                        {
                            List<User_Permission> listUserPermission = await _context.User_Permissions.
                                Include(x => x.Permissions).Where(x => x.USPE_PERID == 9).ToListAsync();
                            List<Employee> listEmployee = await _context.Employees.
                                Include(x => x.Users).Where(x => x.EMP_FACID == testDB.Subjects.SUB_FACID).ToListAsync();
                            for (int j = 0; j < listEmployee.Count; j++)
                            {
                                if (listUserPermission.FirstOrDefault(x => x.USPE_USEID == listEmployee.ElementAt(j).Users.Id) != null)
                                {
                                    User userDB = _context.Users.Include(x => x.Employees).FirstOrDefault(x => x.Id == user);
                                    Nofication nof = new Nofication();
                                    nof.NOF_USEID = listEmployee.ElementAt(j).Users.Id;
                                    nof.NOF_CREATEDBY = user;
                                    nof.NOF_CREATEDDATE = DateTime.Now;
                                    nof.NOF_PARAM = testDB.Id;
                                    nof.NOF_STATUS = 1;
                                    nof.NOF_CONTENT = "Đề thi " + testDB.Test_Types.TETY_NAME + " môn " + testDB.Subjects.SUB_NAME + " được nhờ xét duyệt đã bị xóa bởi " +
                                        _context.User_Permissions.Include(x => x.Permissions).FirstOrDefault(x => x.USPE_USEID == user).Permissions.PER_NAME +
                                        " " + userDB.Employees.ElementAt(0).EMP_LASTNAME + " " + userDB.Employees.ElementAt(0).EMP_FIRSTNAME + "!";
                                    nof.NOF_LINK = "/test";
                                    nof.NOF_TITLE = "Thông báo đề thi cần xét duyệt đã bị xóa!";
                                    _context.Nofications.Add(nof);
                                    await _context.SaveChangesAsync();
                                }
                            }
                        }
                        else if (testDB.TES_STATUS == 2)
                        {
                            User userDB = _context.Users.Include(x => x.Employees).FirstOrDefault(x => x.Id == user);

                            Nofication nofToAccepter = new Nofication();
                            nofToAccepter.NOF_USEID = testDB.TES_ACCEPTEDBY;
                            nofToAccepter.NOF_CREATEDBY = user;
                            nofToAccepter.NOF_CREATEDDATE = DateTime.Now;
                            nofToAccepter.NOF_PARAM = testDB.Id;
                            nofToAccepter.NOF_STATUS = 1;
                            nofToAccepter.NOF_CONTENT = "Đề thi " + testDB.Test_Types.TETY_NAME + " môn " + testDB.Subjects.SUB_NAME + " bạn đã xét duyệt đã bị xóa bởi "
                                + _context.User_Permissions.Include(x => x.Permissions).FirstOrDefault(x => x.USPE_USEID == user).Permissions.PER_NAME + " "
                                + userDB.Employees.ElementAt(0).EMP_LASTNAME + " " + userDB.Employees.ElementAt(0).EMP_FIRSTNAME;
                            nofToAccepter.NOF_LINK = "/test";
                            nofToAccepter.NOF_TITLE = "Thông báo đề thi xét duyệt đã bị xóa!";
                            _context.Nofications.Add(nofToAccepter);
                            await _context.SaveChangesAsync();
                        }

                        //log
                        int countQuestion = 0;
                        for (int j = 0; j < testDB.Test_Questions.Count; j++)
                        {
                            if (testDB.Test_Questions.ElementAt(j).TEQU_QUEID != null)
                                countQuestion++;
                            else
                                countQuestion += testDB.Test_Questions.ElementAt(j).Passages.Questions.Count;
                        }

                        int countTest = 0;
                        for (int j = 0; j < testDB.Test_Details.Count; j++)
                        {
                            string currentCode = "";
                            if (testDB.Test_Details.ElementAt(j).TEDE_CODE != currentCode && testDB.Test_Details.ElementAt(j).TEDE_TESID == testDB.Id)
                            {
                                countTest++;
                                currentCode = testDB.Test_Details.ElementAt(j).TEDE_CODE;
                            }
                        }

                        multiDeleteLog += testDB.Test_Types.TETY_NAME + "<item>"
                            + testDB.Subjects.SUB_NAME + "<item>"
                            + testDB.TES_MAXSCORE + "<item>"
                            + (testDB.TestingFormats.ElementAt(0).TESF_ISSHOWPART == false ? "Thường" : "Toeic") + "<item>"
                            + countQuestion + "<item>"
                            + testDB.TES_TIME + " phút" + "<item>"
                            + (testDB.TES_ISSHUFFLE == false ? "Không trộn" : "Trộn theo mã đề") + "<item>"
                            + (testDB.TES_ISSHUFFLE == false ? "0" : countTest.ToString()) + "<item>"
                            + (testDB.TES_STATUS == 1 ? "Đang chờ duyệt" : (testDB.TES_STATUS == 2 ? "Đã duyệt" : (testDB.TES_STATUS == 3 ? "Đang thi" : "Đã kết thúc")));
                        if (i != arrDel.Length - 1)
                            multiDeleteLog += "<delete>";
                    }

                    log.LOG_CONTENT += title + "<divider>" + multiDeleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetNofication", true);
                    await _hubContext.Clients.All.SendAsync("GetTest", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse { };
        }
        [HttpPost("getListTestMultiDel")]
        public async Task<ActionResult<BaseResponse>> getListTestMultiDel(long[] arrDel)
        {
            List<Test> listTes = new List<Test>();
            for (int i = 0; i < arrDel.Length; i++)
            {
                Test testDB = await _context.Tests.
                Include(x => x.TestingFormats).Include(x => x.Test_Details).
                Include(x => x.Test_Questions).ThenInclude(x => x.Passages).ThenInclude(x => x.Questions).
                Include(x => x.Subjects).Include(x => x.Test_Types).AsNoTracking().
                FirstOrDefaultAsync(x => x.TES_ISDELETED == false && x.Id == arrDel[i]);

                int countQuestion = 0;
                for (int j = 0; j < testDB.Test_Questions.Count; j++)
                {
                    if (testDB.Test_Questions.ElementAt(j).TEQU_QUEID != null)
                        countQuestion++;
                    else
                    {
                        countQuestion += testDB.Test_Questions.ElementAt(j).Passages.Questions.Count;
                    }
                }
                testDB.countQuestion = countQuestion;


                if (testDB != null)
                {
                    listTes.Add(testDB);
                }
            }
            return new BaseResponse
            {
                data = listTes
            };
        }
    }
}