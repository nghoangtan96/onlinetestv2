﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;
using OnlineTestBackend.Models.Request;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ScheduleTestController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public ScheduleTestController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Schedule_Tests.
                Include(x => x.Labs).
                Include(x => x.TestShifts).
                //Include(x => x.Student_Tests).ThenInclude(x => x.Students).
                Include(x => x.Tests).ThenInclude(x => x.Test_Types).
                Include(x => x.Tests).ThenInclude(x => x.Subjects).
                Where(x => x.SCTE_ISDELETED == false).AsNoTracking().ToListAsync()
            };
        }

        [HttpGet("getAllIncoming")]
        public async Task<ActionResult<BaseResponse>> getAllIncoming()
        {
            return new BaseResponse
            {
                data = await _context.Schedule_Tests.
                Include(x => x.Labs).
                Include(x => x.TestShifts).
                Include(x => x.Student_Tests).ThenInclude(x => x.Students).
                Include(x => x.Tests).ThenInclude(x => x.Test_Types).
                Include(x => x.Tests).ThenInclude(x => x.Subjects).
                Where(x => x.SCTE_ISDELETED == false && x.SCTE_STATUS != 4).AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("getListScheduleTestByDate/{date}")]
        public async Task<ActionResult<BaseResponse>> getListScheduleTestByDate(string date)
        {
            DateTime currentDate = new DateTime(int.Parse(date.Split('-')[0]), int.Parse(date.Split('-')[1]), int.Parse(date.Split('-')[2]));
            return new BaseResponse
            {
                data = await _context.Schedule_Tests.
                Include(x => x.Labs).
                Include(x => x.TestShifts).
                Include(x => x.Student_Tests).ThenInclude(x => x.Students).ThenInclude(x => x.Classes).
                Include(x => x.Tests).ThenInclude(x => x.Test_Types).
                Include(x => x.Tests).ThenInclude(x => x.Subjects).
                Where(x => x.SCTE_ISDELETED == false && x.SCTE_DATE.Date == currentDate.Date && x.SCTE_DATE.Month == currentDate.Month && x.SCTE_DATE.Year == currentDate.Year).
                AsNoTracking().
                ToListAsync()
            };
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var scheduleTest = await _context.Schedule_Tests.
                Include(x => x.Labs).
                Include(x => x.TestShifts).
                Include(x => x.Tests).ThenInclude(x => x.Test_Types).
                Include(x => x.Tests).ThenInclude(x => x.Subjects).
                Include(x => x.Tests).ThenInclude(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.Student_Tests).ThenInclude(x => x.Students).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.UserModified).ThenInclude(x => x.Employees).
                AsNoTracking().
                FirstOrDefaultAsync(x => x.SCTE_ISDELETED == false && x.Id == id);
            if (scheduleTest == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (scheduleTest.UserCreated != null)
            {
                if (scheduleTest.UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    scheduleTest.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    scheduleTest.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + scheduleTest.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
            }
            if (scheduleTest.UserModified != null)
            {
                if (scheduleTest.UserModified.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    scheduleTest.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    scheduleTest.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + scheduleTest.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
            }

            return new BaseResponse
            {
                data = scheduleTest
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(ScheduleTestRequest req)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //schedule test
                    req.scheduleTest.SCTE_CREATEDDATE = DateTime.Now;
                    req.scheduleTest.SCTE_ISDELETED = false;
                    req.scheduleTest.SCTE_STATUS = 1;
                    _context.Schedule_Tests.Add(req.scheduleTest);
                    await _context.SaveChangesAsync();

                    //student test
                    Test test = await _context.Tests.Include(x => x.Test_Details). //create list test code
                            Include(x => x.Test_Types).Include(x => x.Subjects).
                            FirstOrDefaultAsync(x => x.Id == req.scheduleTest.SCTE_TESID);
                    List<string> listTestCode = new List<string>();
                    string currentCode = "";
                    Random random = new Random();

                    for (int j = 0; j < test.Test_Details.Count; j++)
                    {
                        if (currentCode != test.Test_Details.ElementAt(j).TEDE_CODE)
                        {
                            listTestCode.Add(test.Test_Details.ElementAt(j).TEDE_CODE);
                            currentCode = test.Test_Details.ElementAt(j).TEDE_CODE;
                        }
                    }

                    for (int i = 0; i < req.listStudent.Length; i++)
                    {
                        Student_Test studentTest = new Student_Test();
                        studentTest.STTE_CREATEDDATE = DateTime.Now;
                        studentTest.STTE_SCTEID = req.scheduleTest.Id;
                        studentTest.STTE_STUID = req.listStudent[i].Id;
                        studentTest.STTE_REMAININGTIME = test.TES_TIME * 60;
                        studentTest.STTE_STATUS = 1;

                        if (test.TES_ISSHUFFLE == true)
                        {
                            studentTest.STTE_TESTCODE = listTestCode[random.Next(listTestCode.Count)];
                        }

                        _context.Student_Tests.Add(studentTest);
                        await _context.SaveChangesAsync();
                    }

                    //nofication
                    Nofication createrNof = new Nofication(); //created
                    createrNof.NOF_USEID = test.TES_CREATEDBY;
                    createrNof.NOF_CREATEDBY = req.scheduleTest.SCTE_CREATEDBY;
                    createrNof.NOF_CREATEDDATE = DateTime.Now;
                    createrNof.NOF_PARAM = req.scheduleTest.Id;
                    createrNof.NOF_STATUS = 1;
                    createrNof.NOF_CONTENT = "Đề thi " + test.Test_Types.TETY_NAME + " môn "
                        + _context.Subjects.FirstOrDefault(x => x.Id == test.TES_SUBID).SUB_NAME
                        + " bạn tạo đã được xếp lịch thi!";
                    createrNof.NOF_LINK = "/scheduletest";
                    createrNof.NOF_TITLE = "Thông báo đề thi đã được xếp lịch!";
                    _context.Nofications.Add(createrNof);
                    await _context.SaveChangesAsync();

                    if (test.TES_MODIFIEDBY != null && test.TES_MODIFIEDBY != test.TES_CREATEDBY && test.TES_MODIFIEDBY != test.TES_ACCEPTEDBY)
                    {
                        Nofication modifierNof = new Nofication(); //modified
                        modifierNof.NOF_USEID = test.TES_MODIFIEDBY;
                        modifierNof.NOF_CREATEDBY = req.scheduleTest.SCTE_CREATEDBY;
                        modifierNof.NOF_CREATEDDATE = DateTime.Now;
                        modifierNof.NOF_PARAM = req.scheduleTest.Id;
                        modifierNof.NOF_STATUS = 1;
                        modifierNof.NOF_CONTENT = "Đề thi " + test.Test_Types.TETY_NAME + " môn "
                            + _context.Subjects.FirstOrDefault(x => x.Id == test.TES_SUBID).SUB_NAME
                            + " bạn chỉnh sửa đã được xếp lịch thi!";
                        modifierNof.NOF_LINK = "/scheduletest";
                        modifierNof.NOF_TITLE = "Thông báo đề thi đã được xếp lịch!";
                        _context.Nofications.Add(modifierNof);
                        await _context.SaveChangesAsync();
                    }

                    if (test.TES_ACCEPTEDBY != null && test.TES_ACCEPTEDBY != test.TES_CREATEDBY && test.TES_ACCEPTEDBY != test.TES_MODIFIEDBY)
                    {
                        Nofication accepterNof = new Nofication(); //accepted
                        accepterNof.NOF_USEID = test.TES_ACCEPTEDBY;
                        accepterNof.NOF_CREATEDBY = req.scheduleTest.SCTE_CREATEDBY;
                        accepterNof.NOF_CREATEDDATE = DateTime.Now;
                        accepterNof.NOF_PARAM = req.scheduleTest.Id;
                        accepterNof.NOF_STATUS = 1;
                        accepterNof.NOF_CONTENT = "Đề thi " + test.Test_Types.TETY_NAME + " môn "
                            + _context.Subjects.FirstOrDefault(x => x.Id == test.TES_SUBID).SUB_NAME
                            + " bạn xét duyệt đã được xếp lịch thi!";
                        accepterNof.NOF_LINK = "/scheduletest";
                        accepterNof.NOF_TITLE = "Thông báo đề thi đã được xếp lịch!";
                        _context.Nofications.Add(accepterNof);
                        await _context.SaveChangesAsync();
                    }

                    for (int i = 0; i < req.listStudent.Length; i++)
                    {
                        Nofication nof = new Nofication();
                        nof.NOF_USEID = req.listStudent[i].Users.Id;
                        nof.NOF_CREATEDBY = req.scheduleTest.SCTE_CREATEDBY;
                        nof.NOF_CREATEDDATE = DateTime.Now;
                        nof.NOF_STATUS = 1;
                        nof.NOF_CONTENT = "Lịch thi " + test.Test_Types.TETY_NAME + " môn "
                            + test.Subjects.SUB_NAME + " đã được sắp xếp. Vui lòng kiểm tra lịch thi!";
                        nof.NOF_LINK = "/shedule";
                        nof.NOF_TITLE = "Thông báo lịch thi";
                        _context.Nofications.Add(nof);
                        await _context.SaveChangesAsync();
                    }

                    //log
                    Log log = new Log();
                    log.LOG_DATE = DateTime.Now;
                    log.LOG_USEID = req.scheduleTest.SCTE_CREATEDBY.GetValueOrDefault();
                    log.LOG_MODULE = "Thêm lịch thi";
                    log.LOG_ACTID = 1;
                    log.LOG_TABID = 1017;
                    string title = "<b>Loại bài thi:</b><title>" +
                        "<b>Môn học:</b><title>" +
                        "<b>Ngày thi:</b><title>" +
                        "<b>Ca thi:</b><title>" +
                        "<b>Phòng thi:</b>";

                    TestShift testShift = await _context.TestShifts.FirstOrDefaultAsync(x => x.Id == req.scheduleTest.SCTE_SHIID);

                    string insertLog = test.Test_Types.TETY_NAME + "<item>"
                        + test.Subjects.SUB_NAME + "<item>"
                        + req.scheduleTest.SCTE_DATE.ToString("dd/MM/yyyy") + "<item>"
                        + testShift.SHI_NAME + " (" + DateTime.Today.Add(testShift.SHI_TIMESTART).ToString("hh:mm tt") + ")<item>"
                        + _context.Labs.FirstOrDefault(x => x.Id == req.scheduleTest.SCTE_LABID).LAB_NAME;

                    log.LOG_CONTENT = title + "<divider>" + insertLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetNofication", true);
                    await _hubContext.Clients.All.SendAsync("GetScheduleTest", true);
                }
                catch (Exception ex)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                        //message = ex.Message
                    };
                }
            }

            return new BaseResponse
            {
                data = req.scheduleTest
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(long id, ScheduleTestRequest req)
        {
            Schedule_Test scheduleTestDB = await _context.Schedule_Tests.
                Include(x => x.Labs).
                Include(x => x.TestShifts).
                Include(x => x.Tests).ThenInclude(x => x.Test_Types).
                Include(x => x.Tests).ThenInclude(x => x.Subjects).
                Include(x => x.Student_Tests).ThenInclude(x => x.Students).ThenInclude(x => x.Users).
                FirstOrDefaultAsync(x => x.SCTE_ISDELETED == false && x.Id == id);

            if (scheduleTestDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            if (scheduleTestDB.SCTE_STATUS == 2 || scheduleTestDB.SCTE_STATUS == 3)
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Lịch thi đang hoặc đã được sử dụng, không thể chỉnh sửa!"
                };

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    scheduleTestDB.SCTE_DATE = req.scheduleTest.SCTE_DATE;
                    scheduleTestDB.SCTE_LABID = req.scheduleTest.SCTE_LABID;
                    scheduleTestDB.SCTE_SHIID = req.scheduleTest.SCTE_SHIID;
                    scheduleTestDB.SCTE_TESID = req.scheduleTest.SCTE_TESID;
                    scheduleTestDB.SCTE_MODIFIEDBY = req.scheduleTest.SCTE_MODIFIEDBY;
                    scheduleTestDB.SCTE_MODIFIEDDATE = DateTime.Now;
                    await _context.SaveChangesAsync();

                    //student test
                    Test test = await _context.Tests.Include(x => x.Test_Details). //create list test code
                            Include(x => x.Test_Types).Include(x => x.Subjects).
                            FirstOrDefaultAsync(x => x.Id == req.scheduleTest.SCTE_TESID);
                    List<string> listTestCode = new List<string>();
                    string currentCode = "";
                    for (int j = 0; j < test.Test_Details.Count; j++)
                    {
                        if (currentCode != test.Test_Details.ElementAt(j).TEDE_CODE)
                        {
                            listTestCode.Add(test.Test_Details.ElementAt(j).TEDE_CODE);
                            currentCode = test.Test_Details.ElementAt(j).TEDE_CODE;
                        }
                    }
                    Random random = new Random();

                    //delete
                    for (int i = 0; i < scheduleTestDB.Student_Tests.Count; i++)
                    {
                        if (req.listStudent.FirstOrDefault(x => x.Id == scheduleTestDB.Student_Tests.ElementAt(i).STTE_STUID) == null)
                        {
                            _context.Student_Tests.Remove(scheduleTestDB.Student_Tests.ElementAt(i));
                            await _context.SaveChangesAsync();

                            Nofication nof = new Nofication();
                            nof.NOF_USEID = scheduleTestDB.Student_Tests.ElementAt(i).Students.Users.Id;
                            nof.NOF_CREATEDBY = req.scheduleTest.SCTE_MODIFIEDBY;
                            nof.NOF_CREATEDDATE = DateTime.Now;
                            nof.NOF_STATUS = 1;
                            nof.NOF_CONTENT = "Lịch thi " + test.Test_Types.TETY_NAME + " môn "
                                + test.Subjects.SUB_NAME + " của bạn đã bị hủy. Vui lòng kiểm tra lại lịch thi!";
                            nof.NOF_LINK = "/shedule";
                            nof.NOF_TITLE = "Thông báo thay đổi lịch thi";
                            _context.Nofications.Add(nof);
                            await _context.SaveChangesAsync();

                            i--;
                        }
                        else //nofication
                        {
                            Nofication nof = new Nofication();
                            nof.NOF_USEID = scheduleTestDB.Student_Tests.ElementAt(i).Students.Users.Id;
                            nof.NOF_CREATEDBY = req.scheduleTest.SCTE_MODIFIEDBY;
                            nof.NOF_CREATEDDATE = DateTime.Now;
                            nof.NOF_STATUS = 1;
                            nof.NOF_CONTENT = "Lịch thi " + test.Test_Types.TETY_NAME + " môn "
                                + test.Subjects.SUB_NAME + " đã được chỉnh sửa. Vui lòng kiểm tra lại lịch thi!";
                            nof.NOF_LINK = "/shedule";
                            nof.NOF_TITLE = "Thông báo thay đổi lịch thi";
                            _context.Nofications.Add(nof);
                            await _context.SaveChangesAsync();
                        }
                    }
                    //insert
                    for (int i = 0; i < req.listStudent.Length; i++)
                    {
                        if (scheduleTestDB.Student_Tests.FirstOrDefault(x => x.STTE_STUID == req.listStudent[i].Id) == null)
                        {
                            Student_Test studentTest = new Student_Test();
                            studentTest.STTE_SCTEID = req.scheduleTest.Id;
                            studentTest.STTE_STUID = req.listStudent[i].Id;
                            studentTest.STTE_CREATEDDATE = DateTime.Now;
                            studentTest.STTE_REMAININGTIME = scheduleTestDB.Tests.TES_TIME;
                            if (scheduleTestDB.Tests.TES_ISSHUFFLE == true)
                            {
                                studentTest.STTE_TESTCODE = listTestCode[random.Next(listTestCode.Count)];
                            }

                            _context.Student_Tests.Add(studentTest);
                            await _context.SaveChangesAsync();

                            Nofication nof = new Nofication();
                            nof.NOF_USEID = req.listStudent[i].Users.Id;
                            nof.NOF_CREATEDBY = req.scheduleTest.SCTE_MODIFIEDBY;
                            nof.NOF_CREATEDDATE = DateTime.Now;
                            nof.NOF_STATUS = 1;
                            nof.NOF_CONTENT = "Bạn đã được xếp lịch thi " + test.Test_Types.TETY_NAME + " môn "
                                + test.Subjects.SUB_NAME + ". Vui lòng kiểm tra lại lịch thi!";
                            nof.NOF_LINK = "/shedule";
                            nof.NOF_TITLE = "Thông báo thay đổi lịch thi";
                            _context.Nofications.Add(nof);
                            await _context.SaveChangesAsync();
                        }
                    }

                    //nofication
                    Nofication createrNof = new Nofication(); //created
                    createrNof.NOF_USEID = test.TES_CREATEDBY;
                    createrNof.NOF_CREATEDBY = req.scheduleTest.SCTE_CREATEDBY;
                    createrNof.NOF_CREATEDDATE = DateTime.Now;
                    createrNof.NOF_PARAM = req.scheduleTest.Id;
                    createrNof.NOF_STATUS = 1;
                    createrNof.NOF_CONTENT = "Đề thi " + test.Test_Types.TETY_NAME + " môn "
                        + _context.Subjects.FirstOrDefault(x => x.Id == test.TES_SUBID).SUB_NAME
                        + " bạn tạo đã được chỉnh sửa lịch thi!";
                    createrNof.NOF_LINK = "/scheduletest";
                    createrNof.NOF_TITLE = "Thông báo đề thi đã được chỉnh sửa lịch thi!";
                    _context.Nofications.Add(createrNof);
                    await _context.SaveChangesAsync();

                    if (test.TES_MODIFIEDBY != null && test.TES_MODIFIEDBY != test.TES_CREATEDBY && test.TES_MODIFIEDBY != test.TES_ACCEPTEDBY)
                    {
                        Nofication modifierNof = new Nofication(); //modified
                        modifierNof.NOF_USEID = test.TES_MODIFIEDBY;
                        modifierNof.NOF_CREATEDBY = req.scheduleTest.SCTE_CREATEDBY;
                        modifierNof.NOF_CREATEDDATE = DateTime.Now;
                        modifierNof.NOF_PARAM = req.scheduleTest.Id;
                        modifierNof.NOF_STATUS = 1;
                        modifierNof.NOF_CONTENT = "Đề thi " + test.Test_Types.TETY_NAME + " môn "
                            + _context.Subjects.FirstOrDefault(x => x.Id == test.TES_SUBID).SUB_NAME
                            + " bạn chỉnh sửa đã được chỉnh sửa lịch thi!";
                        modifierNof.NOF_LINK = "/scheduletest";
                        modifierNof.NOF_TITLE = "Thông báo đề thi đã được chỉnh sửa lịch thi!";
                        _context.Nofications.Add(modifierNof);
                        await _context.SaveChangesAsync();
                    }

                    if (test.TES_ACCEPTEDBY != null && test.TES_ACCEPTEDBY != test.TES_CREATEDBY && test.TES_ACCEPTEDBY != test.TES_MODIFIEDBY)
                    {
                        Nofication accepterNof = new Nofication(); //accepted
                        accepterNof.NOF_USEID = test.TES_ACCEPTEDBY;
                        accepterNof.NOF_CREATEDBY = req.scheduleTest.SCTE_CREATEDBY;
                        accepterNof.NOF_CREATEDDATE = DateTime.Now;
                        accepterNof.NOF_PARAM = req.scheduleTest.Id;
                        accepterNof.NOF_STATUS = 1;
                        accepterNof.NOF_CONTENT = "Đề thi " + test.Test_Types.TETY_NAME + " môn "
                            + _context.Subjects.FirstOrDefault(x => x.Id == test.TES_SUBID).SUB_NAME
                            + " bạn xét duyệt đã được chỉnh sửa lịch thi!";
                        accepterNof.NOF_LINK = "/scheduletest";
                        accepterNof.NOF_TITLE = "Thông báo đề thi đã được chỉnh sửa lịch thi!";
                        _context.Nofications.Add(accepterNof);
                        await _context.SaveChangesAsync();
                    }

                    //log
                    Log log = new Log();
                    log.LOG_DATE = DateTime.Now;
                    log.LOG_USEID = req.scheduleTest.SCTE_MODIFIEDBY.GetValueOrDefault();
                    log.LOG_MODULE = "Sửa lịch thi";
                    log.LOG_ACTID = 2;
                    log.LOG_TABID = 1017;
                    string title = "<b>Loại bài thi:</b><title>" +
                        "<b>Môn học:</b><title>" +
                        "<b>Ngày thi:</b><title>" +
                        "<b>Ca thi:</b><title>" +
                        "<b>Phòng thi:</b>";

                    TestShift testShiftDB = await _context.TestShifts.FirstOrDefaultAsync(x => x.Id == scheduleTestDB.SCTE_SHIID);
                    TestShift testShift = await _context.TestShifts.FirstOrDefaultAsync(x => x.Id == req.scheduleTest.SCTE_SHIID);

                    string updateLog = scheduleTestDB.Tests.Test_Types.TETY_NAME + "<to>" + test.Test_Types.TETY_NAME + "<item>"
                        + scheduleTestDB.Tests.Subjects.SUB_NAME + "<to>" + test.Subjects.SUB_NAME + "<item>"
                        + scheduleTestDB.SCTE_DATE.ToString("dd/MM/yyyy") + "<to>" + req.scheduleTest.SCTE_DATE.ToString("dd/MM/yyyy") + "<item>"
                        + testShiftDB.SHI_NAME + " (" + DateTime.Today.Add(testShiftDB.SHI_TIMESTART).ToString("hh:mm tt") + ")" + "<to>"
                        + (scheduleTestDB.SCTE_SHIID != req.scheduleTest.SCTE_SHIID ? (testShift.SHI_NAME + " (" + DateTime.Today.Add(testShift.SHI_TIMESTART).ToString("hh:mm tt")) : (testShiftDB.SHI_NAME + " (" + DateTime.Today.Add(testShiftDB.SHI_TIMESTART).ToString("hh:mm tt")))
                        + ")<item>"
                        + _context.Labs.FirstOrDefault(x => x.Id == scheduleTestDB.SCTE_LABID).LAB_NAME + "<to>" + _context.Labs.FirstOrDefault(x => x.Id == req.scheduleTest.SCTE_LABID).LAB_NAME;

                    log.LOG_CONTENT = title + "<divider>" + updateLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetNofication", true);
                    await _hubContext.Clients.All.SendAsync("GetScheduleTest", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse
            {
                data = scheduleTestDB
            };
        }

        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long id, long user)
        {
            Schedule_Test scheduleTestDB = await _context.Schedule_Tests.
                Include(x => x.Labs).
                Include(x => x.TestShifts).
                Include(x => x.Tests).ThenInclude(x => x.Test_Types).
                Include(x => x.Tests).ThenInclude(x => x.Subjects).
                Include(x => x.Student_Tests).ThenInclude(x => x.Students).ThenInclude(x => x.Users).
                FirstOrDefaultAsync(x => x.SCTE_ISDELETED == false && x.Id == id);

            if (scheduleTestDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            if (scheduleTestDB.SCTE_STATUS == 2 || scheduleTestDB.SCTE_STATUS == 3)
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Lịch thi đang sử dụng, không thể xóa!"
                };

            try
            {
                scheduleTestDB.SCTE_ISDELETED = true;
                scheduleTestDB.SCTE_MODIFIEDBY = user;
                scheduleTestDB.SCTE_MODIFIEDDATE = DateTime.Now;
                await _context.SaveChangesAsync();

                //nofication
                if (scheduleTestDB.SCTE_STATUS == 1)
                {
                    //nofication
                    Nofication createrNof = new Nofication(); //created
                    createrNof.NOF_USEID = scheduleTestDB.Tests.TES_CREATEDBY;
                    createrNof.NOF_CREATEDBY = user;
                    createrNof.NOF_CREATEDDATE = DateTime.Now;
                    createrNof.NOF_PARAM = scheduleTestDB.Id;
                    createrNof.NOF_STATUS = 1;
                    createrNof.NOF_CONTENT = "Đề thi " + scheduleTestDB.Tests.Test_Types.TETY_NAME + " môn "
                        + scheduleTestDB.Tests.Subjects.SUB_NAME
                        + " bạn tạo đã bị hủy lịch thi!";
                    createrNof.NOF_LINK = "/scheduletest";
                    createrNof.NOF_TITLE = "Thông báo đề thi đã bị hủy lịch thi!";
                    _context.Nofications.Add(createrNof);
                    await _context.SaveChangesAsync();

                    if (scheduleTestDB.Tests.TES_MODIFIEDBY != null && scheduleTestDB.Tests.TES_MODIFIEDBY != scheduleTestDB.Tests.TES_CREATEDBY && scheduleTestDB.Tests.TES_MODIFIEDBY != scheduleTestDB.Tests.TES_ACCEPTEDBY)
                    {
                        Nofication modifierNof = new Nofication(); //modified
                        modifierNof.NOF_USEID = scheduleTestDB.Tests.TES_MODIFIEDBY;
                        modifierNof.NOF_CREATEDBY = user;
                        modifierNof.NOF_CREATEDDATE = DateTime.Now;
                        modifierNof.NOF_PARAM = scheduleTestDB.Id;
                        modifierNof.NOF_STATUS = 1;
                        modifierNof.NOF_CONTENT = "Đề thi " + scheduleTestDB.Tests.Test_Types.TETY_NAME + " môn "
                            + scheduleTestDB.Tests.Subjects.SUB_NAME
                            + " bạn chỉnh sửa đã bị hủy lịch thi!";
                        modifierNof.NOF_LINK = "/scheduletest";
                        modifierNof.NOF_TITLE = "Thông báo đề thi đã bị hủy lịch thi!";
                        _context.Nofications.Add(modifierNof);
                        await _context.SaveChangesAsync();
                    }

                    if (scheduleTestDB.Tests.TES_ACCEPTEDBY != null && scheduleTestDB.Tests.TES_ACCEPTEDBY != scheduleTestDB.Tests.TES_CREATEDBY && scheduleTestDB.Tests.TES_ACCEPTEDBY != scheduleTestDB.Tests.TES_MODIFIEDBY)
                    {
                        Nofication accepterNof = new Nofication(); //accepted
                        accepterNof.NOF_USEID = scheduleTestDB.Tests.TES_ACCEPTEDBY;
                        accepterNof.NOF_CREATEDBY = user;
                        accepterNof.NOF_CREATEDDATE = DateTime.Now;
                        accepterNof.NOF_PARAM = scheduleTestDB.Id;
                        accepterNof.NOF_STATUS = 1;
                        accepterNof.NOF_CONTENT = "Đề thi " + scheduleTestDB.Tests.Test_Types.TETY_NAME + " môn "
                            + scheduleTestDB.Tests.Subjects.SUB_NAME
                            + " bạn xét duyệt đã bị hủy lịch thi!";
                        accepterNof.NOF_LINK = "/scheduletest";
                        accepterNof.NOF_TITLE = "Thông báo đề thi đã bị hủy lịch thi!";
                        _context.Nofications.Add(accepterNof);
                        await _context.SaveChangesAsync();
                    }

                    for (int i = 0; i < scheduleTestDB.Student_Tests.Count; i++)
                    {
                        Nofication nof = new Nofication();
                        nof.NOF_USEID = scheduleTestDB.Student_Tests.ElementAt(i).Students.Users.Id;
                        nof.NOF_CREATEDBY = user;
                        nof.NOF_CREATEDDATE = DateTime.Now;
                        nof.NOF_STATUS = 1;
                        nof.NOF_CONTENT = "Lịch thi " + scheduleTestDB.Tests.Test_Types.TETY_NAME + " môn "
                            + scheduleTestDB.Tests.Subjects.SUB_NAME + " đã bị hủy. Vui lòng kiểm tra lịch thi!";
                        nof.NOF_LINK = "/shedule";
                        nof.NOF_TITLE = "Thông báo hủy lịch thi";
                        _context.Nofications.Add(nof);
                        await _context.SaveChangesAsync();
                    }
                }

                //log
                Log log = new Log();
                log.LOG_DATE = DateTime.Now;
                log.LOG_USEID = user;
                log.LOG_MODULE = "Xóa lịch thi";
                log.LOG_ACTID = 3;
                log.LOG_TABID = 1017;
                string title = "<b>Loại bài thi:</b><title>" +
                        "<b>Môn học:</b><title>" +
                        "<b>Ngày thi:</b><title>" +
                        "<b>Ca thi:</b><title>" +
                        "<b>Phòng thi:</b>";

                TestShift testShift = await _context.TestShifts.FirstOrDefaultAsync(x => x.Id == scheduleTestDB.SCTE_SHIID);

                string deleteLog = scheduleTestDB.Tests.Test_Types.TETY_NAME + "<item>"
                    + scheduleTestDB.Tests.Subjects.SUB_NAME + "<item>"
                    + scheduleTestDB.SCTE_DATE.ToString("dd/MM/yyyy") + "<item>"
                    + testShift.SHI_NAME + " (" + DateTime.Today.Add(testShift.SHI_TIMESTART).ToString("hh:mm tt") + ")<item>"
                    + _context.Labs.FirstOrDefault(x => x.Id == scheduleTestDB.SCTE_LABID).LAB_NAME;

                log.LOG_CONTENT = title + "<divider>" + deleteLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetNofication", true);
                await _hubContext.Clients.All.SendAsync("GetScheduleTest", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = scheduleTestDB
            };
        }

        [HttpPost("multipleDelete/{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long user, Schedule_Test[] arrDel)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 1017;
            log.LOG_MODULE = "Xóa danh sách lịch thi";

            string title = "<b>Loại bài thi:</b><title>" +
                        "<b>Môn học:</b><title>" +
                        "<b>Ngày thi:</b><title>" +
                        "<b>Ca thi:</b><title>" +
                        "<b>Phòng thi:</b>";
            string multiDeleteLog = "";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i = 0; i < arrDel.Length; i++)
                    {
                        Schedule_Test scheduleTestDB = await _context.Schedule_Tests.
                            Include(x => x.Labs).
                            Include(x => x.TestShifts).
                            Include(x => x.Tests).ThenInclude(x => x.Test_Types).
                            Include(x => x.Tests).ThenInclude(x => x.Subjects).
                            Include(x => x.Student_Tests).ThenInclude(x => x.Students).ThenInclude(x => x.Users).
                            FirstOrDefaultAsync(x => x.SCTE_ISDELETED == false && x.Id == arrDel[i].Id);
                        if (scheduleTestDB == null)
                            return new BaseResponse
                            {
                                errorCode = 404,
                                message = "Không tìm thấy dữ liệu!"
                            };
                        if (scheduleTestDB.SCTE_STATUS == 3 || scheduleTestDB.SCTE_STATUS == 2)
                            return new BaseResponse
                            {
                                errorCode = 405,
                                message = "Có lịch thi đang sử dụng, không thể xóa!"
                            };

                        scheduleTestDB.SCTE_ISDELETED = true;
                        scheduleTestDB.SCTE_MODIFIEDBY = user;
                        scheduleTestDB.SCTE_MODIFIEDDATE = DateTime.Now;
                        await _context.SaveChangesAsync();

                        //nofication


                        //log
                        TestShift testShift = await _context.TestShifts.FirstOrDefaultAsync(x => x.Id == scheduleTestDB.SCTE_SHIID);
                        multiDeleteLog += scheduleTestDB.Tests.Test_Types.TETY_NAME + "<item>"
                            + scheduleTestDB.Tests.Subjects.SUB_NAME + "<item>"
                            + scheduleTestDB.SCTE_DATE.ToString("dd/MM/yyyy") + "<item>"
                            + testShift.SHI_NAME + " (" + DateTime.Today.Add(testShift.SHI_TIMESTART).ToString("hh:mm tt") + ")<item>"
                            + _context.Labs.FirstOrDefault(x => x.Id == scheduleTestDB.SCTE_LABID).LAB_NAME;
                        multiDeleteLog += "<delete>";
                    }

                    log.LOG_CONTENT += title + "<divider>" + multiDeleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetNofication", true);
                    await _hubContext.Clients.All.SendAsync("GetScheduleTest", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse { };
        }
        [HttpPost("getListScheduleTestMultiDel")]
        public async Task<ActionResult<BaseResponse>> getListScheduleTestMultiDel(long[] arrDel)
        {
            List<Schedule_Test> listScheduleTest = new List<Schedule_Test>();
            for (int i = 0; i < arrDel.Length; i++)
            {
                Schedule_Test scheduleTestDB = await _context.Schedule_Tests.
                Include(x => x.Labs).
                Include(x => x.TestShifts).
                Include(x => x.Tests).ThenInclude(x => x.Test_Types).
                Include(x => x.Tests).ThenInclude(x => x.Subjects).
                Include(x => x.Student_Tests).ThenInclude(x => x.Students).ThenInclude(x => x.Users).
                AsNoTracking().
                FirstOrDefaultAsync(x => x.SCTE_ISDELETED == false && x.Id == arrDel[i]);

                if (scheduleTestDB != null)
                {
                    listScheduleTest.Add(scheduleTestDB);
                }
            }
            return new BaseResponse
            {
                data = listScheduleTest
            };
        }
    }
}