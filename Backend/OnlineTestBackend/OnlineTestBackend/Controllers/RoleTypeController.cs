﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RoleTypeController : BaseController
    {
        public RoleTypeController(OnlineTestBackendDBContext db) : base(db) { }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Role_Types.
                Include(x => x.Roles).ThenInclude(x => x.Actionns).
                Include(x => x.Roles).ThenInclude(x => x.Tables).
                Include(x => x.Roles).ThenInclude(x => x.Roles).ThenInclude(x => x.Actionns).
                Include(x => x.Roles).ThenInclude(x => x.Roles).ThenInclude(x => x.Tables).
                AsNoTracking().
                ToListAsync()
            };
        }
    }
}