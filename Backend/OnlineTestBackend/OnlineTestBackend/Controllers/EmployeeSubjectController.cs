﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeSubjectController : BaseController
    {
        public EmployeeSubjectController(OnlineTestBackendDBContext db) : base(db) { }
        [HttpGet("getListSubjectIdByEmployeeId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListSubjectIdByEmployeeId(long id)
        {
            return new BaseResponse
            {
                data = await _context.Employee_Subjects.Where(x => x.EMSU_EMPID == id).AsNoTracking().Select(x => x.EMSU_SUBID).ToListAsync()
            };
        }
    }
}