﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Backend.Models.Request;
using Backend.Models.Response;
using System.Net.Mail;
using System.Net;
using OnlineTestBackend.Models;
using OnlineTestBackend.Models.Response;
using OnlineTestBackend.Models.Request;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly OnlineTestBackendDBContext _context;

        public UserController(OnlineTestBackendDBContext context)
        {
            _context = context;
            if (_context.Users.Count() == 0)
            {
                using (var transaction = _context.Database.BeginTransaction())
                {
                    try
                    {
                        User admin = new User
                        {
                            USE_USERNAME = "superadmin",
                            USE_PASSWORD = Utils.Helper.GenHash("superadmin"),
                            USE_STATUS = 1,
                            USE_CREATEDDATE = DateTime.Now,
                            USE_ISACTIVE = true,
                            USE_ISDELETED = false
                        };
                        _context.Users.Add(admin);
                        _context.SaveChanges();
                        Employee admin_emp = new Employee
                        {
                            EMP_FIRSTNAME = "Admin",
                            EMP_LASTNAME = "Super",
                            EMP_DATEOFBIRTH = new DateTime(1996, 11, 18),
                            EMP_GENDER = true,
                            EMP_PHONE = "0906971622",
                            EMP_EMAIL = "nghoangtan96@gmail.com",
                            EMP_ADDRESS = "144c/3 ấp Tam Hòa, xã Hiệp Hòa, Biên Hòa, Đồng Nai",
                            EMP_USEID = admin.Id,
                            EMP_CREATEDBY = admin.Id,
                            EMP_CREATEDDATE = DateTime.Now,
                            EMP_STATUS = 1,
                        };
                        _context.Employees.Add(admin_emp);
                        _context.SaveChanges();
                        User_Permission user_Permission = new User_Permission
                        {
                            USPE_USEID = admin.Id,
                            USPE_PERID = 1,
                            USPE_STATUS = 1,
                            USPE_ISACTIVE = true
                        };
                        _context.User_Permissions.Add(user_Permission);
                        _context.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception) { }
                }
            }
        }
        // GET: api/<controller>
        //[HttpGet]
        //public async Task<ActionResult<BaseResponse>> Get()
        //{
        //    List<User> userList = await _context.Users.Include(x => x.Employees).Include(x => x.Students)
        //        .Where(x => x.USE_ISDELETED == false)
        //        .AsNoTracking().ToListAsync();
        //    foreach (var item in userList)
        //    {
        //        item.USE_PASSWORD = null;
        //        item.permissionList = await _context.User_Permissions.Include(x => x.Permissions).Where(x => x.USPE_USEID == item.Id).ToListAsync();
        //    }

        //    return new BaseResponse
        //    {
        //        data = userList
        //    };
        //}

        //    [HttpGet("dataexport/{id}")]
        //    public async Task<ActionResult<BaseResponse>> GetDataExport(string id)
        //    {
        //        if (id.Equals("full"))
        //        {
        //            var list_user = await _context.Users
        //.Include(x => x.User_Create).ThenInclude(x => x.Employees)
        //.Include(x => x.User_Modified).ThenInclude(x => x.Employees)
        //.Include(x => x.Employees)
        //.Include(x => x.Students)
        //.Include(x => x.User_Permissions).ThenInclude(x => x.Permissions)
        //.AsNoTracking().ToListAsync();

        //            List<UserExport> list = new List<UserExport>();

        //            for (int i = 0; i < list_user.Count; i++)
        //            {
        //                UserExport ex = new UserExport();
        //                ex.username = list_user[i].USE_USERNAME;
        //                ex.status = list_user[i].USE_STATUS;
        //                var per = await _context.User_Permissions.Include(x => x.Permissions).Where(x => x.USEP_USEID == list_user[i].Id).FirstOrDefaultAsync();
        //                ex.user_type = per.Permissions.PER_NAME;
        //                if (list_user[i].Employees.Count > 0)
        //                {
        //                    var emp = await _context.Employees.Where(x => x.EMP_USEID == list_user[i].Id).FirstOrDefaultAsync();
        //                    ex.firstname = emp.EMP_FIRSTNAME;
        //                    ex.lastname = emp.EMP_LASTNAME;
        //                    ex.address = emp.EMP_ADDRESS;
        //                    ex.dob = emp.EMP_DATEOFBIRTH;
        //                    ex.email = emp.EMP_EMAIL;
        //                    ex.gender = emp.EMP_GENDER;
        //                    ex.phone = emp.EMP_PHONE;
        //                }
        //                else
        //                {
        //                    var stu = await _context.Students.Where(x => x.STU_USEID == list_user[i].Id).FirstOrDefaultAsync();
        //                    ex.firstname = stu.STU_FIRSTNAME;
        //                    ex.lastname = stu.STU_LASTNAME;
        //                    ex.address = stu.STU_ADDRESS;
        //                    ex.dob = stu.STU_DATEOFBIRTH;
        //                    ex.email = stu.STU_EMAIL;
        //                    ex.gender = stu.STU_GENDER;
        //                    ex.phone = stu.STU_PHONE;
        //                }
        //                list.Add(ex);
        //            }

        //            return new BaseResponse
        //            {
        //                data = list
        //            };

        //        }
        //        else
        //        {
        //            string[] arr_id;
        //            arr_id = id.Split('_');

        //            List<UserExport> list = new List<UserExport>();

        //            for (int i = 0; i < arr_id.Length; i++)
        //            {
        //                int temp = int.Parse(arr_id[i]);
        //                var list_user = await _context.Users
        //            .Include(x => x.User_Create).ThenInclude(x => x.Employees)
        //            .Include(x => x.User_Modified).ThenInclude(x => x.Employees)
        //            .Include(x => x.Employees)
        //            .Include(x => x.Students)
        //            .Include(x => x.User_Permissions).ThenInclude(x => x.Permissions).Where(x => x.Id == temp)
        //            .AsNoTracking().FirstOrDefaultAsync();
        //                UserExport ex = new UserExport();
        //                ex.username = list_user.USE_USERNAME;
        //                ex.status = list_user.USE_STATUS;
        //                var per = await _context.User_Permissions.Include(x => x.Permissions).Where(x => x.USEP_USEID == list_user.Id).FirstOrDefaultAsync();
        //                ex.user_type = per.Permissions.PER_NAME;
        //                if (list_user.Employees.Count > 0)
        //                {
        //                    var emp = await _context.Employees.Where(x => x.EMP_USEID == list_user.Id).FirstOrDefaultAsync();
        //                    ex.firstname = emp.EMP_FIRSTNAME;
        //                    ex.lastname = emp.EMP_LASTNAME;
        //                    ex.address = emp.EMP_ADDRESS;
        //                    ex.dob = emp.EMP_DATEOFBIRTH;
        //                    ex.email = emp.EMP_EMAIL;
        //                    ex.gender = emp.EMP_GENDER;
        //                    ex.phone = emp.EMP_PHONE;
        //                }
        //                else
        //                {
        //                    var stu = await _context.Students.Where(x => x.STU_USEID == list_user.Id).FirstOrDefaultAsync();
        //                    ex.firstname = stu.STU_FIRSTNAME;
        //                    ex.lastname = stu.STU_LASTNAME;
        //                    ex.address = stu.STU_ADDRESS;
        //                    ex.dob = stu.STU_DATEOFBIRTH;
        //                    ex.email = stu.STU_EMAIL;
        //                    ex.gender = stu.STU_GENDER;
        //                    ex.phone = stu.STU_PHONE;
        //                }
        //                list.Add(ex);
        //            }

        //            return new BaseResponse
        //            {
        //                data = list
        //            };
        //        }
        //    }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var User = await _context.Users
                .Include(x => x.Employees)
                .Include(x => x.Students)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (User == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (User.Employees.Count != 0)
            {
                if (User.Employees.ElementAt(0).EMP_AVATAR != null)
                {
                    User.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + User.Employees.ElementAt(0).EMP_AVATAR;
                    User.Students = null;
                }
            }
            else if (User.Students.Count != 0)
            {
                if (User.Students.ElementAt(0).STU_AVATAR != null)
                {
                    User.Students.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + User.Students.ElementAt(0).STU_AVATAR;
                    User.Employees = null;
                }
            }

            User.USE_PASSWORD = null;
            return new BaseResponse
            {
                data = User
            };
        }

        //[HttpGet("avatar/{id}")]
        //public async Task<ActionResult<BaseResponse>> GetAvatar(long id)
        //{
        //    string path = "";
        //    var emp = await _context.Employees.Where(x => x.EMP_USEID == id).FirstOrDefaultAsync();
        //    if (emp == null)
        //    {
        //        var stu = await _context.Students.Where(x => x.STU_USEID == id).FirstOrDefaultAsync();
        //        if (!String.IsNullOrEmpty(stu.STU_IMAGE) && stu.STU_IMAGE != "df_avatar.png")
        //        {
        //            path = Utils.Helper.GetBaseUrl(Request) + "/Data/User/" + stu.STU_IMAGE;
        //        }
        //        else
        //        {
        //            path = Utils.Helper.GetBaseUrl(Request) + "/Data/" + stu.STU_IMAGE;
        //        }
        //        return new BaseResponse
        //        {
        //            data = path
        //        };
        //    }
        //    if (!String.IsNullOrEmpty(emp.EMP_IMAGE) && emp.EMP_IMAGE != "df_avatar.png")
        //    {
        //        path = Utils.Helper.GetBaseUrl(Request) + "/Data/User/" + emp.EMP_IMAGE;
        //    }
        //    else
        //    {
        //        path = Utils.Helper.GetBaseUrl(Request) + "/Data/" + emp.EMP_IMAGE;
        //    }
        //    return new BaseResponse
        //    {
        //        data = path
        //    };
        //}

        //[HttpGet("getfullnamebyuserid/{id}")]
        //public async Task<ActionResult<BaseResponse>> GetFullNameByUserId(long id)
        //{
        //    var User = await _context.Users.Include(x => x.Employees).Include(x => x.Students).FirstOrDefaultAsync();
        //    if (User == null)
        //    {
        //        return NotFound();
        //    }
        //    return new BaseResponse
        //    {
        //        data = User.Employees.Count != 0 ?
        //        (User.Employees.Take(1).ToList()[0].EMP_LASTNAME + ' ' + User.Employees.Take(1).ToList()[0].EMP_FIRSTNAME) :
        //        (User.Students.Take(1).ToList()[0].STU_LASTNAME + ' ' + User.Students.Take(1).ToList()[0].STU_FIRSTNAME)
        //    };
        //}

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult<BaseResponse>> Login(LoginRequest req)
        {
            if (String.IsNullOrEmpty(req.username) || String.IsNullOrEmpty(req.password))
            {
                return new BaseResponse
                {
                    errorCode = 1,
                    message = "Vui lòng nhập đầy đủ tên tài khoản và mật khẩu!"
                };
            }
            else
            {
                var user_checkusername = await _context.Users.AsNoTracking().FirstOrDefaultAsync(x => x.USE_USERNAME == req.username);
                if (user_checkusername == null)
                {
                    return new BaseResponse
                    {
                        errorCode = 2,
                        message = "Tên tài khoản không tồn tại!"
                    };
                }
                else
                {
                    var user = await _context.Users.Include(x => x.Employees).Include(x => x.Students)
                    .AsNoTracking().FirstOrDefaultAsync(x => x.USE_USERNAME == req.username && x.USE_PASSWORD == Utils.Helper.GenHash(req.password));
                    if (user == null)
                    {
                        return new BaseResponse
                        {
                            errorCode = 3,
                            message = "Sai mật khẩu!"
                        };
                    }
                    else
                    {
                        if (user.USE_STATUS == 2)
                        {
                            return new BaseResponse
                            {
                                errorCode = 4,
                                message = "Tài khoản đã bị khóa!"
                            };
                        }
                        else
                        {
                            var claimdata = new[] { new Claim(ClaimTypes.Name, req.username) };
                            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Utils.Helper.AppKey));
                            var signingCredential = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                            var token = new JwtSecurityToken(
                                issuer: Utils.Helper.Issuer,
                                audience: Utils.Helper.Issuer,
                                expires: DateTime.Now.AddHours(24),
                                claims: claimdata,
                                signingCredentials: signingCredential
                                );
                            var tokenString = new JwtSecurityTokenHandler().WriteToken(token);
                            return new BaseResponse
                            {
                                errorCode = 0,
                                data = new LoginResponse
                                {
                                    Id = user.Id,
                                    username = user.USE_USERNAME,
                                    token = "Bearer " + tokenString,
                                }
                            };
                        }
                    }
                }
            }
        }
        [HttpPost("changepassword")]
        public async Task<ActionResult<BaseResponse>> ChangePassword(ChangePasswordRequest req)
        {
            Log log = new Log();
            log.LOG_USEID = req.userId;
            log.LOG_TABID = 11;
            log.LOG_ACTID = 2;
            log.LOG_DATE = DateTime.Now;
            log.LOG_MODULE = "Đổi mật khẩu";

            var user = await _context.Users.FindAsync(req.userId);
            if (user == null)
            {
                return new BaseResponse
                {
                    errorCode = 1,
                    message = "Không tìm thấy dữ liệu!"
                };
            }
            else
            {
                if (user.USE_PASSWORD != Utils.Helper.GenHash(req.oldPassword))
                {
                    return new BaseResponse
                    {
                        errorCode = 2,
                        message = "Mật khẩu cũ không chính xác!"
                    };
                }
                else
                {
                    user.USE_PASSWORD = Utils.Helper.GenHash(req.newPassword);
                    user.USE_MODIFIEDBY = req.userId;
                    user.USE_MODIFIEDDATE = DateTime.Now;
                    await _context.SaveChangesAsync();

                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    return new BaseResponse
                    {
                        data = user
                    };
                }
            }
        }

        [AllowAnonymous]
        [HttpPost("resetpassword")]
        public async Task<ActionResult<BaseResponse>> ResetPassword(ResetPasswordRequest resetPasswordRequest)
        {
            Employee employee = await _context.Employees.Include(x => x.Users).FirstOrDefaultAsync(x => x.EMP_EMAIL == resetPasswordRequest.email);
            Student student = await _context.Students.Include(x => x.Users).FirstOrDefaultAsync(x => x.STU_EMAIL == resetPasswordRequest.email);
            if (employee == null && student == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Email chưa được đăng ký bởi tài khoản nào!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_ACTID = 2;
            log.LOG_TABID = 11;
            log.LOG_MODULE = "Đặt lại mật khẩu";

            if (employee != null)
            {
                log.LOG_USEID = employee.Users.Id;
                log.LOG_CONTENT = "Nhân viên " + employee.EMP_LASTNAME + " " + employee.EMP_FIRSTNAME +
                    " đã sử dụng chức năng đặt lại mật khẩu thông qua tài khoản email " + employee.EMP_EMAIL;

                var passwordLength = await _context.Settings.FirstOrDefaultAsync(x => x.Id == 2);
                string rawPassword = Utils.Helper.RandomString(passwordLength.SET_VALUE.GetValueOrDefault());

                employee.Users.USE_PASSWORD = Utils.Helper.GenHash(rawPassword);
                await _context.SaveChangesAsync();

                Nofication nofication = new Nofication();
                nofication.NOF_USEID = employee.Users.Id;
                nofication.NOF_CREATEDBY = employee.Users.Id;
                nofication.NOF_LINK = "/info_user";
                nofication.NOF_CREATEDDATE = DateTime.Now;
                nofication.NOF_STATUS = 1;
                nofication.NOF_TITLE = "Thông báo bảo mật tài khoản";
                nofication.NOF_CONTENT = "Tài khoản của bạn vừa được đặt lại mật khẩu thông qua tài khoản email " +
                    employee.EMP_EMAIL + ", vui lòng đổi mật khẩu để tiện sử dụng sau này!";
                _context.Nofications.Add(nofication);
                await _context.SaveChangesAsync();

                //email send
                List<Information> listInformation = await _context.Informations.
                    Where(x => x.INF_ISMULTI == true && x.INF_ISDELETED == false).ToListAsync();

                MailAddress to = new MailAddress(employee.EMP_EMAIL);
                MailAddress from = new MailAddress("hcmunre.fake@gmail.com");

                MailMessage message = new MailMessage(from, to);
                message.Subject = "Đặt lại mật khẩu nhân viên " + employee.EMP_LASTNAME + " " + employee.EMP_FIRSTNAME;
                message.Body = "Kính gửi " + (employee.EMP_GENDER == true ? "ông" : "bà") + " <span class='text-capitalize'>" +
                    employee.EMP_LASTNAME + " " + employee.EMP_FIRSTNAME + "</span>,<br>" +
                    listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + " thông báo: <br> " +
                    "Tài khoản của bạn đã được đặt lại mật khẩu, " +
                    "vui lòng truy cập và sử dụng tại địa chỉ " +
                    "<a href='" + Utils.Helper.Issuer + "'>" + Utils.Helper.Issuer + "</a> với tài khoản:<br>" +
                    "<span class='pl-4'><b>Tên tài khoản:</b> " + employee.Users.USE_USERNAME + "</span><br>" +
                    "<span class='pl-4'><b>Mật khẩu:</b> " + rawPassword + "</span><br>" +
                    "<br>Mọi chi tiết thắc mắc vui lòng liên hệ:<br>" + listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + "<br>" +
                    "Số điện thoại: " + listInformation.Where(x => x.INF_KEYNAME == "Số điện thoại").FirstOrDefault().INF_VALUE + "<br>" +
                    "Email: " + listInformation.Where(x => x.INF_KEYNAME == "Email").FirstOrDefault().INF_VALUE + "<br>" +
                    "Địa chỉ: " + listInformation.Where(x => x.INF_KEYNAME == "Địa chỉ").FirstOrDefault().INF_VALUE;
                message.IsBodyHtml = true;
                //message.Attachments.Add(new Attachment(_hostingEnv.ContentRootPath + "\\Data\\Users\\default_avatar.png"));

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
                {
                    Credentials = new NetworkCredential("hcmunre.fake@gmail.com", "tnmt@12345"),
                    EnableSsl = true
                };

                try
                {
                    await client.SendMailAsync(message);
                }
                catch (SmtpException ex) { Console.WriteLine(ex.ToString()); }

                return new BaseResponse
                {
                    message = "Đặt lại mật khẩu thành công, vui lòng kiểm tra email " + employee.EMP_EMAIL + " để xác nhận tài khoản!"
                };
            }
            if (student != null)
            {
                log.LOG_USEID = employee.Users.Id;
                log.LOG_CONTENT = "Sinh viên " + student.STU_LASTNAME + " " + student.STU_FIRSTNAME +
                    " đã sử dụng chức năng đặt lại mật khẩu thông qua tài khoản email " + student.STU_EMAIL;

                var passwordLength = await _context.Settings.FirstOrDefaultAsync(x => x.Id == 2);
                string rawPassword = Utils.Helper.RandomString(passwordLength.SET_VALUE.GetValueOrDefault());

                student.Users.USE_PASSWORD = Utils.Helper.GenHash(rawPassword);
                await _context.SaveChangesAsync();

                Nofication nofication = new Nofication();
                nofication.NOF_USEID = student.Users.Id;
                nofication.NOF_CREATEDBY = student.Users.Id;
                nofication.NOF_LINK = "/info_user";
                nofication.NOF_CREATEDDATE = DateTime.Now;
                nofication.NOF_STATUS = 1;
                nofication.NOF_TITLE = "Thông báo bảo mật tài khoản";
                nofication.NOF_CONTENT = "Tài khoản của bạn vừa được đặt lại mật khẩu thông qua tài khoản email " +
                    employee.EMP_EMAIL + ", vui lòng đổi mật khẩu để tiện sử dụng sau này!";
                _context.Nofications.Add(nofication);
                await _context.SaveChangesAsync();

                //email send
                List<Information> listInformation = await _context.Informations.
                    Where(x => x.INF_ISMULTI == true && x.INF_ISDELETED == false).ToListAsync();

                MailAddress to = new MailAddress(student.STU_EMAIL);
                MailAddress from = new MailAddress("hcmunre.fake@gmail.com");

                MailMessage message = new MailMessage(from, to);
                message.Subject = "Đặt lại mật khẩu sinh viên " + student.STU_LASTNAME + " " + student.STU_FIRSTNAME;
                message.Body = "Kính gửi " + (student.STU_GENDER == true ? "ông" : "bà") + " <span class='text-capitalize'>" +
                    student.STU_LASTNAME + " " + student.STU_FIRSTNAME + "</span>,<br>" +
                    listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + " thông báo: <br> " +
                    "Tài khoản của bạn đã được đặt lại mật khẩu, " +
                    "vui lòng truy cập và sử dụng tại địa chỉ " +
                    "<a href='" + Utils.Helper.Issuer + "'>" + Utils.Helper.Issuer + "</a> với tài khoản:<br>" +
                    "<span class='pl-4'><b>Tên tài khoản:</b> " + student.Users.USE_USERNAME + "</span><br>" +
                    "<span class='pl-4'><b>Mật khẩu:</b> " + rawPassword + "</span><br>" +
                    "<br>Mọi chi tiết thắc mắc vui lòng liên hệ:<br>" + listInformation.Where(x => x.INF_KEYNAME == "Tên tổ chức").FirstOrDefault().INF_VALUE + "<br>" +
                    "Số điện thoại: " + listInformation.Where(x => x.INF_KEYNAME == "Số điện thoại").FirstOrDefault().INF_VALUE + "<br>" +
                    "Email: " + listInformation.Where(x => x.INF_KEYNAME == "Email").FirstOrDefault().INF_VALUE + "<br>" +
                    "Địa chỉ: " + listInformation.Where(x => x.INF_KEYNAME == "Địa chỉ").FirstOrDefault().INF_VALUE;
                message.IsBodyHtml = true;
                //message.Attachments.Add(new Attachment(_hostingEnv.ContentRootPath + "\\Data\\Users\\default_avatar.png"));

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
                {
                    Credentials = new NetworkCredential("hcmunre.fake@gmail.com", "tnmt@12345"),
                    EnableSsl = true
                };

                try
                {
                    await client.SendMailAsync(message);
                }
                catch (SmtpException ex) { Console.WriteLine(ex.ToString()); }

                return new BaseResponse
                {
                    message = "Đặt lại mật khẩu thành công, vui lòng kiểm tra email " + student.STU_EMAIL + " để xác nhận tài khoản!"
                };
            }
            return new BaseResponse
            {
                errorCode = 2,
                message = "Đã xảy ra lỗi!"
            };
        }

        // POST api/<controller>
        //[HttpPost]
        //public async Task<ActionResult<User>> Post(EmployeeRequest req)
        //{
        //    using (var transaction = _context.Database.BeginTransaction())
        //    {
        //        try
        //        {
        //            string temp_pass = req.user.USE_PASSWORD;
        //            req.user.USE_CREATEDATE = DateTime.Now;
        //            req.user.USE_PASSWORD = Utils.Helper.GenHash(req.user.USE_PASSWORD);
        //            _context.Users.Add(req.user);
        //            await _context.SaveChangesAsync();

        //            req.employee.EMP_USEID = req.user.Id;
        //            req.employee.EMP_CREATEDATE = DateTime.Now;
        //            _context.Employees.Add(req.employee);
        //            await _context.SaveChangesAsync();

        //            for (int i = 0; i < req.user_permissions.Length; i++)
        //            {
        //                _context.User_Permissions.Add(new User_Permission
        //                {
        //                    USEP_USEID = req.user.Id,
        //                    USEP_PERID = req.user_permissions[i],
        //                    USEP_CREATEBY = req.employee.EMP_CREATEBY,
        //                    USEP_CREATEDATE = DateTime.Now
        //                });
        //                await _context.SaveChangesAsync();
        //            }

        //            for (int i = 0; i < req.employee_position.Length; i++)
        //            {
        //                _context.Employee_Positions.Add(new Employee_Position
        //                {
        //                    EMPO_EMPID = req.employee.Id,
        //                    EMPO_POSID = req.employee_position[i],
        //                    EMPO_ISDELETED = false,
        //                    EMPO_STATUS = 1,
        //                    EMPO_CREATEBY = req.employee.EMP_CREATEBY,
        //                    EMPO_CREATEDATE = DateTime.Now
        //                });
        //                await _context.SaveChangesAsync();
        //            }

        //            MailAddress to = new MailAddress(req.employee.EMP_EMAIL);
        //            MailAddress from = new MailAddress("liberal.education.299@gmail.com");

        //            MailMessage message = new MailMessage(from, to);
        //            message.Subject = "Thông tin tài khoản sử dụng tại trung tâm anh ngữ Liberal";
        //            message.Body = "<strong>Tài khoản dành cho nhân viên của bạn đã được tạo </strong> <br> <strong>Tài khoản: </strong>" + req.user.USE_USERNAME + " <br> <strong>Mật khẩu: </strong>" + temp_pass;
        //            message.IsBodyHtml = true;

        //            SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
        //            {
        //                Credentials = new NetworkCredential("liberal.education.299@gmail.com", "ngoaingukhaiphong"),
        //                EnableSsl = true
        //            };
        //            // code in brackets above needed if authentication required 


        //            try
        //            {
        //                client.Send(message);
        //            }
        //            catch (SmtpException ex)
        //            {
        //                Console.WriteLine(ex.ToString());
        //            }

        //            Log log = new Log();
        //            log.LOG_DATE = DateTime.Now;
        //            log.LOG_USEID = req.employee.EMP_CREATEBY;
        //            log.LOG_NOTE = "<span class=\"text-success\"><b>Thêm</b></span> nhân viên <b>" + req.employee.EMP_LASTNAME + " " + req.employee.EMP_FIRSTNAME + "</b> ở <b>Danh sách Tài khoản</b>";
        //            _context.Logs.Add(log);
        //            await _context.SaveChangesAsync();

        //            transaction.Commit();
        //        }
        //        catch (Exception) { }
        //    }
        //    return CreatedAtAction("Get", new { id = req.user.Id }, new BaseResponse { data = req.user });
        //}

        //[HttpPost("student")]
        //public async Task<ActionResult<User>> Post(StudentRequest req)
        //{
        //    using (var transaction = _context.Database.BeginTransaction())
        //    {
        //        try
        //        {
        //            string temp_pass = req.user.USE_PASSWORD;
        //            req.user.USE_CREATEDATE = DateTime.Now;
        //            req.user.USE_PASSWORD = Utils.Helper.GenHash(req.user.USE_PASSWORD);
        //            _context.Users.Add(req.user);
        //            await _context.SaveChangesAsync();

        //            req.student.STU_USEID = req.user.Id;
        //            req.student.STU_CREATEDATE = DateTime.Now;
        //            _context.Students.Add(req.student);
        //            await _context.SaveChangesAsync();

        //            for (int i = 0; i < req.user_permissions.Length; i++)
        //            {
        //                _context.User_Permissions.Add(new User_Permission
        //                {
        //                    USEP_USEID = req.user.Id,
        //                    USEP_PERID = req.user_permissions[i],
        //                    USEP_CREATEBY = req.user.USE_CREATEBY.GetValueOrDefault(),
        //                    USEP_CREATEDATE = DateTime.Now
        //                });
        //                await _context.SaveChangesAsync();
        //            }

        //            MailAddress to = new MailAddress(req.student.STU_EMAIL);
        //            MailAddress from = new MailAddress("liberal.education.299@gmail.com");

        //            MailMessage message = new MailMessage(from, to);
        //            message.Subject = "Thông tin tài khoản sử dụng tại trung tâm anh ngữ Liberal";
        //            message.Body = "<strong>Tài khoản dành cho học viên của bạn đã được tạo </strong> <br> <strong>Tài khoản: </strong>" + req.user.USE_USERNAME + " <br> <strong>Mật khẩu: </strong>" + temp_pass;
        //            message.IsBodyHtml = true;

        //            SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
        //            {
        //                Credentials = new NetworkCredential("liberal.education.299@gmail.com", "ngoaingukhaiphong"),
        //                EnableSsl = true
        //            };

        //            try
        //            {
        //                client.Send(message);
        //            }
        //            catch (SmtpException ex)
        //            {
        //                Console.WriteLine(ex.ToString());
        //            }
        //            Log log = new Log();
        //            log.LOG_DATE = DateTime.Now;
        //            log.LOG_USEID = req.student.STU_CREATEBY.GetValueOrDefault();
        //            log.LOG_NOTE = "<span class=\"text-success\"><b>Thêm</b></span> học viên <b>" + req.student.STU_LASTNAME + " " + req.student.STU_FIRSTNAME + "</b> ở <b>Danh sách Tài khoản</b>";
        //            _context.Logs.Add(log);
        //            await _context.SaveChangesAsync();

        //            transaction.Commit();
        //        }
        //        catch (Exception) { }
        //    }
        //    return CreatedAtAction("Get", new { id = req.user.Id }, new BaseResponse { data = req.user });
        //}

        // PUT api/<controller>/5
        //[HttpPut("{id}")]
        //public async Task<ActionResult<User>> Put(long id, EmployeeRequest req)
        //{
        //    User useDB = await _context.Users.FindAsync(id);
        //    string imageurl = "";
        //    if (useDB == null)
        //    {
        //        return NotFound();
        //    }

        //    using (var transaction = _context.Database.BeginTransaction())
        //    {
        //        try
        //        {
        //            if (useDB.USE_PASSWORD != req.user.USE_PASSWORD)
        //            {
        //                useDB.USE_PASSWORD = Utils.Helper.GenHash(req.user.USE_PASSWORD);
        //            }
        //            useDB.USE_STATUS = req.user.USE_STATUS;
        //            useDB.USE_MODIFIEDBY = req.user.USE_MODIFIEDBY;
        //            useDB.USE_MODIFIEDDATE = DateTime.Now;
        //            await _context.SaveChangesAsync();

        //            var empDB = await _context.Employees.FirstOrDefaultAsync(x => x.EMP_USEID == req.user.Id);
        //            empDB.EMP_FIRSTNAME = req.employee.EMP_FIRSTNAME;
        //            empDB.EMP_LASTNAME = req.employee.EMP_LASTNAME;
        //            empDB.EMP_GENDER = req.employee.EMP_GENDER;
        //            empDB.EMP_DATEOFBIRTH = req.employee.EMP_DATEOFBIRTH;
        //            empDB.EMP_ADDRESS = req.employee.EMP_ADDRESS;
        //            empDB.EMP_EMAIL = req.employee.EMP_EMAIL;
        //            empDB.EMP_PHONE = req.employee.EMP_PHONE;
        //            empDB.EMP_STATUS = req.employee.EMP_STATUS;
        //            empDB.EMP_IDENTITY = req.employee.EMP_IDENTITY;

        //            if ((empDB.EMP_IMAGE == "df_avatar.png" && req.employee.EMP_IMAGE != "df_avatar.png") ||
        //        empDB.EMP_IMAGE != "df_avatar.png" && req.employee.EMP_IMAGE != "df_avatar.png" && empDB.EMP_IMAGE != req.employee.EMP_IMAGE)
        //            {
        //                empDB.EMP_IMAGE = req.employee.EMP_IMAGE;
        //                if (!String.IsNullOrEmpty(empDB.EMP_IMAGE) && empDB.EMP_IMAGE != "df_avatar.png")
        //                {
        //                    imageurl = Utils.Helper.GetBaseUrl(Request) + "/Data/User/" + empDB.EMP_IMAGE;
        //                }
        //                else
        //                {
        //                    imageurl = Utils.Helper.GetBaseUrl(Request) + "/Data/" + empDB.EMP_IMAGE;
        //                }
        //            }
        //            else
        //            {
        //                if (!String.IsNullOrEmpty(empDB.EMP_IMAGE) && empDB.EMP_IMAGE != "df_avatar.png")
        //                {
        //                    imageurl = Utils.Helper.GetBaseUrl(Request) + "/Data/User/" + empDB.EMP_IMAGE;
        //                }
        //                else
        //                {
        //                    imageurl = Utils.Helper.GetBaseUrl(Request) + "/Data/" + empDB.EMP_IMAGE;
        //                }
        //            }
        //            empDB.EMP_MODIFIEDBY = req.user.USE_MODIFIEDBY;
        //            empDB.EMP_MODIFIEDDATE = DateTime.Now;
        //            await _context.SaveChangesAsync();

        //            List<User_Permission> user_perDB = await _context.User_Permissions.Where(x => x.USEP_USEID == req.user.Id).ToListAsync();
        //            //add
        //            for (int i = 0; i < req.user_permissions.Length; i++)
        //            {
        //                bool isExist = false;
        //                for (int j = 0; j < user_perDB.Count; j++)
        //                {
        //                    if (req.user_permissions[i] == user_perDB[j].USEP_PERID)
        //                    {
        //                        isExist = true;
        //                        break;
        //                    }
        //                }
        //                if (isExist == false)
        //                {
        //                    _context.User_Permissions.Add(new User_Permission
        //                    {
        //                        USEP_USEID = req.user.Id,
        //                        USEP_PERID = req.user_permissions[i],
        //                        USEP_CREATEBY = req.employee.EMP_CREATEBY,
        //                        USEP_CREATEDATE = DateTime.Now
        //                    });
        //                    await _context.SaveChangesAsync();
        //                }
        //            }
        //            //delete
        //            for (int i = 0; i < user_perDB.Count; i++)
        //            {
        //                bool isExist = false;
        //                for (int j = 0; j < req.user_permissions.Length; j++)
        //                {
        //                    if (user_perDB[i].USEP_PERID == req.user_permissions[j])
        //                    {
        //                        isExist = true;
        //                        break;
        //                    }
        //                }
        //                if (isExist == false)
        //                {
        //                    _context.User_Permissions.Remove(user_perDB[i]);
        //                    await _context.SaveChangesAsync();
        //                }
        //            }


        //            List<Employee_Position> emp_posDB = await _context.Employee_Positions.Where(x => x.EMPO_EMPID == req.employee.Id).ToListAsync();
        //            //add
        //            for (int i = 0; i < req.employee_position.Length; i++)
        //            {
        //                bool isExist = false;
        //                for (int j = 0; j < emp_posDB.Count; j++)
        //                {
        //                    if (req.employee_position[i] == emp_posDB[j].EMPO_POSID)
        //                    {
        //                        isExist = true;
        //                        break;
        //                    }
        //                }
        //                if (isExist == false)
        //                {
        //                    _context.Employee_Positions.Add(new Employee_Position
        //                    {
        //                        EMPO_EMPID = req.employee.Id,
        //                        EMPO_POSID = req.employee_position[i],
        //                        EMPO_ISDELETED = false,
        //                        EMPO_STATUS = 1,
        //                        EMPO_CREATEBY = req.employee.EMP_CREATEBY,
        //                        EMPO_CREATEDATE = DateTime.Now
        //                    });
        //                    await _context.SaveChangesAsync();
        //                }
        //            }
        //            //delete
        //            for (int i = 0; i < emp_posDB.Count; i++)
        //            {
        //                bool isExist = false;
        //                for (int j = 0; j < req.employee_position.Length; j++)
        //                {
        //                    if (emp_posDB[i].EMPO_POSID == req.employee_position[j])
        //                    {
        //                        isExist = true;
        //                        break;
        //                    }
        //                }
        //                if (isExist == false)
        //                {
        //                    _context.Employee_Positions.Remove(emp_posDB[i]);
        //                    await _context.SaveChangesAsync();
        //                }
        //            }
        //            Log log = new Log();
        //            log.LOG_DATE = DateTime.Now;
        //            log.LOG_USEID = req.employee.EMP_MODIFIEDBY.GetValueOrDefault();
        //            log.LOG_NOTE = "<span class=\"text-primary\"><b>Sửa</b></span> thông tin nhân viên <b>" + req.employee.EMP_LASTNAME + " " + req.employee.EMP_FIRSTNAME + "</b> ở <b>Danh sách Tài khoản</b>";
        //            _context.Logs.Add(log);
        //            await _context.SaveChangesAsync();

        //            transaction.Commit();
        //        }
        //        catch (Exception) { }
        //    }
        //    useDB.imageurl = imageurl;
        //    return Ok(new BaseResponse { data = useDB });
        //}

        //[HttpPut("student/{id}")]
        //public async Task<ActionResult<User>> Put(long id, StudentRequest req)
        //{
        //    var useDB = await _context.Users.FindAsync(id);
        //    string imageurl = "";
        //    if (useDB == null)
        //    {
        //        return NotFound();
        //    }

        //    using (var transaction = _context.Database.BeginTransaction())
        //    {
        //        try
        //        {
        //            if (useDB.USE_PASSWORD != req.user.USE_PASSWORD)
        //            {
        //                useDB.USE_PASSWORD = Utils.Helper.GenHash(req.user.USE_PASSWORD);
        //            }
        //            useDB.USE_STATUS = req.user.USE_STATUS;
        //            useDB.USE_MODIFIEDBY = req.user.USE_MODIFIEDBY;
        //            useDB.USE_MODIFIEDDATE = DateTime.Now;
        //            await _context.SaveChangesAsync();

        //            var stuDB = await _context.Students.FirstOrDefaultAsync(x => x.STU_USEID == req.user.Id);
        //            stuDB.STU_FIRSTNAME = req.student.STU_FIRSTNAME;
        //            stuDB.STU_LASTNAME = req.student.STU_LASTNAME;
        //            stuDB.STU_GENDER = req.student.STU_GENDER;
        //            stuDB.STU_DATEOFBIRTH = req.student.STU_DATEOFBIRTH;
        //            stuDB.STU_ADDRESS = req.student.STU_ADDRESS;
        //            stuDB.STU_EMAIL = req.student.STU_EMAIL;
        //            stuDB.STU_PHONE = req.student.STU_PHONE;
        //            if ((stuDB.STU_IMAGE == "df_avatar.png" && req.student.STU_IMAGE != "df_avatar.png") ||
        //        stuDB.STU_IMAGE != "df_avatar.png" && req.student.STU_IMAGE != "df_avatar.png" && stuDB.STU_IMAGE != req.student.STU_IMAGE)
        //            {
        //                stuDB.STU_IMAGE = req.student.STU_IMAGE;
        //                if (!String.IsNullOrEmpty(stuDB.STU_IMAGE) && stuDB.STU_IMAGE != "df_avatar.png")
        //                {
        //                    imageurl = Utils.Helper.GetBaseUrl(Request) + "/Data/User/" + stuDB.STU_IMAGE;
        //                }
        //                else
        //                {
        //                    imageurl = Utils.Helper.GetBaseUrl(Request) + "/Data/" + stuDB.STU_IMAGE;
        //                }
        //            }
        //            stuDB.STU_MODIFIEDBY = req.user.USE_MODIFIEDBY;
        //            stuDB.STU_MODIFIEDDATE = DateTime.Now;
        //            await _context.SaveChangesAsync();

        //            List<User_Permission> user_perDB = await _context.User_Permissions.Where(x => x.USEP_USEID == req.user.Id).ToListAsync();
        //            //add
        //            for (int i = 0; i < req.user_permissions.Length; i++)
        //            {
        //                bool isExist = false;
        //                for (int j = 0; j < user_perDB.Count; j++)
        //                {
        //                    if (req.user_permissions[i] == user_perDB[j].USEP_PERID)
        //                    {
        //                        isExist = true;
        //                        break;
        //                    }
        //                }
        //                if (isExist == false)
        //                {
        //                    _context.User_Permissions.Add(new User_Permission
        //                    {
        //                        USEP_USEID = req.user.Id,
        //                        USEP_PERID = req.user_permissions[i],
        //                        USEP_CREATEBY = req.user.USE_CREATEBY.GetValueOrDefault(),
        //                        USEP_CREATEDATE = DateTime.Now
        //                    });
        //                    await _context.SaveChangesAsync();
        //                }
        //            }
        //            //delete
        //            for (int i = 0; i < user_perDB.Count; i++)
        //            {
        //                bool isExist = false;
        //                for (int j = 0; j < req.user_permissions.Length; j++)
        //                {
        //                    if (user_perDB[i].USEP_PERID == req.user_permissions[j])
        //                    {
        //                        isExist = true;
        //                        break;
        //                    }
        //                }
        //                if (isExist == false)
        //                {
        //                    _context.User_Permissions.Remove(user_perDB[i]);
        //                    await _context.SaveChangesAsync();
        //                }
        //            }
        //            Log log = new Log();
        //            log.LOG_DATE = DateTime.Now;
        //            log.LOG_USEID = req.student.STU_MODIFIEDBY.GetValueOrDefault();
        //            log.LOG_NOTE = "<span class=\"text-primary\"><b>Sửa</b></span> thông tin học viên <b>" + req.student.STU_LASTNAME + " " + req.student.STU_FIRSTNAME + "</b> ở <b>Danh sách Tài khoản</b>";
        //            _context.Logs.Add(log);
        //            await _context.SaveChangesAsync();
        //            transaction.Commit();
        //        }
        //        catch (Exception) { }
        //    }
        //    useDB.imageurl = imageurl;
        //    return Ok(new BaseResponse { data = useDB });
        //}

        // DELETE api/<controller>/5
        //[HttpDelete("{id}&{user}")]
        //public async Task<ActionResult<BaseResponse>> Delete(long id, long user)
        //{
        //    var useDB = await _context.Users.Include(x => x.Employees).Include(x => x.Students).FirstOrDefaultAsync(x => x.Id == id);
        //    if (useDB == null)
        //    {
        //        return NotFound();
        //    }

        //    useDB.USE_ISDELETED = true;
        //    useDB.USE_MODIFIEDBY = user;
        //    useDB.USE_MODIFIEDDATE = DateTime.Now;
        //    if (useDB.Students.Count == 0)
        //    {
        //        useDB.Employees.Take(1).ToList()[0].FileName = useDB.Employees.Take(1).ToList()[0].EMP_IMAGE;
        //        useDB.Employees.Take(1).ToList()[0].EMP_IMAGE = "df_avatar.png";
        //    }
        //    else
        //    {
        //        useDB.Students.Take(1).ToList()[0].FileName = useDB.Students.Take(1).ToList()[0].STU_IMAGE;
        //        useDB.Students.Take(1).ToList()[0].STU_IMAGE = "df_avatar.png";
        //    }
        //    await _context.SaveChangesAsync();
        //    return new BaseResponse { data = useDB };
        //}
    }
}
