﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SchoolYearController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public SchoolYearController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.SchoolYears.Where(x => x.SCYE_ISDELETED == false).AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var schoolyear = await _context.SchoolYears.Where(x => x.SCYE_ISDELETED == false && x.Id == id).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.UserModified).ThenInclude(x => x.Employees).
                AsNoTracking().
                FirstOrDefaultAsync();
            if (schoolyear == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (schoolyear.SCYE_CREATEDBY != null)
            {
                if (schoolyear.UserCreated.Employees.ElementAt(0).EMP_AVATAR != null)
                {
                    schoolyear.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + schoolyear.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
                else
                {
                    schoolyear.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
            }

            if (schoolyear.SCYE_MODIFIEDBY != null)
            {
                if (schoolyear.UserModified.Employees.ElementAt(0).EMP_AVATAR != null)
                {
                    schoolyear.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + schoolyear.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
                else
                {
                    schoolyear.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
            }

            return new BaseResponse
            {
                data = schoolyear
            };
        }
        [HttpGet("getSchoolYearByCurrentYear/{year}")]
        public async Task<ActionResult<BaseResponse>> getSchoolYearByCurrentYear(int year)
        {
            var schoolyear = await _context.SchoolYears.Where(x => x.SCYE_ISDELETED == false && x.SCYE_STARTYEAR == year).
                AsNoTracking().
                FirstOrDefaultAsync();
            if (schoolyear == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = schoolyear
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(SchoolYear schoolyear)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = schoolyear.SCYE_CREATEDBY.GetValueOrDefault();
            log.LOG_ACTID = 1;
            log.LOG_TABID = 12;
            log.LOG_MODULE = "Thêm niên khóa";
            string title = "<b>Mã niên khóa:</b><title>" +
                "<b>Năm bắt đầu:</b>";

            string insertLog = schoolyear.SCYE_CODE + "<item>"
                + schoolyear.SCYE_STARTYEAR;

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    schoolyear.SCYE_CREATEDDATE = DateTime.Now;
                    schoolyear.SCYE_ISDELETED = false;
                    schoolyear.SCYE_STATUS = 1;

                    _context.SchoolYears.Add(schoolyear);
                    await _context.SaveChangesAsync();

                    log.LOG_CONTENT = title + "<divider>" + insertLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetSchoolYear", true);
                }
                catch (Exception) { }
            };

            return new BaseResponse
            {
                data = schoolyear
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(int id, SchoolYear schoolyear)
        {
            SchoolYear schoolyearDB = await _context.SchoolYears.
                FirstOrDefaultAsync(x => x.SCYE_ISDELETED == false && x.Id == id);

            if (schoolyearDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };

            if (_context.Students.Where(x => x.STU_SCYEID == schoolyearDB.Id).ToList().Count != 0)
                return new BaseResponse
                {
                    errorCode = 1,
                    message = "Không thể chỉnh sửa, niên khóa đã được sử dụng!"
                };

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = schoolyear.SCYE_MODIFIEDBY.GetValueOrDefault();
            log.LOG_ACTID = 2;
            log.LOG_TABID = 12;
            log.LOG_MODULE = "Sửa niên khóa";
            string title = "<b>Mã niên khóa:</b><title>" +
                "<b>Năm bắt đầu:</b>";

            string updateLog = schoolyearDB.SCYE_CODE + "<to>" + schoolyear.SCYE_CODE + "<item>"
                + schoolyearDB.SCYE_STARTYEAR + "<to>" + schoolyear.SCYE_STARTYEAR;

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    schoolyearDB.SCYE_CODE = schoolyear.SCYE_CODE;
                    schoolyearDB.SCYE_STARTYEAR = schoolyear.SCYE_STARTYEAR;
                    schoolyearDB.SCYE_MODIFIEDBY = schoolyear.SCYE_MODIFIEDBY;
                    schoolyearDB.SCYE_MODIFIEDDATE = DateTime.Now;

                    await _context.SaveChangesAsync();

                    log.LOG_CONTENT = title + "<divider>" + updateLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetSchoolYear", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 2,
                        message = "Đã xảy ra lỗi, vui lòng thử lại!"
                    };
                }
            };

            return new BaseResponse
            {
                data = schoolyear
            };
        }

        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long id, long user)
        {

            SchoolYear schoolyearDB = await _context.SchoolYears.
                Include(x => x.Students).
                FirstOrDefaultAsync(x => x.Id == id && x.SCYE_ISDELETED == false);

            if (schoolyearDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            if (schoolyearDB.SCYE_STARTYEAR == DateTime.Now.Year)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Không thể xóa, niên khóa vẫn còn hiệu lực!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 12;
            log.LOG_MODULE = "Xóa niên khóa";

            string title = "<b>Mã niên khóa:</b><title>" +
                "<b>Năm bắt đầu:</b>";

            string deleteLog = schoolyearDB.SCYE_CODE + "<item>"
                + schoolyearDB.SCYE_STARTYEAR;

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    schoolyearDB.SCYE_MODIFIEDBY = user;
                    schoolyearDB.SCYE_MODIFIEDDATE = DateTime.Now;
                    schoolyearDB.SCYE_ISDELETED = true;

                    await _context.SaveChangesAsync();

                    log.LOG_CONTENT = title + "<divider>" + deleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetSchoolYear", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 2,
                        message = "Đã xảy ra lỗi!"
                    };
                }
            };

            return new BaseResponse
            {
                data = schoolyearDB
            };
        }
    }
}