﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using OnlineTestBackend.Models;
using OnlineTestBackend.Models.Request;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionController : BaseController
    {
        private IHostingEnvironment _hostingEnv;
        private readonly IHubContext<SignalHub> _hubContext;
        public QuestionController(OnlineTestBackendDBContext db, IHostingEnvironment env, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hostingEnv = env;
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Questions.Where(x => x.QUE_ISDELETED == false).
                Include(x => x.Parts).ThenInclude(x => x.Subjects).
                Include(x => x.Passages).ThenInclude(x => x.Parts).ThenInclude(x => x.Subjects).
                Include(x => x.AnswerTypes).
                AsNoTracking().
                ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var question = await _context.Questions.
                Include(x => x.Parts).ThenInclude(x => x.Subjects).
                Include(x => x.Passages).ThenInclude(x => x.Parts).ThenInclude(x => x.Subjects).
                Include(x => x.AnswerTypes).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.UserModified).ThenInclude(x => x.Employees).
                Include(x => x.Options).
                Where(x => x.QUE_ISDELETED == false && x.Id == id).AsNoTracking().FirstOrDefaultAsync();
            if (question == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (question.UserCreated != null)
            {
                if (question.UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    question.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    question.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + question.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
            }
            if (question.UserModified != null)
            {
                if (question.UserModified.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    question.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    question.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + question.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
            }
            if (question.QUE_MEDIA != null)
            {
                question.FileUrl = Utils.Helper.GetBaseUrl(Request) + question.QUE_MEDIA;
            }

            question.countTest = _context.Questions.Include(x => x.Test_Questions).FirstOrDefault(x => x.Id == id).Test_Questions.Count;

            return new BaseResponse
            {
                data = question
            };
        }
        [HttpPost("getListQuestionMultiDel")]
        public async Task<ActionResult<BaseResponse>> getListQuestionMultiDel(long[] arrDel)
        {
            List<Question> listQue = new List<Question>();
            for (int i = 0; i < arrDel.Length; i++)
            {
                Question que = await _context.Questions.
                    Include(x => x.Options).
                    Include(x => x.AnswerTypes).
                    Include(x => x.Parts).ThenInclude(x => x.Subjects).
                    Include(x => x.Passages).ThenInclude(x => x.Parts).ThenInclude(x => x.Subjects).
                    Where(x => x.Id == arrDel[i]).AsNoTracking().FirstOrDefaultAsync();
                if (que != null)
                {
                    listQue.Add(que);
                }
            }
            return new BaseResponse
            {
                data = listQue
            };
        }
        [HttpPost("multipleDelete/{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long user, Question[] arrDel)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 1;
            log.LOG_MODULE = "Xóa danh sách đoạn văn";
            log.LOG_CONTENT = "";

            string title = "<b>Nội dung câu hỏi:</b><title>" +
                "<b>Tệp âm thanh, video:</b><title>" +
                "<b>Số điểm:</b><title>" +
                "<b>Độ khó:</b><title>" +
                "<b>Trộn đáp án:</b><title>" +
                "<b>Chia cột đáp án:</b><title>" +
                "<b>Trạng thái:</b><title>" +
                "<b>Loại câu hỏi:</b><title>" +
                "<b>Thuộc học phần:</b><title>" +
                "<b>Thuộc đoạn văn:</b><title>" +
                "<b>Tài liệu tham khảo:</b><title>" +
                "<b>Đáp án:</b>";
            string multiDeleteLog = "";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i = 0; i < arrDel.Length; i++)
                    {
                        Question questionDB = await _context.Questions.
                            Include(x => x.Options).
                            Include(x => x.AnswerTypes).
                            Include(x => x.Parts).ThenInclude(x => x.Subjects).
                            Include(x => x.Passages).ThenInclude(x => x.Parts).ThenInclude(x => x.Subjects).
                            Include(x => x.Test_Questions).
                            Where(x => x.Id == arrDel[i].Id).FirstOrDefaultAsync();
                        if (questionDB != null)
                        {
                            if (questionDB.QUE_MEDIA != null && questionDB.Test_Questions.Count == 0)
                            {
                                new FileController(_hostingEnv).Delete("Question", questionDB.QUE_MEDIA.Split("/")[3]);
                            }
                            multiDeleteLog += questionDB.QUE_CONTENT + "<item>"
                                + (questionDB.QUE_MEDIA != null ? questionDB.QUE_MEDIA : "") + "<item>"
                                + questionDB.QUE_SCORE + "<item>"
                                + questionDB.QUE_LEVEL + "<item>"
                                + (questionDB.QUE_ISSHUFFLE == true ? "Có" : "Không") + "<item>"
                                + (questionDB.QUE_OPTCOLUMN != null ? questionDB.QUE_OPTCOLUMN + "cột" : "") + "<item>"
                                + (questionDB.QUE_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>"
                                + (questionDB.QUE_ANTYID != null ? _context.AnswerTypes.FirstOrDefault(x => x.Id == questionDB.QUE_ANTYID).ANTY_NAME : "") + "<item>"
                                + (questionDB.QUE_PARID != null ? _context.Parts.FirstOrDefault(x => x.Id == questionDB.QUE_PARID).PAR_NAME : "") + "<item>"
                                + (questionDB.QUE_PASID != null && questionDB.QUE_PASID != 0 ? "Đoạn văn " + _context.Passages.FirstOrDefault(x => x.Id == questionDB.QUE_PASID).PAS_ORDER : "") + "<item>"
                                + questionDB.QUE_REFERENCE + "<item>";

                            for (int j = 0; j < questionDB.Options.Count; j++)
                            {
                                if (questionDB.QUE_ANTYID == 1 || questionDB.QUE_ANTYID == 3)
                                {
                                    multiDeleteLog += (questionDB.Options.ElementAt(j).OPT_ISCORRECT == true ? "<i class='fa fa-dot-circle-o'></i>" : "<i class='fa fa-circle-o'></i>") + "<slash>" + questionDB.Options.ElementAt(j).OPT_CONTENT;
                                }
                                else if (questionDB.QUE_ANTYID == 2)
                                {
                                    multiDeleteLog += (questionDB.Options.ElementAt(j).OPT_ISCORRECT == true ? "<i class='fa fa-check-square-o'></i>" : "<i class='fa fa-square-o'></i>") + "<slash>" + questionDB.Options.ElementAt(j).OPT_CONTENT;
                                }
                                if (j != questionDB.Options.Count - 1)
                                    multiDeleteLog += "<opt>";
                            }
                            if (i != arrDel.Length - 1)
                                multiDeleteLog += "<delete>";

                            questionDB.QUE_ISDELETED = true;
                            questionDB.QUE_MODIFIEDBY = user;
                            questionDB.QUE_MODIFIEDDATE = DateTime.Now;
                            await _context.SaveChangesAsync();
                        }
                    }

                    log.LOG_CONTENT += title + "<divider>" + multiDeleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();
                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetQuestion", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }
            return new BaseResponse { };
        }
        [HttpGet("getListByPartId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListByPartId(long id)
        {
            var questionList = await _context.Questions.
                Include(x => x.AnswerTypes).
                Include(x => x.Parts).ThenInclude(x => x.Subjects).
                Where(x => x.QUE_ISDELETED == false && x.QUE_PARID == id).AsNoTracking().ToListAsync();
            if (questionList == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = questionList
            };
        }
        [HttpGet("getListBySubjectId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListBySubjectId(long id)
        {
            var questionList = await _context.Questions.
                Include(x => x.AnswerTypes).
                Include(x => x.Parts).ThenInclude(x => x.Subjects).
                Include(x => x.Options).
                Where(x => x.QUE_ISDELETED == false && x.Parts.PAR_SUBID == id && x.QUE_PASID == null).AsNoTracking().ToListAsync();
            if (questionList == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            for (int i = 0; i < questionList.Count; i++)
            {
                for (int j = 0; j < questionList.ElementAt(i).Options.Count; j++)
                {
                    questionList.ElementAt(i).Options.ElementAt(j).OPT_ISCORRECT = false;
                }

                if (questionList.ElementAt(i).QUE_MEDIA != null)
                {
                    questionList.ElementAt(i).FileUrl = Utils.Helper.GetBaseUrl(Request) + questionList.ElementAt(i).QUE_MEDIA;
                }
            }



            return new BaseResponse
            {
                data = questionList
            };
        }
        [HttpGet("getListBySubjectIdForExam/{id}")]
        public async Task<ActionResult<BaseResponse>> getListBySubjectIdForExam(long id)
        {
            var questionList = await _context.Questions.
                Include(x => x.AnswerTypes).
                Include(x => x.Parts).ThenInclude(x => x.Subjects).
                Include(x => x.Options).
                Include(x => x.Passages).ThenInclude(x => x.Parts).
                Where(x => x.QUE_ISDELETED == false && (x.Parts.PAR_SUBID == id || x.Passages.Parts.PAR_SUBID == id)).
                AsNoTracking().
                ToListAsync();
            if (questionList == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            for (int i = 0; i < questionList.Count; i++)
            {
                for (int j = 0; j < questionList.ElementAt(i).Options.Count; j++)
                {
                    questionList.ElementAt(i).Options.ElementAt(j).OPT_ISCORRECT = false;
                }

                if (questionList.ElementAt(i).QUE_MEDIA != null)
                {
                    questionList.ElementAt(i).FileUrl = Utils.Helper.GetBaseUrl(Request) + questionList.ElementAt(i).QUE_MEDIA;
                }
            }

            return new BaseResponse
            {
                data = questionList
            };
        }
        [HttpGet("getListByPassageId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListByPassageId(long id)
        {
            var questionList = await _context.Questions.
                Include(x => x.AnswerTypes).
                Include(x => x.Passages).ThenInclude(x => x.Parts).ThenInclude(x => x.Subjects).
                Where(x => x.QUE_ISDELETED == false && x.QUE_PASID == id).AsNoTracking().ToListAsync();
            if (questionList == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = questionList
            };
        }
        [HttpGet("getSampleFileLink")]
        public async Task<ActionResult<BaseResponse>> getSampleFileLink()
        {
            return new BaseResponse
            {
                data = Utils.Helper.GetBaseUrl(Request) + "/Data/Excels/DSCH.xlsx"
            };
        }
        [HttpPost("ImportExcel/{user}&{passageId}&{partId}")]
        public async Task<ActionResult<BaseResponse>> Import(long user, long passageId, long partId, [FromForm]IFormFile file, CancellationToken cancellationToken)
        {
            if (file == null || file.Length <= 0)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy file tải lên!"
                };
            }

            if (!Path.GetExtension(file.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Định dạng file không hợp lệ!"
                };
            }

            Passage passageDB = new Passage();
            Part partDB = new Part();
            List<string> listOption = new List<string>();
            List<Question> listDBQuestion = new List<Question>();

            if (passageId != 0)
            {
                passageDB = await _context.Passages.Include(x => x.AnswerTypes).Include(x => x.Parts).AsNoTracking().
                    FirstOrDefaultAsync(x => x.Id == passageId);
            }
            else
            {
                partDB = await _context.Parts.AsNoTracking().FirstOrDefaultAsync(x => x.Id == partId);
            }

            if (partDB != null)
            {
                listDBQuestion = await _context.Questions.Where(x => x.QUE_PARID == partDB.Id).AsNoTracking().ToListAsync();
            }
            else
            {
                listDBQuestion = await _context.Questions.Where(x => x.QUE_PASID == passageDB.Id).AsNoTracking().ToListAsync();
            }

            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream, cancellationToken);

                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;

                    using (var transaction = _context.Database.BeginTransaction())
                    {
                        try
                        {
                            //check valid
                            string errorLog = "";
                            bool hasError = false;
                            for (int row = 2; row <= rowCount; row++)
                            {
                                listOption = new List<string>();
                                bool isCorrect = true;
                                string[] arrTitleSampleFile = new string[] { "Nội dung câu hỏi", "Loại câu hỏi", "Số điểm", "Độ khó", "Chia cột đáp án", "Trộn đáp án", "Lời giải", "Đáp án A", "Đáp án B", "Đáp án C", "Sử dụng đáp án D", "Đáp án D", "Sử dụng đáp án E", "Đáp án E", "Sử dụng đáp án F", "Đáp án F", "Đáp án đúng" };
                                //check column is correct
                                for (int i = 1; i <= 17; i++)
                                {
                                    if (!(worksheet.Cells[1, i].Value != null && worksheet.Cells[1, i].Value.ToString().Trim().Contains(arrTitleSampleFile[i - 1])))
                                    {
                                        isCorrect = false;
                                        break;
                                    }
                                }
                                if (isCorrect == false)
                                {
                                    return new BaseResponse
                                    {
                                        errorCode = 405,
                                        message = "Dữ liệu không hợp lệ!<divider>Các cột dữ liệu không khớp theo file mẫu!"
                                    };
                                }
                                //valid column
                                string validateLog = "<b>Dòng " + (row - 1) + ":</b><br>";
                                //question content
                                if (passageId != 0)
                                {
                                    if (passageDB.Parts.PAR_ISALLOWNULLQUESTION == false)
                                    {
                                        if (worksheet.Cells[row, 1].Value == null)
                                        {
                                            validateLog += "&emsp;- Chưa nhập nội dung câu hỏi.<br>";
                                            hasError = true;
                                        }
                                        else if (listDBQuestion.FirstOrDefault(x => x.QUE_CONTENT.Trim().ToLower() == worksheet.Cells[row, 1].Value.ToString().Trim().ToLower()) != null)
                                        {
                                            validateLog += "&emsp;- Nội dung câu hỏi đã tồn tại.<br>";
                                            hasError = true;
                                        }
                                    }
                                }
                                else
                                {
                                    if (partDB.PAR_ISALLOWNULLQUESTION == false)
                                    {
                                        if (worksheet.Cells[row, 1].Value == null)
                                        {
                                            validateLog += "&emsp;- Chưa nhập nội dung câu hỏi.<br>";
                                            hasError = true;
                                        }
                                        else if (listDBQuestion.FirstOrDefault(x => x.QUE_CONTENT.Trim().ToLower() == worksheet.Cells[row, 1].Value.ToString().Trim().ToLower()) != null)
                                        {
                                            validateLog += "&emsp;- Nội dung câu hỏi đã tồn tại.<br>";
                                            hasError = true;
                                        }
                                    }
                                }
                                //answer type
                                if (worksheet.Cells[row, 2].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập loại câu hỏi.<br>";
                                    hasError = true;
                                }
                                else if (worksheet.Cells[row, 2].Value.ToString().Trim().ToLower() != "một đáp án" && worksheet.Cells[row, 2].Value.ToString().Trim() != "nhiều đáp án" && worksheet.Cells[row, 2].Value.ToString().Trim() != "đúng sai")
                                {
                                    validateLog += "&emsp;- Loại câu hỏi chỉ chấp nhận các giá trị 'một đáp án', 'nhiều đáp án' và 'đúng sai'.<br>";
                                    hasError = true;
                                }
                                else
                                {
                                    if (passageId != 0)
                                    {
                                        if (worksheet.Cells[row, 2].Value.ToString().Trim().ToLower() != passageDB.AnswerTypes.ANTY_NAME.ToLower())
                                        {
                                            validateLog += "&emsp;- Đoạn văn chỉ chấp nhận loại câu hỏi '" + passageDB.AnswerTypes.ANTY_NAME + "'.<br>";
                                            hasError = true;
                                        }
                                    }
                                }
                                //score
                                if (worksheet.Cells[row, 3].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập số điểm.<br>";
                                    hasError = true;
                                }
                                else
                                {
                                    try
                                    {
                                        double.Parse(worksheet.Cells[row, 3].Value.ToString().Trim());
                                    }
                                    catch (Exception)
                                    {
                                        validateLog += "&emsp;- Số điểm sai định dạng.<br>";
                                        hasError = true;
                                    }
                                }
                                //level
                                if (worksheet.Cells[row, 4].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập độ khó.<br>";
                                    hasError = true;
                                }
                                else if (worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() != "dễ" &&
                                    worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() != "trung bình dễ" &&
                                    worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() != "trung bình" &&
                                    worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() != "trung bình khó" &&
                                    worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() != "khó" &&
                                    worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() != "1" &&
                                    worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() != "2" &&
                                    worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() != "3" &&
                                    worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() != "4" &&
                                    worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() != "5")
                                {
                                    validateLog += "&emsp;- Độ khó chỉ chấp nhận các giá trị 'dễ', 'trung bình dễ', 'trung bình', 'trung bình khó', 'khó' hoặc '1', '2', '3', '4', '5'.<br>";
                                    hasError = true;
                                }
                                //opt column
                                if (worksheet.Cells[row, 5].Value != null)
                                {
                                    try
                                    {
                                        int.Parse(worksheet.Cells[row, 5].Value.ToString().Trim());
                                    }
                                    catch (Exception)
                                    {
                                        validateLog += "&emsp;- Chia cột đáp án giá trị không hợp lệ.<br>";
                                        hasError = true;
                                    }
                                }
                                //is shuffle
                                if (worksheet.Cells[row, 6].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập trộn đáp án.<br>";
                                    hasError = true;
                                }
                                else if (worksheet.Cells[row, 6].Value.ToString().Trim().ToLower() != "có" &&
                                    worksheet.Cells[row, 6].Value.ToString().Trim().ToLower() != "không")
                                {
                                    validateLog += "&emsp;- Trộn đáp án chỉ chấp nhận các giá trị 'có' và 'không'.<br>";
                                    hasError = true;
                                }
                                else
                                {
                                    if (passageId != 0)
                                    {
                                        if (passageDB.PAS_ISSHUFFLE == false && worksheet.Cells[row, 6].Value.ToString().Trim().ToLower() == "có")
                                        {
                                            validateLog += "&emsp;- Đoạn văn không cho phép trộn câu hỏi.<br>";
                                            hasError = true;
                                        }
                                    }
                                    else
                                    {
                                        if (partDB.PAR_ISSHUFFLE == false && worksheet.Cells[row, 6].Value.ToString().Trim().ToLower() == "có")
                                        {
                                            validateLog += "&emsp;- Học phần không cho phép trộn câu hỏi.<br>";
                                            hasError = true;
                                        }
                                    }
                                }
                                //option A
                                if (passageId != 0)
                                {
                                    if (passageDB.Parts.PAR_ISALLOWNULLOPTION == false)
                                    {
                                        if (worksheet.Cells[row, 8].Value == null)
                                        {
                                            validateLog += "&emsp;- Chưa nhập đáp án A.<br>";
                                            hasError = true;
                                        }
                                    }
                                }
                                else
                                {
                                    if (partDB.PAR_ISALLOWNULLOPTION == false)
                                    {
                                        if (worksheet.Cells[row, 8].Value == null)
                                        {
                                            validateLog += "&emsp;- Chưa nhập đáp án A.<br>";
                                            hasError = true;
                                        }
                                    }
                                }
                                //option B
                                if (passageId != 0)
                                {
                                    if (passageDB.Parts.PAR_ISALLOWNULLOPTION == false)
                                    {
                                        if (worksheet.Cells[row, 9].Value == null)
                                        {
                                            validateLog += "&emsp;- Chưa nhập đáp án B.<br>";
                                            hasError = true;
                                        }
                                    }
                                }
                                else
                                {
                                    if (partDB.PAR_ISALLOWNULLOPTION == false)
                                    {
                                        if (worksheet.Cells[row, 9].Value == null)
                                        {
                                            validateLog += "&emsp;- Chưa nhập đáp án B.<br>";
                                            hasError = true;
                                        }
                                    }
                                }
                                //option C
                                if (worksheet.Cells[row, 2].Value.ToString().Trim().ToLower() != "đúng sai")
                                {
                                    if (passageId != 0)
                                    {
                                        if (passageDB.Parts.PAR_ISALLOWNULLOPTION == false)
                                        {
                                            if (worksheet.Cells[row, 10].Value == null)
                                            {
                                                validateLog += "&emsp;- Chưa nhập đáp án C.<br>";
                                                hasError = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (partDB.PAR_ISALLOWNULLOPTION == false)
                                        {
                                            if (worksheet.Cells[row, 10].Value == null)
                                            {
                                                validateLog += "&emsp;- Chưa nhập đáp án C.<br>";
                                                hasError = true;
                                            }
                                        }
                                    }
                                }
                                //use option D
                                bool useOptionD;
                                if (worksheet.Cells[row, 11].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập sử dụng đáp án D.<br>";
                                    hasError = true;
                                    useOptionD = false;
                                }
                                else if (worksheet.Cells[row, 11].Value.ToString().Trim().ToLower() != "có" &&
                                    worksheet.Cells[row, 11].Value.ToString().Trim().ToLower() != "không")
                                {
                                    validateLog += "&emsp;- Sử dụng đáp án D chỉ chấp nhận các giá trị 'có' và 'không'.<br>";
                                    hasError = true;
                                    useOptionD = false;
                                }
                                else
                                {
                                    useOptionD = (worksheet.Cells[row, 11].Value.ToString().Trim().ToLower() == "có") ? true : false;
                                }
                                //option D
                                if (useOptionD == true)
                                {
                                    if (passageId != 0)
                                    {
                                        if (passageDB.Parts.PAR_ISALLOWNULLOPTION == false)
                                        {
                                            if (worksheet.Cells[row, 12].Value == null)
                                            {
                                                validateLog += "&emsp;- Chưa nhập đáp án D.<br>";
                                                hasError = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (partDB.PAR_ISALLOWNULLOPTION == false)
                                        {
                                            if (worksheet.Cells[row, 12].Value == null)
                                            {
                                                validateLog += "&emsp;- Chưa nhập đáp án D.<br>";
                                                hasError = true;
                                            }
                                        }
                                    }
                                }
                                //use option E
                                bool useOptionE;
                                if (worksheet.Cells[row, 13].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập sử dụng đáp án E.<br>";
                                    hasError = true;
                                    useOptionE = false;
                                }
                                else if (worksheet.Cells[row, 13].Value.ToString().Trim().ToLower() != "có" &&
                                    worksheet.Cells[row, 13].Value.ToString().Trim().ToLower() != "không")
                                {
                                    validateLog += "&emsp;- Sử dụng đáp án E chỉ chấp nhận các giá trị 'có' và 'không'.<br>";
                                    hasError = true;
                                    useOptionE = false;
                                }
                                else
                                {
                                    useOptionE = (worksheet.Cells[row, 13].Value.ToString().Trim().ToLower() == "có") ? true : false;
                                }
                                //option E
                                if (useOptionE == true)
                                {
                                    if (passageId != 0)
                                    {
                                        if (passageDB.Parts.PAR_ISALLOWNULLOPTION == false)
                                        {
                                            if (worksheet.Cells[row, 14].Value == null)
                                            {
                                                validateLog += "&emsp;- Chưa nhập đáp án E.<br>";
                                                hasError = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (partDB.PAR_ISALLOWNULLOPTION == false)
                                        {
                                            if (worksheet.Cells[row, 14].Value == null)
                                            {
                                                validateLog += "&emsp;- Chưa nhập đáp án E.<br>";
                                                hasError = true;
                                            }
                                        }
                                    }
                                }
                                //use option F
                                bool useOptionF;
                                if (worksheet.Cells[row, 15].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập sử dụng đáp án F.<br>";
                                    hasError = true;
                                    useOptionF = false;
                                }
                                else if (worksheet.Cells[row, 15].Value.ToString().Trim().ToLower() != "có" &&
                                    worksheet.Cells[row, 15].Value.ToString().Trim().ToLower() != "không")
                                {
                                    validateLog += "&emsp;- Sử dụng đáp án F chỉ chấp nhận các giá trị 'có' và 'không'.<br>";
                                    hasError = true;
                                    useOptionF = false;
                                }
                                else
                                {
                                    useOptionF = (worksheet.Cells[row, 15].Value.ToString().Trim().ToLower() == "có") ? true : false;
                                }
                                //option F
                                if (useOptionF == true)
                                {
                                    if (passageId != 0)
                                    {
                                        if (passageDB.Parts.PAR_ISALLOWNULLOPTION == false)
                                        {
                                            if (worksheet.Cells[row, 16].Value == null)
                                            {
                                                validateLog += "&emsp;- Chưa nhập đáp án F.<br>";
                                                hasError = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (partDB.PAR_ISALLOWNULLOPTION == false)
                                        {
                                            if (worksheet.Cells[row, 16].Value == null)
                                            {
                                                validateLog += "&emsp;- Chưa nhập đáp án F.<br>";
                                                hasError = true;
                                            }
                                        }
                                    }
                                }
                                //correct option
                                listOption.Add("A");
                                listOption.Add("B");
                                listOption.Add("C");
                                if (useOptionD == true)
                                    listOption.Add("D");
                                if (useOptionE == true)
                                    listOption.Add("E");
                                if (useOptionF == true)
                                    listOption.Add("F");
                                if (worksheet.Cells[row, 17].Value == null)
                                {
                                    validateLog += "&emsp;- Chưa nhập đáp án đúng.<br>";
                                    hasError = true;
                                }
                                else
                                {
                                    if (worksheet.Cells[row, 2].Value.ToString().Trim().ToLower() == "một đáp án")
                                    {
                                        if (listOption.FirstOrDefault(x => x == worksheet.Cells[row, 17].Value.ToString().Trim().ToUpper()) == null)
                                        {
                                            validateLog += "&emsp;- Đáp án đúng chỉ chấp nhận các giá trị ";
                                            for (int i = 0; i < listOption.Count; i++)
                                            {
                                                validateLog += "'" + listOption.ElementAt(i) + "'";
                                                if (i != listOption.Count - 1)
                                                    validateLog += ", hoặc ";
                                            }
                                            validateLog += " .<br>";
                                            hasError = true;
                                        }
                                    }
                                    else if (worksheet.Cells[row, 2].Value.ToString().Trim().ToLower() == "nhiều đáp án")
                                    {
                                        string[] arr = worksheet.Cells[row, 17].Value.ToString().Trim().Split(",");
                                        for (int j = 0; j < arr.Length; j++)
                                        {
                                            if (listOption.FirstOrDefault(x => x == arr[j].Trim()) == null)
                                            {
                                                validateLog += "&emsp;- Đáp án đúng chỉ chấp nhận các giá trị ";
                                                for (int i = 0; i < listOption.Count; i++)
                                                {
                                                    validateLog += "'" + listOption.ElementAt(i) + "'";
                                                    if (i != listOption.Count - 1)
                                                        validateLog += ", ";
                                                }
                                                validateLog += " cách nhau bằng dấu ','.<br>";
                                                hasError = true;
                                            }
                                        }
                                    }
                                    else if (worksheet.Cells[row, 2].Value.ToString().Trim().ToLower() == "đúng sai")
                                    {
                                        if (worksheet.Cells[row, 17].Value.ToString().Trim().ToUpper() != "A" && worksheet.Cells[row, 17].Value.ToString().Trim().ToUpper() != "B")
                                        {
                                            validateLog += "&emsp;- Đáp án đúng chỉ chấp nhận các giá trị 'A' hoặc 'B'.<br>";
                                            hasError = true;
                                        }
                                    }
                                }
                                if (hasError == true)
                                    errorLog += validateLog;
                            }

                            //log
                            string title = "<b>Nội dung câu hỏi:</b><title>" +
                                "<b>Tệp âm thanh, video:</b><title>" +
                                "<b>Số điểm:</b><title>" +
                                "<b>Độ khó:</b><title>" +
                                "<b>Trộn đáp án:</b><title>" +
                                "<b>Chia cột đáp án:</b><title>" +
                                "<b>Trạng thái:</b><title>" +
                                "<b>Loại câu hỏi:</b><title>" +
                                "<b>Thuộc học phần:</b><title>" +
                                "<b>Thuộc đoạn văn:</b><title>" +
                                "<b>Lời giải:</b><title>" +
                                "<b>Đáp án:</b>";
                            string multiInsertLog = "";

                            if (hasError == false)
                            {
                                //add data
                                for (int row = 2; row <= rowCount; row++)
                                {
                                    //insert data to DB
                                    //question
                                    Question questionInsert = new Question();
                                    if (worksheet.Cells[row, 1].Value != null)
                                    {
                                        questionInsert.QUE_CONTENT = worksheet.Cells[row, 1].Value.ToString().Trim();
                                    }
                                    questionInsert.QUE_ANTYID = _context.AnswerTypes.
                                        FirstOrDefault(x => x.ANTY_NAME.ToLower() == worksheet.Cells[row, 2].Value.ToString().Trim().ToLower()).Id;
                                    questionInsert.QUE_SCORE = double.Parse(worksheet.Cells[row, 3].Value.ToString().Trim());
                                    questionInsert.QUE_LEVEL = ((worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() == "dễ" || worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() == "1") ?
                                        1 : ((worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() == "trung bình dễ" || worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() == "2") ?
                                        2 : ((worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() == "trung bình" || worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() == "3") ?
                                        3 : ((worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() == "trung bình khó" || worksheet.Cells[row, 4].Value.ToString().Trim().ToLower() == "4") ?
                                        4 : 5))));
                                    questionInsert.QUE_OPTCOLUMN = (worksheet.Cells[row, 5].Value == null ? 1 : int.Parse(worksheet.Cells[row, 5].Value.ToString().Trim()));
                                    questionInsert.QUE_ISSHUFFLE = (worksheet.Cells[row, 6].Value.ToString().Trim().ToLower() == "có" ? true : false);
                                    if (worksheet.Cells[row, 7].Value != null)
                                    {
                                        questionInsert.QUE_REFERENCE = worksheet.Cells[row, 7].Value.ToString().Trim();
                                    }
                                    questionInsert.QUE_ISDELETED = false;
                                    questionInsert.QUE_CREATEDBY = user;
                                    questionInsert.QUE_CREATEDDATE = DateTime.Now;
                                    questionInsert.QUE_STATUS = 1;
                                    if (passageId != 0)
                                    {
                                        questionInsert.QUE_PASID = passageId;
                                    }
                                    else
                                    {
                                        questionInsert.QUE_PARID = partId;
                                    }

                                    _context.Questions.Add(questionInsert);
                                    await _context.SaveChangesAsync();

                                    //option
                                    if (worksheet.Cells[row, 2].Value.ToString().Trim().ToLower() == "một đáp án")
                                    {
                                        for (int i = 0; i < listOption.Count; i++)
                                        {
                                            if (i == 0) //option A
                                            {
                                                Option optionInsert = new Option();
                                                if (worksheet.Cells[row, 8].Value != null)
                                                {
                                                    optionInsert.OPT_CONTENT = worksheet.Cells[row, 8].Value.ToString().Trim();
                                                }
                                                optionInsert.OPT_STATUS = 1;
                                                optionInsert.OPT_ISDELETED = false;
                                                optionInsert.OPT_CREATEDBY = user;
                                                optionInsert.OPT_CREATEDDATE = DateTime.Now;
                                                optionInsert.OPT_QUEID = questionInsert.Id;
                                                if (worksheet.Cells[row, 17].Value.ToString().Trim().ToUpper() == "A")
                                                    optionInsert.OPT_ISCORRECT = true;
                                                else
                                                    optionInsert.OPT_ISCORRECT = false;

                                                _context.Options.Add(optionInsert);
                                                await _context.SaveChangesAsync();
                                            }
                                            else if (i == 1) //option B
                                            {
                                                Option optionInsert = new Option();
                                                if (worksheet.Cells[row, 9].Value != null)
                                                {
                                                    optionInsert.OPT_CONTENT = worksheet.Cells[row, 9].Value.ToString().Trim();
                                                }
                                                optionInsert.OPT_STATUS = 1;
                                                optionInsert.OPT_ISDELETED = false;
                                                optionInsert.OPT_CREATEDBY = user;
                                                optionInsert.OPT_CREATEDDATE = DateTime.Now;
                                                optionInsert.OPT_QUEID = questionInsert.Id;
                                                if (worksheet.Cells[row, 17].Value.ToString().Trim().ToUpper() == "B")
                                                    optionInsert.OPT_ISCORRECT = true;
                                                else
                                                    optionInsert.OPT_ISCORRECT = false;

                                                _context.Options.Add(optionInsert);
                                                await _context.SaveChangesAsync();
                                            }
                                            else if (i == 2) //option C
                                            {
                                                Option optionInsert = new Option();
                                                if (worksheet.Cells[row, 10].Value != null)
                                                {
                                                    optionInsert.OPT_CONTENT = worksheet.Cells[row, 10].Value.ToString().Trim();
                                                }
                                                optionInsert.OPT_STATUS = 1;
                                                optionInsert.OPT_ISDELETED = false;
                                                optionInsert.OPT_CREATEDBY = user;
                                                optionInsert.OPT_CREATEDDATE = DateTime.Now;
                                                optionInsert.OPT_QUEID = questionInsert.Id;
                                                if (worksheet.Cells[row, 17].Value.ToString().Trim().ToUpper() == "C")
                                                    optionInsert.OPT_ISCORRECT = true;
                                                else
                                                    optionInsert.OPT_ISCORRECT = false;

                                                _context.Options.Add(optionInsert);
                                                await _context.SaveChangesAsync();
                                            }
                                            else if (i >= 3) //option D
                                            {
                                                if (worksheet.Cells[row, 11].Value.ToString().Trim().ToLower() == "có")
                                                {
                                                    Option optionInsert = new Option();
                                                    if (worksheet.Cells[row, 12].Value != null)
                                                    {
                                                        optionInsert.OPT_CONTENT = worksheet.Cells[row, 12].Value.ToString().Trim();
                                                    }
                                                    optionInsert.OPT_STATUS = 1;
                                                    optionInsert.OPT_ISDELETED = false;
                                                    optionInsert.OPT_CREATEDBY = user;
                                                    optionInsert.OPT_CREATEDDATE = DateTime.Now;
                                                    optionInsert.OPT_QUEID = questionInsert.Id;
                                                    if (worksheet.Cells[row, 17].Value.ToString().Trim().ToUpper() == "D")
                                                        optionInsert.OPT_ISCORRECT = true;
                                                    else
                                                        optionInsert.OPT_ISCORRECT = false;

                                                    _context.Options.Add(optionInsert);
                                                    await _context.SaveChangesAsync();
                                                }
                                                //option E
                                                else if (worksheet.Cells[row, 13].Value.ToString().Trim().ToLower() == "có")
                                                {
                                                    Option optionInsert = new Option();
                                                    if (worksheet.Cells[row, 14].Value != null)
                                                    {
                                                        optionInsert.OPT_CONTENT = worksheet.Cells[row, 14].Value.ToString().Trim();
                                                    }
                                                    optionInsert.OPT_STATUS = 1;
                                                    optionInsert.OPT_ISDELETED = false;
                                                    optionInsert.OPT_CREATEDBY = user;
                                                    optionInsert.OPT_CREATEDDATE = DateTime.Now;
                                                    optionInsert.OPT_QUEID = questionInsert.Id;
                                                    if (worksheet.Cells[row, 17].Value.ToString().Trim().ToUpper() == "E")
                                                        optionInsert.OPT_ISCORRECT = true;
                                                    else
                                                        optionInsert.OPT_ISCORRECT = false;

                                                    _context.Options.Add(optionInsert);
                                                    await _context.SaveChangesAsync();
                                                }
                                                //option F
                                                else if (worksheet.Cells[row, 15].Value.ToString().Trim().ToLower() == "có")
                                                {
                                                    Option optionInsert = new Option();
                                                    if (worksheet.Cells[row, 16].Value != null)
                                                    {
                                                        optionInsert.OPT_CONTENT = worksheet.Cells[row, 16].Value.ToString().Trim();
                                                    }
                                                    optionInsert.OPT_STATUS = 1;
                                                    optionInsert.OPT_ISDELETED = false;
                                                    optionInsert.OPT_CREATEDBY = user;
                                                    optionInsert.OPT_CREATEDDATE = DateTime.Now;
                                                    optionInsert.OPT_QUEID = questionInsert.Id;
                                                    if (worksheet.Cells[row, 17].Value.ToString().Trim().ToUpper() == "F")
                                                        optionInsert.OPT_ISCORRECT = true;
                                                    else
                                                        optionInsert.OPT_ISCORRECT = false;

                                                    _context.Options.Add(optionInsert);
                                                    await _context.SaveChangesAsync();
                                                }
                                            }
                                        }
                                    }
                                    else if (worksheet.Cells[row, 2].Value.ToString().Trim().ToLower() == "nhiều đáp án")
                                    {
                                        for (int i = 0; i < listOption.Count; i++)
                                        {
                                            if (i == 0) //option A
                                            {
                                                Option optionInsert = new Option();
                                                if (worksheet.Cells[row, 8].Value != null)
                                                {
                                                    optionInsert.OPT_CONTENT = worksheet.Cells[row, 8].Value.ToString().Trim();
                                                }
                                                optionInsert.OPT_STATUS = 1;
                                                optionInsert.OPT_ISDELETED = false;
                                                optionInsert.OPT_CREATEDBY = user;
                                                optionInsert.OPT_CREATEDDATE = DateTime.Now;
                                                optionInsert.OPT_QUEID = questionInsert.Id;
                                                if (worksheet.Cells[row, 17].Value.ToString().Trim().Split(",").FirstOrDefault(x => x.Trim().ToUpper() == "A") != null)
                                                    optionInsert.OPT_ISCORRECT = true;
                                                else
                                                    optionInsert.OPT_ISCORRECT = false;

                                                _context.Options.Add(optionInsert);
                                                await _context.SaveChangesAsync();
                                            }
                                            else if (i == 1) //option B
                                            {
                                                Option optionInsert = new Option();
                                                if (worksheet.Cells[row, 9].Value != null)
                                                {
                                                    optionInsert.OPT_CONTENT = worksheet.Cells[row, 9].Value.ToString().Trim();
                                                }
                                                optionInsert.OPT_STATUS = 1;
                                                optionInsert.OPT_ISDELETED = false;
                                                optionInsert.OPT_CREATEDBY = user;
                                                optionInsert.OPT_CREATEDDATE = DateTime.Now;
                                                optionInsert.OPT_QUEID = questionInsert.Id;
                                                if (worksheet.Cells[row, 17].Value.ToString().Trim().Split(",").FirstOrDefault(x => x.Trim().ToUpper() == "B") != null)
                                                    optionInsert.OPT_ISCORRECT = true;
                                                else
                                                    optionInsert.OPT_ISCORRECT = false;

                                                _context.Options.Add(optionInsert);
                                                await _context.SaveChangesAsync();
                                            }
                                            else if (i == 2) //option C
                                            {
                                                Option optionInsert = new Option();
                                                if (worksheet.Cells[row, 10].Value != null)
                                                {
                                                    optionInsert.OPT_CONTENT = worksheet.Cells[row, 10].Value.ToString().Trim();
                                                }
                                                optionInsert.OPT_STATUS = 1;
                                                optionInsert.OPT_ISDELETED = false;
                                                optionInsert.OPT_CREATEDBY = user;
                                                optionInsert.OPT_CREATEDDATE = DateTime.Now;
                                                optionInsert.OPT_QUEID = questionInsert.Id;
                                                if (worksheet.Cells[row, 17].Value.ToString().Trim().Split(",").FirstOrDefault(x => x.Trim().ToUpper() == "C") != null)
                                                    optionInsert.OPT_ISCORRECT = true;
                                                else
                                                    optionInsert.OPT_ISCORRECT = false;

                                                _context.Options.Add(optionInsert);
                                                await _context.SaveChangesAsync();
                                            }
                                            else if (i == 3) //option D
                                            {
                                                if (worksheet.Cells[row, 11].Value.ToString().Trim().ToLower() == "có")
                                                {
                                                    Option optionInsert = new Option();
                                                    if (worksheet.Cells[row, 12].Value != null)
                                                    {
                                                        optionInsert.OPT_CONTENT = worksheet.Cells[row, 12].Value.ToString().Trim();
                                                    }
                                                    optionInsert.OPT_STATUS = 1;
                                                    optionInsert.OPT_ISDELETED = false;
                                                    optionInsert.OPT_CREATEDBY = user;
                                                    optionInsert.OPT_CREATEDDATE = DateTime.Now;
                                                    optionInsert.OPT_QUEID = questionInsert.Id;
                                                    if (worksheet.Cells[row, 17].Value.ToString().Trim().Split(",").FirstOrDefault(x => x.Trim().ToUpper() == "D") != null)
                                                        optionInsert.OPT_ISCORRECT = true;
                                                    else
                                                        optionInsert.OPT_ISCORRECT = false;

                                                    _context.Options.Add(optionInsert);
                                                    await _context.SaveChangesAsync();
                                                }
                                            }
                                            else if (i == 4) //option E
                                            {
                                                if (worksheet.Cells[row, 13].Value.ToString().Trim().ToLower() == "có")
                                                {
                                                    Option optionInsert = new Option();
                                                    if (worksheet.Cells[row, 14].Value != null)
                                                    {
                                                        optionInsert.OPT_CONTENT = worksheet.Cells[row, 14].Value.ToString().Trim();
                                                    }
                                                    optionInsert.OPT_STATUS = 1;
                                                    optionInsert.OPT_ISDELETED = false;
                                                    optionInsert.OPT_CREATEDBY = user;
                                                    optionInsert.OPT_CREATEDDATE = DateTime.Now;
                                                    optionInsert.OPT_QUEID = questionInsert.Id;
                                                    if (worksheet.Cells[row, 17].Value.ToString().Trim().Split(",").FirstOrDefault(x => x.Trim().ToUpper() == "E") != null)
                                                        optionInsert.OPT_ISCORRECT = true;
                                                    else
                                                        optionInsert.OPT_ISCORRECT = false;

                                                    _context.Options.Add(optionInsert);
                                                    await _context.SaveChangesAsync();
                                                }
                                            }
                                            else if (i == 5) //option F
                                            {
                                                if (worksheet.Cells[row, 15].Value.ToString().Trim().ToLower() == "có")
                                                {
                                                    Option optionInsert = new Option();
                                                    if (worksheet.Cells[row, 16].Value != null)
                                                    {
                                                        optionInsert.OPT_CONTENT = worksheet.Cells[row, 16].Value.ToString().Trim();
                                                    }
                                                    optionInsert.OPT_STATUS = 1;
                                                    optionInsert.OPT_ISDELETED = false;
                                                    optionInsert.OPT_CREATEDBY = user;
                                                    optionInsert.OPT_CREATEDDATE = DateTime.Now;
                                                    optionInsert.OPT_QUEID = questionInsert.Id;
                                                    if (worksheet.Cells[row, 17].Value.ToString().Trim().Split(",").FirstOrDefault(x => x.Trim().ToUpper() == "F") != null)
                                                        optionInsert.OPT_ISCORRECT = true;
                                                    else
                                                        optionInsert.OPT_ISCORRECT = false;

                                                    _context.Options.Add(optionInsert);
                                                    await _context.SaveChangesAsync();
                                                }
                                            }
                                        }
                                    }
                                    else if (worksheet.Cells[row, 2].Value.ToString().Trim().ToLower() == "đúng sai")
                                    {
                                        //option A
                                        Option optionInsert = new Option();
                                        optionInsert.OPT_CONTENT = "Đúng";
                                        optionInsert.OPT_STATUS = 1;
                                        optionInsert.OPT_ISDELETED = false;
                                        optionInsert.OPT_CREATEDBY = user;
                                        optionInsert.OPT_CREATEDDATE = DateTime.Now;
                                        optionInsert.OPT_QUEID = questionInsert.Id;
                                        if (worksheet.Cells[row, 17].Value.ToString().Trim().ToUpper() == "A")
                                            optionInsert.OPT_ISCORRECT = true;
                                        else
                                            optionInsert.OPT_ISCORRECT = false;

                                        _context.Options.Add(optionInsert);
                                        await _context.SaveChangesAsync();

                                        //option B
                                        Option optionInsert1 = new Option();
                                        optionInsert1.OPT_CONTENT = "Sai";
                                        optionInsert1.OPT_STATUS = 1;
                                        optionInsert1.OPT_ISDELETED = false;
                                        optionInsert1.OPT_CREATEDBY = user;
                                        optionInsert1.OPT_CREATEDDATE = DateTime.Now;
                                        optionInsert1.OPT_QUEID = questionInsert.Id;
                                        if (worksheet.Cells[row, 17].Value.ToString().Trim().ToUpper() == "B")
                                            optionInsert1.OPT_ISCORRECT = true;
                                        else
                                            optionInsert1.OPT_ISCORRECT = false;

                                        _context.Options.Add(optionInsert1);
                                        await _context.SaveChangesAsync();
                                    }

                                    //log write
                                    multiInsertLog += questionInsert.QUE_CONTENT + "<item>"
                                        + "<item>"
                                        + questionInsert.QUE_SCORE + "<item>"
                                        + questionInsert.QUE_LEVEL + "<item>"
                                        + (questionInsert.QUE_ISSHUFFLE == true ? "Có" : "Không") + "<item>"
                                        + (questionInsert.QUE_OPTCOLUMN != null ? questionInsert.QUE_OPTCOLUMN + "cột" : "") + "<item>"
                                        + (questionInsert.QUE_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>"
                                        + (questionInsert.QUE_ANTYID != null ? _context.AnswerTypes.FirstOrDefault(x => x.Id == questionInsert.QUE_ANTYID).ANTY_NAME : "") + "<item>"
                                        + (questionInsert.QUE_PARID != null ? _context.Parts.FirstOrDefault(x => x.Id == questionInsert.QUE_PARID).PAR_NAME : "") + "<item>"
                                        + (questionInsert.QUE_PASID != null && questionInsert.QUE_PASID != 0 ? "Đoạn văn " + _context.Passages.FirstOrDefault(x => x.Id == questionInsert.QUE_PASID).PAS_ORDER : "") + "<item>"
                                        + questionInsert.QUE_REFERENCE + "<item>";

                                    List<Option> listOptionLog = await _context.Options.Where(x => x.OPT_QUEID == questionInsert.Id).ToListAsync();
                                    for (int i = 0; i < listOptionLog.Count; i++)
                                    {
                                        if (i == 0)
                                            multiInsertLog += "A. ";
                                        if (i == 1)
                                            multiInsertLog += "B. ";
                                        if (i == 2)
                                            multiInsertLog += "C. ";
                                        if (i == 3)
                                            multiInsertLog += "D. ";
                                        if (i == 4)
                                            multiInsertLog += "E. ";
                                        if (i == 5)
                                            multiInsertLog += "F. ";
                                        multiInsertLog += listOptionLog.ElementAt(i).OPT_CONTENT;
                                        if (i != listOptionLog.Count - 1)
                                        {
                                            multiInsertLog += ", ";
                                        }
                                    }

                                    if (row != rowCount)
                                        multiInsertLog += "<delete>";

                                }

                                Log log = new Log();
                                log.LOG_ACTID = 1;
                                log.LOG_TABID = 1;
                                log.LOG_MODULE = "Thêm danh sách câu hỏi";
                                log.LOG_USEID = user;
                                log.LOG_DATE = DateTime.Now;
                                log.LOG_CONTENT = title + "<divider>" + multiInsertLog;
                                _context.Logs.Add(log);
                                await _context.SaveChangesAsync();

                                transaction.Commit();

                                //real time reload
                                await _hubContext.Clients.All.SendAsync("GetLog", true);
                                await _hubContext.Clients.All.SendAsync("GetQuestion", true);
                            }
                            else
                                return new BaseResponse
                                {
                                    errorCode = 405,
                                    message = "Dữ liệu không hợp lệ!<divider>" + errorLog
                                };
                        }
                        catch (Exception)
                        {
                            return new BaseResponse
                            {
                                errorCode = 500,
                                message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                            };
                        }
                    }
                }
            }

            return new BaseResponse { };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(QuestionRequest questionRequest)
        {
            if (questionRequest.question.QUE_PASID != 0 && questionRequest.question.QUE_PASID != null)
            {
                questionRequest.question.QUE_PARID = null;
            }
            else
            {
                questionRequest.question.QUE_PASID = null;
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = questionRequest.question.QUE_CREATEDBY.GetValueOrDefault();
            log.LOG_MODULE = "Thêm câu hỏi";
            log.LOG_ACTID = 1;
            log.LOG_TABID = 1;
            log.LOG_CONTENT = "";
            string title = "<b>Nội dung câu hỏi:</b><title>" +
                "<b>Tệp âm thanh, video:</b><title>" +
                "<b>Số điểm:</b><title>" +
                "<b>Độ khó:</b><title>" +
                "<b>Trộn đáp án:</b><title>" +
                "<b>Chia cột đáp án:</b><title>" +
                "<b>Trạng thái:</b><title>" +
                "<b>Loại câu hỏi:</b><title>" +
                "<b>Thuộc học phần:</b><title>" +
                "<b>Thuộc đoạn văn:</b><title>" +
                "<b>Lời giải:</b><title>" +
                "<b>Đáp án:</b>";
            string insertLog = questionRequest.question.QUE_CONTENT + "<item>"
                + (questionRequest.question.QUE_MEDIA != null ? questionRequest.question.QUE_MEDIA : "") + "<item>"
                + questionRequest.question.QUE_SCORE + "<item>"
                + questionRequest.question.QUE_LEVEL + "<item>"
                + (questionRequest.question.QUE_ISSHUFFLE == true ? "Có" : "Không") + "<item>"
                + (questionRequest.question.QUE_OPTCOLUMN != null ? questionRequest.question.QUE_OPTCOLUMN + "cột" : "") + "<item>"
                + (questionRequest.question.QUE_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>"
                + (questionRequest.question.QUE_ANTYID != null ? _context.AnswerTypes.FirstOrDefault(x => x.Id == questionRequest.question.QUE_ANTYID).ANTY_NAME : "") + "<item>"
                + (questionRequest.question.QUE_PARID != null ? _context.Parts.FirstOrDefault(x => x.Id == questionRequest.question.QUE_PARID).PAR_NAME : "") + "<item>"
                + (questionRequest.question.QUE_PASID != null && questionRequest.question.QUE_PASID != 0 ? "Đoạn văn " + _context.Passages.FirstOrDefault(x => x.Id == questionRequest.question.QUE_PASID).PAS_ORDER : "") + "<item>"
                + questionRequest.question.QUE_REFERENCE + "<item>";

            questionRequest.question.QUE_CREATEDDATE = DateTime.Now;
            questionRequest.question.QUE_ISDELETED = false;
            if (questionRequest.question.QUE_OPTCOLUMN == null)
                questionRequest.question.QUE_OPTCOLUMN = 1;
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    _context.Questions.Add(questionRequest.question);
                    await _context.SaveChangesAsync();

                    for (int i = 0; i < questionRequest.listOption.Length; i++)
                    {
                        if (questionRequest.question.QUE_ANTYID == 1 || questionRequest.question.QUE_ANTYID == 3)
                        {
                            insertLog += (questionRequest.listOption[i].OPT_ISCORRECT == true ? "<i class='fa fa-dot-circle-o'></i>" : "<i class='fa fa-circle-o'></i>") + "<slash>" + questionRequest.listOption[i].OPT_CONTENT;
                        }
                        else if (questionRequest.question.QUE_ANTYID == 2)
                        {
                            insertLog += (questionRequest.listOption[i].OPT_ISCORRECT == true ? "<i class='fa fa-check-square-o'></i>" : "<i class='fa fa-square-o'></i>") + "<slash>" + questionRequest.listOption[i].OPT_CONTENT;
                        }
                        if (i != questionRequest.listOption.Length - 1)
                            insertLog += "<opt>";

                        questionRequest.listOption[i].OPT_CREATEDBY = questionRequest.question.QUE_CREATEDBY;
                        questionRequest.listOption[i].OPT_CREATEDDATE = DateTime.Now;
                        questionRequest.listOption[i].OPT_ISDELETED = false;
                        questionRequest.listOption[i].OPT_STATUS = 1;
                        questionRequest.listOption[i].OPT_QUEID = questionRequest.question.Id;
                        _context.Options.Add(questionRequest.listOption[i]);
                        await _context.SaveChangesAsync();
                    }

                    log.LOG_CONTENT += title + "<divider>" + insertLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();
                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetQuestion", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse
            {
                data = questionRequest.question
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(long id, QuestionRequest questionRequest)
        {
            Question queDB = await _context.Questions.Include(x => x.Options).Include(x => x.Test_Questions).
                FirstOrDefaultAsync(x => x.Id == id);

            if (queDB == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (queDB.Test_Questions.Count != 0)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Câu hỏi đã được sử dụng, không thể chỉnh sửa!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = questionRequest.question.QUE_CREATEDBY.GetValueOrDefault();
            log.LOG_ACTID = 2;
            log.LOG_TABID = 1;
            log.LOG_MODULE = "Sửa câu hỏi";
            log.LOG_CONTENT = "";
            //log title
            string title = "<b>Nội dung câu hỏi:</b><title>" +
                "<b>Tệp âm thanh, video:</b><title>" +
                "<b>Số điểm:</b><title>" +
                "<b>Độ khó:</b><title>" +
                "<b>Trộn đáp án:</b><title>" +
                "<b>Chia cột đáp án:</b><title>" +
                "<b>Trạng thái:</b><title>" +
                "<b>Loại câu hỏi:</b><title>" +
                "<b>Thuộc học phần:</b><title>" +
                "<b>Thuộc đoạn văn:</b><title>" +
                "<b>Lời giải:</b><title>" +
                "<b>Đáp án:</b>";

            string updateLog = queDB.QUE_CONTENT + "<to>" + questionRequest.question.QUE_CONTENT + "<item>"
                + (queDB.QUE_MEDIA != null ? queDB.QUE_MEDIA : "") + "<to>" + (questionRequest.question.QUE_MEDIA != null ? questionRequest.question.QUE_MEDIA : "") + "<item>"
                + queDB.QUE_SCORE + "<to>" + questionRequest.question.QUE_SCORE + "<item>"
                + queDB.QUE_LEVEL + "<to>" + questionRequest.question.QUE_LEVEL + "<item>"
                + (queDB.QUE_ISSHUFFLE == true ? "Có" : "Không") + "<to>" + (questionRequest.question.QUE_ISSHUFFLE == true ? "Có" : "Không") + "<item>"
                + (queDB.QUE_OPTCOLUMN != null ? queDB.QUE_OPTCOLUMN + "cột" : "") + "<to>" + (questionRequest.question.QUE_OPTCOLUMN != null ? questionRequest.question.QUE_OPTCOLUMN + "cột" : "") + "<item>"
                + (queDB.QUE_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<to>" + (questionRequest.question.QUE_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>"
                + (queDB.QUE_ANTYID != null ? _context.AnswerTypes.FirstOrDefault(x => x.Id == queDB.QUE_ANTYID).ANTY_NAME : "") + "<to>" + (questionRequest.question.QUE_ANTYID != null ? _context.AnswerTypes.FirstOrDefault(x => x.Id == questionRequest.question.QUE_ANTYID).ANTY_NAME : "") + "<item>"
                + (queDB.QUE_PARID != null ? _context.Parts.FirstOrDefault(x => x.Id == queDB.QUE_PARID).PAR_NAME : _context.Passages.Include(x => x.Parts).FirstOrDefault(x => x.Id == queDB.QUE_PASID).Parts.PAR_NAME) + "<to>" + (questionRequest.question.QUE_PARID != null ? _context.Parts.FirstOrDefault(x => x.Id == questionRequest.question.QUE_PARID).PAR_NAME : "") + "<item>"
                + (queDB.QUE_PASID != null ? "Đoạn văn " + _context.Passages.FirstOrDefault(x => x.Id == queDB.QUE_PASID).PAS_ORDER : "") + "<to>" + (questionRequest.question.QUE_PASID != null && questionRequest.question.QUE_PASID != 0 ? "Đoạn văn " + _context.Passages.FirstOrDefault(x => x.Id == questionRequest.question.QUE_PASID).PAS_ORDER : "") + "<item>"
                + queDB.QUE_REFERENCE + "<to>" + questionRequest.question.QUE_REFERENCE + "<item>";

            if (questionRequest.question.QUE_PASID != 0 && questionRequest.question.QUE_PASID != null)
            {
                questionRequest.question.QUE_PARID = null;
            }
            else
            {
                questionRequest.question.QUE_PASID = null;
            }

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    queDB.QUE_CONTENT = questionRequest.question.QUE_CONTENT;
                    queDB.QUE_MEDIA = questionRequest.question.QUE_MEDIA;
                    queDB.QUE_SCORE = questionRequest.question.QUE_SCORE;
                    queDB.QUE_LEVEL = questionRequest.question.QUE_LEVEL;
                    queDB.QUE_ISSHUFFLE = questionRequest.question.QUE_ISSHUFFLE;
                    if (questionRequest.question.QUE_OPTCOLUMN != null)
                    {
                        queDB.QUE_OPTCOLUMN = questionRequest.question.QUE_OPTCOLUMN;
                    }
                    queDB.QUE_STATUS = questionRequest.question.QUE_STATUS;
                    queDB.QUE_REFERENCE = questionRequest.question.QUE_REFERENCE;
                    queDB.QUE_PARID = questionRequest.question.QUE_PARID;
                    queDB.QUE_PASID = questionRequest.question.QUE_PASID;
                    queDB.QUE_MODIFIEDBY = questionRequest.question.QUE_MODIFIEDBY;
                    queDB.QUE_MODIFIEDDATE = DateTime.Now;
                    await _context.SaveChangesAsync();

                    if (queDB.Options.Count == questionRequest.listOption.Length)
                    {
                        for (int i = 0; i < queDB.Options.Count; i++)
                        {
                            if (queDB.QUE_ANTYID == 1 || queDB.QUE_ANTYID == 3)
                            {
                                updateLog += queDB.Options.ElementAt(i).OPT_ISCORRECT == true ? "<i class='fa fa-dot-circle-o'></i><slash>" : "<i class='fa fa-circle-o'></i><slash>" + queDB.Options.ElementAt(i).OPT_CONTENT
                                    + "<to>"
                                    + (questionRequest.listOption[i].OPT_ISCORRECT == true ? "<i class='fa fa-dot-circle-o'></i><slash>" : "<i class='fa fa-circle-o'></i><slash>") + questionRequest.listOption[i].OPT_CONTENT;
                            }
                            else if (queDB.QUE_ANTYID == 2)
                            {
                                updateLog += queDB.Options.ElementAt(i).OPT_ISCORRECT == true ? "<i class='fa fa-check-square-o'></i><slash>" : "<i class='fa fa-square-o'></i><slash>" + queDB.Options.ElementAt(i).OPT_CONTENT
                                    + "<to>"
                                    + (questionRequest.listOption[i].OPT_ISCORRECT == true ? "<i class='fa fa-check-square-o'></i><slash>" : "<i class='fa fa-square-o'></i><slash>") + questionRequest.listOption[i].OPT_CONTENT;
                            }

                            if (i != queDB.Options.Count - 1)
                                updateLog += "<opt>";

                            queDB.Options.ElementAt(i).OPT_CONTENT = questionRequest.listOption[i].OPT_CONTENT;
                            queDB.Options.ElementAt(i).OPT_ISCORRECT = questionRequest.listOption[i].OPT_ISCORRECT;
                            queDB.Options.ElementAt(i).OPT_MODIFIEDBY = questionRequest.question.QUE_MODIFIEDBY;
                            queDB.Options.ElementAt(i).OPT_MODIFIEDDATE = DateTime.Now;
                            await _context.SaveChangesAsync();
                        }
                    }
                    else
                    {
                        //delete
                        if (queDB.Options.Count > questionRequest.listOption.Length)
                        {
                            for (int i = 0; i < queDB.Options.Count; i++)
                            {
                                if ((i + 1) <= questionRequest.listOption.Length)
                                {
                                    if (queDB.QUE_ANTYID == 1 || queDB.QUE_ANTYID == 3)
                                    {
                                        updateLog += queDB.Options.ElementAt(i).OPT_ISCORRECT == true ? "<i class='fa fa-dot-circle-o'></i><slash>" : "<i class='fa fa-circle-o'></i><slash>" + queDB.Options.ElementAt(i).OPT_CONTENT
                                            + "<to>"
                                            + (questionRequest.listOption[i].OPT_ISCORRECT == true ? "<i class='fa fa-dot-circle-o'></i><slash>" : "<i class='fa fa-circle-o'></i><slash>") + questionRequest.listOption[i].OPT_CONTENT;
                                    }
                                    else if (queDB.QUE_ANTYID == 2)
                                    {
                                        updateLog += queDB.Options.ElementAt(i).OPT_ISCORRECT == true ? "<i class='fa fa-check-square-o'></i><slash>" : "<i class='fa fa-square-o'></i><slash>" + queDB.Options.ElementAt(i).OPT_CONTENT
                                            + "<to>"
                                            + (questionRequest.listOption[i].OPT_ISCORRECT == true ? "<i class='fa fa-check-square-o'></i><slash>" : "<i class='fa fa-square-o'></i><slash>") + questionRequest.listOption[i].OPT_CONTENT;
                                    }

                                    queDB.Options.ElementAt(i).OPT_CONTENT = questionRequest.listOption[i].OPT_CONTENT;
                                    queDB.Options.ElementAt(i).OPT_ISCORRECT = questionRequest.listOption[i].OPT_ISCORRECT;
                                    queDB.Options.ElementAt(i).OPT_MODIFIEDBY = questionRequest.question.QUE_MODIFIEDBY;
                                    queDB.Options.ElementAt(i).OPT_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                                else
                                {
                                    if (queDB.QUE_ANTYID == 1 || queDB.QUE_ANTYID == 3)
                                    {
                                        updateLog += queDB.Options.ElementAt(i).OPT_ISCORRECT == true ? "<i class='fa fa-dot-circle-o'></i><slash>" : "<i class='fa fa-circle-o'></i><slash>" + queDB.Options.ElementAt(i).OPT_CONTENT
                                            + "<to>";
                                    }
                                    else if (queDB.QUE_ANTYID == 2)
                                    {
                                        updateLog += queDB.Options.ElementAt(i).OPT_ISCORRECT == true ? "<i class='fa fa-check-square-o'></i><slash>" : "<i class='fa fa-square-o'></i><slash>" + queDB.Options.ElementAt(i).OPT_CONTENT
                                            + "<to>";
                                    }
                                    _context.Options.Remove(queDB.Options.ElementAt(i));
                                    await _context.SaveChangesAsync();
                                }
                                if (i != queDB.Options.Count - 1)
                                    updateLog += "<opt>";
                            }
                        }
                        //insert
                        else
                        {
                            for (int i = 0; i < questionRequest.listOption.Length; i++)
                            {
                                if ((i + 1) <= queDB.Options.Count)
                                {
                                    if (queDB.QUE_ANTYID == 1 || queDB.QUE_ANTYID == 3)
                                    {
                                        updateLog += queDB.Options.ElementAt(i).OPT_ISCORRECT == true ? "<i class='fa fa-dot-circle-o'></i><slash>" : "<i class='fa fa-circle-o'></i><slash>" + queDB.Options.ElementAt(i).OPT_CONTENT
                                            + "<to>"
                                            + (questionRequest.listOption[i].OPT_ISCORRECT == true ? "<i class='fa fa-dot-circle-o'></i><slash>" : "<i class='fa fa-circle-o'></i><slash>") + questionRequest.listOption[i].OPT_CONTENT;
                                    }
                                    else if (queDB.QUE_ANTYID == 2)
                                    {
                                        updateLog += queDB.Options.ElementAt(i).OPT_ISCORRECT == true ? "<i class='fa fa-check-square-o'></i><slash>" : "<i class='fa fa-square-o'></i><slash>" + queDB.Options.ElementAt(i).OPT_CONTENT
                                            + "<to>"
                                            + (questionRequest.listOption[i].OPT_ISCORRECT == true ? "<i class='fa fa-check-square-o'></i><slash>" : "<i class='fa fa-square-o'></i><slash>") + questionRequest.listOption[i].OPT_CONTENT;
                                    }

                                    queDB.Options.ElementAt(i).OPT_CONTENT = questionRequest.listOption[i].OPT_CONTENT;
                                    queDB.Options.ElementAt(i).OPT_ISCORRECT = questionRequest.listOption[i].OPT_ISCORRECT;
                                    queDB.Options.ElementAt(i).OPT_MODIFIEDBY = questionRequest.question.QUE_MODIFIEDBY;
                                    queDB.Options.ElementAt(i).OPT_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                                else
                                {
                                    if (queDB.QUE_ANTYID == 1 || queDB.QUE_ANTYID == 3)
                                    {
                                        updateLog += "<to>"
                                            + (questionRequest.listOption[i].OPT_ISCORRECT == true ? "<i class='fa fa-dot-circle-o'></i><slash>" : "<i class='fa fa-circle-o'></i><slash>") + questionRequest.listOption[i].OPT_CONTENT;
                                    }
                                    else if (queDB.QUE_ANTYID == 2)
                                    {
                                        updateLog += "<to>"
                                            + (questionRequest.listOption[i].OPT_ISCORRECT == true ? "<i class='fa fa-check-square-o'></i><slash>" : "<i class='fa fa-square-o'></i><slash>") + questionRequest.listOption[i].OPT_CONTENT;
                                    }

                                    questionRequest.listOption[i].OPT_CREATEDBY = questionRequest.question.QUE_CREATEDBY;
                                    questionRequest.listOption[i].OPT_CREATEDDATE = DateTime.Now;
                                    questionRequest.listOption[i].OPT_ISDELETED = false;
                                    questionRequest.listOption[i].OPT_STATUS = 1;
                                    questionRequest.listOption[i].OPT_QUEID = questionRequest.question.Id;
                                    _context.Options.Add(questionRequest.listOption[i]);
                                    await _context.SaveChangesAsync();
                                }
                                if (i != queDB.Options.Count - 1)
                                    updateLog += "<opt>";
                            }
                        }
                    }

                    log.LOG_CONTENT = title + "<divider>" + updateLog;

                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();
                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetQuestion", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse
            {
                data = questionRequest.question
            };
        }
        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long id, long user)
        {
            Question questionDB = await _context.Questions.
                Include(x => x.Options).
                Include(x => x.Parts).
                Include(x => x.Passages).ThenInclude(x => x.Parts).
                Include(x => x.Test_Questions).
                FirstOrDefaultAsync(x => x.Id == id);

            if (questionDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 1;
            log.LOG_MODULE = "Xóa câu hỏi";
            log.LOG_CONTENT = "";

            string title = "<b>Nội dung câu hỏi:</b><title>" +
                "<b>Tệp âm thanh, video:</b><title>" +
                "<b>Số điểm:</b><title>" +
                "<b>Độ khó:</b><title>" +
                "<b>Trộn đáp án:</b><title>" +
                "<b>Chia cột đáp án:</b><title>" +
                "<b>Trạng thái:</b><title>" +
                "<b>Loại câu hỏi:</b><title>" +
                "<b>Thuộc học phần:</b><title>" +
                "<b>Thuộc đoạn văn:</b><title>" +
                "<b>Lời giải:</b><title>" +
                "<b>Đáp án:</b>";

            string deleteLog = questionDB.QUE_CONTENT + "<item>"
                + (questionDB.QUE_MEDIA != null ? questionDB.QUE_MEDIA : "") + "<item>"
                + questionDB.QUE_SCORE + "<item>"
                + questionDB.QUE_LEVEL + "<item>"
                + (questionDB.QUE_ISSHUFFLE == true ? "Có" : "Không") + "<item>"
                + (questionDB.QUE_OPTCOLUMN != null ? questionDB.QUE_OPTCOLUMN + "cột" : "") + "<item>"
                + (questionDB.QUE_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>"
                + (questionDB.QUE_ANTYID != null ? _context.AnswerTypes.FirstOrDefault(x => x.Id == questionDB.QUE_ANTYID).ANTY_NAME : "") + "<item>"
                + (questionDB.QUE_PARID != null ? questionDB.Parts.PAR_NAME : questionDB.Passages.Parts.PAR_NAME) + "<item>"
                + (questionDB.QUE_PASID != null && questionDB.QUE_PASID != 0 ? "Đoạn văn " + questionDB.Passages.PAS_ORDER : "") + "<item>"
                + questionDB.QUE_REFERENCE + "<item>";

            for (int i = 0; i < questionDB.Options.Count; i++)
            {
                if (questionDB.QUE_ANTYID == 1 || questionDB.QUE_ANTYID == 3)
                {
                    deleteLog += questionDB.Options.ElementAt(i).OPT_ISCORRECT == true ? "<i class='fa fa-dot-circle-o'></i><slash>" : "<i class='fa fa-circle-o'></i><slash>" + questionDB.Options.ElementAt(i).OPT_CONTENT;
                }
                else if (questionDB.QUE_ANTYID == 2)
                {
                    deleteLog += questionDB.Options.ElementAt(i).OPT_ISCORRECT == true ? "<i class='fa fa-check-square-o'></i><slash>" : "<i class='fa fa-square-o'></i><slash>" + questionDB.Options.ElementAt(i).OPT_CONTENT;
                }
                if (i != questionDB.Options.Count - 1)
                    deleteLog += "<opt>";
            }

            if (questionDB.Test_Questions.Count == 0 && questionDB.QUE_MEDIA != null)
            {
                new FileController(_hostingEnv).Delete("Question", questionDB.QUE_MEDIA.Split("/")[3]);
            }

            try
            {
                questionDB.QUE_MODIFIEDBY = user;
                questionDB.QUE_MODIFIEDDATE = DateTime.Now;
                questionDB.QUE_ISDELETED = true;
                await _context.SaveChangesAsync();

                log.LOG_CONTENT += title + "<divider>" + deleteLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetQuestion", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = questionDB
            };
        }
    }
}