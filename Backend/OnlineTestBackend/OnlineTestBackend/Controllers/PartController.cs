﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Hubs;
using Backend.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;

namespace OnlineTestBackend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PartController : BaseController
    {
        private readonly IHubContext<SignalHub> _hubContext;
        public PartController(OnlineTestBackendDBContext db, IHubContext<SignalHub> hubContext) : base(db)
        {
            _hubContext = hubContext;
        }
        [HttpGet]
        public async Task<ActionResult<BaseResponse>> Get()
        {
            return new BaseResponse
            {
                data = await _context.Parts.Include(x => x.Subjects).Where(x => x.PAR_ISDELETED == false).AsNoTracking().ToListAsync()
            };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> Get(long id)
        {
            var part = await _context.Parts.
                Include(x => x.Subjects).
                Include(x => x.Passages).
                Include(x => x.Questions).
                Include(x => x.UserCreated).ThenInclude(x => x.Employees).
                Include(x => x.UserModified).ThenInclude(x => x.Employees).
                Where(x => x.PAR_ISDELETED == false && x.Id == id).AsNoTracking().FirstOrDefaultAsync();
            if (part == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            if (part.UserCreated != null)
            {
                if (part.UserCreated.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    part.UserCreated.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    part.UserCreated.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + part.UserCreated.Employees.ElementAt(0).EMP_AVATAR;
                }
            }
            if (part.UserModified != null)
            {
                if (part.UserModified.Employees.ElementAt(0).EMP_AVATAR == null)
                {
                    part.UserModified.Employees.ElementAt(0).FileUrl = "assets/img/default_avatar.png";
                }
                else
                {
                    part.UserModified.Employees.ElementAt(0).FileUrl = Utils.Helper.GetBaseUrl(Request) + part.UserModified.Employees.ElementAt(0).EMP_AVATAR;
                }
            }

            if (part.Passages.Where(x => x.PAS_ISDELETED == false).ToList().Count == 0)
                part.Passages.Clear();

            if (part.Questions.Where(x => x.QUE_ISDELETED == false).ToList().Count == 0)
                part.Questions.Clear();

            return new BaseResponse
            {
                data = part
            };
        }
        [HttpGet("getListBySubjectId/{id}")]
        public async Task<ActionResult<BaseResponse>> getListBySubjectId(long id)
        {
            var partList = await _context.Parts.
                Include(x => x.Subjects).
                Include(x => x.Passages).
                Include(x => x.Questions).
                Where(x => x.PAR_ISDELETED == false && x.PAR_SUBID == id).AsNoTracking().ToListAsync();
            if (partList == null)
            {
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            }

            for (int i = 0; i < partList.Count; i++)
            {
                partList.ElementAt(i).countPassage = partList.ElementAt(i).Passages.Where(x => x.PAS_ISDELETED == false).ToList().Count;
                partList.ElementAt(i).countQuestion = partList.ElementAt(i).Questions.Where(x => x.QUE_ISDELETED == false).ToList().Count;
                partList.ElementAt(i).Passages.Clear();
                partList.ElementAt(i).Questions.Clear();
            }

            return new BaseResponse
            {
                data = partList
            };
        }
        [HttpPost]
        public async Task<ActionResult<BaseResponse>> Post(Part part)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = part.PAR_CREATEDBY.GetValueOrDefault();
            log.LOG_MODULE = "Thêm học phần";
            log.LOG_ACTID = 1;
            log.LOG_TABID = 3;
            string title = "<b>Tên học phần:</b><title>" +
                "<b>Số điểm:</b><title>" +
                "<b>Độ khó:</b><title>" +
                "<b>Hiển thị câu hỏi:</b><title>" +
                "<b>Trạng thái:</b><title>" +
                "<b>Trộn câu hỏi:</b><title>" +
                "<b>Cho phép trộn câu hỏi:</b><title>" +
                "<b>Cho phép trộn đáp án:</b><title>" +
                "<b>Cho phép tạo câu hỏi không có nội dung:</b><title>" +
                "<b>Cho phép tạo đáp án không có nội dung:</b><title>" +
                "<b>Ghi chú:</b><title>" +
                "<b>Hướng dẫn:</b>";

            string insertLog = part.PAR_NAME + "<item>"
                + part.PAR_DEFAULTSCORE + "<item>"
                + part.PAR_DEFAULTLEVEL + "<item>"
                + part.PAR_DEFAULTCOLUMN + "<item>"
                + (part.PAR_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>"
                + (part.PAR_ISSHUFFLE == true ? "Có" : "Không") + "<item>"
                + (part.PAR_ISALLOWSHUFFLEQUESTION == true ? "Có" : "Không") + "<item>"
                + (part.PAR_ISALLOWSHUFFLEOPTION == true ? "Có" : "Không") + "<item>"
                + (part.PAR_ISALLOWNULLQUESTION == true ? "Có" : "Không") + "<item>"
                + (part.PAR_ISALLOWNULLOPTION == true ? "Có" : "Không") + "<item>"
                + (part.PAR_NOTE == null ? "" : part.PAR_NOTE) + "<item>"
                + part.PAR_DIRECTION + "<item>";

            part.PAR_CREATEDDATE = DateTime.Now;
            part.PAR_ISDELETED = false;

            try
            {
                _context.Parts.Add(part);
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + insertLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetPart", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = part
            };
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<BaseResponse>> Put(long id, Part part)
        {
            Part partDB = await _context.Parts.Include(x => x.Questions).Include(x => x.Passages).
                FirstOrDefaultAsync(x => x.Id == id);

            if (partDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };
            if (partDB.Questions.Count > 0 || partDB.Passages.Count > 0)
            {
                return new BaseResponse
                {
                    errorCode = 405,
                    message = "Không thể chỉnh sửa, học phần đã được sử dụng!"
                };
            }

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = part.PAR_MODIFIEDBY.GetValueOrDefault();
            log.LOG_ACTID = 2;
            log.LOG_TABID = 3;
            log.LOG_MODULE = "Sửa học phần";

            string title = "<b>Tên học phần:</b><title>" +
                "<b>Số điểm:</b><title>" +
                "<b>Độ khó:</b><title>" +
                "<b>Hiển thị câu hỏi:</b><title>" +
                "<b>Trạng thái:</b><title>" +
                "<b>Trộn câu hỏi:</b><title>" +
                "<b>Cho phép trộn câu hỏi:</b><title>" +
                "<b>Cho phép trộn đáp án:</b><title>" +
                "<b>Cho phép tạo câu hỏi không có nội dung:</b><title>" +
                "<b>Cho phép tạo đáp án không có nội dung:</b><title>" +
                "<b>Ghi chú:</b><title>" +
                "<b>Hướng dẫn:</b>";

            string updateLog = partDB.PAR_NAME + "<to>" + part.PAR_NAME + "<item>"
                + partDB.PAR_DEFAULTSCORE + "<to>" + part.PAR_DEFAULTSCORE + "<item>"
                + partDB.PAR_DEFAULTLEVEL + "<to>" + part.PAR_DEFAULTLEVEL + "<item>"
                + partDB.PAR_DEFAULTCOLUMN + "<to>" + part.PAR_DEFAULTCOLUMN + "<item>"
                + (partDB.PAR_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<to>" + (part.PAR_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>"
                + (partDB.PAR_ISSHUFFLE == true ? "Có" : "Không") + "<to>" + (part.PAR_ISSHUFFLE == true ? "Có" : "Không") + "<item>"
                + (partDB.PAR_ISALLOWSHUFFLEQUESTION == true ? "Có" : "Không") + "<to>" + (part.PAR_ISALLOWSHUFFLEQUESTION == true ? "Có" : "Không") + "<item>"
                + (partDB.PAR_ISALLOWSHUFFLEOPTION == true ? "Có" : "Không") + "<to>" + (part.PAR_ISALLOWSHUFFLEOPTION == true ? "Có" : "Không") + "<item>"
                + (partDB.PAR_ISALLOWNULLQUESTION == true ? "Có" : "Không") + "<to>" + (part.PAR_ISALLOWNULLQUESTION == true ? "Có" : "Không") + "<item>"
                + (partDB.PAR_ISALLOWNULLOPTION == true ? "Có" : "Không") + "<to>" + (part.PAR_ISALLOWNULLOPTION == true ? "Có" : "Không") + "<item>"
                + (partDB.PAR_NOTE == null ? "" : partDB.PAR_NOTE) + "<to>" + (part.PAR_NOTE == null ? "" : part.PAR_NOTE) + "<item>"
                + partDB.PAR_DIRECTION + "<to>" + part.PAR_DIRECTION + "<item>";

            try
            {
                partDB.PAR_NAME = part.PAR_NAME;
                partDB.PAR_DEFAULTLEVEL = part.PAR_DEFAULTLEVEL;
                partDB.PAR_DEFAULTSCORE = part.PAR_DEFAULTSCORE;
                partDB.PAR_DEFAULTCOLUMN = part.PAR_DEFAULTCOLUMN;
                partDB.PAR_DIRECTION = part.PAR_DIRECTION;
                partDB.PAR_ISSHUFFLE = part.PAR_ISSHUFFLE;
                partDB.PAR_NOTE = part.PAR_NOTE;
                partDB.PAR_STATUS = part.PAR_STATUS;
                partDB.PAR_MODIFIEDBY = part.PAR_MODIFIEDBY;
                partDB.PAR_MODIFIEDDATE = DateTime.Now;
                partDB.PAR_SUBID = part.PAR_SUBID;
                await _context.SaveChangesAsync();

                log.LOG_CONTENT = title + "<divider>" + updateLog;
                _context.Logs.Add(log);
                await _context.SaveChangesAsync();

                await _hubContext.Clients.All.SendAsync("GetLog", true);
                await _hubContext.Clients.All.SendAsync("GetPart", true);
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    errorCode = 500,
                    message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                };
            }

            return new BaseResponse
            {
                data = partDB
            };
        }
        [HttpDelete("{id}&{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long id, long user)
        {
            Part partDB = await _context.Parts.
                Include(x => x.Passages).ThenInclude(x => x.Questions).
                Include(x => x.Questions).FirstOrDefaultAsync(x => x.Id == id);

            if (partDB == null)
                return new BaseResponse
                {
                    errorCode = 404,
                    message = "Không tìm thấy dữ liệu!"
                };

            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_MODULE = "Xóa học phần";
            log.LOG_ACTID = 3;
            log.LOG_TABID = 3;

            string title = "<b>Tên học phần:</b><title>" +
                "<b>Số điểm:</b><title>" +
                "<b>Độ khó:</b><title>" +
                "<b>Hiển thị câu hỏi:</b><title>" +
                "<b>Trạng thái:</b><title>" +
                "<b>Trộn câu hỏi:</b><title>" +
                "<b>Cho phép trộn câu hỏi:</b><title>" +
                "<b>Cho phép trộn đáp án:</b><title>" +
                "<b>Cho phép tạo câu hỏi không có nội dung:</b><title>" +
                "<b>Cho phép tạo đáp án không có nội dung:</b><title>" +
                "<b>Ghi chú:</b><title>" +
                "<b>Hướng dẫn:</b>";
            string deleteLog = partDB.PAR_NAME + "<item>"
                + partDB.PAR_DEFAULTSCORE + "<item>"
                + partDB.PAR_DEFAULTLEVEL + "<item>"
                + partDB.PAR_DEFAULTCOLUMN + "<item>"
                + (partDB.PAR_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>"
                + (partDB.PAR_ISSHUFFLE == true ? "Có" : "Không") + "<item>"
                + (partDB.PAR_ISALLOWSHUFFLEQUESTION == true ? "Có" : "Không") + "<item>"
                + (partDB.PAR_ISALLOWSHUFFLEOPTION == true ? "Có" : "Không") + "<item>"
                + (partDB.PAR_ISALLOWNULLQUESTION == true ? "Có" : "Không") + "<item>"
                + (partDB.PAR_ISALLOWNULLOPTION == true ? "Có" : "Không") + "<item>"
                + (partDB.PAR_NOTE == null ? "" : partDB.PAR_NOTE) + "<item>"
                + partDB.PAR_DIRECTION + "<item>";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //passage delete
                    if (partDB.Passages.Where(x => x.PAS_ISDELETED == false).ToList().Count != 0)
                    {
                        List<Passage> listPassage = partDB.Passages.Where(x => x.PAS_ISDELETED == false).ToList();
                        for (int i = 0; i < listPassage.Count; i++)
                        {
                            if (listPassage[i].Questions.Where(x => x.QUE_ISDELETED == false).ToList().Count != 0)
                            {
                                List<Question> listQuestion = listPassage[i].Questions.Where(x => x.QUE_ISDELETED == false).ToList();
                                for (int j = 0; j < listQuestion.Count; j++)
                                {
                                    listQuestion[j].QUE_ISDELETED = true;
                                    listQuestion[j].QUE_MODIFIEDBY = user;
                                    listQuestion[j].QUE_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                            }

                            listPassage[i].PAS_ISDELETED = true;
                            listPassage[i].PAS_MODIFIEDBY = user;
                            listPassage[i].PAS_MODIFIEDDATE = DateTime.Now;
                            await _context.SaveChangesAsync();
                        }
                    }
                    //question delete
                    if (partDB.Questions.Where(x => x.QUE_ISDELETED == false).ToList().Count != 0)
                    {
                        List<Question> listQuestion = partDB.Questions.Where(x => x.QUE_ISDELETED == false).ToList();
                        for (int i = 0; i < listQuestion.Count; i++)
                        {
                            listQuestion[i].QUE_ISDELETED = true;
                            listQuestion[i].QUE_MODIFIEDBY = user;
                            listQuestion[i].QUE_MODIFIEDDATE = DateTime.Now;
                            await _context.SaveChangesAsync();
                        }
                    }

                    partDB.PAR_MODIFIEDBY = user;
                    partDB.PAR_MODIFIEDDATE = DateTime.Now;
                    partDB.PAR_ISDELETED = true;
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetPart", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            log.LOG_CONTENT = title + "<divider>" + deleteLog;
            _context.Logs.Add(log);
            await _context.SaveChangesAsync();

            return new BaseResponse
            {
                data = partDB
            };
        }
        [HttpPost("getListPartMultiDel")]
        public async Task<ActionResult<BaseResponse>> getListPartMultiDel(long[] arrDel)
        {
            List<Part> listPar = new List<Part>();
            for (int i = 0; i < arrDel.Length; i++)
            {
                Part par = await _context.Parts.Include(x => x.Subjects).Where(x => x.Id == arrDel[i]).AsNoTracking().FirstOrDefaultAsync();
                if (par != null)
                {
                    listPar.Add(par);
                }
            }
            return new BaseResponse
            {
                data = listPar
            };
        }
        [HttpPost("multipleDelete/{user}")]
        public async Task<ActionResult<BaseResponse>> Delete(long user, Part[] arrDel)
        {
            Log log = new Log();
            log.LOG_DATE = DateTime.Now;
            log.LOG_USEID = user;
            log.LOG_ACTID = 3;
            log.LOG_TABID = 3;
            log.LOG_MODULE = "Xóa danh sách học phần";
            string title = "<b>Tên học phần:</b><title>" +
                "<b>Số điểm:</b><title>" +
                "<b>Độ khó:</b><title>" +
                "<b>Hiển thị câu hỏi:</b><title>" +
                "<b>Trạng thái:</b><title>" +
                "<b>Trộn câu hỏi:</b><title>" +
                "<b>Cho phép trộn câu hỏi:</b><title>" +
                "<b>Cho phép trộn đáp án:</b><title>" +
                "<b>Cho phép tạo câu hỏi không có nội dung:</b><title>" +
                "<b>Cho phép tạo đáp án không có nội dung:</b><title>" +
                "<b>Ghi chú:</b><title>" +
                "<b>Hướng dẫn:</b>";
            string multiDeleteLog = "";

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i = 0; i < arrDel.Length; i++)
                    {
                        var partDB = _context.Parts.
                            Include(x => x.Passages).ThenInclude(x => x.Questions).
                            Include(x => x.Questions).
                            Where(x => x.Id == arrDel[i].Id).FirstOrDefault();
                        if (partDB != null)
                        {
                            multiDeleteLog += partDB.PAR_NAME + "<item>"
                                + partDB.PAR_DEFAULTSCORE + "<item>"
                                + partDB.PAR_DEFAULTLEVEL + "<item>"
                                + partDB.PAR_DEFAULTCOLUMN + "<item>"
                                + (partDB.PAR_STATUS == 1 ? "Sử dụng" : "Loại bỏ") + "<item>"
                                + (partDB.PAR_ISSHUFFLE == true ? "Có" : "Không") + "<item>"
                                + (partDB.PAR_ISALLOWSHUFFLEQUESTION == true ? "Có" : "Không") + "<item>"
                                + (partDB.PAR_ISALLOWSHUFFLEOPTION == true ? "Có" : "Không") + "<item>"
                                + (partDB.PAR_ISALLOWNULLQUESTION == true ? "Có" : "Không") + "<item>"
                                + (partDB.PAR_ISALLOWNULLOPTION == true ? "Có" : "Không") + "<item>"
                                + (partDB.PAR_NOTE == null ? "" : partDB.PAR_NOTE) + "<item>"
                                + partDB.PAR_DIRECTION + "<item>";
                            if (i != arrDel.Length - 1)
                                multiDeleteLog += "<delete>";

                            //passage delete
                            if (partDB.Passages.Where(x => x.PAS_ISDELETED == false).ToList().Count != 0)
                            {
                                List<Passage> listPassage = partDB.Passages.Where(x => x.PAS_ISDELETED == false).ToList();
                                for (int j = 0; j < listPassage.Count; j++)
                                {
                                    if (listPassage[j].Questions.Where(x => x.QUE_ISDELETED == false).ToList().Count != 0)
                                    {
                                        List<Question> listQuestion = listPassage[j].Questions.Where(x => x.QUE_ISDELETED == false).ToList();
                                        for (int k = 0; k < listQuestion.Count; k++)
                                        {
                                            listQuestion[k].QUE_ISDELETED = true;
                                            listQuestion[k].QUE_MODIFIEDBY = user;
                                            listQuestion[k].QUE_MODIFIEDDATE = DateTime.Now;
                                            await _context.SaveChangesAsync();
                                        }
                                    }

                                    listPassage[j].PAS_ISDELETED = true;
                                    listPassage[j].PAS_MODIFIEDBY = user;
                                    listPassage[j].PAS_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                            }
                            //question delete
                            if (partDB.Questions.Where(x => x.QUE_ISDELETED == false).ToList().Count != 0)
                            {
                                List<Question> listQuestion = partDB.Questions.Where(x => x.QUE_ISDELETED == false).ToList();
                                for (int j = 0; j < listQuestion.Count; j++)
                                {
                                    listQuestion[j].QUE_ISDELETED = true;
                                    listQuestion[j].QUE_MODIFIEDBY = user;
                                    listQuestion[j].QUE_MODIFIEDDATE = DateTime.Now;
                                    await _context.SaveChangesAsync();
                                }
                            }

                            partDB.PAR_MODIFIEDBY = user;
                            partDB.PAR_MODIFIEDDATE = DateTime.Now;
                            partDB.PAR_ISDELETED = true;
                            await _context.SaveChangesAsync();
                        }
                    }

                    log.LOG_CONTENT = title + "<divider>" + multiDeleteLog;
                    _context.Logs.Add(log);
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    await _hubContext.Clients.All.SendAsync("GetLog", true);
                    await _hubContext.Clients.All.SendAsync("GetPart", true);
                }
                catch (Exception)
                {
                    return new BaseResponse
                    {
                        errorCode = 500,
                        message = "Đã xảy ra lỗi cơ sở dữ liệu!"
                    };
                }
            }

            return new BaseResponse { };
        }
    }
}