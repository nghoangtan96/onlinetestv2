﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Backend.Hubs;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using OnlineTestBackend.Models;
using OnlineTestBackend.Utils;

namespace OnlineTestBackend
{
    public class Startup
    {
        //public Startup(IConfiguration con)
        //{
        //    Configuration = con;
        //}
        public IHostingEnvironment HostingEnviroment { get; set; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            HostingEnviroment = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration.GetConnectionString("DBConnection");
            services.AddDbContext<OnlineTestBackendDBContext>(opt => opt.UseSqlServer(connectionString));

            services.AddSignalR();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllHeaders",
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                               .AllowAnyMethod()
                               .AllowAnyHeader()
                               .AllowCredentials();
                    });
            });
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(
                opt =>
                {
                    opt.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateIssuerSigningKey = true,
                        RequireExpirationTime = true,
                        ValidIssuer = Utils.Helper.Issuer,
                        ValidAudience = Utils.Helper.Issuer,
                        IssuerSigningKey = new SymmetricSecurityKey(
                                Encoding.UTF8.GetBytes(Utils.Helper.AppKey)
                            )
                    };
                });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddJsonOptions(
                    opt =>
                    {
                        //opt.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                        opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    }
                );
            services.AddMvc().AddControllersAsServices();
            services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue; // In case of multipart
                //x.MemoryBufferThreshold = int.MaxValue;
            });

            //HangFire
            services.AddHangfire(configuration => configuration
                .UseSqlServerStorage(Configuration["Data:HangfireConnection:Data Source=.;Initial Catalog=ENGLISHCENTER;User Id=sa;Password=123456"]));
            services.AddHangfire(configuration => configuration
                .UseSqlServerStorage(@"server=.;database=ENGLISHCENTER; integrated security=True"));

            var containerBulder = new ContainerBuilder();

            containerBulder.Register(icomponentcontext =>
                new BackgroundJobClient(new SqlServerStorage(Configuration["Data:HangfireConnection:ConnectionString"])))
                .As<IBackgroundJobClient>();

            services.AddScoped<ISchedulerService, SchedulerService>();

            // Add the processing server as IHostedService
            services.AddHangfireServer();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStaticFiles(); //share data in wwwroot folder
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(HostingEnviroment.ContentRootPath, "Data")),
                RequestPath = "/Data" // change to hide root folder in server
            });
            app.UseCors("AllowAllHeaders");
            app.UseAuthentication();
            app.UseSignalR(routes =>
            {
                routes.MapHub<SignalHub>("/signalHub");
            });
            app.UseMvc();

            //HangFire
            app.UseHangfireServer();
            app.UseHangfireDashboard();

            RecurringJob.AddOrUpdate<ISchedulerService>(x => x.DayReset(), "* 23 * * *", TimeZoneInfo.Local);
        }
    }
}
