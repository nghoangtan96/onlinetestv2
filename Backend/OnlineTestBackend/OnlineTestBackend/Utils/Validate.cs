﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Utils
{
    public class carriersNumber
    {
        public string oldNumber { get; set; }
        public string newNumber { get; set; }
        public string mobileNetOper { get; set; }
    }
    public class Validate
    {
        public static string customizePhoneNumber(string phonenumber)
        {
            phonenumber = phonenumber.Trim();
            phonenumber = phonenumber.Replace("(+84)", "0");
            phonenumber = phonenumber.Replace("+84", "0");
            phonenumber = phonenumber.Replace("0084", "0");
            phonenumber = phonenumber.Replace(" ", "");
            phonenumber = phonenumber.Replace("-", "");
            phonenumber = phonenumber.Replace(".", "");
            return phonenumber;
        }
        public static bool validPhone(string phone)
        {
            List<carriersNumber> listCarriersNumber = new List<carriersNumber>
            {
                new carriersNumber { oldNumber= "086", newNumber = "086", mobileNetOper = "Viettel"},
                new carriersNumber { oldNumber= "096", newNumber= "096", mobileNetOper= "Viettel" },
                new carriersNumber { oldNumber= "097", newNumber= "097", mobileNetOper= "Viettel" },
                new carriersNumber { oldNumber= "098", newNumber= "098", mobileNetOper= "Viettel" },
                new carriersNumber { oldNumber= "0162", newNumber= "032", mobileNetOper= "Viettel" },
                new carriersNumber { oldNumber= "0163", newNumber= "033", mobileNetOper= "Viettel" },
                new carriersNumber { oldNumber= "0164", newNumber= "034", mobileNetOper= "Viettel" },
                new carriersNumber { oldNumber= "0165", newNumber= "035", mobileNetOper= "Viettel" },
                new carriersNumber { oldNumber= "0166", newNumber= "036", mobileNetOper= "Viettel" },
                new carriersNumber { oldNumber= "0167", newNumber= "037", mobileNetOper= "Viettel" },
                new carriersNumber { oldNumber= "0168", newNumber= "038", mobileNetOper= "Viettel" },
                new carriersNumber { oldNumber= "0169", newNumber= "039", mobileNetOper= "Viettel" },

                new carriersNumber { oldNumber= "089", newNumber= "089", mobileNetOper= "MobiFone" },
                new carriersNumber { oldNumber= "090", newNumber= "090", mobileNetOper= "MobiFone" },
                new carriersNumber { oldNumber= "093", newNumber= "093", mobileNetOper= "MobiFone" },
                new carriersNumber { oldNumber= "0120", newNumber= "070", mobileNetOper= "MobiFone" },
                new carriersNumber { oldNumber= "0121", newNumber= "079", mobileNetOper= "MobiFone" },
                new carriersNumber { oldNumber= "0122", newNumber= "077", mobileNetOper= "MobiFone" },
                new carriersNumber { oldNumber= "0126", newNumber= "076", mobileNetOper= "MobiFone" },
                new carriersNumber { oldNumber= "0128", newNumber= "078", mobileNetOper= "MobiFone" },

                new carriersNumber { oldNumber= "088", newNumber= "088", mobileNetOper= "VinaPhone" },
                new carriersNumber { oldNumber= "091", newNumber= "091", mobileNetOper= "VinaPhone" },
                new carriersNumber { oldNumber= "094", newNumber= "094", mobileNetOper= "VinaPhone" },
                new carriersNumber { oldNumber= "0123", newNumber= "083", mobileNetOper= "VinaPhone" },
                new carriersNumber { oldNumber= "0124", newNumber= "084", mobileNetOper= "VinaPhone" },
                new carriersNumber { oldNumber= "0125", newNumber= "085", mobileNetOper= "VinaPhone" },
                new carriersNumber { oldNumber= "0127", newNumber= "081", mobileNetOper= "VinaPhone" },
                new carriersNumber { oldNumber= "0129", newNumber= "082", mobileNetOper= "VinaPhone" },

                new carriersNumber { oldNumber= "092", newNumber= "092", mobileNetOper= "Vietnamobile" },
                new carriersNumber { oldNumber= "056", newNumber= "056", mobileNetOper= "Vietnamobile" },
                new carriersNumber { oldNumber= "058", newNumber= "058", mobileNetOper= "Vietnamobile" },

                new carriersNumber { oldNumber= "099", newNumber= "099", mobileNetOper= "Gmobile" },
                new carriersNumber { oldNumber= "0199", newNumber= "059", mobileNetOper= "Gmobile" }
            };

            phone = customizePhoneNumber(phone);

            for (int i = 0; i < listCarriersNumber.Count; i++)
            {
                if(listCarriersNumber.ElementAt(i).oldNumber.Length == 3)
                {
                    if (phone.Substring(0, listCarriersNumber.ElementAt(i).oldNumber.Length) == listCarriersNumber.ElementAt(i).oldNumber && phone.Length == 10)
                    {
                        return true;
                    }
                }
                else
                {
                    if (phone.Substring(0, listCarriersNumber.ElementAt(i).oldNumber.Length) == listCarriersNumber.ElementAt(i).oldNumber && phone.Length == 11)
                    {
                        return true;
                    }
                }
            }
            for (int i = 0; i < listCarriersNumber.Count; i++)
            {
                if (phone.Substring(0, listCarriersNumber.ElementAt(i).newNumber.Length) == listCarriersNumber.ElementAt(i).newNumber && phone.Length == 10)
                {
                    return true;
                }
            }
            return false;
        }
        public static bool validEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
