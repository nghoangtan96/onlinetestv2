﻿using Backend.Hubs;
using Hangfire;
using Hangfire.Storage;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OnlineTestBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Utils
{
    public interface ISchedulerService
    {
        void DayReset(); // Reset hangfire event every day
        void OpenRoom1();
        void LockRoom1();
        void EndRoom1();

        void OpenRoom2();
        void LockRoom2();
        void EndRoom2();

        void OpenRoom3();
        void LockRoom3();
        void EndRoom3();

        void OpenRoom4();
        void LockRoom4();
        void EndRoom4();

        void OpenRoom5();
        void LockRoom5();
        void EndRoom5();
    }

    public class SchedulerService : ISchedulerService
    {
        private readonly OnlineTestBackendDBContext _context;
        private readonly IHubContext<SignalHub> _hubContext;

        public SchedulerService(IHubContext<SignalHub> hubContext, OnlineTestBackendDBContext _context)
        {
            this._context = _context;
            this._hubContext = hubContext;
        }

        public void DayReset()
        {
            //delete backgroud job
            using (var connection = JobStorage.Current.GetConnection())
            {
                foreach (var recurringJob in connection.GetRecurringJobs())
                {
                    RecurringJob.RemoveIfExists(recurringJob.Id);
                }
            }

            DateTime currentDate = DateTime.Now;
            currentDate = currentDate.AddDays(1);

            RecurringJob.AddOrUpdate<ISchedulerService>(x => x.DayReset(), "* 23 * * *", TimeZoneInfo.Local);

            List<Schedule_Test> listScheduleTest = _context.Schedule_Tests.Include(x => x.TestShifts).Include(x => x.Tests).
                Where(x => x.SCTE_STATUS == 1 && x.SCTE_ISDELETED == false && x.SCTE_DATE.Day == currentDate.Day && x.SCTE_DATE.Month == currentDate.Month && x.SCTE_DATE.Year == currentDate.Year).ToList();
            List<TestShift> listTestShift = _context.TestShifts.Where(x => x.SHI_ISDELETED == false && x.SHI_STATUS == 1).ToList();

            if (listScheduleTest.Count != 0)
            {
                for (int i = 0; i < listTestShift.Count; i++)
                {
                    if (i == 0 && listScheduleTest.FirstOrDefault(x => x.SCTE_SHIID == listTestShift.ElementAt(i).Id) != null)
                    {
                        DateTime timeOpenRoom = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, listTestShift.ElementAt(i).SHI_TIMESTART.Hours, listTestShift.ElementAt(i).SHI_TIMESTART.Minutes, 0);
                        timeOpenRoom = timeOpenRoom.AddMinutes(-30);
                        string triggerTimeOpen = timeOpenRoom.Minute + " "
                            + timeOpenRoom.Hour
                            + " * * *";
                        RecurringJob.AddOrUpdate<ISchedulerService>(x => x.OpenRoom1(), triggerTimeOpen, TimeZoneInfo.Local);

                        string triggerTimeLock = listTestShift.ElementAt(i).SHI_TIMESTART.Minutes + " "
                        + listTestShift.ElementAt(i).SHI_TIMESTART.Hours
                        + " * * *";
                        RecurringJob.AddOrUpdate<ISchedulerService>(x => x.LockRoom1(), triggerTimeLock, TimeZoneInfo.Local);

                        DateTime timeEndRoom = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, listTestShift.ElementAt(i).SHI_TIMESTART.Hours, listTestShift.ElementAt(i).SHI_TIMESTART.Minutes, 0);
                        timeEndRoom = timeEndRoom.AddMinutes(150);
                        string triggerTimeEnd = timeEndRoom.Minute + " "
                            + timeEndRoom.Hour
                            + " * * *";
                        RecurringJob.AddOrUpdate<ISchedulerService>(x => x.EndRoom1(), triggerTimeEnd, TimeZoneInfo.Local);
                    }
                    else if (i == 1 && listScheduleTest.FirstOrDefault(x => x.SCTE_SHIID == listTestShift.ElementAt(i).Id) != null)
                    {
                        DateTime timeOpenRoom = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, listTestShift.ElementAt(i).SHI_TIMESTART.Hours, listTestShift.ElementAt(i).SHI_TIMESTART.Minutes, 0);
                        timeOpenRoom = timeOpenRoom.AddMinutes(-30);
                        string triggerTimeOpen = timeOpenRoom.Minute + " "
                            + timeOpenRoom.Hour
                            + " * * *";
                        RecurringJob.AddOrUpdate<ISchedulerService>(x => x.OpenRoom2(), triggerTimeOpen, TimeZoneInfo.Local);

                        string triggerTimeLock = listTestShift.ElementAt(i).SHI_TIMESTART.Minutes + " "
                        + listTestShift.ElementAt(i).SHI_TIMESTART.Hours
                        + " * * *";
                        RecurringJob.AddOrUpdate<ISchedulerService>(x => x.LockRoom2(), triggerTimeLock, TimeZoneInfo.Local);

                        DateTime timeEndRoom = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, listTestShift.ElementAt(i).SHI_TIMESTART.Hours, listTestShift.ElementAt(i).SHI_TIMESTART.Minutes, 0);
                        timeEndRoom = timeEndRoom.AddMinutes(150);
                        string triggerTimeEnd = timeEndRoom.Minute + " "
                            + timeEndRoom.Hour
                            + " * * *";
                        RecurringJob.AddOrUpdate<ISchedulerService>(x => x.EndRoom2(), triggerTimeEnd, TimeZoneInfo.Local);
                    }
                    else if (i == 2 && listScheduleTest.FirstOrDefault(x => x.SCTE_SHIID == listTestShift.ElementAt(i).Id) != null)
                    {
                        DateTime timeOpenRoom = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, listTestShift.ElementAt(i).SHI_TIMESTART.Hours, listTestShift.ElementAt(i).SHI_TIMESTART.Minutes, 0);
                        timeOpenRoom = timeOpenRoom.AddMinutes(-30);
                        string triggerTimeOpen = timeOpenRoom.Minute + " "
                            + timeOpenRoom.Hour
                            + " * * *";
                        RecurringJob.AddOrUpdate<ISchedulerService>(x => x.OpenRoom3(), triggerTimeOpen, TimeZoneInfo.Local);

                        string triggerTimeLock = listTestShift.ElementAt(i).SHI_TIMESTART.Minutes + " "
                        + listTestShift.ElementAt(i).SHI_TIMESTART.Hours
                        + " * * *";
                        RecurringJob.AddOrUpdate<ISchedulerService>(x => x.LockRoom3(), triggerTimeLock, TimeZoneInfo.Local);

                        DateTime timeEndRoom = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, listTestShift.ElementAt(i).SHI_TIMESTART.Hours, listTestShift.ElementAt(i).SHI_TIMESTART.Minutes, 0);
                        timeEndRoom = timeEndRoom.AddMinutes(150);
                        string triggerTimeEnd = timeEndRoom.Minute + " "
                            + timeEndRoom.Hour
                            + " * * *";
                        RecurringJob.AddOrUpdate<ISchedulerService>(x => x.EndRoom3(), triggerTimeEnd, TimeZoneInfo.Local);
                    }
                    else if (i == 3 && listScheduleTest.FirstOrDefault(x => x.SCTE_SHIID == listTestShift.ElementAt(i).Id) != null)
                    {
                        DateTime timeOpenRoom = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, listTestShift.ElementAt(i).SHI_TIMESTART.Hours, listTestShift.ElementAt(i).SHI_TIMESTART.Minutes, 0);
                        timeOpenRoom = timeOpenRoom.AddMinutes(-30);
                        string triggerTimeOpen = timeOpenRoom.Minute + " "
                            + timeOpenRoom.Hour
                            + " * * *";
                        RecurringJob.AddOrUpdate<ISchedulerService>(x => x.OpenRoom4(), triggerTimeOpen, TimeZoneInfo.Local);

                        string triggerTimeLock = listTestShift.ElementAt(i).SHI_TIMESTART.Minutes + " "
                        + listTestShift.ElementAt(i).SHI_TIMESTART.Hours
                        + " * * *";
                        RecurringJob.AddOrUpdate<ISchedulerService>(x => x.LockRoom4(), triggerTimeLock, TimeZoneInfo.Local);

                        DateTime timeEndRoom = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, listTestShift.ElementAt(i).SHI_TIMESTART.Hours, listTestShift.ElementAt(i).SHI_TIMESTART.Minutes, 0);
                        timeEndRoom = timeEndRoom.AddMinutes(150);
                        string triggerTimeEnd = timeEndRoom.Minute + " "
                            + timeEndRoom.Hour
                            + " * * *";
                        RecurringJob.AddOrUpdate<ISchedulerService>(x => x.EndRoom4(), triggerTimeEnd, TimeZoneInfo.Local);
                    }
                    else if (i == 4 && listScheduleTest.FirstOrDefault(x => x.SCTE_SHIID == listTestShift.ElementAt(i).Id) != null)
                    {
                        DateTime timeOpenRoom = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, listTestShift.ElementAt(i).SHI_TIMESTART.Hours, listTestShift.ElementAt(i).SHI_TIMESTART.Minutes, 0);
                        timeOpenRoom = timeOpenRoom.AddMinutes(-30);
                        string triggerTimeOpen = timeOpenRoom.Minute + " "
                            + timeOpenRoom.Hour
                            + " * * *";
                        RecurringJob.AddOrUpdate<ISchedulerService>(x => x.OpenRoom5(), triggerTimeOpen, TimeZoneInfo.Local);

                        string triggerTimeLock = listTestShift.ElementAt(i).SHI_TIMESTART.Minutes + " "
                        + listTestShift.ElementAt(i).SHI_TIMESTART.Hours
                        + " * * *";
                        RecurringJob.AddOrUpdate<ISchedulerService>(x => x.LockRoom5(), triggerTimeLock, TimeZoneInfo.Local);

                        DateTime timeEndRoom = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, listTestShift.ElementAt(i).SHI_TIMESTART.Hours, listTestShift.ElementAt(i).SHI_TIMESTART.Minutes, 0);
                        timeEndRoom = timeEndRoom.AddMinutes(150);
                        string triggerTimeEnd = timeEndRoom.Minute + " "
                            + timeEndRoom.Hour
                            + " * * *";
                        RecurringJob.AddOrUpdate<ISchedulerService>(x => x.EndRoom5(), triggerTimeEnd, TimeZoneInfo.Local);
                    }
                }
            }
        }

        public void OpenRoom1()
        {
            _hubContext.Clients.All.SendAsync("GetRoomView", true);
        }
        public void LockRoom1()
        {
            _hubContext.Clients.All.SendAsync("GetRoomView", true);
        }
        public void EndRoom1()
        {
            DateTime currentDate = DateTime.Now;
            TimeSpan currentTime = new TimeSpan(currentDate.Hour, currentDate.Minute, 0);
            List<Schedule_Test> listScheduleTest = _context.Schedule_Tests.Include(x => x.TestShifts).
                Where(x => x.SCTE_DATE.Year == currentDate.Year && x.SCTE_DATE.Month == currentDate.Month && x.SCTE_DATE.Day == currentDate.Day && x.SCTE_STATUS == 1).
                ToList();

            for (int i = 0; i < listScheduleTest.Count; i++)
            {
                if (TimeSpan.Compare(listScheduleTest.ElementAt(i).TestShifts.SHI_TIMESTART, currentTime) == -1)
                {
                    //end schedule test
                    listScheduleTest.ElementAt(i).SCTE_STATUS = 4;
                    _context.SaveChanges();

                    //end test
                    Test test = _context.Tests.FirstOrDefault(x => x.Id == listScheduleTest.ElementAt(i).SCTE_TESID);
                    test.TES_STATUS = 4;
                    _context.SaveChanges();

                    //update score student
                    List<Student_Test> listStudentTest = _context.Student_Tests.
                        Where(x => x.STTE_SCTEID == listScheduleTest.ElementAt(i).Id).ToList();
                    for (int j = 0; j < listStudentTest.Count; j++)
                    {
                        if (listStudentTest.ElementAt(j).STTE_STATUS == 1)
                        {
                            listStudentTest.ElementAt(j).STTE_STATUS = 3;
                            listStudentTest.ElementAt(j).STTE_SCORE = 0;
                            _context.SaveChanges();
                        }
                    }
                }
            }
            _hubContext.Clients.All.SendAsync("GetRoomView", true);
            _hubContext.Clients.All.SendAsync("GetTest", true);
            _hubContext.Clients.All.SendAsync("GetScheduleTest", true);
        }
        public void OpenRoom2()
        {
            _hubContext.Clients.All.SendAsync("GetRoomView", true);
        }
        public void LockRoom2()
        {
            _hubContext.Clients.All.SendAsync("GetRoomView", true);
        }
        public void EndRoom2()
        {
            DateTime currentDate = DateTime.Now;
            TimeSpan currentTime = new TimeSpan(currentDate.Hour, currentDate.Minute, 0);
            List<Schedule_Test> listScheduleTest = _context.Schedule_Tests.Include(x => x.TestShifts).
                Where(x => x.SCTE_DATE.Year == currentDate.Year && x.SCTE_DATE.Month == currentDate.Month && x.SCTE_DATE.Day == currentDate.Day && x.SCTE_STATUS == 1).
                ToList();

            for (int i = 0; i < listScheduleTest.Count; i++)
            {
                if (TimeSpan.Compare(listScheduleTest.ElementAt(i).TestShifts.SHI_TIMESTART, currentTime) == -1)
                {
                    //end schedule test
                    listScheduleTest.ElementAt(i).SCTE_STATUS = 4;
                    _context.SaveChanges();

                    //end test
                    Test test = _context.Tests.FirstOrDefault(x => x.Id == listScheduleTest.ElementAt(i).SCTE_TESID);
                    test.TES_STATUS = 4;
                    _context.SaveChanges();

                    //update score student
                    List<Student_Test> listStudentTest = _context.Student_Tests.
                        Where(x => x.STTE_SCTEID == listScheduleTest.ElementAt(i).Id).ToList();
                    for (int j = 0; j < listStudentTest.Count; j++)
                    {
                        if (listStudentTest.ElementAt(j).STTE_STATUS == 1)
                        {
                            listStudentTest.ElementAt(j).STTE_STATUS = 3;
                            listStudentTest.ElementAt(j).STTE_SCORE = 0;
                            _context.SaveChanges();
                        }
                    }
                }
            }
            _hubContext.Clients.All.SendAsync("GetRoomView", true);
            _hubContext.Clients.All.SendAsync("GetTest", true);
            _hubContext.Clients.All.SendAsync("GetScheduleTest", true);
        }
        public void OpenRoom3()
        {
            _hubContext.Clients.All.SendAsync("GetRoomView", true);
        }
        public void LockRoom3()
        {
            _hubContext.Clients.All.SendAsync("GetRoomView", true);
        }
        public void EndRoom3()
        {
            DateTime currentDate = DateTime.Now;
            TimeSpan currentTime = new TimeSpan(currentDate.Hour, currentDate.Minute, 0);
            List<Schedule_Test> listScheduleTest = _context.Schedule_Tests.Include(x => x.TestShifts).
                Where(x => x.SCTE_DATE.Year == currentDate.Year && x.SCTE_DATE.Month == currentDate.Month && x.SCTE_DATE.Day == currentDate.Day && x.SCTE_STATUS == 1).
                ToList();

            for (int i = 0; i < listScheduleTest.Count; i++)
            {
                if (TimeSpan.Compare(listScheduleTest.ElementAt(i).TestShifts.SHI_TIMESTART, currentTime) == -1)
                {
                    //end schedule test
                    listScheduleTest.ElementAt(i).SCTE_STATUS = 4;
                    _context.SaveChanges();

                    //end test
                    Test test = _context.Tests.FirstOrDefault(x => x.Id == listScheduleTest.ElementAt(i).SCTE_TESID);
                    test.TES_STATUS = 4;
                    _context.SaveChanges();

                    //update score student
                    List<Student_Test> listStudentTest = _context.Student_Tests.
                        Where(x => x.STTE_SCTEID == listScheduleTest.ElementAt(i).Id).ToList();
                    for (int j = 0; j < listStudentTest.Count; j++)
                    {
                        if (listStudentTest.ElementAt(j).STTE_STATUS == 1)
                        {
                            listStudentTest.ElementAt(j).STTE_STATUS = 3;
                            listStudentTest.ElementAt(j).STTE_SCORE = 0;
                            _context.SaveChanges();
                        }
                    }
                }
            }
            _hubContext.Clients.All.SendAsync("GetRoomView", true);
            _hubContext.Clients.All.SendAsync("GetTest", true);
            _hubContext.Clients.All.SendAsync("GetScheduleTest", true);
        }
        public void OpenRoom4()
        {
            _hubContext.Clients.All.SendAsync("GetRoomView", true);
        }
        public void LockRoom4()
        {
            _hubContext.Clients.All.SendAsync("GetRoomView", true);
        }
        public void EndRoom4()
        {
            DateTime currentDate = DateTime.Now;
            TimeSpan currentTime = new TimeSpan(currentDate.Hour, currentDate.Minute, 0);
            List<Schedule_Test> listScheduleTest = _context.Schedule_Tests.Include(x => x.TestShifts).
                Where(x => x.SCTE_DATE.Year == currentDate.Year && x.SCTE_DATE.Month == currentDate.Month && x.SCTE_DATE.Day == currentDate.Day && x.SCTE_STATUS == 1).
                ToList();

            for (int i = 0; i < listScheduleTest.Count; i++)
            {
                if (TimeSpan.Compare(listScheduleTest.ElementAt(i).TestShifts.SHI_TIMESTART, currentTime) == -1)
                {
                    //end schedule test
                    listScheduleTest.ElementAt(i).SCTE_STATUS = 4;
                    _context.SaveChanges();

                    //end test
                    Test test = _context.Tests.FirstOrDefault(x => x.Id == listScheduleTest.ElementAt(i).SCTE_TESID);
                    test.TES_STATUS = 4;
                    _context.SaveChanges();

                    //update score student
                    List<Student_Test> listStudentTest = _context.Student_Tests.
                        Where(x => x.STTE_SCTEID == listScheduleTest.ElementAt(i).Id).ToList();
                    for (int j = 0; j < listStudentTest.Count; j++)
                    {
                        if (listStudentTest.ElementAt(j).STTE_STATUS == 1)
                        {
                            listStudentTest.ElementAt(j).STTE_STATUS = 3;
                            listStudentTest.ElementAt(j).STTE_SCORE = 0;
                            _context.SaveChanges();
                        }
                    }
                }
            }
            _hubContext.Clients.All.SendAsync("GetRoomView", true);
            _hubContext.Clients.All.SendAsync("GetTest", true);
            _hubContext.Clients.All.SendAsync("GetScheduleTest", true);
        }
        public void OpenRoom5()
        {
            _hubContext.Clients.All.SendAsync("GetRoomView", true);
        }
        public void LockRoom5()
        {
            _hubContext.Clients.All.SendAsync("GetRoomView", true);
        }
        public void EndRoom5()
        {
            DateTime currentDate = DateTime.Now;
            TimeSpan currentTime = new TimeSpan(currentDate.Hour, currentDate.Minute, 0);
            List<Schedule_Test> listScheduleTest = _context.Schedule_Tests.Include(x => x.TestShifts).
                Where(x => x.SCTE_DATE.Year == currentDate.Year && x.SCTE_DATE.Month == currentDate.Month && x.SCTE_DATE.Day == currentDate.Day && x.SCTE_STATUS == 1).
                ToList();

            for (int i = 0; i < listScheduleTest.Count; i++)
            {
                if (TimeSpan.Compare(listScheduleTest.ElementAt(i).TestShifts.SHI_TIMESTART, currentTime) == -1)
                {
                    //end schedule test
                    listScheduleTest.ElementAt(i).SCTE_STATUS = 4;
                    _context.SaveChanges();

                    //end test
                    Test test = _context.Tests.FirstOrDefault(x => x.Id == listScheduleTest.ElementAt(i).SCTE_TESID);
                    test.TES_STATUS = 4;
                    _context.SaveChanges();

                    //update score student
                    List<Student_Test> listStudentTest = _context.Student_Tests.
                        Where(x => x.STTE_SCTEID == listScheduleTest.ElementAt(i).Id).ToList();
                    for (int j = 0; j < listStudentTest.Count; j++)
                    {
                        if (listStudentTest.ElementAt(j).STTE_STATUS == 1)
                        {
                            listStudentTest.ElementAt(j).STTE_STATUS = 3;
                            listStudentTest.ElementAt(j).STTE_SCORE = 0;
                            _context.SaveChanges();
                        }
                    }
                }
            }
            _hubContext.Clients.All.SendAsync("GetRoomView", true);
            _hubContext.Clients.All.SendAsync("GetTest", true);
            _hubContext.Clients.All.SendAsync("GetScheduleTest", true);
        }
    }
}
