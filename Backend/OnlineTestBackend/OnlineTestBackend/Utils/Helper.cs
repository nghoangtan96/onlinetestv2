﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace OnlineTestBackend.Utils
{
    public class Helper
    {
        public static readonly string AppKey = "7eTJ8fLbHMuCOsNGf4wM";
        public static readonly string Issuer = "http://localhost:4200";
        private static Random random = new Random();
        public static string GenHash(string input)
        {
            return string.Join("", new SHA1Managed()
                        .ComputeHash(Encoding.UTF8.GetBytes(input))
                        .Select(x => x.ToString("X2")).ToArray());
        }
        public static string GetBaseUrl(HttpRequest request)
        {
            return request.Scheme + "://" + request.Host.ToString();
        }
        public static string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
