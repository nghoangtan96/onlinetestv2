﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("LABS")]
    public class Lab
    {
        [Column("LAB_ID")]
        public int Id { get; set; }
        [Required]
        public string LAB_NAME { get; set; }
        public string LAB_ADDRESS { get; set; }
        public bool? LAB_ISDELETED { get; set; }
        public int? LAB_STATUS { get; set; }
        public long? LAB_CREATEDBY { get; set; }
        public DateTime? LAB_CREATEDDATE { get; set; }
        public long? LAB_MODIFIEDBY { get; set; }
        public DateTime? LAB_MODIFIEDDATE { get; set; }
        [ForeignKey("LAB_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("LAB_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        public virtual ICollection<Schedule_Test> Schedule_Tests { get; set; }
    }
}
