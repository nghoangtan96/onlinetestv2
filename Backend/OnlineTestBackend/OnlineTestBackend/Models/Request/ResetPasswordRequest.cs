﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models.Request
{
    public class ResetPasswordRequest
    {
        public string email { get; set; }
    }
}
