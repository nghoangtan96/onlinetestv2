﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models.Request
{
    public class PermissionRequest
    {
        public Permission permission { get; set; }
        public Role_Permission[] listRolePermission { get; set; }
    }
}
