﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models.Request
{
    public class EmployeeRequest
    {
        public User user { get; set; }
        public Employee employee { get; set; }
        public int[] user_permissions { get; set; }
        public long[] list_subject { get; set; }
    }
}
