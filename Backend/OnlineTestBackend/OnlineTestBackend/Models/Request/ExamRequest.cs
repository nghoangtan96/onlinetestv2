﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models.Request
{
    public class ExamRequest
    {
        public int type { get; set; }
        public long userId { get; set; }
        public long scheduleTestId { get; set; }
        public int remainingTime { get; set; }
        public string listQuestion { get; set; }
        public string listOption { get; set; }
        public long currentPart { get; set; }
        public int currentTimeAudio { get; set; }
    }
}
