﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models.Request
{
    public class SettingRequest
    {
        public long userId { get; set; }
        public long id { get; set; }
        public int? value { get; set; }
        public bool? isactive { get; set; }
        public string choicevalue { get; set; }
        public string multichoicevalue { get; set; }
    }
}
