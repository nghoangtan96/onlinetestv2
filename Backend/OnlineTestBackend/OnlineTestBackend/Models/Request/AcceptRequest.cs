﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models.Request
{
    public class AcceptRequest
    {
        public long id { get; set; }
        public long userid { get; set; }
        public string password { get; set; }
    }
}
