﻿using OnlineTestBackend.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models.Request
{
    public class TestRequest
    {
        public Test test { get; set; }
        public Test_Question[] testQuestion { get; set; }
        public int shuffle { get; set; }
        public TestShuffle[] testShuffle { get; set; }
        public int testFormat { get; set; }
        public int numberOfQuestion { get; set; }
        public int numberOfTest { get; set; }
        public bool showScore { get; set; }
    }
}
