﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models.Request
{
    public class ChangePasswordRequest
    {
        public long userId { get; set; }
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
    }
}
