﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models.Request
{
    public class QuestionRequest
    {
        public Question question { get; set; }
        public Option[] listOption { get; set; }
    }
}
