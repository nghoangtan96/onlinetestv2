﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models.Request
{
    public class ScheduleTestRequest
    {
        public Schedule_Test scheduleTest { get; set; }
        public Student[] listStudent { get; set; }
    }
}
