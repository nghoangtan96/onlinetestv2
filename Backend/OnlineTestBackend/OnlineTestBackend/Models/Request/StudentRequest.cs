﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models.Request
{
    public class StudentRequest
    {
        public User user { get; set; }
        public Student student { get; set; }
        public int[] user_permissions { get; set; }
    }
}
