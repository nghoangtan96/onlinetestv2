﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("PERMISSIONS")]
    public class Permission
    {
        [Column("PER_ID")]
        public int Id { get; set; }
        [Required]
        [MaxLength(400)]
        public string PER_NAME { get; set; }
        public int? PER_STATUS { get; set; }
        public bool? PER_ISACTIVE { get; set; }
        public bool? PER_ISDELETED { get; set; }
        public long? PER_CREATEDBY { get; set; }
        public DateTime? PER_CREATEDDATE { get; set; }
        public long? PER_MODIFIEDBY { get; set; }
        public DateTime? PER_MODIFIEDDATE { get; set; }
        [ForeignKey("PER_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("PER_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        public virtual ICollection<Role_Permission> Role_Permissions { get; set; }
        public virtual ICollection<User_Permission> User_Permissions { get; set; }
    }
}
