﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("EMPLOYEES")]
    public class Employee
    {
        [Column("EMP_ID")]
        public long Id { get; set; }
        [MaxLength(50)]
        public string EMP_CODE { get; set; }
        public string EMP_IDENTITY { get; set; }
        [Required]
        [MaxLength(50)]
        public string EMP_FIRSTNAME { get; set; }
        [Required]
        [MaxLength(100)]
        public string EMP_LASTNAME { get; set; }
        public bool? EMP_GENDER { get; set; }
        public DateTime? EMP_DATEOFBIRTH { get; set; }
        [MaxLength(15)]
        public string EMP_PHONE { get; set; }
        [MaxLength(255)]
        public string EMP_EMAIL { get; set; }
        [MaxLength(4000)]
        public string EMP_ADDRESS { get; set; }
        public string EMP_AVATAR { get; set; }
        public long? EMP_CREATEDBY { get; set; }
        public DateTime? EMP_CREATEDDATE { get; set; }
        public long? EMP_MODIFIEDBY { get; set; }
        public DateTime? EMP_MODIFIEDDATE { get; set; }
        [Required]
        public long EMP_USEID { get; set; }
        public int? EMP_FACID { get; set; }
        public int? EMP_STATUS { get; set; }
        public bool? EMP_ISDELETED { get; set; }
        [NotMapped]
        public string origin { get; set; }
        [NotMapped]
        public string FileUrl { get; set; }
        [NotMapped]
        public virtual User UserCreated { get; set; }
        [NotMapped]
        public virtual User UserModified { get; set; }
        [ForeignKey("EMP_USEID")]
        public virtual User Users { get; set; }
        [ForeignKey("EMP_FACID")]
        public virtual Faculty Faculties { get; set; }
        public virtual ICollection<Employee_Subject> Employee_Subjects { get; set; }
    }
}
