﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("SUBJECTS")]
    public class Subject
    {
        [Column("SUB_ID")]
        public long Id { get; set; }
        public string SUB_CODE { get; set; }
        public string SUB_NAME { get; set; }
        public int? SUB_CREDIT { get; set; }
        public int? SUB_STATUS { get; set; }
        public bool? SUB_ISDELETED { get; set; }
        public bool? SUB_ISALL { get; set; }
        public long? SUB_CREATEDBY { get; set; }
        public DateTime? SUB_CREATEDDATE { get; set; }
        public long? SUB_MODIFIEDBY { get; set; }
        public DateTime? SUB_MODIFIEDDATE { get; set; }
        public int? SUB_FACID { get; set; }
        [ForeignKey("SUB_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("SUB_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        public virtual ICollection<Subject_Branch> Subject_Branches { get; set; }
        [NotMapped]
        public int[] listBranch { get; set; }
        public virtual ICollection<Part> Parts { get; set; }
        [ForeignKey("SUB_FACID")]
        public virtual Faculty Faculties { get; set; }
    }
}
