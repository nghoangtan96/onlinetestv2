﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("TESTS")]
    public class Test
    {
        [Column("TES_ID")]
        public long Id { get; set; }
        public string TES_CODE { get; set; }
        public string TES_TITLE { get; set; }
        public string TES_VERSION { get; set; }
        public DateTime TES_DATE { get; set; }
        [Required]
        public long TES_TIME { get; set; }
        public string TES_LANGUAGE { get; set; }
        public bool TES_ISSHUFFLE { get; set; }
        public bool? TES_ISHEADPHONE { get; set; }
        public bool? TES_ISFRAME { get; set; }
        public bool? TES_ISACTIVE { get; set; }
        public bool? TES_ISDELETED { get; set; }
        public bool TES_ISLOCKED { get; set; }
        [Required]
        public string TES_PASSWORD { get; set; }
        public string TES_ENCPASSWORD { get; set; }
        [Required]
        public double TES_MAXSCORE { get; set; }
        [Column(TypeName = "ntext")]
        public string TES_NOTE { get; set; }
        public int? TES_STATUS { get; set; }
        public long? TES_CREATEDBY { get; set; }
        public DateTime? TES_CREATEDDATE { get; set; }
        public long? TES_ACCEPTEDBY { get; set; }
        public DateTime? TES_ACCEPTEDDATE { get; set; }
        public long? TES_MODIFIEDBY { get; set; }
        public DateTime? TES_MODIFIEDDATE { get; set; }
        [Required]
        public long TES_SUBID { get; set; }
        [Required]
        public int TES_TETYID { get; set; }
        [ForeignKey("TES_SUBID")]
        public virtual Subject Subjects { get; set; }
        [ForeignKey("TES_TETYID")]
        public virtual Test_Type Test_Types { get; set; }
        [ForeignKey("TES_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("TES_ACCEPTEDBY")]
        public virtual User UserAccepted { get; set; }
        [ForeignKey("TES_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        public virtual ICollection<TestingFormat> TestingFormats { get; set; }
        public virtual ICollection<Test_Detail> Test_Details { get; set; }
        public virtual ICollection<Test_Question> Test_Questions { get; set; }
        [NotMapped]
        public int countQuestion { get; set; }
        public virtual ICollection<Schedule_Test> Schedule_Tests { get; set; }
    }
}
