﻿using OnlineTestBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models.Response
{
    public class LoginResponse
    {
        public long Id { get; set; }
        public string username { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string token { get; set; } = "";
        //public Permission permission { get; set; }
        //public Permission[] permission_list { get; set; }
    }
}
