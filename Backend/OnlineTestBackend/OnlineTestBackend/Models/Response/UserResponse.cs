﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models.Response
{
    public class UserResponse
    {
        public long id { get; set; }
        public string username { get; set; }
        public int? status { get; set; }
        public int perid { get; set; }
        public ICollection<Employee> employee { get; set; }
        public ICollection<Student> student { get; set; }
        public ICollection<User_Permission> list_per { get; set; }
    }
}
