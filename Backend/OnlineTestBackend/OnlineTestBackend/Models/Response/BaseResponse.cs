﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models.Response
{
    public class BaseResponse
    {
        public int errorCode { get; set; } = 0;
        public string message { get; set; } = "";
        public object data { get; set; }
    }
}
