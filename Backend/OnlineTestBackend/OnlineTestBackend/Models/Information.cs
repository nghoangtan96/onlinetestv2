﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("INFORMATIONS")]
    public class Information
    {
        [Column("INF_ID")]
        public int Id { get; set; }
        public string INF_KEYNAME { get; set; }
        public string INF_VALUE { get; set; }
        public int? INF_ORDER { get; set; }
        public int? INF_STATUS { get; set; }
        public bool? INF_ISDELETED { get; set; }
        public bool? INF_ISMULTI { get; set; }
        public int? INF_INFID { get; set; }
        public long? INF_CREATEDBY { get; set; }
        public DateTime? INF_CREATEDDATE { get; set; }
        public long? INF_MODIFIEDBY { get; set; }
        public DateTime? INF_MODIFIEDDATE { get; set; }
        [NotMapped]
        public string FileUrl { get; set; }
        [ForeignKey("INF_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("INF_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        [ForeignKey("INF_INFID")]
        public virtual ICollection<Information> Informations { get; set; }
    }
}
