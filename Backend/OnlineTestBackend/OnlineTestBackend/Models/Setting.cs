﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("SETTINGS")]
    public class Setting
    {
        [Column("SET_ID")]
        public long Id { get; set; }
        [Required]
        [MaxLength(255)]
        public string SET_NAME { get; set; }
        public int? SET_VALUE { get; set; }
        public bool? SET_ISACTIVE { get; set; }
        public string SET_CHOICE { get; set; }
        public string SET_CHOICEVALUE { get; set; }
        public string SET_MULTICHOICE { get; set; }
        public string SET_MULTICHOICEVALUE { get; set; }
        public long? SET_MODIFIEDBY { get; set; }
        public DateTime? SET_MODIFIEDDATE { get; set; }
        public int? SET_SETYID { get; set; }
        [MaxLength(100)]
        public string SET_UNIT { get; set; }
        [Column(TypeName = "ntext")]
        public string SET_NOTE { get; set; }
        [ForeignKey("SET_SETYID")]
        public virtual SettingType SettingTypes { get; set; }
        [ForeignKey("SET_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
    }
}
