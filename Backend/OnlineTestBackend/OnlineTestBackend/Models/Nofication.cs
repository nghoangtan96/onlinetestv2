﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("NOFICATIONS")]
    public class Nofication
    {
        [Column("NOF_ID")]
        public long Id { get; set; }
        public long? NOF_USEID { get; set; }
        public string NOF_TITLE { get; set; }
        public string NOF_CONTENT { get; set; }
        public string NOF_LINK { get; set; }
        public string NOF_KEYPARAM { get; set; }
        public long? NOF_PARAM { get; set; }
        public int? NOF_STATUS { get; set; }
        public long? NOF_CREATEDBY { get; set; }
        public DateTime? NOF_CREATEDDATE { get; set; }
        public DateTime? NOF_MODIFIEDDATE { get; set; }
        [ForeignKey("NOF_USEID")]
        public virtual User Users { get; set; }
        [ForeignKey("NOF_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [NotMapped]
        public string timeSpan { get; set; }
    }
}
