﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("SUBJECT_BRANCHES")]
    public class Subject_Branch
    {
        [Column("SUBR_ID")]
        public long Id { get; set; }
        [Required]
        public long SUBR_SUBID { get; set; }
        [Required]
        public int SUBR_BRAID { get; set; }
        public int? SUBR_STATUS { get; set; }
        public long? SUBR_CREATEDBY { get; set; }
        public DateTime? SUBR_CREATEDDATE { get; set; }
        [ForeignKey("SUBR_BRAID")]
        public virtual Branch Branches { get; set; }
        [ForeignKey("SUBR_SUBID")]
        public virtual Subject Subjects { get; set; }
        [ForeignKey("SUBR_CREATEDBY")]
        public virtual User UserCreated { get; set; }
    }
}
