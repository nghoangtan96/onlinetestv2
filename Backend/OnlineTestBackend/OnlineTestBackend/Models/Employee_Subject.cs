﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("EMPLOYEE_SUBJECTS")]
    public class Employee_Subject
    {
        [Column("EMSU_ID")]
        public long Id { get; set; }
        public long EMSU_EMPID { get; set; }
        public long EMSU_SUBID { get; set; }
        public long? EMSU_CREATEDBY { get; set; }
        public DateTime? EMSU_CREATEDDATE { get; set; }
        [ForeignKey("EMSU_EMPID")]
        public virtual Employee Employees { get; set; }
        [ForeignKey("EMSU_SUBID")]
        public virtual Subject Subjects { get; set; }
        [ForeignKey("EMSU_CREATEDBY")]
        public virtual User UserCreated { get; set; }
    }
}
