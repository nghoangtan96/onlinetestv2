﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("CLASSES")]
    public class Class
    {
        [Column("CLA_ID")]
        public long Id { get; set; }
        public string CLA_CODE { get; set; }
        public string CLA_NAME { get; set; }
        public long? CLA_CREATEDBY { get; set; }
        public DateTime? CLA_CREATEDDATE { get; set; }
        public long? CLA_MODIFIEDBY { get; set; }
        public DateTime? CLA_MODIFIEDDATE { get; set; }
        public int? CLA_STATUS { get; set; }
        public bool? CLA_ISDELETED { get; set; }
        public int CLA_BRAID { get; set; }
        public int CLA_SCYEID { get; set; }
        [ForeignKey("CLA_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("CLA_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        [ForeignKey("CLA_BRAID")]
        public virtual Branch Branches { get; set; }
        [ForeignKey("CLA_SCYEID")]
        public virtual SchoolYear SchoolYears { get; set; }
        public virtual ICollection<Student> Students { get; set; }
    }
}
