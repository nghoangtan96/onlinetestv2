﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("TABLES")]
    public class Table
    {
        [Column("TAB_ID")]
        public int Id { get; set; }
        [Required]
        [MaxLength(150)]
        public string TAB_NAME { get; set; }
        public int? TAB_STATUS { get; set; }
        public string TAB_CODE { get; set; }
    }
}
