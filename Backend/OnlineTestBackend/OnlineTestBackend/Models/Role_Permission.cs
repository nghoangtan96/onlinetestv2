﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("ROLE_PERMISSIONS")]
    public class Role_Permission
    {
        [Column("ROPE_ID")]
        public long Id { get; set; }
        [Required]
        public long ROPE_ROLID { get; set; }
        [Required]
        public int ROPE_PERID { get; set; }
        public long? ROPE_CREATEDBY { get; set; }
        public DateTime? ROPE_CREATEDDATE { get; set; }
        public long? ROPE_MODIFIEDBY { get; set; }
        public DateTime? ROPE_MODIFIEDDATE { get; set; }
        public bool? ROPE_ISACTIVE { get; set; }
        public int? ROPE_STATUS { get; set; }
        [ForeignKey("ROPE_ROLID")]
        public virtual Role Roles { get; set; }
        [ForeignKey("ROPE_PERID")]
        public virtual Permission Permissions { get; set; }
        [ForeignKey("ROPE_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("ROPE_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
    }
}
