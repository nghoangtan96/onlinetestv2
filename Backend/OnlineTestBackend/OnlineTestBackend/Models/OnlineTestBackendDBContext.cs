﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    public class OnlineTestBackendDBContext : DbContext
    {
        public OnlineTestBackendDBContext(DbContextOptions<OnlineTestBackendDBContext> options) : base(options) { }

        public DbSet<Actionn> Actionns { get; set; }

        public DbSet<Table> Tables { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Permission> Permissions { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Role_Permission> Role_Permissions { get; set; }

        public DbSet<Branch> Branches { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Student> Students { get; set; }

        public DbSet<User_Permission> User_Permissions { get; set; }

        public DbSet<Nofication> Nofications { get; set; }

        public DbSet<SettingType> SettingTypes { get; set; }

        public DbSet<Setting> Settings { get; set; }

        public DbSet<Log> Logs { get; set; }

        public DbSet<Subject> Subjects { get; set; }

        public DbSet<Subject_Branch> Subject_Branches { get; set; }

        public DbSet<Part> Parts { get; set; }

        public DbSet<AnswerType> AnswerTypes { get; set; }

        public DbSet<Passage> Passages { get; set; }

        public DbSet<Question> Questions { get; set; }

        public DbSet<Option> Options { get; set; }

        public DbSet<Test> Tests { get; set; }

        public DbSet<Test_Type> Test_Types { get; set; }

        public DbSet<Semester> Semesters { get; set; }

        public DbSet<Test_Question> Test_Questions { get; set; }

        public DbSet<Faculty> Faculties { get; set; }

        public DbSet<Role_Type> Role_Types { get; set; }

        public DbSet<Employee_Subject> Employee_Subjects { get; set; }

        public DbSet<Information> Informations { get; set; }

        public DbSet<SchoolYear> SchoolYears { get; set; }

        public DbSet<Class> Classes { get; set; }

        public DbSet<Lab> Labs { get; set; }

        public DbSet<TestShift>  TestShifts { get; set; }

        public DbSet<Test_Detail> TestDetails { get; set; }

        public DbSet<TestingFormat> TestingFormats { get; set; }

        public DbSet<Schedule_Test> Schedule_Tests { get; set; }

        public DbSet<Student_Test> Student_Tests { get; set; }
    }
}
