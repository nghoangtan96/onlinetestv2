﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("OPTIONS")]
    public class Option
    {
        [Column("OPT_ID")]
        public long Id { get; set; }
        [Column(TypeName = "ntext")]
        public string OPT_CONTENT { get; set; }
        [Required]
        public bool OPT_ISCORRECT { get; set; }
        public int? OPT_ORDER { get; set; }
        public bool? OPT_ISDELETED { get; set; }
        public int? OPT_STATUS { get; set; }
        public long? OPT_CREATEDBY { get; set; }
        public DateTime? OPT_CREATEDDATE { get; set; }
        public long? OPT_MODIFIEDBY { get; set; }
        public DateTime? OPT_MODIFIEDDATE { get; set; }
        public long OPT_QUEID { get; set; }
        [ForeignKey("OPT_QUEID")]
        public virtual Question Questions { get; set; }
        [ForeignKey("OPT_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("OPT_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
    }
}
