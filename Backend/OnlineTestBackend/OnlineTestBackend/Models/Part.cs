﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("PARTS")]
    public class Part
    {
        [Column("PAR_ID")]
        public long Id { get; set; }
        public string PAR_CODE { get; set; }
        [Required]
        public string PAR_NAME { get; set; }
        public bool? PAR_ISSHUFFLE { get; set; }
        public bool? PAR_ISALLOWSHUFFLEQUESTION { get; set; }
        public bool? PAR_ISALLOWSHUFFLEOPTION { get; set; }
        public bool? PAR_ISALLOWNULLQUESTION { get; set; }
        public bool? PAR_ISALLOWNULLOPTION { get; set; }
        public string PAR_DIRECTION { get; set; }
        public double? PAR_DEFAULTSCORE { get; set; }
        public int? PAR_DEFAULTCOLUMN { get; set; }
        public int PAR_DEFAULTLEVEL { get; set; }
        public int? PAR_ORDER { get; set; }
        public string PAR_NOTE { get; set; }
        public bool? PAR_ISDELETED { get; set; }
        public int? PAR_STATUS { get; set; }
        public long? PAR_CREATEDBY { get; set; }
        public DateTime? PAR_CREATEDDATE { get; set; }
        public long? PAR_MODIFIEDBY { get; set; }
        public DateTime? PAR_MODIFIEDDATE { get; set; }
        [Required]
        public long PAR_SUBID { get; set; }
        [NotMapped]
        public long countQuestion { get; set; }
        [NotMapped]
        public long countPassage { get; set; }
        [ForeignKey("PAR_SUBID")]
        public virtual Subject Subjects { get; set; }
        [ForeignKey("PAR_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("PAR_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        public virtual ICollection<Passage> Passages { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        [NotMapped]
        public long numberOfQuestion { get; set; }

    }
}
