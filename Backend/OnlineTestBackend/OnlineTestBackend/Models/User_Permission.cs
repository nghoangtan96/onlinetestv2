﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("USER_PERMISSIONS")]
    public class User_Permission
    {
        [Column("USPE_ID")]
        public long Id { get; set; }
        [Required]
        public long USPE_USEID { get; set; }
        [Required]
        public int USPE_PERID { get; set; }
        public long? USPE_CREATEDBY { get; set; }
        public DateTime? USPE_CREATEDDATE { get; set; }
        public long? USPE_MODIFIEDBY { get; set; }
        public DateTime? USPE_MODIFIEDDATE { get; set; }
        public int? USPE_STATUS { get; set; }
        public bool? USPE_ISACTIVE { get; set; }
        [ForeignKey("USPE_USEID")]
        public virtual User Users { get; set; }
        [ForeignKey("USPE_PERID")]
        public virtual Permission Permissions { get; set; }
        [ForeignKey("USPE_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("USPE_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
    }
}
