﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("LOGS")]
    public class Log
    {
        [Column("LOG_ID")]
        public long Id { get; set; }
        public DateTime? LOG_DATE { get; set; }
        public string LOG_MODULE { get; set; }
        [Column(TypeName = "ntext")]
        public string LOG_CONTENT { get; set; }
        [Required]
        public long LOG_USEID { get; set; }
        public int? LOG_ACTID { get; set; }
        public int? LOG_TABID { get; set; }
        [ForeignKey("LOG_USEID")]
        public virtual User Users { get; set; }
    }
}
