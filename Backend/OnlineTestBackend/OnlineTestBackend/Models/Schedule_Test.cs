﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("SCHEDULE_TESTS")]
    public class Schedule_Test
    {
        [Column("SCTE_ID")]
        public long Id { get; set; }
        [Required]
        public long SCTE_TESID { get; set; }
        [Required]
        public int SCTE_SHIID { get; set; }
        [Required]
        public DateTime SCTE_DATE { get; set; }
        [Required]
        public int SCTE_LABID { get; set; }
        public long? SCTE_CREATEDBY { get; set; }
        public DateTime? SCTE_CREATEDDATE { get; set; }
        public long? SCTE_MODIFIEDBY { get; set; }
        public DateTime? SCTE_MODIFIEDDATE { get; set; }
        public bool? SCTE_ISDELETED { get; set; }
        public int? SCTE_STATUS { get; set; }
        [ForeignKey("SCTE_TESID")]
        public virtual Test Tests { get; set; }
        [ForeignKey("SCTE_SHIID")]
        public virtual TestShift TestShifts { get; set; }
        [ForeignKey("SCTE_LABID")]
        public virtual Lab Labs { get; set; }
        [ForeignKey("SCTE_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("SCTE_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        public virtual ICollection<Student_Test> Student_Tests { get; set; }
    }
}
