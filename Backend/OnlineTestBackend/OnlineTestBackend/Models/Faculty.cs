﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("FACULTIES")]
    public class Faculty
    {
        [Column("FAC_ID")]
        public int Id { get; set; }
        public string FAC_CODE { get; set; }
        [Required]
        public string FAC_NAME { get; set; }
        public string FAC_ADDRESS { get; set; }
        public string FAC_PHONE { get; set; }
        public string FAC_EMAIL { get; set; }
        public string FAC_WEBSITE { get; set; }
        [Column(TypeName = "ntext")]
        public string FAC_INFORMATION { get; set; }
        public bool? FAC_ISDELETED { get; set; }
        public int? FAC_STATUS { get; set; }
        public long? FAC_CREATEDBY { get; set; }
        public DateTime? FAC_CREATEDDATE { get; set; }
        public long? FAC_MODIFIEDBY { get; set; }
        public DateTime? FAC_MODIFIEDDATE { get; set; }
        [ForeignKey("FAC_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("FAC_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
        public virtual ICollection<Subject> Subjects { get; set; }
        public virtual ICollection<Branch> Branches { get; set; }
    }
}
