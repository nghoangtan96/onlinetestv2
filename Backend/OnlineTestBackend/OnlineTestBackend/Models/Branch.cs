﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("BRANCHES")]
    public class Branch
    {
        [Column("BRA_ID")]
        public int Id { get; set; }
        [MaxLength(20)]
        public string BRA_CODE { get; set; }
        [Required]
        [MaxLength(255)]
        public string BRA_NAME { get; set; }
        public bool? BRA_ISDELETED { get; set; }
        public bool? BRA_ISPARENT { get; set; }
        public int? BRA_STATUS { get; set; }
        public int? BRA_PARENT { get; set; }
        public double? BRA_DURATION { get; set; }
        public int? BRA_LEVEL { get; set; }
        public long? BRA_CREATEDBY { get; set; }
        public DateTime? BRA_CREATEDDATE { get; set; }
        public long? BRA_MODIFIEDBY { get; set; }
        public DateTime? BRA_MODIFIEDDATE { get; set; }
        public int? BRA_FACID { get; set; }
        [ForeignKey("BRA_PARENT")]
        public virtual ICollection<Branch> Branches { get; set; }
        public virtual Branch BranchesParent { get; set; }
        [ForeignKey("BRA_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("BRA_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        public virtual ICollection<Subject_Branch> Subject_Branches { get; set; }
        [ForeignKey("BRA_FACID")]
        public virtual Faculty Faculties { get; set; }
        public virtual ICollection<Student> Students { get; set; }
    }
}
