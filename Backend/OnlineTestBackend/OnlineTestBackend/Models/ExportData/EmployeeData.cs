﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models.ExportData
{
    public class EmployeeData
    {
        [Description("STT")]
        public long stt { get; set; }
        [Description("Mã nhân viên")]
        public string code { get; set; }
        [Description("CMND")]
        public string identity { get; set; }
        [Description("Họ")]
        public string lastname { get; set; }
        [Description("Tên")]
        public string firstname { get; set; }
        [Description("Giới tính")]
        public string gender { get; set; }
        [Description("Ngày sinh")]
        public string dob { get; set; }
        [Description("Số điện thoại")]
        public string phone { get; set; }
        [Description("Email")]
        public string email { get; set; }
        [Description("Địa chỉ")]
        public string address { get; set; }
        [Description("Khoa")]
        public string faculty { get; set; }
        [Description("Chuyên môn")]
        public string subjects { get; set; } = "";
        [Description("Tên tài khoản")]
        public string username { get; set; }
        [Description("Vai trò")]
        public string permission { get; set; } = "";
        [Description("Trạng thái tài khoản")]
        public string accountstatus { get; set; }
    }
}
