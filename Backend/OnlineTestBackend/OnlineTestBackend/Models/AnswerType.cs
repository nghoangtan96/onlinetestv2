﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("ANSWER_TYPES")]
    public class AnswerType
    {
        [Column("ANTY_ID")]
        public int Id { get; set; }
        public string ANTY_NAME { get; set; }
        public int? ANTY_ORDER { get; set; }
        public string ANTY_SAMPLE { get; set; }
        public bool? ANTY_ISDELETED { get; set; }
        public int? ANTY_STATUS { get; set; }
        public long? ANTY_CREATEDBY { get; set; }
        public DateTime? ANTY_CREATEDDATE { get; set; }
        public long? ANTY_MODIFIEDBY { get; set; }
        public DateTime? ANTY_MODIFIEDDATE { get; set; }
        [ForeignKey("ANTY_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("ANTY_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
    }
}
