﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("STUDENTS")]
    public class Student
    {
        [Column("STU_ID")]
        public long Id { get; set; }
        [MaxLength(50)]
        public string STU_CODE { get; set; }
        public string STU_IDENTITY { get; set; }
        [Required]
        [MaxLength(50)]
        public string STU_FIRSTNAME { get; set; }
        [Required]
        [MaxLength(100)]
        public string STU_LASTNAME { get; set; }
        public bool? STU_GENDER { get; set; }
        public DateTime? STU_DATEOFBIRTH { get; set; }
        [MaxLength(15)]
        public string STU_PHONE { get; set; }
        [MaxLength(255)]
        public string STU_EMAIL { get; set; }
        [MaxLength(4000)]
        public string STU_ADDRESS { get; set; }
        [MaxLength(400)]
        public string STU_AVATAR { get; set; }
        public long? STU_CREATEDBY { get; set; }
        public DateTime? STU_CREATEDDATE { get; set; }
        public long? STU_MODIFIEDBY { get; set; }
        public DateTime? STU_MODIFIEDDATE { get; set; }
        [Required]
        public long STU_USEID { get; set; }
        public int? STU_STATUS { get; set; }
        public bool? STU_ISDELETED { get; set; }
        [Required]
        public int STU_BRAID { get; set; }
        [Required]
        public long STU_CLAID { get; set; }
        [Required]
        public int STU_SCYEID { get; set; }
        [NotMapped]
        public string FileUrl { get; set; }
        [ForeignKey("STU_USEID")]
        public virtual User Users { get; set; }
        [NotMapped]
        public virtual User UserCreated { get; set; }
        [NotMapped]
        public virtual User UserModified { get; set; }
        [ForeignKey("STU_BRAID")]
        public virtual Branch Branches { get; set; }
        [ForeignKey("STU_CLAID")]
        public virtual Class Classes { get; set; }
        [ForeignKey("STU_SCYEID")]
        public virtual SchoolYear SchoolYears { get; set; }
        public virtual ICollection<Student_Test> Student_Tests { get; set; }
    }
}
