﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("ROLE_TYPES")]
    public class Role_Type
    {
        [Column("ROTY_ID")]
        public int Id { get; set; }
        public string ROTY_NAME { get; set; }
        public int? ROTY_STATUS { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
}
