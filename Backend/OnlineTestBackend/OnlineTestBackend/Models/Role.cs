﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("ROLES")]
    public class Role
    {
        [Column("ROL_ID")]
        public long Id { get; set; }
        public int? ROL_ACTID { get; set; }
        public int? ROL_TABID { get; set; }
        [MaxLength(2000)]
        public string ROL_URL { get; set; }
        [MaxLength(2000)]
        public string ROL_TRIGGER { get; set; }
        public bool? ROL_ISACTIVE { get; set; }
        public int? ROL_STATUS { get; set; }
        public bool? ROL_ISPARENT { get; set; }
        public long? ROL_PARENT { get; set; }
        public int? ROL_ROTYID { get; set; }
        [ForeignKey("ROL_PARENT")]
        public virtual ICollection<Role> Roles { get; set; }
        [ForeignKey("ROL_ROTYID")]
        public virtual Role_Type Role_Types { get; set; }
        [ForeignKey("ROL_ACTID")]
        public virtual Actionn Actionns { get; set; }
        [ForeignKey("ROL_TABID")]
        public virtual Table Tables { get; set; }
    }
}
