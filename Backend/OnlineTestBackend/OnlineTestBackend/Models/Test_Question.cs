﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("TEST_QUESTIONS")]
    public class Test_Question
    {
        [Column("TEQU_ID")]
        public long Id { get; set; }
        public long? TEQU_TESID { get; set; }
        public long? TEQU_QUEID { get; set; }
        public long? TEQU_PASID { get; set; }
        public long? TEQU_CREATEDBY { get; set; }
        public DateTime? TEQU_CREATEDDATE { get; set; }
        [ForeignKey("TEQU_TESID")]
        public virtual Test Tests { get; set; }
        [ForeignKey("TEQU_QUEID")]
        public virtual Question Questions { get; set; }
        [ForeignKey("TEQU_PASID")]
        public virtual Passage Passages { get; set; }
        [ForeignKey("TEQU_CREATEDBY")]
        public virtual User UserCreated { get; set; }
    }
}
