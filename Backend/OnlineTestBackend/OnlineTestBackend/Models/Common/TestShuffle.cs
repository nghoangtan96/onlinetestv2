﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models.Common
{
    public class TestShuffle
    {
        public string code { get; set; }
        public long[] listQuestion { get; set; }
        public string[] listOption { get; set; }
        public int[] listTypeQuestion { get; set; }
        public PassageShuffle[] listQuestionPassage { get; set; }
    }
}
