﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models.Common
{
    public class PassageShuffle
    {
        public long[] listQuestionOfPassage { get; set; }
        public string[] listOptionOfPassage { get; set; }
    }
}
