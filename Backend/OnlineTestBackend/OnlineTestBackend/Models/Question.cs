﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("QUESTIONS")]
    public class Question
    {
        [Column("QUE_ID")]
        public long Id { get; set; }
        [Column(TypeName = "ntext")]
        public string QUE_CONTENT { get; set; }
        public bool? QUE_ISSHUFFLE { get; set; }
        [Required]
        public double QUE_SCORE { get; set; }
        public int? QUE_OPTCOLUMN { get; set; }
        public bool? QUE_ISBANK { get; set; }
        [Required]
        public int QUE_LEVEL { get; set; }
        public string QUE_MEDIA { get; set; }
        [Column(TypeName = "ntext")]
        public string QUE_REFERENCE { get; set; }
        public int? QUE_ORDER { get; set; }
        public bool? QUE_ISDELETED { get; set; }
        public int? QUE_STATUS { get; set; }
        public long? QUE_CREATEDBY { get; set; }
        public DateTime? QUE_CREATEDDATE { get; set; }
        public long? QUE_MODIFIEDBY { get; set; }
        public DateTime? QUE_MODIFIEDDATE { get; set; }
        public int? QUE_ANTYID { get; set; }
        public long? QUE_PASID { get; set; }
        public long? QUE_PARID { get; set; }
        [NotMapped]
        public string FileUrl { get; set; }
        [NotMapped]
        public int countTest { get; set; }
        [NotMapped]
        public string fileType { get; set; }
        [ForeignKey("QUE_ANTYID")]
        public virtual AnswerType AnswerTypes { get; set; }
        [ForeignKey("QUE_PASID")]
        public virtual Passage Passages { get; set; }
        [ForeignKey("QUE_PARID")]
        public virtual Part Parts { get; set; }
        public virtual ICollection<Option> Options { get; set; }
        [ForeignKey("QUE_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("QUE_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        public virtual ICollection<Test_Question> Test_Questions { get; set; }
    }
}
