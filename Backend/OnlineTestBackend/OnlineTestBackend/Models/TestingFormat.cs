﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("TESTING_FORMATS")]
    public class TestingFormat
    {
        [Column("TESF_ID")]
        public long Id { get; set; }
        [Required]
        public long TESF_TESID { get; set; }
        public bool? TESF_ISFREE { get; set; }
        public bool? TESF_ISSHOWPART { get; set; }
        public bool? TESF_ISSHOWSCORE { get; set; }
        public bool? TESF_ISSHOWPROCTOR { get; set; }
        public bool? TESF_ISSHOWFEEDBACK { get; set; }
        public bool? TESF_ISSHOWSIGNATURE { get; set; }
        [ForeignKey("TESF_TESID")]
        public virtual Test Tests { get; set; }
    }
}
