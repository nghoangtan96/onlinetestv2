﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("TESTSHIFTS")]
    public class TestShift
    {
        [Column("SHI_ID")]
        public int Id { get; set; }
        public string SHI_NAME { get; set; }
        public TimeSpan SHI_TIMESTART { get; set; }
        public int? SHI_STATUS { get; set; }
        public bool? SHI_ISDELETED { get; set; }
        public long? SHI_CREATEDBY { get; set; }
        public DateTime? SHI_CREATEDDATE { get; set; }
        public long? SHI_MODIFIEDBY { get; set; }
        public DateTime? SHI_MODIFIEDDATE { get; set; }
        [ForeignKey("SHI_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("SHI_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        public virtual ICollection<Schedule_Test> Schedule_Tests { get; set; }
    }
}
