﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("USERS")]
    public class User
    {
        [Column("USE_ID")]
        public long Id { get; set; }
        [Required]
        [MaxLength(255)]
        public string USE_USERNAME { get; set; }
        [Required]
        [MaxLength(255)]
        public string USE_PASSWORD { get; set; }
        //public string USE_RAWPASSWORD { get; set; }
        public long? USE_CREATEDBY { get; set; }
        public DateTime? USE_CREATEDDATE { get; set; }
        public long? USE_MODIFIEDBY { get; set; }
        public DateTime? USE_MODIFIEDDATE { get; set; }
        public bool? USE_ISACTIVE { get; set; }
        public int? USE_STATUS { get; set; }
        public bool? USE_ISDELETED { get; set; }
        [ForeignKey("USE_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("USE_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
        public virtual ICollection<Student> Students { get; set; }
        [NotMapped]
        public virtual ICollection<User_Permission> User_Permissions { get; set; }
    }
}
