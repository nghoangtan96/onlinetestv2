﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("TEST_TYPES")]
    public class Test_Type
    {
        [Column("TETY_ID")]
        public int Id { get; set; }
        [Required]
        public string TETY_NAME { get; set; }
        public bool? TETY_ISCURRENT { get; set; }
        public int? TETY_ORDER { get; set; }
        public bool? TETY_ISDELETED { get; set; }
        public int? TETY_STATUS { get; set; }
        public long? TETY_CREATEDBY { get; set; }
        public DateTime? TETY_CREATEDDATE { get; set; }
        public long? TETY_MODIFIEDBY { get; set; }
        public DateTime? TETY_MODIFIEDDATE { get; set; }
        [ForeignKey("TETY_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("TETY_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        public virtual ICollection<Test> Tests { get; set; }
    }
}
