﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("SCHOOLYEARS")]
    public class SchoolYear
    {
        [Column("SCYE_ID")]
        public int Id { get; set; }
        public string SCYE_CODE { get; set; }
        public int? SCYE_STARTYEAR { get; set; }
        public long? SCYE_CREATEDBY { get; set; }
        public DateTime? SCYE_CREATEDDATE { get; set; }
        public long? SCYE_MODIFIEDBY { get; set; }
        public DateTime? SCYE_MODIFIEDDATE { get; set; }
        public int? SCYE_STATUS { get; set; }
        public bool? SCYE_ISDELETED { get; set; }
        [ForeignKey("SCYE_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("SCYE_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        public virtual ICollection<Student> Students { get; set; }
    }
}
