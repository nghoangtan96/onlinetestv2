﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("PASSAGES")]
    public class Passage
    {
        [Column("PAS_ID")]
        public long Id { get; set; }
        [Column(TypeName = "ntext")]
        public string PAS_CONTENT { get; set; }
        public bool? PAS_ISSHUFFLE { get; set; }
        public bool? PAS_ISSHOWALL { get; set; }
        public string PAS_MEDIA { get; set; }
        public bool? PAS_ISDELETED { get; set; }
        public int? PAS_STATUS { get; set; }
        public int? PAS_ORDER { get; set; }
        public int? PAS_ANTYID { get; set; }
        public long? PAS_PARID { get; set; }
        public long? PAS_CREATEDBY { get; set; }
        public DateTime? PAS_CREATEDDATE { get; set; }
        public long? PAS_MODIFIEDBY { get; set; }
        public DateTime? PAS_MODIFIEDDATE { get; set; }
        [NotMapped]
        public string FileUrl { get; set; }
        [NotMapped]
        public int countTest { get; set; }
        [ForeignKey("PAS_ANTYID")]
        public virtual AnswerType AnswerTypes { get; set; }
        [ForeignKey("PAS_PARID")]
        public virtual Part Parts { get; set; }
        [ForeignKey("PAS_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("PAS_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<Test_Question> Test_Questions { get; set; }
    }
}
