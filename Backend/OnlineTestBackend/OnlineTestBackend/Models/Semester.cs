﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("SEMESTERS")]
    public class Semester
    {
        [Column("SEM_ID")]
        public int Id { get; set; }
        [Required]
        public int SEM_SEMESTER { get; set; }
        [Required]
        public int SEM_YEAR { get; set; }
        public bool? SEM_ISCURRENT { get; set; }
        public int? SEM_STATUS { get; set; }
        public bool? SEM_ISDELETED { get; set; }
        public long? SEM_CREATEDBY { get; set; }
        public DateTime? SEM_CREATEDDATE { get; set; }
        public long? SEM_MODIFIEDBY { get; set; }
        public DateTime? SEM_MODIFIEDDATE { get; set; }
        [ForeignKey("SEM_CREATEDBY")]
        public virtual User UserCreated { get; set; }
        [ForeignKey("SEM_MODIFIEDBY")]
        public virtual User UserModified { get; set; }
    }
}
