﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("ACTIONS")]
    public class Actionn
    {
        [Column("ACT_ID")]
        public int Id { get; set; }
        [Required]
        [MaxLength(150)]
        public string ACT_NAME { get; set; }
        public int? ACT_STATUS { get; set; }
        public string ACT_CODE { get; set; }
    }
}
