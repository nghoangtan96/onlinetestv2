﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("TEST_DETAILS")]
    public class Test_Detail
    {
        [Column("TEDE_ID")]
        public long Id { get; set; }
        public string TEDE_CODE { get; set; }
        [Required]
        public long TEDE_TESID { get; set; }
        public long? TEDE_QUEID { get; set; }
        public long? TEDE_PASID { get; set; }
        public string TEDE_QUEOPTION { get; set; }
        public string TEDE_PASQUESTION { get; set; }
        public string TEDE_PASOPTION { get; set; }
        public long? TEDE_CREATEDBY { get; set; }
        public DateTime? TEDE_CREATEDDATE { get; set; }
        public bool? TEDE_ISDELETED { get; set; }
        [ForeignKey("TEDE_TESID")]
        public virtual Test Tests { get; set; }
        [ForeignKey("TEDE_QUEID")]
        public virtual Question Questions { get; set; }
        [ForeignKey("TEDE_PASID")]
        public virtual Passage Passages { get; set; }
        [ForeignKey("TEDE_CREATEDBY")]
        public virtual User UserCreated { get; set; }
    }
}
