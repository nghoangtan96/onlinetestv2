﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("SETTING_TYPES")]
    public class SettingType
    {
        [Column("SETY_ID")]
        public int Id { get; set; }
        [Required]
        [MaxLength(255)]
        public string SETY_NAME { get; set; }
        public virtual ICollection<Setting> Settings { get; set; }
    }
}
