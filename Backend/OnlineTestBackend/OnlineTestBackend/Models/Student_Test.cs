﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTestBackend.Models
{
    [Table("STUDENT_TESTS")]
    public class Student_Test
    {
        [Column("STTE_ID")]
        public long Id { get; set; }
        public string STTE_TESTCODE { get; set; }
        public long? STTE_SCTEID { get; set; }
        public long? STTE_STUID { get; set; }
        public string STTE_QUESTION { get; set; }
        public string STTE_OPTION { get; set; }
        public string STTE_PASSWORD { get; set; }
        public long? STTE_REMAININGTIME { get; set; }
        public double? STTE_SCORE { get; set; }
        public long? STTE_AUTONUMBER { get; set; }
        public string STTE_HASH { get; set; }
        public DateTime? STTE_CREATEDDATE { get; set; }
        public DateTime? STTE_MODIFIEDDATE { get; set; }
        public int? STTE_STATUS { get; set; }
        public long? STTE_CURRENTPART { get; set; }
        public int? STTE_CURRENTTIMEAUDIO { get; set; }
        [ForeignKey("STTE_SCTEID")]
        public virtual Schedule_Test Schedule_Tests { get; set; }
        [ForeignKey("STTE_STUID")]
        public virtual Student Students { get; set; }
    }
}
