import { Component, OnDestroy, Inject, ViewChild, Renderer2, HostListener, ElementRef } from '@angular/core';
import { DOCUMENT, Location } from '@angular/common';
import { navItems, NavData } from '../../_nav';
import { User, UserService } from '../../services/user.service';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { UserPermissionService, User_Permission } from '../../services/user-permission.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NoficationService, Nofication } from '../../services/nofication.service';
import { changePasswordRequest } from '../../models/changePassword_request';
import { PnotifyService } from '../../services/pnotify.service';
import { InformationService, Information } from '../../services/information.service';
import { RoleService } from '../../services/role.service';
import { Setting, SettingService } from '../../services/setting.service';
import { SignalrService } from '../../services/signalr.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnDestroy {

  informations: [Information];
  nofications: [Nofication];
  user: User = {} as User;
  changePasswordRequest: changePasswordRequest = {} as changePasswordRequest;

  userPers: [User_Permission];
  nofication_count: number = 0;
  isHaveNewNof: boolean = true;
  isHaveOldNof: boolean = true;
  lengthPassword: number;

  navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;

  user_id = parseInt(this.cookieSer.get('Id'));
  isLogin = parseInt(this.cookieSer.get('isLogin'));
  pnotify_layout = undefined;

  @ViewChild('modalChangePassword', { static: false }) modalChangePassword: ModalDirective;
  @ViewChild('modalConfirmLayout', { static: false }) modalConfirmLayout: ModalDirective;

  constructor(private useSer: UserService, private cookieSer: CookieService,
    private router: Router, private userPermissionSer: UserPermissionService,
    private settingSer: SettingService, private _DomSanitizer: DomSanitizer, //unsafe bug fix
    private noficationSer: NoficationService, private location: Location, private authSer: AuthService,
    private pNotify: PnotifyService, private informationSer: InformationService,
    private roleSer: RoleService, private signalRService: SignalrService,
    // private renderer2: Renderer2, @Inject(DOCUMENT) private _documentscript,
    @Inject(DOCUMENT) _document?: any) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
    this.pnotify_layout = this.pNotify.getPNotify();
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  ngOnInit() {
    this.informationSer.getAll().subscribe(res => {
      this.informations = res.data;
      if (res.data.find(x => x.inF_KEYNAME == "Logo").inF_VALUE == null) {
        $(".navbar-brand-full").attr('src', "assets/img/default_logo.png");
        $(".navbar-brand-minimized").attr('src', "assets/img/default_logo.png");
      }
      else {
        $(".navbar-brand-full").attr('src', res.data.find(x => x.inF_KEYNAME == "Logo").fileUrl);
        $(".navbar-brand-minimized").attr('src', res.data.find(x => x.inF_KEYNAME == "Logo").fileUrl);
      }
    });
    this.useSer.get(this.user_id).subscribe(res => {
      this.user = res.data;
      if (res.data.employees != null && res.data.employees[0] != null) {
        $("#text_username").html(res.data.employees[0].emP_LASTNAME + " " + res.data.employees[0].emP_FIRSTNAME);
        if (res.data.employees[0].emP_AVATAR == null) {
          $("#ava_icon").attr("src", "assets/img/default_avatar.png");
        }
        else {
          $("#ava_icon").attr("src", res.data.employees[0].fileUrl);
        }
      }
      else if (res.data.students != null && res.data.students[0] != null) {
        $("#text_username").html(res.data.students[0].stU_LASTNAME + " " + res.data.students[0].stU_FIRSTNAME);
        if (res.data.students[0].stU_AVATAR == null) {
          $("#ava_icon").attr("src", "assets/img/default_avatar.png");
        }
        else {
          $("#ava_icon").attr("src", res.data.students[0].fileUrl);
        }
      }
    });
    this.noficationSer.getTop5NoficationNewestOrAllNotReadByUserId(this.user_id).subscribe(res => {
      this.nofications = res.data;
      for (let i = 0; i < res.data.length; i++) {
        if (res.data[i].noF_STATUS == 1)
          this.nofication_count++;
      }
      if (res.data.find(x => x.noF_STATUS == 1) == null) {
        this.isHaveNewNof = false;
      }
      if (res.data.find(x => x.noF_STATUS == 0) == null) {
        this.isHaveOldNof = false;
      }
    });
    this.settingSer.get(2).subscribe(res => {
      this.lengthPassword = res.data.seT_VALUE;
    });

    //permission load menu
    this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get("Id"))).subscribe(res => {
      this.roleSer.getAll().subscribe(res1 => {
        let countAll = 0;
        let countParentMenuQuestion = 0;
        let countParentMenuUser = 0;
        let countParentMenuFaculty = 0;
        let countParentMenuResource = 0;
        let countParentMenuSystem = 0;
        for (let i = 0; i < res1.data.length; i++) {
          let count = 0;
          for (let j = 0; j < res.data.length; j++) {
            if (res.data[j].permissions.peR_STATUS == 1) {
              if (res.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res1.data[i].id).ropE_ISACTIVE == true) {
                countAll++;
                count++;
              }
              if (res1.data[i].roL_TABID == 1 || res1.data[i].roL_TABID == 2) {
                if (res.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res1.data[i].id).ropE_ISACTIVE == true)
                  countParentMenuQuestion++;
              }
              if (res1.data[i].roL_TABID == 9 || res1.data[i].roL_TABID == 14 || res1.data[i].roL_TABID == 8) {
                if (res.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res1.data[i].id).ropE_ISACTIVE == true)
                  countParentMenuUser++;
              }
              if (res1.data[i].roL_TABID == 7 || res1.data[i].roL_TABID == 6 || res1.data[i].roL_TABID == 5 || res1.data[i].roL_TABID == 3 || res1.data[i].roL_TABID == 13 || res1.data[i].roL_TABID == 1016 || res1.data[i].roL_TABID == 1017) {
                if (res.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res1.data[i].id).ropE_ISACTIVE == true)
                  countParentMenuFaculty++;
              }
              if (res1.data[i].roL_TABID == 4 || res1.data[i].roL_TABID == 12 || res1.data[i].roL_TABID == 1015 || res1.data[i].roL_TABID == 1018 || res1.data[i].roL_TABID == 1019) {
                if (res.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res1.data[i].id).ropE_ISACTIVE == true)
                  countParentMenuResource++;
              }
              if (res1.data[i].roL_TABID == 10 || res1.data[i].roL_TABID == 1020 || res1.data[i].roL_TABID == 1021) {
                if (res.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res1.data[i].id).ropE_ISACTIVE == true)
                  countParentMenuSystem++;
              }
            }
          }

          if (count > 0) {
            $("." + res1.data[i].roL_TRIGGER).show();
          }
          else {
            $("." + res1.data[i].roL_TRIGGER).hide();
          }
        }

        if (countParentMenuQuestion > 0) {
          $(".questiongroup").show();
        }
        else {
          $(".questiongroup").hide();
        }

        if (countParentMenuUser > 0) {
          $(".usergroup").show();
        }
        else {
          $(".usergroup").hide();
        }

        if (countParentMenuFaculty > 0) {
          $(".facultygroup").show();
        }
        else {
          $(".facultygroup").hide();
        }
        
        if (countParentMenuResource > 0) {
          $(".resourcegroup").show();
        }
        else {
          $(".resourcegroup").hide();
        }

        if (countParentMenuSystem > 0) {
          $(".systemgroup").show();
        }
        else {
          $(".systemgroup").hide();
        }

        if (countAll > 0) {
          $(".manager").show();
        }
        else {
          $(".manager").hide();
        }
      });
    });
    //realtime
    this.signalRService.nofication.subscribe((res) => {
      if (res === true) {
        this.noficationSer.getTop5NoficationNewestOrAllNotReadByUserId(this.user_id).subscribe(res => {
          this.nofications = res.data;
          this.nofication_count = 0;
          for (let i = 0; i < res.data.length; i++) {
            if (res.data[i].noF_STATUS == 1)
              this.nofication_count++;
          }
          if (res.data.find(x => x.noF_STATUS == 1) == null) {
            this.isHaveNewNof = false;
          }
          if (res.data.find(x => x.noF_STATUS == 0) == null) {
            this.isHaveOldNof = false;
          }
        });
      }
    });
    this.signalRService.information.subscribe((res1) => {
      if (res1 === true) {
        this.informationSer.getAll().subscribe(res => {
          this.informations = res.data;
          if (res.data.find(x => x.inF_KEYNAME == "Logo").inF_VALUE == null) {
            $(".navbar-brand-full").attr('src', "assets/img/default_logo.png");
            $(".navbar-brand-minimized").attr('src', "assets/img/default_logo.png");
          }
          else {
            $(".navbar-brand-full").attr('src', res.data.find(x => x.inF_KEYNAME == "Logo").fileUrl);
            $(".navbar-brand-minimized").attr('src', res.data.find(x => x.inF_KEYNAME == "Logo").fileUrl);
          }
        });
      }
    });
  }

  toggleUserManager(event = null) {
    if (event != null) {
      event.preventDefault();
    }
  }

  openMessageDropdown(event = null) {
    if (event != null) {
      event.preventDefault();
    }
    if (this.nofication_count != 0) {
      this.noficationSer.messageRead(Number(this.cookieSer.get('Id'))).subscribe(res => {
        this.nofication_count = 0;
      });
    }
  }

  Logout() {
    this.cookieSer.deleteAll();
    this.router.navigate(['/login']);
  }
  validate_CP_oldpassword() {
    if ($("#field_CP_1").val() == '') {
      $("#field_CP_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_CP_1").html("Vui lòng nhập mật khẩu cũ!");
      return false;
    }
    else {
      $("#field_CP_1").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_CP_1").html("");
      return true;
    }
  }
  validate_CP_newpassword() {
    if ($("#field_CP_2").val() == '') {
      $("#field_CP_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_CP_2").html("Vui lòng nhập mật khẩu mới!");
      return false;
    }
    else if (($("#field_CP_2").val()).toString().length < this.lengthPassword) {
      $("#field_CP_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_CP_2").html("Độ dài mật khẩu phải từ " + this.lengthPassword + " ký tự trở lên!");
      return false;
    }
    else {
      $("#field_CP_2").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_CP_2").html("");
      return true;
    }
  }
  validate_CP_confirmnewpassword() {
    if ($("#field_CP_3").val() == '') {
      $("#field_CP_3").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_CP_3").html("Vui lòng xác nhận mật khẩu mới!");
      return false;
    }
    else {
      if ($("#field_CP_2").val() != $("#field_CP_3").val()) {
        $("#field_CP_3").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_CP_3").html("Xác nhận mật khẩu mới không khớp!");
        return false;
      }
      else {
        $("#field_CP_3").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_CP_3").html("");
        return true;
      }
    }
  }
  validate() {
    var check = true;
    if (this.validate_CP_oldpassword() == false)
      check = false;
    if (this.validate_CP_newpassword() == false)
      check = false;
    if (this.validate_CP_confirmnewpassword() == false)
      check = false;
    //validate all
    if (check == false)
      return false;
    else
      return true;
  }
  clearField() {
    for (let i = 1; i <= 3; i++) {
      $("#field_CP_" + i).removeClass('is-invalid is-valid');
      $("#field_CP_" + i).val("");
      $("#valid_mes_CP_" + i).html("");
    }
  }
  OpenModalChangePassword(event = null) {
    this.unlockModal("upd");
    if (event != null) {
      event.preventDefault();
    }
    this.clearField();
    this.modalChangePassword.show();
  }
  ChangePassword() {
    if (this.validate() == false) {
      return false;
    }
    this.lockModal("upd");
    this.changePasswordRequest.userId = Number(this.cookieSer.get('Id'));
    this.authSer.changePassword(this.changePasswordRequest).subscribe(res => {
      if (res.errorCode == 0) {
        this.pnotify_layout.success({
          title: 'Thông báo',
          text: 'Đổi mật khẩu thành công!'
        });
        this.modalChangePassword.hide();
        $("#message_layout").html("Mật khẩu đã thay đổi. Bạn có muốn đăng nhập lại?");
        this.modalConfirmLayout.show();
      }
      else if (res.errorCode == 2) {
        this.unlockModal("upd");
        $("#field_CP_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_CP_1").html("Mật khẩu cũ không chính xác!");
        return false;
      }
    });

  }

  lockModal(key: string) {
    $(".close_modal_CP_" + key).prop("disabled", true);
    $("#save_modal_CP_" + key).prop("disabled", true);
    $("#save_modal_CP_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_CP_" + key).prop("disabled", false);
    $("#save_modal_CP_" + key).prop("disabled", false);
    $("#save_modal_CP_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }
}
