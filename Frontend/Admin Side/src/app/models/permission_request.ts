import { Permission } from '../services/permission.service';
import { Role } from '../services/role.service';
import { RolePermission } from '../services/role-permission.service';

export interface permissionRequest {
    permission: Permission;
    listRolePermission: RolePermission[];
}