import { ScheduleTest } from '../services/schedule-test.service';
import { Student } from '../services/student.service';

export interface ScheduleTestRequest{
    scheduleTest: ScheduleTest;
    listStudent: Student[];
}