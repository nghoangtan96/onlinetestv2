import { User } from '../services/user.service';
import { Employee } from '../services/employee.service';
import { User_Permission } from '../services/user-permission.service';

export interface EmployeeRequest{
    user: User;
    employee: Employee;
    user_permissions: User_Permission[];
    list_subject: number[];
}