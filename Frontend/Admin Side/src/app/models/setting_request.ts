export interface settingRequest{
    userId: number;
    id: number;
    value: number;
    isactive: boolean;
    choicevalue: string;
    multichoicevalue: string;
}