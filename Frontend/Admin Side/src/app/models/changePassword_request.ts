export interface changePasswordRequest{
    userId: number;
    oldPassword: string;
    newPassword: string;
}