import { Question } from '../services/question.service';
import { Option } from '../services/option.service';

export interface questionRequest {
    question: Question;
    listOption: Option[];
}