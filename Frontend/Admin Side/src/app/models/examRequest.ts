export interface ExamRequest{
    type: number;
    userId: number;
    scheduleTestId: number;
    remainingTime: number;
    listQuestion: string;
    listOption: string;
    currentPart: number;
    currentTimeAudio: number;
}