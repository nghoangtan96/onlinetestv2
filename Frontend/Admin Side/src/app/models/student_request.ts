import { User } from '../services/user.service';
import { Student } from '../services/student.service';

export interface StudentRequest{
    user: User;
    student: Student;
    user_permissions: number[];
}