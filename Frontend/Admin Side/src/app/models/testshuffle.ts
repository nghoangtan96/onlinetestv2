export interface TestShuffle {
    code: string;
    listQuestion: number[];
    listOption: string[];
    listTypeQuestion: number[];
    listQuestionPassage: QuestionOfPassage[];
}
export interface QuestionOfPassage {
    listQuestionOfPassage: number[];
    listOptionOfPassage: string[];
}