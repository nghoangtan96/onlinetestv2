import { Test } from '../services/test.service';
import { TestQuestion } from '../services/test-question.service';
import { TestShuffle } from './testshuffle';

export interface TestRequest {
    // public Test test { get; set; }
    // public Test_Question[] listQuestionRaw { get; set; }
    // public int shuffle { get; set; }
    // public TestShuffle[] listTestShuffle { get; set; }
    // public int testFormat { get; set; }
    // public int numberOfQuestion { get; set; }
    // public int numberOfTestShuffle { get; set; }
    test: Test;
    testQuestion: TestQuestion[];
    shuffle: number;
    showScore: boolean;
    testShuffle: TestShuffle[];
    testFormat: number;
    numberOfQuestion: number;
    numberOfTest: number;
}