interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    title: true,
    name: 'Quản lý',
    class: "manager"
  },
  {
    name: 'Quản lý ng dùng',
    url: '/users',
    icon: 'fa fa-users',
    class: "usergroup",
    children: [
      // {
      //   name: 'Tài khoản',
      //   url: '/user',
      //   icon: 'fa fa-user-plus',
      //   class: "use_all"
      // },
      {
        name: 'Quản lý vai trò',
        url: '/permission',
        class: 'per_all',
        icon: 'fa fa-lock'
      },
      {
        name: 'Nhân viên',
        url: '/employee',
        icon: 'fa fa-user-o',
        class: "emp_all"
      },
      {
        name: 'Sinh viên',
        url: '/student',
        icon: 'fa fa-graduation-cap',
        class: "stu_all"
      }
    ]
  },
  {
    name: 'Quản lý khoa/ bộ môn',
    url: '/users',
    icon: 'fa fa-home',
    class: "facultygroup",
    children: [
      {
        name: 'Quản lý khoa',
        url: '/faculty',
        class: 'fac_all',
        icon: 'fa fa-university'
      },
      {
        name: 'Quản lý ngành học',
        url: '/branch',
        class: 'bra_all',
        icon: 'fa fa-graduation-cap',
      },
      {
        name: 'Quản lý môn học',
        url: '/subject',
        class: 'sub_all',
        icon: 'fa fa-book'
      },
      {
        name: 'Quản lý học phần',
        url: '/part',
        class: 'par_all',
        icon: 'fa fa-file'
      },
      {
        name: 'Quản lý lớp học',
        url: '/class',
        class: 'cla_all',
        icon: 'fa fa-object-group'
      },
      {
        name: 'Quản lý đề thi',
        url: '/test',
        class: 'tes_all',
        icon: 'fa fa-file-text-o'
      },
      {
        name: 'Quản lý lịch thi',
        url: '/scheduletest',
        class: 'scte_all',
        icon: 'fa fa-calendar'
      },
    ]
  },
  {
    name: 'Quản lý tài nguyên',
    url: '/users',
    icon: 'fa fa-tree',
    class: "resourcegroup",
    children: [
      {
        name: 'Quản lý niên khóa',
        url: '/schoolyear',
        class: 'scye_all',
        icon: 'fa fa-industry'
      },
      {
        name: 'Quản lý phòng thi',
        url: '/lab',
        class: 'lab_all',
        icon: 'fa fa-home'
      },
      {
        name: 'Quản lý ca thi',
        url: '/testshift',
        class: 'tesf_all',
        icon: 'fa fa-clock-o'
      },
      {
        name: 'Quản lý loại bài thi',
        url: '/testtype',
        class: 'test_all',
        icon: 'fa fa-trophy'
      },
      {
        name: 'Loại câu hỏi',
        url: '/answertype',
        class: 'ans_all',
        icon: 'fa fa-tag'
      }
    ]
  },
  // {
  //   name: 'Thi',
  //   url: '/exam',
  //   icon: 'fa fa-archive'
  // },
  // {
  //   name: 'Quản lý thông báo',
  //   url: '/nofication',
  //   class: 'nof_all',
  //   icon: 'fa fa-bell'
  // },
  {
    name: 'Quản lý câu hỏi',
    url: '/quesmanager',
    icon: 'fa fa-question-circle',
    class: 'questiongroup',
    children: [
      {
        name: 'Câu hỏi đoạn văn',
        url: '/passage',
        class: 'pas_all',
        icon: 'fa fa-cubes'
      },
      {
        name: 'Câu hỏi đơn',
        url: '/question',
        class: 'que_all',
        icon: 'fa fa-cube'
      },
    ]
  },
  {
    name: 'Quản lý hệ thống',
    url: '/users',
    icon: 'fa fa-cog',
    class: "systemgroup",
    children: [
      {
        name: 'Quản lý lịch sử',
        url: '/log',
        icon: 'fa fa-search',
        class: "log_all"
      },
      {
        name: 'Quản lý trang web',
        url: '/information',
        icon: 'fa fa-info',
        class: "inf_all"
      },
      {
        name: 'Thiết lập hệ thống',
        url: '/setting',
        icon: 'fa fa-cog',
        class: "set_all"
      }
    ]
  },
  {
    title: true,
    name: 'Sinh viên',
    class: "student"
  },
  {
    name: 'Lịch thi',
    url: '/scheduletestview',
    icon: 'fa fa-calendar'
  },
  {
    name: 'Phòng thi',
    url: '/roomview',
    icon: 'fa fa-archive'
  },
  {
    name: 'Tra cứu điểm',
    url: '/result',
    icon: 'fa fa-diamond'
  }
];

// {
//   name: 'Disabled',
//   url: '/dashboard',
//   icon: 'icon-ban',
//   badge: {
//     variant: 'secondary',
//     text: 'NEW'
//   },
//   attributes: { disabled: true },
// },
// {
//   name: 'Download CoreUI',
//   url: 'http://coreui.io/angular/',
//   icon: 'icon-cloud-download',
//   class: 'mt-auto',
//   variant: 'success',
//   attributes: { target: '_blank', rel: 'noopener' }
// },
// {
//   name: 'Try CoreUI PRO',
//   url: 'http://coreui.io/pro/angular/',
//   icon: 'icon-layers',
//   variant: 'danger',
//   attributes: { target: '_blank', rel: 'noopener' }
// }
