import {
  NgModule
} from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';

// Import Containers
import {
  DefaultLayoutComponent
} from './containers';

import {
  P404Component
} from './views/error/404.component';
import {
  P500Component
} from './views/error/500.component';
import {
  LoginComponent
} from './views/login/login.component';
import {
  RegisterComponent
} from './views/register/register.component';
import {
  TesttypeComponent
} from './views/testtype/testtype.component';
import {
  InformationService
} from './services/information.service';
import {
  InformationComponent
} from './views/information/information.component';
import {
  BarComponent
} from 'ngx-bootstrap/progressbar';
import {
  UserComponent
} from './views/user/user.component';
import {
  EmployeeComponent
} from './views/employee/employee.component';
import {
  ClassComponent
} from './views/class/class.component';
import {
  PositionComponent
} from './views/position/position.component';
import {
  StudentComponent
} from './views/student/student.component';
import {
  InfoUserComponent
} from './views/info-user/info-user.component';
import {
  ReportsComponent
} from './views/reports/reports.component';
import {
  AuthGuard
} from './helpers/auth.guard';
import { SettingComponent } from './views/setting/setting.component';
import { LogComponent } from './views/log/log.component';
import { BranchComponent } from './views/branch/branch.component';
import { SubjectComponent } from './views/subject/subject.component';
import { PartComponent } from './views/part/part.component';
import { AnswertypeComponent } from './views/answertype/answertype.component';
import { QuestionComponent } from './views/question/question.component';
import { PassageComponent } from './views/passage/passage.component';
import { FacultyComponent } from './views/faculty/faculty.component';
import { PermissionComponent } from './views/permission/permission.component';
import { NoficationComponent } from './views/nofication/nofication.component';
import { SchoolyearComponent } from './views/schoolyear/schoolyear.component';
import { LabComponent } from './views/lab/lab.component';
import { TestshiftComponent } from './views/testshift/testshift.component';
import { TestComponent } from './views/test/test.component';
import { ScheduleTestComponent } from './views/schedule-test/schedule-test.component';
import { ScheduleviewComponent } from './views/scheduleview/scheduleview.component';
import { RoomviewComponent } from './views/roomview/roomview.component';
import { ExamComponent } from './views/exam/exam.component';
import { ResultComponent } from './views/result/result.component';
// import { FileTeacherComponent } from './views/file-teacher/file-teacher.component';

export const routes: Routes = [{
  path: '',
  redirectTo: 'info_user',
  pathMatch: 'full',
},
{
  path: '404',
  component: P404Component,
  data: {
    title: 'Page 404'
  }
},
{
  path: '500',
  component: P500Component,
  data: {
    title: 'Page 500'
  }
},
{
  path: 'login',
  component: LoginComponent,
  data: {
    title: 'Login Page'
  }
},
{
  path: 'exam',
  component: ExamComponent,
  canActivate: [AuthGuard],
  data: {
    title: 'Thi'
  }
},
{
  path: '',
  component: DefaultLayoutComponent,
  canActivate: [AuthGuard],
  data: {
    title: 'Trang chính'
  },
  children: [
    // {
    //   path: 'user',
    //   component: UserComponent,
    //   canActivate: [AuthGuard],
    //   data: {
    //     title: 'Quản lý người dùng'
    //   }
    // },
    {
      path: 'setting',
      component: SettingComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Thiết lập hệ thống'
      }
    },
    {
      path: 'branch',
      component: BranchComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý ngành học'
      }
    },
    {
      path: 'subject',
      component: SubjectComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý môn học'
      }
    },
    {
      path: 'part',
      component: PartComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý học phần'
      }
    },
    {
      path: 'answertype',
      component: AnswertypeComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý loại câu hỏi'
      }
    },
    {
      path: 'passage',
      component: PassageComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý câu hỏi đoạn văn'
      }
    },
    {
      path: 'faculty',
      component: FacultyComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý khoa'
      }
    },
    {
      path: 'permission',
      component: PermissionComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý vai trò'
      }
    },
    {
      path: 'question',
      component: QuestionComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý câu hỏi đơn'
      }
    },
    {
      path: 'employee',
      component: EmployeeComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý nhân viên'
      }
    },
    {
      path: 'student',
      component: StudentComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý sinh viên'
      }
    },
    {
      path: 'position',
      component: PositionComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý chức vụ'
      }
    },
    {
      path: 'class',
      component: ClassComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý lớp học'
      }
    },
    {
      path: 'lab',
      component: LabComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý phòng thi'
      }
    },
    {
      path: 'testshift',
      component: TestshiftComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý ca thi'
      }
    },
    {
      path: 'testtype',
      component: TesttypeComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý loại bài thi'
      }
    },
    {
      path: 'test',
      component: TestComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý bài thi'
      }
    },
    {
      path: 'scheduletest',
      component: ScheduleTestComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý lịch thi'
      }
    },
    {
      path: 'scheduletestview',
      component: ScheduleviewComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Lịch thi'
      }
    },
    {
      path: 'roomview',
      component: RoomviewComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Phòng thi'
      }
    },
    {
      path: 'result',
      component: ResultComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Tra cứu điểm'
      }
    },
    {
      path: 'testtype',
      component: TesttypeComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý loại thi'
      }
    },
    {
      path: 'information',
      component: InformationComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý trang web'
      }
    },
    {
      path: 'nofication',
      component: NoficationComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý thông báo'
      }
    },
    {
      path: 'schoolyear',
      component: SchoolyearComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý niên khóa'
      }
    },
    {
      path: 'info_user',
      component: InfoUserComponent,
      data: {
        title: 'Thông tin tài khoản'
      }
    },
    {
      path: 'reports',
      component: ReportsComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Thống kê'
      }
    },
    {
      path: 'log',
      component: LogComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Quản lý lịch sử'
      }
    }
  ]
},
{
  path: '**',
  component: P404Component
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
