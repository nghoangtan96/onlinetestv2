import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService, User } from '../../services/user.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { UserPermissionService, User_Permission } from '../../services/user-permission.service';
import { DomSanitizer } from '@angular/platform-browser';
import { FileService } from '../../services/file.service';
import { PermissionService } from '../../services/permission.service';
import { EmployeeService, Employee } from '../../services/employee.service';
import { StudentService, Student } from '../../services/student.service';
import { PnotifyService } from '../../services/pnotify.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PositionService } from '../../services/position.service';
import { EmployeePositionService, EmployeePosition } from '../../services/employee-position.service';
import { EmployeeRequest } from '../../models/employee_request';
import { StudentRequest } from '../../models/student_request';
import { SettingService } from '../../services/setting.service';
import { changePasswordRequest } from '../../models/changePassword_request';
import { AuthService } from '../../services/auth.service';
import { StudentTestService } from '../../services/student-test.service';
import { SignalrService } from '../../services/signalr.service';

@Component({
  selector: 'app-info-user',
  templateUrl: './info-user.component.html',
  styleUrls: ['./info-user.component.scss']
})
export class InfoUserComponent implements OnInit {

  user: User = {} as User;
  employee: Employee;
  student: Student;
  changePasswordRequest: changePasswordRequest = {} as changePasswordRequest;

  pnotify = undefined;
  lengthPassword: number;

  @ViewChild('modalChangePassword', { static: false }) modalChangePassword: ModalDirective;
  @ViewChild('modalConfirmLayout', { static: false }) modalConfirmLayout: ModalDirective;
  constructor(private userSer: UserService, private pNotify: PnotifyService,
    private cookieSer: CookieService, private router: Router, private authSer: AuthService,
    private employeeSer: EmployeeService, private studentSer: StudentService,
    private _DomSanitizer: DomSanitizer, private settingSer: SettingService,
    private studentTestSer: StudentTestService, private signalRSer: SignalrService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.userSer.get(parseInt(this.cookieSer.get("Id"))).subscribe(res => {
      this.user = res.data;
      if (res.errorCode === 0) {
        if (res.data.employees != null && res.data.employees[0] != null) {
          this.employeeSer.get(res.data.employees[0].id).subscribe(res1 => {
            this.employee = res1.data;
          });
        }
        else {
          this.studentSer.get(res.data.students[0].id).subscribe(res1 => {
            this.student = res1.data;
            // console.log(res1.data);
          });
        }
      }
    });
    this.settingSer.get(2).subscribe(res => {
      this.lengthPassword = res.data.seT_VALUE;
    });
    //navigate interrupt test
    this.studentTestSer.getAllByUserId(Number(this.cookieSer.get('Id'))).subscribe(res => {
      for (let i = 0; i < res.data.length; i++) {
        if (res.data.find(x => x.sttE_STATUS == 2) != null) {
          this.router.navigate(['/exam'], { queryParams: { scheduleTestId: res.data.find(x => x.sttE_STATUS == 2).sttE_SCTEID } })
        }
      }
    });
    //realtime
    this.signalRSer.employee.subscribe((res1) => {
      if (res1 === true) {
        this.userSer.get(parseInt(this.cookieSer.get("Id"))).subscribe(res => {
          this.user = res.data;
          if (res.errorCode === 0) {
            if (res.data.employees != null) {
              this.employeeSer.get(res.data.employees[0].id).subscribe(res1 => {
                this.employee = res1.data;
              });
            }
            else {
              this.studentSer.get(res.data.students[0].id).subscribe(res1 => {
                this.student = res1.data;
                console.log(res1.data);
              });
            }
          }
        });
      }
    });
    this.signalRSer.student.subscribe((res1) => {
      if (res1 === true) {
        this.userSer.get(parseInt(this.cookieSer.get("Id"))).subscribe(res => {
          this.user = res.data;
          if (res.errorCode === 0) {
            if (res.data.employees != null) {
              this.employeeSer.get(res.data.employees[0].id).subscribe(res1 => {
                this.employee = res1.data;
              });
            }
            else {
              this.studentSer.get(res.data.students[0].id).subscribe(res1 => {
                this.student = res1.data;
                console.log(res1.data);
              });
            }
          }
        });
      }
    });
    this.signalRSer.setting.subscribe((res1) => {
      if (res1 === true) {
        this.settingSer.get(2).subscribe(res => {
          this.lengthPassword = res.data.seT_VALUE;
        });
      }
    });
  }

  Logout() {
    this.cookieSer.deleteAll();
    this.router.navigate(['/login']);
  }
  validate_CP_info_oldpassword() {
    if ($("#field_CP_info_1").val() == '') {
      $("#field_CP_info_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_CP_info_1").html("Vui lòng nhập mật khẩu cũ!");
      return false;
    }
    else {
      $("#field_CP_info_1").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_CP_info_1").html("");
      return true;
    }
  }
  validate_CP_info_newpassword() {
    if ($("#field_CP_info_2").val() == '') {
      $("#field_CP_info_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_CP_info_2").html("Vui lòng nhập mật khẩu mới!");
      return false;
    }
    else if (($("#field_CP_info_2").val()).toString().length < this.lengthPassword) {
      $("#field_CP_info_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_CP_info_2").html("Độ dài mật khẩu phải từ " + this.lengthPassword + " ký tự trở lên!");
      return false;
    }
    else {
      $("#field_CP_info_2").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_CP_info_2").html("");
      return true;
    }
  }
  validate_CP_info_confirmnewpassword() {
    if ($("#field_CP_info_3").val() == '') {
      $("#field_CP_info_3").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_CP_info_3").html("Vui lòng xác nhận mật khẩu mới!");
      return false;
    }
    else {
      if ($("#field_CP_info_2").val() != $("#field_CP_info_3").val()) {
        $("#field_CP_info_3").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_CP_info_3").html("Xác nhận mật khẩu mới không khớp!");
        return false;
      }
      else {
        $("#field_CP_info_3").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_CP_info_3").html("");
        return true;
      }
    }
  }
  validate() {
    var check = true;
    if (this.validate_CP_info_oldpassword() == false)
      check = false;
    if (this.validate_CP_info_newpassword() == false)
      check = false;
    if (this.validate_CP_info_confirmnewpassword() == false)
      check = false;
    //validate all
    if (check == false)
      return false;
    else
      return true;
  }
  clearField() {
    for (let i = 1; i <= 3; i++) {
      $("#field_CP_info_" + i).removeClass('is-invalid is-valid');
      $("#field_CP_info_" + i).val("");
      $("#valid_mes_CP_info_" + i).html("");
    }
  }
  OpenModalChangePassword(event = null) {
    this.unlockModal("upd");
    if (event != null) {
      event.preventDefault();
    }
    this.clearField();
    this.modalChangePassword.show();
  }
  ChangePassword() {
    if (this.validate() == false) {
      return false;
    }
    this.lockModal("upd");
    this.changePasswordRequest.userId = Number(this.cookieSer.get('Id'));
    this.authSer.changePassword(this.changePasswordRequest).subscribe(res => {
      if (res.errorCode == 0) {
        this.pnotify.success({
          title: 'Thông báo',
          text: 'Đổi mật khẩu thành công!'
        });
        this.modalChangePassword.hide();
        $("#message_layout_info").html("Mật khẩu đã thay đổi. Bạn có muốn đăng nhập lại?");
        this.modalConfirmLayout.show();
      }
      else if (res.errorCode == 2) {
        this.unlockModal("upd");
        $("#field_CP_info_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_CP_info_1").html("Mật khẩu cũ không chính xác!");
        return false;
      }
    });

  }

  lockModal(key: string) {
    $(".close_modal_CP_info_" + key).prop("disabled", true);
    $("#save_modal_CP_info_" + key).prop("disabled", true);
    $("#save_modal_CP_info_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_CP_info_" + key).prop("disabled", false);
    $("#save_modal_CP_info_" + key).prop("disabled", false);
    $("#save_modal_CP_info_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

}
