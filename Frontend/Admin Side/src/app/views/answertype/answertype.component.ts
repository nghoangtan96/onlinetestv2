import { Component, OnInit, ViewChild } from '@angular/core';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { PnotifyService } from '../../services/pnotify.service';
import { AnswertypeService, AnswerType } from '../../services/answertype.service';
import { CookieService } from 'ngx-cookie-service';
import { ActivatedRoute } from '@angular/router';
import { SignalrService } from '../../services/signalr.service';
import { RoleService } from '../../services/role.service';
import { UserPermissionService } from '../../services/user-permission.service';

@Component({
  selector: 'app-answertype',
  templateUrl: './answertype.component.html',
  styleUrls: ['./answertype.component.css']
})
export class AnswertypeComponent implements OnInit {

  pnotify = undefined;
  answertypes: [AnswerType];
  answertype: AnswerType = {} as AnswerType;
  antyid: number;

  ans_upd: boolean = true;

  editorConfig = {
    placeholder: 'Mẫu câu hỏi...',
    language: 'vi'
  };
  public Editor = ClassicEditor;

  @ViewChild('modal', { static: false }) modal: ModalDirective;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  constructor(private pNotify: PnotifyService, private answertypeSer: AnswertypeService,
    private cookieSer: CookieService, private route: ActivatedRoute,
    private signalRSer: SignalrService, private roleSer: RoleService,
    private userPermissionSer: UserPermissionService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.answertypeSer.getAll().subscribe(res => {
      this.answertypes = res.data;
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
    //permission
    this.roleSer.get(50).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'ans_upd':
                this.ans_upd = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    //realtime
    this.signalRSer.answertype.subscribe((res) => {
      if (res === true) {
        this.answertypeSer.getAll().subscribe(res1 => {
          this.answertypes = res1.data;
          this.rerender();
        });
      }
    });
  }
  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.antyid = params['id'];
      if (this.antyid != null) {
        this.openModal(this.antyid);
      }
    });
  }
  onReadyCKEditor(editor) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  clearField() {
    $("#field_1>.ck-editor").removeAttr("style");
  }

  checkField() {
    $("#field_1>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
  }

  validate_anty_sample() {
    $("#field_1>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
    $("#valid_mes_1").html("");
  }

  openModal(id: number = 0) {
    this.unlockModal("addupd");
    if (id > 0) { // update
      this.checkField();

      this.answertypeSer.get(id).subscribe(res => {
        this.answertype = res.data;
      });

      $("#modal_title_inside").html("Sửa loại câu hỏi");
      this.modal.show();
    }
  }
  save() {

    this.validate_anty_sample();
    this.lockModal("addupd");

    if (this.answertype.id != 0) { // update
      this.answertype.antY_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.answertypeSer.put(this.answertype).subscribe(res => {
        if (res.errorCode === 0) {
          this.answertypeSer.getAll().subscribe(res1 => {
            this.answertypes = res1.data;
            this.rerender();
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Sửa loại câu hỏi thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
  }

  lockModal(key: string) {
    $(".close_modal_ans_" + key).prop("disabled", true);
    $("#save_modal_ans_" + key).prop("disabled", true);
    $("#save_modal_ans_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_ans_" + key).prop("disabled", false);
    $("#save_modal_ans_" + key).prop("disabled", false);
    $("#save_modal_ans_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

}
