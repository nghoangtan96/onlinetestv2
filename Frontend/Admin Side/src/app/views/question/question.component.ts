import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Question, QuestionService } from '../../services/question.service';
import { PnotifyService } from '../../services/pnotify.service';
import { PartService, Part } from '../../services/part.service';
import { CookieService } from 'ngx-cookie-service';
import { PassageService, Passage } from '../../services/passage.service';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import '@ckeditor/ckeditor5-build-classic/build/translations/vi';
import { Setting, SettingService } from '../../services/setting.service';
import { FileService } from '../../services/file.service';
import { AnswerType, AnswertypeService } from '../../services/answertype.service';
import { Subjectt, SubjectService } from '../../services/subject.service'; import { questionRequest } from '../../models/question_request';
import { Option } from '../../services/option.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { SignalrService } from '../../services/signalr.service';
import { RoleService } from '../../services/role.service';
import { UserPermissionService } from '../../services/user-permission.service';
import { UserService } from '../../services/user.service';
import { EmployeeSubjectService } from '../../services/employee-subject.service';
;

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  @Input() public partId: number; //parse parameter through selector
  @Input() public passageId: number;
  queid: number;

  pnotify = undefined;
  questions: [Question];
  questionsMultiDel: [Question];
  question: Question = {} as Question;
  part: Part = {} as Part;
  passage: Passage = {} as Passage;
  file: File = null;
  fileIsExist: boolean;
  questionRequest: questionRequest = {} as questionRequest;
  listOption: Option[] = [];
  subjectsConstraint: [number];

  passages: [Passage];
  answertypes: [AnswerType];
  parts: [Part];
  subjects: [Subjectt];
  sub_id: number;
  column: number = 12;
  label_opt: number = 2;
  ckeditor_opt: number = 10;
  yesNoQuestionType: number = 1;
  isListItemNull: boolean = true;
  mediaType: string;
  tabControl: number = 1;

  //import
  fileImport: File = null;
  fileImportIsExist: boolean;
  sampleFileLink: string = "";
  errorImport: boolean = false;
  errorImportLog: string = "";
  subIdImport: number;
  parIdImport: number;
  passageIdImport: number;
  partsImport: [Part];
  passagesImport: [Passage];
  parIdImportError: number;

  que_add: boolean = true;
  que_upd: boolean = true;
  que_del: boolean = true;
  que_con: boolean = true;
  que_ref: boolean = true;

  editorConfig = {
    placeholder: 'Nội dung câu hỏi...',
    language: 'vi'
  };

  editorConfig1 = {
    placeholder: 'Lời giải...',
    language: 'vi'
  };

  // editorViewConfig = {
  //   toolbar: [],
  //   language: 'vi'
  // };

  editorConfigOptionA = {
    placeholder: 'Đáp án A...',
    language: 'vi'
  };
  editorConfigOptionB = {
    placeholder: 'Đáp án B...',
    language: 'vi'
  };
  editorConfigOptionC = {
    placeholder: 'Đáp án C...',
    language: 'vi'
  };
  editorConfigOptionD = {
    placeholder: 'Đáp án D...',
    language: 'vi'
  };
  editorConfigOptionE = {
    placeholder: 'Đáp án E...',
    language: 'vi'
  };
  editorConfigOptionF = {
    placeholder: 'Đáp án F...',
    language: 'vi'
  };

  optionA: string = "";
  optionB: string = "";
  optionC: string = "";
  optionD: string = "";
  optionE: string = "";
  optionF: string = "";

  optionKey = ['A', 'B', 'C', 'D', 'E', 'F'];
  videoExtension = ['mp4', 'webm', 'ogg'];
  audioExtension = ['mpeg', 'wav', 'm4a', 'mp3'];

  public Editor = ClassicEditor;
  public Editor1 = ClassicEditor;
  // public EditorView = ClassicEditor;
  public EditorA = ClassicEditor;
  public EditorB = ClassicEditor;
  public EditorC = ClassicEditor;
  public EditorD = ClassicEditor;
  public EditorE = ClassicEditor;
  public EditorF = ClassicEditor;

  difficultDisplay: Setting = {} as Setting;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild('modalDeletMul', { static: false }) modalDeletMul: ModalDirective;
  @ViewChild('modalNofication', { static: false }) modalNofication: ModalDirective;
  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;
  @ViewChild('modalImport', { static: false }) modalImport: ModalDirective;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  constructor(private pNotify: PnotifyService, private partSer: PartService,
    private cookieSer: CookieService, private passageSer: PassageService,
    private questionSer: QuestionService, private fileSer: FileService,
    private settingSer: SettingService, private answerTypeSer: AnswertypeService,
    private subjectSer: SubjectService, private _DomSanitizer: DomSanitizer,
    private route: ActivatedRoute, private signalRSer: SignalrService,
    private roleSer: RoleService, private userPermissionSer: UserPermissionService,
    private userSer: UserService, private employeeSubjectSer: EmployeeSubjectService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.dtOptions = {
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      order: [[1, 'asc']]
    };
    if (this.partId == null && this.passageId == null) {
      this.questionSer.getAll().subscribe(res => {
        this.questions = res.data;
        this.dtTrigger.next();
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.columns().every(function () {
            const that = this;
            $('input', this.footer()).on('keyup change', function () {
              if (that.search() !== this['value']) {
                that
                  .search(this['value'])
                  .draw();
              }
            });
          });
        });
      });
    }
    else {
      if (this.partId != null) {
        this.questionSer.getListByPartId(this.partId).subscribe(res => {
          this.questions = res.data;
          this.dtTrigger.next();
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.columns().every(function () {
              const that = this;
              $('input', this.footer()).on('keyup change', function () {
                if (that.search() !== this['value']) {
                  that
                    .search(this['value'])
                    .draw();
                }
              });
            });
          });
        });
        this.partSer.get(this.partId).subscribe(res => {
          this.part = res.data;
          this.partSer.getListBySubjectId(res.data.subjects.id).subscribe(res1 => {
            this.parts = res1.data;
          });
        });
      }
      else {
        this.questionSer.getListByPassageId(this.passageId).subscribe(res => {
          this.questions = res.data;
          this.dtTrigger.next();
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.columns().every(function () {
              const that = this;
              $('input', this.footer()).on('keyup change', function () {
                if (that.search() !== this['value']) {
                  that
                    .search(this['value'])
                    .draw();
                }
              });
            });
          });
        });
        this.passageSer.get(this.passageId).subscribe(res => {
          this.passage = res.data;
          // console.log(res.data);
          this.partSer.getListBySubjectId(res.data.parts.subjects.id).subscribe(res1 => {
            this.parts = res1.data;
          });
        });
      }
    }
    this.subjectSer.getAll().subscribe(res => {
      this.subjects = res.data;
    });
    this.settingSer.get(9).subscribe(res => {
      this.difficultDisplay = res.data;
    });
    this.answerTypeSer.getAll().subscribe(res => {
      this.answertypes = res.data;
    });
    //permission
    this.roleSer.get(26).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);s
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'que_add':
                this.que_add = false;
                break;
              case 'que_upd':
                this.que_upd = false;
                break;
              case 'que_del':
                this.que_del = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    this.roleSer.get(105).subscribe(res => {
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        let count = 0;
        let countNoConstraint = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == false) != null) {
              countNoConstraint++;
            }
          }
        }
        if (count > 0 && countNoConstraint == 0) {
          this.que_con = false;
          this.userSer.get(Number(this.cookieSer.get('Id'))).subscribe(res2 => {
            this.employeeSubjectSer.getListSubjectIdByEmployeeId(res2.data.employees[0].id).subscribe(res3 => {
              this.subjectsConstraint = res3.data;
            });
          });
        }
        else if (countNoConstraint > 0) {
          this.que_con = true;
        }
      })
    });
    //realtime
    this.signalRSer.question.subscribe((res) => {
      if (res === true) {
        if (this.partId == null && this.passageId == null) {
          this.questionSer.getAll().subscribe(res1 => {
            this.questions = res1.data;
            this.rerender();
          });
        }
        else {
          if (this.partId != null) {
            this.questionSer.getListByPartId(this.partId).subscribe(res1 => {
              this.questions = res1.data;
              this.rerender();
            });
          }
          else {
            this.questionSer.getListByPassageId(this.passageId).subscribe(res1 => {
              this.questions = res1.data;
              this.rerender();
            });
          }
        }
      }
    });
    this.signalRSer.answertype.subscribe((res) => {
      if (res === true) {
        this.answerTypeSer.getAll().subscribe(res1 => {
          this.answertypes = res1.data;
        });
      }
    });
    this.signalRSer.subject.subscribe((res) => {
      if (res === true) {
        this.subjectSer.getAll().subscribe(res1 => {
          this.subjects = res1.data;
        });
      }
    });
    this.signalRSer.setting.subscribe((res) => {
      if (res === true) {
        this.settingSer.get(9).subscribe(res1 => {
          this.difficultDisplay = res1.data;
        });
      }
    });
  }
  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.queid = params['id'];
      if (this.queid != null) {
        this.openModalDetail(this.queid);
      }
    });
  }
  onReadyCKEditor(editor) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
  }

  onSelectFile(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      this.file = event.target.files[0];

      // reader.onload = (event: any) => { // called once readAsDataURL is completed
      //   this.url = event.target.result;
      // }

      $("#label_field_que_2").html(
        this.file.name.length > 30 ?
          this.file.name.substring(0, 9) + "..." + this.file.name.substring(this.file.name.length - 9, this.file.name.length) :
          this.file.name
      );
      this.fileIsExist = true;
    }
  }

  onSelectFileImport(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      this.fileImport = event.target.files[0];

      // reader.onload = (event: any) => { // called once readAsDataURL is completed
      //   this.url = event.target.result;
      // }

      $("#label_field_que_import").html(
        this.fileImport.name.length > 30 ?
          this.fileImport.name.substring(0, 9) + "..." + this.fileImport.name.substring(this.fileImport.name.length - 9, this.fileImport.name.length) :
          this.fileImport.name
      );
      this.fileImportIsExist = true;
    }
  }

  removeFile(event = null) {
    if (event != null) {
      event.preventDefault();
    }
    this.file = null;
    $("#field_que_2").val("");
    this.fileIsExist = null;
    $("#label_field_que_2").html("Tải tệp lên...");
    this.validate_que_media();
  }

  removeFileImport(event = null) {
    if (event != null) {
      event.preventDefault();
    }
    this.fileImport = null;
    $("#field_que_import").val("");
    this.fileImportIsExist = null;
    $("#label_field_que_import").html("Tải tệp lên...");
    this.validate_que_importfile();
  }

  stopMedia(mode: number) {
    if (mode == 1) {
      $("#que_media").trigger("pause");
      $("#que_media").prop("currentTime", 0);
    }
    else {
      $("#que_mediaview").trigger("pause");
      $("#que_mediaview").prop("currentTime", 0);
    }
  }

  Select_Clear_All() {
    $(".toggle-que-all").closest('tr').toggleClass('selected');
    if ($(".toggle-que-all").closest('tr').hasClass('selected')) {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.select();
        });
      });
    }
    else {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.deselect();
        });
      });
    }
  }

  generatePartList() {
    this.partSer.getListBySubjectId(this.sub_id).subscribe(res => {
      this.parts = res.data;
      if (res.data[0] == null) {
        this.question.quE_PARID = 0;
        this.generateAnswerType();
      }
      else {
        this.question.quE_PARID = res.data[0].id;
        // if (this.question.id == 0) {
        this.passageSer.getListByPartId(this.question.quE_PARID).subscribe(res1 => {
          this.passages = res1.data;
          this.question.quE_PASID = 0;
          this.generateAnswerType();

          if (this.question.id == 0) {
            if (this.parts.find(x => x.id == this.question.quE_PARID).paR_ISALLOWSHUFFLEOPTION == false) {
              $("#field_que_5").prop("disabled", true);
            }
            else {
              $("#field_que_5").prop("disabled", false);
            }
            $("#field_que_5").prop("checked", false);
          }
          else {
            if (this.parts.find(x => x.id == this.question.quE_PARID).paR_ISALLOWSHUFFLEOPTION == false) {
              $("#field_que_5").prop("disabled", true);
            }
            else {
              $("#field_que_5").prop("disabled", false);
            }
            if (this.question.quE_ISSHUFFLE == false) {
              $("#field_que_5").prop("checked", false);
            }
            else {
              $("#field_que_5").prop("checked", true);
            }
          }
        });
        // }
        // else {
        //   this.passageSer.getListByPartIdAndAnswerTypeId(this.question.quE_PARID, this.question.quE_ANTYID).subscribe(res1 => {
        //     this.passages = res1.data;
        //     this.question.quE_PASID = 0;
        //     console.log(res1.data);
        //   });
        // }
      }
    });
  }
  generatePassageList() {
    if (this.question.id == 0) {
      if (this.question.quE_PARID != null && this.question.quE_PARID != 0) {
        if (this.parts.find(x => x.id == this.question.quE_PARID).paR_ISALLOWSHUFFLEOPTION == false) {
          $("#field_que_5").prop("disabled", true);
        }
        else {
          $("#field_que_5").prop("disabled", false);
        }
        $("#field_que_5").prop("checked", false);
        this.passageSer.getListByPartId(this.question.quE_PARID).subscribe(res => {
          this.passages = res.data;
          this.question.quE_PASID = 0;
          this.generateAnswerType();
        });
      }
    }
    else {
      if (this.question.quE_PARID != null && this.question.quE_PARID != 0) {
        if (this.parts.find(x => x.id == this.question.quE_PARID).paR_ISALLOWSHUFFLEOPTION == false) {
          $("#field_que_5").prop("disabled", true);
        }
        else {
          $("#field_que_5").prop("disabled", false);
        }
        if (this.question.quE_ISSHUFFLE == false) {
          $("#field_que_5").prop("checked", false);
        }
        else {
          $("#field_que_5").prop("checked", true);
        }
        this.passageSer.getListByPartIdAndAnswerTypeId(this.question.quE_PARID, this.question.quE_ANTYID).subscribe(res => {
          this.passages = res.data;
          this.question.quE_PASID = 0;
          // this.generateAnswerType();
        });
      }
    }
    // else
    //   this.generateAnswerType();
  }
  generateColumn() {
    if (this.question.quE_OPTCOLUMN != null) {
      if (this.question.quE_OPTCOLUMN <= 1) {
        this.column = 12;
        this.label_opt = 2;
        this.ckeditor_opt = 10;
      }
      if (this.question.quE_OPTCOLUMN == 2) {
        this.column = 6;
        this.label_opt = 3;
        this.ckeditor_opt = 9;
      }
      if (this.question.quE_OPTCOLUMN == 3) {
        this.column = 4;
        this.label_opt = 12;
        this.ckeditor_opt = 12;
      }
      if (this.question.quE_OPTCOLUMN >= 4) {
        this.column = 3;
        this.label_opt = 12;
        this.ckeditor_opt = 12;
      }
    }
  }

  generatePartListImport() {
    this.partSer.getListBySubjectId(this.subIdImport).subscribe(res => {
      this.partsImport = res.data;
      if (res.data[0] == null) {
        this.parIdImport = 0;
      }
      else {
        this.parIdImport = res.data[0].id;
        // if (this.question.id == 0) {
        this.passageSer.getListByPartId(this.parIdImport).subscribe(res1 => {
          this.passagesImport = res1.data;
          this.passageIdImport = 0;
        });
      }
    });
  }

  generatePassageListImport() {
    this.passageSer.getListByPartId(this.parIdImport).subscribe(res => {
      this.passagesImport = res.data;
      this.passageIdImport = 0;
    });
  }

  generateYesNoQuestion() {
    if (this.yesNoQuestionType == 1) {
      $("#field_opt_content_1").val("Đúng");
      $("#field_opt_content_2").val("Sai");
    }
    else if (this.yesNoQuestionType == 2) {
      $("#field_opt_content_1").val("Đồng ý");
      $("#field_opt_content_2").val("Không đồng ý");
    }
  }

  generateAnswerType() {
    if (this.question.id == 0) {
      if (this.question.quE_PASID != null && this.question.quE_PASID != 0) {
        this.passageSer.get(this.question.quE_PASID).subscribe(res => {
          this.question.quE_ANTYID = res.data.paS_ANTYID;
          $("#field_que_7").prop("disabled", true);
          $("#warning_mes_que_10").html("<i class='fa fa-warning'></i> Đoạn văn chỉ được tạo câu hỏi loại " + res.data.answerTypes.antY_NAME.toLowerCase() + "!");
        });
      }
      else {
        $("#warning_mes_que_10").html("");
        $("#field_que_7").prop("disabled", false);
      }
    }
  }

  addOption() {
    if (this.question.quE_ANTYID != null && this.question.quE_ANTYID != 0) {
      for (let i = 4; i <= 6; i++) {
        if ($("#option" + i).is(":visible")) {
          continue;
        }
        else {
          $("#option" + i).show();
          break;
        }
      }
    }
  }

  removeOption() {
    if (this.question.quE_ANTYID != null && this.question.quE_ANTYID != 0) {
      for (let i = 6; i >= 4; i--) {
        if ($("#option" + i).is(":visible")) {
          $("#option" + i).hide();
          $("#field_opt_" + i + ">.ck-editor").removeAttr("style");
          break;
        }
        else {
          continue;
        }
      }
    }
  }

  removePassagePick() {
    this.question.quE_PASID = 0;
    this.generateAnswerType();
  }
  removePassagePickImport() {
    this.passageIdImport = 0;
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
  }

  clearField() {
    for (let i = 1; i <= 12; i++) {
      // $("#field_ins_" + i).val("");
      if (i == 1) {
        $("#field_que_1>.ck-editor").removeAttr("style");
        $("#valid_mes_que_" + i).html("");
      }
      else if (i == 12) {
        $("#field_que_12>.ck-editor").removeAttr("style");
        $("#valid_mes_que_" + i).html("");
      }
      else {
        $("#field_que_" + i).removeClass('is-invalid is-valid');
        $("#valid_mes_que_" + i).html("");
      }
    }
    if (this.partId == null && this.passageId == null) {
      this.sub_id = null;
    }
    this.file = null;
    this.fileIsExist = null;
    this.listOption = [];
    $("#label_field_que_2").html("Tải tệp lên...");
    $("#warning_mes_que_8").html("");
    for (let i = 1; i <= 6; i++) {
      $("#field_opt_content_" + i + ">.ck-editor").removeAttr("style");
      $("#field_opt_" + i).prop("checked", false);
      $("#valid_mes_opt_" + i).html("");
    }
    $("#valid_opt_duplicate").hide();
    $("#valid_opt_all").hide();
    this.optionA = "";
    this.optionB = "";
    this.optionC = "";
    this.optionD = "";
    this.optionE = "";
    this.optionF = "";
    this.generateColumn();
    if (this.passageId == null) {
      this.passages = null;
    }
    this.yesNoQuestionType = 1;

    if (this.question.quE_ISSHUFFLE == false) {
      $("#field_que_5").prop("checked", false);
    }
    else {
      $("#field_que_5").prop("checked", true);
    }
  }

  checkField() {
    for (let i = 1; i <= 12; i++) {
      if (i == 1) {
        $("#field_que_1>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
        $("#valid_mes_que_" + i).html("");
      }
      else if (i == 12) {
        $("#field_que_12>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
        $("#valid_mes_que_" + i).html("");
      }
      else if (i == 5)
        continue;
      else {
        $("#field_que_" + i).removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_que_" + i).html("");
      }
    }
    this.listOption = [];
    this.file = null;
    this.generateColumn();
    // for (let i = 1; i <= 6; i++) {
    //   if (i <= 3) {
    //     $("#field_opt_content_" + i + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
    //     $("#valid_mes_opt_" + i).html("");
    //   }
    //   else {
    //     if ($("#option" + i).is(":visible")) {
    //       $("#field_opt_content_" + i + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
    //       $("#valid_mes_opt_" + i).html("");
    //     }
    //     else {
    //       $("#field_opt_content_" + i + ">.ck-editor").removeAttr("style");
    //       $("#valid_mes_opt_" + i).html("");
    //     }
    //   }
    // }
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_que_" + id).is(":visible")) {
      $("#buttonicon_que_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_que_" + id).slideToggle();
    }
    else {
      $("#buttonicon_que_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_que_" + id).slideToggle();
    }
  }

  ToggleGroupMulti(id: number) {
    if ($("#detailToggle_que_mul_" + id).is(":visible")) {
      $("#buttonicon_que_mul_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_que_mul_" + id).slideToggle();
    }
    else {
      $("#buttonicon_que_mul_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_que_mul_" + id).slideToggle();
    }
  }

  validate_que_content() {
    if (this.question.id == 0) {
      if (this.question.quE_PARID != null && this.question.quE_PARID != 0) {
        if (this.parts != null && this.parts.find(x => x.id == this.question.quE_PARID).paR_ISALLOWNULLQUESTION == false) {
          if (this.question.quE_CONTENT == '') {
            $("#field_que_1>.ck-editor").removeAttr("style").attr("style", "border:1px solid #dc3545;");
            $("#valid_mes_que_1").html("Vui lòng nhập nội dung câu hỏi!");
            return false;
          }
          else {
            $("#field_que_1>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
            $("#valid_mes_que_1").html("");
            return true;
          }
        }
      }
      $("#field_que_1>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
      $("#valid_mes_que_1").html("");
      return true;
    }
    else {
      if (this.parts.find(x => x.id == this.question.quE_PARID).paR_ISALLOWNULLQUESTION == true) {
        $("#field_que_1>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
        $("#valid_mes_que_1").html("");
        return true;
      }
      else {
        if (this.question.quE_CONTENT == '') {
          $("#field_que_1>.ck-editor").removeAttr("style").attr("style", "border:1px solid #dc3545;");
          $("#valid_mes_que_1").html("Vui lòng nhập nội dung câu hỏi!");
          return false;
        }
        else {
          $("#field_que_1>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
          $("#valid_mes_que_1").html("");
          return true;
        }
      }
    }
  }

  validate_que_media() {
    $("#field_que_2").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_que_2").html("");
  }

  validate_que_score() {
    if ($("#field_que_3").val() == '') {
      $("#field_que_3").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_que_3").html("Vui lòng nhập số điểm!");
      return false;
    }
    else if ($("#field_que_3").val() <= 0) {
      $("#field_que_3").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_que_3").html("Số điểm phải lớn hơn 0!");
      return false;
    }
    else {
      $("#field_que_3").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_que_3").html("");
      return true;
    }
  }

  validate_que_level() {
    if (Number($("#field_que_4").val()) == 0) {
      $("#field_que_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_que_4").html("Vui lòng chọn độ khó!");
      return false;
    }
    else {
      $("#field_que_4").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_que_4").html("");
      return true;
    }
  }

  validate_que_column() {
    if ($("#field_que_11").val() == '') {
      $("#field_que_11").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_que_11").html("Vui lòng nhập số cột!");
      return false;
    }
    else if ($("#field_que_11").val() <= 0) {
      $("#field_que_11").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_que_11").html("Số cột phải lớn hơn 0!");
      return false;
    }
    else {
      $("#field_que_11").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_que_11").html("");
      return true;
    }
  }

  validate_que_status() {
    $("#field_que_6").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_que_6").html("");
  }

  validate_que_antyid() {
    if (Number($("#field_que_7").val()) == 0) {
      $("#field_que_7").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_que_7").html("Vui lòng chọn loại câu hỏi!");
      return false;
    }
    else {
      $("#field_que_7").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_que_7").html("");
      return true;
    }
  }

  validate_que_parid() {
    if (Number($("#field_que_9").val()) == 0 || this.question.quE_PARID == 0) {
      $("#field_que_9").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_que_9").html("Vui lòng chọn học phần!");
      $("#warning_mes_que_8").html("<i class='fa fa-warning'></i> Chọn môn học để mở các học phần!")
      return false;
    }
    else {
      if (this.parts.find(x => x.id == this.question.quE_PARID).countPassage != 0 && this.question.quE_PASID == 0) {
        $("#field_que_9").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_que_9").html("Học phần sủ dụng câu hỏi đoạn văn, vui lòng xóa hết câu hỏi đoạn văn nếu muốn tạo câu hỏi đơn!");
        return false;
      }
      else {
        $("#field_que_9").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_que_9").html("");
        $("#warning_mes_que_8").html("");
        return true;
      }
    }
  }

  validate_que_subid() {
    $("#field_que_8").removeClass('is-invalid').addClass('is-valid');
  }

  validate_que_pasid() {
    $("#field_que_10").removeClass('is-invalid').addClass('is-valid');
  }

  validate_que_reference() {
    $("#field_que_12>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
  }

  validate_opt(index: number) {
    this.validate_option_all();
    // console.log(this.sub_id + " " + this.question.quE_PARID);
    if (this.sub_id != null && this.question.quE_PARID != 0) {
      if (this.parts.find(x => x.id == this.question.quE_PARID).paR_ISALLOWNULLOPTION == false) {
        this.validate_option_duplicate();
        if (index < 4) {
          switch (index) {
            case 1:
              if (this.optionA == "") {
                $("#field_opt_content_1>.ck-editor").removeAttr("style").attr("style", "border:1px solid #dc3545;");
                $("#valid_mes_opt_1").html("Vui lòng nhập đán án A!");
                return false;
              } else {
                $("#field_opt_content_1>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                $("#valid_mes_opt_1").html("");
                return true;
              }
            case 2:
              if (this.optionB == "") {
                $("#field_opt_content_2>.ck-editor").removeAttr("style").attr("style", "border:1px solid #dc3545;");
                $("#valid_mes_opt_2").html("Vui lòng nhập đán án B!");
                return false;
              } else {
                $("#field_opt_content_2>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                $("#valid_mes_opt_2").html("");
                return true;
              }
            case 3:
              if (this.optionC == "") {
                $("#field_opt_content_3>.ck-editor").removeAttr("style").attr("style", "border:1px solid #dc3545;");
                $("#valid_mes_opt_3").html("Vui lòng nhập đán án C!");
                return false;
              } else {
                $("#field_opt_content_3>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                $("#valid_mes_opt_3").html("");
                return true;
              }
            default:
              break;
          }
        }
        else {
          if ($("#option" + index).is(":visible")) {
            switch (index) {
              case 4:
                if (this.optionD == "") {
                  $("#field_opt_content_4>.ck-editor").removeAttr("style").attr("style", "border:1px solid #dc3545;");
                  $("#valid_mes_opt_4").html("Vui lòng nhập đán án D!");
                  return false;
                } else {
                  $("#field_opt_content_4>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                  $("#valid_mes_opt_4").html("");
                  return true;
                }
              case 5:
                if (this.optionE == "") {
                  $("#field_opt_content_5>.ck-editor").removeAttr("style").attr("style", "border:1px solid #dc3545;");
                  $("#valid_mes_opt_5").html("Vui lòng nhập đán án E!");
                  return false;
                } else {
                  $("#field_opt_content_5>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                  $("#valid_mes_opt_5").html("");
                  return true;
                }
              case 6:
                if (this.optionF == "") {
                  $("#field_opt_content_6>.ck-editor").removeAttr("style").attr("style", "border:1px solid #dc3545;");
                  $("#valid_mes_opt_6").html("Vui lòng nhập đán án F!");
                  return false;
                } else {
                  $("#field_opt_content_6>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                  $("#valid_mes_opt_6").html("");
                  return true;
                }
              default:
                break;
            }
          }
          else {
            return true;
          }
        }
      }
      else {
        $("#field_opt_content_" + index + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
        $("#valid_mes_opt_" + index).html("");
        return true;
      }
    }
    else {
      $("#field_opt_content_" + index + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
      $("#valid_mes_opt_" + index).html("");
      return true;
    }
  }

  validate_option_all() {
    //check iscorrect option
    let isHaveAnswer = true;
    let count = 0;
    if (this.question.quE_ANTYID != null && this.question.quE_ANTYID != 0) {
      if (this.question.quE_ANTYID == 1 || this.question.quE_ANTYID == 3) {
        for (let i = 1; i <= 6; i++) {
          if (i <= 3) {
            if ($("#field_opt_" + i).is(":checked")) {
              count++;
              break;
            }
          }
          else {
            if ($("#option" + i).is(":visible")) {
              if ($("#field_opt_" + i).is(":checked")) {
                count++;
                break;
              }
            }
          }
        }
        if (count != 0) {
          $("#valid_opt_all").hide();
          return true;
        }
        else {
          $("#valid_opt_all").show();
          return false;
        }
      }
      else if (this.question.quE_ANTYID == 2) {
        for (let i = 1; i <= 6; i++) {
          if (i <= 3) {
            if ($("#field_opt_" + i).is(":checked")) {
              count++;
            }
          }
          else {
            if ($("#option" + i).is(":visible")) {
              if ($("#field_opt_" + i).is(":checked")) {
                count++;
              }
            }
          }
        }
        if (count > 1) {
          $("#valid_opt_all").hide();
          return true;
        }
        else {
          $("#valid_opt_all").show();
          return false;
        }
      }
    }
  }

  validate_option_duplicate() {
    //check duplicate
    let arr = [this.optionA, this.optionB, this.optionC, this.optionD, this.optionE, this.optionF];
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] != "") {
        for (let j = 0; j < arr.length; j++) {
          if (arr[i].replace(/\s/g, '') == arr[j].replace(/\s/g, '') && i != j) {
            $("#valid_opt_duplicate").show();
            $("#option_duplicate").html(this.optionKey[i] + ", " + this.optionKey[j]);
            return false;
          }
        }
      }
    }
    $("#valid_opt_duplicate").hide();
    return true;
  }

  validate_que_importfile() {
    if (this.fileImport == null) {
      $("#field_que_import").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_que_import").html("Vui lòng tải tệp lên!");
      return false;
    }
    else {
      $("#field_que_import").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_que_import").html("");
      return true;
    }
  }

  validate_import_parid() {
    if (this.parIdImport == 0 || this.parIdImport == null) {
      $("#field_import_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_import_2").html("Vui lòng chọn học phần!");
      $("#warning_mes_import_2").html("<i class='fa fa-warning'></i> Chọn môn học để mở các học phần!");
      return false;
    }
    else if (this.partsImport.find(x => x.id == this.parIdImport).countPassage > 0 && this.passageIdImport == 0) {
      $("#field_import_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_import_2").html("Học phần chỉ sử dụng câu hỏi đoạn văn!");
      return false;
    }
    else {
      $("#field_import_2").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_import_2").html("");
      $("#warning_mes_import_2").html("");
      return true;
    }
  }

  validate_import_subid() {
    $("#field_import_1").removeClass('is-invalid').addClass('is-valid');
    return true;
  }

  validate_import_pasid() {
    $("#field_import_3").removeClass('is-invalid').addClass('is-valid');
    return true;
  }

  validate() {
    var check = true;
    if (this.validate_que_content() == false)
      check = false;
    if (this.validate_que_score() == false)
      check = false;
    if (this.validate_que_level() == false)
      check = false;
    if (this.validate_que_column() == false)
      check = false;
    if (this.validate_que_antyid() == false)
      check = false;
    if (this.partId == null && this.passageId == null) {
      if (this.validate_que_parid() == false)
        check = false;
    }

    this.validate_que_media();
    this.validate_que_status();
    this.validate_que_subid();
    this.validate_que_pasid();
    this.validate_que_reference();

    if (this.question.quE_ANTYID != 3) {
      //validate option
      for (let i = 1; i <= 6; i++) {
        if (this.validate_opt(i))
          continue;
        else {
          check = false;
        }
      }
      //check duplicate option
      if (this.validate_option_duplicate() == false)
        check = false;
    }
    if (this.validate_option_all() == false)
      check = false;

    //validate all
    if (check == false) {
      $("#valid_que_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  openModal(id: number = 0) {
    this.unlockModal("addupd");
    if (id > 0) { // update
      this.questionSer.get(id).subscribe(res => {
        this.question = res.data;
        // console.log(res.data);
        if (res.data.quE_MEDIA != null) {
          $("#label_field_que_2").html(
            res.data.quE_MEDIA.split('/')[3].length > 30 ?
              res.data.quE_MEDIA.split('/')[3].substring(0, 9) + "..." + res.data.quE_MEDIA.split('/')[3].substring(res.data.quE_MEDIA.split('/')[3].length - 9, res.data.quE_MEDIA.split('/')[3].length) :
              res.data.quE_MEDIA.split('/')[3]
          );
          this.fileIsExist = true;
        }
        else {
          $("#label_field_que_2").html("Tải tệp lên...");
          this.fileIsExist = null;
        }

        if (this.question.passages != null) {
          if (this.question.passages.parts.paR_ISALLOWSHUFFLEOPTION == false) {
            $("#field_que_5").prop("disabled", true);
          }
          else {
            $("#field_que_5").prop("disabled", false);
          }
        }
        else {
          if (this.question.parts.paR_ISALLOWSHUFFLEOPTION == false) {
            $("#field_que_5").prop("disabled", true);
          }
          else {
            $("#field_que_5").prop("disabled", false);
          }
        }

        if (this.question.quE_ISSHUFFLE == false) {
          $("#field_que_5").prop("checked", false);
        }
        else {
          $("#field_que_5").prop("checked", true);
        }

        if (res.data.parts != null) {
          this.sub_id = res.data.parts.subjects.id;
          this.partSer.getListBySubjectId(this.sub_id).subscribe(res1 => {
            this.parts = res1.data;
            this.question.quE_PASID = 0;
            this.passageSer.getListByPartIdAndAnswerTypeId(this.question.quE_PARID, this.question.quE_ANTYID).subscribe(res2 => {
              this.passages = res2.data;

              if (this.question.quE_ANTYID == 3) {
                if (res.data.options[0].opT_CONTENT == "Đúng") {
                  this.yesNoQuestionType = 1;
                }
                else {
                  this.yesNoQuestionType = 2;
                }
                let count = 1;
                res.data.options.forEach(ele => {
                  $("#field_opt_content_" + count).val(ele.opT_CONTENT);
                  // console.log(ele.opT_CONTENT);
                  if (ele.opT_ISCORRECT == true) {
                    $("#field_opt_" + count).prop("checked", true);
                  }
                  count++;
                });
              }
              else {
                let count = 1;
                for (let i = 4; i <= 6; i++) {
                  $("#field_opt_content_" + i + ">.ck-editor").removeAttr("style");
                  $("#valid_mes_opt_" + i).html("");
                  $("#option" + i).hide();
                  switch (i) {
                    case 4:
                      this.optionD = "";
                      break;
                    case 5:
                      this.optionE = "";
                      break;
                    case 6:
                      this.optionF = "";
                      break;
                    default:
                      break;
                  }
                }
                res.data.options.forEach(ele => {
                  if (count > 3) {
                    $("#option" + count).show();
                  }
                  if (ele.opT_ISCORRECT == true) {
                    $("#field_opt_" + count).prop("checked", true);
                  }
                  switch (count) {
                    case 1:
                      this.optionA = ele.opT_CONTENT;
                      $("#field_opt_content_" + count + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                      $("#valid_mes_opt_" + count).html("");
                      break;
                    case 2:
                      this.optionB = ele.opT_CONTENT;
                      $("#field_opt_content_" + count + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                      $("#valid_mes_opt_" + count).html("");
                      break;
                    case 3:
                      this.optionC = ele.opT_CONTENT;
                      $("#field_opt_content_" + count + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                      $("#valid_mes_opt_" + count).html("");
                      break;
                    case 4:
                      this.optionD = ele.opT_CONTENT;
                      $("#field_opt_content_" + count + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                      $("#valid_mes_opt_" + count).html("");
                      break;
                    case 5:
                      this.optionE = ele.opT_CONTENT;
                      $("#field_opt_content_" + count + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                      $("#valid_mes_opt_" + count).html("");
                      break;
                    case 6:
                      this.optionF = ele.opT_CONTENT;
                      $("#field_opt_content_" + count + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                      $("#valid_mes_opt_" + count).html("");
                      break;
                    default:
                      break;
                  }
                  count++;
                });
              }

              this.checkField();
              $("#modal_title_que").html("Sửa câu hỏi");
              this.modal.show();
            });
          });
        }
        else {
          this.sub_id = res.data.passages.parts.subjects.id;
          this.partSer.getListBySubjectId(this.sub_id).subscribe(res1 => {
            this.parts = res1.data;
            this.question.quE_PARID = res1.data.find(x => x.id == res.data.passages.parts.id).id;
            this.passageSer.getListByPartIdAndAnswerTypeId(this.question.quE_PARID, this.question.quE_ANTYID).subscribe(res2 => {
              this.passages = res2.data;

              if (this.question.quE_ANTYID == 3) {
                if (res.data.options[0].opT_CONTENT == "Đúng") {
                  this.yesNoQuestionType = 1;
                }
                else {
                  this.yesNoQuestionType = 2;
                }
                let count = 1;
                res.data.options.forEach(ele => {
                  $("#field_opt_content_" + count).val(ele.opT_CONTENT);
                  // console.log(ele.opT_CONTENT);
                  if (ele.opT_ISCORRECT == true) {
                    $("#field_opt_" + count).prop("checked", true);
                  }
                  count++;
                });
              }
              else {
                let count = 1;
                for (let i = 4; i <= 6; i++) {
                  $("#field_opt_content_" + i + ">.ck-editor").removeAttr("style");
                  $("#valid_mes_opt_" + i).html("");
                  $("#option" + i).hide();
                  switch (i) {
                    case 4:
                      this.optionD = "";
                      break;
                    case 5:
                      this.optionE = "";
                      break;
                    case 6:
                      this.optionF = "";
                      break;
                    default:
                      break;
                  }
                }
                res.data.options.forEach(ele => {
                  if (count > 3) {
                    $("#option" + count).show();
                    // console.log("option" + count);
                  }
                  if (ele.opT_ISCORRECT == true) {
                    $("#field_opt_" + count).prop("checked", true);
                    // console.log("field_opt_" + count);
                  }
                  switch (count) {
                    case 1:
                      this.optionA = ele.opT_CONTENT;
                      $("#field_opt_content_" + count + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                      $("#valid_mes_opt_" + count).html("");
                      break;
                    case 2:
                      this.optionB = ele.opT_CONTENT;
                      $("#field_opt_content_" + count + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                      $("#valid_mes_opt_" + count).html("");
                      break;
                    case 3:
                      this.optionC = ele.opT_CONTENT;
                      $("#field_opt_content_" + count + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                      $("#valid_mes_opt_" + count).html("");
                      break;
                    case 4:
                      this.optionD = ele.opT_CONTENT;
                      $("#field_opt_content_" + count + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                      $("#valid_mes_opt_" + count).html("");
                      break;
                    case 5:
                      this.optionE = ele.opT_CONTENT;
                      $("#field_opt_content_" + count + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                      $("#valid_mes_opt_" + count).html("");
                      break;
                    case 6:
                      this.optionF = ele.opT_CONTENT;
                      $("#field_opt_content_" + count + ">.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
                      $("#valid_mes_opt_" + count).html("");
                      break;
                    default:
                      break;
                  }
                  count++;
                });
              }

              this.checkField();
              $("#modal_title_que").html("Sửa câu hỏi");
              this.modal.show();
            });
          });
        }
      });
    }
    else { // insert
      if (this.passageId == null && this.partId == null) {
        this.question = {
          id: 0,
          quE_CONTENT: "",
          quE_REFERENCE: "",
          quE_ISSHUFFLE: false,
          quE_LEVEL: 0,
          quE_OPTCOLUMN: 1,
          quE_STATUS: 1,
          quE_ANTYID: 0,
          quE_PARID: 0,
          quE_PASID: 0
        } as Question;
      }
      else {
        if (this.passageId != null) {
          this.sub_id = this.passage.parts.subjects.id;
          this.question = {
            id: 0,
            quE_CONTENT: "",
            quE_REFERENCE: "",
            quE_ISSHUFFLE: false,
            quE_LEVEL: 0,
            quE_OPTCOLUMN: 1,
            quE_STATUS: 1,
            quE_ANTYID: this.passage.paS_ANTYID,
            quE_PARID: this.passage.parts.id,
            quE_PASID: this.passageId
          } as Question;

          if (this.passage.parts.paR_ISALLOWSHUFFLEOPTION == true) {
            $("#field_que_5").prop("disabled", false);
          }
          else {
            $("#field_que_5").prop("disabled", true);
          }
        }
        else {
          this.sub_id = this.part.subjects.id;
          this.question = {
            id: 0,
            quE_CONTENT: "",
            quE_REFERENCE: "",
            quE_ISSHUFFLE: false,
            quE_LEVEL: 0,
            quE_OPTCOLUMN: 1,
            quE_STATUS: 1,
            quE_ANTYID: 0,
            quE_PARID: this.part.id,
            quE_PASID: 0
          } as Question;

          if (this.part.paR_ISALLOWSHUFFLEOPTION == true) {
            $("#field_que_5").prop("disabled", false);
          }
          else {
            $("#field_que_5").prop("disabled", true);
          }
        }
      }

      this.clearField();
      $("#modal_title_que").html("Thêm câu hỏi");
      this.modal.show();
    }
  }
  save() {
    if (this.validate() == false) {
      return false;
    }
    this.lockModal("addupd");

    if (this.partId != null)
      this.question.quE_PARID = this.partId;

    if ($("#field_que_5").is(":checked"))
      this.question.quE_ISSHUFFLE = true;
    else
      this.question.quE_ISSHUFFLE = false;

    if (this.question.quE_ANTYID == 1 || this.question.quE_ANTYID == 2) {
      let arr = [this.optionA, this.optionB, this.optionC, this.optionD, this.optionE, this.optionF];
      for (let i = 0; i < arr.length; i++) {
        if (i < 3) {
          this.listOption.push({
            id: 0,
            opT_CONTENT: arr[i],
            opT_ORDER: i + 1,
            opT_ISCORRECT: $("#field_opt_" + (i + 1)).is(":checked"),
            opT_QUEID: 0
          } as Option);
        }
        else {
          if ($("#option" + (i + 1)).is(":visible")) {
            this.listOption.push({
              id: 0,
              opT_CONTENT: arr[i],
              opT_ORDER: i + 1,
              opT_ISCORRECT: $("#field_opt_" + (i + 1)).is(":checked"),
              opT_QUEID: 0
            } as Option);
          }
        }
      }
    }
    else if (this.question.quE_ANTYID == 3) {
      this.listOption.push({
        id: 0,
        opT_CONTENT: $("#field_opt_content_1").val(),
        opT_ORDER: 1,
        opT_ISCORRECT: $("#field_opt_1").is(":checked"),
        opT_QUEID: 0
      } as Option);
      this.listOption.push({
        id: 0,
        opT_CONTENT: $("#field_opt_content_2").val(),
        opT_ORDER: 2,
        opT_ISCORRECT: $("#field_opt_2").is(":checked"),
        opT_QUEID: 0
      } as Option);
    }

    this.questionRequest = {
      question: this.question,
      listOption: this.listOption
    } as questionRequest;

    if (this.question.id === 0) { // insert
      this.question.quE_CREATEDBY = parseInt(this.cookieSer.get('Id'));
      if (this.file != null) {
        const formData = new FormData();
        formData.append('file', this.file);
        this.fileSer.add("Question", formData).subscribe(res => {
          this.question.quE_MEDIA = res.data;
          this.questionSer.add(this.questionRequest).subscribe(res1 => {
            if (res1.errorCode === 0) {
              if (this.partId == null && this.passageId == null) {
                this.questionSer.getAll().subscribe(res2 => {
                  this.questions = res2.data;
                  this.rerender();
                });
              }
              else {
                if (this.partId != null) {
                  this.questionSer.getListByPartId(this.partId).subscribe(res2 => {
                    this.questions = res2.data;
                    this.rerender();
                    $("#pas_tab").addClass("disabled");
                  });
                }
                else {
                  this.questionSer.getListByPassageId(this.passageId).subscribe(res2 => {
                    this.questions = res2.data;
                    this.rerender();
                  });
                }
              }
              this.modal.hide();
              this.pnotify.success({
                title: 'Thông báo',
                text: 'Thêm câu hỏi thành công!'
              });
            }
            else {
              this.modal.hide();
              this.pnotify.error({
                title: 'Thông báo',
                text: res1.message
              });
            }
          });
        });
      }
      else {
        this.questionSer.add(this.questionRequest).subscribe(res => {
          if (res.errorCode === 0) {
            if (this.partId == null && this.passageId == null) {
              this.questionSer.getAll().subscribe(res1 => {
                this.questions = res1.data;
                this.rerender();
              });
            }
            else {
              if (this.partId != null) {
                this.questionSer.getListByPartId(this.partId).subscribe(res1 => {
                  this.questions = res1.data;
                  this.rerender();
                  $("#pas_tab").addClass("disabled");
                });
              }
              else {
                this.questionSer.getListByPassageId(this.passageId).subscribe(res1 => {
                  this.questions = res1.data;
                  this.rerender();
                });
              }
            }
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Thêm câu hỏi thành công!'
            });
          }
          else {
            this.modal.hide();
            this.pnotify.error({
              title: 'Thông báo',
              text: res.message
            });
          }
        });
      }
    }
    else { // update
      this.question.quE_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      if (this.file != null) {
        if (this.question.quE_MEDIA != null && this.question.countTest == 0) {
          const formData = new FormData();
          formData.append('file', this.file);
          this.fileSer.put(this.question.quE_MEDIA.split('/')[3], "Question", formData).subscribe(res2 => {
            this.question.quE_MEDIA = res2.data;
            this.questionSer.put(this.questionRequest).subscribe(res => {
              if (res.errorCode === 0) {
                if (this.partId == null && this.passageId == null) {
                  this.questionSer.getAll().subscribe(res1 => {
                    this.questions = res1.data;
                    this.rerender();
                  });
                }
                else {
                  if (this.partId != null) {
                    this.questionSer.getListByPartId(this.partId).subscribe(res1 => {
                      this.questions = res1.data;
                      this.rerender();
                    });
                  }
                  else {
                    this.questionSer.getListByPassageId(this.passageId).subscribe(res1 => {
                      this.questions = res1.data;
                      this.rerender();
                    });
                  }
                }
                this.modal.hide();
                if (this.modalDetail.isShown) {
                  this.openModalDetail(res.data.id);
                }
                this.pnotify.success({
                  title: 'Thông báo',
                  text: 'Sửa câu hỏi thành công!'
                });
              }
              else {
                this.modal.hide();
                this.pnotify.error({
                  title: 'Thông báo',
                  text: res.message
                });
              }
            });
          });
        }
        else {
          const formData = new FormData();
          formData.append('file', this.file);
          this.fileSer.add("Question", formData).subscribe(res2 => {
            this.question.quE_MEDIA = res2.data;
            this.questionSer.put(this.questionRequest).subscribe(res => {
              if (res.errorCode === 0) {
                if (this.partId == null && this.passageId == null) {
                  this.questionSer.getAll().subscribe(res1 => {
                    this.questions = res1.data;
                    this.rerender();
                  });
                }
                else {
                  if (this.partId != null) {
                    this.questionSer.getListByPartId(this.partId).subscribe(res1 => {
                      this.questions = res1.data;
                      this.rerender();
                    });
                  }
                  else {
                    this.questionSer.getListByPassageId(this.passageId).subscribe(res1 => {
                      this.questions = res1.data;
                      this.rerender();
                    });
                  }
                }
                this.modal.hide();
                if (this.modalDetail.isShown) {
                  this.openModalDetail(res.data.id);
                }
                this.pnotify.success({
                  title: 'Thông báo',
                  text: 'Sửa câu hỏi thành công!'
                });
              }
              else {
                this.modal.hide();
                this.pnotify.error({
                  title: 'Thông báo',
                  text: res.message
                });
              }
            });
          });
        }
      }
      else {
        if (this.question.quE_MEDIA != null && this.fileIsExist == null) {
          if (this.question.countTest == 0) {
            this.fileSer.delete('Question', this.question.quE_MEDIA.split('/')[3]).subscribe(res => {
              this.question.quE_MEDIA = null;
              this.questionSer.put(this.questionRequest).subscribe(res => {
                if (res.errorCode === 0) {
                  if (this.partId == null && this.passageId == null) {
                    this.questionSer.getAll().subscribe(res1 => {
                      this.questions = res1.data;
                      this.rerender();
                    });
                  }
                  else {
                    if (this.partId != null) {
                      this.questionSer.getListByPartId(this.partId).subscribe(res1 => {
                        this.questions = res1.data;
                        this.rerender();
                      });
                    }
                    else {
                      this.questionSer.getListByPassageId(this.passageId).subscribe(res1 => {
                        this.questions = res1.data;
                        this.rerender();
                      });
                    }
                  }
                  this.modal.hide();
                  if (this.modalDetail.isShown) {
                    this.openModalDetail(res.data.id);
                  }
                  this.pnotify.success({
                    title: 'Thông báo',
                    text: 'Sửa câu hỏi thành công!'
                  });
                }
                else {
                  this.modal.hide();
                  this.pnotify.error({
                    title: 'Thông báo',
                    text: res.message
                  });
                }
              });
            });
          }
          else {
            this.question.quE_MEDIA = null;
            this.questionSer.put(this.questionRequest).subscribe(res => {
              if (res.errorCode === 0) {
                if (this.partId == null && this.passageId == null) {
                  this.questionSer.getAll().subscribe(res1 => {
                    this.questions = res1.data;
                    this.rerender();
                  });
                }
                else {
                  if (this.partId != null) {
                    this.questionSer.getListByPartId(this.partId).subscribe(res1 => {
                      this.questions = res1.data;
                      this.rerender();
                    });
                  }
                  else {
                    this.questionSer.getListByPassageId(this.passageId).subscribe(res1 => {
                      this.questions = res1.data;
                      this.rerender();
                    });
                  }
                }
                this.modal.hide();
                if (this.modalDetail.isShown) {
                  this.openModalDetail(res.data.id);
                }
                this.pnotify.success({
                  title: 'Thông báo',
                  text: 'Sửa câu hỏi thành công!'
                });
              }
              else {
                this.modal.hide();
                this.pnotify.error({
                  title: 'Thông báo',
                  text: res.message
                });
              }
            });
          }
        }
        else {
          this.questionSer.put(this.questionRequest).subscribe(res => {
            if (res.errorCode === 0) {
              if (this.partId == null && this.passageId == null) {
                this.questionSer.getAll().subscribe(res1 => {
                  this.questions = res1.data;
                  this.rerender();
                });
              }
              else {
                if (this.partId != null) {
                  this.questionSer.getListByPartId(this.partId).subscribe(res1 => {
                    this.questions = res1.data;
                    this.rerender();
                  });
                }
                else {
                  this.questionSer.getListByPassageId(this.passageId).subscribe(res1 => {
                    this.questions = res1.data;
                    this.rerender();
                  });
                }
              }
              this.modal.hide();
              if (this.modalDetail.isShown) {
                this.openModalDetail(res.data.id);
              }
              this.pnotify.success({
                title: 'Thông báo',
                text: 'Sửa câu hỏi thành công!'
              });
            }
            else {
              this.modal.hide();
              this.pnotify.error({
                title: 'Thông báo',
                text: res.message
              });
            }
          });
        }
      }
    }
  }
  openModalDelete(id: number) {
    this.unlockModal("del");
    this.questionSer.get(id).subscribe(res => {
      this.question = res.data;
      this.modalDelete.show();
    });
  }

  delete() {
    this.lockModal("del");
    if (this.question.id != 0) {
      this.questionSer.delete(this.question.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          if (this.partId == null && this.passageId == null) {
            this.questionSer.getAll().subscribe(res1 => {
              this.questions = res1.data;
              this.rerender();
            });
          }
          else {
            if (this.partId != null) {
              this.questionSer.getListByPartId(this.partId).subscribe(res1 => {
                this.questions = res1.data;
                this.rerender();
                if (res1.data[0] == null) {
                  $("#pas_tab").removeClass("disabled");
                }
              });
            }
            else {
              this.questionSer.getListByPassageId(this.passageId).subscribe(res1 => {
                this.questions = res1.data;
                this.rerender();
              });
            }
          }
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Xóa câu hỏi thành công!'
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
        this.modalDetail.hide();
      });
    }
  }

  openModalDeleteMul() {
    this.unlockModal("muldel");
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      let arr = [];
      dtInstance.rows({ selected: true }).every(function () {
        if (this.data()[1] != null) {
          arr.push(this.data()[1]);
        }
      });
      if (arr.length == 0) {
        this.isListItemNull = true;
        this.modalDeletMul.show();
      }
      else {
        this.isListItemNull = false;
        this.questionSer.getListQuestionMultiDel(arr).subscribe(res => {
          this.questionsMultiDel = res.data;
          this.modalDeletMul.show();
        });
      }
    });
  }

  deleteMul() {
    this.lockModal("muldel");
    this.questionSer.deleteMulti(this.questionsMultiDel, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
      if (res.errorCode === 0) {
        if (this.partId == null && this.passageId == null) {
          this.questionSer.getAll().subscribe(res1 => {
            this.questions = res1.data;
            this.rerender();
          });
        }
        else {
          if (this.partId != null) {
            this.questionSer.getListByPartId(this.partId).subscribe(res1 => {
              this.questions = res1.data;
              this.rerender();
              if (res1.data[0] == null) {
                $("#pas_tab").removeClass("disabled");
              }
            });
          }
          else {
            this.questionSer.getListByPassageId(this.passageId).subscribe(res1 => {
              this.questions = res1.data;
              this.rerender();
            });
          }
        }
        this.modalDeletMul.hide();
        this.pnotify.success({
          title: 'Thông báo',
          text: "Xóa danh sách câu hỏi thành công!"
        });
      }
      else {
        this.modalDeletMul.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message
        });
      }
    });
  }

  openModalDetail(id: number) {
    this.questionSer.get(id).subscribe(res => {
      this.question = res.data;
      if (res.data.quE_MEDIA != null) {
        if ($.inArray(res.data.quE_MEDIA.substr(res.data.quE_MEDIA.length - 3, res.data.quE_MEDIA.length), this.videoExtension) != -1) {
          this.mediaType = "video";
        }
        else {
          this.mediaType = "audio";
        }
      }
      this.modalDetail.show();
    });
  }

  lockModal(key: string) {
    $(".close_modal_que_" + key).prop("disabled", true);
    $("#save_modal_que_" + key).prop("disabled", true);
    $("#save_modal_que_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_que_" + key).prop("disabled", false);
    $("#save_modal_que_" + key).prop("disabled", false);
    $("#save_modal_que_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  loadTab(key: number) {
    this.tabControl = key;
  }

  downloadFile(event = null) {
    if (event != null) {
      event.preventDefault();
    }
    saveAs(this.sampleFileLink);
  }

  openModalImport() {
    this.unlockModal("import");

    // clear modal
    this.fileImport = null;
    $("#field_que_import").val("");
    this.fileImportIsExist = null;
    $("#label_field_que_import").html("Tải tệp lên...");
    $("#field_que_import").removeClass('is-invalid is-valid');
    $("#field_que_import").html("");
    $("#valid_mes_que_import").html("");
    for (let i = 1; i <= 3; i++) {
      $("#field_import_" + i).removeClass('is-invalid is-valid');
      $("#warning_mes_import_" + i).html("");
    }
    this.subIdImport = 0;
    this.parIdImport = 0;
    this.passageIdImport = 0;
    this.errorImport = false;

    this.questionSer.getSampleFileLink().subscribe(res => {
      this.sampleFileLink = res.data;
    });
    this.modalImport.show();
  }

  ImportExcel() {
    if (!this.validate_que_importfile()) {
      $("#valid_que_import").show(0).delay(3000).hide(0);
      return false;
    }
    if (this.partId == null && this.passageId == null) {
      if (!this.validate_import_subid()) {
        $("#valid_que_import").show(0).delay(3000).hide(0);
        return false;
      }
      if (!this.validate_import_parid()) {
        $("#valid_que_import").show(0).delay(3000).hide(0);
        return false;
      }
      if (!this.validate_import_pasid()) {
        $("#valid_que_import").show(0).delay(3000).hide(0);
        return false;
      }
    }

    this.lockModal("import");
    const formData = new FormData();
    formData.append('file', this.fileImport);
    if (this.partId != null) {
      this.parIdImport = this.partId;
      this.passageIdImport = 0;
    }
    else if (this.passageId != null) {
      this.parIdImport = 0;
      this.passageIdImport = this.passageId;
    }
    else {
      if (this.passageIdImport != null && this.passageIdImport != 0) {
        this.parIdImportError = this.parIdImport;
        this.parIdImport = 0;
      }
      else {
        this.passageIdImport = 0;
      }
    }
    this.questionSer.importExcel(Number(this.cookieSer.get('Id')), this.passageIdImport, this.parIdImport, formData).subscribe(res => {
      // console.log(res);      
      if (res.errorCode === 0) {
        this.errorImport = false;
        if (this.partId == null && this.passageId == null) {
          this.questionSer.getAll().subscribe(res1 => {
            this.questions = res1.data;
            this.rerender();
            this.modalImport.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: "Thêm danh sách câu hỏi thành công!"
            });
          });
        }
        else {
          if (this.partId != null) {
            this.questionSer.getListByPartId(this.partId).subscribe(res2 => {
              this.questions = res2.data;
              this.rerender();
              $("#pas_tab").addClass("disabled");
              this.modalImport.hide();
              this.pnotify.success({
                title: 'Thông báo',
                text: "Thêm danh sách câu hỏi thành công!"
              });
            });
          }
          else {
            this.questionSer.getListByPassageId(this.passageId).subscribe(res2 => {
              this.questions = res2.data;
              this.rerender();
              this.modalImport.hide();
              this.pnotify.success({
                title: 'Thông báo',
                text: "Thêm danh sách câu hỏi thành công!"
              });
            });
          }
        }
      }
      else if (res.errorCode == 405) {
        this.unlockModal("import");
        this.parIdImport = this.parIdImportError;
        this.errorImport = true;
        this.errorImportLog = res.message.split("<divider>")[1];
        this.removeFileImport();
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message.split("<divider>")[0]
        });
      }
      else {
        this.modalImport.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message
        });
      }
    });
  }
}
