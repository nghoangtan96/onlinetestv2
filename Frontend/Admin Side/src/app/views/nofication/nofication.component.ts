import { Component, OnInit, ViewChild } from '@angular/core';
import { NoficationService, Nofication } from '../../services/nofication.service';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nofication',
  templateUrl: './nofication.component.html',
  styleUrls: ['./nofication.component.css']
})
export class NoficationComponent implements OnInit {

  nofid: number;

  pnotify = undefined;
  nofications: [Nofication];
  nofication: Nofication = {} as Nofication;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;
  constructor(private noficationSer: NoficationService, private pNotify: PnotifyService,
    private cookieSer: CookieService, private route: ActivatedRoute) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.noficationSer.getAllNoficationByUserId(Number(this.cookieSer.get('Id'))).subscribe(res => {
      this.nofications = res.data;
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
  }

  // ngAfterViewInit() {
  //   this.route.queryParams.subscribe(params => {
  //     this.nofid = params['nofid'];
  //     if (this.nofid != null) {
  //       this.openModalDetail(this.nofid);
  //     }
  //   });
  // }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  // openModalDetail(id: number) {
  //   this.noficationSer.get(id).subscribe(res => {
  //     this.nofication = res.data;
  //     // console.log(res.data);
  //     this.modalDetail.show();
  //   });
  // }
}
