import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Position, PositionService } from '../../services/position.service';
import { ExportService } from '../../services/export.service';
import { UserPermissionService, User_Permission } from '../../services/user-permission.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.scss']
})
export class PositionComponent implements OnInit {

  fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  fileExtension = '.xlsx';
  exportData: any = [];

  positions: [Position];
  position: Position = {} as Position;

  userPers: [User_Permission];
  id_check: any[];
  pnotify = undefined;
  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  // dtOptions: DataTables.Settings = {};
  constructor(private posSer: PositionService, private pNotify: PnotifyService, private exportService: ExportService,
    private userPerSer: UserPermissionService, private router: Router,
    private cookieSer: CookieService) {
    this.pnotify = this.pNotify.getPNotify();
  }
  ngOnInit() {
    if (this.cookieSer.check('isLogin') == false || this.cookieSer.get('isLogin') == '0') {
      this.router.navigate(['/login']);
    }
    if (parseInt(this.cookieSer.get('Role')) == 1) {
    }
    else {
      this.router.navigate(['/404']);
    }
    // this.userPerSer.getlistperbyuserid(parseInt(this.cookieSer.get('Id'))).subscribe(res => {
    //   this.userPers = res.data;
    //   let check = 0;
    //   for (let i = 0; i < this.userPers.length; i++) {
    //     if (this.userPers[i].useP_PERID == 1){
    //       check = 1;
    //       break;
    //     }
    //   }
    //   if (check === 0) {
    //     this.router.navigate(['/404']);
    //   }
    // });
    this.dtOptions = {
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      order: [[1, 'asc']]
    };
    this.posSer.getAll().subscribe(res => {
      this.positions = res.data;
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
  }
  export(event = null) {
    if (event != null) {
      event.preventDefault();
    }
    this.exportData = [];
    this.dtElement.dtInstance.then((dtInstance: any) => {
      let tmp = [];
      dtInstance.rows({ selected: true }).every(function () {
        if (this.data()[1] != null) {
          tmp.push(this.data()[1]);
        }
      });
      this.id_check = null;
      this.id_check = tmp;
      var id_ex = '';
      for (let i = 0; i < this.id_check.length; i++) {
        if (id_ex == '') {
          id_ex = this.id_check[i].toString();
        }
        else {
          id_ex = id_ex + "_" + this.id_check[i].toString();
        }
      }
      if (id_ex == '') id_ex = 'full';
      this.posSer.getAllDataExport(id_ex).subscribe(res => {
        if (res.data != null) {
          for (let i = 0; i < res.data.length; i++) {
            this.exportData.push({
              STT: i + 1,
              'Chức vụ': res.data[i].poS_NAME,
              'Mức lương': res.data[i].poS_SALARYRATE,
              'Trạng thái': res.data[i].poS_STATUS == 0 ? 'Tắt' : 'Sử dụng',
              'Sử dụng hệ thống': res.data[i].poS_ISALLOWUSER == true ? 'Cho phép' : 'Không cho phép'
            });
          }
          this.exportService.exportExcel(this.exportData, 'Danh sách chức vụ');
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Xuất excel thành công!'
          });
          this.id_check = null;
        }
      });
    });
  }
  Select_Clear_All() {
    $(".toggle-all").closest('tr').toggleClass('selected');
    if ($(".toggle-all").closest('tr').hasClass('selected')) {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.select();
        });
      });
    }
    else {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.deselect();
        });
      });
    }
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  // ngAfterViewInit(): void {
  //   this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
  //     dtInstance.columns().every(function () {
  //       const that = this;
  //       $('input', this.footer()).on('keyup change', function () {
  //         if (that.search() !== this['value']) {
  //           that
  //             .search(this['value'])
  //             .draw();
  //         }
  //       });
  //     });
  //   });
  // }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  validate_pos_name() {
    if ($("#field_1").val() == '') {
      $("#field_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_1").html("Vui lòng nhập chức vụ!");
      return false;
    }
    else {
      let isExist = false;
      for (let i = 0; i < this.positions.length; i++) {
        if ($("#field_1").val().toString().trim().toLowerCase() == this.positions[i].poS_NAME.toLowerCase() && this.positions[i].id != this.position.id) {
          isExist = true;
          break;
        }
      }
      if (isExist) {
        $("#field_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_1").html("Chức vụ đã tồn tại!");
        return false;
      }
      $("#field_1").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_1").html("");
      return true;
    }
  }
  validate_pos_salary() {
    if ($("#field_2").val() == '') {
      $("#field_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_2").html("Vui lòng nhập lương!");
      return false;
    }
    else {
      if ($("#field_2").val() <= 0) {
        $("#field_2").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_2").html("Mức lương không được âm!");
        return false;
      }
      else {
        $("#field_2").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_2").html("");
        return true;
      }
    }
  }

  validate() {
    var check = true;
    if (this.validate_pos_name() == false)
      check = false;
    if (this.validate_pos_salary() == false)
      check = false;

    //validate all
    if (check == false)
      return false;
    else
      return true;
  }
  clearField() {
    for (let i = 1; i <= 2; i++) {
      var field = "#field_" + i;
      var validmes = "#valid_mes_" + i;
      $(field).removeClass('is-invalid is-valid')
      $(validmes).html("");
    }
  }
  checkField() {
    for (let i = 1; i <= 2; i++) {
      var field = "#field_" + i;
      var validmes = "#valid_mes_" + i;
      $(field).removeClass('is-invalid').addClass('is-valid');
      $(validmes).html("");
    }
  }

  showModal(event = null, id: Number = 0) {
    if (event != null) {
      event.preventDefault();
    }
    if (id > 0) {
      this.checkField();
      this.posSer.get(id).subscribe(res => {
        this.position = res.data;
        $("#modal_title").html("Sửa chức vụ");
        this.modal.show();
      });
    }
    else {
      this.clearField();
      this.position = {
        id: 0
      } as Position
      $("#modal_title").html("Thêm chức vụ");
      this.modal.show();
    }
  }
  save() {
    if (this.validate() == false) {
      return false;
    }
    if (this.position.id === 0) {
      this.position.poS_CREATEBY = parseInt(this.cookieSer.get('Id'));
      if ($("#field_7").is(":checked")) {
        this.position.poS_STATUS = 1;
      }
      else {
        this.position.poS_STATUS = 0;
      }
      if ($("#field_8").is(":checked")) {
        this.position.poS_ISALLOWUSER = true;
      }
      else {
        this.position.poS_ISALLOWUSER = false;
      }

      // add
      this.posSer.add(this.position).subscribe(res => {
        if (res.errorCode === 0) {
          this.posSer.getAll().subscribe(resList => {
            this.positions = resList.data;
            this.rerender();
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Thêm chức vụ thành công!'
            });
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: 'Tên chức vụ không được trùng!'
          });
        }
      });
    }
    else {
      this.position.poS_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      //update
      if ($("#field_7").is(":checked")) {
        this.position.poS_STATUS = 1;
      }
      else {
        this.position.poS_STATUS = 0;
      }
      if ($("#field_8").is(":checked")) {
        this.position.poS_ISALLOWUSER = true;
      }
      else {
        this.position.poS_ISALLOWUSER = false;
      }
      this.posSer.put(this.position).subscribe(res => {
        if (res.errorCode === 0) {
          this.posSer.getAll().subscribe(resList => {
            this.positions = resList.data;
            this.rerender();
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Cập nhật chức vụ thành công!'
            });
          });
        }
        else {
          if (res.errorCode == 1) {
            this.pnotify.error({
              title: 'Thông báo',
              text: 'Tên chức vụ không được trùng!'
            });
          }
          if (res.errorCode == 2) {
            this.pnotify.error({
              title: 'Thông báo',
              text: 'Không được tắt trạng thái khi chức vụ đang được sử dụng!'
            });
          }
        }
      });
    }
  }
  delete() {
    let id = $("#id_delete").val();
    this.posSer.delete(id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
      if (res.errorCode === 0) {
        const deletedItem = this.positions.find(x => x.id == id);
        const index = this.positions.indexOf(deletedItem);
        this.positions.splice(index, 1);
        this.rerender();
        this.modalDelete.hide();
        this.pnotify.success({
          title: 'Thông báo',
          text: 'Xóa loại phản hồi "' + res.data.poS_NAME + '" thành công!'
        });
      }
      else {
        this.modalDelete.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: 'Xóa loại phản hồi "' + res.data.poS_NAME + '" thất bại!'
        });
      }
    });
  }
  open_Delete(event, id) {
    if (event != null) {
      event.preventDefault();
    }
    $("#id_delete").val(id);
    var temp = "#pos_" + id;
    $("#delete_message").html("Bạn có chắc chắn muốn xóa chức vụ <strong class='text-danger'>" + $(temp).html() + "</strong> không?");
    this.modalDelete.show();
  }
}

