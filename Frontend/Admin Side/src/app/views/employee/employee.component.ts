import { Component, OnInit, ViewChild, HostListener, OnDestroy, ViewChildren, QueryList, Input } from '@angular/core';
import { UserService, User } from '../../services/user.service';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { DomSanitizer } from '@angular/platform-browser';
import { FileService } from '../../services/file.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Employee, EmployeeService } from '../../services/employee.service';
import { PermissionService, Permission } from '../../services/permission.service';
import { EmployeeRequest } from '../../models/employee_request';
import { UserPermissionService, User_Permission } from '../../services/user-permission.service';
import { Subject, Subscription } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';;
import { Router } from '@angular/router';
import { Setting, SettingService } from '../../services/setting.service';
import { Faculty, FacultyService } from '../../services/faculty.service';
import { ValidateService } from '../../services/validate.service';
import { SubjectService, Subjectt } from '../../services/subject.service';
import { EmployeeSubjectService } from '../../services/employee-subject.service';
import { saveAs } from 'file-saver';
import { SignalrService } from '../../services/signalr.service';
import { Student, StudentService } from '../../services/student.service';
import { RoleService } from '../../services/role.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
})
export class EmployeeComponent implements OnInit {

  @Input() public permissionId: number; //parse parameter through selector
  @Input() public facultyId: number; //parse parameter through selector

  employees: [Employee];
  employeesMultiDel: [Employee];
  employeesExport: [Employee];
  employee: Employee = {} as Employee;
  students: [Student];
  user: User = {} as User;
  users: [User];
  faculties: [Faculty];
  faculty: Faculty = {} as Faculty;
  facultyConstraint: Faculty;
  subjects: [Subjectt];
  permissions: [Permission];
  permission: Permission = {} as Permission;
  userPermission: User_Permission[] = [];
  pnotify = undefined;
  list_subject: any = [];
  list_permission: any = [];
  employeeRequest: EmployeeRequest = {} as EmployeeRequest;

  file: File = null;
  url: any = "";
  fileIsExist: boolean;
  isListItemNull: boolean = true;
  autoGenCode: Setting = {} as Setting;
  lengthUsername: Setting = {} as Setting;
  lengthPassword: Setting = {} as Setting;
  exportType: number = 1;

  emp_add: boolean = true;
  emp_upd: boolean = true;
  emp_del: boolean = true;
  emp_con: boolean = true;
  emp_ref: boolean = true;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  dtTrigger1: Subject<any> = new Subject();
  dtOptions1: any = {};
  @ViewChildren(DataTableDirective)
  dtElements: QueryList<DataTableDirective>;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild('modalDeletMul', { static: false }) modalDeletMul: ModalDirective;
  @ViewChild('modalExport', { static: false }) modalExport: ModalDirective;
  constructor(private userSer: UserService, private pNotify: PnotifyService,
    private cookieSer: CookieService, private _DomSanitizer: DomSanitizer, //unsafe bug fix
    private fileSer: FileService, private permissionSer: PermissionService,
    private employeeSer: EmployeeService, private userPermissionSer: UserPermissionService,
    private facultySer: FacultyService, private studentSer: StudentService,
    private router: Router, private settingSer: SettingService,
    private validateSer: ValidateService, private subjectSer: SubjectService,
    private employeeSubjectSer: EmployeeSubjectService, private signalRService: SignalrService,
    private roleSer: RoleService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.dtOptions = {
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      order: [[1, 'asc']]
    };
    if (this.permissionId != null) {
      this.employeeSer.getListByPermissionId(this.permissionId).subscribe(res => {
        //table
        this.employees = res.data;
        // console.log(res.data);
        this.dtTrigger.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
        //excel
        this.employeesExport = res.data;
        this.dtTrigger1.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
      });
      this.permissionSer.get(this.permissionId).subscribe(res => {
        this.permission = res.data;
      });
    }
    else if (this.facultyId != null) {
      this.employeeSer.getListByFacultyId(this.facultyId).subscribe(res => {
        //table
        this.employees = res.data;
        // console.log(res.data);
        this.dtTrigger.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
        //excel
        this.employeesExport = res.data;
        this.dtTrigger1.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
      });
      this.facultySer.get(this.facultyId).subscribe(res => {
        this.faculty = res.data;
      });
    }
    else {
      this.employeeSer.getAll().subscribe(res => {
        //table
        this.employees = res.data;
        this.dtTrigger.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
        //excel
        this.employeesExport = res.data;
        this.dtTrigger1.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
      });
    }
    this.studentSer.getAll().subscribe(res => {
      this.students = res.data;
    });
    this.facultySer.getAll().subscribe(res => {
      this.faculties = res.data;
    });
    this.permissionSer.getAll().subscribe(res => {
      this.permissions = res.data;
    });
    this.settingSer.get(3).subscribe(res => {
      this.autoGenCode = res.data;
    });
    this.settingSer.get(1).subscribe(res => {
      this.lengthUsername = res.data;
    });
    this.settingSer.get(2).subscribe(res => {
      this.lengthPassword = res.data;
    });
    //permission
    this.roleSer.get(88).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'emp_add':
                this.emp_add = false;
                break;
              case 'emp_upd':
                this.emp_upd = false;
                break;
              case 'emp_del':
                this.emp_del = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    this.roleSer.get(105).subscribe(res => {
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        let count = 0;
        let countNoConstraint = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == false) != null) {
              countNoConstraint++;
            }
          }
        }
        if (count > 0 && countNoConstraint == 0) {
          this.emp_con = false;
          this.userSer.get(Number(this.cookieSer.get('Id'))).subscribe(res2 => {
            this.facultySer.get(res2.data.employees[0].emP_FACID).subscribe(res3 => {
              this.facultyConstraint = res3.data;
            });
          });
        }
        else if (countNoConstraint > 0) {
          this.emp_con = true;
        }
      })
    });
    //realtime
    this.signalRService.employee.subscribe((res) => {
      if (res === true) {
        if (this.permissionId != null) {
          this.employeeSer.getListByPermissionId(this.permissionId).subscribe(res1 => {
            this.employees = res1.data;
            this.rerender();
            this.employeesExport = res1.data;
            this.rerender1();
          });
        }
        else if (this.facultyId != null) {
          this.employeeSer.getListByFacultyId(this.facultyId).subscribe(res1 => {
            this.employees = res1.data;
            this.rerender();
            this.employeesExport = res1.data;
            this.rerender1();
          });
        }
        else {
          this.employeeSer.getAll().subscribe(res1 => {
            this.employees = res1.data;
            this.rerender();
            this.employeesExport = res1.data;
            this.rerender1();
          });
        }
      }
    });
    this.signalRService.student.subscribe((res) => {
      if (res === true) {
        this.studentSer.getAll().subscribe(res1 => {
          this.students = res1.data;
        });
      }
    });
    this.signalRService.faculty.subscribe((res) => {
      if (res === true) {
        this.facultySer.getAll().subscribe(res1 => {
          this.faculties = res1.data;
        });
      }
    });
    this.signalRService.permission.subscribe((res) => {
      if (res === true) {
        this.permissionSer.getAll().subscribe(res1 => {
          this.permissions = res1.data;
        });
      }
    });
    this.signalRService.setting.subscribe((res1) => {
      if (res1 === true) {
        this.settingSer.get(3).subscribe(res => {
          this.autoGenCode = res.data;
        });
        this.settingSer.get(1).subscribe(res => {
          this.lengthUsername = res.data;
        });
        this.settingSer.get(2).subscribe(res => {
          this.lengthPassword = res.data;
        });
      }
    });
  }

  onSelectFile(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      this.file = event.target.files[0];

      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      }

      $("#label_field_emp_15").html(
        this.file.name.length > 30 ?
          this.file.name.substring(0, 9) + "..." + this.file.name.substring(this.file.name.length - 9, this.file.name.length) :
          this.file.name
      );
      this.fileIsExist = true;
    }
  }

  removeFile(event = null) {
    if (event != null) {
      event.preventDefault();
    }
    this.file = null;
    $("#field_emp_15").val("");
    this.fileIsExist = null;
    this.url = "";
    $("#label_field_emp_15").html("Tải tệp lên...");
    this.validate_emp_avatar();
  }

  Select_Clear_All() {
    $(".toggle-emp-all").closest('tr').toggleClass('selected');
    if ($(".toggle-emp-all").closest('tr').hasClass('selected')) {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_main") {
              dtInstance.rows({ search: 'applied' }).every(function () {
                this.select();
              });
            }
          });
        }
      });
    }
    else {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_main") {
              dtInstance.rows({ search: 'applied' }).every(function () {
                this.deselect();
              });
            }
          });
        }
      });
    }
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    this.dtTrigger1.unsubscribe();
  }
  rerender(): void {
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      if (dtElement.dtInstance != null) {
        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == "tb_main") {
            dtInstance.destroy();
            this.dtTrigger.next();
            this.dtElements.forEach((dtElement: DataTableDirective) => {
              if (dtElement.dtInstance != null) {
                dtElement.dtInstance.then((dtInstance: any) => {
                  if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
                    dtInstance.columns().every(function () {
                      const that = this;
                      $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this['value']) {
                          that
                            .search(this['value'])
                            .draw();
                        }
                      });
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }

  rerender1(): void {
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      if (dtElement.dtInstance != null) {
        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == "tb_sub") {
            dtInstance.destroy();
            this.dtTrigger1.next();
          }
        });
      }
    });
  }

  GenCode() {
    let code = "XXXXX";
    if (this.employee.emP_FACID != 0 && this.employee.emP_FACID != null) {
      code = this.faculties.find(x => x.id == this.employee.emP_FACID).faC_CODE + code.substr(2, 3);

      let count = 0;
      for (let i = 0; i < this.employees.length; i++) {
        if (this.employees[i].emP_FACID == this.employee.emP_FACID) {
          count++;
        }
      }

      do {
        code = code.substr(0, 2) + (count < 9 ? "00" + (count + 1) : (count < 99 ? "0" + (count + 1) : (count + 1)))
        count++;
      } while (this.employees.find(x => x.emP_CODE == code) != null);
    }

    return code;
  }

  generateSubjects() {
    this.subjectSer.getListSubjectByFacultyId(this.employee.emP_FACID).subscribe(res => {
      this.subjects = res.data;
      this.list_subject = [];
      this.employee.emP_CODE = this.GenCode();
    });
  }

  generateUser() {
    if (this.employee.emP_CODE.length >= this.lengthUsername.seT_VALUE) {
      this.user.usE_USERNAME = this.employee.emP_CODE;
    }
    else {
      this.user.usE_USERNAME = this.removeAccents(this.employee.emP_LASTNAME.trim().replace(' ', '') + this.employee.emP_FIRSTNAME.trim().replace(' ', '') + this.employee.emP_CODE).toLowerCase();
    }
    this.user.usE_PASSWORD = this.employee.emP_DATEOFBIRTH.toString().split('-').reverse().join('/');
    $("#field_emp_10").val(this.employee.emP_DATEOFBIRTH.toString().split('-').reverse().join('/'));
  }

  removeAccents(str: string) {
    return str.normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .replace(/đ/g, 'd').replace(/Đ/g, 'D');
  }

  showPassword(id: number) {
    if ($("#field_emp_" + id).attr('type') == 'password') {
      $("#field_emp_" + id).prop('type', 'text');
      $("#button_field_" + id).prop('title', 'Ẩn mật khẩu');
      $("#icon_field_" + id).removeClass('fa-eye').addClass('fa-eye-slash');
    }
    else {
      $("#field_emp_" + id).prop('type', 'password');
      $("#button_field_" + id).prop('title', 'Hiện mật khẩu');
      $("#icon_field_" + id).removeClass('fa-eye-slash').addClass('fa-eye');
    }
  }

  clearField() {
    for (let i = 1; i <= 16; i++) {
      if (i == 13) {
        $("#field_emp_" + i + ">.ng-select-container").removeAttr("style");
        $("#valid_mes_emp_" + i).html("");
      }
      else if (i == 14) {
        $("#field_emp_" + i + ">.ng-select-container").removeAttr("style");
        $("#valid_mes_emp_" + i).html("");
      }
      else {
        $("#field_emp_" + i).removeClass('is-invalid is-valid');
        $("#valid_mes_emp_" + i).html("");
      }
    }
    this.subjects = null;
    this.file = null;
    this.fileIsExist = null;
    $("#label_field_emp_15").html("Tải tệp lên");
    this.list_subject = [];
    this.list_permission = [];
    this.url = "";
    $("#field_emp_10").val("");

    $("#field_emp_9").prop('type', 'password');
    $("#button_field_9").prop('title', 'Hiện mật khẩu');
    $("#icon_field_9").removeClass('fa-eye-slash').addClass('fa-eye');
    $("#field_emp_10").prop('type', 'password');
    $("#button_field_10").prop('title', 'Hiện mật khẩu');
    $("#icon_field_10").removeClass('fa-eye-slash').addClass('fa-eye');
  }

  checkField() {
    for (let i = 1; i <= 16; i++) {
      if (i == 13) {
        $("#field_emp_" + i + ">.ng-select-container").removeAttr("style").attr("style", "border-color:#28a745;");
        $("#valid_mes_emp_" + i).html("");
      }
      else if (i == 14) {
        $("#field_emp_" + i + ">.ng-select-container").removeAttr("style").attr("style", "border-color:#28a745;");
        $("#valid_mes_emp_" + i).html("");
      }
      else {
        $("#field_emp_" + i).removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_emp_" + i).html("");
      }
    }

    this.file = null;
    $("#field_emp_9").prop('type', 'password');
    $("#button_field_9").prop('title', 'Hiện mật khẩu');
    $("#icon_field_9").removeClass('fa-eye-slash').addClass('fa-eye');
    $("#field_emp_10").prop('type', 'password');
    $("#button_field_10").prop('title', 'Hiện mật khẩu');
    $("#icon_field_10").removeClass('fa-eye-slash').addClass('fa-eye');
  }

  validate_emp_firstname() {
    if ($("#field_emp_1").val() == '') {
      $("#field_emp_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_1").html("Vui lòng nhập họ nhân viên!");
      return false;
    }
    else {
      let isHaveNumber = false;
      let arr = $("#field_emp_1").val().toString().split('');
      for (let i = 0; i < arr.length; i++) {
        if ($.isNumeric(arr[i])) {
          isHaveNumber = true;
          break;
        }
      }
      if (isHaveNumber == true) {
        $("#field_emp_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_emp_1").html("Vui lòng nhập đúng định dạng họ tên!");
        return false;
      }
      else {
        $("#field_emp_1").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_emp_1").html("");
        return true;
      }
    }
  }

  validate_emp_lastname() {
    if ($("#field_emp_2").val() == '') {
      $("#field_emp_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_2").html("Vui lòng nhập tên nhân viên!");
      return false;
    }
    else {
      let isHaveNumber = false;
      let arr = $("#field_emp_2").val().toString().split('');
      for (let i = 0; i < arr.length; i++) {
        if ($.isNumeric(arr[i])) {
          isHaveNumber = true;
          break;
        }
      }
      if (isHaveNumber == true) {
        $("#field_emp_2").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_emp_2").html("Vui lòng nhập đúng định dạng họ tên!");
        return false;
      }
      else {
        $("#field_emp_2").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_emp_2").html("");
        return true;
      }
    }
  }

  validate_emp_dateofbirth() {
    if ($("#field_emp_3").val() == '') {
      $("#field_emp_3").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_3").html("Vui lòng nhập ngày sinh!");
      return false;
    }
    else if (parseInt($("#field_emp_3").val().toString().split('-')[0]) > (new Date().getFullYear() - 21)) {
      $("#field_emp_3").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_3").html("Ngày sinh không hợp lệ, ít nhất 21 tuổi trở lên!");
      return false;
    }
    else {
      $("#field_emp_3").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_emp_3").html("");
      return true;
    }
  }

  validate_emp_phone() {
    if (this.validateSer.validate_phonenumber($("#field_emp_4").val().toString()) == '1') {
      $("#field_emp_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_4").removeClass('text-success').html("Vui lòng nhập số điện thoại!");
      return false;
    }
    else if (this.validateSer.validate_phonenumber($("#field_emp_4").val().toString()) == '2') {
      $("#field_emp_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_4").removeClass('text-success').html("Số điện thoại sai định dạng!");
      return false;
    }
    else if (this.students.find(x => this.validateSer.customizePhoneNumber(x.stU_PHONE) == this.validateSer.customizePhoneNumber($("#field_emp_4").val().toString())) != null || this.employees.find(x => this.validateSer.customizePhoneNumber(x.emP_PHONE) == this.validateSer.customizePhoneNumber($("#field_emp_4").val().toString()) && x.id != this.employee.id) != null) {
      $("#field_emp_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_4").removeClass('text-success').html("Số điện thoại đã được sử dụng!");
      return false;
    }
    else {
      $("#field_emp_4").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_emp_4").addClass('text-success').html("Số điện thoại " + this.validateSer.validate_phonenumber($("#field_emp_4").val().toString()));
      return true;
    }
  }

  validate_emp_identity() {
    if ($("#field_emp_16").val() == '') {
      $("#field_emp_16").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_16").removeClass('text-success').html("Vui lòng nhập CMND!");
      return false;
    }
    else if (!$.isNumeric($("#field_emp_16").val())) {
      $("#field_emp_16").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_16").removeClass('text-success').html("CMND sai định dạng!");
      return false;
    }
    else if ($("#field_emp_16").val().toString().length != 9 && $("#field_emp_16").val().toString().length != 12) {
      $("#field_emp_16").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_16").removeClass('text-success').html("CMND phải đủ 9 hoặc 12 số!");
      return false;
    }
    else if (this.students.find(x => x.stU_IDENTITY == $("#field_emp_16").val().toString()) != null || this.employees.find(x => x.emP_IDENTITY == $("#field_emp_16").val().toString() && x.id != this.employee.id) != null) {
      $("#field_emp_16").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_16").removeClass('text-success').html("CMND đã được sử dụng!");
      return false;
    }
    else {
      $("#field_emp_16").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_emp_16").html("");
      return true;
    }
  }

  validate_emp_email() {
    var regex = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if ($("#field_emp_6").val() == '') {
      $("#field_emp_6").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_6").html("Vui lòng nhập địa chỉ email!");
      return false;
    }
    else if (!regex.test($("#field_emp_6").val().toString())) {
      $("#field_emp_6").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_6").html("Email sai định dạng!");
      return false;
    }
    else if (this.students.find(x => x.stU_EMAIL == $("#field_emp_6").val()) != null || this.employees.find(x => x.emP_EMAIL == $("#field_emp_6").val() && x.id != this.employee.id) != null) {
      $("#field_emp_6").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_6").html("Email đã được sử dụng!");
      return false;
    }
    else {
      $("#field_emp_6").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_emp_6").html("");
      return true;
    }
  }

  validate_emp_address() {
    $("#field_emp_5").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_emp_5").html("");
    return true;
  }

  validate_emp_faculty() {
    if (this.employee.emP_FACID == 0) {
      $("#field_emp_12").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_12").html("Vui lòng chọn khoa!");
      return false;
    }
    else {
      $("#field_emp_12").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_emp_12").html("");
      return true;
    }
  }

  validate_emp_subject() {
    $("#field_emp_13>.ng-select-container").removeAttr("style").attr("style", "border-color:#28a745;");
    $("#valid_mes_emp_13").html("");
    return true;
  }

  validate_emp_code() {
    if ($("#field_emp_7").val() == '') {
      $("#field_emp_7").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_7").html("Vui lòng nhập mã nhân viên!");
      return false;
    }
    else {
      let employeeCode = $("#field_emp_7").val().toString().trim();
      let isExist = false;
      for (let i = 0; i < this.employees.length; i++) {
        if (employeeCode == this.employees[i].emP_CODE && this.employee.id != this.employees[i].id) {
          isExist = true;
          break;
        }
      }

      if (isExist == true) {
        $("#field_emp_7").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_emp_7").html("Mã nhân viên đã tồn tại!");
        return false;
      }
      else {
        $("#field_emp_7").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_emp_7").html("");
        return true;
      }
    }
  }

  validate_emp_username() {
    if ($("#field_emp_8").val() == '') {
      $("#field_emp_8").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_8").html("Vui lòng nhập tên tài khoản!");
      return false;
    }
    else if ($("#field_emp_8").val().toString().length < this.lengthUsername.seT_VALUE) {
      $("#field_emp_8").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_8").html("Tên tài khoản bắt buộc từ " + this.lengthUsername.seT_VALUE + " ký tự!");
      return false;
    }
    else {
      $("#field_emp_8").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_emp_8").html("");
      return true;
    }
  }

  validate_emp_password() {
    if ($("#field_emp_9").val() == '') {
      $("#field_emp_9").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_9").html("Vui lòng nhập mật khẩu!");
      return false;
    }
    else if ($("#field_emp_9").val().toString().length < this.lengthPassword.seT_VALUE) {
      $("#field_emp_9").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_9").html("Mật khẩu bắt buộc từ " + this.lengthPassword.seT_VALUE + " ký tự!");
      return false;
    }
    else {
      $("#field_emp_9").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_emp_9").html("");
      return true;
    }
  }

  validate_emp_confirmpassword() {
    if ($("#field_emp_10").val() == '') {
      $("#field_emp_10").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_10").html("Vui lòng nhập xác nhận mật khẩu!");
      return false;
    }
    else if ($("#field_emp_10").val() != $("#field_emp_9").val()) {
      $("#field_emp_10").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_emp_10").html("Xác nhận mật khẩu không khớp!");
      return false;
    }
    else {
      $("#field_emp_10").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_emp_10").html("");
      return true;
    }
  }

  validate_emp_status() {
    $("#field_emp_11").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_emp_11").html("");
    return true;
  }

  validate_emp_permission() {
    if (this.list_permission[0] == null) {
      $("#field_emp_14>.ng-select-container").removeAttr("style").attr("style", "border-color:#dc3545;");
      $("#valid_mes_emp_14").html("Vui lòng chọn vai trò!");
      return false;
    }
    else {
      $("#field_emp_14>.ng-select-container").removeAttr("style").attr("style", "border-color:#28a745;");
      $("#valid_mes_emp_14").html("");
      return true;
    }
  }

  validate_emp_avatar() {
    $("#field_emp_15").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_emp_15").html("");
  }

  validate() {
    var check = true;
    if (this.validate_emp_firstname() == false)
      check = false;
    if (this.validate_emp_lastname() == false)
      check = false;
    if (this.validate_emp_dateofbirth() == false)
      check = false;
    if (this.validate_emp_phone() == false)
      check = false;
    if (this.validate_emp_email() == false)
      check = false;
    if (this.validate_emp_faculty() == false)
      check = false;
    if (this.validate_emp_code() == false)
      check = false;
    if (this.validate_emp_username() == false)
      check = false;
    if (this.validate_emp_password() == false)
      check = false;
    if (this.validate_emp_confirmpassword() == false)
      check = false;
    if (this.validate_emp_permission() == false)
      check = false;
    if (this.validate_emp_identity() == false)
      check = false;

    this.validate_emp_address();
    this.validate_emp_subject();
    this.validate_emp_avatar();
    //validate all
    if (check == false) {
      $("#valid_emp_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_emp_" + id).is(":visible")) {
      $("#buttonicon_emp_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_emp_" + id).slideToggle();
    }
    else {
      $("#buttonicon_emp_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_emp_" + id).slideToggle();
    }
  }

  ToggleGroupMulti(id: number) {
    if ($("#detailToggle_emp_mul_" + id).is(":visible")) {
      $("#buttonicon_emp_mul_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_emp_mul_" + id).slideToggle();
    }
    else {
      $("#buttonicon_emp_mul_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_emp_mul_" + id).slideToggle();
    }
  }

  openModal(id: number = 0) {
    this.unlockModal("addupdate");
    if (id > 0) { // update

      this.employeeSer.get(id).subscribe(res => {
        this.employee = res.data;
        // console.log(res.data);
        this.employee.emP_DATEOFBIRTH = this.employee.emP_DATEOFBIRTH.substring(0, 10);
        $("#field_emp_10").val(res.data.users.usE_PASSWORD);
        this.user = res.data.users;
        this.url = this.employee.fileUrl;

        this.userPermissionSer.getListPermissionIdByUserId(res.data.users.id).subscribe(res1 => {
          this.list_permission = res1.data;
        });

        this.subjectSer.getListSubjectByFacultyId(res.data.emP_FACID).subscribe(res1 => {
          this.subjects = res1.data;
          this.employeeSubjectSer.getListSubjectIdByEmployeeId(res.data.id).subscribe(res2 => {
            this.list_subject = res2.data;
          });
        });

        if (res.data.emP_AVATAR != null) {
          $("#label_field_emp_15").html(
            res.data.emP_AVATAR.split('/')[3].length > 30 ?
              res.data.emP_AVATAR.split('/')[3].substring(0, 9) + "..." + res.data.emP_AVATAR.split('/')[3].substring(res.data.emP_AVATAR.split('/')[3].length - 9, res.data.emP_AVATAR.split('/')[3].length) :
              res.data.emP_AVATAR.split('/')[3]
          );
          this.fileIsExist = true;
        }
        else {
          $("#label_field_emp_15").html("Tải ảnh lên...");
          this.fileIsExist = null;
        }
      });

      this.checkField();
      $("#modal_title_emp").html("Sửa thông tin nhân viên");
      this.modal.show();
    }
    else { // insert
      if (this.autoGenCode.seT_ISACTIVE == true) {
        this.employee = {
          id: 0,
          emP_GENDER: true,
          emP_CODE: "XXXXX",
          emP_FACID: 0
        } as Employee;
        this.user = {
          id: 0,
          usE_STATUS: 1
        } as User;
      }
      else {
        this.employee = {
          id: 0,
          emP_GENDER: true,
          emP_FACID: 0
        } as Employee;
        this.user = {
          id: 0,
          usE_STATUS: 1
        } as User;
      }

      this.clearField();

      if (this.permissionId != null) {
        this.list_permission = [this.permission.id];
      }
      if (this.facultyId != null) {
        this.employee.emP_FACID = this.faculty.id;
        this.employee.emP_CODE = this.GenCode();
        this.subjectSer.getListSubjectByFacultyId(this.facultyId).subscribe(res => {
          this.subjects = res.data;
        });
      }

      $("#modal_title_emp").html("Thêm nhân viên");
      this.modal.show();
    }
  }

  save() {

    if (this.validate() == false) {
      return false;
    }

    this.lockModal("addupdate");

    this.employee.origin = window.location.origin;

    if (this.employee.id === 0) { // insert
      this.employee.emP_CREATEDBY = parseInt(this.cookieSer.get('Id'));
      this.user.usE_CREATEDBY = parseInt(this.cookieSer.get('Id'));
      this.employeeRequest = {
        employee: this.employee,
        user: this.user,
        user_permissions: this.list_permission,
        list_subject: this.list_subject
      } as EmployeeRequest;

      if (this.file != null) {
        const formData = new FormData();
        formData.append('file', this.file);
        this.fileSer.add("Users", formData).subscribe(res1 => {
          this.employee.emP_AVATAR = res1.data;
          this.employeeSer.add(this.employeeRequest).subscribe(res => {
            if (res.errorCode === 0) {
              if (this.permissionId != null) {
                this.employeeSer.getListByPermissionId(this.permissionId).subscribe(res2 => {
                  this.employees = res2.data;
                  this.rerender();
                });
              }
              else if (this.facultyId != null) {
                this.employeeSer.getListByFacultyId(this.facultyId).subscribe(res2 => {
                  this.employees = res2.data;
                  this.rerender();
                });
              }
              else {
                this.employeeSer.getAll().subscribe(res2 => {
                  this.employees = res2.data;
                  this.rerender();
                });
              }
              this.modal.hide();
              this.pnotify.success({
                title: 'Thông báo',
                text: 'Thêm nhân viên thành công!'
              });
            }
            else {
              this.modal.hide();
              this.pnotify.error({
                title: 'Thông báo',
                text: res.message
              });
            }
          });
        });
      }
      else {
        this.employeeSer.add(this.employeeRequest).subscribe(res => {
          if (res.errorCode === 0) {
            if (this.permissionId != null) {
              this.employeeSer.getListByPermissionId(this.permissionId).subscribe(res2 => {
                this.employees = res2.data;
                this.rerender();
              });
            }
            else if (this.facultyId != null) {
              this.employeeSer.getListByFacultyId(this.facultyId).subscribe(res2 => {
                this.employees = res2.data;
                this.rerender();
              });
            }
            else {
              this.employeeSer.getAll().subscribe(res2 => {
                this.employees = res2.data;
                this.rerender();
              });
            }
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Thêm nhân viên thành công!'
            });
          }
          else {
            this.modal.hide();
            this.pnotify.error({
              title: 'Thông báo',
              text: res.message
            });
          }
        });
      }
    }
    else { // update
      this.employee.emP_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.user.usE_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.employeeRequest = {
        employee: this.employee,
        user: this.user,
        list_subject: this.list_subject,
        user_permissions: this.list_permission
      } as EmployeeRequest;

      if (this.file != null) {
        const formData = new FormData();
        formData.append('file', this.file);
        if (this.employee.emP_AVATAR != null) {
          this.fileSer.put(this.employee.emP_AVATAR.split('/')[3], "Users", formData).subscribe(res => {
            this.employee.emP_AVATAR = res.data;
            this.employeeSer.put(this.employeeRequest).subscribe(res1 => {
              if (res1.errorCode === 0) {
                if (this.permissionId != null) {
                  this.employeeSer.getListByPermissionId(this.permissionId).subscribe(res2 => {
                    this.employees = res2.data;
                    this.rerender();
                  });
                }
                else if (this.facultyId != null) {
                  this.employeeSer.getListByFacultyId(this.facultyId).subscribe(res2 => {
                    this.employees = res2.data;
                    this.rerender();
                  });
                }
                else {
                  this.employeeSer.getAll().subscribe(res2 => {
                    this.employees = res2.data;
                    this.rerender();
                  });
                }
                this.modal.hide();
                this.pnotify.success({
                  title: 'Thông báo',
                  text: 'Sửa thông tin nhân viên thành công!'
                });
              }
              else {
                this.modal.hide();
                this.pnotify.error({
                  title: 'Thông báo',
                  text: res1.message
                });
              }
            });
          });
        }
        else {
          this.fileSer.add("Users", formData).subscribe(res => {
            this.employee.emP_AVATAR = res.data;
            this.employeeSer.put(this.employeeRequest).subscribe(res1 => {
              if (res1.errorCode === 0) {
                if (this.permissionId != null) {
                  this.employeeSer.getListByPermissionId(this.permissionId).subscribe(res2 => {
                    this.employees = res2.data;
                    this.rerender();
                  });
                }
                else if (this.facultyId != null) {
                  this.employeeSer.getListByFacultyId(this.facultyId).subscribe(res2 => {
                    this.employees = res2.data;
                    this.rerender();
                  });
                }
                else {
                  this.employeeSer.getAll().subscribe(res2 => {
                    this.employees = res2.data;
                    this.rerender();
                  });
                }
                this.modal.hide();
                this.pnotify.success({
                  title: 'Thông báo',
                  text: 'Sửa thông tin nhân viên thành công!'
                });
              }
              else {
                this.modal.hide();
                this.pnotify.error({
                  title: 'Thông báo',
                  text: res1.message
                });
              }
            });
          });
        }
      }
      else {
        if (this.fileIsExist == null && this.employee.emP_AVATAR != null) {
          this.fileSer.delete("Users", this.employee.emP_AVATAR.split('/')[3]).subscribe(res => {
            this.employee.emP_AVATAR = null;
            this.employeeSer.put(this.employeeRequest).subscribe(res1 => {
              if (res1.errorCode === 0) {
                if (this.permissionId != null) {
                  this.employeeSer.getListByPermissionId(this.permissionId).subscribe(res2 => {
                    this.employees = res2.data;
                    this.rerender();
                  });
                }
                else if (this.facultyId != null) {
                  this.employeeSer.getListByFacultyId(this.facultyId).subscribe(res2 => {
                    this.employees = res2.data;
                    this.rerender();
                  });
                }
                else {
                  this.employeeSer.getAll().subscribe(res2 => {
                    this.employees = res2.data;
                    this.rerender();
                  });
                }
                this.modal.hide();
                this.pnotify.success({
                  title: 'Thông báo',
                  text: 'Sửa thông tin nhân viên thành công!'
                });
              }
              else {
                this.modal.hide();
                this.pnotify.error({
                  title: 'Thông báo',
                  text: res1.message
                });
              }
            });
          });
        }
        else {
          this.employeeSer.put(this.employeeRequest).subscribe(res1 => {
            if (res1.errorCode === 0) {
              if (this.permissionId != null) {
                this.employeeSer.getListByPermissionId(this.permissionId).subscribe(res2 => {
                  this.employees = res2.data;
                  this.rerender();
                });
              }
              else if (this.facultyId != null) {
                this.employeeSer.getListByFacultyId(this.facultyId).subscribe(res2 => {
                  this.employees = res2.data;
                  this.rerender();
                });
              }
              else {
                this.employeeSer.getAll().subscribe(res2 => {
                  this.employees = res2.data;
                  this.rerender();
                });
              }
              this.modal.hide();
              this.pnotify.success({
                title: 'Thông báo',
                text: 'Sửa thông tin nhân viên thành công!'
              });
            }
            else {
              this.modal.hide();
              this.pnotify.error({
                title: 'Thông báo',
                text: res1.message
              });
            }
          });
        }
      }
    }
  }

  openModalDelete(id: number) {
    this.unlockModal("delete");
    this.employeeSer.get(id).subscribe(res => {
      this.employee = res.data;
      // console.log(res.data);
      this.modalDelete.show();
    });
  }

  delete() {
    this.lockModal("delete");
    if (this.employee.id != 0) {
      this.employeeSer.delete(this.employee.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          if (this.permissionId != null) {
            this.employeeSer.getListByPermissionId(this.permissionId).subscribe(res1 => {
              this.employees = res1.data;
              this.rerender();
            });
          }
          else if (this.facultyId != null) {
            this.employeeSer.getListByFacultyId(this.facultyId).subscribe(res1 => {
              this.employees = res1.data;
              this.rerender();
            });
          }
          else {
            this.employeeSer.getAll().subscribe(res1 => {
              this.employees = res1.data;
              this.rerender();
            });
          }
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Xóa nhân viên thành công!'
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
      });
    }
  }

  openModalDeleteMul() {
    this.unlockModal("multidel");
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      if (dtElement.dtInstance != null) {
        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == "tb_main") {
            let arr = [];
            dtInstance.rows({ selected: true }).every(function () {
              if (this.data()[1] != null) {
                arr.push(this.data()[1]);
              }
            });
            if (arr.length == 0) {
              this.isListItemNull = true;
              this.modalDeletMul.show();
            }
            else {
              this.isListItemNull = false;
              this.employeeSer.getListEmployeeMultiDel(arr).subscribe(res => {
                this.employeesMultiDel = res.data;
                this.modalDeletMul.show();
              });
            }
          }
        });
      }
    });
  }

  deleteMul() {
    this.lockModal("multidel");
    this.employeeSer.deleteMulti(this.employeesMultiDel, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
      if (res.errorCode === 0) {
        if (this.permissionId != null) {
          this.employeeSer.getListByPermissionId(this.permissionId).subscribe(res1 => {
            this.employees = res1.data;
            this.rerender();
          });
        }
        else if (this.facultyId != null) {
          this.employeeSer.getListByFacultyId(this.facultyId).subscribe(res1 => {
            this.employees = res1.data;
            this.rerender();
          });
        }
        else {
          this.employeeSer.getAll().subscribe(res1 => {
            this.employees = res1.data;
            this.rerender();
          });
        }
        if ($(".toggle-emp-all").closest('tr').hasClass('selected')) {
          $(".toggle-emp-all").closest('tr').toggleClass('selected');
        }
        this.modalDeletMul.hide();
        this.pnotify.success({
          title: 'Thông báo',
          text: "Xóa danh sách nhân viên thành công!"
        });
      }
      else {
        if ($(".toggle-emp-all").closest('tr').hasClass('selected')) {
          $(".toggle-emp-all").closest('tr').toggleClass('selected');
        }
        this.modalDeletMul.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message
        });
      }
    });
  }

  openModalDetail(id: number) {
    this.employeeSer.get(id).subscribe(res => {
      this.employee = res.data;
      this.modalDetail.show();
    });
  }

  lockModal(key: string) {
    $(".close_modal_" + key).prop("disabled", true);
    $("#save_modal_" + key).prop("disabled", true);
    $("#save_modal_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_" + key).prop("disabled", false);
    $("#save_modal_" + key).prop("disabled", false);
    $("#save_modal_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  openModalExport() {
    this.unlockModal("export");
    if (this.exportType == 1) {
      if (this.permissionId != null) {
        this.employeeSer.getListByPermissionId(this.permissionId).subscribe(res => {
          this.employeesExport = res.data;
          this.rerender1();
        });
      }
      else if (this.facultyId != null) {
        this.employeeSer.getListByFacultyId(this.facultyId).subscribe(res => {
          this.employeesExport = res.data;
          this.rerender1();
        });
      }
      else {
        this.employeeSer.getAll().subscribe(res => {
          this.employeesExport = res.data;
          this.rerender1();
        });
      }
    }
    else {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_main") {
              let arr = [];
              dtInstance.rows({ selected: true }).every(function () {
                if (this.data()[1] != null) {
                  arr.push(this.data()[1]);
                }
              });
              if (arr.length == 0) {
                this.isListItemNull = true;
              }
              else {
                this.isListItemNull = false;
                this.employeeSer.getListEmployeeMultiDel(arr).subscribe(res => {
                  this.employeesExport = res.data;
                  this.rerender1();
                });
              }
            }
          });
        }
      });
    }
    this.modalExport.show();
  }

  exportExcel() {
    this.lockModal("export");
    console.log(this.employeesExport);
    this.employeeSer.exportExcelLinkDownload(this.employeesExport).subscribe(res => {
      if (res.errorCode === 0) {
        //download file
        //file saver getting cors error
        saveAs(res.data);
        // window.open can't get filename
        // var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        // var blob = new Blob([res.data], { type: contentType });
        // var url = window.URL.createObjectURL(blob);
        // window.open(url);
        // a tag child
        // var link = document.createElement('a');
        // if (typeof link.download === 'string') {
        //   document.body.appendChild(link); // Firefox requires the link to be in the body
        //   link.download = res.data.split('/')[res.data.split('/').length - 1];
        //   link.href = res.data;
        //   link.click();
        //   document.body.removeChild(link); // remove the link when done
        // } else {
        //   location.replace(res.data);
        // }

        if (this.permissionId != null) {
          this.employeeSer.getListByPermissionId(this.permissionId).subscribe(res1 => {
            this.employees = res1.data;
            this.rerender();
          });
        }
        else if (this.facultyId != null) {
          this.employeeSer.getListByFacultyId(this.facultyId).subscribe(res1 => {
            this.employees = res1.data;
            this.rerender();
          });
        }
        else {
          this.employeeSer.getAll().subscribe(res1 => {
            this.employees = res1.data;
            this.rerender();
          });
        }

        if ($(".toggle-emp-all").closest('tr').hasClass('selected')) {
          $(".toggle-emp-all").closest('tr').toggleClass('selected');
        }

        this.modalExport.hide();
        this.pnotify.success({
          title: 'Thông báo',
          text: "Xuất excel danh sách nhân viên thành công!"
        });
      }
    });
  }
}