import { Component, OnInit, ViewChild } from '@angular/core';
import { Faculty, FacultyService } from '../../services/faculty.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'ngx-bootstrap/modal';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { DomSanitizer } from '@angular/platform-browser';
import { SettingService, Setting } from '../../services/setting.service';
import { ActivatedRoute } from '@angular/router';
import { SignalrService } from '../../services/signalr.service';
import { ValidateService } from '../../services/validate.service';
import { RoleService } from '../../services/role.service';
import { UserPermissionService } from '../../services/user-permission.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-faculty',
  templateUrl: './faculty.component.html',
  styleUrls: ['./faculty.component.css']
})
export class FacultyComponent implements OnInit {

  facid: number;
  faculty_Id: number;

  pnotify = undefined;
  faculties: [Faculty];
  facultiesMultiDel: [Faculty];
  faculty: Faculty = {} as Faculty;
  facultyConstraint: Faculty;

  isListItemNull: boolean = true;
  autoGenCode: Setting = {} as Setting;
  tabControl: number = 1;

  fac_add: boolean = true;
  fac_upd: boolean = true;
  fac_del: boolean = true;
  fac_con: boolean = true;
  fac_ref_emp: boolean = true;
  fac_ref_bra: boolean = true;
  fac_ref_sub: boolean = true;

  editorConfig = {
    placeholder: 'Thông tin ngành...',
    language: 'vi'
  };
  public Editor = ClassicEditor;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild('modalDeletMul', { static: false }) modalDeletMul: ModalDirective;
  constructor(private pNotify: PnotifyService, private facultySer: FacultyService,
    private cookieSer: CookieService, private _DomSanitizer: DomSanitizer,
    private settingSer: SettingService, private route: ActivatedRoute,
    private signalRSer: SignalrService, private validateSer: ValidateService,
    private roleSer: RoleService, private userPermissionSer: UserPermissionService, 
    private userSer: UserService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.dtOptions = {
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      order: [[1, 'asc']]
    };
    this.facultySer.getAll().subscribe(res => {
      this.faculties = res.data;
      // console.log(res.data);
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
    this.settingSer.get(3).subscribe(res => {
      this.autoGenCode = res.data;
    });
    //permission
    this.roleSer.get(31).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'fac_add':
                this.fac_add = false;
                break;
              case 'fac_upd':
                this.fac_upd = false;
                break;
              case 'fac_del':
                this.fac_del = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    this.roleSer.get(88).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        let count = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
          }
        }
        if (!(count > 0)) {
          this.fac_ref_emp = false;
        }
      });
    });
    this.roleSer.get(44).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        let count = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
          }
        }
        if (!(count > 0)) {
          this.fac_ref_bra = false;
        }
      });
    });
    this.roleSer.get(6).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        let count = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
          }
        }
        if (!(count > 0)) {
          this.fac_ref_sub = false;
        }
      });
    });
    this.roleSer.get(105).subscribe(res => {
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        let count = 0;
        let countNoConstraint = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == false) != null) {
              countNoConstraint++;
            }
          }
        }
        if (count > 0 && countNoConstraint == 0) {
          this.fac_con = false;
          this.userSer.get(Number(this.cookieSer.get('Id'))).subscribe(res2 => {
            this.facultySer.get(res2.data.employees[0].emP_FACID).subscribe(res3 => {
              this.facultyConstraint = res3.data;
            });
          });
        }
        else if (countNoConstraint > 0) {
          this.fac_con = true;
        }
      })
    });
    //realtime
    this.signalRSer.faculty.subscribe((res) => {
      if (res === true) {
        this.facultySer.getAll().subscribe(res1 => {
          this.faculties = res1.data;
          this.rerender();
        });
      }
    });
    this.signalRSer.setting.subscribe((res1) => {
      if (res1 === true) {
        this.settingSer.get(3).subscribe(res => {
          this.autoGenCode = res.data;
        });
      }
    });
  }

  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.facid = params['id'];
      if (this.facid != null) {
        // console.log(this.braid);
        this.openModalDetail(this.facid);
      }
    });
  }

  Select_Clear_All() {
    $(".toggle-fac-all").closest('tr').toggleClass('selected');
    if ($(".toggle-fac-all").closest('tr').hasClass('selected')) {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.select();
        });
      });
    }
    else {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.deselect();
        });
      });
    }
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
  }

  GenCode() {
    let code = "";
    let count = 1;
    do {
      code = "";
      if ((this.faculties.length + 1) < 10) {
        code += "0" + (this.faculties.length + count);
      }
      else {
        code += (this.faculties.length + count);
      }
      count++;
    } while (this.faculties.find(x => x.faC_CODE == code) != null);
    return code;
  }

  clearField() {
    for (let i = 1; i <= 7; i++) {
      if (i == 7)
        $("#field_fac_7>.ck-editor").removeAttr("style");
      else {
        $("#field_fac_" + i).removeClass('is-invalid is-valid');
        $("#valid_mes_fac_" + i).html("");
      }
    }
  }

  checkField() {
    for (let i = 1; i <= 7; i++) {
      if (i == 7)
        $("#field_fac_7>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
      else {
        $("#field_fac_" + i).removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_fac_" + i).html("");
      }
    }
  }

  validate_fac_name() {
    if ($("#field_fac_1").val() == '') {
      $("#field_fac_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_fac_1").html("Vui lòng nhập tên khoa!");
      return false;
    }
    else {
      let facultyName = $("#field_fac_1").val().toString().trim().toLowerCase();
      let isExist = false;
      for (let i = 0; i < this.faculties.length; i++) {
        if (facultyName == this.faculties[i].faC_NAME.toLowerCase() && this.faculty.id != this.faculties[i].id) {
          isExist = true;
          break;
        }
      }

      if (isExist == true) {
        $("#field_fac_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_fac_1").html("Tên khoa đã tồn tại!");
        return false;
      }
      else {
        $("#field_fac_1").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_fac_1").html("");
        return true;
      }
    }
  }

  validate_fac_code() {
    if ($("#field_fac_2").val() == '') {
      $("#field_fac_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_fac_2").html("Vui lòng nhập mã khoa!");
      return false;
    }
    else {
      let facultyCode = $("#field_fac_2").val().toString().trim();
      let isExist = false;
      for (let i = 0; i < this.faculties.length; i++) {
        if (facultyCode == this.faculties[i].faC_CODE && this.faculty.id != this.faculties[i].id) {
          isExist = true;
          break;
        }
      }

      if (isExist == true) {
        $("#field_fac_2").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_fac_2").html("Mã khoa đã tồn tại!");
        return false;
      }
      else {
        $("#field_fac_2").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_fac_2").html("");
        return true;
      }
    }
  }

  validate_fac_address() {
    $("#field_fac_3").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_fac_3").html("");
  }

  validate_fac_email() {
    var regex = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if ($("#field_fac_5").val() != '' && $("#field_fac_5").val() != null) {
      if (!regex.test($("#field_fac_5").val().toString())) {
        $("#field_fac_5").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_fac_5").html("Email sai định dạng!");
        return false;
      }
      else if (this.faculties.find(x => x.faC_EMAIL == $("#field_fac_5").val() && x.id != this.faculty.id)) {
        $("#field_fac_5").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_fac_5").html("Email đã được sử dụng!");
        return false;
      }
      else {
        $("#field_fac_5").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_fac_5").html("");
        return true;
      }
    }
    else {
      $("#field_fac_5").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_fac_5").html("");
      return true;
    }
  }

  validate_fac_website() {
    $("#field_fac_6").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_fac_6").html("");
  }

  validate_fac_information() {
    $("#field_fac_7>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
    $("#valid_mes_fac_7").html("");
  }

  validate_fac_phone() {
    if (this.validateSer.validate_phonenumber($("#field_fac_4").val().toString()) == '1') {
      $("#field_fac_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_fac_4").removeClass('text-success').html("Vui lòng nhập số điện thoại!");
      return false;
    }
    else if (this.validateSer.validate_phonenumber($("#field_fac_4").val().toString()) == '2') {
      $("#field_fac_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_fac_4").removeClass('text-success').html("Số điện thoại sai định dạng!");
      return false;
    }
    else if (this.faculties.find(x => this.validateSer.customizePhoneNumber(x.faC_PHONE) == this.validateSer.customizePhoneNumber($("#field_fac_4").val().toString()) && x.id != this.faculty.id) != null) {
      $("#field_fac_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_fac_4").removeClass('text-success').html("Số điện thoại đã được sử dụng!");
      return false;
    }
    else {
      $("#field_fac_4").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_fac_4").addClass('text-success').html("Số điện thoại " + this.validateSer.validate_phonenumber($("#field_fac_4").val().toString()));
      return true;
    }
  }

  validate() {
    var check = true;
    if (this.validate_fac_name() == false)
      check = false;
    if (this.validate_fac_code() == false)
      check = false;
    if (this.validate_fac_phone() == false)
      check = false;

    this.validate_fac_address();
    this.validate_fac_email();
    this.validate_fac_website();
    this.validate_fac_information();
    //validate all
    if (check == false) {
      $("#valid_fac_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_fac_" + id).is(":visible")) {
      $("#buttonicon_fac_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_fac_" + id).slideToggle();
    }
    else {
      $("#buttonicon_fac_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_fac_" + id).slideToggle();
    }
  }

  ToggleGroupMulti(id: number) {
    if ($("#detailToggle_fac_mul_" + id).is(":visible")) {
      $("#buttonicon_fac_mul_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_fac_mul_" + id).slideToggle();
    }
    else {
      $("#buttonicon_fac_mul_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_fac_mul_" + id).slideToggle();
    }
  }

  openModal(id: number = 0) {
    this.unlockModal("addupd");
    if (id > 0) { // update
      this.checkField();

      this.facultySer.get(id).subscribe(res => {
        this.faculty = res.data;
      });

      $("#modal_title_fac").html("Sửa khoa");
      this.modal.show();
    }
    else { // insert
      if (this.autoGenCode.seT_ISACTIVE == true) {
        this.faculty = {
          id: 0,
          faC_CODE: this.GenCode(),
          faC_STATUS: 1,
          faC_INFORMATION: "",
        } as Faculty
      }
      else {
        this.faculty = {
          id: 0,
          faC_STATUS: 1,
          faC_INFORMATION: "",
        } as Faculty
      }

      this.clearField();
      $("#modal_title_fac").html("Thêm khoa");
      this.modal.show();
    }
  }

  save() {

    if (this.validate() == false) {
      return false;
    }

    this.lockModal("addupd");
    if (this.faculty.id === 0) { // insert
      this.faculty.faC_CREATEDBY = parseInt(this.cookieSer.get('Id'));

      this.facultySer.add(this.faculty).subscribe(res => {
        if (res.errorCode === 0) {
          this.facultySer.getAll().subscribe(res1 => {
            this.faculties = res1.data;
            this.rerender();
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Thêm khoa thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
    else { // update
      this.faculty.faC_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.facultySer.put(this.faculty).subscribe(res => {
        if (res.errorCode === 0) {
          this.facultySer.getAll().subscribe(res1 => {
            this.faculties = res1.data;
            this.rerender();
            this.modal.hide();
            if (this.modalDetail.isShown) {
              this.openModalDetail(res.data.id);
            }
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Sửa khoa thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
  }
  openModalDelete(id: number) {
    this.unlockModal("del");
    this.facultySer.get(id).subscribe(res => {
      this.faculty = res.data;
      this.modalDelete.show();
    });
  }
  delete() {
    this.lockModal("del");
    if (this.faculty.id != 0) {
      this.facultySer.delete(this.faculty.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          this.facultySer.getAll().subscribe(res1 => {
            this.faculties = res1.data;
            this.rerender();
          });
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Xóa khoa thành công!'
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
        this.modalDetail.hide();
      });
    }
  }
  openModalDeleteMul() {
    this.unlockModal("muldel");
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      let arr = [];
      dtInstance.rows({ selected: true }).every(function () {
        if (this.data()[1] != null) {
          arr.push(this.data()[1]);
        }
      });
      if (arr.length == 0) {
        this.isListItemNull = true;
        this.modalDeletMul.show();
      }
      else {
        this.isListItemNull = false;
        this.facultySer.getListFacultyMultiDel(arr).subscribe(res => {
          this.facultiesMultiDel = res.data;
          // console.log(this.subjectsMultiDel);
          this.modalDeletMul.show();
        });
      }
    });
  }
  deleteMul() {
    this.lockModal("muldel");
    this.facultySer.deleteMulti(this.facultiesMultiDel, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
      if (res.errorCode === 0) {
        this.facultySer.getAll().subscribe(res1 => {
          this.faculties = res1.data;
          this.rerender();
          if ($(".toggle-fac-all").closest('tr').hasClass('selected')) {
            $(".toggle-fac-all").closest('tr').toggleClass('selected');
          }
          this.modalDeletMul.hide();
          this.pnotify.success({
            title: 'Thông báo',
            text: "Xóa danh sách khoa thành công!"
          });
        });
      }
      else {
        if ($(".toggle-fac-all").closest('tr').hasClass('selected')) {
          $(".toggle-fac-all").closest('tr').toggleClass('selected');
        }
        this.modalDeletMul.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message
        });
      }
    });
  }

  openModalDetail(id: number) {
    this.facultySer.get(id).subscribe(res => {
      this.faculty = res.data;
      this.faculty_Id = res.data.id;
      this.modalDetail.show();
    });
  }

  resetDetailModal() {
    this.faculty_Id = null;
    //reset tab
    for (let i = 1; i <= 4; i++) {
      if (i == 1) {
        $("#tab_fac_" + i).addClass("active");
        $("#content_fac_" + i).addClass("active");
      }
      else {
        $("#tab_fac_" + i).removeClass("active");
        $("#content_fac_" + i).removeClass("active").addClass("fade");
      }
    }
    this.loadTab(1);
  }

  lockModal(key: string) {
    $(".close_modal_fac_" + key).prop("disabled", true);
    $("#save_modal_fac_" + key).prop("disabled", true);
    $("#save_modal_fac_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_fac_" + key).prop("disabled", false);
    $("#save_modal_fac_" + key).prop("disabled", false);
    $("#save_modal_fac_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  loadTab(key: number) {
    this.tabControl = key;
  }
}
