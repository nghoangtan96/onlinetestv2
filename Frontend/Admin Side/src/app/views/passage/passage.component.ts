import { Component, OnInit, Input, ViewChild } from '@angular/core';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import '@ckeditor/ckeditor5-build-classic/build/translations/vi';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { PnotifyService } from '../../services/pnotify.service';
import { PartService, Part } from '../../services/part.service';
import { CookieService } from 'ngx-cookie-service';
import { PassageService, Passage } from '../../services/passage.service';
import { AnswertypeService, AnswerType } from '../../services/answertype.service';
import { Subjectt, SubjectService } from '../../services/subject.service';
import { FileService } from '../../services/file.service';
import { Setting, SettingService } from '../../services/setting.service';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { SignalrService } from '../../services/signalr.service';
import { UserPermissionService } from '../../services/user-permission.service';
import { RoleService } from '../../services/role.service';
import { UserService } from '../../services/user.service';
import { EmployeeSubjectService } from '../../services/employee-subject.service';

@Component({
  selector: 'app-passage',
  templateUrl: './passage.component.html',
  styleUrls: ['./passage.component.css']
})
export class PassageComponent implements OnInit {

  @Input() public partId: number; //parse parameter through selector
  pasid: number
  sub_id: number;

  pnotify = undefined;
  passages: [Passage];
  passagesMultiDel: [Passage];
  passage: Passage = {} as Passage;
  parts: [Part];
  part: Part = {} as Part;
  answertypes: [AnswerType];
  subjects: [Subjectt];
  file: File = null;
  fileIsExist: boolean;
  passage_Id: number;
  isListItemNull: boolean = true;
  subjectsConstraint: [number];

  tabControl: number = 1;
  mediaType: string;
  optionKey = ['A', 'B', 'C', 'D', 'E', 'F'];
  videoExtension = ['mp4', 'webm', 'ogg'];
  audioExtension = ['mpeg', 'wav', 'm4a', 'mp3'];

  pas_add: boolean = true;
  pas_upd: boolean = true;
  pas_del: boolean = true;
  pas_con: boolean = true;
  pas_ref: boolean = true;

  editorConfig = {
    placeholder: 'Nội dung đoạn văn...',
    language: 'vi'
  };
  editorViewConfig = {
    toolbar: [],
    language: 'vi'
  };
  public Editor = ClassicEditor;
  public EditorView = ClassicEditor;
  difficultDisplay: Setting = {} as Setting;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild('modalDeletMul', { static: false }) modalDeletMul: ModalDirective;
  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;
  @ViewChild('modalViewMode', { static: false }) modalViewMode: ModalDirective;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  constructor(private pNotify: PnotifyService, private partSer: PartService,
    private cookieSer: CookieService, private passageSer: PassageService,
    private answerTypeSer: AnswertypeService, private subjectSer: SubjectService,
    private fileSer: FileService, private settingSer: SettingService,
    private route: ActivatedRoute, private _DomSanitizer: DomSanitizer,
    private signalRSer: SignalrService, private roleSer: RoleService,
    private userPermissionSer: UserPermissionService, private userSer: UserService, 
    private employeeSubjectSer: EmployeeSubjectService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.dtOptions = {
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      order: [[1, 'asc']]
    };
    if (this.partId != null) {
      this.passageSer.getListByPartId(this.partId).subscribe(res => {
        this.passages = res.data;
        this.dtTrigger.next();
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.columns().every(function () {
            const that = this;
            $('input', this.footer()).on('keyup change', function () {
              if (that.search() !== this['value']) {
                that
                  .search(this['value'])
                  .draw();
              }
            });
          });
        });
      });
      this.partSer.get(this.partId).subscribe(res => {
        this.part = res.data;
      });
    }
    else {
      this.passageSer.getAll().subscribe(res => {
        this.passages = res.data;
        this.dtTrigger.next();
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.columns().every(function () {
            const that = this;
            $('input', this.footer()).on('keyup change', function () {
              if (that.search() !== this['value']) {
                that
                  .search(this['value'])
                  .draw();
              }
            });
          });
        });
      });
    }
    this.answerTypeSer.getAll().subscribe(res => {
      this.answertypes = res.data;
    });
    this.subjectSer.getAll().subscribe(res => {
      this.subjects = res.data;
    });
    this.settingSer.get(9).subscribe(res => {
      this.difficultDisplay = res.data;
      // console.log(res.data);
    });
    //permission
    this.roleSer.get(21).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'pas_add':
                this.pas_add = false;
                break;
              case 'pas_upd':
                this.pas_upd = false;
                break;
              case 'pas_del':
                this.pas_del = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    this.roleSer.get(26).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        let count = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
          }
        }
        if (!(count > 0)) {
          this.pas_ref = false;
        }
      });
    });
    this.roleSer.get(105).subscribe(res => {
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        let count = 0;
        let countNoConstraint = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == false) != null) {
              countNoConstraint++;
            }
          }
        }
        if (count > 0 && countNoConstraint == 0) {
          this.pas_con = false;
          this.userSer.get(Number(this.cookieSer.get('Id'))).subscribe(res2 => {
            this.employeeSubjectSer.getListSubjectIdByEmployeeId(res2.data.employees[0].id).subscribe(res3 => {
              this.subjectsConstraint = res3.data;
            });
          });
        }
        else if (countNoConstraint > 0) {
          this.pas_con = true;
        }
      })
    });
    //realtime
    this.signalRSer.passage.subscribe((res) => {
      if (res === true) {
        if (this.partId != null) {
          this.passageSer.getListByPartId(this.partId).subscribe(res1 => {
            this.passages = res1.data;
            this.rerender();
          });
        }
        else {
          this.passageSer.getAll().subscribe(res1 => {
            this.passages = res1.data;
            this.rerender();
          });
        }
      }
    });
    this.signalRSer.answertype.subscribe((res) => {
      if (res === true) {
        this.answerTypeSer.getAll().subscribe(res1 => {
          this.answertypes = res1.data;
        });
      }
    });
    this.signalRSer.subject.subscribe((res) => {
      if (res === true) {
        this.subjectSer.getAll().subscribe(res1 => {
          this.subjects = res1.data;
        });
      }
    });
    this.signalRSer.setting.subscribe((res) => {
      if (res === true) {
        this.settingSer.get(9).subscribe(res1 => {
          this.difficultDisplay = res1.data;
        });
      }
    });
  }

  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.pasid = params['id'];
      if (this.pasid != null) {
        this.openModalDetail(this.pasid);
      }
    });
  }

  onReadyCKEditor(editor) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
  }

  onSelectFile(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      this.file = event.target.files[0];

      // reader.onload = (event: any) => { // called once readAsDataURL is completed
      //   this.url = event.target.result;
      // }

      $("#label_field_pas_2").html(
        this.file.name.length > 30 ?
          this.file.name.substring(0, 9) + "..." + this.file.name.substring(this.file.name.length - 9, this.file.name.length) :
          this.file.name
      );
      this.fileIsExist = true;
    }
  }

  removeFile(event = null) {
    if (event != null) {
      event.preventDefault();
    }
    this.file = null;
    $("#field_pas_2").val("");
    this.fileIsExist = null;
    $("#label_field_pas_2").html("Tải tệp lên");
    this.validate_pas_media();
  }

  Select_Clear_All() {
    $(".toggle-pas-all").closest('tr').toggleClass('selected');
    if ($(".toggle-pas-all").closest('tr').hasClass('selected')) {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.select();
        });
      });
    }
    else {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.deselect();
        });
      });
    }
  }

  stopMedia(mode: number) {
    if (mode == 1) {
      $("#pas_media").trigger("pause");
      $("#pas_media").prop("currentTime", 0);
    }
    else {
      $("#pas_mediaview").trigger("pause");
      $("#pas_mediaview").prop("currentTime", 0);
    }
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
  }

  clearField() {
    for (let i = 1; i <= 7; i++) {
      // $("#field_ins_" + i).val("");
      if (i == 1) {
        $("#field_pas_1>.ck-editor").removeAttr("style");
        $("#valid_mes_pas_" + i).html("");
      }
      else {
        $("#field_pas_" + i).removeClass('is-invalid is-valid');
        $("#valid_mes_pas_" + i).html("");
      }
    }
    this.sub_id = null;
    this.file = null;
    this.fileIsExist = null;
    $("#label_field_pas_2").html("Tải tệp lên");
    $("#warning_mes_pas_6").html("");
  }

  checkField() {
    for (let i = 1; i <= 7; i++) {
      if (i == 1) {
        $("#field_pas_1>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
        $("#valid_mes_pas_" + i).html("");
      }
      else if (i == 3)
        continue;
      else {
        $("#field_pas_" + i).removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_pas_" + i).html("");
      }
    }
    this.file = null;
  }

  generatePartList() {
    this.partSer.getListBySubjectId(this.sub_id).subscribe(res => {
      this.parts = res.data;
      // console.log(res.data);
      if (res.data[0] == null) {
        this.passage.paS_PARID = 0;
      }
      else {
        this.passage.paS_PARID = res.data[0].id;
        this.CheckPartAllowShuffle();
      }
    });
  }

  CheckPartAllowShuffle() {
    if (this.passage.id == 0) {
      if (this.parts.find(x => x.id == this.passage.paS_PARID).paR_ISALLOWSHUFFLEQUESTION == true) {
        $("#field_pas_3").prop("disabled", false);
      }
      else {
        $("#field_pas_3").prop("disabled", true);
      }
      $("#field_pas_3").prop("checked", false);
    }
    else {
      if (this.parts.find(x => x.id == this.passage.paS_PARID).paR_ISALLOWSHUFFLEQUESTION == true) {
        $("#field_pas_3").prop("disabled", false);
      }
      else {
        $("#field_pas_3").prop("disabled", true);
      }
      if (this.passage.paS_ISSHUFFLE == false) {
        $("#field_pas_3").prop("checked", false);
      }
      else {
        $("#field_pas_3").prop("checked", true);
      }
    }
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_pas_" + id).is(":visible")) {
      $("#buttonicon_pas_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_pas_" + id).slideToggle();
    }
    else {
      $("#buttonicon_pas_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_pas_" + id).slideToggle();
    }
  }

  ToggleGroupMulti(id: number) {
    if ($("#detailToggle_pas_mul_" + id).is(":visible")) {
      $("#buttonicon_pas_mul_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_pas_mul_" + id).slideToggle();
    }
    else {
      $("#buttonicon_pas_mul_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_pas_mul_" + id).slideToggle();
    }
  }

  validate_pas_content() {
    if (this.passage.paS_CONTENT == '' && this.file == null && this.fileIsExist == null) {
      $("#field_pas_1>.ck-editor").removeAttr("style").attr("style", "border:1px solid #dc3545;");
      $("#valid_mes_pas_1").html("Vui lòng nhập nội dung hoặc tải tệp lên!");
      return false;
    }
    else {
      $("#field_pas_1>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
      $("#valid_mes_pas_1").html("");
      $("#field_pas_2").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_pas_2").html("");
      return true;
    }
  }

  validate_pas_media() {
    if (this.passage.paS_CONTENT == '' && this.file == null && this.fileIsExist == null) {
      $("#field_pas_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_pas_2").html("Vui lòng nhập nội dung hoặc tải tệp lên!");
      return false;
    }
    else {
      $("#field_pas_1>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
      $("#valid_mes_pas_1").html("");
      $("#field_pas_2").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_pas_2").html("");
      return true;
    }
  }

  validate_pas_status() {
    if (Number($("#field_pas_4").val()) == 0) {
      $("#field_pas_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_pas_4").html("Vui lòng chọn trạng thái!");
      return false;
    }
    else {
      $("#field_pas_4").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_pas_4").html("");
      return true;
    }
  }

  validate_pas_antyid() {
    if (Number($("#field_pas_5").val()) == 0) {
      $("#field_pas_5").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_pas_5").html("Vui lòng chọn loại câu hỏi!");
      return false;
    }
    else {
      $("#field_pas_5").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_pas_5").html("");
      return true;
    }
  }

  validate_pas_parid() {
    if (Number($("#field_pas_7").val()) == 0 || this.passage.paS_PARID == 0) {
      $("#field_pas_7").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_pas_7").html("Vui lòng chọn học phần!");
      $("#warning_mes_pas_6").html("<i class='fa fa-warning'></i> Chọn môn học để mở các học phần!")
      return false;
    }
    else {
      if (this.parts.find(x => x.id == this.passage.paS_PARID).countQuestion != 0) {
        $("#field_pas_7").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_pas_7").html("Học phần sủ dụng câu hỏi đơn, vui lòng xóa hết câu hỏi đơn nếu muốn tạo câu hỏi đoạn văn!");
        return false;
      }
      else {
        $("#field_pas_7").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_pas_7").html("");
        $("#warning_mes_pas_6").html("");
        return true;
      }
    }
  }

  validate_pas_subid() {
    $("#field_pas_6").removeClass('is-invalid').addClass('is-valid');
  }

  validate() {
    var check = true;
    if (this.validate_pas_content() == false)
      check = false;
    if (this.validate_pas_media() == false)
      check = false;
    if (this.validate_pas_status() == false)
      check = false;
    if (this.validate_pas_antyid() == false)
      check = false;
    if (this.partId == null) {
      if (this.validate_pas_parid() == false)
        check = false;
    }
    this.validate_pas_subid();

    //validate all
    if (check == false) {
      $("#valid_pas_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  openModal(id: number = 0) {
    this.unlockModal("addupd");
    if (id > 0) { // update

      this.passageSer.get(id).subscribe(res => {

        if (res.data.paS_MEDIA != null) {
          $("#label_field_pas_2").html(
            res.data.paS_MEDIA.split('/')[3].length > 30 ?
              res.data.paS_MEDIA.split('/')[3].substring(0, 9) + "..." + res.data.paS_MEDIA.split('/')[3].substring(res.data.paS_MEDIA.split('/')[3].length - 9, res.data.paS_MEDIA.split('/')[3].length) :
              res.data.paS_MEDIA.split('/')[3]
          );
          this.fileIsExist = true;
        }
        else {
          $("#label_field_pas_2").html("Tải tệp lên...");
          this.fileIsExist = null;
        }

        if (res.data.paS_ISSHUFFLE == false) {
          $("#field_pas_3").prop("checked", false);
        }
        else {
          $("#field_pas_3").prop("checked", true);
        }

        this.sub_id = res.data.parts.subjects.id;
        this.partSer.getListBySubjectId(res.data.parts.subjects.id).subscribe(res1 => {
          this.parts = res1.data;
          if (res1.data.find(x => x.id == res.data.paS_PARID).paR_ISALLOWSHUFFLEQUESTION == false) {
            $("#field_pas_3").prop("disabled", true);
          }
          else {
            $("#field_pas_3").prop("disabled", false);
          }
        });

        this.passage = res.data;
      });

      this.checkField();
      $("#modal_title_pas").html("Sửa đoạn văn");
      this.modal.show();
    }
    else { // insert

      if (this.partId == null) {
        this.passage = {
          id: 0,
          paS_CONTENT: "",
          paS_STATUS: 1,
          paS_ANTYID: 0,
          paS_PARID: 0,
          paS_ISSHUFFLE: false
        } as Passage
      }
      else {
        this.passage = {
          id: 0,
          paS_CONTENT: "",
          paS_STATUS: 1,
          paS_ANTYID: 0,
          paS_PARID: this.partId,
          paS_ISSHUFFLE: false
        } as Passage
        this.partSer.get(this.partId).subscribe(res => {
          if (res.data.paR_ISALLOWSHUFFLEQUESTION == true) {
            $("#field_pas_3").prop("disabled", false);
          }
          else {
            $("#field_pas_3").prop("disabled", true);
          }
        });
      }

      this.clearField();
      $("#modal_title_pas").html("Thêm đoạn văn");
      this.modal.show();
    }
  }

  save() {
    if (this.validate() == false) {
      return false;
    }
    this.lockModal("addupd")

    if (this.partId != null)
      this.passage.paS_PARID = this.partId;
    if ($("#field_pas_3").is(":checked")) {
      this.passage.paS_ISSHUFFLE = true;
    }
    else
      this.passage.paS_ISSHUFFLE = false;

    if (this.passage.id === 0) { // insert
      this.passage.paS_CREATEDBY = parseInt(this.cookieSer.get('Id'));
      if (this.file != null) {
        const formData = new FormData();
        formData.append('file', this.file);
        this.fileSer.add("Passage", formData).subscribe(res => {
          this.passage.paS_MEDIA = res.data;
          this.passageSer.add(this.passage).subscribe(res1 => {
            if (res1.errorCode === 0) {
              if (this.partId != null) {
                this.passageSer.getListByPartId(this.partId).subscribe(res2 => {
                  this.passages = res2.data;
                  this.rerender();
                  $("#que_tab").addClass("disabled");
                });
              }
              else {
                this.passageSer.getAll().subscribe(res2 => {
                  this.passages = res2.data;
                  this.rerender();
                });
              }
              this.modal.hide();
              this.pnotify.success({
                title: 'Thông báo',
                text: 'Thêm đoạn văn thành công!'
              });
            }
            else {
              this.modal.hide();
              this.pnotify.error({
                title: 'Thông báo',
                text: res1.message
              });
            }
          });
        });
      }
      else {
        this.passageSer.add(this.passage).subscribe(res => {
          if (res.errorCode === 0) {
            if (this.partId != null) {
              this.passageSer.getListByPartId(this.partId).subscribe(res1 => {
                this.passages = res1.data;
                this.rerender();
                $("#que_tab").addClass("disabled");
              });
            }
            else {
              this.passageSer.getAll().subscribe(res1 => {
                this.passages = res1.data;
                this.rerender();
              });
            }
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Thêm đoạn văn thành công!'
            });
          }
          else {
            this.modal.hide();
            this.pnotify.error({
              title: 'Thông báo',
              text: res.message
            });
          }
        });
      }
    }
    else { // update
      this.passage.paS_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      if (this.file != null) {
        if (this.passage.paS_MEDIA != null && this.passage.countTest == 0) {
          const formData = new FormData();
          formData.append('file', this.file);
          this.fileSer.put(this.passage.paS_MEDIA.split('/')[3], "Passage", formData).subscribe(res2 => {
            this.passage.paS_MEDIA = res2.data;
            this.passageSer.put(this.passage).subscribe(res => {
              if (res.errorCode === 0) {
                if (this.partId != null) {
                  this.passageSer.getListByPartId(this.partId).subscribe(res1 => {
                    this.passages = res1.data;
                    this.rerender();
                  });
                }
                else {
                  this.passageSer.getAll().subscribe(res1 => {
                    this.passages = res1.data;
                    this.rerender();
                  });
                }
                this.modal.hide();
                if (this.modalDetail.isShown) {
                  this.openModalDetail(res.data.id);
                }
                this.pnotify.success({
                  title: 'Thông báo',
                  text: 'Sửa đoạn văn thành công!'
                });
              }
              else {
                this.modal.hide();
                this.pnotify.error({
                  title: 'Thông báo',
                  text: res.message
                });
              }
            });
          });
        }
        else {
          const formData = new FormData();
          formData.append('file', this.file);
          this.fileSer.add("Passage", formData).subscribe(res2 => {
            this.passage.paS_MEDIA = res2.data;
            this.passageSer.put(this.passage).subscribe(res => {
              if (res.errorCode === 0) {
                if (this.partId != null) {
                  this.passageSer.getListByPartId(this.partId).subscribe(res1 => {
                    this.passages = res1.data;
                    this.rerender();
                  });
                }
                else {
                  this.passageSer.getAll().subscribe(res1 => {
                    this.passages = res1.data;
                    this.rerender();
                  });
                }
                this.modal.hide();
                if (this.modalDetail.isShown) {
                  this.openModalDetail(res.data.id);
                }
                this.pnotify.success({
                  title: 'Thông báo',
                  text: 'Sửa đoạn văn thành công!'
                });
              }
              else {
                this.modal.hide();
                this.pnotify.error({
                  title: 'Thông báo',
                  text: res.message
                });
              }
            });
          });
        }
      }
      else {
        if (this.passage.paS_MEDIA != null && this.fileIsExist == null) {
          if (this.passage.countTest == 0) {
            this.fileSer.delete('Passage', this.passage.paS_MEDIA.split('/')[3]).subscribe(res => {
              this.passage.paS_MEDIA = null;
              this.passageSer.put(this.passage).subscribe(res => {
                if (res.errorCode === 0) {
                  if (this.partId != null) {
                    this.passageSer.getListByPartId(this.partId).subscribe(res1 => {
                      this.passages = res1.data;
                      this.rerender();
                    });
                  }
                  else {
                    this.passageSer.getAll().subscribe(res1 => {
                      this.passages = res1.data;
                      this.rerender();
                    });
                  }
                  this.modal.hide();
                  if (this.modalDetail.isShown) {
                    this.openModalDetail(res.data.id);
                  }
                  this.pnotify.success({
                    title: 'Thông báo',
                    text: 'Sửa đoạn văn thành công!'
                  });
                }
                else {
                  this.modal.hide();
                  this.pnotify.error({
                    title: 'Thông báo',
                    text: res.message
                  });
                }
              });
            });
          }
          else {
            this.passage.paS_MEDIA = null;
            this.passageSer.put(this.passage).subscribe(res => {
              if (res.errorCode === 0) {
                if (this.partId != null) {
                  this.passageSer.getListByPartId(this.partId).subscribe(res1 => {
                    this.passages = res1.data;
                    this.rerender();
                  });
                }
                else {
                  this.passageSer.getAll().subscribe(res1 => {
                    this.passages = res1.data;
                    this.rerender();
                  });
                }
                this.modal.hide();
                if (this.modalDetail.isShown) {
                  this.openModalDetail(res.data.id);
                }
                this.pnotify.success({
                  title: 'Thông báo',
                  text: 'Sửa đoạn văn thành công!'
                });
              }
              else {
                this.modal.hide();
                this.pnotify.error({
                  title: 'Thông báo',
                  text: res.message
                });
              }
            });
          }
        }
        else {
          this.passageSer.put(this.passage).subscribe(res => {
            if (res.errorCode === 0) {
              if (this.partId != null) {
                this.passageSer.getListByPartId(this.partId).subscribe(res1 => {
                  this.passages = res1.data;
                  this.rerender();
                });
              }
              else {
                this.passageSer.getAll().subscribe(res1 => {
                  this.passages = res1.data;
                  this.rerender();
                });
              }
              this.modal.hide();
              if (this.modalDetail.isShown) {
                this.openModalDetail(res.data.id);
              }
              this.pnotify.success({
                title: 'Thông báo',
                text: 'Sửa đoạn văn thành công!'
              });
            }
            else {
              this.modal.hide();
              this.pnotify.error({
                title: 'Thông báo',
                text: res.message
              });
            }
          });
        }
      }
    }
  }

  openModalDelete(id: number) {
    this.unlockModal("del");
    this.passageSer.get(id).subscribe(res => {
      this.passage = res.data;
      // console.log(res.data);
      this.modalDelete.show();
    });
  }

  delete() {
    this.unlockModal("del");
    if (this.passage.id != 0) {
      this.passageSer.delete(this.passage.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          if (this.partId != null) {
            this.passageSer.getListByPartId(this.partId).subscribe(res1 => {
              this.passages = res1.data;
              this.rerender();
              if (res1.data[0] == null) {
                $("#que_tab").removeClass("disabled");
              }
            });
          } else {
            this.passageSer.getAll().subscribe(res1 => {
              this.passages = res1.data;
              this.rerender();
            });
          }
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Xóa đoạn văn thành công!'
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
        this.modalDetail.hide();
      });
    }
  }

  openModalDeleteMul() {
    this.unlockModal("muldel");
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      let arr = [];
      dtInstance.rows({ selected: true }).every(function () {
        if (this.data()[1] != null) {
          arr.push(this.data()[1]);
        }
      });
      if (arr.length == 0) {
        this.isListItemNull = true;
        this.modalDeletMul.show();
      }
      else {
        this.isListItemNull = false;
        this.passageSer.getListPassageMultiDel(arr).subscribe(res => {
          this.passagesMultiDel = res.data;
          // console.log(this.partMultiDel);
          this.modalDeletMul.show();
        });
      }
    });
  }

  deleteMul() {
    this.lockModal("muldel");
    this.passageSer.deleteMulti(this.passagesMultiDel, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
      if (res.errorCode === 0) {
        if (this.partId != null) {
          this.passageSer.getListByPartId(this.partId).subscribe(res1 => {
            this.passages = res1.data;
            this.rerender();
            if (res1.data[0] == null) {
              $("#que_tab").removeClass("disabled");
            }
          });
        } else {
          this.passageSer.getAll().subscribe(res1 => {
            this.passages = res1.data;
            this.rerender();
          });
        }
        if ($(".toggle-pas-all").closest('tr').hasClass('selected')) {
          $(".toggle-pas-all").closest('tr').toggleClass('selected');
        }
        this.modalDeletMul.hide();
        this.pnotify.success({
          title: 'Thông báo',
          text: "Xóa danh sách đoạn văn thành công!"
        });
      }
      else {
        if ($(".toggle-pas-all").closest('tr').hasClass('selected')) {
          $(".toggle-pas-all").closest('tr').toggleClass('selected');
        }
        this.modalDeletMul.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message
        });
      }
    });
  }

  openModalDetail(id: number) {
    this.passageSer.get(id).subscribe(res => {
      this.passage = res.data;
      // console.log(res.data);
      if (res.data.paS_MEDIA != null) {
        if ($.inArray(res.data.paS_MEDIA.substr(res.data.paS_MEDIA.length - 3, res.data.paS_MEDIA.length), this.videoExtension) != -1) {
          this.mediaType = "video";
        }
        else {
          this.mediaType = "audio";
        }
      }
      this.passage_Id = id;
      this.modalDetail.show();
    });
  }

  resetDetailModal() {
    this.passage_Id = null;
    for (let i = 1; i <= 2; i++) {
      if (i == 1) {
        $("#tab_pas_" + i).addClass("active");
        $("#content_pas_" + i).addClass("active");
      }
      else {
        $("#tab_pas_" + i).removeClass("active");
        $("#content_pas_" + i).removeClass("active").addClass("fade");
      }
    }
    this.loadTab(1);
  }

  openModalViewMode(id: number) {
    this.passageSer.get(id).subscribe(res => {
      this.passage = res.data;
      if (res.data.paS_MEDIA != null) {
        if ($.inArray(res.data.paS_MEDIA.substr(res.data.paS_MEDIA.length - 3, res.data.paS_MEDIA.length), this.videoExtension) != -1) {
          this.mediaType = "video";
        }
        else {
          this.mediaType = "audio";
        }
      }
      this.modalViewMode.show();
    });
  }

  lockModal(key: string) {
    $(".close_modal_pas_" + key).prop("disabled", true);
    $("#save_modal_pas_" + key).prop("disabled", true);
    $("#save_modal_pas_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_pas_" + key).prop("disabled", false);
    $("#save_modal_pas_" + key).prop("disabled", false);
    $("#save_modal_pas_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  loadTab(key: number) {
    this.tabControl = key;
  }
}
