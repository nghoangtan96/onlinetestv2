import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Route, Router } from '@angular/router';

@Component({
  templateUrl: '500.component.html'
})
export class P500Component {

  constructor(private cookieservice: CookieService, private router: Router) { 
    if (this.cookieservice.check('isLogin') == true || this.cookieservice.get('isLogin')=='1')
    {
      this.router.navigate(['/info_user']); 
    }
  }

}
