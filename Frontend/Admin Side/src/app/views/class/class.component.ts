import { Component, OnInit, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ClassService, Class } from '../../services/class.service';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { DatePipe } from '@angular/common';
import { ExportService } from '../../services/export.service';
import { DomSanitizer } from '@angular/platform-browser';
import { DataTableDirective } from 'angular-datatables';
import { Subject, Subscription } from 'rxjs';
import { Employee, EmployeeService } from '../../services/employee.service';
import { Student, StudentService } from '../../services/student.service';
import { Testtype, TestTypeService } from '../../services/test-type.service';
import { UserPermissionService, User_Permission } from '../../services/user-permission.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Setting, SettingService } from '../../services/setting.service';
import { SignalrService } from '../../services/signalr.service';
import { BranchService, Branch } from '../../services/branch.service';
import { SchoolyearService, SchoolYear } from '../../services/schoolyear.service';
import { FacultyService, Faculty } from '../../services/faculty.service';
import { NumericLiteral } from 'typescript';
import { RoleService } from '../../services/role.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.scss'],
  providers: [DatePipe]
})
export class ClassComponent implements OnInit {

  classes: [Class];
  classesMultiDel: [Class];
  branches: [Branch];
  branchParents: [Branch];
  faculties: [Faculty];
  facultyConstraint: Faculty;
  schoolyear: SchoolYear = {} as SchoolYear;
  classs: Class = {} as Class;
  pnotify = undefined;
  math: Math = Math;
  class_Id: number;
  claid: number;

  autoGenCode: Setting = {} as Setting;
  isHasChild: Setting = {} as Setting;
  extendSize: Setting = {} as Setting;
  size: number;
  fac_id: number = 0;
  isListItemNull: boolean = true;
  tabControl: number = 1;

  cla_add: boolean = true;
  cla_upd: boolean = true;
  cla_del: boolean = true;
  cla_con: boolean = true;
  cla_ref: boolean = true;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild('modalDeletMul', { static: false }) modalDeletMul: ModalDirective;

  constructor(private classSer: ClassService, private signalRSer: SignalrService,
    private pNotify: PnotifyService, private cookieSer: CookieService,
    private branchSer: BranchService, private schoolYearSer: SchoolyearService,
    private settingSer: SettingService, private facultySer: FacultyService,
    private roleSer: RoleService, private userPermissionSer: UserPermissionService, 
    private route: ActivatedRoute, private userSer: UserService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.dtOptions = {
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      order: [[1, 'asc']]
    };
    this.classSer.getAll().subscribe(res => {
      this.classes = res.data;
      // console.log(res.data);
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
    this.facultySer.getAll().subscribe(res => {
      this.faculties = res.data;
    });
    this.schoolYearSer.getSchoolYearByCurrentYear(new Date().getFullYear()).subscribe(res => {
      this.schoolyear = res.data;
    });
    this.branchSer.getAll().subscribe(res => {
      this.branches = res.data;
    });
    this.settingSer.get(3).subscribe(res => {
      this.autoGenCode = res.data;
    });
    this.settingSer.get(4).subscribe(res => {
      this.isHasChild = res.data;
    });
    this.settingSer.get(5).subscribe(res => {
      this.extendSize = res.data;
      this.size = Number(res.data.seT_CHOICEVALUE);
    });
    //permission
    this.roleSer.get(59).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'cla_add':
                this.cla_add = false;
                break;
              case 'cla_upd':
                this.cla_upd = false;
                break;
              case 'cla_del':
                this.cla_del = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    this.roleSer.get(92).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        let count = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
          }
        }
        if (!(count > 0)) {
          this.cla_ref = false;
        }
      });
    });
    this.roleSer.get(105).subscribe(res => {
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        let count = 0;
        let countNoConstraint = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == false) != null) {
              countNoConstraint++;
            }
          }
        }
        if (count > 0 && countNoConstraint == 0) {
          this.cla_con = false;
          this.userSer.get(Number(this.cookieSer.get('Id'))).subscribe(res2 => {
            this.facultySer.get(res2.data.employees[0].emP_FACID).subscribe(res3 => {
              this.facultyConstraint = res3.data;
            });
          });
        }
        else if (countNoConstraint > 0) {
          this.cla_con = true;
        }
      })
    });
    //realtime
    this.signalRSer.class.subscribe((res) => {
      if (res === true) {
        this.classSer.getAll().subscribe(res1 => {
          this.classes = res1.data;
          this.rerender();
        });
      }
    });
    this.signalRSer.schoolyear.subscribe((res) => {
      if (res === true) {
        this.schoolYearSer.getSchoolYearByCurrentYear(new Date().getFullYear()).subscribe(res1 => {
          this.schoolyear = res1.data;
        });
      }
    });
    this.signalRSer.branch.subscribe((res) => {
      if (res === true) {
        this.branchSer.getAll().subscribe(res => {
          this.branches = res.data;
        });
      }
    });
    this.signalRSer.faculty.subscribe((res1) => {
      if (res1 === true) {
        this.facultySer.getAll().subscribe(res => {
          this.faculties = res.data;
        });
      }
    });
    this.signalRSer.setting.subscribe((res1) => {
      if (res1 === true) {
        this.settingSer.get(3).subscribe(res => {
          this.autoGenCode = res.data;
        });
        this.settingSer.get(4).subscribe(res => {
          this.isHasChild = res.data;
        });
        this.settingSer.get(5).subscribe(res => {
          this.extendSize = res.data;
          this.size = Number(res.data.seT_CHOICEVALUE);
        });
      }
    });
  }

  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.claid = params['id'];
      if (this.claid != null) {
        // console.log(this.braid);
        this.openModalDetail(this.claid);
      }
    });
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
  }

  genCode() {
    let code = 'XX_XXXXX_XX';
    if (this.classs.clA_SCYEID != null && this.classs.clA_SCYEID != 0) {
      code = this.schoolyear.scyE_CODE + "_" + code.split("_")[1] + "_" + code.split("_")[2];
    }

    if (this.classs.clA_BRAID != null && this.classs.clA_BRAID != 0 && this.branchParents[0] != null) {
      let branchName = this.branches.find(x => x.id == this.classs.clA_BRAID).brA_NAME;
      let branchCode = "";
      for (let i = 0; i < branchName.split(" ").length; i++) {
        branchCode += branchName.split(" ")[i].substr(0, 1);
      }
      // check duplicate branch code
      if (this.isExistBracnhCode(branchCode, this.classs.clA_BRAID) == true) {
        code = code.split("_")[0] + "_" + branchCode + branchName.split(" ")[branchName.split(" ").length - 1].substr(1, 1) + "_" + code.split("_")[2];
      }
      else {
        code = code.split("_")[0] + "_" + branchCode + "_" + code.split("_")[2];
      }
    }

    if (this.classs.clA_SCYEID != null && this.classs.clA_SCYEID != 0 && this.classs.clA_BRAID != null && this.classs.clA_BRAID != 0) {
      let count = 0;
      for (let i = 0; i < this.classes.length; i++) {
        if (this.classes[i].clA_BRAID == this.classs.clA_BRAID && this.classes[i].clA_SCYEID == this.classs.clA_SCYEID && this.classes[i].id != this.classs.id) {
          count++;
        }
      }
      let isSortNumber = true;
      for (let i = 0; i <= count; i++) {
        let temp = code.split("_")[0] + "_" + code.split("_")[1] + "_" + (i >= 9 ? (i + 1) : "0" + (i + 1));
        if (this.classes.find(x => x.clA_CODE == temp.toUpperCase() && x.clA_BRAID == this.classs.clA_BRAID && x.clA_SCYEID == this.classs.clA_SCYEID) == null) {
          isSortNumber = false;
          code = temp;
          // console.log(code);
          break;
        }
      }
      if (isSortNumber == true) {
        code = code.split("_")[0] + "_" + code.split("_")[1] + "_" + (count >= 9 ? (count + 1) : "0" + (count + 1));
      }
    }

    return code.toUpperCase();
  }

  isExistBracnhCode(branchCode: string, branchId: number) {
    for (let i = 0; i < this.branches.length; i++) {
      let code = "";
      for (let j = 0; j < this.branches[i].brA_NAME.split(" ").length; j++) {
        code += this.branches[i].brA_NAME.split(" ")[j].substr(0, 1);
      }
      if (code.toUpperCase() == branchCode.toUpperCase() && this.branches[i].id != branchId)
        return true;
    }
    return false;
  }

  resetCode() {
    this.classs.clA_CODE = this.genCode();
  }

  generateBranch() {
    if (this.fac_id != null && this.fac_id != 0) {
      if (this.isHasChild.seT_ISACTIVE == true) {
        this.branchSer.getAllParentBranch().subscribe(res => {
          this.branchParents = res.data;
          if (res.data.find(x => x.brA_FACID == this.fac_id) != null)
            this.classs.clA_BRAID = res.data.find(x => x.brA_FACID == this.fac_id).id;
          else
            this.classs.clA_BRAID = 0;
          this.resetCode();
        });
      }
      else {
        this.branchSer.getAll().subscribe(res => {
          this.branchParents = res.data;
          if (res.data.find(x => x.brA_FACID == this.fac_id) != null)
            this.classs.clA_BRAID = res.data.find(x => x.brA_FACID == this.fac_id).id;
          else
            this.classs.clA_BRAID = 0;
          this.resetCode();
        });
      }
    }
  }

  clearField() {
    for (let i = 1; i <= 4; i++) {
      $("#field_cla_" + i).removeClass('is-invalid is-valid');
      $("#valid_mes_cla_" + i).html("");
      this.fac_id = 0;
    }
  }

  checkField() {
    for (let i = 1; i <= 4; i++) {
      $("#field_cla_" + i).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_cla_" + i).html("");
    }
  }

  validate_cla_code() {
    if (this.autoGenCode.seT_ISACTIVE == false) {
      if ($("#field_cla_1").val() == '') {
        $("#field_cla_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_cla_1").html("Vui lòng nhập mã lớp học!");
        return false;
      }
      else {
        let classCode = $("#field_cla_1").val().toString().trim();
        let isExist = false;
        for (let i = 0; i < this.classes.length; i++) {
          if (classCode == this.classes[i].clA_CODE && this.schoolyear.id != this.classes[i].id) {
            isExist = true;
            break;
          }
        }

        if (isExist == true) {
          $("#field_cla_1").removeClass('is-valid').addClass('is-invalid');
          $("#valid_mes_cla_1").html("Mã lớp học đã được sử dụng!");
          return false;
        }
        else {
          $("#field_cla_1").removeClass('is-invalid').addClass('is-valid');
          $("#valid_mes_cla_1").html("");
          return true;
        }
      }
    }
    else {
      $("#field_cla_1").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_cla_1").html("");
      return true;
    }
  }

  validate_cla_schoolyear() {
    if ($("#field_cla_2").val() == '' || $("#field_cla_2").val() == 0 || $("#field_cla_2").val() == null) {
      $("#field_cla_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_cla_2").html("Vui lòng chọn niên khóa!");
      return false;
    }
    else {
      $("#field_cla_2").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_cla_2").html("");
      return true;
    }
  }

  validate_cla_facid() {
    $("#field_cla_3").removeClass('is-invalid').addClass('is-valid');
  }

  validate_cla_braid() {
    if ($("#field_cla_4").val() == '' || $("#field_cla_4").val() == 0 || $("#field_cla_4").val() == null) {
      $("#field_cla_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_cla_4").html("Vui lòng chọn ngành học!");
      return false;
    }
    else {
      $("#field_cla_4").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_cla_4").html("");
      return true;
    }
  }

  validate() {
    var check = true;
    if (this.validate_cla_code() == false)
      check = false;
    if (this.validate_cla_schoolyear() == false)
      check = false;
    if (this.validate_cla_braid() == false)
      check = false;

    this.validate_cla_facid();
    //validate all
    if (check == false) {
      $("#valid_cla_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_cla_" + id).is(":visible")) {
      $("#buttonicon_cla_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_cla_" + id).slideToggle();
    }
    else {
      $("#buttonicon_cla_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_cla_" + id).slideToggle();
    }
  }

  ToggleGroupMulti(id: number) {
    if ($("#detailToggle_cla_mul_" + id).is(":visible")) {
      $("#buttonicon_cla_mul_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_cla_mul_" + id).slideToggle();
    }
    else {
      $("#buttonicon_cla_mul_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_cla_mul_" + id).slideToggle();
    }
  }

  openModal(id: number = 0) {
    this.unlockModal("addupdate");
    if (id > 0) { // update

      this.classSer.get(id).subscribe(res => {
        this.classs = res.data;
        // console.log(res.data);
        this.fac_id = res.data.branches.brA_FACID;

        if (this.isHasChild.seT_ISACTIVE == true) {
          this.branchSer.getAllParentBranch().subscribe(res => {
            this.branchParents = res.data;
          });
        }
        else {
          this.branchSer.getAll().subscribe(res => {
            this.branchParents = res.data;
          });
        }
      });

      this.checkField();
      $("#modal_title_cla").html("Sửa lớp học");
      this.modal.show();
    }
    else { // insert
      if (this.autoGenCode.seT_ISACTIVE == true) {
        this.classs = {
          id: 0,
          clA_CODE: "XX_XXXXX_XX"
        } as Class;
      }
      else {
        this.classs = {
          id: 0
        } as Class;
      }

      this.clearField();
      $("#modal_title_cla").html("Thêm lớp học");
      this.modal.show();
    }
  }

  save() {

    if (this.validate() == false) {
      return false;
    }

    this.lockModal("addupdate");

    if (this.classs.id === 0) { // insert
      this.classs.clA_CREATEDBY = parseInt(this.cookieSer.get('Id'));
      this.classSer.add(this.classs).subscribe(res => {
        if (res.errorCode === 0) {
          this.classSer.getAll().subscribe(res1 => {
            this.classes = res1.data;
            this.rerender();
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Thêm lớp học thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
    else { // update
      this.classs.clA_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.classSer.put(this.classs).subscribe(res => {
        if (res.errorCode === 0) {
          this.classSer.getAll().subscribe(res1 => {
            this.classes = res1.data;
            this.rerender();
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Sửa lớp học thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
  }

  openModalDelete(id: number) {
    this.unlockModal("delete");
    this.classSer.get(id).subscribe(res => {
      this.classs = res.data;
      // console.log(res.data);
      this.modalDelete.show();
    });
  }

  delete() {
    this.lockModal("delete");
    if (this.classs.id != 0) {
      this.classSer.delete(this.classs.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          this.classSer.getAll().subscribe(res1 => {
            this.classes = res1.data;
            this.rerender();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Xóa lớp học thành công!'
            });
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
      });
    }
  }

  openModalDeleteMul() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      let arr = [];
      dtInstance.rows({ selected: true }).every(function () {
        if (this.data()[1] != null) {
          arr.push(this.data()[1]);
        }
      });
      if (arr.length == 0) {
        this.isListItemNull = true;
        this.modalDeletMul.show();
      }
      else {
        this.isListItemNull = false;
        this.classSer.getListClassMultiDel(arr).subscribe(res => {
          this.classesMultiDel = res.data;
          // console.log(this.subjectsMultiDel);
          this.modalDeletMul.show();
        });
      }
    });
  }
  deleteMul() {
    this.classSer.deleteMulti(this.classesMultiDel, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
      if (res.errorCode === 0) {
        this.classSer.getAll().subscribe(res1 => {
          this.classes = res1.data;
          this.rerender();
          this.modalDeletMul.hide();
          this.pnotify.success({
            title: 'Thông báo',
            text: "Xóa danh sách lớp học thành công!"
          });
        });
      }
      else {
        this.modalDeletMul.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message
        });
      }
    });
  }

  openModalDetail(id: number) {
    this.classSer.get(id).subscribe(res => {
      this.classs = res.data;
      this.class_Id = res.data.id;
      this.modalDetail.show();
    });
  }

  resetDetailModal() {
    this.class_Id = null;
    for (let i = 1; i <= 2; i++) {
      if (i == 1) {
        $("#tab_cla_" + i).addClass("active");
        $("#content_cla_" + i).addClass("active");
      }
      else {
        $("#tab_cla_" + i).removeClass("active");
        $("#content_cla_" + i).removeClass("active").addClass("fade");
      }
    }
    this.loadTab(1);
  }

  loadTab(key: number) {
    this.tabControl = key;
  }

  lockModal(key: string) {
    $(".close_modal_cla_" + key).prop("disabled", true);
    $("#save_modal_cla_" + key).prop("disabled", true);
    $("#save_modal_cla_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_cla_" + key).prop("disabled", false);
    $("#save_modal_cla_" + key).prop("disabled", false);
    $("#save_modal_cla_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }
}
