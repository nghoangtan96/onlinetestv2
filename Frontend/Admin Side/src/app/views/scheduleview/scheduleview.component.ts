import { Component, OnInit, ViewChild } from '@angular/core';
import { ScheduleTestService, ScheduleTest } from '../../services/schedule-test.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-scheduleview',
  templateUrl: './scheduleview.component.html',
  styleUrls: ['./scheduleview.component.css']
})
export class ScheduleviewComponent implements OnInit {

  scheduleTests: [ScheduleTest];

  isListItemNull: boolean = true;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  constructor(private scheduleTestSer: ScheduleTestService, private cookieSer: CookieService) { }

  ngOnInit() {
    this.scheduleTestSer.getAllIncoming().subscribe(res => {
      this.scheduleTests = res.data;

      for (let i = 0; i < res.data.length; i++) {
        if (res.data[i].student_Tests.find(x => x.students.stU_USEID == Number(this.cookieSer.get('Id'))) == null) {
          // delete res.data[i];
          res.data.splice(i, 1);
          i--;
        }
      }

      if (res.data[0] != null) {
        for (let i = 0; i < res.data.length; i++) {
          var date = new Date("2020-06-22 " + res.data[i].testShifts.shI_TIMESTART);
          date.setTime(date.getTime() + res.data[i].tests.teS_TIME * 60000);
          var hours = date.getHours();
          var minutes = date.getMinutes();
          res.data[i].testShifts.timeEnd = ((hours > 12) ? (hours - 12 + ':' + minutes + ' PM') : (hours + ':' + minutes + ' AM'));
        }
        this.isListItemNull = false;
      }
      else {
        this.isListItemNull = true;
      }
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
  }

}
