import { Component, ViewChild } from '@angular/core';
import { LoginRequest, AuthService, ResetPasswordResult, ResetPasswordRequest } from '../../services/auth.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { UserService, User } from '../../services/user.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PnotifyService } from '../../services/pnotify.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {

  message = '';
  loginreq: LoginRequest = {} as LoginRequest;
  resetPasswordRequest: ResetPasswordRequest = {} as ResetPasswordRequest;

  pnotify = undefined;

  @ViewChild('modalResetPassword', { static: false }) modalResetPassword: ModalDirective;
  constructor(private authSer: AuthService, private router: Router,
    private cookieservice: CookieService, private useSer: UserService,
    private pNotify: PnotifyService) {
    this.pnotify = this.pNotify.getPNotify();
  }
  ngOnInit() {
    if (this.cookieservice.check('Id') == true) {
      this.router.navigate(['/info_user']);
    }

  }

  validate_user() {
    if ($("#field_1").val() == '') {
      $("#field_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_1").html("Vui lòng nhập tên đăng nhập!");
      return false;
    }
    else {
      $("#field_1").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_1").html("");
      return true;
    }
  }
  validate_pass() {
    if ($("#field_2").val() == '' || $("#field_2").val() == null) {
      $("#field_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_2").html("Vui lòng nhập mật khẩu!");
      return false;
    }
    else {
      $("#field_2").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_2").html("");
      return true;
    }
  }
  validate() {
    var check = true;
    if (this.validate_user() == false)
      check = false;
    if (this.validate_pass() == false)
      check = false;
    //validate all
    if (check == false)
      return false;
    else
      return true;
  }
  login() {
    if (this.validate() == false) {
      return false;
    }
    this.authSer.login(this.loginreq).subscribe(res => {
      if (res.errorCode === 0) {
        // console.log(res);
        // save info user
        this.cookieservice.set('Id', res.data.id.toString());
        this.cookieservice.set('Username', res.data.username);
        this.cookieservice.set('token', res.data.token);
        // this.cookieservice.set('isLogin', '1');

        this.router.navigate(['/info_user']);
      } else if (res.errorCode == 2) {
        $("#field_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_1").html(res.message);
      }
      else if (res.errorCode == 3) {
        $("#field_2").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_2").html(res.message);
      }
      else {
        this.message = res.message;
      }
    });
  }

  validate_RP_email() {
    var regex = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if ($("#field_RP_1").val() == '') {
      $("#field_RP_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_RP_1").html("Vui lòng nhập địa chỉ email!");
      return false;
    }
    else if (!regex.test($("#field_RP_1").val().toString())) {
      $("#field_RP_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_RP_1").html("Email sai định dạng!");
      return false;
    }
    else {
      $("#field_RP_1").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_RP_1").html("");
      return true;
    }
  }

  openModalResetPassword() {
    this.unlockModal("upd");
    $("#field_RP_1").removeClass('is-invalid is-valid');
    $("#valid_mes_RP_1").html("");
    this.resetPasswordRequest = {
      email: ""
    } as ResetPasswordRequest;
    this.modalResetPassword.show();
  }

  resetPassword() {
    if (this.validate_RP_email() == false)
      return false;
    this.lockModal("upd");

    this.authSer.resetPassword(this.resetPasswordRequest).subscribe(res => {
      if (res.errorCode === 404) {
        this.unlockModal("upd");
        $("#field_RP_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_RP_1").html(res.message);
      }
      else {
        this.modalResetPassword.hide();
        this.pnotify.success({
          title: 'Thông báo',
          text: res.message
        });
      }
    });
  }

  lockModal(key: string) {
    $(".close_modal_RP_" + key).prop("disabled", true);
    $("#save_modal_RP_" + key).prop("disabled", true);
    $("#save_modal_RP_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_RP_" + key).prop("disabled", false);
    $("#save_modal_RP_" + key).prop("disabled", false);
    $("#save_modal_RP_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }
}
