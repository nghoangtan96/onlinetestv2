import { Component, OnInit, ViewChild } from '@angular/core';
import { StudentTest, StudentTestService } from '../../services/student-test.service';
import { Testtype, TestTypeService } from '../../services/test-type.service';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { SettingService, Setting } from '../../services/setting.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  studentTests: [StudentTest] = null;
  testTypes: [Testtype];
  studentCode: string;
  pnotify = undefined;
  isAllow4: boolean;
  isExist: boolean;
  isWrong: boolean = false;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  constructor(private pNotify: PnotifyService, private cookieSer: CookieService,
    private TestTypeSer: TestTypeService, private settingSer: SettingService,
    private studentTestSer: StudentTestService, private route: ActivatedRoute) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.TestTypeSer.getAll().subscribe(res => {
      this.testTypes = res.data;
    });
    this.settingSer.get(11).subscribe(res => {
      if (res.errorCode === 0) {
        this.isAllow4 = (res.data.seT_MULTICHOICEVALUE.split("/")[1] == "1" ? true : false);
      }
    });
  }

  searchResult() {
    this.studentTestSer.getAllByUserIdForResult(this.studentCode).subscribe(res => {
      // console.log(res.data);
      if (res.errorCode === 0 || res.errorCode == 404) {
        if (this.dtElement.dtInstance != null) {
          this.studentTests = res.data;
          this.rerender();
        }
        else {
          this.studentTests = res.data;
          this.dtTrigger.next();
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.columns().every(function () {
              const that = this;
              $('input', this.footer()).on('keyup change', function () {
                if (that.search() !== this['value']) {
                  that
                    .search(this['value'])
                    .draw();
                }
              });
            });
          });
        }
        this.isExist = true;
        this.isWrong = false;
      }
      else {
        this.isExist = false;
        this.isWrong = true;
        // this.studentTests = null;
      }
    });
  }

  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.studentCode = params['id'];
      if (this.studentCode != null) {
        // console.log(this.braid);
        this.searchResult();
      }
    });
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
  }

}
