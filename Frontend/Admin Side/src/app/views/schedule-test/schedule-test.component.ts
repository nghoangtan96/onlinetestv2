import { Component, OnInit, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { ScheduleTest, ScheduleTestService } from '../../services/schedule-test.service';
import { Testtype, TestTypeService } from '../../services/test-type.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { ActivatedRoute } from '@angular/router';
import { SettingService } from '../../services/setting.service';
import { SignalrService } from '../../services/signalr.service';
import { TestService, Test } from '../../services/test.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subjectt, SubjectService } from '../../services/subject.service';
import { TestshiftService, TestShift } from '../../services/testshift.service';
import { LabService, Lab } from '../../services/lab.service';
import { ScheduleTestRequest } from '../../models/scheduleTestRequest';
import { StudentService, Student } from '../../services/student.service';
import { StudentTestService } from '../../services/student-test.service';
import { RoleService } from '../../services/role.service';
import { UserPermissionService } from '../../services/user-permission.service';

@Component({
  selector: 'app-schedule-test',
  templateUrl: './schedule-test.component.html',
  styleUrls: ['./schedule-test.component.css']
})
export class ScheduleTestComponent implements OnInit {

  id: number;
  scheduleTest_Id: number;
  test_Id: number;

  scheduleTests: [ScheduleTest];
  scheduleTest: ScheduleTest = {} as ScheduleTest;
  scheduleTestView: ScheduleTest;
  scheduleTestsMultiDel: [ScheduleTest];
  testtypes: [Testtype];
  testtype: Testtype = {} as Testtype;
  testtypeID: number;
  subjectID: number;
  subjects: [Subjectt];
  tests: [Test];
  testShifts: [TestShift];
  labs: [Lab];
  scheduleTestRequest: ScheduleTestRequest = {} as ScheduleTestRequest;

  pnotify = undefined;
  isListItemNull: boolean = true;
  tabControl: number = 1;
  tabControlAddupd: number = 1;

  //add student
  countStudent: number = 0;
  maxStudent: number = 50;
  minStudent: number = 16;
  listStudent: Student[] = [];
  numberOfStudentLeft: number;
  countStudentPickLeft: number;
  students: [Student];
  student: Student;

  //update
  allowUpdateStudent: boolean = true;

  scte_add: boolean = true;
  scte_upd: boolean = true;
  scte_del: boolean = true;
  scte_ref: boolean = true;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  dtTrigger1: Subject<any> = new Subject();
  dtOptions1: any = {};
  @ViewChildren(DataTableDirective)
  dtElements: QueryList<DataTableDirective>;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalAddStudent', { static: false }) modalAddStudent: ModalDirective;
  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild('modalDeletMul', { static: false }) modalDeletMul: ModalDirective;
  @ViewChild('modalTestView', { static: false }) modalTestView: ModalDirective;

  constructor(private pNotify: PnotifyService, private cookieSer: CookieService,
    private settingSer: SettingService, private route: ActivatedRoute,
    private signalRSer: SignalrService, private testSer: TestService,
    private testTypeSer: TestTypeService, private _DomSanitizer: DomSanitizer,
    private scheduleTestSer: ScheduleTestService, private subjectSer: SubjectService,
    private testshiftSer: TestshiftService, private labSer: LabService,
    private studentSer: StudentService, private studentTestSer: StudentTestService,
    private roleSer: RoleService, private userPermissionSer: UserPermissionService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.dtOptions = {
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      order: [[1, 'asc']]
    };
    this.scheduleTestSer.getAll().subscribe(res => {
      for (let i = 0; i < res.data.length; i++) {
        var date = new Date("2020-06-22 " + res.data[i].testShifts.shI_TIMESTART);
        date.setTime(date.getTime() + res.data[i].tests.teS_TIME * 60000);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        res.data[i].testShifts.timeEnd = ((hours > 12) ? (hours - 12 + ':' + minutes + ' PM') : (hours + ':' + minutes + ' AM'));
      }
      this.scheduleTests = res.data;
      this.dtTrigger.next();
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
              dtInstance.columns().every(function () {
                const that = this;
                $('input', this.footer()).on('keyup change', function () {
                  if (that.search() !== this['value']) {
                    that
                      .search(this['value'])
                      .draw();
                  }
                });
              });
            }
          });
        }
      });
    });
    this.testTypeSer.getAll().subscribe(res => {
      this.testtypes = res.data;
    });
    this.subjectSer.getAll().subscribe(res => {
      this.subjects = res.data;
    });
    this.testshiftSer.getAll().subscribe(res => {
      this.testShifts = res.data;
    });
    this.labSer.getAll().subscribe(res => {
      this.labs = res.data;
    });
    //permission
    this.roleSer.get(101).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);s
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'scte_add':
                this.scte_add = false;
                break;
              case 'scte_upd':
                this.scte_upd = false;
                break;
              case 'scte_del':
                this.scte_del = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    //realtime
    this.signalRSer.scheduletest.subscribe((res1) => {
      if (res1 === true) {
        this.scheduleTestSer.getAll().subscribe(res => {
          for (let i = 0; i < res.data.length; i++) {
            var date = new Date("2020-06-22 " + res.data[i].testShifts.shI_TIMESTART);
            date.setTime(date.getTime() + res.data[i].tests.teS_TIME * 60000);
            var hours = date.getHours();
            var minutes = date.getMinutes();
            res.data[i].testShifts.timeEnd = ((hours > 12) ? (hours - 12 + ':' + minutes + ' PM') : (hours + ':' + minutes + ' AM'));
          }
          this.scheduleTests = res.data;
          this.rerender();
        });
      }
    });
    this.signalRSer.testtype.subscribe((res1) => {
      if (res1 === true) {
        this.testTypeSer.getAll().subscribe(res => {
          this.testtypes = res.data;
        });
      }
    });
    this.signalRSer.subject.subscribe((res1) => {
      if (res1 === true) {
        this.subjectSer.getAll().subscribe(res => {
          this.subjects = res.data;
        });
      }
    });
    this.signalRSer.testshift.subscribe((res1) => {
      if (res1 === true) {
        this.testshiftSer.getAll().subscribe(res => {
          this.testShifts = res.data;
        });
      }
    });
    this.signalRSer.lab.subscribe((res1) => {
      if (res1 === true) {
        this.labSer.getAll().subscribe(res => {
          this.labs = res.data;
        });
      }
    });
  }

  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
      if (this.id != null) {
        // this.openModalDetail(this.id);
      }
    });
  }

  Select_Clear_All() {
    $(".toggle-scte-all").closest('tr').toggleClass('selected');
    if ($(".toggle-scte-all").closest('tr').hasClass('selected')) {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_main") {
              dtInstance.rows({ search: 'applied' }).every(function () {
                this.select();
              });
            }
          });
        }
      });
    }
    else {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_main") {
              dtInstance.rows({ search: 'applied' }).every(function () {
                this.deselect();
              });
            }
          });
        }
      });
    }
  }

  Select_Clear_All_Student() {
    $(".toggle-scte-stu-all").closest('tr').toggleClass('selected');
    if ($(".toggle-scte-stu-all").closest('tr').hasClass('selected')) {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_sub") {
              dtInstance.rows({ search: 'applied' }).every(function () {
                this.select();
              });
            }
          });
        }
      });
    }
    else {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_sub") {
              dtInstance.rows({ search: 'applied' }).every(function () {
                this.deselect();
              });
            }
          });
        }
      });
    }
    this.countStudentPickFunc();
  }

  countStudentPickFunc() {
    this.countStudentPickLeft = 0;
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      if (dtElement.dtInstance != null) {
        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
            let arr = [];
            dtInstance.rows({ selected: true }).every(function () {
              if (this.data()[1] != null) {
                arr.push(this.data()[1]);
              }
            });
            this.countStudentPickLeft += arr.length;
          }
        });
      }
    });
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    this.dtTrigger1.unsubscribe();
  }

  rerender(): void {
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      if (dtElement.dtInstance != null) {
        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == "tb_main") {
            dtInstance.destroy();
            this.dtTrigger.next();
            this.dtElements.forEach((dtElement: DataTableDirective) => {
              if (dtElement.dtInstance != null) {
                dtElement.dtInstance.then((dtInstance: any) => {
                  if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
                    dtInstance.columns().every(function () {
                      const that = this;
                      $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this['value']) {
                          that
                            .search(this['value'])
                            .draw();
                        }
                      });
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }

  rerender1(): void {
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      if (dtElement.dtInstance != null) {
        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == "tb_sub") {
            dtInstance.destroy();
            this.dtTrigger1.next();
            this.dtElements.forEach((dtElement: DataTableDirective) => {
              if (dtElement.dtInstance != null) {
                dtElement.dtInstance.then((dtInstance: any) => {
                  if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
                    dtInstance.columns().every(function () {
                      const that = this;
                      $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this['value']) {
                          that
                            .search(this['value'])
                            .draw();
                        }
                      });
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }

  clearField() {
    this.tabControlAddupd = 1;
    for (let i = 1; i <= 6; i++) {
      $("#field_scte_" + i).removeClass('is-invalid is-valid');
      $("#valid_mes_scte_" + i).html("");
      if (i != 3) {
        $("#field_scte_" + i).prop("disabled", false);
      }
    }

    for (let i = 1; i <= 4; i++) {
      if (i == 1) {
        $("#tab_addupd_scte_" + i).addClass("active");
        $("#content_addupd_scte_" + i).removeClass("fade").addClass("active");
      }
      else {
        $("#tab_addupd_scte_" + i).removeClass("active");
        $("#content_addupd_scte_" + i).removeClass("active").addClass("fade");
      }
    }

    $("#valid_mes_tes_all_tab1").html();
    this.testtypeID = null;
    this.subjectID = null;
    this.listStudent = [];
  }

  checkField() {
    this.tabControlAddupd = 2;
    for (let i = 1; i <= 6; i++) {
      $("#field_scte_" + i).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_scte_" + i).html("");
      if (i != 3) {
        $("#field_scte_" + i).prop("disabled", true);
      }
    }

    for (let i = 1; i <= 4; i++) {
      if (i == 2) {
        $("#tab_addupd_scte_" + i).addClass("active");
        $("#content_addupd_scte_" + i).removeClass("fade").addClass("active");
      }
      else {
        $("#tab_addupd_scte_" + i).removeClass("active");
        $("#content_addupd_scte_" + i).removeClass("active").addClass("fade");
      }
    }
    this.listStudent = [];
  }

  validate_scte_subject() {
    if ($("#field_scte_2").val() == '' || $("#field_scte_2").val() == 0 || $("#field_scte_2").val() == null) {
      $("#field_scte_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_scte_2").html("Vui lòng chọn môn học!");
      return false;
    }
    else {
      $("#field_scte_2").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_scte_2").html("");
      return true;
    }
  }

  validate_scte_testtype() {
    if ($("#field_scte_1").val() == '' || $("#field_scte_1").val() == 0 || $("#field_scte_1").val() == null) {
      $("#field_scte_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_scte_1").html("Vui lòng chọn loại bài thi!");
      return false;
    }
    else {
      $("#field_scte_1").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_scte_1").html("");
      return true;
    }
  }

  validate_scte_test() {
    if ($("#field_scte_3").val() == '' || $("#field_scte_3").val() == 0 || $("#field_scte_3").val() == null) {
      $("#field_scte_3").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_scte_3").html("Đề thi chưa được tạo hoặc chưa được duyệt!");
      return false;
    }
    else {
      $("#field_scte_3").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_scte_3").html("");
      return true;
    }
  }

  validate_scte_date() {
    var date = new Date();
    date.setDate(date.getDate() + 7);
    // console.log(date);
    // console.log(this.scheduleTest.sctE_DATE);
    if ($("#field_scte_4").val() == '' || $("#field_scte_4").val() == null) {
      $("#field_scte_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_scte_4").html("Vui lòng chọn ngày thi!");
      return false;
    }
    else if (new Date(this.scheduleTest.sctE_DATE) < date) {
      $("#field_scte_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_scte_4").html("Ngày thi phải cách ngày hiện tại ít nhất 7 ngày!");
      return false;
    }
    else {
      $("#field_scte_4").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_scte_4").html("");
      if (new Date(this.scheduleTest.sctE_DATE) < date) {
        this.allowUpdateStudent = false;
      }
      else {
        this.allowUpdateStudent = true;
      }
      return true;
    }
  }

  validate_scte_testshift() {
    if ($("#field_scte_5").val() == '' || $("#field_scte_5").val() == 0 || $("#field_scte_5").val() == null) {
      $("#field_scte_5").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_scte_5").html("Vui lòng chọn ca thi!");
      return false;
    }
    else {
      $("#field_scte_5").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_scte_5").html("");
      return true;
    }
  }

  validate_scte_lab() {
    if ($("#field_scte_6").val() == '' || $("#field_scte_6").val() == 0 || $("#field_scte_6").val() == null) {
      $("#field_scte_6").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_scte_6").html("Vui lòng chọn phòng thi!");
      return false;
    }
    else {
      $("#field_scte_6").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_scte_6").html("");
      return true;
    }
  }

  validate_scte_scheduletest_allow() {
    if (this.scheduleTest.sctE_TESID != null && this.scheduleTest.sctE_DATE != null && this.scheduleTest.sctE_SHIID != null && this.scheduleTest.sctE_LABID != null) {
      if (this.scheduleTests.find(x => x.sctE_TESID == this.scheduleTest.sctE_TESID) != null) {
        let scheduleTestTemp = this.scheduleTests.find(x => x.sctE_TESID == this.scheduleTest.sctE_TESID && x.id != this.scheduleTest.id && x.sctE_STATUS != 3);
        if (scheduleTestTemp != null) {
          if (new Date(scheduleTestTemp.sctE_DATE).getDate() == new Date(this.scheduleTest.sctE_DATE).getDate() && new Date(scheduleTestTemp.sctE_DATE).getMonth() == new Date(this.scheduleTest.sctE_DATE).getMonth() && new Date(scheduleTestTemp.sctE_DATE).getFullYear() == new Date(this.scheduleTest.sctE_DATE).getFullYear() && scheduleTestTemp.sctE_SHIID == this.scheduleTest.sctE_SHIID && scheduleTestTemp.sctE_LABID == this.scheduleTest.sctE_LABID && this.scheduleTest.sctE_STATUS != 3) {
            $("#valid_mes_tes_all_tab1").html('<i class="fa fa-exclamation-circle"></i> Lịch thi đã tồn tại, vui lòng thay đổi ngày thi, ca thi hoặc phòng thi!');
            return false;
          }
          else if ((new Date(scheduleTestTemp.sctE_DATE).getDate() != new Date(this.scheduleTest.sctE_DATE).getDate() || new Date(scheduleTestTemp.sctE_DATE).getMonth() != new Date(this.scheduleTest.sctE_DATE).getMonth() || new Date(scheduleTestTemp.sctE_DATE).getFullYear() != new Date(this.scheduleTest.sctE_DATE).getFullYear() || scheduleTestTemp.sctE_SHIID != this.scheduleTest.sctE_SHIID) && this.scheduleTest.sctE_STATUS != 3) {
            $("#valid_mes_tes_all_tab1").html('<i class="fa fa-exclamation-circle"></i> Không thể xếp ngày thi, ca thi khác nhau trong cùng một đề thi!');
            return false;
          }
          else {
            $("#valid_mes_tes_all_tab1").html("");
            return true;
          }
        }
        else {
          $("#valid_mes_tes_all_tab1").html("");
          return true;
        }
      }
      else {
        let scheduleTestTemp = this.scheduleTests.find(x => x.id != this.scheduleTest.id && x.sctE_STATUS != 3);
        if (scheduleTestTemp != null) {
          if (new Date(scheduleTestTemp.sctE_DATE).getDate() == new Date(this.scheduleTest.sctE_DATE).getDate() && new Date(scheduleTestTemp.sctE_DATE).getMonth() == new Date(this.scheduleTest.sctE_DATE).getMonth() && new Date(scheduleTestTemp.sctE_DATE).getFullYear() == new Date(this.scheduleTest.sctE_DATE).getFullYear() && scheduleTestTemp.sctE_SHIID == this.scheduleTest.sctE_SHIID && scheduleTestTemp.sctE_LABID == this.scheduleTest.sctE_LABID && this.scheduleTest.sctE_STATUS != 3) {
            $("#valid_mes_tes_all_tab1").html('<i class="fa fa-exclamation-circle"></i> Lịch thi đã tồn tại, vui lòng thay đổi ngày thi, ca thi hoặc phòng thi!');
            return false;
          }
          else {
            $("#valid_mes_tes_all_tab1").html("");
            return true;
          }
        }
        else {
          $("#valid_mes_tes_all_tab1").html("");
          return true;
        }
      }
    }
  }

  validate() {
    var check = true;
    if (this.tabControlAddupd == 1) {
      if (this.validate_scte_subject() == false)
        check = false;
      if (this.validate_scte_testtype() == false)
        check = false;
      if (this.validate_scte_test() == false)
        check = false;
      if (this.validate_scte_testshift() == false)
        check = false;
      if (this.validate_scte_date() == false)
        check = false;
      if (this.validate_scte_lab() == false)
        check = false;
      if (this.validate_scte_scheduletest_allow() == false)
        check = false;
    }
    else if (this.tabControlAddupd == 2) {
      // if (this.countStudent < this.minStudent || this.countStudent > this.maxStudent) {
      //   check = false;
      //   $("#valid_scte_all").show(0).delay(3000).hide(0);
      // }
    }
    //validate all
    if (check == false) {
      $("#valid_scte_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_scte_" + id).is(":visible")) {
      $("#buttonicon_scte_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_scte_" + id).slideToggle();
    }
    else {
      $("#buttonicon_scte_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_scte_" + id).slideToggle();
    }
  }

  ToggleGroupMulti(id: number) {
    if ($("#detailToggle_scte_mul_" + id).is(":visible")) {
      $("#buttonicon_scte_mul_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_scte_mul_" + id).slideToggle();
    }
    else {
      $("#buttonicon_scte_mul_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_scte_mul_" + id).slideToggle();
    }
  }

  openModal(id: number = 0) {
    this.unlockModal("addupd");

    if (id > 0) { // update
      this.checkField();
      this.scheduleTestSer.get(id).subscribe(res => {
        this.scheduleTest = res.data;
        // console.log(res.data);
        this.testtypeID = res.data.tests.test_Types.id;
        this.subjectID = res.data.tests.subjects.id;
        this.scheduleTest.sctE_DATE = this.scheduleTest.sctE_DATE.substring(0, 10);
        let countStudent = 0;
        for (let i = 0; i < this.scheduleTest.student_Tests.length; i++) {
          this.listStudent.push(this.scheduleTest.student_Tests[i].students);
          countStudent++;
        }
        this.countStudent = countStudent;
        var date = new Date();
        date.setDate(date.getDate() + 7);
        if (new Date(this.scheduleTest.sctE_DATE) < date) {
          this.allowUpdateStudent = false;
        }
        else {
          this.allowUpdateStudent = true;
        }
        this.testSer.getTestByTesttypeIdAndSubjectId(this.testtypeID, this.subjectID).subscribe(res1 => {
          this.tests = res1.data;
        });
      });

      $("#modal_title_scte").html("Sửa lịch thi");
      this.modal.show();
    }
    else { // insert
      this.scheduleTest = {
        id: 0,
        sctE_TESID: 0
      } as ScheduleTest;

      this.clearField();
      $("#modal_title_scte").html("Thêm lịch thi");
      this.modal.show();
    }
  }

  save() {

    if (this.validate() == false) {
      return false;
    }

    this.scheduleTestRequest = {
      scheduleTest: this.scheduleTest,
      listStudent: this.listStudent
    } as ScheduleTestRequest;
    console.log(this.scheduleTestRequest);

    this.lockModal("addupd");
    if (this.scheduleTest.id === 0) { // insert
      this.scheduleTest.sctE_CREATEDBY = parseInt(this.cookieSer.get('Id'));

      this.scheduleTestSer.add(this.scheduleTestRequest).subscribe(res => {
        if (res.errorCode === 0) {
          this.scheduleTestSer.getAll().subscribe(res1 => {
            this.scheduleTests = res1.data;
            for (let i = 0; i < res1.data.length; i++) {
              var date = new Date("2020-06-22 " + res1.data[i].testShifts.shI_TIMESTART);
              date.setTime(date.getTime() + res1.data[i].tests.teS_TIME * 60000);
              var hours = date.getHours();
              var minutes = date.getMinutes();
              res1.data[i].testShifts.timeEnd = ((hours > 12) ? (hours - 12 + ':' + minutes + ' PM') : (hours + ':' + minutes + ' AM'));
            }
            this.rerender();
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Thêm lịch thi thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
    else { // update
      this.scheduleTest.sctE_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.scheduleTestSer.put(this.scheduleTestRequest).subscribe(res => {
        if (res.errorCode === 0) {
          this.scheduleTestSer.getAll().subscribe(res1 => {
            this.scheduleTests = res1.data;
            for (let i = 0; i < res1.data.length; i++) {
              var date = new Date("2020-06-22 " + res1.data[i].testShifts.shI_TIMESTART);
              date.setTime(date.getTime() + res1.data[i].tests.teS_TIME * 60000);
              var hours = date.getHours();
              var minutes = date.getMinutes();
              res1.data[i].testShifts.timeEnd = ((hours > 12) ? (hours - 12 + ':' + minutes + ' PM') : (hours + ':' + minutes + ' AM'));
            }
            this.rerender();
            this.modal.hide();
            if (this.modalDetail.isShown) {
              this.openModalDetail(res.data.id);
            }
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Sửa lịch thi thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
  }

  lockModal(key: string) {
    $(".close_modal_scte_" + key).prop("disabled", true);
    $("#save_modal_scte_" + key).prop("disabled", true);
    $("#save_modal_scte_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_scte_" + key).prop("disabled", false);
    $("#save_modal_scte_" + key).prop("disabled", false);
    $("#save_modal_scte_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  generateTest() {
    if (this.subjectID != null && this.testtypeID != null) {
      this.testSer.getTestByTesttypeIdAndSubjectId(this.testtypeID, this.subjectID).subscribe(res => {
        this.tests = res.data;
        // console.log(res.data);
        if (res.data[0] != null) {
          this.scheduleTest.sctE_TESID = res.data[0].id;
          // console.log(this.scheduleTest.sctE_TESID);
        }
      });
    }
  }

  showTest(id: number) {
    // console.log(id);
    if (id != null && id != 0) {
      this.test_Id = id;
      this.modalTestView.show();
    }
  }

  resetshowTest() {
    this.test_Id = null;
  }

  nextStep() {
    if (this.validate() == false) {
      return false;
    }

    if (this.tabControlAddupd == 1) {
      for (let i = 0; i <= 6; i++) {
        if (i != 3) {
          $("#field_scte_" + i).prop("disabled", true);
        }
      }

      for (let i = 1; i <= 2; i++) {
        if (i == 2) {
          $("#tab_addupd_scte_" + i).addClass("active");
          $("#content_addupd_scte_" + i).removeClass("fade").addClass("active");
        }
        else {
          $("#tab_addupd_scte_" + i).removeClass("active");
          $("#content_addupd_scte_" + i).removeClass("active").addClass("fade");
        }
      }

      this.tabControlAddupd = 2;
    }
  }

  prevStep() {
    if (this.tabControlAddupd == 2) {
      for (let i = 0; i <= 6; i++) {
        if (i != 3) {
          $("#field_scte_" + i).prop("disabled", false);
        }
      }

      for (let i = 1; i <= 2; i++) {
        if (i == 1) {
          $("#tab_addupd_scte_" + i).addClass("active");
          $("#content_addupd_scte_" + i).removeClass("fade").addClass("active");
        }
        else {
          $("#tab_addupd_scte_" + i).removeClass("active");
          $("#content_addupd_scte_" + i).removeClass("active").addClass("fade");
        }
      }

      this.tabControlAddupd = 1;
    }
  }

  openModalAddStudent() {
    this.unlockModal("addstu");
    $("#valid_mes_tes_addstudent").html("");
    this.numberOfStudentLeft = this.maxStudent - this.countStudent;
    this.countStudentPickLeft = 0;
    this.studentSer.getListBySubjectId(this.subjectID).subscribe(res => {

      for (let i = 0; i < res.data.length; i++) {
        if (this.listStudent.find(x => x.id == res.data[i].id) != null) {
          res.data.splice(i, 1);
          i--;
        }
        else {
          for (let j = 0; j < this.scheduleTests.length; j++) {
            if (this.scheduleTests[j].sctE_TESID == this.scheduleTest.sctE_TESID && this.scheduleTests[j].sctE_STATUS != 3 && this.scheduleTests[j].student_Tests.find(x => x.sttE_STUID == res.data[i].id) != null) {
              res.data.splice(i, 1);
              i--;
            }
          }
        }
      }

      if (this.students != null) {
        this.students = res.data;
        this.rerender1();
      }
      else {
        this.students = res.data;
        this.dtTrigger1.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
      }
    });

    this.modalAddStudent.show();
  }

  addStudent() {
    this.lockModal("addque");
    this.countStudent += this.countStudentPickLeft;
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      if (dtElement.dtInstance != null) {
        dtElement.dtInstance.then((dtInstance: any) => {
          //que
          if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
            let arr = [];
            dtInstance.rows({ selected: true }).every(function () {
              if (this.data()[1] != null) {
                arr.push(this.data()[1]);
              }
            });
            // console.log(this.questionsData);
            for (let i = 0; i < arr.length; i++) {
              this.listStudent.push(this.students.find(x => x.id == arr[i]));
            }
            this.sortMarkQuestion();
          }
        });
      }
    });

    if ($(".toggle-scte-stu-all").closest('tr').hasClass('selected')) {
      $(".toggle-scte-stu-all").closest('tr').toggleClass('selected');
    }

    this.modalAddStudent.hide();
  }

  studentPick(id: number) {
    if ($("#table_quepick_row_" + id).hasClass('selected') == true) {
      this.countStudent--;
    }
    else {
      this.countStudent++;
    }
  }

  sortMarkQuestion() {
    let count = 0;
    for (let i = 0; i < this.listStudent.length; i++) {
      $("#markStudent_" + this.listStudent[i].id).html((count + 1).toString());
      count++;
    }
  }

  showQuestion(id: number, event = null) {
    if (event != null) {
      event.preventDefault();
    }
    this.studentSer.get(id).subscribe(res => {
      this.student = res.data;
    });
  }

  removeStudent(id: number, event = null) {
    if (event != null) {
      event.preventDefault();
    }
    if (this.tabControlAddupd == 2) {
      this.countStudent--;

      const deletedItem = this.listStudent.find(x => x.id == id);
      const index = this.listStudent.indexOf(deletedItem);
      this.listStudent.splice(index, 1);

      if (this.student != null && this.student.id == id) {
        this.student = null;
      }

      this.sortMarkQuestion();
    }
  }

  openModalDetail(id: number) {
    this.scheduleTestSer.get(id).subscribe(res => {
      this.scheduleTestView = res.data;
      var date = new Date("2020-06-22 " + res.data.testShifts.shI_TIMESTART);
      date.setTime(date.getTime() + res.data.tests.teS_TIME * 60000);
      var hours = date.getHours();
      var minutes = date.getMinutes();
      res.data.testShifts.timeEnd = ((hours > 12) ? (hours - 12 + ':' + minutes + ' PM') : (hours + ':' + minutes + ' AM'));
      this.scheduleTest_Id = res.data.id;
      this.modalDetail.show();
    });
  }

  resetDetailModal() {
    this.scheduleTest_Id = null;
    //reset tab
    for (let i = 1; i <= 2; i++) {
      if (i == 1) {
        $("#tab_scte_" + i).addClass("active");
        $("#content_scte_" + i).addClass("active");
      }
      else {
        $("#tab_scte_" + i).removeClass("active");
        $("#content_scte_" + i).removeClass("active").addClass("fade");
      }
    }
    this.loadTab(1);
  }

  loadTab(key: number) {
    this.tabControl = key;
  }

  openModalDelete(id: number) {
    this.unlockModal("del");
    this.scheduleTestSer.get(id).subscribe(res => {
      this.scheduleTest = res.data;
      var date = new Date("2020-06-22 " + res.data.testShifts.shI_TIMESTART);
      date.setTime(date.getTime() + res.data.tests.teS_TIME * 60000);
      var hours = date.getHours();
      var minutes = date.getMinutes();
      res.data.testShifts.timeEnd = ((hours > 12) ? (hours - 12 + ':' + minutes + ' PM') : (hours + ':' + minutes + ' AM'));
      this.modalDelete.show();
    });
  }

  delete() {
    this.lockModal("del");
    if (this.scheduleTest.id != 0) {
      this.scheduleTestSer.delete(this.scheduleTest.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          this.scheduleTestSer.getAll().subscribe(res1 => {
            this.scheduleTests = res1.data;
            for (let i = 0; i < res1.data.length; i++) {
              var date = new Date("2020-06-22 " + res1.data[i].testShifts.shI_TIMESTART);
              date.setTime(date.getTime() + res1.data[i].tests.teS_TIME * 60000);
              var hours = date.getHours();
              var minutes = date.getMinutes();
              res1.data[i].testShifts.timeEnd = ((hours > 12) ? (hours - 12 + ':' + minutes + ' PM') : (hours + ':' + minutes + ' AM'));
            }
            this.rerender();
          });
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Xóa lịch thi thành công!'
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
        this.modalDetail.hide();
      });
    }
  }

  openModalDeleteMul() {
    this.unlockModal("muldel");
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      if (dtElement.dtInstance != null) {
        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
            let arr = [];
            dtInstance.rows({ selected: true }).every(function () {
              if (this.data()[1] != null) {
                arr.push(this.data()[1]);
              }
            });
            if (arr.length == 0) {
              this.isListItemNull = true;
              this.modalDeletMul.show();
            }
            else {
              this.isListItemNull = false;
              this.scheduleTestSer.getListScheduleTestMultiDel(arr).subscribe(res => {
                this.scheduleTestsMultiDel = res.data;
                for (let i = 0; i < res.data.length; i++) {
                  var date = new Date("2020-06-22 " + res.data[i].testShifts.shI_TIMESTART);
                  date.setTime(date.getTime() + res.data[i].tests.teS_TIME * 60000);
                  var hours = date.getHours();
                  var minutes = date.getMinutes();
                  res.data[i].testShifts.timeEnd = ((hours > 12) ? (hours - 12 + ':' + minutes + ' PM') : (hours + ':' + minutes + ' AM'));
                }
                // console.log(this.subjectsMultiDel);
                this.modalDeletMul.show();
              });
            }
          }
        });
      }
    });
  }

  deleteMul() {
    this.lockModal("muldel");
    this.scheduleTestSer.deleteMulti(this.scheduleTestsMultiDel, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
      if (res.errorCode === 0) {
        this.scheduleTestSer.getAll().subscribe(res1 => {
          this.scheduleTests = res1.data;
          for (let i = 0; i < res1.data.length; i++) {
            var date = new Date("2020-06-22 " + res1.data[i].testShifts.shI_TIMESTART);
            date.setTime(date.getTime() + res1.data[i].tests.teS_TIME * 60000);
            var hours = date.getHours();
            var minutes = date.getMinutes();
            res1.data[i].testShifts.timeEnd = ((hours > 12) ? (hours - 12 + ':' + minutes + ' PM') : (hours + ':' + minutes + ' AM'));
          }
          this.rerender();
          if ($(".toggle-scte-all").closest('tr').hasClass('selected')) {
            $(".toggle-scte-all").closest('tr').toggleClass('selected');
          }
          this.modalDeletMul.hide();
          this.pnotify.success({
            title: 'Thông báo',
            text: "Xóa danh sách lịch thi thành công!"
          });
        });
      }
      else {
        if ($(".toggle-scte-all").closest('tr').hasClass('selected')) {
          $(".toggle-scte-all").closest('tr').toggleClass('selected');
        }
        this.modalDeletMul.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message
        });
      }
    });
  }
}
