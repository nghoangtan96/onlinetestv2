import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { ActivatedRoute, Router } from '@angular/router';
import { ScheduleTest, ScheduleTestService } from '../../services/schedule-test.service';
import { PnotifyService } from '../../services/pnotify.service';
import { SignalrService } from '../../services/signalr.service';
import { Test, TestService } from '../../services/test.service';
import { CountdownModule, CountdownComponent } from 'ngx-countdown';
import { Information, InformationService } from '../../services/information.service';
import { Testtype, TestTypeService } from '../../services/test-type.service';
import { DomSanitizer } from '@angular/platform-browser';
import { TestDetail } from '../../services/test-detail.service';
import { TestQuestion } from '../../services/test-question.service';
import { SubjectService, Subjectt } from '../../services/subject.service';
import { Question, QuestionService } from '../../services/question.service';
import { Option, OptionService } from '../../services/option.service';
import { Passage, PassageService } from '../../services/passage.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ExamRequest } from '../../models/examRequest';
import { interval, Subscription, Observable } from 'rxjs';
import { StudentTestService, StudentTest } from '../../services/student-test.service';
import { Track } from 'ngx-audio-player';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.css']
})

export class ExamComponent implements OnInit {

  userId: number = Number(this.cookieSer.get('Id'));
  scheduleTest_Id: number;
  scheduleTests: [ScheduleTest];
  scheduleTest: ScheduleTest;
  test: Test;
  pnotify = undefined;
  informations: [Information];
  testCode: string;
  testDetails: [TestDetail];
  testQuestionsView: TestQuestion[] = [];
  subject: Subjectt;
  questions: [Question];
  options: [Option];
  passages: [Passage];
  studentTest: StudentTest;
  showScore: boolean;
  countQuestionDone: number = 0;
  partKey = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', 'XIII', 'XIV', 'XV'];
  optionKey = ['A', 'B', 'C', 'D', 'E', 'F'];
  videoExtension = ['mp4', 'webm', 'ogg'];
  audioExtension = ['mpeg', 'wav', 'm4a', 'mp3'];
  mediaType: string;
  testFormat: number;
  isEnd: boolean = false;
  examRequest: ExamRequest = {} as ExamRequest;
  isSubmit: boolean = false;

  msaapPlaylist: Track[] = [];

  listQuestion: string = "";
  listOption: string = "";

  //information
  organizationName: string;
  logoUrl: any;
  testtype: Testtype;

  //auto save
  subscription: Subscription;

  //question load count
  loadCount: number = 0;
  typeOfTes: number;
  partCount: number = 0;
  listPassQuestion: number[] = [];
  audio: any;
  video: any;

  timeCount: number;
  timeRemainingExam: number = 10;
  timeLeft: number;
  subscriptionCountdown: Subscription;

  //toeic
  currentPart: number;
  currentTimeAudio: number;

  @ViewChild('cd', { static: false }) private countdown: CountdownComponent;
  @ViewChild('modalConfirmBox', { static: false }) modalConfirmBox: ModalDirective;
  @HostListener('window:beforeunload', ['$event'])
  unloadHandler(event: any) {
    this.route.queryParams.subscribe(params => {
      // if (this.test.testingFormats[0].tesF_ISSHOWPART == true) {
      //   this.examRequest = {
      //     userId: Number(this.cookieSer.get('Id')),
      //     scheduleTestId: params['scheduleTestId'],
      //     listQuestion: this.listQuestion,
      //     listOption: this.listOption,
      //     remainingTime: this.timeLeft,
      //     currentPart: this.currentPart,
      //     currentTimeAudio: this.audio.currentTime
      //   } as ExamRequest;
      // }
      // else {
      this.examRequest = {
        userId: Number(this.cookieSer.get('Id')),
        scheduleTestId: params['scheduleTestId'],
        listQuestion: this.listQuestion,
        listOption: this.listOption,
        remainingTime: this.timeLeft
      } as ExamRequest;
      // }
      // console.log(this.examRequest);
      this.studentTestSer.updateExam(this.examRequest).subscribe(res => { });
    });
  }

  constructor(private cookieSer: CookieService, private route: ActivatedRoute,
    private scheduleTestSer: ScheduleTestService, private pNotify: PnotifyService,
    private signalRSer: SignalrService, private testSer: TestService,
    private router: Router, private informationSer: InformationService,
    private testTypeSer: TestTypeService, private _DomSanitizer: DomSanitizer,
    private subjectSer: SubjectService, private questionSer: QuestionService,
    private passageSer: PassageService, private optionSer: OptionService,
    private studentTestSer: StudentTestService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.scheduleTest_Id = params['scheduleTestId'];

      this.studentTestSer.getByScheduleTestIdAndStudentId(params['scheduleTestId'], Number(this.cookieSer.get('Id'))).subscribe(res3 => {
        this.studentTest = res3.data;

        if (res3.data.sttE_STATUS == 2) {
          this.scheduleTestSer.get(this.scheduleTest_Id).subscribe(res => {
            this.timeRemainingExam = res3.data.sttE_REMAININGTIME;
            this.timeLeft = res3.data.sttE_REMAININGTIME;
            // count timeRemaining
            const timePer = interval(1000);
            this.subscriptionCountdown = timePer.subscribe(val => this.countTimeLeft());
            // auto save
            const source = interval(300000);
            this.subscription = source.subscribe(val => this.updateTest());

            this.scheduleTest = res.data;

            this.subjectSer.get(res.data.tests.teS_SUBID).subscribe(res5 => {
              this.subject = res5.data;
              this.questionSer.getListBySubjectIdForExam(res5.data.id).subscribe(res2 => {
                this.questions = res2.data;
                // console.log(res2.data);
                this.passageSer.getListBySubjectId(res5.data.id).subscribe(res7 => {
                  this.passages = res7.data

                  this.testSer.get(this.scheduleTest.sctE_TESID).subscribe(res1 => {
                    this.test = res1.data;
                    // console.log(res1.data);
                    this.showScore = res1.data.testingFormats[0].tesF_ISSHOWSCORE;

                    if (res1.data.teS_ISSHUFFLE == false) {
                      if (res1.data.testingFormats[0].tesF_ISSHOWPART == true) {
                        this.testQuestionsView = res1.data.test_Questions;
                        let step = 1;
                        let count;
                        this.subjectSer.get(res1.data.teS_SUBID).subscribe(res6 => {
                          for (let i = 0; i < this.subject.parts.length; i++) {
                            count = 0;
                            for (let j = 0; j < this.testQuestionsView.length; j++) {
                              if (this.testQuestionsView[j].teqU_QUEID != null && this.testQuestionsView[j].questions.quE_PARID == this.subject.parts[i].id) {
                                this.testQuestionsView[j].questions.index = step;
                                step++;
                                count++;
                              }
                              else if (this.testQuestionsView[j].teqU_PASID != null && this.testQuestionsView[j].passages.paS_PARID == this.subject.parts[i].id) {
                                for (let k = 0; k < this.testQuestionsView[j].passages.questions.length; k++) {
                                  this.testQuestionsView[j].passages.questions[k].index = step;
                                  step++;
                                  count++;
                                }
                              }
                            }
                            this.subject.parts[i].noquestion = count;
                          }
                        });
                        this.typeOfTes = 2;
                        let countPart = 0;
                        // console.log(this.testQuestionsView);
                        // for (let i = 0; i < this.subject.parts.length; i++) {
                        //   let check = false;
                        //   for (let j = 0; j < this.testQuestionsView.length; j++) {
                        //     if (this.testQuestionsView[j].teqU_QUEID != null) {
                        //       if (this.testQuestionsView[j].questions.quE_PARID == this.subject.parts[i].id) {
                        //         this.loadCount = j;
                        //         check = true;
                        //         break;
                        //       }
                        //     }
                        //     else {
                        //       if (this.testQuestionsView[j].passages.paS_PARID == this.subject.parts[i].id) {
                        //         this.loadCount = j;
                        //         check = true;
                        //         break;
                        //       }
                        //     }
                        //   }
                        //   if (check == true)
                        //     break;
                        // }

                      }
                      else {
                        this.testQuestionsView = res1.data.test_Questions;
                        // console.log(this.testQuestionsView);
                        let step = 1;
                        for (let i = 0; i < this.testQuestionsView.length; i++) {
                          if (this.testQuestionsView[i].teqU_QUEID != null) {
                            this.testQuestionsView[i].questions.index = step;
                            step++;

                            //media
                            if (this.testQuestionsView[i].questions.quE_MEDIA != null) {
                              if ($.inArray(this.testQuestionsView[i].questions.quE_MEDIA.substr(this.testQuestionsView[i].questions.quE_MEDIA.length - 3, this.testQuestionsView[i].questions.quE_MEDIA.length), this.videoExtension) != -1) {
                                this.testQuestionsView[i].questions.mediaType = "video";
                              }
                              else {
                                this.testQuestionsView[i].questions.mediaType = "audio";
                              }
                            }
                          }
                          if (this.testQuestionsView[i].teqU_PASID != null) {
                            for (let j = 0; j < this.testQuestionsView[i].passages.questions.length; j++) {
                              this.testQuestionsView[i].passages.questions[j].index = step;
                              step++;

                              //media question
                              if (this.testQuestionsView[i].passages.questions[j].quE_MEDIA != null) {
                                if ($.inArray(this.testQuestionsView[i].passages.questions[j].quE_MEDIA.substr(this.testQuestionsView[i].passages.questions[j].quE_MEDIA.length - 3, this.testQuestionsView[i].passages.questions[j].quE_MEDIA.length), this.videoExtension) != -1) {
                                  this.testQuestionsView[i].passages.questions[j].mediaType = "video";
                                }
                                else {
                                  this.testQuestionsView[i].passages.questions[j].mediaType = "audio";
                                }
                              }
                            }

                            //media passage
                            if (this.testQuestionsView[i].passages.paS_MEDIA != null) {
                              if ($.inArray(this.testQuestionsView[i].passages.paS_MEDIA.substr(this.testQuestionsView[i].passages.paS_MEDIA.length - 3, this.testQuestionsView[i].passages.paS_MEDIA.length), this.videoExtension) != -1) {
                                this.testQuestionsView[i].passages.mediaType = "video";
                              }
                              else {
                                this.testQuestionsView[i].passages.mediaType = "audio";
                              }
                            }
                          }
                        }
                        this.typeOfTes = 1;
                      }
                    }
                    //test shuffle
                    else {
                      this.testCode = this.scheduleTest.student_Tests.find(x => x.students.stU_USEID == Number(this.cookieSer.get('Id'))).sttE_TESTCODE;
                      this.testQuestionsView = [];
                      let step = 1;
                      for (let i = 0; i < res1.data.test_Details.length; i++) {
                        if (res1.data.test_Details[i].tedE_CODE == this.testCode) {
                          // console.log(res1.data.test_Details[i]);
                          // console.log(this.questions);
                          if (res1.data.test_Details[i].tedE_QUEID != null) {
                            let listOption = [];
                            for (let j = 0; j < res1.data.test_Details[i].tedE_QUEOPTION.split("-").length; j++) {
                              listOption.push(this.questions.find(x => x.id == res1.data.test_Details[i].tedE_QUEID).options.find(x => x.id == Number(res1.data.test_Details[i].tedE_QUEOPTION.split("-")[j])));
                            }
                            let question = this.questions.find(x => x.id == res1.data.test_Details[i].tedE_QUEID);
                            question.options = listOption as [Option];
                            question.index = step;
                            step++;

                            //media
                            if (question.quE_MEDIA != null) {
                              if ($.inArray(question.quE_MEDIA.substr(question.quE_MEDIA.length - 3, question.quE_MEDIA.length), this.videoExtension) != -1) {
                                question.mediaType = "video";
                              }
                              else {
                                question.mediaType = "audio";
                              }
                            }

                            this.testQuestionsView.push({ teqU_QUEID: res1.data.test_Details[i].tedE_QUEID, questions: question } as TestQuestion);
                          }
                          else {
                            let listQuestion = [];
                            for (let j = 0; j < res1.data.test_Details[i].tedE_PASQUESTION.split('.').length; j++) {
                              let listOption = [];
                              for (let k = 0; k < res1.data.test_Details[i].tedE_PASOPTION.split('.')[j].split("-").length; k++) {
                                listOption.push(this.passages.find(x => x.id == res1.data.test_Details[i].tedE_PASID).questions.find(x => x.id == Number(res1.data.test_Details[i].tedE_PASQUESTION.split('.')[j])).options.find(x => x.id == Number(res1.data.test_Details[i].tedE_PASOPTION.split('.')[j].split('-')[k])));
                              }
                              let question = this.passages.find(x => x.id == res1.data.test_Details[i].tedE_PASID).questions.find(x => x.id == Number(res1.data.test_Details[i].tedE_PASQUESTION.split('.')[j]));
                              question.options = listOption as [Option];
                              question.index = step;
                              step++;

                              //media question
                              if (question.quE_MEDIA != null) {
                                if ($.inArray(question.quE_MEDIA.substr(question.quE_MEDIA.length - 3, question.quE_MEDIA.length), this.videoExtension) != -1) {
                                  question.mediaType = "video";
                                }
                                else {
                                  question.mediaType = "audio";
                                }
                              }

                              listQuestion.push(question);
                            }
                            let passage = this.passages.find(x => x.id == res1.data.test_Details[i].tedE_PASID);
                            passage.questions = listQuestion as [Question];

                            //media passage
                            if (passage.paS_MEDIA != null) {
                              if ($.inArray(passage.paS_MEDIA.substr(passage.paS_MEDIA.length - 3, passage.paS_MEDIA.length), this.videoExtension) != -1) {
                                passage.mediaType = "video";
                              }
                              else {
                                passage.mediaType = "audio";
                              }
                            }

                            this.testQuestionsView.push({ teqU_PASID: res1.data.test_Details[i].tedE_PASID, passages: passage } as TestQuestion);
                          }
                        }
                        // console.log(this.testQuestionsView);
                      }
                      this.typeOfTes = 1;
                    }
                    this.timeCount = null;

                    //option is done
                    let allDone = true;
                    let count = 0;
                    let check = false;
                    this.listOption = res3.data.sttE_OPTION;
                    this.listQuestion = res3.data.sttE_QUESTION;
                    for (let i = 0; i < res3.data.sttE_QUESTION.split('-').length; i++) {
                      if (res3.data.sttE_OPTION.split('-')[i] == "0") {
                        allDone = false;
                        if (i == 0) {
                          if (check == false) {
                            this.loadCount = i;
                            if (this.testQuestionsView[this.loadCount].teqU_QUEID != null) {
                              this.currentPart = this.testQuestionsView[this.loadCount].questions.quE_PARID;
                            }
                            else {
                              this.currentPart = this.testQuestionsView[this.loadCount].passages.paS_PARID;
                            }
                            check = true;
                          }
                        }
                        else {
                          if (check == false) {
                            if (this.testQuestionsView.find(x => x.teqU_QUEID == Number(res3.data.sttE_QUESTION.split('-')[i])) != null) {
                              this.loadCount = this.testQuestionsView.indexOf(this.testQuestionsView.find(x => x.teqU_QUEID == Number(res3.data.sttE_QUESTION.split('-')[i]))) - 1;
                              this.currentPart = this.testQuestionsView[(this.loadCount < 0 ? 0 : this.loadCount)].questions != null ? this.testQuestionsView[(this.loadCount < 0 ? 0 : this.loadCount)].questions.quE_PARID : this.testQuestionsView[(this.loadCount < 0 ? 0 : this.loadCount)].passages.paS_PARID;
                            }
                            else {
                              for (let j = 0; j < this.testQuestionsView.length; j++) {
                                if (this.testQuestionsView[j].passages != null && this.testQuestionsView[j].passages.questions.find(x => x.id == Number(res3.data.sttE_QUESTION.split('-')[i])) != null) {
                                  this.loadCount = this.testQuestionsView.indexOf(this.testQuestionsView[j]) - 1;
                                  this.currentPart = this.testQuestionsView[(this.loadCount < 0 ? 0 : this.loadCount)].passages != null ? this.testQuestionsView[(this.loadCount < 0 ? 0 : this.loadCount)].passages.paS_PARID : this.testQuestionsView[(this.loadCount < 0 ? 0 : this.loadCount)].questions.quE_PARID;
                                }
                              }
                            }
                            if (this.loadCount < 0)
                              this.loadCount = 0;
                            check = true;
                          }
                        }
                        // break;
                        // console.log(this.loadCount);
                      }
                      else {
                        count++;
                        if (this.testQuestionsView.find(x => x.teqU_QUEID == Number(res3.data.sttE_QUESTION.split("-")[i])) != null) {
                          this.testQuestionsView.find(x => x.teqU_QUEID == Number(res3.data.sttE_QUESTION.split("-")[i])).questions.isDone = true;
                          for (let j = 0; j < res3.data.sttE_OPTION.split("-")[i].split(".").length; j++) {
                            if (res3.data.sttE_OPTION.split("-")[i].split(".")[j] != null) {
                              this.testQuestionsView.find(x => x.teqU_QUEID == Number(res3.data.sttE_QUESTION.split("-")[i])).questions.options.find(x => x.id == Number(res3.data.sttE_OPTION.split("-")[i].split(".")[j])).isChoose = true;
                            }
                          }
                        }
                        else {
                          for (let j = 0; j < this.testQuestionsView.length; j++) {
                            if (this.testQuestionsView[j].passages != null && this.testQuestionsView[j].passages.questions.find(x => x.id == Number(res3.data.sttE_QUESTION.split("-")[i])) != null) {
                              this.testQuestionsView[j].passages.questions.find(x => x.id == Number(res3.data.sttE_QUESTION.split("-")[i])).isDone = true;
                              for (let k = 0; k < res3.data.sttE_OPTION.split("-")[i].split(".").length; k++) {
                                this.testQuestionsView[j].passages.questions.find(x => x.id == Number(res3.data.sttE_QUESTION.split("-")[i])).options.find(x => x.id == Number(res3.data.sttE_OPTION.split("-")[i].split(".")[k])).isChoose = true;
                              }
                            }
                          }
                        }
                        // $("#que_icon_" + res3.data.sttE_QUESTION.split("-")[i]).html('<i class="fa fa-check-square-o text-success"></i>');
                      }
                    }
                    this.countQuestionDone = count;
                    if (allDone == true) {
                      // this.loadCount = res3.data.sttE_QUESTION.split('-').length - 1;
                      this.loadCount = this.testQuestionsView.length - 1;
                      for (let j = 0; j < res3.data.sttE_OPTION.split("-")[res3.data.sttE_QUESTION.split('-').length - 1].split(".").length; j++) {
                        $("#option_que_" + res3.data.sttE_OPTION.split("-")[res3.data.sttE_QUESTION.split('-').length - 1].split(".")[j]).prop("checked", true);
                      }
                      $("#que_icon_" + res3.data.sttE_QUESTION.split("-")[res3.data.sttE_QUESTION.split('-').length - 1]).html('<i class="fa fa-check-square-o text-success"></i>');
                    }

                    // console.log(this.testQuestionsView[this.loadCount]);
                    // console.log(this.testQuestionsView);
                    // console.log(this.loadCount);
                    if (this.testQuestionsView[this.loadCount].teqU_PASID != null && this.testQuestionsView[this.loadCount].passages.paS_MEDIA != null) {
                      if ($.inArray(this.testQuestionsView[this.loadCount].passages.paS_MEDIA.substr(this.testQuestionsView[this.loadCount].passages.paS_MEDIA.length - 3, this.testQuestionsView[this.loadCount].passages.paS_MEDIA.length), this.videoExtension) != -1) {
                        this.mediaType = "video";
                      }
                      else {
                        this.mediaType = "audio";
                        this.audio = new Audio();
                        this.audio.src = this.testQuestionsView[this.loadCount].passages.fileUrl;
                        this.audio.load();
                        // this.audio.muted = true;
                        // this.audio.currentTime = res3.data.sttE_CURRENTTIMEAUDIO;
                        // this.audio.play();
                        // this.audio.muted = false;
                        const playPromise = this.audio.play();
                        // if (playPromise !== null) {
                        //   playPromise.catch(() => { this.audio.play(); })
                        // }
                        if (playPromise) {
                          //Older browsers may not return a promise, according to the MDN website
                          playPromise.catch(function (error) { console.error(error); });
                        }
                      }
                    }
                  });
                });
              });
            });

          });
        }
        else if (res3.data.sttE_STATUS == 3) {
          this.scheduleTestSer.get(this.scheduleTest_Id).subscribe(res => {
            this.scheduleTest = res.data;
            this.isEnd = true;
            this.timeRemainingExam = 0;
          });
        }
        else {
          this.scheduleTestSer.get(this.scheduleTest_Id).subscribe(res => {
            if (res.data.student_Tests.find(x => x.students.stU_USEID == Number(this.cookieSer.get('Id'))) == null) {
              this.router.navigate(['/404']);
            }

            let date = new Date();
            let dateString = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
            let testDate = new Date(res.data.sctE_DATE);
            if (!(testDate.getFullYear() == date.getFullYear() && testDate.getMonth() == date.getMonth() && testDate.getDate() == date.getDate())) {
              this.router.navigate(['/404']);
            }

            var dateUnlock = new Date(dateString + " " + res.data.testShifts.shI_TIMESTART);
            dateUnlock.setTime(dateUnlock.getTime() - 15 * 60000);
            var dateStart = new Date(dateString + " " + res.data.testShifts.shI_TIMESTART);
            var dateEnd = new Date(dateString + " " + res.data.testShifts.shI_TIMESTART);
            dateEnd.setTime(dateEnd.getTime() + res.data.tests.teS_TIME * 60000);

            if (date.getHours() >= dateUnlock.getHours() && date.getHours() <= dateEnd.getHours()) {
              if (date.getHours() == dateUnlock.getHours() && date.getMinutes() < dateUnlock.getMinutes()) {
                this.router.navigate(['/404']);
              }
              else if (date.getHours() == dateEnd.getHours() && date.getMinutes() > dateEnd.getMinutes()) {
                this.router.navigate(['/404']);
              }
              else {
                this.scheduleTest = res.data;
                if (date.getHours() < dateStart.getHours() ||
                  (date.getHours() == dateStart.getHours() && date.getMinutes() < dateStart.getMinutes()) ||
                  (date.getHours() == dateStart.getHours() && date.getMinutes() == dateStart.getMinutes() && date.getSeconds() < dateStart.getSeconds())) {
                  this.timeCount = (dateStart.getTime() - date.getTime()) / 1000;
                  this.timeRemainingExam = null;
                }
                else {
                  this.testSer.get(this.scheduleTest.sctE_TESID).subscribe(res1 => {
                    this.test = res1.data;
                    this.timeRemainingExam = (dateEnd.getTime() - date.getTime()) / 1000;
                    this.timeCount = null;

                    if (res.data.student_Tests.find(x => x.students.stU_USEID == Number(this.cookieSer.get('Id'))).sttE_STATUS != null) {
                      let listQuestion = res.data.student_Tests.find(x => x.students.stU_USEID == Number(this.cookieSer.get('Id'))).sttE_QUESTION;
                      let listOption = res.data.student_Tests.find(x => x.students.stU_USEID == Number(this.cookieSer.get('Id'))).sttE_OPTION;
                      for (let i = 0; i < listQuestion.split('-').length; i++) {
                        if (listOption.split('-')[i] != "0") {
                          $("#option_que_" + listOption.split('-')[i]).prop('checked', true);
                          this.checkOption(Number(listQuestion.split('-')[i]), Number(listOption.split('-')[i]));
                        }
                      }
                    }

                  });
                }

                // this.timeCount = 5; // test

                this.subjectSer.get(res.data.tests.teS_SUBID).subscribe(res1 => {
                  this.subject = res1.data;
                  this.questionSer.getListBySubjectIdForExam(res1.data.id).subscribe(res2 => {
                    this.questions = res2.data;
                    // console.log(res2.data);
                  });
                  this.passageSer.getListBySubjectId(res1.data.id).subscribe(res2 => {
                    this.passages = res2.data
                  });
                });
              }
            }
            else {
              this.router.navigate(['/404']);
            }
          });
        }
      });
    });

    this.informationSer.getAll().subscribe(res => {
      this.informations = res.data;
      this.organizationName = res.data.find(x => x.inF_KEYNAME == "Tên tổ chức").inF_VALUE;
      this.logoUrl = res.data.find(x => x.inF_KEYNAME == "Logo").fileUrl;
    });

  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.subscriptionCountdown) {
      this.subscriptionCountdown.unsubscribe();
    }
  }

  stopMedia() {
    $(".exam_media").trigger("pause");
    $(".exam_media").prop("currentTime", 0);
  }

  loadTest($event) {
    if ($event["action"] == "done") {
      this.testSer.get(this.scheduleTest.sctE_TESID).subscribe(res => {
        this.test = res.data;
        this.showScore = res.data.testingFormats[0].tesF_ISSHOWSCORE;
        // console.log(res.data);

        if (res.data.teS_ISSHUFFLE == false) {
          if (res.data.testingFormats[0].tesF_ISSHOWPART == true) {
            this.testQuestionsView = res.data.test_Questions;
            let step = 1;
            let count;
            for (let i = 0; i < this.subject.parts.length; i++) {
              count = 0;
              for (let j = 0; j < this.testQuestionsView.length; j++) {
                if (this.testQuestionsView[j].teqU_QUEID != null && this.testQuestionsView[j].questions.quE_PARID == this.subject.parts[i].id) {
                  this.testQuestionsView[j].questions.index = step;
                  step++;
                  count++;
                }
                else if (this.testQuestionsView[j].teqU_PASID != null && this.testQuestionsView[j].passages.paS_PARID == this.subject.parts[i].id) {
                  for (let k = 0; k < this.testQuestionsView[j].passages.questions.length; k++) {
                    this.testQuestionsView[j].passages.questions[k].index = step;
                    step++;
                    count++;
                  }
                }
              }
              this.subject.parts[i].noquestion = count;
            }
            this.typeOfTes = 2;
            // console.log(this.testQuestionsView);
            for (let i = 0; i < this.subject.parts.length; i++) {
              let check = false;
              for (let j = 0; j < this.testQuestionsView.length; j++) {
                if (this.testQuestionsView[j].teqU_QUEID != null) {
                  if (this.testQuestionsView[j].questions.quE_PARID == this.subject.parts[i].id) {
                    this.loadCount = j;
                    this.partCount = i;
                    this.currentPart = i;
                    check = true;

                    // // add media
                    // if (this.testQuestionsView[j].questions.quE_MEDIA != null && $.inArray(this.testQuestionsView[j].questions.quE_MEDIA.substr(this.testQuestionsView[j].questions.quE_MEDIA.length - 3, this.testQuestionsView[j].questions.quE_MEDIA.length), this.audioExtension) != -1) {
                    //   this.msaapPlaylist.push({ link: this.testQuestionsView[j].questions.fileUrl } as Track);
                    // }

                    break;
                  }
                }
                else {
                  if (this.testQuestionsView[j].passages.paS_PARID == this.subject.parts[i].id) {
                    this.loadCount = j;
                    this.partCount = i;
                    this.currentPart = i;
                    check = true;

                    // //add media
                    // if (this.testQuestionsView[j].passages.paS_MEDIA != null && $.inArray(this.testQuestionsView[j].passages.paS_MEDIA.substr(this.testQuestionsView[j].passages.paS_MEDIA.length - 3, this.testQuestionsView[j].passages.paS_MEDIA.length), this.audioExtension) != -1) {
                    //   this.msaapPlaylist.push({ link: this.testQuestionsView[j].passages.fileUrl } as Track);
                    // }

                    break;
                  }
                }
              }
              if (check == true)
                break;
            }

            if (this.testQuestionsView[this.loadCount].teqU_PASID != null && this.testQuestionsView[this.loadCount].passages.paS_MEDIA != null) {
              if ($.inArray(this.testQuestionsView[this.loadCount].passages.paS_MEDIA.substr(this.testQuestionsView[this.loadCount].passages.paS_MEDIA.length - 3, this.testQuestionsView[this.loadCount].passages.paS_MEDIA.length), this.videoExtension) != -1) {
                this.mediaType = "video";
              }
              else {
                this.mediaType = "audio";
                this.audio = new Audio();
                this.audio.src = this.testQuestionsView[this.loadCount].passages.fileUrl;
                this.audio.load();
                this.audio.play();
              }
            }

          }
          else {
            this.testQuestionsView = res.data.test_Questions;
            // console.log(this.testQuestionsView);
            let step = 1;
            for (let i = 0; i < this.testQuestionsView.length; i++) {
              if (this.testQuestionsView[i].teqU_QUEID != null) {
                this.testQuestionsView[i].questions.index = step;
                step++;

                //media
                if (this.testQuestionsView[i].questions.quE_MEDIA != null) {
                  if ($.inArray(this.testQuestionsView[i].questions.quE_MEDIA.substr(this.testQuestionsView[i].questions.quE_MEDIA.length - 3, this.testQuestionsView[i].questions.quE_MEDIA.length), this.videoExtension) != -1) {
                    this.testQuestionsView[i].questions.mediaType = "video";
                  }
                  else {
                    this.testQuestionsView[i].questions.mediaType = "audio";
                  }
                }
              }
              if (this.testQuestionsView[i].teqU_PASID != null) {
                for (let j = 0; j < this.testQuestionsView[i].passages.questions.length; j++) {
                  this.testQuestionsView[i].passages.questions[j].index = step;
                  step++;

                  //media question
                  if (this.testQuestionsView[i].passages.questions[j].quE_MEDIA != null) {
                    if ($.inArray(this.testQuestionsView[i].passages.questions[j].quE_MEDIA.substr(this.testQuestionsView[i].passages.questions[j].quE_MEDIA.length - 3, this.testQuestionsView[i].passages.questions[j].quE_MEDIA.length), this.videoExtension) != -1) {
                      this.testQuestionsView[i].passages.questions[j].mediaType = "video";
                    }
                    else {
                      this.testQuestionsView[i].passages.questions[j].mediaType = "audio";
                    }
                  }
                }

                //media passage
                if (this.testQuestionsView[i].passages.paS_MEDIA != null) {
                  if ($.inArray(this.testQuestionsView[i].passages.paS_MEDIA.substr(this.testQuestionsView[i].passages.paS_MEDIA.length - 3, this.testQuestionsView[i].passages.paS_MEDIA.length), this.videoExtension) != -1) {
                    this.testQuestionsView[i].passages.mediaType = "video";
                  }
                  else {
                    this.testQuestionsView[i].passages.mediaType = "audio";
                  }
                }
              }
            }
            this.typeOfTes = 1;
          }
        }
        else {
          this.testCode = this.scheduleTest.student_Tests.find(x => x.students.stU_USEID == Number(this.cookieSer.get('Id'))).sttE_TESTCODE;
          this.testQuestionsView = [];
          let step = 1;
          for (let i = 0; i < res.data.test_Details.length; i++) {
            if (res.data.test_Details[i].tedE_CODE == this.testCode) {
              if (res.data.test_Details[i].tedE_QUEID != null) {
                let listOption = [];
                for (let j = 0; j < res.data.test_Details[i].tedE_QUEOPTION.split("-").length; j++) {
                  listOption.push(this.questions.find(x => x.id == res.data.test_Details[i].tedE_QUEID).options.find(x => x.id == Number(res.data.test_Details[i].tedE_QUEOPTION.split("-")[j])));
                }
                let question = this.questions.find(x => x.id == res.data.test_Details[i].tedE_QUEID);
                question.options = listOption as [Option];
                question.index = step;
                step++;

                //media
                if (question.quE_MEDIA != null) {
                  if ($.inArray(question.quE_MEDIA.substr(question.quE_MEDIA.length - 3, question.quE_MEDIA.length), this.videoExtension) != -1) {
                    question.mediaType = "video";
                  }
                  else {
                    question.mediaType = "audio";
                  }
                }

                this.testQuestionsView.push({ teqU_QUEID: res.data.test_Details[i].tedE_QUEID, questions: question } as TestQuestion);
              }
              else {
                let listQuestion = [];
                for (let j = 0; j < res.data.test_Details[i].tedE_PASQUESTION.split('.').length; j++) {
                  let listOption = [];
                  for (let k = 0; k < res.data.test_Details[i].tedE_PASOPTION.split('.')[j].split("-").length; k++) {
                    listOption.push(this.passages.find(x => x.id == res.data.test_Details[i].tedE_PASID).questions.find(x => x.id == Number(res.data.test_Details[i].tedE_PASQUESTION.split('.')[j])).options.find(x => x.id == Number(res.data.test_Details[i].tedE_PASOPTION.split('.')[j].split('-')[k])));
                  }
                  let question = this.passages.find(x => x.id == res.data.test_Details[i].tedE_PASID).questions.find(x => x.id == Number(res.data.test_Details[i].tedE_PASQUESTION.split('.')[j]));
                  question.options = listOption as [Option];
                  question.index = step;
                  step++;

                  //media question
                  if (question.quE_MEDIA != null) {
                    if ($.inArray(question.quE_MEDIA.substr(question.quE_MEDIA.length - 3, question.quE_MEDIA.length), this.videoExtension) != -1) {
                      question.mediaType = "video";
                    }
                    else {
                      question.mediaType = "audio";
                    }
                  }

                  listQuestion.push(question);
                }
                let passage = this.passages.find(x => x.id == res.data.test_Details[i].tedE_PASID);
                passage.questions = listQuestion as [Question];

                //media passage
                if (passage.paS_MEDIA != null) {
                  if ($.inArray(passage.paS_MEDIA.substr(passage.paS_MEDIA.length - 3, passage.paS_MEDIA.length), this.videoExtension) != -1) {
                    passage.mediaType = "video";
                  }
                  else {
                    passage.mediaType = "audio";
                  }
                }

                this.testQuestionsView.push({ teqU_PASID: res.data.test_Details[i].tedE_PASID, passages: passage } as TestQuestion);
              }
            }
          }
          this.typeOfTes = 1;
        }
        // console.log(this.testQuestionsView);
        for (let i = 0; i < this.testQuestionsView.length; i++) {
          if (this.testQuestionsView[i].teqU_QUEID != null) {
            this.listQuestion += this.testQuestionsView[i].teqU_QUEID;
            this.listOption += "0";
          }
          else {
            for (let j = 0; j < this.testQuestionsView[i].passages.questions.length; j++) {
              this.listQuestion += this.testQuestionsView[i].passages.questions[j].id;
              this.listOption += "0";
              if (j != this.testQuestionsView[i].passages.questions.length - 1) {
                this.listOption += "-";
                this.listQuestion += "-";
              }
            }
          }

          if (i != this.testQuestionsView.length - 1) {
            this.listOption += "-";
            this.listQuestion += "-";
          }
        }

        let date = new Date();
        let dateString = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
        var dateEnd = new Date(dateString + " " + this.scheduleTest.testShifts.shI_TIMESTART);
        dateEnd.setTime(dateEnd.getTime() + res.data.teS_TIME * 60000);

        this.timeRemainingExam = (dateEnd.getTime() - date.getTime()) / 1000;
        // this.timeRemainingExam = 1800; // test
        this.timeCount = null;
        this.timeLeft = this.timeRemainingExam;

        //auto save
        const source = interval(300000);
        this.subscription = source.subscribe(val => this.updateTest());

        const timePer = interval(1000);
        this.subscriptionCountdown = timePer.subscribe(val => this.countTimeLeft());

        this.examRequest = {
          userId: Number(this.cookieSer.get("Id")),
          scheduleTestId: this.scheduleTest.id,
          listOption: this.listOption,
          listQuestion: this.listQuestion
        } as ExamRequest;
        this.studentTestSer.doingExam(this.examRequest).subscribe();

        // console.log(this.currentPart);
      });
    }
  }

  autoSubmit($event) {
    this.timeLeft = $event.left;
    if ($event["action"] == "done") {
      this.examRequest = {
        type: 1,
        userId: Number(this.cookieSer.get('Id')),
        scheduleTestId: this.scheduleTest.id, // bug
        listQuestion: this.listQuestion,
        listOption: this.listOption
      } as ExamRequest;
      // console.log(this.examRequest);

      this.test = null;
      // this.timeRemainingExam = 0;
      this.isEnd = true;

      if (this.isSubmit != true) {
        this.studentTestSer.testSubmit(this.examRequest).subscribe(res => {
          if (res.errorCode === 0) {
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Nộp bài thi thành công!'
            });
          }
        });
      }

      // this.subscription.unsubscribe() // test
    }
  }

  openConfirmSubmit() {
    this.unlockModal("confirm");
    // console.log(this.timeRemainingExam);
    let hour = Math.floor(this.timeRemainingExam / 3600);
    let minute = Math.floor((this.timeRemainingExam % 3600) / 60);
    let second = Math.floor((this.timeRemainingExam % 3600) % 60);
    $("#confirmText").html('<i class="fa fa-warning text-warning"></i> Vẫn còn khoảng <span class="text-danger">' +
      (hour == 0 ? "" : hour + " giờ ") + (minute == 0 ? "" : minute + " phút ") + (second == 0 ? "" : second + " giây ") +
      '</span> thời gian làm bài' +
      (this.countQuestionDone != this.test.countQuestion ? (" và <span class='text-danger'>" + (this.test.countQuestion - this.countQuestionDone) + " câu hỏi</span> chưa làm") : "") +
      '. Bạn chắc chắn muốn nộp bài thi!');
    this.modalConfirmBox.show();
  }

  submit() {
    this.lockModal("confirm");
    this.isSubmit = true;
    this.examRequest = {
      type: 1,
      userId: Number(this.cookieSer.get('Id')),
      scheduleTestId: this.scheduleTest.id,
      listQuestion: this.listQuestion,
      listOption: this.listOption
    } as ExamRequest;
    // console.log(this.examRequest);

    this.test = null;
    this.timeRemainingExam = 0;
    this.isEnd = true;

    this.studentTestSer.testSubmit(this.examRequest).subscribe(res => {
      if (res.errorCode === 0) {
        this.pnotify.success({
          title: 'Thông báo',
          text: 'Nộp bài thi thành công!'
        });
      }
    });
    this.modalConfirmBox.hide();
  }

  showQuestion(type: number, id: number, event = null) {
    if (event != null) {
      event.preventDefault();
    }
    if (type == 1) {
      location.hash = "#que" + id;
    }
    if (type == 2) {
      location.hash = "#pas" + id;
    }
    if (type == 3) {
      location.hash = "#pasque" + id;
    }
  }

  checkOption(queid: number, optid: number) {
    const item = this.listQuestion.split("-").find(x => Number(x) == queid);
    const index = this.listQuestion.split("-").indexOf(item);

    if ($("#option_que_" + optid).is(":checked") == true) {
      if (this.questions.find(x => x.id == queid).quE_ANTYID != 2) {
        let arrOption = this.listOption.split("-");
        arrOption[index] = optid.toString();

        this.listOption = arrOption.join("-");

        //check mark
        $("#que_icon_" + queid).html('<i class="fa fa-check-square-o text-success"></i>');
        let count = 0;
        for (let i = 0; i < arrOption.length; i++) {
          if (arrOption[i] != "0")
            count++;
        }
        this.countQuestionDone = count;
      }
      else {
        let arrOption = this.listOption.split("-");
        let option = "";
        for (let i = 0; i < this.questions.find(x => x.id == queid).options.length; i++) {
          if ($("#option_que_" + this.questions.find(x => x.id == queid).options[i].id).is(":checked") == true) {
            if (option != "")
              option += ".";
            option += this.questions.find(x => x.id == queid).options[i].id;
          }
        }
        arrOption[index] = option;

        this.listOption = arrOption.join("-");

        //check mark
        $("#que_icon_" + queid).html('<i class="fa fa-check-square-o text-success"></i>');
        let count = 0;
        for (let i = 0; i < arrOption.length; i++) {
          if (arrOption[i] != "0")
            count++;
        }
        this.countQuestionDone = count;
      }
    }
    else {
      if (this.questions.find(x => x.id == queid).quE_ANTYID == 2) {
        let arrOption = this.listOption.split("-");
        let option = "";
        for (let i = 0; i < this.questions.find(x => x.id == queid).options.length; i++) {
          if ($("#option_que_" + this.questions.find(x => x.id == queid).options[i].id).is(":checked") == true) {
            if (option != "")
              option += ".";
            option += this.questions.find(x => x.id == queid).options[i].id;
          }
        }
        if (option == "") {
          arrOption[index] = "0";
        }
        else {
          arrOption[index] = option;
        }

        this.listOption = arrOption.join("-");

        //check mark
        $("#que_icon_" + queid).html('<i class="fa fa-check-square-o text-success"></i>');
        let count = 0;
        for (let i = 0; i < arrOption.length; i++) {
          if (arrOption[i] != "0")
            count++;
        }
        this.countQuestionDone = count;
      }
    }

    console.log(this.listQuestion);
    console.log(this.listOption);
  }

  lockModal(key: string) {
    $(".close_modal_exam_" + key).prop("disabled", true);
    $("#save_modal_exam_" + key).prop("disabled", true);
    $("#save_modal_exam_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_exam_" + key).prop("disabled", false);
    $("#save_modal_exam_" + key).prop("disabled", false);
    $("#save_modal_exam_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  updateTest() {
    this.route.queryParams.subscribe(params => {
      this.examRequest = {
        userId: Number(this.cookieSer.get('Id')),
        scheduleTestId: params['scheduleTestId'],
        listQuestion: this.listQuestion,
        listOption: this.listOption,
        remainingTime: this.timeLeft
      } as ExamRequest;
      this.studentTestSer.updateExam(this.examRequest).subscribe(res => { });
    });
  }

  nextQuestion() {
    if (this.typeOfTes == 1) {
      this.loadCount = this.loadCount + 1;
      this.stopMedia();
    }
    else {
      if (this.audio != null) {
        this.audio.pause();
      }
      this.partCount++;
      this.loadCount++;
      console.log(this.testQuestionsView[this.loadCount]);
      if (this.testQuestionsView[this.loadCount].teqU_PASID != null) {
        if (this.testQuestionsView[this.loadCount].passages.paS_MEDIA != null) {
          if ($.inArray(this.testQuestionsView[this.loadCount].passages.paS_MEDIA.substr(this.testQuestionsView[this.loadCount].passages.paS_MEDIA.length - 3, this.testQuestionsView[this.loadCount].passages.paS_MEDIA.length), this.videoExtension) != -1) {
            this.mediaType = "video";
          }
          else {
            this.mediaType = "audio";
            this.audio = new Audio();
            this.audio.src = this.testQuestionsView[this.loadCount].passages.fileUrl;
            this.audio.load();
            this.audio.currentTime = this.studentTest.sttE_CURRENTTIMEAUDIO;
            this.audio.play();
          }
        }

      }
    }
  }

  countTimeLeft() {
    this.timeLeft = this.timeLeft - 1000;
    // console.log(this.timeLeft);
  }
}
