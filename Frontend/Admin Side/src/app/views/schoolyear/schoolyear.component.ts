import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { SchoolYear, SchoolyearService } from '../../services/schoolyear.service';
import { Setting, SettingService } from '../../services/setting.service';
import { SignalrService } from '../../services/signalr.service';
import { RoleService } from '../../services/role.service';
import { UserPermissionService } from '../../services/user-permission.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-schoolyear',
  templateUrl: './schoolyear.component.html',
  styleUrls: ['./schoolyear.component.css']
})
export class SchoolyearComponent implements OnInit {

  schoolyears: [SchoolYear];
  schoolyear: SchoolYear = {} as SchoolYear;
  id: number;

  pnotify = undefined;
  autoGenCode: Setting = {} as Setting;
  currentYear: number = new Date().getFullYear();
  schoolyear_Id: number;

  scye_add: boolean = true;
  scye_upd: boolean = true;
  scye_del: boolean = true;
  scye_ref: boolean = true;
  tabControl: number = 1;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;

  constructor(private pNotify: PnotifyService, private cookieSer: CookieService,
    private schoolYearSer: SchoolyearService, private settingSer: SettingService,
    private signalRSer: SignalrService, private roleSer: RoleService,
    private userPermissionSer: UserPermissionService, private route: ActivatedRoute) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.schoolYearSer.getAll().subscribe(res => {
      this.schoolyears = res.data;
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
    this.settingSer.get(3).subscribe(res => {
      this.autoGenCode = res.data;
    });
    //permission
    this.roleSer.get(36).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'scye_add':
                this.scye_add = false;
                break;
              case 'scye_upd':
                this.scye_upd = false;
                break;
              case 'scye_del':
                this.scye_del = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    this.roleSer.get(92).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        let count = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
          }
        }
        if (!(count > 0)) {
          this.scye_ref = false;
        }
      });
    });
    //realtime
    this.signalRSer.schoolyear.subscribe((res) => {
      if (res === true) {
        this.schoolYearSer.getAll().subscribe(res1 => {
          this.schoolyears = res1.data;
          this.rerender();
        });
      }
    });
    this.signalRSer.setting.subscribe((res1) => {
      if (res1 === true) {
        this.settingSer.get(3).subscribe(res => {
          this.autoGenCode = res.data;
        });
      }
    });
  }

  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
      if (this.id != null) {
        // console.log(this.braid);
        this.openModalDetail(this.id);
      }
    });
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
  }

  GenCode() {
    let code = "";
    if ((this.schoolyears.length + 1) < 10) {
      code += "0" + (this.schoolyears.length + 1);
    }
    else {
      code += (this.schoolyears.length + 1);
    }

    while (this.schoolyears.find(x => x.scyE_CODE == code && x.id != this.schoolyear.id) != null) {
      let updateCode = parseInt(code) + 1;
      if (updateCode < 10) {
        code = "0" + updateCode;
      }
      else {
        code = updateCode.toString();
      }
    }

    return code;
  }

  clearField() {
    for (let i = 1; i <= 2; i++) {
      $("#field_sch_" + i).removeClass('is-invalid is-valid');
      $("#valid_mes_sch_" + i).html("");
    }
  }

  checkField() {
    for (let i = 1; i <= 2; i++) {
      $("#field_sch_" + i).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_sch_" + i).html("");
    }
  }

  autoGenData() {
    if (this.autoGenCode.seT_ISACTIVE == true) {
      this.schoolyear.scyE_STARTYEAR = new Date().getFullYear();
    }
    else {
      this.schoolyear.scyE_CODE = this.GenCode();
      this.schoolyear.scyE_STARTYEAR = new Date().getFullYear();
    }
    this.checkField();
  }

  validate_sch_code() {
    if ($("#field_sch_1").val() == '') {
      $("#field_sch_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_sch_1").html("Vui lòng nhập mã niên khóa!");
      return false;
    }
    else {
      let schoolyearCode = $("#field_sch_1").val().toString().trim();
      let isExist = false;
      for (let i = 0; i < this.schoolyears.length; i++) {
        if (schoolyearCode == this.schoolyears[i].scyE_CODE && this.schoolyear.id != this.schoolyears[i].id) {
          isExist = true;
          break;
        }
      }

      if (isExist == true) {
        $("#field_sch_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_sch_1").html("Mã niên khóa đã được sử dụng!");
        return false;
      }
      else {
        $("#field_sch_1").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_sch_1").html("");
        return true;
      }
    }
  }
  validate_sch_startyear() {
    if ($("#field_sch_2").val() == '') {
      $("#field_sch_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_sch_2").html("Vui lòng nhập năm bắt đầu!");
      return false;
    }
    else if (this.schoolyears.find(x => x.scyE_STARTYEAR == $("#field_sch_2").val() && x.id != this.schoolyear.id) != null) {
      $("#field_sch_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_sch_2").html("Năm bắt đầu đã được sử dụng!");
      return false;
    }
    else if ($("#field_sch_2").val() < new Date().getFullYear()) {
      $("#field_sch_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_sch_2").html("Năm bắt đầu không được trước năm hiện tại!");
      return false;
    }
    else {
      $("#field_sch_2").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_sch_2").html("");
      return true;
    }
  }
  validate() {
    var check = true;
    if (this.validate_sch_code() == false)
      check = false;
    if (this.validate_sch_startyear() == false)
      check = false;

    //validate all
    if (check == false) {
      $("#valid_sch_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_sch_" + id).is(":visible")) {
      $("#buttonicon_sch_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_sch_" + id).slideToggle();
    }
    else {
      $("#buttonicon_sch_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_sch_" + id).slideToggle();
    }
  }

  openModal(id: number = 0) {
    this.unlockModal("addupdate");
    if (id > 0) { // update

      this.schoolYearSer.get(id).subscribe(res => {
        this.schoolyear = res.data;
      });

      this.checkField();
      $("#modal_title_sch").html("Sửa niên khóa");
      this.modal.show();
    }
    else { // insert
      if (this.autoGenCode.seT_ISACTIVE == true) {
        this.schoolyear = {
          id: 0,
          scyE_CODE: this.GenCode()
        } as SchoolYear;
      }
      else {
        this.schoolyear = {
          id: 0
        } as SchoolYear;
      }

      this.clearField();
      $("#modal_title_sch").html("Thêm niên khóa");
      this.modal.show();
    }
  }

  save() {

    if (this.validate() == false) {
      return false;
    }

    this.lockModal("addupdate");

    if (this.schoolyear.id === 0) { // insert
      this.schoolyear.scyE_CREATEDBY = parseInt(this.cookieSer.get('Id'));
      this.schoolYearSer.add(this.schoolyear).subscribe(res => {
        if (res.errorCode === 0) {
          this.schoolYearSer.getAll().subscribe(res1 => {
            this.schoolyears = res1.data;
            this.rerender();
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Thêm niên khóa thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
    else { // update
      this.schoolyear.scyE_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));

      this.schoolYearSer.put(this.schoolyear).subscribe(res => {
        if (res.errorCode === 0) {
          this.schoolYearSer.getAll().subscribe(res1 => {
            this.schoolyears = res1.data;
            this.rerender();
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Sửa niên khóa thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
  }

  openModalDelete(id: number) {
    this.unlockModal("delete");
    this.schoolYearSer.get(id).subscribe(res => {
      this.schoolyear = res.data;
      this.modalDelete.show();
    });
  }

  delete() {
    this.lockModal("delete");
    if (this.schoolyear.id != 0) {
      this.schoolYearSer.delete(this.schoolyear.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          this.schoolYearSer.getAll().subscribe(res1 => {
            this.schoolyears = res1.data;
            this.rerender();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Xóa niên khóa thành công!'
            });
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
      });
    }
  }
  lockModal(key: string) {
    $(".close_modal_sch_" + key).prop("disabled", true);
    $("#save_modal_sch_" + key).prop("disabled", true);
    $("#save_modal_sch_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_sch_" + key).prop("disabled", false);
    $("#save_modal_sch_" + key).prop("disabled", false);
    $("#save_modal_sch_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  openModalDetail(id: number) {
    this.schoolYearSer.get(id).subscribe(res => {
      this.schoolyear = res.data;
      this.schoolyear_Id = id;
      this.modalDetail.show();
    });
  }

  resetDetailModal() {
    this.schoolyear_Id = null;
    for (let i = 1; i <= 2; i++) {
      if (i == 1) {
        $("#tab_scye_" + i).addClass("active");
        $("#content_scye_" + i).addClass("active");
      }
      else {
        $("#tab_scye_" + i).removeClass("active");
        $("#content_scye_" + i).removeClass("active").addClass("fade");
      }
    }
    this.loadTab(1);
  }

  loadTab(key: number) {
    this.tabControl = key;
  }
}
