import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Permission, PermissionService } from '../../services/permission.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { DomSanitizer } from '@angular/platform-browser';
import { RoleService, Role } from '../../services/role.service';
import { RoleType, RoleTypeService } from '../../services/role-type.service';
import { permissionRequest } from '../../models/permission_request';
import { RolePermission } from '../../services/role-permission.service';
import { ActivatedRoute } from '@angular/router';
import { SignalrService } from '../../services/signalr.service';
import { UserPermissionService } from '../../services/user-permission.service';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.css']
})
export class PermissionComponent implements OnInit {

  permission_Id: number;
  perid: number;

  pnotify = undefined;
  permissions: [Permission];
  permissionsMultiDel: [Permission];
  permission: Permission = {} as Permission;
  roleTypes: [RoleType];
  permissionRequest: permissionRequest = {} as permissionRequest;
  listRolePermission: RolePermission[] = [];

  isListItemNull: boolean = true;
  roleIsNull: number = 0;
  tabControl: number = 1;

  //permission
  per_add: boolean = true;
  per_upd: boolean = true;
  per_del: boolean = true;
  per_ref: boolean = true;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild('modalDeletMul', { static: false }) modalDeletMul: ModalDirective;

  constructor(private pNotify: PnotifyService, private permissionSer: PermissionService,
    private cookieSer: CookieService, private _DomSanitizer: DomSanitizer,
    private roleTypeSer: RoleTypeService, private route: ActivatedRoute,
    private signalRSer: SignalrService, private roleSer: RoleService,
    private userPermissionSer: UserPermissionService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    // this.dtOptions = {
    //   columnDefs: [{
    //     orderable: false,
    //     className: 'select-checkbox',
    //     targets: 0
    //   }],
    //   select: {
    //     style: 'multi',
    //     selector: 'td:first-child'
    //   },
    //   order: [[1, 'asc']]
    // };
    this.permissionSer.getAll().subscribe(res => {
      this.permissions = res.data;
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
    this.roleTypeSer.getAll().subscribe(res => {
      this.roleTypes = res.data;
    });
    //permission
    this.roleSer.get(84).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'per_add':
                this.per_add = false;
                break;
              case 'per_upd':
                this.per_upd = false;
                break;
              case 'per_del':
                this.per_del = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    this.roleSer.get(88).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        let count = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
          }
        }
        if (!(count > 0)) {
          this.per_ref = false;
        }
      });
    });
    //realtime
    this.signalRSer.permission.subscribe((res) => {
      if (res === true) {
        this.permissionSer.getAll().subscribe(res1 => {
          this.permissions = res1.data;
          this.rerender();
        });
      }
    });
  }

  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.perid = params['id'];
      if (this.perid != null) {
        this.openModalDetail(this.perid);
      }
    });
  }

  Select_Clear_All() {
    $(".toggle-per-all").closest('tr').toggleClass('selected');
    if ($(".toggle-per-all").closest('tr').hasClass('selected')) {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.select();
        });
      });
    }
    else {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.deselect();
        });
      });
    }
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
  }

  clearField() {
    for (let i = 1; i <= 2; i++) {
      $("#field_per_" + i).removeClass('is-invalid is-valid');
      $("#valid_mes_per_" + i).html("");
    }
    this.roleIsNull = 0;
    for (let i = 0; i < this.roleTypes.length; i++) {
      if (this.roleTypes[i].roles[0] != null) {
        let arrRoleGroup = this.roleTypes[i].roles;
        for (let j = 0; j < arrRoleGroup.length; j++) {
          if (arrRoleGroup[j].roles[0] != null) {
            let arrRole = arrRoleGroup[j].roles;
            for (let k = 0; k < arrRole.length; k++) {
              $("#role_" + arrRole[k].id).prop("checked", false);
            }
          }
          if ($("#role_" + arrRoleGroup[j].id).is(":checked") == true) {
            $("#role_" + arrRoleGroup[j].id).prop("checked", false);
            if ($("#child_" + arrRoleGroup[j].id).css('display') == 'block') {
              $("#icon_" + arrRoleGroup[j].id).removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
              $("#child_" + arrRoleGroup[j].id).slideToggle();
            }
          }
        }
      }
    }
  }

  checkField() {
    for (let i = 1; i <= 2; i++) {
      $("#field_per_" + i).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_per_" + i).html("");
    }
    for (let i = 0; i < this.roleTypes.length; i++) {
      if (this.roleTypes[i].roles[0] != null) {
        let arrRoleGroup = this.roleTypes[i].roles;
        for (let j = 0; j < arrRoleGroup.length; j++) {

          let item = this.permission.role_Permissions.find(x => x.ropE_ROLID == arrRoleGroup[j].id);
          // console.log(item);
          if (item.ropE_ISACTIVE == true) { //set check = true
            if ($("#role_" + item.ropE_ROLID).is(":checked") == false) {
              $("#role_" + item.ropE_ROLID).prop("checked", true);
              if ($("#child_" + item.ropE_ROLID).css('display') == 'none') {
                $("#icon_" + item.ropE_ROLID).removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-right");
                $("#child_" + item.ropE_ROLID).slideToggle();
              }
            }
            else {
              if ($("#child_" + item.ropE_ROLID).css('display') == 'none') {
                $("#icon_" + item.ropE_ROLID).removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-right");
                $("#child_" + item.ropE_ROLID).slideToggle();
              }
            }
          }
          else {
            if ($("#role_" + item.ropE_ROLID).is(":checked") == true) {
              $("#role_" + item.ropE_ROLID).prop("checked", false);
              if ($("#child_" + item.ropE_ROLID).css('display') == 'block') {
                $("#icon_" + item.ropE_ROLID).removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
                $("#child_" + item.ropE_ROLID).slideToggle();
              }
            }
            else {
              if ($("#child_" + item.ropE_ROLID).css('display') == 'block') {
                $("#icon_" + item.ropE_ROLID).removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
                $("#child_" + item.ropE_ROLID).slideToggle();
              }
            }
          }

          if (arrRoleGroup[j].roles[0] != null) {
            let arrRole = arrRoleGroup[j].roles;
            for (let k = 0; k < arrRole.length; k++) {
              let item1 = this.permission.role_Permissions.find(x => x.ropE_ROLID == arrRole[k].id);
              if (item1.ropE_ISACTIVE == true)
                $("#role_" + item1.ropE_ROLID).prop("checked", true);
              else
                $("#role_" + item1.ropE_ROLID).prop("checked", false);
            }
          }
        }
      }
    }
  }

  showItem(id: number) {
    if ($("#role_" + id).is(":checked")) {
      if ($("#child_" + id).is(":visible")) {
        $("#icon_" + id).removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
        $("#child_" + id).slideToggle();
      }
      else {
        $("#icon_" + id).removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-right");
        $("#child_" + id).slideToggle();
      }
    }
    else {
      if ($("#child_" + id).is(":visible")) {
        $("#icon_" + id).removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
        $("#child_" + id).slideToggle();
      }
    }
  }

  showItemView(id: number) {
    if ($("#role_vie_" + id).is(":checked")) {
      if ($("#child_vie_" + id).is(":visible")) {
        $("#icon_vie_" + id).removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
        $("#child_vie_" + id).slideToggle();
      }
      else {
        $("#icon_vie_" + id).removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-right");
        $("#child_vie_" + id).slideToggle();
      }
    }
    else {
      if ($("#child_vie_" + id).is(":visible")) {
        $("#icon_vie_" + id).removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
        $("#child_vie_" + id).slideToggle();
      }
    }
  }

  checkItem(roleid: number, roletypeid: number) {
    if (this.roleTypes.find(x => x.id == roletypeid).roles.find(x => x.id == roleid).roles[0] != null) {
      if ($("#role_" + roleid).is(":checked")) {
        let arr = this.roleTypes.find(x => x.id == roletypeid).roles.find(x => x.id == roleid).roles;
        for (let i = 0; i < arr.length; i++) {
          $("#role_" + arr[i].id).prop("checked", true);
        }
        this.roleIsNull++;
      } else {
        let arr = this.roleTypes.find(x => x.id == roletypeid).roles.find(x => x.id == roleid).roles;
        for (let i = 0; i < arr.length; i++) {
          $("#role_" + arr[i].id).prop("checked", false);
        }
        this.roleIsNull--;
      }
    }
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_per_" + id).is(":visible")) {
      $("#buttonicon_per_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_per_" + id).slideToggle();
    }
    else {
      $("#buttonicon_per_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_per_" + id).slideToggle();
    }
  }

  ToggleGroupMulti(id: number) {
    if ($("#detailToggle_per_mul_" + id).is(":visible")) {
      $("#buttonicon_per_mul_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_per_mul_" + id).slideToggle();
    }
    else {
      $("#buttonicon_per_mul_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_per_mul_" + id).slideToggle();
    }
  }

  validate_per_name() {
    if ($("#field_per_1").val() == '') {
      $("#field_per_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_per_1").html("Vui lòng nhập tên vai trò!");
      return false;
    }
    else {
      let permissionName = $("#field_per_1").val().toString().trim().toLowerCase();
      let isExist = false;
      for (let i = 0; i < this.permissions.length; i++) {
        if (permissionName == this.permissions[i].peR_NAME.toLowerCase() && this.permission.id != this.permissions[i].id && this.permissions[i].peR_STATUS == 1) {
          isExist = true;
          break;
        }
      }

      if (isExist == true) {
        $("#field_per_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_per_1").html("Tên vai trò đã tồn tại!");
        return false;
      }
      else {
        $("#field_per_1").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_per_1").html("");
        return true;
      }
    }
  }
  validate() {
    var check = true;
    if (this.validate_per_name() == false)
      check = false;

    //validate all
    if (check == false) {
      $("#valid_per_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  openModal(id: number = 0) {
    this.unlockModal("addupdate");
    if (id > 0) { // update

      this.permissionSer.get(id).subscribe(res => {
        this.permission = res.data;
        for (let i = 0; i < res.data.role_Permissions.length; i++) {
          if (res.data.role_Permissions[i].ropE_ISACTIVE == true && (res.data.role_Permissions[i].roles.roL_ACTID == 6 || res.data.role_Permissions[i].roles.roL_ACTID == 8))
            this.roleIsNull++;
        }
        this.checkField();
      });

      $("#modal_title_per").html("Sửa vai trò");
      this.modal.show();
    }
    else { // insert
      this.permission = {
        id: 0,
        peR_STATUS: 1
      } as Permission

      this.clearField();
      $("#modal_title_per").html("Thêm vai trò");
      this.modal.show();
    }
  }

  save() {

    if (this.validate() == false) {
      return false;
    }

    this.lockModal("addupdate");

    for (let i = 0; i < this.roleTypes.length; i++) {
      if (this.roleTypes[i].roles[0] != null) {
        let arrRoleGroup = this.roleTypes[i].roles;
        for (let j = 0; j < arrRoleGroup.length; j++) {
          this.listRolePermission.push({
            id: 0,
            ropE_ROLID: arrRoleGroup[j].id,
            ropE_ISACTIVE: $("#role_" + arrRoleGroup[j].id).is(":checked")
          } as RolePermission);
          if (arrRoleGroup[j].roles[0] != null) {
            let arrRole = arrRoleGroup[j].roles;
            for (let k = 0; k < arrRole.length; k++) {
              this.listRolePermission.push({
                id: 0,
                ropE_ROLID: arrRole[k].id,
                ropE_ISACTIVE: $("#role_" + arrRole[k].id).is(":checked")
              } as RolePermission);
            }
          }
        }
      }
    }

    this.permissionRequest = {
      permission: this.permission,
      listRolePermission: this.listRolePermission
    } as permissionRequest;
    // console.log(this.permissionRequest);

    if (this.permission.id === 0) { // insert
      this.permission.peR_CREATEDBY = parseInt(this.cookieSer.get('Id'));

      this.permissionSer.add(this.permissionRequest).subscribe(res => {
        // console.log(res.data);
        if (res.errorCode === 0) {
          this.permissionSer.getAll().subscribe(res1 => {
            this.permissions = res1.data;
            this.rerender();
            this.listRolePermission = [];
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Thêm vai trò thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
    else { // update
      this.permission.peR_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.permissionSer.put(this.permissionRequest).subscribe(res => {
        console.log(res);
        if (res.errorCode === 0) {
          this.permissionSer.getAll().subscribe(res1 => {
            this.permissions = res1.data;
            this.rerender();
            this.modal.hide();
            this.listRolePermission = [];
            if (this.modalDetail.isShown) {
              this.openModalDetail(res.data.id);
            }
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Sửa vai trò thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
  }

  openModalDelete(id: number) {
    this.unlockModal("delete");
    this.permissionSer.get(id).subscribe(res => {
      this.permission = res.data;
      this.modalDelete.show();
    });
  }
  delete() {
    this.lockModal("delete");
    if (this.permission.id != 0) {
      this.permissionSer.delete(this.permission.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          this.permissionSer.getAll().subscribe(res1 => {
            this.permissions = res1.data;
            this.rerender();
          });
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Xóa vai trò thành công!'
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
        this.modalDetail.hide();
      });
    }
  }
  openModalDeleteMul() {
    this.unlockModal("multidel");
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      let arr = [];
      dtInstance.rows({ selected: true }).every(function () {
        if (this.data()[1] != null) {
          arr.push(this.data()[1]);
        }
      });
      if (arr.length == 0) {
        this.isListItemNull = true;
        this.modalDeletMul.show();
      }
      else {
        this.isListItemNull = false;
        this.permissionSer.getListPermissionMultiDel(arr).subscribe(res => {
          this.permissionsMultiDel = res.data;
          // console.log(res.data);
          this.modalDeletMul.show();
        });
      }
    });
  }
  deleteMul() {
    this.lockModal("multidel");
    this.permissionSer.deleteMulti(this.permissionsMultiDel, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
      if (res.errorCode === 0) {
        this.permissionSer.getAll().subscribe(res1 => {
          this.permissions = res1.data;
          this.rerender();
          if ($(".toggle-per-all").closest('tr').hasClass('selected')) {
            $(".toggle-per-all").closest('tr').toggleClass('selected');
          }
          this.modalDeletMul.hide();
          this.pnotify.success({
            title: 'Thông báo',
            text: "Xóa danh sách vai trò thành công!"
          });
        });
      }
      else {
        if ($(".toggle-per-all").closest('tr').hasClass('selected')) {
          $(".toggle-per-all").closest('tr').toggleClass('selected');
        }
        this.modalDeletMul.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message
        });
      }
    });
  }

  openModalDetail(id: number) {
    this.permissionSer.get(id).subscribe(res => {
      this.permission = res.data;
      this.permission_Id = res.data.id;
      // for (let i = 0; i < res.data.role_Permissions.length; i++) {
      //   if (res.data.role_Permissions[i].ropE_ISACTIVE == true && (res.data.role_Permissions[i].roles.roL_ACTID == 6 || res.data.role_Permissions[i].roles.roL_ACTID == 8))
      //     this.roleIsNull++;
      // }
      for (let i = 0; i < this.roleTypes.length; i++) {
        if (this.roleTypes[i].roles[0] != null) {
          let arrRoleGroup = this.roleTypes[i].roles;
          for (let j = 0; j < arrRoleGroup.length; j++) {

            let item = this.permission.role_Permissions.find(x => x.ropE_ROLID == arrRoleGroup[j].id);
            // console.log(item);
            if (item.ropE_ISACTIVE == true) { //set check = true
              if ($("#role_vie_" + item.ropE_ROLID).is(":checked") == false) {
                $("#role_vie_" + item.ropE_ROLID).prop("checked", true);
                if ($("#child_vie_" + item.ropE_ROLID).css('display') == 'none') {
                  $("#icon_vie_" + item.ropE_ROLID).removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-right");
                  $("#child_vie_" + item.ropE_ROLID).slideToggle();
                }
              }
              else {
                if ($("#child_vie_" + item.ropE_ROLID).css('display') == 'none') {
                  $("#icon_vie_" + item.ropE_ROLID).removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-right");
                  $("#child_vie_" + item.ropE_ROLID).slideToggle();
                }
              }
            }
            else {
              if ($("#role_vie_" + item.ropE_ROLID).is(":checked") == true) {
                $("#role_vie_" + item.ropE_ROLID).prop("checked", false);
                if ($("#child_vie_" + item.ropE_ROLID).css('display') == 'block') {
                  $("#icon_vie_" + item.ropE_ROLID).removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
                  $("#child_vie_" + item.ropE_ROLID).slideToggle();
                }
              }
              else {
                if ($("#child_vie_" + item.ropE_ROLID).css('display') == 'block') {
                  $("#icon_vie_" + item.ropE_ROLID).removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
                  $("#child_vie_" + item.ropE_ROLID).slideToggle();
                }
              }
            }

            if (arrRoleGroup[j].roles[0] != null) {
              let arrRole = arrRoleGroup[j].roles;
              for (let k = 0; k < arrRole.length; k++) {
                let item1 = this.permission.role_Permissions.find(x => x.ropE_ROLID == arrRole[k].id);
                if (item1.ropE_ISACTIVE == true)
                  $("#role_vie_" + item1.ropE_ROLID).prop("checked", true);
                else
                  $("#role_vie_" + item1.ropE_ROLID).prop("checked", false);
              }
            }
          }
        }
      }
      // console.log(res.data);
      this.modalDetail.show();
    });
  }

  resetDetailModal() {
    this.permission_Id = null;
    //reset tab
    for (let i = 1; i <= 2; i++) {
      if (i == 1) {
        $("#tab_per_" + i).addClass("active");
        $("#content_" + i).addClass("active");
      }
      else {
        $("#tab_per_" + i).removeClass("active");
        $("#content_" + i).removeClass("active").addClass("fade");
      }
    }
    this.loadTab(1);
  }

  lockModal(key: string) {
    $(".close_modal_per_" + key).prop("disabled", true);
    $("#save_modal_per_" + key).prop("disabled", true);
    $("#save_modal_per_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_per_" + key).prop("disabled", false);
    $("#save_modal_per_" + key).prop("disabled", false);
    $("#save_modal_per_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  loadTab(key: number) {
    this.tabControl = key;
  }
}
