import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { BranchService, Branch } from '../../services/branch.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PnotifyService } from '../../services/pnotify.service';
import { Setting, SettingService } from '../../services/setting.service';
import { CookieService } from 'ngx-cookie-service';
import { SubjectBranch, SubjectBranchService } from '../../services/subject-branch.service';
import { Subjectt } from '../../services/subject.service';
import { ActivatedRoute } from '@angular/router';
import { Faculty, FacultyService } from '../../services/faculty.service';
import { SignalrService } from '../../services/signalr.service';
import { UserPermissionService } from '../../services/user-permission.service';
import { RoleService } from '../../services/role.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css']
})
export class BranchComponent implements OnInit {

  @Input() public facultyId: number; //parse parameter through selector
  braid: number;

  pnotify = undefined;
  branches: [Branch];
  branchParents: [Branch];
  branchAllowChilds: [Branch];
  faculties: [Faculty];
  faculty: Faculty = {} as Faculty;
  subjectList: [Subjectt];
  branch: Branch = {} as Branch;
  branchDetail: Branch = {} as Branch;
  autoGenCode: Setting = {} as Setting;
  isHasChild: Setting = {} as Setting;
  extendSize: Setting = {} as Setting;
  size: number;
  countBranches: [Branch];

  branch_Id: number;
  tabControl: number = 1;

  bra_add: boolean = true;
  bra_upd: boolean = true;
  bra_del: boolean = true;
  bra_ref: boolean = true;
  bra_con: boolean = true;
  facultyConstraint: Faculty;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;

  constructor(private branchSer: BranchService, private pNotify: PnotifyService,
    private settingSer: SettingService, private cookieSer: CookieService,
    private subjectBranchSer: SubjectBranchService, private route: ActivatedRoute,
    private facultySer: FacultyService, private signalRSer: SignalrService,
    private roleSer: RoleService, private userPermissionSer: UserPermissionService,
    private userSer: UserService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    if (this.facultyId == null) {
      this.branchSer.getAll().subscribe(res => {
        this.branches = res.data;
        // console.log(res.data);
      });
      this.branchSer.getAllParentBranch().subscribe(res => {
        this.branchParents = res.data;
        // console.log(res.data);
      });
      this.branchSer.getAllAllowChildBranch().subscribe(res => {
        this.branchAllowChilds = res.data;
        // console.log(res.data);
      });
    }
    else {
      this.branchSer.getListBranchByFacultyId(this.facultyId).subscribe(res => {
        this.branches = res.data;
      });
      this.branchSer.getListParentBranchByFacultyId(this.facultyId).subscribe(res => {
        this.branchParents = res.data;
      });
      this.branchSer.getListAllowChildBranchByFacultyId(this.facultyId).subscribe(res => {
        this.branchAllowChilds = res.data;
      });
      this.facultySer.get(this.facultyId).subscribe(res => {
        this.faculty = res.data;
      });
    }
    this.branchSer.getAll().subscribe(res => {
      this.countBranches = res.data;
      // console.log(res.data);
    });
    this.facultySer.getAll().subscribe(res => {
      this.faculties = res.data;
    });
    this.settingSer.get(3).subscribe(res => {
      this.autoGenCode = res.data;
    });
    this.settingSer.get(4).subscribe(res => {
      this.isHasChild = res.data;
    });
    this.settingSer.get(5).subscribe(res => {
      this.extendSize = res.data;
      this.size = Number(res.data.seT_CHOICEVALUE);
    });
    //permission
    this.roleSer.get(44).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        let count = 0;
        for (let i = 0; i < res.data.roles.length; i++) {
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          // console.log(count);
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'bra_add':
                this.bra_add = false;
                break;
              case 'bra_upd':
                this.bra_upd = false;
                break;
              case 'bra_del':
                this.bra_del = false;
                break;
              default:
                break;
            }
          }
          // console.log(this.bra_add);
          // console.log(this.bra_upd);
          // console.log(this.bra_del);
        }
      });
    });
    this.roleSer.get(6).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        let count = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
          }
        }
        if (!(count > 0)) {
          this.bra_ref = false;
        }
      });
    });
    this.roleSer.get(105).subscribe(res => {
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // let count = 0;
        let countNoConstraint = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            // if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
            //   count++;
            // }
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == false) != null) {
              countNoConstraint++;
            }
          }
        }

        if (countNoConstraint == 0) {
          this.bra_con = false;
          this.userSer.get(Number(this.cookieSer.get('Id'))).subscribe(res2 => {
            this.facultySer.get(res2.data.employees[0].emP_FACID).subscribe(res3 => {
              this.facultyConstraint = res3.data;
            });
          });
        }
        else {
          this.bra_con = true;
        }
      });
    });
    //realtime
    this.signalRSer.branch.subscribe((res) => {
      if (res === true) {
        if (this.facultyId == null) {
          this.branchSer.getAll().subscribe(res1 => {
            this.branches = res1.data;
            // console.log(res.data);
          });
          this.branchSer.getAllParentBranch().subscribe(res1 => {
            this.branchParents = res1.data;
            // console.log(res.data);
          });
          this.branchSer.getAllAllowChildBranch().subscribe(res1 => {
            this.branchAllowChilds = res1.data;
            // console.log(res.data);
          });
        }
        else {
          this.branchSer.getListBranchByFacultyId(this.facultyId).subscribe(res1 => {
            this.branches = res1.data;
            // console.log(res.data);
          });
          this.branchSer.getListParentBranchByFacultyId(this.facultyId).subscribe(res1 => {
            this.branchParents = res1.data;
            // console.log(res.data);
          });
          this.branchSer.getListAllowChildBranchByFacultyId(this.facultyId).subscribe(res1 => {
            this.branchAllowChilds = res1.data;
            // console.log(res.data);
          });
        }
        this.branchSer.getAll().subscribe(res1 => {
          this.countBranches = res1.data;
        });
      }
    });
    this.signalRSer.faculty.subscribe((res) => {
      if (res === true) {
        this.facultySer.getAll().subscribe(res1 => {
          this.faculties = res1.data;
        });
      }
    });
    this.signalRSer.setting.subscribe((res1) => {
      if (res1 === true) {
        this.settingSer.get(3).subscribe(res => {
          this.autoGenCode = res.data;
        });
        this.settingSer.get(4).subscribe(res => {
          this.isHasChild = res.data;
        });
        this.settingSer.get(5).subscribe(res => {
          this.extendSize = res.data;
          this.size = Number(res.data.seT_CHOICEVALUE);
        });
      }
    });
  }

  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.braid = params['id'];
      if (this.braid != null) {
        // console.log(this.braid);
        this.showSubject(event = null, this.braid);
      }
    });
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  change_icon = function (id) {
    if ($('#child_' + id).hasClass('show')) {
      $('#icon_' + id).removeClass('fa-chevron-right').addClass('fa-chevron-down');
    }
    else {
      $('#icon_' + id).removeClass('fa-chevron-down').addClass('fa-chevron-right');
    }
  }

  GenCode() {
    let code = "";
    let count = this.countBranches.length;
    do {
      code = "";
      if ((count + 1) < 10) {
        code += "00" + (count + 1);
      }
      else if ((count + 1) < 100) {
        code += "0" + (count + 1);
      }
      else {
        code += (count + 1);
      }
      count++;
    } while (this.countBranches.find(x => x.brA_CODE == code) != null);

    // $("#field_bra_2").val(code);
    return code;
  }

  clearField() {
    for (let i = 1; i <= 6; i++) {
      if (i == 5)
        continue;
      $("#field_bra_" + i).removeClass('is-invalid is-valid')
      $("#valid_mes_bra_" + i).html("");
    }
    $("#field_bra_5").prop("checked", false);
  }

  checkField() {
    for (let i = 1; i <= 6; i++) {
      if (i == 5)
        continue;
      $("#field_bra_" + i).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_bra_" + i).html("");
    }
  }

  validate_bra_name() {
    if ($("#field_bra_1").val() == '') {
      $("#field_bra_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_bra_1").html("Vui lòng nhập tên ngành học!");
      return false;
    }
    else {
      let branchName = $("#field_bra_1").val().toString().trim().toLowerCase();
      let isExist = false;
      for (let i = 0; i < this.branches.length; i++) {
        if (branchName == this.branches[i].brA_NAME.toLowerCase() && this.branch.id != this.branches[i].id) {
          isExist = true;
          break;
        }
      }

      if (isExist == true) {
        $("#field_bra_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_bra_1").html("Tên ngành học đã tồn tại!");
        return false;
      }
      else {
        $("#field_bra_1").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_bra_1").html("");
        return true;
      }
    }
  }
  validate_bra_code() {
    if ($("#field_bra_2").val() == '') {
      $("#field_bra_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_bra_2").html("Vui lòng nhập mã ngành học!");
      return false;
    }
    else {
      let branchCode = $("#field_bra_2").val().toString().trim();
      let isExist = false;
      for (let i = 0; i < this.branches.length; i++) {
        if (branchCode == this.branches[i].brA_CODE && this.branch.id != this.branches[i].id) {
          isExist = true;
          break;
        }
      }

      if (isExist == true) {
        $("#field_bra_2").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_bra_2").html("Mã ngành học đã tồn tại!");
        return false;
      }
      else {
        $("#field_bra_2").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_bra_2").html("");
        return true;
      }
    }
  }
  validate_bra_duration() {
    if ($("#field_bra_3").val() == '') {
      $("#field_bra_3").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_bra_3").html("Vui lòng nhập thời lượng!");
      return false;
    }
    else if (parseInt($("#field_bra_3").val().toString()) <= 0) {
      $("#field_bra_3").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_bra_3").html("Thời lượng học phải lớn hơn 0!");
      return false;
    }
    else {
      $("#field_bra_3").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_bra_3").html("");
      return true;
    }
  }
  validate_bra_parent() {
    $("#field_bra_4").removeClass('is-invalid').addClass('is-valid');
    return true;
  }
  validate_bra_faculty() {
    if ($("#field_bra_6").val() == '' || $("#field_bra_6").val() == null || $("#field_bra_6").val() == 0) {
      $("#field_bra_6").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_bra_6").html("Vui lòng chọn khoa!");
      return false;
    }
    else if (this.branch.brA_PARENT != 0 && this.branch.brA_PARENT != null && this.faculties.find(x => x.id == this.branch.brA_FACID).branches.find(x => x.id == this.branch.brA_PARENT) == null) {
      $("#field_bra_6").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_bra_6").html("Khoa không có ngành này!");
      return false;
    }
    else {
      $("#field_bra_6").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_bra_6").html("");
      return true;
    }
  }
  validate() {
    var check = true;
    if (this.validate_bra_name() == false)
      check = false;
    if (this.validate_bra_code() == false)
      check = false;
    if (this.validate_bra_duration() == false)
      check = false;
    if (this.validate_bra_parent() == false)
      check = false;
    if (this.validate_bra_faculty() == false)
      check = false;

    //validate all
    if (check == false) {
      $("#valid_bra_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true
  }

  showSubject(event = null, id: number) {
    if (event != null) {
      event.preventDefault();
    }
    this.branch_Id = null;
    this.branchSer.get(id).subscribe(res => {
      this.branchDetail = res.data;
      this.branch_Id = res.data.id;
    });
    //reset tab
    for (let i = 1; i <= 2; i++) {
      if (i == 1) {
        $("#tab_bra_" + i).addClass("active");
        $("#content_bra_" + i).addClass("active");
      }
      else {
        $("#tab_bra_" + i).removeClass("active");
        $("#content_bra_" + i).removeClass("active").addClass("fade");
      }
    }
    this.loadTab(1);
  }
  changeIcon(id: number) {
    if ($("#child_" + id).is(":visible") == true) {
      $("#icon_" + id).removeClass("fa-chevron-right").addClass("fa-chevron-down");
    } else {
      $("#icon_" + id).removeClass("fa-chevron-down").addClass("fa-chevron-right");
    }
  }

  removeBranchPick() {
    this.branch.brA_PARENT = 0;
    if (this.branch.brA_FACID != 0 && this.branch.brA_FACID != null) {
      $("#field_bra_6").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_bra_6").html("");
    }
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_bra_" + id).is(":visible")) {
      $("#buttonicon_bra_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_bra_" + id).slideToggle();
    }
    else {
      $("#buttonicon_bra_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_bra_" + id).slideToggle();
    }
  }

  openModal(id: number = 0) {
    this.unlockModal("addupd");
    if (id > 0) { // update
      this.checkField();

      this.branchSer.get(id).subscribe(res => {
        this.branch = res.data;
        if (res.data.brA_PARENT != null) {
          $("#field_bra_4").val(res.data.brA_PARENT);
        }
        else {
          $("#field_bra_4").val(0);
        }
        if (res.data.brA_ISPARENT == true)
          $("#field_bra_5").prop("checked", true);
        else
          $("#field_bra_5").prop("checked", false);
      });

      $("#modal_title_bra").html("Sửa ngành học");
      this.modal.show();
    }
    else { // insert
      this.clearField();
      if (this.autoGenCode.seT_ISACTIVE == true) {
        this.GenCode();
      }

      this.branch = {
        id: 0,
        brA_CODE: this.GenCode(),
        brA_PARENT: 0,
        brA_FACID: 0
      } as Branch

      if (this.facultyId != null) {
        this.branch.brA_FACID = this.facultyId;
      }

      $("#modal_title_bra").html("Thêm ngành học");
      this.modal.show();
    }
  }
  save() {
    if (this.validate() == false) {
      return false;
    }

    this.lockModal("addupd");
    this.branch.brA_CODE = $("#field_bra_2").val().toString();
    if ($("#field_bra_5").is(":checked") == true) {
      this.branch.brA_ISPARENT = true;
    }
    else
      this.branch.brA_ISPARENT = false;

    if (this.branch.brA_PARENT == 0)
      this.branch.brA_PARENT = null;

    if (this.branch.id === 0) { // insert
      this.branch.brA_CREATEDBY = parseInt(this.cookieSer.get('Id'));
      this.branchSer.add(this.branch).subscribe(res => {
        if (res.errorCode === 0) {
          if (this.facultyId == null) {
            this.branchSer.getAllParentBranch().subscribe(res1 => {
              this.branchParents = res1.data;
            });
            this.branchSer.getAllAllowChildBranch().subscribe(res1 => {
              this.branchAllowChilds = res1.data;
            });
            this.branchSer.getAll().subscribe(res1 => {
              this.branches = res1.data;
            });
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Thêm ngành học thành công!'
            });
          }
          else {
            this.branchSer.getListParentBranchByFacultyId(this.facultyId).subscribe(res1 => {
              this.branchParents = res1.data;
            });
            this.branchSer.getListAllowChildBranchByFacultyId(this.facultyId).subscribe(res1 => {
              this.branchAllowChilds = res1.data;
            });
            this.branchSer.getListBranchByFacultyId(this.facultyId).subscribe(res1 => {
              this.branches = res1.data;
            });
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Thêm ngành học thành công!'
            });
          }
          this.branchSer.getAll().subscribe(res1 => {
            this.countBranches = res1.data;
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
    else { // update
      this.branch.brA_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.branchSer.put(this.branch).subscribe(res => {
        if (res.errorCode === 0) {
          if (this.facultyId == null) {
            this.branchSer.getAllParentBranch().subscribe(res1 => {
              this.branchParents = res1.data;
            });
            this.branchSer.getAllAllowChildBranch().subscribe(res1 => {
              this.branchAllowChilds = res1.data;
            });
            this.branchSer.getAll().subscribe(res1 => {
              this.branches = res1.data;
            });
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Sửa ngành học thành công!'
            });
          }
          else {
            this.branchSer.getListParentBranchByFacultyId(this.facultyId).subscribe(res1 => {
              this.branchParents = res1.data;
            });
            this.branchSer.getListAllowChildBranchByFacultyId(this.facultyId).subscribe(res1 => {
              this.branchAllowChilds = res1.data;
            });
            this.branchSer.getListBranchByFacultyId(this.facultyId).subscribe(res1 => {
              this.branches = res1.data;
            });
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Sửa ngành học thành công!'
            });
          }
          this.branchSer.getAll().subscribe(res1 => {
            this.countBranches = res1.data;
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
  }
  openModalDelete(id: number) {
    this.unlockModal("del");
    this.branchSer.get(id).subscribe(res => {
      this.branch = res.data;
      // console.log(res.data);
      this.modalDelete.show();
    });
  }
  delete() {
    if (this.branch.id != 0) {
      this.lockModal("del");
      this.branchSer.delete(this.branch.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          if (this.facultyId == null) {
            this.branchSer.getAll().subscribe(res1 => {
              this.branches = res1.data;
            });
            this.branchSer.getAllParentBranch().subscribe(res1 => {
              this.branchParents = res1.data;
            });
            this.branchSer.getAllAllowChildBranch().subscribe(res1 => {
              this.branchAllowChilds = res1.data;
            });
            if (this.branchDetail != null && res.data.id == this.branchDetail.id) {
              this.branchDetail.brA_NAME = null;
              this.branch_Id = null;
            }
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Xóa ngành học thành công!'
            });
          }
          else {
            this.branchSer.getListBranchByFacultyId(this.facultyId).subscribe(res1 => {
              this.branches = res1.data;
            });
            this.branchSer.getListParentBranchByFacultyId(this.facultyId).subscribe(res1 => {
              this.branchParents = res1.data;
            });
            this.branchSer.getListAllowChildBranchByFacultyId(this.facultyId).subscribe(res1 => {
              this.branchAllowChilds = res1.data;
            });
            if (this.branchDetail != null && res.data.id == this.branchDetail.id) {
              this.branchDetail.brA_NAME = null;
              this.branch_Id = null;
            }
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Xóa ngành học thành công!'
            });
          }
          this.branchSer.getAll().subscribe(res1 => {
            this.countBranches = res1.data;
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
      });
    }
  }

  lockModal(key: string) {
    $(".close_modal_bra_" + key).prop("disabled", true);
    $("#save_modal_bra_" + key).prop("disabled", true);
    $("#save_modal_bra_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_bra_" + key).prop("disabled", false);
    $("#save_modal_bra_" + key).prop("disabled", false);
    $("#save_modal_bra_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  loadTab(key: number) {
    this.tabControl = key;
  }
}
