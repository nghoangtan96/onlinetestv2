import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PnotifyService } from '../../services/pnotify.service';
import * as $ from "jquery";
import { CookieService } from 'ngx-cookie-service';
import { InformationService, Information } from '../../services/information.service';
import { DomSanitizer } from '@angular/platform-browser';
import { FileService } from '../../services/file.service';
import { Router } from '@angular/router';
import { ValidateService } from '../../services/validate.service';
import { RoleService } from '../../services/role.service';
import { UserPermissionService } from '../../services/user-permission.service';
import { SignalrService } from '../../services/signalr.service';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss']
})
export class InformationComponent implements OnInit {

  informations: [Information];
  Updateinformations: [Information];
  information: Information = {} as Information;
  informationDelete: Information = {} as Information;
  pnotify = undefined;

  url: any = "";
  file: File = null;
  updateUrl: any = "";

  inf_add: boolean = true;
  inf_upd: boolean = true;
  inf_del: boolean = true;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalAdd', { static: false }) modalAdd: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;

  constructor(private informationSer: InformationService, private pNotify: PnotifyService,
    private cookieSer: CookieService, private _DomSanitizer: DomSanitizer, //unsafe bug fix 
    private router: Router, private fileSer: FileService,
    private validateSer: ValidateService, private roleSer: RoleService,
    private userPermissionSer: UserPermissionService, private signalRSer: SignalrService) {
    this.pnotify = this.pNotify.getPNotify();
  }
  ngOnInit() {
    this.informationSer.getAll().subscribe(res => {
      this.informations = res.data;
      // console.log(res.data);
      if (this.informations.find(x => x.inF_KEYNAME == 'Logo').inF_VALUE == null) {
        this.url = "assets/img/default_logo.png";
      }
      else {
        this.url = this.informations.find(x => x.inF_KEYNAME == 'Logo').fileUrl;
      }
    });
    //permission
    this.roleSer.get(71).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true){
                count++;
              }
            }
          }
          // console.log(count);
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'inf_add':
                this.inf_add = false;
                break;
              case 'inf_upd':
                this.inf_upd = false;
                break;
              case 'inf_del':
                this.inf_del = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    //realtime
    this.signalRSer.information.subscribe((res1) => {
      if (res1 === true) {
        this.informationSer.getAll().subscribe(res => {
          this.informations = res.data;
          // console.log(res.data);
          if (this.informations.find(x => x.inF_KEYNAME == 'Logo').inF_VALUE == null) {
            this.url = "assets/img/default_logo.png";
          }
          else {
            this.url = this.informations.find(x => x.inF_KEYNAME == 'Logo').fileUrl;
          }
        });
      }
    });
  }

  onSelectFile(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      this.file = event.target.files[0];

      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.updateUrl = event.target.result;
      }

      $("#label_field_inf_1").html(
        this.file.name.length > 20 ?
          this.file.name.substring(0, 5) + "..." + this.file.name.substring(this.file.name.length - 9, this.file.name.length) :
          this.file.name
      );
    }
  }

  clearField() {
    for (let i = 1; i <= 7; i++) {
      // $("#field_ins_" + i).val("");
      if (i == 1) {
        $("#field_pas_1>.ck-editor").removeAttr("style");
        $("#valid_mes_pas_" + i).html("");
      }
      else {
        $("#field_pas_" + i).removeClass('is-invalid is-valid');
        $("#valid_mes_pas_" + i).html("");
      }
    }
    this.file = null;
    $("#label_field_pas_2").html("Tải tệp lên");
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_inf_" + id).is(":visible")) {
      $("#buttonicon_inf_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_inf_" + id).slideToggle();
    }
    else {
      $("#buttonicon_inf_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_inf_" + id).slideToggle();
    }
  }

  validate_inf_logo() {
    $("#field_inf_1").removeClass('is-invalid').addClass('is-valid');
  }

  validate_inf_name() {
    if ($("#field_inf_2").val() == '') {
      $("#field_inf_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_inf_2").html("Vui lòng nhập tên tổ chức!");
      return false;
    }
    else {
      $("#field_inf_2").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_inf_2").html("");
      return true;
    }
  }

  validate_inf_phone(id: any = null) {
    if (id == null) {
      if (this.validateSer.validate_phonenumber($("#field_inf_3").val().toString()) == '1') {
        $("#field_inf_3").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_inf_3").removeClass('text-success').html("Vui lòng nhập số điện thoại!");
        return false;
      }
      else if (this.validateSer.validate_phonenumber($("#field_inf_3").val().toString()) == '2') {
        $("#field_inf_3").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_inf_3").removeClass('text-success').html("Số điện thoại sai định dạng!");
        return false;
      }
      else {
        $("#field_inf_3").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_inf_3").addClass('text-success').html("Số điện thoại " + this.validateSer.validate_phonenumber($("#field_inf_3").val().toString()));
        return true;
      }
    }
    else {
      if (this.validateSer.validate_phonenumber($("#field_inf_" + id).val().toString()) == '1') {
        $("#field_inf_" + id).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_inf_" + id).removeClass('text-success').html("Vui lòng nhập số điện thoại!");
        return false;
      }
      else if (this.validateSer.validate_phonenumber($("#field_inf_" + id).val().toString()) == '2') {
        $("#field_inf_" + id).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_inf_" + id).removeClass('text-success').html("Số điện thoại sai định dạng!");
        return false;
      }
      else {
        if (this.validate_inf_duplicatePhone(id) == false) {
          $("#field_inf_" + id).removeClass('is-valid').addClass('is-invalid');
          $("#valid_mes_inf_" + id).removeClass('text-success').html("Số điện thoại đã tồn tại!");
          return false;
        }
        else {
          $("#field_inf_" + id).removeClass('is-invalid').addClass('is-valid');
          $("#valid_mes_inf_" + id).addClass('text-success').html("Số điện thoại " + this.validateSer.validate_phonenumber($("#field_inf_" + id).val().toString()));
          return true;
        }
      }
    }
  }

  validate_inf_duplicatePhone(id: number) {
    if (this.information.id === 0) {
      let arr = this.informations.find(x => x.inF_KEYNAME == 'Số điện thoại');
      let mainPhone = this.validateSer.customizePhoneNumber(arr.inF_VALUE);
      let currentPhone = this.validateSer.customizePhoneNumber(this.information.inF_VALUE);
      if (mainPhone == currentPhone) {
        return false;
      }
      for (let i = 0; i < arr.informations.length; i++) {
        if (this.validateSer.customizePhoneNumber(arr.informations[i].inF_VALUE) == currentPhone && arr.informations[i].id != id) {
          return false;
        }
      }
      return true;
    }
    else {
      let arr = this.Updateinformations.find(x => x.inF_KEYNAME == 'Số điện thoại');
      let mainPhone = this.validateSer.customizePhoneNumber(arr.inF_VALUE);
      let currentPhone = this.validateSer.customizePhoneNumber(arr.informations.find(x => x.id == id).inF_VALUE);
      if (mainPhone == currentPhone) {
        return false;
      }
      for (let i = 0; i < arr.informations.length; i++) {
        if (this.validateSer.customizePhoneNumber(arr.informations[i].inF_VALUE) == currentPhone && arr.informations[i].id != id) {
          return false;
        }
      }
      return true;
    }
  }

  validate_inf_email(id: any = null) {
    var regex = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if (id == null) {
      if ($("#field_inf_4").val() == '') {
        $("#field_inf_4").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_inf_4").html("Vui lòng nhập email!");
        return false;
      }
      else if (!regex.test($("#field_inf_4").val().toString())) {
        $("#field_inf_4").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_inf_4").html("Email sai định dạng!");
        return false;
      }
      else {
        $("#field_inf_4").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_inf_4").html("");
        return true;
      }
    }
    else {
      if ($("#field_inf_" + id).val() == '') {
        $("#field_inf_" + id).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_inf_" + id).html("Vui lòng nhập email!");
        return false;
      }
      else if (!regex.test($("#field_inf_" + id).val().toString())) {
        $("#field_inf_" + id).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_inf_" + id).html("Email sai định dạng!");
        return false;
      }
      else {
        if (this.validate_inf_duplicateEmail(id) == false) {
          $("#field_inf_" + id).removeClass('is-valid').addClass('is-invalid');
          $("#valid_mes_inf_" + id).removeClass('text-success').html("Email đã tồn tại!");
          return false;
        }
        else {
          $("#field_inf_" + id).removeClass('is-invalid').addClass('is-valid');
          $("#valid_mes_inf_" + id).html("");
          return true;
        }
      }
    }
  }

  validate_inf_duplicateEmail(id: number) {
    if (this.information.id === 0) {
      let arr = this.informations.find(x => x.inF_KEYNAME == 'Email');
      let mainPhone = this.validateSer.customizePhoneNumber(arr.inF_VALUE);
      let currentPhone = this.validateSer.customizePhoneNumber(this.information.inF_VALUE);
      if (mainPhone == currentPhone) {
        return false;
      }
      for (let i = 0; i < arr.informations.length; i++) {
        if (this.validateSer.customizePhoneNumber(arr.informations[i].inF_VALUE) == currentPhone && arr.informations[i].id != id) {
          return false;
        }
      }
      return true;
    }
    else {
      let arr = this.Updateinformations.find(x => x.inF_KEYNAME == 'Email');
      let mainEmail = arr.inF_VALUE;
      let currentEmail = arr.informations.find(x => x.id == id).inF_VALUE;
      if (mainEmail == currentEmail) {
        return false;
      }
      for (let i = 0; i < arr.informations.length; i++) {
        if (arr.informations[i].inF_VALUE == currentEmail && arr.informations[i].id != id) {
          return false;
        }
      }
      return true;
    }
  }

  validate_inf_fax(id: any = null) {
    var regex = /^\+?[0-9]{7,}$/;
    if (id == null) {
      if ($("#field_inf_5").val() != '' && !regex.test(this.validateSer.customizeFaxNumber($("#field_inf_5").val().toString()))) {
        $("#field_inf_5").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_inf_5").html("Fax sai định dạng!");
        return false;
      }
      else {
        $("#field_inf_5").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_inf_5").html("");
        return true;
      }
    }
    else {
      if ($("#field_inf_" + id).val() == '') {
        $("#field_inf_" + id).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_inf_" + id).html("Vui lòng nhập Fax!");
        return false;
      }
      else if (!regex.test(this.validateSer.customizeFaxNumber($("#field_inf_" + id).val().toString()))) {
        $("#field_inf_" + id).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_inf_" + id).html("Fax sai định dạng!");
        return false;
      }
      else {
        if (this.validate_inf_duplicateFax(id) == false) {
          $("#field_inf_" + id).removeClass('is-valid').addClass('is-invalid');
          $("#valid_mes_inf_" + id).html("Fax đã tồn tại!");
          return false;
        }
        else {
          $("#field_inf_" + id).removeClass('is-invalid').addClass('is-valid');
          $("#valid_mes_inf_" + id).html("");
          return true;
        }
      }
    }
  }

  validate_inf_duplicateFax(id: number) {
    if (this.information.id === 0) {
      let arr = this.informations.find(x => x.inF_KEYNAME == 'Fax');
      let mainPhone = this.validateSer.customizePhoneNumber(arr.inF_VALUE);
      let currentPhone = this.validateSer.customizePhoneNumber(this.information.inF_VALUE);
      if (mainPhone == currentPhone) {
        return false;
      }
      for (let i = 0; i < arr.informations.length; i++) {
        if (this.validateSer.customizePhoneNumber(arr.informations[i].inF_VALUE) == currentPhone && arr.informations[i].id != id) {
          return false;
        }
      }
      return true;
    }
    else {
      let arr = this.Updateinformations.find(x => x.inF_KEYNAME == 'Fax');
      let mainFax = this.validateSer.customizeFaxNumber(arr.inF_VALUE);
      let currentFax = this.validateSer.customizeFaxNumber(arr.informations.find(x => x.id == id).inF_VALUE);
      if (mainFax == currentFax) {
        return false;
      }
      for (let i = 0; i < arr.informations.length; i++) {
        if (this.validateSer.customizeFaxNumber(arr.informations[i].inF_VALUE) == currentFax && arr.informations[i].id != id) {
          return false;
        }
      }
      return true;
    }
  }

  validate_inf_address(id: any = null) {
    if (id == null) {
      if ($("#field_inf_6").val() == '') {
        $("#field_inf_6").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_inf_6").html("Vui lòng nhập địa chỉ!");
        return false;
      }
      else {
        $("#field_inf_6").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_inf_6").html("");
        return true;
      }
    }
    else {
      if ($("#field_inf_" + id).val() == '') {
        $("#field_inf_" + id).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_inf_" + id).html("Vui lòng nhập địa chỉ!");
        return false;
      }
      else {
        if (this.validate_inf_duplicateAddress(id) == false) {
          $("#field_inf_" + id).removeClass('is-valid').addClass('is-invalid');
          $("#valid_mes_inf_" + id).html("Địa chỉ đã tồn tại!");
          return false;
        }
        else {
          $("#field_inf_" + id).removeClass('is-invalid').addClass('is-valid');
          $("#valid_mes_inf_" + id).html("");
          return true;
        }
      }
    }
  }

  validate_inf_duplicateAddress(id: number) {
    if (this.information.id === 0) {
      let arr = this.informations.find(x => x.inF_KEYNAME == 'Địa chỉ');
      let mainPhone = this.validateSer.customizePhoneNumber(arr.inF_VALUE);
      let currentPhone = this.validateSer.customizePhoneNumber(this.information.inF_VALUE);
      if (mainPhone == currentPhone) {
        return false;
      }
      for (let i = 0; i < arr.informations.length; i++) {
        if (this.validateSer.customizePhoneNumber(arr.informations[i].inF_VALUE) == currentPhone && arr.informations[i].id != id) {
          return false;
        }
      }
      return true;
    }
    else {
      let arr = this.Updateinformations.find(x => x.inF_KEYNAME == 'Địa chỉ');
      let mainAddress = arr.inF_VALUE.replace(' ', '');
      let currentAddress = arr.informations.find(x => x.id == id).inF_VALUE.replace(' ', '');
      if (mainAddress == currentAddress) {
        return false;
      }
      for (let i = 0; i < arr.informations.length; i++) {
        if (arr.informations[i].inF_VALUE.replace(' ', '') == currentAddress && arr.informations[i].id != id) {
          return false;
        }
      }
      return true;
    }
  }

  validate_inf_website(id: any = null) {
    if (id == null) {
      $("#field_inf_7").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_inf_7").html("");
      return true;
    }
    else {
      if ($("#field_inf_" + id).val() == '') {
        $("#field_inf_" + id).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_inf_" + id).html("Vui lòng nhập địa chỉ website!");
        return false;
      }
      else {
        if (this.validate_inf_duplicateWebsite(id) == false) {
          $("#field_inf_" + id).removeClass('is-valid').addClass('is-invalid');
          $("#valid_mes_inf_" + id).html("Địa chỉ website đã tồn tại!");
          return false;
        }
        else {
          $("#field_inf_" + id).removeClass('is-invalid').addClass('is-valid');
          $("#valid_mes_inf_" + id).html("");
          return true;
        }
      }
    }
  }

  validate_inf_duplicateWebsite(id: number) {
    if (this.information.id === 0) {
      let arr = this.informations.find(x => x.inF_KEYNAME == 'Website');
      let mainPhone = this.validateSer.customizePhoneNumber(arr.inF_VALUE);
      let currentPhone = this.validateSer.customizePhoneNumber(this.information.inF_VALUE);
      if (mainPhone == currentPhone) {
        return false;
      }
      for (let i = 0; i < arr.informations.length; i++) {
        if (this.validateSer.customizePhoneNumber(arr.informations[i].inF_VALUE) == currentPhone && arr.informations[i].id != id) {
          return false;
        }
      }
      return true;
    }
    else {
      let arr = this.Updateinformations.find(x => x.inF_KEYNAME == 'Website');
      let mainWebsite = arr.inF_VALUE;
      let currentWebsite = arr.informations.find(x => x.id == id).inF_VALUE;
      if (mainWebsite == currentWebsite) {
        return false;
      }
      for (let i = 0; i < arr.informations.length; i++) {
        if (arr.informations[i].inF_VALUE == currentWebsite && arr.informations[i].id != id) {
          return false;
        }
      }
      return true;
    }
  }

  validate() {
    var check = true;
    if (this.validate_inf_name() == false)
      check = false;
    if (this.validate_inf_phone() == false)
      check = false;
    if (this.validate_inf_email() == false)
      check = false;
    if (this.validate_inf_fax() == false)
      check = false;
    if (this.validate_inf_address() == false)
      check = false;

    for (let i = 0; i < this.Updateinformations.length; i++) {
      if (this.Updateinformations[i].inF_KEYNAME == "Số điện thoại") {
        let arr = this.Updateinformations[i].informations;
        for (let j = 0; j < arr.length; j++) {
          if (this.validate_inf_phone(arr[j].id) == false)
            check = false;
        }
      }
      if (this.Updateinformations[i].inF_KEYNAME == "Email") {
        let arr = this.Updateinformations[i].informations;
        for (let j = 0; j < arr.length; j++) {
          if (this.validate_inf_email(arr[j].id) == false)
            check = false;
        }
      }
      if (this.Updateinformations[i].inF_KEYNAME == "Fax") {
        let arr = this.Updateinformations[i].informations;
        for (let j = 0; j < arr.length; j++) {
          if (this.validate_inf_fax(arr[j].id) == false)
            check = false;
        }
      }
      if (this.Updateinformations[i].inF_KEYNAME == "Địa chỉ") {
        let arr = this.Updateinformations[i].informations;
        for (let j = 0; j < arr.length; j++) {
          if (this.validate_inf_address(arr[j].id) == false)
            check = false;
        }
      }
      if (this.Updateinformations[i].inF_KEYNAME == "Website") {
        let arr = this.Updateinformations[i].informations;
        for (let j = 0; j < arr.length; j++) {
          if (this.validate_inf_website(arr[j].id) == false)
            check = false;
        }
      }
    }

    this.validate_inf_logo();
    this.validate_inf_website();
    //validate all
    if (check == false) {
      $("#valid_inf_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  openModal() {
    this.unlockModal("upd")
    this.informationSer.getAll().subscribe(res => {
      this.Updateinformations = res.data;

      if (res.data.find(x => x.inF_KEYNAME == 'Logo').inF_VALUE != null) {
        $("#label_field_inf_1").html(
          res.data.find(x => x.inF_KEYNAME == 'Logo').inF_VALUE.split('/')[3].length > 30 ?
            res.data.find(x => x.inF_KEYNAME == 'Logo').inF_VALUE.split('/')[3].substring(0, 9) + "..." + res.data.find(x => x.inF_KEYNAME == 'Logo').inF_VALUE.split('/')[3].substring(res.data.find(x => x.inF_KEYNAME == 'Logo').inF_VALUE.split('/')[3].length - 9, res.data.find(x => x.inF_KEYNAME == 'Logo').inF_VALUE.split('/')[3].length) :
            res.data.find(x => x.inF_KEYNAME == 'Logo').inF_VALUE.split('/')[3]
        );
        this.updateUrl = res.data.find(x => x.inF_KEYNAME == 'Logo').fileUrl;
      }
      else {
        $("#label_field_inf_1").html("Tải tệp lên...");
        this.updateUrl = "assets/img/default_logo.png";
      }
    });

    $("#modal_title_inf").html("Sửa thông tin trang web");
    this.modal.show();
  }
  save() {

    if (this.validate() == false) {
      return false;
    }
    this.lockModal("upd");

    if (this.file != null) {
      const formData = new FormData();
      formData.append('file', this.file);
      if (this.informations.find(x => x.inF_KEYNAME == 'Logo').inF_VALUE == null) {
        this.fileSer.add('Informations', formData).subscribe(res => {
          this.Updateinformations.find(x => x.inF_KEYNAME == "Logo").inF_VALUE = res.data;
          this.informationSer.put(parseInt(this.cookieSer.get('Id')), this.Updateinformations).subscribe(res1 => {
            if (res1.errorCode === 0) {
              this.informationSer.getAll().subscribe(res2 => {
                this.informations = res2.data;
                this.url = res2.data.find(x => x.inF_KEYNAME == 'Logo').fileUrl;
                $(".navbar-brand-full").attr('src', res2.data.find(x => x.inF_KEYNAME == "Logo").fileUrl);
                $(".navbar-brand-minimized").attr('src', res2.data.find(x => x.inF_KEYNAME == "Logo").fileUrl);
                this.modal.hide();
                this.pnotify.success({
                  title: 'Thông báo',
                  text: 'Sửa thông tin trang web thành công!'
                });
              });
            }
            else {
              this.modal.hide();
              this.pnotify.error({
                title: 'Thông báo',
                text: res1.message
              });
            }
          });
        });
      }
      else {
        this.fileSer.put(this.informations.find(x => x.inF_KEYNAME == 'Logo').inF_VALUE.split('/')[3], 'Informations', formData).subscribe(res => {
          this.Updateinformations.find(x => x.inF_KEYNAME == "Logo").inF_VALUE = res.data;
          this.informationSer.put(parseInt(this.cookieSer.get('Id')), this.Updateinformations).subscribe(res1 => {
            if (res1.errorCode === 0) {
              this.informationSer.getAll().subscribe(res2 => {
                this.informations = res2.data;
                this.url = res2.data.find(x => x.inF_KEYNAME == 'Logo').fileUrl;
                $(".navbar-brand-full").attr('src', res2.data.find(x => x.inF_KEYNAME == "Logo").fileUrl);
                $(".navbar-brand-minimized").attr('src', res2.data.find(x => x.inF_KEYNAME == "Logo").fileUrl);
                this.modal.hide();
                this.pnotify.success({
                  title: 'Thông báo',
                  text: 'Sửa thông tin trang web thành công!'
                });
              });
            }
          });
        });
      }
    }
    else {
      this.informationSer.put(parseInt(this.cookieSer.get('Id')), this.Updateinformations).subscribe(res => {
        if (res.errorCode === 0) {
          this.informationSer.getAll().subscribe(res1 => {
            this.informations = res1.data;
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Sửa thông tin trang web thành công!'
            });
          });
        }
      });
    }
  }

  openModalAdd() {
    this.unlockModal("add");
    $("#field_inf_add_2").removeClass('is-invalid is-valid');
    $("#valid_mes_inf_add_2").html("");

    this.information = {
      id: 0,
      inF_INFID: 4,
      inF_ISMULTI: false
    } as Information;

    this.modalAdd.show();
  }
  saveAdd() {
    if (this.information.inF_INFID == 4 && this.validate_inf_phone('add_2') == false) {
      return false;
    }
    if (this.information.inF_INFID == 5 && this.validate_inf_email('add_2') == false) {
      return false;
    }
    if (this.information.inF_INFID == 6 && this.validate_inf_fax('add_2') == false) {
      return false;
    }
    if (this.information.inF_INFID == 7 && this.validate_inf_address('add_2') == false) {
      return false;
    }
    if (this.information.inF_INFID == 8 && this.validate_inf_website('add_2') == false) {
      return false;
    }

    this.lockModal("add");
    this.information.inF_CREATEDBY = Number(this.cookieSer.get('Id'));
    this.information.inF_KEYNAME = $("#opt_" + this.information.inF_INFID).html();
    this.informationSer.add(this.information).subscribe(res => {
      if (res.errorCode === 0) {
        this.informationSer.getAll().subscribe(res1 => {
          this.informations = res1.data;
          this.modalAdd.hide();
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Thêm thông tin trang web thành công!'
          });
        });
      }
      else {
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message
        });
      }
    });
  }

  openModalDelete(id: number) {
    this.unlockModal("del");
    this.informationSer.get(id).subscribe(res => {
      this.informationDelete = res.data;
      this.modalDelete.show();
    });
  }

  delete() {
    this.lockModal("del");
    if (this.information.id != 0) {
      this.informationSer.delete(this.informationDelete.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          this.informationSer.getAll().subscribe(res1 => {
            this.informations = res1.data;
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Xóa thông tin trang web thành công!'
            });
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
      });
    }
  }

  lockModal(key: string) {
    $(".close_modal_inf_" + key).prop("disabled", true);
    $("#save_modal_inf_" + key).prop("disabled", true);
    $("#save_modal_inf_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_inf_" + key).prop("disabled", false);
    $("#save_modal_inf_" + key).prop("disabled", false);
    $("#save_modal_inf_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }
}
