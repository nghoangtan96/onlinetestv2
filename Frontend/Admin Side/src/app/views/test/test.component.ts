import { Component, OnInit, ViewChild, ViewChildren, QueryList, Input } from '@angular/core';
import { Test, TestService } from '../../services/test.service';
import { Subject, fromEventPattern } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { SettingService, Setting } from '../../services/setting.service';
import { ActivatedRoute } from '@angular/router';
import { SignalrService } from '../../services/signalr.service';
import { EmployeeService } from '../../services/employee.service';
import { Employee_Subject, EmployeeSubjectService } from '../../services/employee-subject.service';
import { UserService } from '../../services/user.service';
import { TestTypeService, Testtype } from '../../services/test-type.service';
import { Question, QuestionService } from '../../services/question.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Passage, PassageService } from '../../services/passage.service';
import { TestQuestion } from '../../services/test-question.service';
import { Subjectt, SubjectService } from '../../services/subject.service';
import { InformationService, Information } from '../../services/information.service';
import { TestShuffle, QuestionOfPassage } from '../../models/testshuffle';
import { Option } from '../../services/option.service';
import { TestRequest } from '../../models/test_request';
import { Permission } from '../../services/permission.service';
import { AcceptRequest } from '../../models/acceptRequest';
import { RoleService } from '../../services/role.service';
import { UserPermissionService } from '../../services/user-permission.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  @Input() public testId: number; //parse parameter through selector
  id: number;
  test_Id: number;

  tests: [Test];
  test: Test = {} as Test;
  testsMultiDel: [Test];
  employeeSubjects: [Employee_Subject];
  testtypes: [Testtype];
  testtype: Testtype = {} as Testtype;
  testFormat: number;
  numberOfQuestion: number;
  numberOfQuestionLeft: number;
  shuffle: number;
  questions: [Question];
  question: Question;
  passages: [Passage];
  passage: Passage;
  informations: [Information];
  testQuestions: TestQuestion[] = [];
  testQuestionsView: TestQuestion[] = [];
  subject: Subjectt = {} as Subjectt;
  testRequest: TestRequest = {} as TestRequest;
  subjectsConstraint: [number];

  //check update
  listTestQuestionOld: TestQuestion[] = [];

  pnotify = undefined;
  isListItemNull: boolean = true;
  tabControl: number = 1;
  tabControlAddupd: number = 1;

  optionKey = ['A', 'B', 'C', 'D', 'E', 'F'];
  partKey = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', 'XIII', 'XIV', 'XV']
  difficultClass = ['very-easy-color', 'easy-color', 'medium-color', 'hard-color', 'very-hard-color'];
  listQuestionPick: number[] = [];
  listPassagePick: number[] = [];
  difficultDisplay: Setting = {} as Setting;
  countQuestionPick: number;
  countQuestionPickLeft: number;
  partPercentHTML: string = "";
  difficultPercentHTML: string = "";
  partPercentState: boolean = false;
  difficultPercentState: boolean = false;
  showScore: boolean;

  //auto gen test
  listNumberQuestionPerPart: number[] = [];
  listNumberQuestionPerDifficult: number[] = [];
  checkOptionKey: number = 0;
  questionsAuto: [Question];
  passagesAuto: [Passage];
  countQuestionPickAuto: number;
  countQuestionPickDifAuto: number;
  totalVEasy: number;
  totalEasy: number;
  totalMedium: number;
  totalHard: number;
  totalVHard: number;

  //shuffle
  numberOfTest: number;
  arrShuffle: number[] = [2, 3, 4, 5, 6, 7, 8];
  arrShuffleTest: TestShuffle[] = [];
  questionsList: [Question];
  passagesList: [Passage];

  //savePreview
  organizationName: string;
  logoUrl: any = "";
  showCode: number = 1;
  testCode: string;

  //detailShow
  listTestShuffleView: string[] = [];
  userLogin: number = Number(this.cookieSer.get('Id'));

  //acceptTest
  acceptPassword: string;
  password: string;
  correctPassword: boolean;

  videoExtension = ['mp4', 'webm', 'ogg'];
  audioExtension = ['mpeg', 'wav', 'm4a', 'mp3'];

  tes_add: boolean = true;
  tes_upd: boolean = true;
  tes_del: boolean = true;
  tes_acc: boolean = true;
  tes_con: boolean = true;
  tes_ref: boolean = true;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  dtTrigger1: Subject<any> = new Subject();
  dtOptions1: any = {};
  dtTrigger2: Subject<any> = new Subject();
  dtOptions2: any = {};
  @ViewChildren(DataTableDirective)
  dtElements: QueryList<DataTableDirective>;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalAddQuestion', { static: false }) modalAddQuestion: ModalDirective;
  @ViewChild('modalAutoGenerateTest', { static: false }) modalAutoGenerateTest: ModalDirective;
  @ViewChild('modalSavePreview', { static: false }) modalSavePreview: ModalDirective;
  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild('modalDeletMul', { static: false }) modalDeletMul: ModalDirective;
  @ViewChild('modalConfirmBox', { static: false }) modalConfirmBox: ModalDirective;
  @ViewChild('modalPassword', { static: false }) modalPassword: ModalDirective;

  constructor(private pNotify: PnotifyService, private cookieSer: CookieService,
    private settingSer: SettingService, private route: ActivatedRoute,
    private signalRSer: SignalrService, private testSer: TestService,
    private employeeSer: EmployeeService, private userSer: UserService,
    private testTypeSer: TestTypeService, private questionSer: QuestionService,
    private _DomSanitizer: DomSanitizer, private passageSer: PassageService,
    private subjectSer: SubjectService, private informationSer: InformationService,
    private roleSer: RoleService, private userPermissionSer: UserPermissionService,
    private employeeSubjectSer: EmployeeSubjectService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.dtOptions = {
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      order: [[1, 'asc']]
    };
    if (this.testId == null) {
      this.testSer.getAll().subscribe(res => {
        this.tests = res.data;
        // console.log(res.data);
        this.dtTrigger.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
      });
    }
    else {
      this.testSer.getListById(this.testId).subscribe(res => {
        this.tests = res.data;
        this.dtTrigger.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
      });
    }
    this.userSer.get(Number(this.cookieSer.get('Id'))).subscribe(res => {
      if (res.errorCode === 0) {
        this.employeeSer.get(res.data.employees[0].id).subscribe(res1 => {
          this.employeeSubjects = res1.data.employee_Subjects;
        });
      }
    });
    this.testTypeSer.getAll().subscribe(res => {
      this.testtypes = res.data;
    });
    this.settingSer.get(9).subscribe(res => {
      this.difficultDisplay = res.data;
    });
    // this.questionSer.getAll().subscribe(res => {
    //   this.questions = res.data;
    //   this.dtTrigger1.next();
    //   this.dtElements.forEach((dtElement: DataTableDirective) => {
    //     if (dtElement.dtInstance != null) {
    //       dtElement.dtInstance.then((dtInstance: any) => {
    //         if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
    //           dtInstance.columns().every(function () {
    //             const that = this;
    //             $('input', this.footer()).on('keyup change', function () {
    //               if (that.search() !== this['value']) {
    //                 that
    //                   .search(this['value'])
    //                   .draw();
    //               }
    //             });
    //           });
    //         }
    //       });
    //     }
    //   });
    // });
    //permission
    this.roleSer.get(96).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'tes_add':
                this.tes_add = false;
                break;
              case 'tes_upd':
                this.tes_upd = false;
                break;
              case 'tes_del':
                this.tes_del = false;
                break;
              case 'tes_acc':
                this.tes_acc = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    this.roleSer.get(105).subscribe(res => {
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        let count = 0;
        let countNoConstraint = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == false) != null) {
              countNoConstraint++;
            }
          }
        }
        if (count > 0 && countNoConstraint == 0) {
          this.tes_con = false;
          this.userSer.get(Number(this.cookieSer.get('Id'))).subscribe(res2 => {
            this.employeeSubjectSer.getListSubjectIdByEmployeeId(res2.data.employees[0].id).subscribe(res3 => {
              this.subjectsConstraint = res3.data;
            });
          });
        }
        else if (countNoConstraint > 0) {
          this.tes_con = true;
        }
      })
    });
    //realtime
    this.signalRSer.test.subscribe((res1) => {
      if (res1 === true) {
        if (this.testId == null) {
          this.testSer.getAll().subscribe(res => {
            this.tests = res.data;
            // console.log(res.data);
            this.rerender();
          });
        }
        else {
          this.testSer.getListById(this.testId).subscribe(res => {
            this.tests = res.data;
            this.rerender();
          });
        }
      }
    });
    this.signalRSer.testshift.subscribe((res) => {
      if (res === true) {
        this.testSer.getAll().subscribe(res1 => {
          this.tests = res1.data;
          this.rerender();
        });
      }
    });
    this.signalRSer.employee.subscribe((res2) => {
      if (res2 === true) {
        this.userSer.get(Number(this.cookieSer.get('Id'))).subscribe(res => {
          if (res.errorCode === 0) {
            this.employeeSer.get(res.data.employees[0].id).subscribe(res1 => {
              this.employeeSubjects = res1.data.employee_Subjects;
            });
          }
        });
      }
    });
    this.signalRSer.testtype.subscribe((res1) => {
      if (res1 === true) {
        this.testTypeSer.getAll().subscribe(res => {
          this.testtypes = res.data;
        });
      }
    });
    this.signalRSer.setting.subscribe((res1) => {
      if (res1 === true) {
        this.settingSer.get(9).subscribe(res => {
          this.difficultDisplay = res.data;
        });
      }
    });
  }

  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
      if (this.id != null) {
        // console.log(this.braid);
        this.openModalDetail(this.id);
      }
    });
  }

  Select_Clear_All() {
    $(".toggle-tes-all").closest('tr').toggleClass('selected');
    if ($(".toggle-tes-all").closest('tr').hasClass('selected')) {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_main") {
              dtInstance.rows({ search: 'applied' }).every(function () {
                this.select();
              });
            }
          });
        }
      });
    }
    else {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_main") {
              dtInstance.rows({ search: 'applied' }).every(function () {
                this.deselect();
              });
            }
          });
        }
      });
    }
  }

  Select_Clear_All_Question() {
    $(".toggle-tes-que-all").closest('tr').toggleClass('selected');
    if ($(".toggle-tes-que-all").closest('tr').hasClass('selected')) {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_sub") {
              dtInstance.rows({ search: 'applied' }).every(function () {
                this.select();
              });
            }
          });
        }
      });
    }
    else {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_sub") {
              dtInstance.rows({ search: 'applied' }).every(function () {
                this.deselect();
              });
            }
          });
        }
      });
    }
    this.countQuestionPickFunc();
  }

  Select_Clear_All_Passage() {
    $(".toggle-tes-pas-all").closest('tr').toggleClass('selected');
    if ($(".toggle-tes-pas-all").closest('tr').hasClass('selected')) {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_sub1") {
              dtInstance.rows({ search: 'applied' }).every(function () {
                this.select();
              });
            }
          });
        }
      });
    }
    else {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_sub1") {
              dtInstance.rows({ search: 'applied' }).every(function () {
                this.deselect();
              });
            }
          });
        }
      });
    }
    this.countQuestionPickFunc();
  }

  countQuestionPickFunc() {
    this.countQuestionPickLeft = 0;
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      if (dtElement.dtInstance != null) {
        dtElement.dtInstance.then((dtInstance: any) => {
          //que
          if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
            let arr = [];
            dtInstance.rows({ selected: true }).every(function () {
              if (this.data()[1] != null) {
                arr.push(this.data()[1]);
              }
            });
            this.countQuestionPickLeft += arr.length;
          }
          //pas
          if (dtInstance.table().node().id == "tb_sub1" && dtInstance != null) {
            let arr = [];
            dtInstance.rows({ selected: true }).every(function () {
              if (this.data()[1] != null) {
                arr.push(this.data()[1]);
              }
            });
            for (let i = 0; i < arr.length; i++) {
              this.countQuestionPickLeft += this.passages.find(x => x.id == arr[i]).questions.length;
            }
          }
        });
      }
    });
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    this.dtTrigger1.unsubscribe();
    this.dtTrigger2.unsubscribe();
  }

  rerender(): void {
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      if (dtElement.dtInstance != null) {
        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == "tb_main") {
            dtInstance.destroy();
            this.dtTrigger.next();
            this.dtElements.forEach((dtElement: DataTableDirective) => {
              if (dtElement.dtInstance != null) {
                dtElement.dtInstance.then((dtInstance: any) => {
                  if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
                    dtInstance.columns().every(function () {
                      const that = this;
                      $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this['value']) {
                          that
                            .search(this['value'])
                            .draw();
                        }
                      });
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }

  rerender1(): void {
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      if (dtElement.dtInstance != null) {
        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == "tb_sub") {
            dtInstance.destroy();
            this.dtTrigger1.next();
            this.dtElements.forEach((dtElement: DataTableDirective) => {
              if (dtElement.dtInstance != null) {
                dtElement.dtInstance.then((dtInstance: any) => {
                  if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
                    dtInstance.columns().every(function () {
                      const that = this;
                      $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this['value']) {
                          that
                            .search(this['value'])
                            .draw();
                        }
                      });
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }

  rerender2(): void {
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      if (dtElement.dtInstance != null) {
        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == "tb_sub1") {
            dtInstance.destroy();
            this.dtTrigger2.next();
            this.dtElements.forEach((dtElement: DataTableDirective) => {
              if (dtElement.dtInstance != null) {
                dtElement.dtInstance.then((dtInstance: any) => {
                  if (dtInstance.table().node().id == "tb_sub1" && dtInstance != null) {
                    dtInstance.columns().every(function () {
                      const that = this;
                      $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this['value']) {
                          that
                            .search(this['value'])
                            .draw();
                        }
                      });
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }

  showPassword(id: number) {
    if ($("#field_tes_" + id).attr('type') == 'password') {
      $("#field_tes_" + id).prop('type', 'text');
      $("#button_field_" + id).prop('title', 'Ẩn mật khẩu');
      $("#icon_field_" + id).removeClass('fa-eye').addClass('fa-eye-slash');
    }
    else {
      $("#field_tes_" + id).prop('type', 'password');
      $("#button_field_" + id).prop('title', 'Hiện mật khẩu');
      $("#icon_field_" + id).removeClass('fa-eye-slash').addClass('fa-eye');
    }
  }

  autoGenPassword(type: number, length: number, event = null) {
    if (event != null) {
      event.preventDefault();
    }

    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    if (type == 1) {
      this.test.teS_PASSWORD = result;
      $("#field_tes_4").val(result);
      this.validate_tes_password();
    }
    else if (type == 2) {
      this.acceptPassword = result;
      $("#field_tes_20").val(result);
      this.validate_tes_acceptPassword();
    }
  }

  clearField() {
    this.tabControlAddupd = 1;
    for (let i = 1; i <= 10; i++) {
      $("#field_tes_" + i).removeClass('is-invalid is-valid');
      $("#valid_mes_tes_" + i).html("");
      $("#field_tes_" + i).prop("disabled", false);
    }

    for (let i = 1; i <= 4; i++) {
      if (i == 1) {
        $("#tab_addupd_tes_" + i).addClass("active");
        $("#content_addupd_tes_" + i).removeClass("fade").addClass("active");
      }
      else {
        $("#tab_addupd_tes_" + i).removeClass("active");
        $("#content_addupd_tes_" + i).removeClass("active").addClass("fade");
      }
    }
    this.testFormat = null;
    this.numberOfQuestion = null;
    this.shuffle = null;
    this.testQuestions = [];
    this.arrShuffleTest = [];
    this.showScore = false;
    this.listQuestionPick = [];
    this.listPassagePick = [];
  }

  checkField() {
    this.tabControlAddupd = 4;
    for (let i = 1; i <= 10; i++) {
      $("#field_tes_" + i).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_tes_" + i).html("");
      if (i != 10) {
        $("#field_tes_" + i).prop("disabled", true);
      }
    }

    for (let i = 1; i <= 4; i++) {
      if (i == 4) {
        $("#tab_addupd_tes_" + i).addClass("active");
        $("#content_addupd_tes_" + i).removeClass("fade").addClass("active");
      }
      else {
        $("#tab_addupd_tes_" + i).removeClass("active");
        $("#content_addupd_tes_" + i).removeClass("active").addClass("fade");
      }
    }

    this.arrShuffleTest = [];
  }

  validate_tes_name() {
    if ($("#field_tes_1").val() == '' || $("#field_tes_1").val() == 0 || $("#field_tes_1").val() == null) {
      $("#field_tes_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_1").html("Vui lòng chọn môn học!");
      return false;
    }
    else if (this.tests.find(x => x.teS_SUBID == this.test.teS_SUBID && x.teS_TETYID == this.test.teS_TETYID && (x.teS_STATUS != 4)) != null) {
      $("#field_tes_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_1").html("Bạn đã tạo đề thi này rồi, vui lòng kiểm tra lại danh sách đề thi!");
      return false;
    }
    else {
      $("#field_tes_1").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_tes_1").html("");
      return true;
    }
  }

  validate_tes_testtype() {
    this.validate_tes_name();
    if ($("#field_tes_2").val() == '' || $("#field_tes_2").val() == 0 || $("#field_tes_2").val() == null) {
      $("#field_tes_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_2").html("Vui lòng chọn loại bài thi!");
      return false;
    }
    else if (this.tests.find(x => x.teS_SUBID == this.test.teS_SUBID && x.teS_TETYID == this.test.teS_TETYID && (x.teS_STATUS != 4)) != null) {
      $("#field_tes_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_2").html("Bạn đã tạo đề thi này rồi, vui lòng kiểm tra lại danh sách đề thi!");
      return false;
    }
    else {
      $("#field_tes_2").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_tes_2").html("");
      return true;
    }
  }

  validate_tes_maxscore() {
    if ($("#field_tes_3").val() == '' || $("#field_tes_3").val() == 0 || $("#field_tes_3").val() == null) {
      $("#field_tes_3").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_3").html("Vui lòng chọn số điểm!");
      return false;
    }
    else {
      $("#field_tes_3").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_tes_3").html("");
      return true;
    }
  }

  validate_tes_password() {
    if ($("#field_tes_4").val() == '' || $("#field_tes_4").val() == null) {
      $("#field_tes_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_4").html("Vui lòng nhập mật khẩu!");
      return false;
    }
    else {
      $("#field_tes_4").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_tes_4").html("");
      return true;
    }
  }

  validate_tes_acceptPassword() {
    if ($("#field_tes_20").val() == '' || $("#field_tes_20").val() == null) {
      $("#field_tes_20").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_20").html("Vui lòng nhập mật khẩu!");
      return false;
    }
    else {
      $("#field_tes_20").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_tes_20").html("");
      return true;
    }
  }

  validate_tes_Unlockpassword() {
    if ($("#field_tes_21").val() == '' || $("#field_tes_21").val() == null) {
      $("#field_tes_21").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_21").html("Vui lòng nhập mật khẩu!");
      return false;
    }
    else {
      $("#field_tes_21").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_tes_21").html("");
      return true;
    }
  }

  validate_tes_note() {
    $("#field_tes_5").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_tes_5").html("");
    return true;
  }

  validate_tes_testformat() {
    if ($("#field_tes_6").val() == '' || $("#field_tes_6").val() == 0 || $("#field_tes_6").val() == null) {
      $("#field_tes_6").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_6").html("Vui lòng chọn định dạng bài thi!");
      return false;
    }
    else {
      $("#field_tes_6").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_tes_6").html("");
      return true;
    }
  }

  validate_tes_numquestion() {
    if ($("#field_tes_7").val() == '' || $("#field_tes_7").val() == null) {
      $("#field_tes_7").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_7").html("Vui lòng nhập số lượng câu hỏi!");
      return false;
    }
    else if (Number($("#field_tes_7").val()) < 30 || Number($("#field_tes_7").val()) > 200) {
      $("#field_tes_7").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_7").html("Số lượng câu hỏi quy định trong khoảng 30 - 200!");
      return false;
    }
    else {
      $("#field_tes_7").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_tes_7").html("");
      return true;
    }
  }

  validate_tes_time() {
    if ($("#field_tes_8").val() == '' || $("#field_tes_8").val() == 0 || $("#field_tes_8").val() == null) {
      $("#field_tes_8").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_8").html("Vui lòng chọn thời gian làm bài!");
      return false;
    }
    else if (this.numberOfQuestion > this.test.teS_TIME) {
      $("#field_tes_8").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_8").html("Thời gian làm bài phải nhiều hơn hoặc bằng số lượng câu hỏi!");
      return false;
    }
    else {
      $("#field_tes_8").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_tes_8").html("");
      return true;
    }
  }

  validate_tes_shuffle() {
    if ($("#field_tes_9").val() == '' || $("#field_tes_9").val() == 0 || $("#field_tes_9").val() == null) {
      $("#field_tes_9").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_9").html("Vui lòng chọn kiểu trộn câu hỏi, đáp án!");
      return false;
    }
    else {
      $("#field_tes_9").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_tes_9").html("");
      return true;
    }
  }

  validate_tes_auto_numQuestion(type: number, id: number) {
    if (type == 1) {
      if ($("#field_tes_auto_" + id).val() < 0) {
        $("#field_tes_auto_" + id).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_tes_auto_" + id).html("Số lượng câu hỏi phải bằng 0 hoặc lớn hơn!");
        return false;
      }
      else if ($("#field_tes_auto_" + id).val() > 0) {
        let count = 0;
        let checkNumberQuestionCorrect = false;
        for (let i = 0; i < this.questionsAuto.length; i++) {
          if (this.questionsAuto[i].quE_PARID == id) {
            count++;
          }
        }
        for (let i = 0; i < this.passagesAuto.length; i++) {
          if (this.passagesAuto[i].paS_PARID == id) {
            count += this.passagesAuto[i].questions.length;
          }
        }

        if ($("#field_tes_auto_" + id).val() > count) {
          $("#field_tes_auto_" + id).removeClass('is-valid').addClass('is-invalid');
          $("#valid_mes_tes_auto_" + id).html("Số lượng câu hỏi của học phần không đủ!");
          return false;
        }
        else {
          $("#field_tes_auto_" + id).removeClass('is-invalid').addClass('is-valid');
          $("#valid_mes_tes_auto_" + id).html("");
          return true;
        }
      }
      else {
        $("#field_tes_auto_" + id).removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_tes_auto_" + id).html("");
        return true;
      }
    }
    if (type == 2) {
      if ($("#field_tes_auto_dif_" + id).val() < 0) {
        $("#field_tes_auto_dif_" + id).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_tes_auto_dif_" + id).html("Số lượng câu hỏi phải bằng 0 hoặc lớn hơn!");
        return false;
      }
      else if ($("#field_tes_auto_dif_" + id).val() > 0) {
        if (id == 1) {
          if ($("#field_tes_auto_dif_" + id).val() > this.totalVEasy) {
            $("#field_tes_auto_dif_" + id).removeClass('is-valid').addClass('is-invalid');
            $("#valid_mes_tes_auto_dif_" + id).html("Số lượng câu hỏi không đủ!");
          }
          else {
            $("#field_tes_auto_dif_" + id).removeClass('is-invalid').addClass('is-valid');
            $("#valid_mes_tes_auto_dif_" + id).html("");
            return true;
          }
        }
        if (id == 2) {
          if ($("#field_tes_auto_dif_" + id).val() > this.totalEasy) {
            $("#field_tes_auto_dif_" + id).removeClass('is-valid').addClass('is-invalid');
            $("#valid_mes_tes_auto_dif_" + id).html("Số lượng câu hỏi không đủ!");
          }
          else {
            $("#field_tes_auto_dif_" + id).removeClass('is-invalid').addClass('is-valid');
            $("#valid_mes_tes_auto_dif_" + id).html("");
            return true;
          }
        }
        if (id == 3) {
          if ($("#field_tes_auto_dif_" + id).val() > this.totalMedium) {
            $("#field_tes_auto_dif_" + id).removeClass('is-valid').addClass('is-invalid');
            $("#valid_mes_tes_auto_dif_" + id).html("Số lượng câu hỏi không đủ!");
          }
          else {
            $("#field_tes_auto_dif_" + id).removeClass('is-invalid').addClass('is-valid');
            $("#valid_mes_tes_auto_dif_" + id).html("");
            return true;
          }
        }
        if (id == 4) {
          if ($("#field_tes_auto_dif_" + id).val() > this.totalHard) {
            $("#field_tes_auto_dif_" + id).removeClass('is-valid').addClass('is-invalid');
            $("#valid_mes_tes_auto_dif_" + id).html("Số lượng câu hỏi không đủ!");
          }
          else {
            $("#field_tes_auto_dif_" + id).removeClass('is-invalid').addClass('is-valid');
            $("#valid_mes_tes_auto_dif_" + id).html("");
            return true;
          }
        }
        if (id == 2) {
          if ($("#field_tes_auto_dif_" + id).val() > this.totalVHard) {
            $("#field_tes_auto_dif_" + id).removeClass('is-valid').addClass('is-invalid');
            $("#valid_mes_tes_auto_dif_" + id).html("Số lượng câu hỏi không đủ!");
          }
          else {
            $("#field_tes_auto_dif_" + id).removeClass('is-invalid').addClass('is-valid');
            $("#valid_mes_tes_auto_dif_" + id).html("");
            return true;
          }
        }
      }
      else {
        $("#field_tes_auto_dif_" + id).removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_tes_auto_dif_" + id).html("");
        return true;
      }
    }
  }

  validate_tes_auto_all() {
    if (this.checkOptionKey == 1) {
      let count = 0;
      for (let i = 0; i < this.subject.parts.length; i++) {
        count += Number($("#field_tes_auto_" + this.subject.parts[i].id).val());
      }

      this.countQuestionPickAuto = count;

      if (count > this.numberOfQuestion) {
        $("#valid_mes_tes_auto_part_all").html("<i class='fa fa-exclamation-circle'></i> Số lượng câu hỏi lớn hơn tổng số câu hỏi của đề thi (" + this.countQuestionPickAuto + "/" + this.numberOfQuestion + ")!");
        return false;
      }
      else {
        $("#valid_mes_tes_auto_part_all").html("");
        return true;
      }
    }
    else if (this.checkOptionKey == 2) {
      let count = 0;
      for (let i = 1; i <= 5; i++) {
        count += Number($("#field_tes_auto_dif_" + i).val());
      }

      this.countQuestionPickDifAuto = count;

      if (count > this.numberOfQuestion) {
        $("#valid_mes_tes_auto_dif_all").html("<i class='fa fa-exclamation-circle'></i> Số lượng câu hỏi lớn hơn tổng số câu hỏi của đề thi (" + this.countQuestionPickDifAuto + "/" + this.numberOfQuestion + ")!");
        return false;
      }

      else {
        $("#valid_mes_tes_auto_part_all").html("");
        return true;
      }
    }
    else if (this.checkOptionKey == 3) {
      //check part question
      let count = 0;
      for (let i = 0; i < this.subject.parts.length; i++) {
        count += Number($("#field_tes_auto_" + this.subject.parts[i].id).val());
      }

      this.countQuestionPickAuto = count;

      if (count > this.numberOfQuestion) {
        $("#valid_mes_tes_auto_part_all").html("<i class='fa fa-exclamation-circle'></i> Số lượng câu hỏi lớn hơn tổng số câu hỏi của đề thi (" + this.countQuestionPickAuto + "/" + this.numberOfQuestion + ")!");
        return false;
      }
      else {
        $("#valid_mes_tes_auto_part_all").html("");
      }

      //check dif question
      let count1 = 0;
      for (let i = 1; i <= 5; i++) {
        count1 += Number($("#field_tes_auto_dif_" + i).val());
      }

      this.countQuestionPickDifAuto = count1;

      if (count1 > this.numberOfQuestion) {
        $("#valid_mes_tes_auto_dif_all").html("<i class='fa fa-exclamation-circle'></i> Số lượng câu hỏi lớn hơn tổng số câu hỏi của đề thi (" + this.countQuestionPickDifAuto + "/" + this.numberOfQuestion + ")!");
        return false;
      }
      else {
        $("#valid_mes_tes_auto_dif_all").html("");
      }

      //check part & dif question
      let countVE = 0;
      let countE = 0;
      let countM = 0;
      let countH = 0;
      let countVH = 0;
      let partText = "";
      for (let i = 0; i < this.subject.parts.length; i++) {
        if ($("#field_tes_auto_" + this.subject.parts[i].id).val() > 0) {
          for (let j = 0; j < this.questionsAuto.length; j++) {
            if (this.questionsAuto[j].quE_PARID == this.subject.parts[i].id) {
              switch (this.questionsAuto[j].quE_LEVEL) {
                case 1:
                  countVE++;
                  break;
                case 2:
                  countE++;
                  break;
                case 3:
                  countM++;
                  break;
                case 4:
                  countH++;
                  break;
                case 5:
                  countVH++;
                  break;
                default:
                  break;
              }
            }
          }

          if (partText == "") {
            partText += (i + 1);
          }
          else {
            partText += ", " + (i + 1);
          }
        }
      }
      for (let i = 1; i <= 5; i++) {
        switch (i) {
          case 1:
            if ($("#field_tes_auto_dif_1").val() > countVE) {
              $("#valid_mes_tes_auto_dif_all").html("<i class='fa fa-exclamation-circle'></i> Số lượng câu hỏi "
                + (this.difficultDisplay.seT_CHOICEVALUE == 'Bằng chữ' ? "<span class='very-easy-color'>Dễ</span>" : "<span class='very-easy-color'>1</span>")
                + " trong các học phần " + partText + " không đủ!");
              $("#valid_mes_tes_auto_part_all").html("<i class='fa fa-exclamation-circle'></i> Các học phần " + partText
                + " không có đủ số lượng câu hỏi " + (this.difficultDisplay.seT_CHOICEVALUE == 'Bằng chữ' ? "<span class='very-easy-color'>Dễ</span>" : "<span class='very-easy-color'>1</span>")
                + "!");
              return false;
            }
            break;
          case 2:
            if ($("#field_tes_auto_dif_2").val() > countE) {
              $("#valid_mes_tes_auto_dif_all").html("<i class='fa fa-exclamation-circle'></i> Số lượng câu hỏi "
                + (this.difficultDisplay.seT_CHOICEVALUE == 'Bằng chữ' ? "<span class='easy-color'>Trung bình Dễ</span>" : "<span class='very-easy-color'>2</span>")
                + " trong các học phần " + partText + " không đủ!");
              $("#valid_mes_tes_auto_part_all").html("<i class='fa fa-exclamation-circle'></i> Các học phần " + partText
                + " không có đủ số lượng câu hỏi " + (this.difficultDisplay.seT_CHOICEVALUE == 'Bằng chữ' ? "<span class='easy-color'>Trung bình Dễ</span>" : "<span class='very-easy-color'>2</span>")
                + "!");
              return false;
            }
            break;
          case 3:
            if ($("#field_tes_auto_dif_3").val() > countM) {
              $("#valid_mes_tes_auto_dif_all").html("<i class='fa fa-exclamation-circle'></i> Số lượng câu hỏi "
                + (this.difficultDisplay.seT_CHOICEVALUE == 'Bằng chữ' ? "<span class='medium-color'>Trung bình</span>" : "<span class='very-easy-color'>3</span>")
                + " trong các học phần " + partText + " không đủ!");
              $("#valid_mes_tes_auto_part_all").html("<i class='fa fa-exclamation-circle'></i> Các học phần " + partText
                + " không có đủ số lượng câu hỏi " + (this.difficultDisplay.seT_CHOICEVALUE == 'Bằng chữ' ? "<span class='medium-color'>Trung bình</span>" : "<span class='very-easy-color'>3</span>")
                + "!");
              return false;
            }
            break;
          case 4:
            if ($("#field_tes_auto_dif_4").val() > countH) {
              $("#valid_mes_tes_auto_dif_all").html("<i class='fa fa-exclamation-circle'></i> Số lượng câu hỏi "
                + (this.difficultDisplay.seT_CHOICEVALUE == 'Bằng chữ' ? "<span class='hard-color'>Trung bình Khó</span>" : "<span class='very-easy-color'>4</span>")
                + " trong các học phần " + partText + " không đủ!");
              $("#valid_mes_tes_auto_part_all").html("<i class='fa fa-exclamation-circle'></i> Các học phần " + partText
                + " không có đủ số lượng câu hỏi " + (this.difficultDisplay.seT_CHOICEVALUE == 'Bằng chữ' ? "<span class='hard-color'>Trung bình Khó</span>" : "<span class='very-easy-color'>4</span>")
                + "!");
              return false;
            }
            break;
          case 5:
            if ($("#field_tes_auto_dif_5").val() > countVH) {
              $("#valid_mes_tes_auto_dif_all").html("<i class='fa fa-exclamation-circle'></i> Số lượng câu hỏi "
                + (this.difficultDisplay.seT_CHOICEVALUE == 'Bằng chữ' ? "<span class='very-hard-color'>Khó</span>" : "<span class='very-easy-color'>5</span>")
                + " trong các học phần " + partText + " không đủ!");
              $("#valid_mes_tes_auto_part_all").html("<i class='fa fa-exclamation-circle'></i> Các học phần " + partText
                + " không có đủ số lượng câu hỏi " + (this.difficultDisplay.seT_CHOICEVALUE == 'Bằng chữ' ? "<span class='very-hard-color'>Khó</span>" : "<span class='very-easy-color'>5</span>")
                + "!");
              return false;
            }
            break;
          default:
            break;
        }
      }

      $("#valid_mes_tes_auto_dif_all").html("");
      return true;
    }
  }

  validate_tes_shuffletest() {
    if (this.shuffle == 2 && this.arrShuffleTest.length == 0) {
      $("#field_tes_10").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_10").html("Vui lòng chọn nút tạo mã đề!");
      return false;
    }
    else {
      $("#field_tes_10").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_tes_10").html("");
      return true;
    }
  }

  validate() {
    var check = true;
    if (this.tabControlAddupd == 1) {
      if (this.validate_tes_name() == false)
        check = false;
      if (this.validate_tes_testtype() == false)
        check = false;
      if (this.validate_tes_maxscore() == false)
        check = false;
      if (this.validate_tes_password() == false)
        check = false;

      this.validate_tes_note();
    }
    else if (this.tabControlAddupd == 2) {
      if (this.validate_tes_testformat() == false)
        check = false;
      if (this.validate_tes_numquestion() == false)
        check = false;
      if (this.validate_tes_time() == false)
        check = false;
      if (this.validate_tes_shuffle() == false)
        check = false;
    }
    else if (this.tabControlAddupd == 3) {
      if (this.countQuestionPick != this.numberOfQuestion) {
        check = false;
      }
    }
    else if (this.tabControlAddupd == 4) {
      if (this.validate_tes_shuffletest() == false)
        check = false;
    }

    //validate all
    if (check == false) {
      $("#valid_tes_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_tes_" + id).is(":visible")) {
      $("#buttonicon_tes_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_tes_" + id).slideToggle();
    }
    else {
      $("#buttonicon_tes_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_tes_" + id).slideToggle();
    }
  }

  ToggleGroupMulti(id: number) {
    if ($("#detailToggle_tes_mul_" + id).is(":visible")) {
      $("#buttonicon_tes_mul_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_tes_mul_" + id).slideToggle();
    }
    else {
      $("#buttonicon_tes_mul_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_tes_mul_" + id).slideToggle();
    }
  }

  openModal(id: number = 0) {
    this.unlockModal("addupd");

    // //test
    // for (let i = 1; i <= 4; i++) {
    //   if (i == 3) {
    //     $("#tab_addupd_tes_" + i).addClass("active");
    //     $("#content_addupd_tes_" + i).removeClass("fade").addClass("active");
    //   }
    //   else {
    //     $("#tab_addupd_tes_" + i).removeClass("active");
    //     $("#content_addupd_tes_" + i).removeClass("active").addClass("fade");
    //   }
    // }

    // this.tabControlAddupd = 3;
    // //endtest

    if (id > 0) { // update
      this.correctPassword = false;
      this.password = "";
      // console.log(this.correctPassword);
      this.checkField();
      this.testSer.get(id).subscribe(res => {
        this.test = res.data;
        // console.log(res.data);
        this.testQuestions = res.data.test_Questions;
        this.testFormat = res.data.testingFormats[0].tesF_ISSHOWPART == true ? 2 : 1;
        this.showScore = res.data.testingFormats[0].tesF_ISSHOWSCORE;
        this.shuffle = res.data.teS_ISSHUFFLE == true ? 2 : 1;
        this.numberOfQuestion = res.data.countQuestion;
        this.countQuestionPick = res.data.countQuestion;
        for (let i = 0; i < res.data.test_Questions.length; i++) {
          this.listTestQuestionOld.push(res.data.test_Questions[i]);
        }

        this.arrShuffleTest = [];
        // console.log(this.testQuestions);

        if (res.data.test_Details != null && res.data.test_Details[0] != null) {
          for (let i = 0; i < res.data.test_Details.length; i++) {
            if (this.listTestShuffleView.length == 0) {
              this.listTestShuffleView.push(res.data.test_Details[i].tedE_CODE);
            }
            else {
              if (this.listTestShuffleView.find(x => x == res.data.test_Details[i].tedE_CODE) == null) {
                this.listTestShuffleView.push(res.data.test_Details[i].tedE_CODE);
              }
            }
            this.numberOfTest = this.listTestShuffleView.length;
          }

          // console.log(this.listTestShuffleView);
          //listTestShuffle
          for (let i = 0; i < this.listTestShuffleView.length; i++) {
            let listQuestion = [];
            let listOption = [];
            let listTypeQuestion = [];
            let listQuestionPassage = [];
            for (let j = 0; j < res.data.test_Details.length; j++) {
              if (res.data.test_Details[j].tedE_CODE == this.listTestShuffleView[i]) {
                if (res.data.test_Details[j].tedE_QUEID != null) {
                  listQuestion.push(res.data.test_Details[j].tedE_QUEID);
                  listOption.push(res.data.test_Details[j].tedE_QUEOPTION);
                  listTypeQuestion.push(1);
                  listQuestionPassage.push(null);
                }
                else {
                  let listQuestionOfPassage = [];
                  let listOptionOfPassage = [];
                  for (let k = 0; k < res.data.test_Details[j].tedE_PASQUESTION.split(".").length; k++) {
                    listQuestionOfPassage.push(Number(res.data.test_Details[j].tedE_PASQUESTION.split(".")[k]));
                    listOptionOfPassage.push(res.data.test_Details[j].tedE_PASOPTION.split(".")[k]);
                  }
                  listQuestion.push(res.data.test_Details[j].tedE_PASID);
                  listOption.push(null);
                  listTypeQuestion.push(2);
                  listQuestionPassage.push({ listQuestionOfPassage: listQuestionOfPassage, listOptionOfPassage: listOptionOfPassage } as QuestionOfPassage)
                }
              }
            }
            this.arrShuffleTest.push({ code: this.listTestShuffleView[i], listQuestion: listQuestion, listOption: listOption, listTypeQuestion: listTypeQuestion, listQuestionPassage: listQuestionPassage } as TestShuffle)
          }

          for (let i = 1; i <= 4; i++) {
            if (i == 4) {
              $("#tab_addupd_tes_" + i).addClass("active");
              $("#content_addupd_tes_" + i).removeClass("fade").addClass("active");
            }
            else {
              $("#tab_addupd_tes_" + i).removeClass("active");
              $("#content_addupd_tes_" + i).removeClass("active").addClass("fade");
            }
          }

          this.questionSer.getListBySubjectId(this.test.teS_SUBID).subscribe(res1 => {
            this.questionsList = res1.data;
          });
          this.passageSer.getListBySubjectId(this.test.teS_SUBID).subscribe(res1 => {
            this.passagesList = res1.data;
          });
          this.subjectSer.get(res.data.teS_SUBID).subscribe(res1 => {
            this.subject = res1.data;
          });

        }
      });

      $("#modal_title_tes").html("Sửa đề thi");
      this.modal.show();
    }
    else { // insert
      this.countQuestionPick = 0;
      this.test = {
        id: 0,
        teS_SUBID: 0,
        teS_STATUS: 1,
      } as Test

      this.clearField();
      $("#modal_title_tes").html("Thêm đề thi");
      this.modal.show();

      // // test
      // this.test.teS_SUBID = 4;
      // this.test.teS_TIME = 30;
      // this.test.teS_TETYID = 1;
      // this.numberOfQuestion = 5;
      // this.countQuestionPick = 0;
      // this.subjectSer.get(this.test.teS_SUBID).subscribe(res => {
      //   this.subject = res.data;
      // });
      // this.shuffle = 2;
      // // endtest
    }
  }

  save() {

    if (this.validate() == false) {
      return false;
    }

    this.testRequest = {
      test: this.test,
      testFormat: this.testFormat,
      testQuestion: this.testQuestions,
      numberOfQuestion: this.numberOfQuestion,
      numberOfTest: this.numberOfTest,
      testShuffle: this.arrShuffleTest,
      shuffle: this.shuffle,
      showScore: this.showScore
    } as TestRequest;
    // console.log(this.testRequest);

    this.lockModal("addupd");
    if (this.test.id === 0) { // insert
      this.test.teS_CREATEDBY = parseInt(this.cookieSer.get('Id'));
      // this.test.teS_CODE = "0" + this.test.teS_TETYID + this.subject.suB_CODE + "0".repeat(10 - (this.tests.length + 1).toString().length) + (this.tests.length + 1).toString();

      this.testSer.add(this.testRequest).subscribe(res => {
        if (res.errorCode === 0) {
          this.testSer.getAll().subscribe(res1 => {
            this.tests = res1.data;
            this.rerender();
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Thêm đề thi thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
    else { // update
      this.test.teS_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.testSer.put(this.testRequest).subscribe(res => {
        if (res.errorCode === 0) {
          this.testSer.getAll().subscribe(res1 => {
            this.tests = res1.data;
            this.rerender();
            this.modal.hide();
            if (this.modalDetail.isShown) {
              this.openModalDetail(res.data.id);
            }
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Sửa đề thi thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
  }

  openModalDetail(id: number) {
    this.correctPassword = false;
    this.password = "";
    this.arrShuffleTest = [];
    this.testSer.get(id).subscribe(res => {
      this.test = res.data;
      // console.log(res.data);

      this.questionSer.getListBySubjectId(this.test.teS_SUBID).subscribe(res1 => {
        this.questionsList = res1.data;
      });
      this.passageSer.getListBySubjectId(this.test.teS_SUBID).subscribe(res1 => {
        this.passagesList = res1.data;
      });
      this.subjectSer.get(res.data.teS_SUBID).subscribe(res1 => {
        this.subject = res1.data;
      });

      this.testQuestions = res.data.test_Questions;
      this.testFormat = res.data.testingFormats[0].tesF_ISSHOWPART == true ? 2 : 1;
      this.arrShuffleTest = [];
      // console.log(this.testQuestions);

      if (res.data.test_Details != null && res.data.test_Details[0] != null) {
        for (let i = 0; i < res.data.test_Details.length; i++) {
          if (this.listTestShuffleView.length == 0) {
            this.listTestShuffleView.push(res.data.test_Details[i].tedE_CODE);
          }
          else {
            if (this.listTestShuffleView.find(x => x == res.data.test_Details[i].tedE_CODE) == null) {
              this.listTestShuffleView.push(res.data.test_Details[i].tedE_CODE);
            }
          }


        }
        // console.log(this.listTestShuffleView);
        //listTestShuffle
        for (let i = 0; i < this.listTestShuffleView.length; i++) {
          let listQuestion = [];
          let listOption = [];
          let listTypeQuestion = [];
          let listQuestionPassage = [];
          for (let j = 0; j < res.data.test_Details.length; j++) {
            if (res.data.test_Details[j].tedE_CODE == this.listTestShuffleView[i]) {
              if (res.data.test_Details[j].tedE_QUEID != null) {
                listQuestion.push(res.data.test_Details[j].tedE_QUEID);
                listOption.push(res.data.test_Details[j].tedE_QUEOPTION);
                listTypeQuestion.push(1);
                listQuestionPassage.push(null);
              }
              else {
                let listQuestionOfPassage = [];
                let listOptionOfPassage = [];
                for (let k = 0; k < res.data.test_Details[j].tedE_PASQUESTION.split(".").length; k++) {
                  listQuestionOfPassage.push(Number(res.data.test_Details[j].tedE_PASQUESTION.split(".")[k]));
                  listOptionOfPassage.push(res.data.test_Details[j].tedE_PASOPTION.split(".")[k]);
                }
                listQuestion.push(res.data.test_Details[j].tedE_PASID);
                listOption.push(null);
                listTypeQuestion.push(2);
                listQuestionPassage.push({ listQuestionOfPassage: listQuestionOfPassage, listOptionOfPassage: listOptionOfPassage } as QuestionOfPassage)
              }
            }
          }
          this.arrShuffleTest.push({ code: this.listTestShuffleView[i], listQuestion: listQuestion, listOption: listOption, listTypeQuestion: listTypeQuestion, listQuestionPassage: listQuestionPassage } as TestShuffle)
        }
      }
      // console.log(this.arrShuffleTest);

      this.test_Id = res.data.id;
      this.modalDetail.show();
    });
  }

  stopMedia() {
    $(".testpreview_media").trigger("pause");
    $(".testpreview_media").prop("currentTime", 0);
  }

  resetDetailModal() {
    this.test_Id = null;
    //reset tab
    for (let i = 1; i <= 2; i++) {
      if (i == 1) {
        $("#tab_tes_" + i).addClass("active");
        $("#content_tes_" + i).addClass("active");
      }
      else {
        $("#tab_tes_" + i).removeClass("active");
        $("#content_tes_" + i).removeClass("active").addClass("fade");
      }
    }
    this.loadTab(1);
  }

  lockModal(key: string) {
    $(".close_modal_tes_" + key).prop("disabled", true);
    $("#save_modal_tes_" + key).prop("disabled", true);
    $("#save_modal_tes_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_tes_" + key).prop("disabled", false);
    $("#save_modal_tes_" + key).prop("disabled", false);
    $("#save_modal_tes_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  loadTab(key: number) {
    this.tabControl = key;
  }

  // loadTabAddupd(key: number) {
  //   this.tabControlAddupd = key;
  // }

  nextStep() {
    if (this.validate() == false) {
      return false;
    }

    if (this.tabControlAddupd == 1) {
      for (let i = 0; i <= 5; i++) {
        $("#field_tes_" + i).prop("disabled", true);
      }

      for (let i = 1; i <= 4; i++) {
        if (i == 2) {
          $("#tab_addupd_tes_" + i).addClass("active");
          $("#content_addupd_tes_" + i).removeClass("fade").addClass("active");
        }
        else {
          $("#tab_addupd_tes_" + i).removeClass("active");
          $("#content_addupd_tes_" + i).removeClass("active").addClass("fade");
        }
      }

      this.tabControlAddupd = 2;
      this.subjectSer.get(this.test.teS_SUBID).subscribe(res => {
        this.subject = res.data;
      });
    }
    else if (this.tabControlAddupd == 2) {
      for (let i = 6; i <= 9; i++) {
        $("#field_tes_" + i).prop("disabled", true);
      }

      for (let i = 1; i <= 4; i++) {
        if (i == 3) {
          $("#tab_addupd_tes_" + i).addClass("active");
          $("#content_addupd_tes_" + i).removeClass("fade").addClass("active");
        }
        else {
          $("#tab_addupd_tes_" + i).removeClass("active");
          $("#content_addupd_tes_" + i).removeClass("active").addClass("fade");
        }
      }

      this.tabControlAddupd = 3;
    }
    else if (this.tabControlAddupd == 3) {

      for (let i = 1; i <= 4; i++) {
        if (i == 4) {
          $("#tab_addupd_tes_" + i).addClass("active");
          $("#content_addupd_tes_" + i).removeClass("fade").addClass("active");
        }
        else {
          $("#tab_addupd_tes_" + i).removeClass("active");
          $("#content_addupd_tes_" + i).removeClass("active").addClass("fade");
        }
      }

      if (this.test.id != 0) {
        //check number of ques
        if (this.numberOfQuestion != this.test.countQuestion) {
          this.arrShuffleTest = [];
        }
        //check listquestion different
        else {
          let check = true;
          console.log(this.testQuestions);
          console.log(this.listTestQuestionOld);
          if (this.testQuestions.length != this.listTestQuestionOld.length) {
            this.arrShuffleTest = [];
          }
          else {
            for (let i = 0; i < this.testQuestions.length; i++) {
              if (this.testQuestions[i].teqU_QUEID != null) {
                if (this.listTestQuestionOld.find(x => x.teqU_QUEID == this.testQuestions[i].teqU_QUEID) == null) {
                  check = false;
                  break;
                }
              }
              else {
                if (this.listTestQuestionOld.find(x => x.teqU_PASID == this.testQuestions[i].teqU_PASID) == null) {
                  check = false;
                  break;
                }
              }
            }

            if (check == false) {
              this.arrShuffleTest = [];
            }
          }
        }
      }

      this.tabControlAddupd = 4;
      this.numberOfTest = this.arrShuffle[0];
      this.questionSer.getListBySubjectId(this.test.teS_SUBID).subscribe(res => {
        this.questionsList = res.data;
      });
      this.passageSer.getListBySubjectId(this.test.teS_SUBID).subscribe(res => {
        this.passagesList = res.data;
      });

      console.log(this.arrShuffleTest);
    }

  }

  prevStep() {
    if (this.tabControlAddupd == 2) {
      for (let i = 0; i <= 5; i++) {
        $("#field_tes_" + i).prop("disabled", false);
      }

      for (let i = 1; i <= 4; i++) {
        if (i == 1) {
          $("#tab_addupd_tes_" + i).addClass("active");
          $("#content_addupd_tes_" + i).removeClass("fade").addClass("active");
        }
        else {
          $("#tab_addupd_tes_" + i).removeClass("active");
          $("#content_addupd_tes_" + i).removeClass("active").addClass("fade");
        }
      }

      this.tabControlAddupd = 1;
    }
    else if (this.tabControlAddupd == 3) {
      for (let i = 6; i <= 9; i++) {
        $("#field_tes_" + i).prop("disabled", false);
      }

      for (let i = 1; i <= 4; i++) {
        if (i == 2) {
          $("#tab_addupd_tes_" + i).addClass("active");
          $("#content_addupd_tes_" + i).removeClass("fade").addClass("active");
        }
        else {
          $("#tab_addupd_tes_" + i).removeClass("active");
          $("#content_addupd_tes_" + i).removeClass("active").addClass("fade");
        }
      }
      this.testQuestions = [];
      this.listQuestionPick = [];
      this.listPassagePick = [];
      this.countQuestionPick = 0;
      this.tabControlAddupd = 2;
    }
    else if (this.tabControlAddupd == 4) {

      for (let i = 1; i <= 4; i++) {
        if (i == 3) {
          $("#tab_addupd_tes_" + i).addClass("active");
          $("#content_addupd_tes_" + i).removeClass("fade").addClass("active");
        }
        else {
          $("#tab_addupd_tes_" + i).removeClass("active");
          $("#content_addupd_tes_" + i).removeClass("active").addClass("fade");
        }
      }
      this.tabControlAddupd = 3;

      if (this.test.id > 0) {
        this.listQuestionPick = [];
        this.listPassagePick = [];
        for (let i = 0; i < this.test.test_Questions.length; i++) {
          if (this.test.test_Questions[i].teqU_QUEID != null) {
            this.listQuestionPick.push(this.test.test_Questions[i].teqU_QUEID);
          }
          else {
            this.listPassagePick.push(this.test.test_Questions[i].teqU_PASID);
          }
        }
      }
    }
  }

  openModalAddQuestion() {
    if (this.partPercentState == true) {
      $("#button_partPercent").click();
    }
    if (this.difficultPercentState == true) {
      $("#button_difficultPercent").click();
    }
    this.unlockModal("addque");
    this.questions = null;
    this.passages = null;
    $("#valid_mes_tes_addquestion").html("");
    this.numberOfQuestionLeft = this.numberOfQuestion - this.countQuestionPick;
    this.countQuestionPickLeft = 0;
    this.questionSer.getListBySubjectId(this.test.teS_SUBID).subscribe(res => {

      for (let i = 0; i < res.data.length; i++) {
        if (this.listQuestionPick.find(x => x == res.data[i].id) != null) {
          // delete res.data[i];
          res.data.splice(i, 1);
          i--;
        }
      }
      if (this.questions != null) {
        this.questions = res.data;
        this.rerender1();
      }
      else {
        this.questions = res.data;
        this.dtTrigger1.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
      }
    });
    this.passageSer.getListBySubjectId(this.test.teS_SUBID).subscribe(res => {
      for (let i = 0; i < res.data.length; i++) {
        if (this.listPassagePick.find(x => x == res.data[i].id) != null) {
          // delete res.data[i];
          res.data.splice(i, 1);
          i--;
        }
      }
      if (this.passages != null) {
        for (let i = 0; i < res.data.length; i++) {
          if (res.data[i].paS_MEDIA != null) {
            if ($.inArray(res.data[i].paS_MEDIA.substr(res.data[i].paS_MEDIA.length - 3, res.data[i].paS_MEDIA.length), this.videoExtension) != -1) {
              res.data[i].mediaType = "video";
            }
            else {
              res.data[i].mediaType = "audio";
            }
          }
        }
        this.passages = res.data;
        this.rerender2();
      }
      else {
        for (let i = 0; i < res.data.length; i++) {
          if (res.data[i].paS_MEDIA != null) {
            if ($.inArray(res.data[i].paS_MEDIA.substr(res.data[i].paS_MEDIA.length - 3, res.data[i].paS_MEDIA.length), this.videoExtension) != -1) {
              res.data[i].mediaType = "video";
            }
            else {
              res.data[i].mediaType = "audio";
            }
          }
        }
        this.passages = res.data;
        this.dtTrigger2.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_sub1" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
      }
    });

    this.modalAddQuestion.show();
  }

  addQuestion() {
    if (this.countQuestionPickLeft == 0) {
      $("#valid_mes_tes_addquestion").html("Không có câu hỏi nào được chọn!");
      $("#valid_tes_que_all").show(0).delay(3000).hide(0);
      return false;
    }
    else if (this.countQuestionPickLeft > this.numberOfQuestionLeft) {
      $("#valid_mes_tes_addquestion").html("Số lượng câu hỏi được chọn vượt quá số lượng câu hỏi cần thiết của đề thi!");
      $("#valid_tes_que_all").show(0).delay(3000).hide(0);
      return false;
    }
    else {
      this.lockModal("addque");
      this.countQuestionPick += this.countQuestionPickLeft;
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            //que
            if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
              let arr = [];
              dtInstance.rows({ selected: true }).every(function () {
                if (this.data()[1] != null) {
                  arr.push(this.data()[1]);
                }
              });
              // console.log(this.questionsData);
              for (let i = 0; i < arr.length; i++) {
                this.testQuestions.push({ id: 0, teqU_TESID: null, teqU_QUEID: arr[i], teqU_PASID: null, teqU_CREATEDBY: null, teqU_CREATEDDATE: null } as TestQuestion);
                this.testQuestions[this.testQuestions.length - 1].questions = this.questions.find(x => x.id == this.testQuestions[this.testQuestions.length - 1].teqU_QUEID);
                this.listQuestionPick.push(arr[i]);
              }
              this.sortMarkQuestion();
            }
            //pas
            if (dtInstance.table().node().id == "tb_sub1" && dtInstance != null) {
              let arr = [];
              dtInstance.rows({ selected: true }).every(function () {
                if (this.data()[1] != null) {
                  arr.push(this.data()[1]);
                }
              });
              for (let i = 0; i < arr.length; i++) {
                this.testQuestions.push({ id: 0, teqU_TESID: null, teqU_QUEID: null, teqU_PASID: arr[i], teqU_CREATEDBY: null, teqU_CREATEDDATE: null } as TestQuestion);
                this.testQuestions[this.testQuestions.length - 1].passages = this.passages.find(x => x.id == this.testQuestions[this.testQuestions.length - 1].teqU_PASID);
                this.listPassagePick.push(arr[i]);
              }
            }
          });
        }
      });

      if ($(".toggle-tes-que-all").closest('tr').hasClass('selected')) {
        $(".toggle-tes-que-all").closest('tr').toggleClass('selected');
      }
      if ($(".toggle-tes-pas-all").closest('tr').hasClass('selected')) {
        $(".toggle-tes-pas-all").closest('tr').toggleClass('selected');
      }

      this.modalAddQuestion.hide();
    }
  }

  questionPick(key: number, id: number) {
    if (key == 1) {
      if ($("#table_quepick_row_" + id).hasClass('selected') == true) {
        this.countQuestionPickLeft--;
      }
      else {
        this.countQuestionPickLeft++;
      }
    }
    if (key == 2) {
      if ($("#table_paspick_row_" + id).hasClass('selected') == true) {
        this.countQuestionPickLeft -= parseInt($("#numQuestionOfPas_" + id).html());
      }
      else {
        this.countQuestionPickLeft += parseInt($("#numQuestionOfPas_" + id).html());
      }
    }
  }

  loadPartPercent() {
    this.partPercentState = !this.partPercentState;
    if (this.partPercentState == true && this.difficultPercentState == true) {
      $("#button_difficultPercent").click();
    }
    if (this.test.teS_SUBID != null || this.test.teS_SUBID != 0) {
      this.partPercentHTML = "";
      for (let i = 0; i < this.subject.parts.length; i++) {
        let count = 0;
        this.partPercentHTML += '<div class="col-12"><div class="row"><div class="col-4 pr-0"><span>';
        for (let j = 0; j < this.testQuestions.length; j++) {
          if (this.testQuestions[j].teqU_QUEID != null && this.testQuestions[j].questions.quE_PARID == this.subject.parts[i].id) {
            count++;
          }
          if (this.testQuestions[j].teqU_PASID != null && this.testQuestions[j].passages.paS_PARID == this.subject.parts[i].id) {
            count += this.testQuestions[j].passages.questions.length;
          }
        }
        this.partPercentHTML += count + "/" + this.numberOfQuestion + " (<span class='text-success'>" + Math.round((count / this.numberOfQuestion) * 100)
          + "%</span>)</span></div><div class='col-8'><span>" + this.subject.parts[i].paR_NAME + "</span></div></div></div>";
        if (i < this.subject.parts.length - 1) {
          this.partPercentHTML += "<div class='col-12'><hr class='my-1'></div>"
        }
      }
    }
  }

  loadDifficultPercent() {
    this.difficultPercentState = !this.difficultPercentState;
    if (this.difficultPercentState == true && this.partPercentState == true) {
      $("#button_partPercent").click();
    }
    if (this.test.teS_SUBID != null || this.test.teS_SUBID != 0) {
      this.difficultPercentHTML = "";
      for (let i = 1; i <= 5; i++) {
        let count = 0;
        this.difficultPercentHTML += '<div class="col-12"><div class="row"><div class="col-4 pr-0"><span>';
        for (let j = 0; j < this.testQuestions.length; j++) {
          if (this.testQuestions[j].teqU_QUEID != null && this.testQuestions[j].questions.quE_LEVEL == i) {
            count++;
          }
          if (this.testQuestions[j].teqU_PASID != null) {
            for (let k = 0; k < this.testQuestions[j].passages.questions.length; k++) {
              if (this.testQuestions[j].passages.questions[k].quE_LEVEL == i) {
                count++;
              }
            }
          }
        }
        this.difficultPercentHTML += count + "/" + this.numberOfQuestion + " (<span class='" + this.difficultClass[i - 1] + "'>" + Math.round((count / this.numberOfQuestion) * 100)
          + "%</span>)</span></div><div class='col-8'><span>Mức độ <span class='" + this.difficultClass[i - 1] + "'>"
          + (i == 1 ? (this.difficultDisplay.seT_CHOICEVALUE == 'Bằng chữ' ? "dễ" : i) : (i == 2 ? (this.difficultDisplay.seT_CHOICEVALUE == 'Bằng chữ' ? "trung bình dễ" : i) : (i == 3 ? (this.difficultDisplay.seT_CHOICEVALUE == 'Bằng chữ' ? "trung bình" : i) : (i == 4 ? (this.difficultDisplay.seT_CHOICEVALUE == 'Bằng chữ' ? "khó" : i) : (this.difficultDisplay.seT_CHOICEVALUE == 'Bằng chữ' ? "trung bình khó" : i)))))
          + "</span></span></div></div></div>";
        if (i < this.subject.parts.length - 1) {
          this.partPercentHTML += "<div class='col-12'><hr class='my-1'></div>"
        }
      }
    }
  }

  removeQuestion(type: number, id: number, event = null) {
    if (event != null) {
      event.preventDefault();
    }
    if (this.tabControlAddupd == 3) {
      if (type == 1) {
        this.countQuestionPick--;

        const deletedItem = this.testQuestions.find(x => x.teqU_QUEID == id);
        const index = this.testQuestions.indexOf(deletedItem);
        this.testQuestions.splice(index, 1);

        const deletedItem1 = this.listQuestionPick.find(x => x == id);
        const index1 = this.listQuestionPick.indexOf(deletedItem1);
        this.listQuestionPick.splice(index1, 1);

        if (this.question != null && this.question.id == id) {
          this.question = null;
        }
      }
      if (type == 2) {
        this.countQuestionPick -= this.testQuestions.find(x => x.teqU_PASID == id).passages.questions.length;

        const deletedItem = this.testQuestions.find(x => x.teqU_PASID == id);
        const index = this.testQuestions.indexOf(deletedItem);
        this.testQuestions.splice(index, 1);

        const deletedItem1 = this.listPassagePick.find(x => x == id);
        const index1 = this.listPassagePick.indexOf(deletedItem1);
        this.listPassagePick.splice(index1, 1);

        if (this.passage != null && this.passage.id == id) {
          this.passage = null;
        }
      }

      this.sortMarkQuestion();
    }
    // console.log(this.testQuestions);
    // console.log(this.listQuestionPick);
  }

  sortMarkQuestion() {
    let count = 0;
    for (let i = 0; i < this.testQuestions.length; i++) {
      if (this.testQuestions[i].teqU_QUEID != null) {
        $("#markQuestion_" + this.testQuestions[i].teqU_QUEID).html((count + 1).toString());
        count++;
      }
    }
  }

  showQuestion(type: number, id: number, event = null, extendPassageId: number = null) {
    if (event != null) {
      event.preventDefault();
    }
    if (type == 1) {
      this.passage = null;
      this.questionSer.get(id).subscribe(res => {
        this.question = res.data;

        //media
        if (res.data.quE_MEDIA != null) {
          if ($.inArray(res.data.quE_MEDIA.substr(res.data.quE_MEDIA.length - 3, res.data.quE_MEDIA.length), this.videoExtension) != -1) {
            res.data.mediaType = "video";
          }
          else {
            res.data.mediaType = "audio";
          }
        }
      });
    }
    if (type == 2) {
      this.question = null;
      this.passageSer.get(id).subscribe(res => {
        this.passage = res.data;
        location.hash = "#markLocationPas_" + id;

        //media
        if (res.data.paS_MEDIA != null) {
          if ($.inArray(res.data.paS_MEDIA.substr(res.data.paS_MEDIA.length - 3, res.data.paS_MEDIA.length), this.videoExtension) != -1) {
            res.data.mediaType = "video";
          }
          else {
            res.data.mediaType = "audio";
          }
        }
      });
    }
    if (type == 3) {
      this.question = null;
      if (this.passage != null && this.passage.id == extendPassageId) {
        location.hash = "#markLocation_" + id;
      }
      else {
        this.passageSer.get(extendPassageId).subscribe(res => {
          this.passage = res.data;
          location.hash = "#markLocation_" + id;

          //media
          if (res.data.paS_MEDIA != null) {
            if ($.inArray(res.data.paS_MEDIA.substr(res.data.paS_MEDIA.length - 3, res.data.paS_MEDIA.length), this.videoExtension) != -1) {
              res.data.mediaType = "video";
            }
            else {
              res.data.mediaType = "audio";
            }
          }
        });
      }
    }
  }

  openModalAutoGenerateTest() {
    //reset modal
    this.countQuestionPickAuto = 0;
    this.countQuestionPickDifAuto = 0;
    this.totalVEasy = 0;
    this.totalEasy = 0;
    this.totalMedium = 0;
    this.totalHard = 0;
    this.totalVHard = 0;
    if (this.partPercentState == true) {
      $("#button_partPercent").click();
    }
    if (this.difficultPercentState == true) {
      $("#button_difficultPercent").click();
    }
    this.checkOptionKey = 0;
    $("#field_tes_auto_general_1").prop("checked", false);
    $("#field_tes_auto_general_2").prop("checked", false);
    for (let i = 0; i < this.subject.parts.length; i++) {
      $("#field_tes_auto_" + this.subject.parts[i].id).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_tes_auto_" + this.subject.parts[i].id).html("");
    }
    for (let i = 1; i <= 5; i++) {
      $("#field_tes_auto_dif_" + i).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_tes_auto_dif_" + i).html("");
    }
    $("#valid_mes_tes_auto_dif_all").html("");
    $("#valid_mes_tes_auto_part_all").html("");


    //show modal
    this.unlockModal("auto");
    this.questionSer.getListBySubjectId(this.test.teS_SUBID).subscribe(res => {
      this.questionsAuto = res.data;
    });
    this.passageSer.getListBySubjectId(this.test.teS_SUBID).subscribe(res => {
      this.passagesAuto = res.data;
    });

    this.modalAutoGenerateTest.show();
  }

  generateTest() {
    if (this.checkOptionKey != 0) {
      //valid field
      if (this.checkOptionKey == 1) {
        for (let i = 0; i < this.subject.parts.length; i++) {
          if (this.validate_tes_auto_numQuestion(1, this.subject.parts[i].id) == false) {
            $("#valid_tes_auto_all").show(0).delay(3000).hide(0);
            return false;
          }
        }
      }
      else if (this.checkOptionKey == 2) {
        for (let i = 1; i <= 5; i++) {
          if (this.validate_tes_auto_numQuestion(2, i) == false) {
            $("#valid_tes_auto_all").show(0).delay(3000).hide(0);
            return false;
          }
        }
      }
      else {
        for (let i = 0; i < this.subject.parts.length; i++) {
          if (this.validate_tes_auto_numQuestion(1, this.subject.parts[i].id) == false) {
            $("#valid_tes_auto_all").show(0).delay(3000).hide(0);
            return false;
          }
        }
        for (let i = 1; i <= 5; i++) {
          if (this.validate_tes_auto_numQuestion(2, i) == false) {
            $("#valid_tes_auto_all").show(0).delay(3000).hide(0);
            return false;
          }
        }
      }
      //valid total question
      if (this.validate_tes_auto_all() == false) {
        $("#valid_tes_auto_all").show(0).delay(3000).hide(0);
        return false
      }
    }

    this.lockModal("auto");
    if (this.checkOptionKey == 0) {
      this.testQuestions = [];
      for (let i = 0; i < this.numberOfQuestion; i++) {
        if (this.testQuestions.length == this.questionsAuto.length)
          break;
        let id;
        do {
          id = this.questionsAuto[Math.floor(Math.random() * this.questionsAuto.length)].id;
        } while (this.testQuestions.find(x => x.teqU_QUEID == id) != null);
        this.testQuestions.push({ id: 0, teqU_TESID: null, teqU_QUEID: id, teqU_PASID: null, teqU_CREATEDBY: null, teqU_CREATEDDATE: null } as TestQuestion);
        this.testQuestions[this.testQuestions.length - 1].questions = this.questionsAuto.find(x => x.id == this.testQuestions[this.testQuestions.length - 1].teqU_QUEID);
        this.listQuestionPick.push(id);
      }
      this.sortMarkQuestion();
    }
    else if (this.checkOptionKey == 1) {
      this.testQuestions = [];
      for (let i = 0; i < this.subject.parts.length; i++) {
        if ($("#field_tes_auto_" + this.subject.parts[i].id).val() != 0) {
          for (let j = 0; j < $("#field_tes_auto_" + this.subject.parts[i].id).val(); j++) {
            let id;
            do {
              id = this.questionsAuto[Math.floor(Math.random() * this.questionsAuto.length)].id;
            } while (!(this.testQuestions.find(x => x.teqU_QUEID == id) == null && this.questionsAuto.find(x => x.id == id).quE_PARID == this.subject.parts[i].id));
            this.testQuestions.push({ id: 0, teqU_TESID: null, teqU_QUEID: id, teqU_PASID: null, teqU_CREATEDBY: null, teqU_CREATEDDATE: null } as TestQuestion);
            this.testQuestions[this.testQuestions.length - 1].questions = this.questionsAuto.find(x => x.id == this.testQuestions[this.testQuestions.length - 1].teqU_QUEID);
            this.listQuestionPick.push(id);
          }
        }
      }
      this.sortMarkQuestion();
    }
    else if (this.checkOptionKey == 2) {
      this.testQuestions = [];
      for (let i = 1; i <= 5; i++) {
        if ($("#field_tes_auto_dif_" + i).val() != 0) {
          for (let j = 0; j < $("#field_tes_auto_dif_" + i).val(); j++) {
            let id;
            do {
              id = this.questionsAuto[Math.floor(Math.random() * this.questionsAuto.length)].id;
            } while (!(this.testQuestions.find(x => x.teqU_QUEID == id) == null && this.questionsAuto.find(x => x.id == id).quE_LEVEL == i));
            this.testQuestions.push({ id: 0, teqU_TESID: null, teqU_QUEID: id, teqU_PASID: null, teqU_CREATEDBY: null, teqU_CREATEDDATE: null } as TestQuestion);
            this.testQuestions[this.testQuestions.length - 1].questions = this.questionsAuto.find(x => x.id == this.testQuestions[this.testQuestions.length - 1].teqU_QUEID);
            this.listQuestionPick.push(id);
          }
        }
      }
      this.sortMarkQuestion();
    }
    else {
      this.testQuestions = [];
      let arrPart = [];
      for (let i = 0; i < this.subject.parts.length; i++) {
        arrPart.push($("#field_tes_auto_" + this.subject.parts[i].id).val());
      }
      for (let i = 1; i <= 5; i++) {
        if ($("#field_tes_auto_dif_" + i).val() != 0) {
          let id;
          let index;
          let check
          do {
            id = this.questionsAuto[Math.floor(Math.random() * this.questionsAuto.length)].id;
            check = true;
            for (let j = 0; j < arrPart.length; j++) {
              if (this.subject.parts[j].id == this.questionsAuto.find(x => x.id == id).parts.id) {
                if (arrPart[j] == 0) {
                  let check = false;
                }
                index = j;
              }
            }
          } while (!(this.testQuestions.find(x => x.teqU_QUEID == id) == null && this.questionsAuto.find(x => x.id == id).quE_LEVEL == i && check == true));
          this.testQuestions.push({ id: 0, teqU_TESID: null, teqU_QUEID: id, teqU_PASID: null, teqU_CREATEDBY: null, teqU_CREATEDDATE: null } as TestQuestion);
          this.testQuestions[this.testQuestions.length - 1].questions = this.questionsAuto.find(x => x.id == this.testQuestions[this.testQuestions.length - 1].teqU_QUEID);
          this.listQuestionPick.push(id);
          arrPart[index]--;
          // console.log(this.testQuestions);
          // console.log(arrPart);
          // console.log("*".repeat(10));
        }
      }
      if (this.countQuestionPickAuto > this.testQuestions.length) {
        for (let i = 0; i < this.subject.parts.length; i++) {
          if (arrPart[i] > 0) {
            for (let j = 0; j < arrPart[i]; j++) {
              let id;
              do {
                id = this.questionsAuto[Math.floor(Math.random() * this.questionsAuto.length)].id;
              } while (!(this.testQuestions.find(x => x.teqU_QUEID == id) == null && this.questionsAuto.find(x => x.id == id).quE_PARID == this.subject.parts[i].id));
              this.testQuestions.push({ id: 0, teqU_TESID: null, teqU_QUEID: id, teqU_PASID: null, teqU_CREATEDBY: null, teqU_CREATEDDATE: null } as TestQuestion);
              this.testQuestions[this.testQuestions.length - 1].questions = this.questionsAuto.find(x => x.id == this.testQuestions[this.testQuestions.length - 1].teqU_QUEID);
              this.listQuestionPick.push(id);
            }
          }
        }
      }
    }

    this.countQuestionPick = this.testQuestions.length;
    this.modalAutoGenerateTest.hide();
  }

  checkOption(key: number) {
    if ($("#field_tes_auto_general_1").is(":checked") == false && $("#field_tes_auto_general_2").is(":checked") == false) {
      this.checkOptionKey = 0;
    }
    else if ($("#field_tes_auto_general_1").is(":checked") == true && $("#field_tes_auto_general_2").is(":checked") == false) {
      this.checkOptionKey = 1;
    }
    else if ($("#field_tes_auto_general_1").is(":checked") == false && $("#field_tes_auto_general_2").is(":checked") == true) {
      this.checkOptionKey = 2;

      this.totalVEasy = 0;
      this.totalEasy = 0;
      this.totalMedium = 0;
      this.totalHard = 0;
      this.totalVHard = 0;

      for (let i = 0; i < this.questionsAuto.length; i++) {
        if (this.questionsAuto[i].quE_LEVEL == 1)
          this.totalVEasy++;
        if (this.questionsAuto[i].quE_LEVEL == 2)
          this.totalEasy++;
        if (this.questionsAuto[i].quE_LEVEL == 3)
          this.totalMedium++;
        if (this.questionsAuto[i].quE_LEVEL == 4)
          this.totalHard++;
        if (this.questionsAuto[i].quE_LEVEL == 5)
          this.totalVHard++;
      }
    }
    else {
      this.checkOptionKey = 3;

      this.totalVEasy = 0;
      this.totalEasy = 0;
      this.totalMedium = 0;
      this.totalHard = 0;
      this.totalVHard = 0;

      for (let i = 0; i < this.questionsAuto.length; i++) {
        if (this.questionsAuto[i].quE_LEVEL == 1)
          this.totalVEasy++;
        if (this.questionsAuto[i].quE_LEVEL == 2)
          this.totalEasy++;
        if (this.questionsAuto[i].quE_LEVEL == 3)
          this.totalMedium++;
        if (this.questionsAuto[i].quE_LEVEL == 4)
          this.totalHard++;
        if (this.questionsAuto[i].quE_LEVEL == 5)
          this.totalVHard++;
      }
    }
  }

  savePreview(showCode: number, testCode: string = "") {
    this.showCode = showCode;
    this.testCode = testCode;

    this.informationSer.getAll().subscribe(res => {
      this.informations = res.data;
      this.organizationName = res.data.find(x => x.inF_KEYNAME == "Tên tổ chức").inF_VALUE;
      this.logoUrl = res.data.find(x => x.inF_KEYNAME == "Logo").fileUrl;
      this.testTypeSer.get(this.test.teS_TETYID).subscribe(res1 => {
        this.testtype = res1.data;
      });
    });

    if (showCode == 1) {
      //main test
      if (this.testFormat == 2) {
        this.testQuestionsView = this.testQuestions;
        let step = 1;
        let count;
        for (let i = 0; i < this.subject.parts.length; i++) {
          count = 0;
          for (let j = 0; j < this.testQuestionsView.length; j++) {
            if (this.testQuestionsView[j].teqU_QUEID != null && this.testQuestionsView[j].questions.quE_PARID == this.subject.parts[i].id) {
              this.testQuestionsView[j].questions.index = step;
              step++;
              count++;

              //media
              if (this.testQuestionsView[i].questions.quE_MEDIA != null) {
                if ($.inArray(this.testQuestionsView[i].questions.quE_MEDIA.substr(this.testQuestionsView[i].questions.quE_MEDIA.length - 3, this.testQuestionsView[i].questions.quE_MEDIA.length), this.videoExtension) != -1) {
                  this.testQuestionsView[i].questions.mediaType = "video";
                }
                else {
                  this.testQuestionsView[i].questions.mediaType = "audio";
                }
              }
            }
            else if (this.testQuestionsView[j].teqU_PASID != null && this.testQuestionsView[j].passages.paS_PARID == this.subject.parts[i].id) {
              for (let k = 0; k < this.testQuestionsView[j].passages.questions.length; k++) {
                this.testQuestionsView[j].passages.questions[k].index = step;
                step++;
                count++;
              }

              //media passage
              if (this.testQuestionsView[i].passages.paS_MEDIA != null) {
                if ($.inArray(this.testQuestionsView[i].passages.paS_MEDIA.substr(this.testQuestionsView[i].passages.paS_MEDIA.length - 3, this.testQuestionsView[i].passages.paS_MEDIA.length), this.videoExtension) != -1) {
                  this.testQuestionsView[i].passages.mediaType = "video";
                }
                else {
                  this.testQuestionsView[i].passages.mediaType = "audio";
                }
              }
            }
          }
          this.subject.parts[i].noquestion = count;
        }
      }
      else {
        this.testQuestionsView = this.testQuestions;
        console.log(this.testQuestionsView);
        let step = 1;
        for (let i = 0; i < this.testQuestionsView.length; i++) {
          if (this.testQuestionsView[i].teqU_QUEID != null) {
            this.testQuestionsView[i].questions.index = step;
            step++;
            //media
            if (this.testQuestionsView[i].questions.quE_MEDIA != null) {
              if ($.inArray(this.testQuestionsView[i].questions.quE_MEDIA.substr(this.testQuestionsView[i].questions.quE_MEDIA.length - 3, this.testQuestionsView[i].questions.quE_MEDIA.length), this.videoExtension) != -1) {
                this.testQuestionsView[i].questions.mediaType = "video";
              }
              else {
                this.testQuestionsView[i].questions.mediaType = "audio";
              }
            }
          }
          if (this.testQuestionsView[i].teqU_PASID != null) {
            for (let j = 0; j < this.testQuestionsView[i].passages.questions.length; j++) {
              this.testQuestionsView[i].passages.questions[j].index = step;
              step++;
              //media question
              if (this.testQuestionsView[i].passages.questions[j].quE_MEDIA != null) {
                if ($.inArray(this.testQuestionsView[i].passages.questions[j].quE_MEDIA.substr(this.testQuestionsView[i].passages.questions[j].quE_MEDIA.length - 3, this.testQuestionsView[i].passages.questions[j].quE_MEDIA.length), this.videoExtension) != -1) {
                  this.testQuestionsView[i].passages.questions[j].mediaType = "video";
                }
                else {
                  this.testQuestionsView[i].passages.questions[j].mediaType = "audio";
                }
              }
            }
            //media passage
            if (this.testQuestionsView[i].passages.paS_MEDIA != null) {
              if ($.inArray(this.testQuestionsView[i].passages.paS_MEDIA.substr(this.testQuestionsView[i].passages.paS_MEDIA.length - 3, this.testQuestionsView[i].passages.paS_MEDIA.length), this.videoExtension) != -1) {
                this.testQuestionsView[i].passages.mediaType = "video";
              }
              else {
                this.testQuestionsView[i].passages.mediaType = "audio";
              }
            }
          }
        }
      }
    }
    //shuffle test
    else {
      this.testQuestionsView = [];
      let step = 1;
      let test = this.arrShuffleTest.find(x => x.code == testCode);
      for (let i = 0; i < test.listQuestion.length; i++) {
        if (test.listTypeQuestion[i] == 1) {
          let listOption = [];
          for (let j = 0; j < test.listOption[i].split("-").length; j++) {
            listOption.push(this.questionsList.find(x => x.id == test.listQuestion[i]).options.find(x => x.id == Number(test.listOption[i].split("-")[j])));
          }
          let question = this.questionsList.find(x => x.id == test.listQuestion[i]);
          question.options = listOption as [Option];
          question.index = step;
          step++;

          //media
          if (question.quE_MEDIA != null) {
            if ($.inArray(question.quE_MEDIA.substr(question.quE_MEDIA.length - 3, question.quE_MEDIA.length), this.videoExtension) != -1) {
              question.mediaType = "video";
            }
            else {
              question.mediaType = "audio";
            }
          }

          this.testQuestionsView.push({ teqU_QUEID: test.listQuestion[i], questions: question } as TestQuestion);
        }
        else {
          let listQuestion = [];
          for (let j = 0; j < test.listQuestionPassage[i].listQuestionOfPassage.length; j++) {
            let listOption = [];
            for (let k = 0; k < test.listQuestionPassage[i].listOptionOfPassage[j].split("-").length; k++) {
              listOption.push(this.passagesList.find(x => x.id == test.listQuestion[i]).questions.find(x => x.id == test.listQuestionPassage[i].listQuestionOfPassage[j]).options.find(x => x.id == Number(test.listQuestionPassage[i].listOptionOfPassage[j].split("-")[k])));
            }
            let question = this.passagesList.find(x => x.id == test.listQuestion[i]).questions.find(x => x.id == test.listQuestionPassage[i].listQuestionOfPassage[j]);
            question.options = listOption as [Option];
            question.index = step;
            step++;

            //media question
            if (question.quE_MEDIA != null) {
              if ($.inArray(question.quE_MEDIA.substr(question.quE_MEDIA.length - 3, question.quE_MEDIA.length), this.videoExtension) != -1) {
                question.mediaType = "video";
              }
              else {
                question.mediaType = "audio";
              }
            }

            listQuestion.push(question);
          }
          let passage = this.passagesList.find(x => x.id == test.listQuestion[i]);
          passage.questions = listQuestion as [Question];

          //media passage
          if (passage.paS_MEDIA != null) {
            if ($.inArray(passage.paS_MEDIA.substr(passage.paS_MEDIA.length - 3, passage.paS_MEDIA.length), this.videoExtension) != -1) {
              passage.mediaType = "video";
            }
            else {
              passage.mediaType = "audio";
            }
          }

          this.testQuestionsView.push({ teqU_PASID: test.listQuestion[i], passages: passage } as TestQuestion);
        }
      }
      // console.log(this.testQuestionsView);
    }

    this.modalSavePreview.show();
  }

  generateShuffleTest() {
    // let count = 0;
    this.arrShuffleTest = [];
    for (let i = 0; i < this.numberOfTest; i++) {
      let code;
      do {
        code = Math.floor(Math.random() * 1000);
      } while (this.arrShuffleTest.find(x => x.code == code) != null);
      let listQuestion = [];
      let listOption = [];
      let listTypeQuestion = [];
      let listQuestionOptionPassage = [];

      //log
      // console.log("number test: " + this.numberOfTest);
      // console.log("test code: " + code);

      //shuffle list
      let arrQuestionShuffle = this.shuffleArr(this.testQuestions);
      for (let i = 0; i < arrQuestionShuffle.length; i++) {
        if (arrQuestionShuffle[i].teqU_QUEID != null) {
          listQuestion.push(arrQuestionShuffle[i].teqU_QUEID);
          listTypeQuestion.push(1);
          listQuestionOptionPassage.push(null);

          if (arrQuestionShuffle[i].questions.quE_ISSHUFFLE == true) {
            let arrOptionOfQuestionShuffle = this.shuffleArr(arrQuestionShuffle[i].questions.options);
            let optionShuffle = "";
            for (let j = 0; j < arrOptionOfQuestionShuffle.length; j++) {
              optionShuffle += arrOptionOfQuestionShuffle[j].id;
              if (j != arrOptionOfQuestionShuffle.length - 1)
                optionShuffle += "-";
            }
            listOption.push(optionShuffle);
          }
          else {
            let optionShuffle = "";
            for (let j = 0; j < arrQuestionShuffle[i].questions.options.length; j++) {
              optionShuffle += arrQuestionShuffle[i].questions.options[j].id;
              if (j != arrQuestionShuffle[i].questions.options.length - 1)
                optionShuffle += "-";
            }
            listOption.push(optionShuffle);
          }
        }
        else if (arrQuestionShuffle[i].teqU_PASID != null) {
          listQuestion.push(arrQuestionShuffle[i].teqU_PASID);
          listTypeQuestion.push(2);
          listOption.push(null);

          if (arrQuestionShuffle[i].passages.paS_ISSHUFFLE == true) {
            let arrQuestionOfPassageShuffle = this.shuffleArr(arrQuestionShuffle[i].passages.questions);
            let listQuestionOfPassage = [];
            let listOptionOfPassage = [];
            for (let j = 0; j < arrQuestionOfPassageShuffle.length; j++) {
              listQuestionOfPassage.push(arrQuestionOfPassageShuffle[j].id);
              // console.log()
              if (arrQuestionOfPassageShuffle[j].quE_ISSHUFFLE == true) {
                // console.log("abc");
                let arrOptionOfPassageShuffle = this.shuffleArr(arrQuestionOfPassageShuffle[j].options);
                let optionShuffle = "";
                for (let k = 0; k < arrOptionOfPassageShuffle.length; k++) {
                  // listOptionOfPassage.push(arrOptionOfPassageShuffle[k].id);
                  optionShuffle += arrOptionOfPassageShuffle[k].id;
                  if (k != arrOptionOfPassageShuffle.length - 1)
                    optionShuffle += "-";
                }
                listOptionOfPassage.push(optionShuffle);
              }
              else {
                let optionShuffle = "";
                for (let k = 0; k < arrQuestionOfPassageShuffle[j].options.length; k++) {
                  // listOptionOfPassage.push(arrQuestionOfPassageShuffle[j].options[k].id);
                  optionShuffle += arrQuestionOfPassageShuffle[j].options[k].id;
                  if (k != arrQuestionOfPassageShuffle[j].options.length - 1)
                    optionShuffle += "-";
                }
                listOptionOfPassage.push(optionShuffle);
              }
            }
            listQuestionOptionPassage.push({ listQuestionOfPassage: listQuestionOfPassage, listOptionOfPassage: listOptionOfPassage } as QuestionOfPassage);
          }
          else {
            let listQuestionOfPassage = [];
            let listOptionOfPassage = [];
            for (let j = 0; j < arrQuestionShuffle[i].passages.questions.length; j++) {
              listQuestion.push(arrQuestionShuffle[i].passages.questions[j].id);
              if (arrQuestionShuffle[i].passages.questions[j].quE_ISSHUFFLE == true) {
                let arrOptionOfPassageShuffle = this.shuffleArr(arrQuestionShuffle[i].passages.questions[j].options);
                let optionShuffle = "";
                for (let k = 0; k < arrOptionOfPassageShuffle.length; k++) {
                  // listOptionOfPassage.push(arrOptionOfPassageShuffle[k].id);
                  optionShuffle += arrOptionOfPassageShuffle[k].id;
                  if (k != arrOptionOfPassageShuffle.length - 1)
                    optionShuffle += "-";
                }
                listOptionOfPassage.push(optionShuffle);
              }
              else {
                let optionShuffle = "";
                for (let k = 0; k < arrQuestionShuffle[i].passages.questions[j].options.length; k++) {
                  // listOptionOfPassage.push(arrQuestionShuffle[i].passages.questions[j].options[k].id);
                  optionShuffle += arrQuestionShuffle[i].passages.questions[j].options[k].id;
                  if (k != arrQuestionShuffle[i].passages.questions[j].options.length - 1)
                    optionShuffle += "-";
                }
                listOptionOfPassage.push(optionShuffle);
              }
            }
            listQuestionOptionPassage.push({ listQuestionOfPassage: listQuestionOfPassage, listOptionOfPassage: listOptionOfPassage } as QuestionOfPassage);
          }
        }
      }

      this.arrShuffleTest.push({ code: code.toString(), listQuestion: listQuestion, listOption: listOption, listTypeQuestion: listTypeQuestion, listQuestionPassage: listQuestionOptionPassage } as TestShuffle);
      // console.log(this.arrShuffleTest);

    }
  }

  shuffleArr(list) {
    return list.reduce((p, n) => {
      const size = p.length;
      const index = Math.trunc(Math.random() * (size - 1));
      p.splice(index, 0, n);
      return p;
    }, []);
  };

  setShuffle() {
    if (this.testFormat == 2) {
      this.shuffle = 1;
    }
  }

  checkOptionShowScore() {
    // if ($("#field_tes_11").is('checked') == true) {
    //   this.showScore = true;
    // }
    // else {
    //   this.showScore = false;
    // }
  }

  openConfirmModal(type: number) {
    this.unlockModal("confirm");
    this.tabControlAddupd == 1
    $("#field_tes_20").removeClass('is-valid is-invalid');
    $("#valid_mes_tes_20").html("");

    if (type == 1) {
      $("#confirmText").html("Bạn có chắc chắn xét duyệt đề thi này?");
    }
    this.modalConfirmBox.show();
  }

  acceptTest() {
    if (this.validate_tes_acceptPassword() == false) {
      return false;
    }
    this.lockModal("confirm");
    // console.log(this.acceptPassword);
    this.testSer.acceptTest({ id: this.test.id, userid: Number(this.cookieSer.get('Id')), password: this.acceptPassword } as AcceptRequest).subscribe(res => {
      if (res.errorCode === 0) {
        this.testSer.getAll().subscribe(res1 => {
          this.tests = res1.data;
          this.rerender();
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Duyệt đề thi thành công!'
          });
          // this.openModalDetail(res.data.id);
          this.modalDetail.hide();
        });
      }
      else {
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message
        });
      }
      this.modalConfirmBox.hide();
    });
  }

  checkTestPassword() {
    if (this.validate_tes_Unlockpassword() == false) {
      return false;
    }

    if (this.password == this.test.teS_PASSWORD) {
      this.correctPassword = true;
    }
    else {
      $("#field_tes_21").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tes_21").html("Mật khẩu không chính xác!");
      return false;
    }
  }

  openModalDelete(id: number) {
    this.unlockModal("del");
    this.correctPassword = false;
    this.password = "";
    this.testSer.get(id).subscribe(res => {
      this.test = res.data;
      this.modalDelete.show();
    });
  }

  delete() {
    this.lockModal("del");
    if (this.test.id != 0) {
      this.testSer.delete(this.test.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          this.testSer.getAll().subscribe(res1 => {
            this.tests = res1.data;
            this.rerender();
          });
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Xóa đề thi thành công!'
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
        this.modalDetail.hide();
      });
    }
  }

  // openModalDeleteMul() {
  //   this.unlockModal("muldel");
  //   this.dtElements.forEach((dtElement: DataTableDirective) => {
  //     if (dtElement.dtInstance != null) {
  //       dtElement.dtInstance.then((dtInstance: any) => {
  //         if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
  //           let arr = [];
  //           dtInstance.rows({ selected: true }).every(function () {
  //             if (this.data()[1] != null) {
  //               arr.push(this.data()[1]);
  //             }
  //           });
  //           if (arr.length == 0) {
  //             this.isListItemNull = true;
  //             this.modalDeletMul.show();
  //           }
  //           else {
  //             this.isListItemNull = false;
  //             this.testSer.getListTestMultiDel(arr).subscribe(res => {
  //               this.testsMultiDel = res.data;
  //               // console.log(this.subjectsMultiDel);
  //               this.modalDeletMul.show();
  //             });
  //           }
  //         }
  //       });
  //     }
  //   });
  // }

  // deleteMul() {
  //   this.lockModal("muldel");
  //   this.testSer.deleteMulti(this.testsMultiDel, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
  //     if (res.errorCode === 0) {
  //       this.testSer.getAll().subscribe(res1 => {
  //         this.tests = res1.data;
  //         this.rerender();
  //         if ($(".toggle-tes-all").closest('tr').hasClass('selected')) {
  //           $(".toggle-tes-all").closest('tr').toggleClass('selected');
  //         }
  //         this.modalDeletMul.hide();
  //         this.pnotify.success({
  //           title: 'Thông báo',
  //           text: "Xóa danh sách đề thi thành công!"
  //         });
  //       });
  //     }
  //     else {
  //       if ($(".toggle-tes-all").closest('tr').hasClass('selected')) {
  //         $(".toggle-tes-all").closest('tr').toggleClass('selected');
  //       }
  //       this.modalDeletMul.hide();
  //       this.pnotify.error({
  //         title: 'Thông báo',
  //         text: res.message
  //       });
  //     }
  //   });
  // }
}
