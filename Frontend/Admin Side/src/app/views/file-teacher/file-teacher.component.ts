// import { Component, OnInit, ViewChild } from '@angular/core';
// import { FileUpload } from '../../services/file-upload.service';
// import { Subject } from 'rxjs';
// import { ModalDirective } from 'ngx-bootstrap';
// import { DataTableDirective } from 'angular-datatables';
// import { PnotifyService } from '../../services/pnotify.service';
// import { CookieService } from 'ngx-cookie-service';
// import { UserPermissionService } from '../../services/user-permission.service';
// import { Router } from '@angular/router';
// import { DomSanitizer } from '@angular/platform-browser';
// import { FileService } from '../../services/file.service';
// import { ClassService, Class } from '../../services/class.service';

// @Component({
//   selector: 'app-file-teacher',
//   templateUrl: './file-teacher.component.html',
//   styleUrls: ['./file-teacher.component.scss']
// })
// export class FileTeacherComponent implements OnInit {

//   pnotify = undefined;
//   url: any = "";
//   file: File = null;
//   old_filename: string = "";

//   id_class: number = 0;
//   id_user = parseInt(this.cookieSer.get('Id'));
//   classes: [Class];
//   class: Class = {} as Class;
//   show: number = 0;

//   files: [FileUpload];
//   fileupload: FileUpload;

//   dtTrigger: Subject<any> = new Subject();
//   @ViewChild('modal', { static: false }) modal: ModalDirective;
//   @ViewChild(DataTableDirective, { static: false })
//   dtElement: DataTableDirective;
//   dtOptions: any = {};

//   constructor(private pNotify: PnotifyService, private claSer: ClassService,
//     private cookieSer: CookieService,
//     private userPerSer: UserPermissionService, private router: Router,
//     private _DomSanitizer: DomSanitizer, //unsafe bug fix
//     private fileSer: FileService) {
//     this.pnotify = this.pNotify.getPNotify();
//   }

//   ngOnInit() {
//     if (this.cookieSer.check('isLogin') == false || this.cookieSer.get('isLogin') == '0') {
//       this.router.navigate(['/login']);
//     }
//     if (parseInt(this.cookieSer.get('Role')) == 3) {
//     }
//     else {
//       this.router.navigate(['/404']);
//     };
//     this.claSer.getAttendance(this.id_user).subscribe(res => {
//       this.classes = res.data;
//     });
//   }
//   loadFile() {
//     this.claSer.get(this.id_class).subscribe(res => {
//       this.class = res.data;
//       this.show = 1;
//     })
//   }
//   onSelectFile(event) { // called each time file input changes
//     if (event.target.files && event.target.files[0]) {
//       var reader = new FileReader();

//       reader.readAsDataURL(event.target.files[0]); // read file as data url
//       this.file = event.target.files[0];

//       reader.onload = (event: any) => { // called once readAsDataURL is completed
//         this.url = event.target.result;
//       }

//       $("#label_field_1").html(
//         this.file.name.length > 20 ?
//           this.file.name.substring(0, 5) + "..." + this.file.name.substring(this.file.name.length - 9, this.file.name.length) :
//           this.file.name
//       );
//     }
//   }
//   validate_stt() {
//     if ($("#field_2").val() == null) {
//       $("#field_2").removeClass('is-valid').addClass('is-invalid');
//       $("#valid_mes_2").html("Vui lòng chọn trạng thái tập tin!");
//       return false;
//     }
//     else {
//       $("#field_2").removeClass('is-invalid').addClass('is-valid');
//       $("#valid_mes_2").html("");
//       return true;
//     }
//   }
//   showModal(event = null, id: Number = 0) {
//     if (event != null) {
//       event.preventDefault();
//     }
//     if (id > 0) {
//       // this.checkField()
//       this.fileSer.get(id).subscribe(res => {
//         this.address = res.data;
//         $("#modal_title").html("Sửa địa chỉ");
//         this.modal.show();
//       });
//     }
//     else {
//       this.clearField()
//       this.address = {
//         id: 0
//       } as Address
//       $("#modal_title").html("Thêm địa chỉ");
//       this.modal.show();
//     }
//   }

// }
