import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService, User } from '../../services/user.service';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { DomSanitizer } from '@angular/platform-browser';
import { FileService } from '../../services/file.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Employee, EmployeeService } from '../../services/employee.service';
import { Student, StudentService } from '../../services/student.service';
import { PermissionService, Permission } from '../../services/permission.service';
import { EmployeeRequest } from '../../models/employee_request';
import { UserPermissionService, User_Permission } from '../../services/user-permission.service';
import { StudentRequest } from '../../models/student_request';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { PositionService, Position } from '../../services/position.service';
import { EmployeePositionService, EmployeePosition } from '../../services/employee-position.service';
import { ExportService } from '../../services/export.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  fileExtension = '.xlsx';
  exportData: any = [];
  id_check: any[] = [];

  users: [User];
  permissions: [Permission];
  positions: [Position];
  employees: [Employee];
  students: [Student];
  user_permissions: [User_Permission];
  employee_positions: [EmployeePosition];
  user: User = {} as User;
  pnotify = undefined;
  employee: Employee = {} as Employee;
  student: Student = {} as Student;
  emp_req: EmployeeRequest = {} as EmployeeRequest;
  stu_req: StudentRequest = {} as StudentRequest;
  url: any = "";
  file: File = null;
  list_per: any = [];
  list_pos: any = [];
  old_filename: string = "";
  dtTrigger: Subject<any> = new Subject();
  date: string = "";
  dtOptions: any = {};

  isClick: number = 0;
  isClick2: number = 0;
  userPers: [User_Permission];
  maxDate: Date;

  @ViewChild('modalemp', { static: false }) modalemp: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild('modalViewemp', { static: false }) modalViewemp: ModalDirective;
  @ViewChild('modalstu', { static: false }) modalstu: ModalDirective;
  @ViewChild('modalViewstu', { static: false }) modalViewstu: ModalDirective;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  constructor(private userSer: UserService, private pNotify: PnotifyService,
    private cookieSer: CookieService,
    private _DomSanitizer: DomSanitizer, //unsafe bug fix
    private fileSer: FileService, private permissionSer: PermissionService,
    private userPerSer: UserPermissionService, private router: Router,
    private employeeSer: EmployeeService, private studentSer: StudentService,
    private userpermissionSer: UserPermissionService,
    private positionSer: PositionService, private employeepositionSer: EmployeePositionService, private exportService: ExportService) {
    this.pnotify = this.pNotify.getPNotify();
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate());
  }

  ngOnInit() {
    // if (this.cookieSer.check('isLogin') == false || this.cookieSer.get('isLogin') == '0') {
    //   this.router.navigate(['/login']);
    // }
    // if (parseInt(this.cookieSer.get('Role')) == 1) {
    // }
    // else {
    //   this.router.navigate(['/404']);
    // }
    // this.userPerSer.getlistperbyuserid(parseInt(this.cookieSer.get('Id'))).subscribe(res => {
    //   this.userPers = res.data;
    //   let check = 0;
    //   for (let i = 0; i < this.userPers.length; i++) {
    //     if (this.userPers[i].useP_PERID == 1) {
    //       check = 1;
    //       break;
    //     }
    //   }
    //   if (check === 0) {
    //     this.router.navigate(['/404']);
    //   }
    // });
    this.dtOptions = {
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      order: [[1, 'asc']]
    };
    this.userSer.getAll().subscribe(res => {
      this.users = res.data;
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
    this.permissionSer.getAll().subscribe(res => {
      this.permissions = res.data;
    });
    this.positionSer.getAll().subscribe(res => {
      this.positions = res.data;
    });
    this.employeeSer.getAll().subscribe(res => {
      this.employees = res.data;
    });
    this.studentSer.getAll().subscribe(res => {
      this.students = res.data;
    })
    this.employee.emP_AVATAR = "";
    // this.student.stU_IMAGE = "";
  }
  clickDOB() {
    this.isClick = 1;
  }
  clickDOB2() {
    this.isClick2 = 1;
  }
  export(event = null) {
    if (event != null) {
      event.preventDefault();
    }
    this.exportData = [];
    this.dtElement.dtInstance.then((dtInstance: any) => {
      let tmp = [];
      dtInstance.rows({ selected: true }).every(function () {
        if (this.data()[1] != null) {
          tmp.push(this.data()[1]);
        }
      });
      this.id_check = null;
      this.id_check = tmp;
      var id_ex = '';
      for (let i = 0; i < this.id_check.length; i++) {
        if (id_ex == '') {
          id_ex = this.id_check[i].toString();
        }
        else {
          id_ex = id_ex + "_" + this.id_check[i].toString();
        }
      }
      if (id_ex == '') id_ex = 'full';
      // this.userSer.getAllDataExport(id_ex).subscribe(res => {
      //   if (res.data != null) {
      //     for (let i = 0; i < res.data.length; i++) {
      //       this.exportData.push({
      //         STT: i + 1,
      //         'Tài khoản': res.data[i].username,
      //         'Loại tài khoản': res.data[i].user_type,
      //         'Trạng thái': res.data[i].status == 0 ? 'Ngưng sử dụng' : 'Sử dụng',
      //         Họ: res.data[i].lastname,
      //         Tên: res.data[i].firstname,
      //         'Ngày sinh': res.data[i].dob == null ? '' : res.data[i].dob.toString().substring(0, 10).split('-').reverse().join('/'),
      //         'Giới tính': res.data[i].gender == true ? 'Nam' : 'Nữ',
      //         'Số điện thoại': res.data[i].phone == null ? '' : res.data[i].phone,
      //         Email: res.data[i].email,
      //         'Địa chỉ': res.data[i].address == null ? '' : res.data[i].address,
      //       });
      //     }
      //     this.exportService.exportExcel(this.exportData, 'Danh sách tài khoản');
      //     this.pnotify.success({
      //       title: 'Thông báo',
      //       text: 'Xuất excel thành công!'
      //     });
      //     this.id_check = null;
      //   }
      // });
    });
  }
  Select_Clear_All() {
    $(".toggle-all").closest('tr').toggleClass('selected');
    if ($(".toggle-all").closest('tr').hasClass('selected')) {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.select();
        });
      });
    }
    else {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.deselect();
        });
      });
    }
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  onKeydown(event = null) {
    if (event != null) {
      event.preventDefault();
    }
  }
  // export(event = null) {
  //   if (event != null) {
  //     event.preventDefault();
  //   }
  //   this.userSer.getAllDataExport().subscribe(res => {
  //     if (res.data != null) {
  //       for (let i = 0; i < res.data.length; i++) {
  //         let use_per = '';
  //         let emp_pos = '';
  //         //set emp_pos
  //         for (let k = 0; k < res.data[i].employee_Positions.length; k++) {
  //           if (k == 0) {
  //             emp_pos += res.data[i].employee_Positions[k].positions.poS_NAME;
  //           }
  //           else {
  //             emp_pos += ", " + res.data[i].employee_Positions[k].positions.poS_NAME;
  //           }
  //         }
  //         //check exist user
  //         if (res.data[i].users == null) {
  //           this.exportData.push({
  //             STT: i + 1,
  //             Họ: res.data[i].emP_LASTNAME,
  //             Tên: res.data[i].emP_FIRSTNAME,
  //             'Ngày sinh': res.data[i].emP_DATEOFBIRTH == null ? '' : res.data[i].emP_DATEOFBIRTH.toString().substring(0, 10).split('-').reverse().join('/'),
  //             'Giới tính': res.data[i].emP_GENDER == true ? 'Nữ' : 'Nam',
  //             'Số điện thoại': res.data[i].emP_PHONE,
  //             Email: res.data[i].emP_EMAIL,
  //             'Chức vụ': emp_pos,
  //             'Trạng thái': res.data[i].emP_STATUS == 1 ? 'Đang làm việc' : 'Đã nghỉ việc',
  //             'Tên tài khoản': '',
  //             'Nhóm quyền': use_per,
  //             'Trạng thái tài khoản': ''
  //           });
  //         }
  //         else {
  //           for (let j = 0; j < res.data[i].users.user_Permissions.length; j++) {
  //             if (j == 0) {
  //               use_per += res.data[i].users.user_Permissions[j].permissions.peR_NAME;
  //             }
  //             else {
  //               use_per += ", " + res.data[i].users.user_Permissions[j].permissions.peR_NAME;
  //             }
  //           }
  //           this.exportData.push({
  //             STT: i + 1,
  //             Họ: res.data[i].emP_LASTNAME,
  //             Tên: res.data[i].emP_FIRSTNAME,
  //             'Ngày sinh': res.data[i].emP_DATEOFBIRTH == null ? '' : res.data[i].emP_DATEOFBIRTH.toString().substring(0, 10).split('-').reverse().join('/'),
  //             'Giới tính': res.data[i].emP_GENDER == true ? 'Nữ' : 'Nam',
  //             'Số điện thoại': res.data[i].emP_PHONE,
  //             Email: res.data[i].emP_EMAIL,
  //             'Chức vụ': emp_pos,
  //             'Trạng thái': res.data[i].emP_STATUS == 1 ? 'Đang làm việc' : 'Đã nghỉ việc',
  //             'Tên tài khoản': res.data[i].users.usE_USERNAME,
  //             'Nhóm quyền': use_per,
  //             'Trạng thái tài khoản': res.data[i].users.usE_STATUS == 1 ? 'Kích hoạt' : 'Khóa'
  //           });
  //         }
  //       }

  //       // //Excel Title, Header, Data
  //       // const header = ['STT', 'Họ', 'Tên', 'Ngày sinh', 'Giới tính', 'Số điện thoại', 'Email', 'Chức vụ', 'Trạng thái', 'Tên tài khoản', 'Nhóm quyền', 'Trạng thái tài khoản'];
  //       // const data = this.exportData;
  //       // const Excel = require('exceljs');
  //       // //Create workbook and worksheet
  //       // let workbook = new Excel.Workbook();
  //       // let worksheet = workbook.addWorksheet('Danh sách nhân viên');
  //       // //Add Header Row
  //       // let headerRow = worksheet.addRow(header);
  //       // // Cell Style : Fill and Border
  //       // headerRow.eachCell((cell, number) => {
  //       //   cell.fill = {
  //       //     type: 'pattern',
  //       //     pattern: 'solid',
  //       //     fgColor: { argb: 'DCE6F1' }
  //       //   }
  //       //   cell.font = { name: 'Cambria', family: 4, bold: true, size: 13 };
  //       //   cell.alignment = { horizontal: 'center' };
  //       //   cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
  //       // })
  //       // // Add Data and Conditional Formatting
  //       // data.forEach((element) => {
  //       //   let eachRow = [];
  //       //   header.forEach((headers) => {
  //       //     eachRow.push(element[headers])
  //       //   })
  //       //   if (element.isDeleted === "Y") {
  //       //     let deletedRow = worksheet.addRow(eachRow);
  //       //     deletedRow.eachCell((cell, number) => {
  //       //       cell.font = { name: 'Cambria', family: 4, size: 13, bold: false, strike: true };
  //       //       cell.alignment = { wrapText: true, vertical: 'middle' };
  //       //     })
  //       //   } else {
  //       //     let insertRow = worksheet.addRow(eachRow);
  //       //     insertRow.eachCell((cell, number) => {
  //       //       cell.font = { name: 'Cambria', family: 4, size: 13, bold: false, strike: false };
  //       //       cell.alignment = { wrapText: true, vertical: 'middle' };
  //       //     })
  //       //   }
  //       // })
  //       // worksheet.getColumn(1).width = 6;
  //       // worksheet.getColumn(2).width = 24;
  //       // worksheet.getColumn(3).width = 15;
  //       // worksheet.getColumn(4).width = 16;
  //       // worksheet.getColumn(4).alignment = { horizontal: 'center', wrapText: true, vertical: 'middle' };
  //       // worksheet.getColumn(5).width = 15;
  //       // worksheet.getColumn(5).alignment = { horizontal: 'center', wrapText: true, vertical: 'middle' };
  //       // worksheet.getColumn(6).width = 17;
  //       // worksheet.getColumn(6).alignment = { horizontal: 'center', wrapText: true, vertical: 'middle' };
  //       // worksheet.getColumn(7).width = 47;
  //       // worksheet.getColumn(8).width = 35;
  //       // worksheet.getColumn(9).width = 16;
  //       // worksheet.getColumn(9).alignment = { horizontal: 'center', wrapText: true, vertical: 'middle' };
  //       // worksheet.getColumn(10).width = 25;
  //       // worksheet.getColumn(10).alignment = { horizontal: 'center', wrapText: true, vertical: 'middle' };
  //       // worksheet.getColumn(11).width = 34;
  //       // worksheet.getColumn(12).width = 34;
  //       // worksheet.getColumn(12).alignment = { horizontal: 'center', wrapText: true, vertical: 'middle' };
  //       // worksheet.addRow([]);
  //       // //Generate Excel File with given name
  //       // workbook.xlsx.writeBuffer().then((data) => {
  //       //   let blob = new Blob([data], { type: this.fileType });
  //       //   FileSaver.saveAs(blob, 'Danh sách nhân viên' + this.fileExtension);
  //       // });

  //       // this.exportService.exportAsExcelFile(this.exportData, 'Danh sách nhân viên',
  //       //   ['STT', 'Họ', 'Tên', 'Ngày sinh', 'Giới tính', 'Số điện thoại', 'Email', 'Chức vụ', 'Trạng thái', 'Tên tài khoản', 'Nhóm quyền', 'Trạng thái tài khoản']);
  //       // this.pnotify.success({
  //       //   title: 'Thông báo',
  //       //   text: 'Xuất excel thành công!'
  //       // });

  //       this.exportService.exportExcel(this.exportData, 'Danh sách nhân viên');
  //       this.pnotify.success({
  //         title: 'Thông báo',
  //         text: 'Xuất excel thành công!'
  //       });

  //     }
  //   });
  // }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  onSelectFile(event, par: string) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      this.file = event.target.files[0];

      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      }

      $("#label_field_7" + par).html(
        this.file.name.length > 20 ?
          this.file.name.substring(0, 5) + "..." + this.file.name.substring(this.file.name.length - 9, this.file.name.length) :
          this.file.name
      );
    }
  }
  validate_use_username(par: string) {
    if ($("#field_1" + par).val() == '') {
      $("#field_1" + par).removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_1" + par).html("Vui lòng nhập tên tài khoản!");
      return false;
    }
    else if ($("#field_1" + par).val().toString().length <= 6) {
      $("#field_1" + par).removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_1" + par).html("Tên tài khoản phải trên 6 ký tự!");
      return false;
    }
    else {
      let isExist = false;
      for (let i = 0; i < this.users.length; i++) {
        if ($("#field_1" + par).val().toString().trim().toLowerCase() == this.users[i].usE_USERNAME.toLowerCase() && this.users[i].id != this.user.id) {
          isExist = true;
          break;
        }
      }
      if (isExist) {
        $("#field_1" + par).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_1" + par).html("Tên tài khoản đã tồn tại! Vui lòng nhập tên khác (Ví dụ: " + $("#field_1" + par).val() + "123, " + $("#field_1" + par).val() + "abc,...)");
        return false;
      }
      $("#field_1" + par).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_1" + par).html("");
      return true;
    }
  }
  validate_use_password(par: string) {
    if ($("#field_2" + par).val() == '') {
      $("#field_2" + par).removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_2" + par).html("Vui lòng nhập mật khẩu!");
      return false;
    }
    else if ($("#field_2" + par).val().toString().length <= 6) {
      $("#field_2" + par).removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_2" + par).html("Mật khẩu phải trên 6 ký tự!");
      return false;
    }
    else {
      $("#field_2" + par).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_2" + par).html("");
      return true;
    }
  }
  validate_use_cfpassword(par: string) {
    if ($("#field_3" + par).val() == '') {
      $("#field_3" + par).removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_3" + par).html("Vui lòng nhập xác nhận mật khẩu!");
      return false;
    }
    else if ($("#field_3" + par).val() != $("#field_2").val()) {
      $("#field_3" + par).removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_3" + par).html("Xác nhận mật khẩu không khớp!");
      return false;
    }
    else {
      $("#field_3" + par).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_3" + par).html("");
      return true;
    }
  }
  validate_use_firstname(par: string) {
    var regex = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?0123456789]/;
    if ($("#field_4" + par).val() == '') {
      $("#field_4" + par).removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_4" + par).html("Vui lòng nhập họ tên lót!");
      return false;
    }
    else {
      if (regex.test($("#field_4" + par).val().toString()) == true) {
        $("#field_4" + par).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_4" + par).html("Vui lòng nhập họ tên lót chỉ chứa chữ cái!");
        return false;
      }
      else {
        $("#field_4" + par).removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_4" + par).html("");
        return true;
      }
    }
  }
  validate_use_lastname(par: string) {
    var regex = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?0123456789]/;
    if ($("#field_5" + par).val() == '') {
      $("#field_5" + par).removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_5" + par).html("Vui lòng nhập tên!");
      return false;
    }
    else {
      if (regex.test($("#field_5" + par).val().toString()) == true) {
        $("#field_5" + par).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_5" + par).html("Vui lòng nhập tên chỉ chứa chữ cái!");
        return false;
      }
      else {
        $("#field_5" + par).removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_5" + par).html("");
        return true;
      }
    }
  }
  validate_use_phone(par: string) {
    var regex = /((09|03|07|08|05)+([0-9]{8})\b)/;
    var regex2 = /^\d+$/;
    var regex3 = /((024|0203|0204|0205|0206|0207|0208|0209|0210|0211|0212|0213|0214|0215|0216|0218|0219|0220|0222|0225|0226|0227|0228|0229|0235|0236|0237|0238|0239|0252|0255|0256|0257|0258|0259|0260|0261|0262|0263|028|0251|0254|0270|0271|0272|0273|0274|0275|0276|0277|0290|0291|0292|0293|0294|0296|0297|0299|0269)+(3|6)+([0-9]{7})\b)/;
    if ((par != '') && ($("#field_6" + par).val() == '')) {
      $("#field_6" + par).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_6" + par).html("");
      return true;
    }
    else {
      if ($("#field_6" + par).val() == '') {
        $("#field_6" + par).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_6" + par).html("Vui lòng nhập số điện thoại!");
        return false;
      }
      else
        if (regex.test($("#field_6" + par).val().toString()) == false || regex2.test($("#field_6" + par).val().toString()) == false) {
          if (regex.test($("#field_6" + par).val().toString()) == false || regex3.test($("#field_6" + par).val().toString()) == false) {
            $("#field_6" + par).removeClass('is-valid').addClass('is-invalid');
            $("#valid_mes_6" + par).html("Vui lòng nhập đúng số điện thoại!");
            return false;
          }
          else {
            if ($("#field_6" + par).val().toString().length != 10) {
              $("#field_6" + par).removeClass('is-valid').addClass('is-invalid');
              $("#valid_mes_6" + par).html("Vui lòng nhập đúng số điện thoại!");
              return false;
            }
            else {
              let isExist = false;
              if (par != '') {
                for (let i = 0; i < this.students.length; i++) {
                  if (this.students[i].stU_PHONE != null) {
                    if ($("#field_6" + par).val().toString().trim().toLowerCase() == this.students[i].stU_PHONE.toLowerCase() && this.students[i].id != this.student.id) {
                      isExist = true;
                      break;
                    }
                  }
                }
              }
              else {
                for (let i = 0; i < this.employees.length; i++) {
                  if ($("#field_6" + par).val().toString().trim().toLowerCase() == this.employees[i].emP_PHONE.toLowerCase() && this.employees[i].id != this.employee.id) {
                    isExist = true;
                    break;
                  }
                }
              }
              if (isExist) {
                $("#field_6" + par).removeClass('is-valid').addClass('is-invalid');
                $("#valid_mes_6" + par).html("Số điện thoại đã tồn tại!");
                return false;
              }
              $("#field_6" + par).removeClass('is-invalid').addClass('is-valid');
              $("#valid_mes_6" + par).html("");
              return true;
            }
          }
        }
        else {
          if ($("#field_6" + par).val().toString().length != 10) {
            $("#field_6" + par).removeClass('is-valid').addClass('is-invalid');
            $("#valid_mes_6" + par).html("Vui lòng nhập đúng số điện thoại!");
            return false;
          }
          else {
            let isExist = false;
            if (par != '') {
              for (let i = 0; i < this.students.length; i++) {
                if (this.students[i].stU_PHONE != null) {
                  if ($("#field_6" + par).val().toString().trim().toLowerCase() == this.students[i].stU_PHONE.toLowerCase() && this.students[i].id != this.student.id) {
                    isExist = true;
                    break;
                  }
                }
              }
            }
            else {
              for (let i = 0; i < this.employees.length; i++) {
                if ($("#field_6" + par).val().toString().trim().toLowerCase() == this.employees[i].emP_PHONE.toLowerCase() && this.employees[i].id != this.employee.id) {
                  isExist = true;
                  break;
                }
              }
            }
            if (isExist) {
              $("#field_6" + par).removeClass('is-valid').addClass('is-invalid');
              $("#valid_mes_6" + par).html("Số điện thoại đã tồn tại!");
              return false;
            }
            $("#field_6" + par).removeClass('is-invalid').addClass('is-valid');
            $("#valid_mes_6" + par).html("");
            return true;
          }
        }
    }
  }
  validate_use_identity(par: string) {
    // var regex = /(([0-9])\b)/;
    var regex = /^\d+$/;
    if ($("#field_20" + par).val() == '') {
      $("#field_20" + par).removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_20" + par).html("Vui lòng nhập số chứng minh nhân dân!");
      return false;
    }
    if ((regex.test($("#field_20" + par).val().toString()) == true && ($("#field_20" + par).val().toString()).trim().length == 9)
      || (regex.test($("#field_20" + par).val().toString()) == true && ($("#field_20" + par).val().toString()).trim().length == 12)
    ) {
      let isExist = false;
      for (let i = 0; i < this.employees.length; i++) {
        if ($("#field_20" + par).val().toString().trim().toLowerCase() == this.employees[i].emP_IDENTITY.toLowerCase() && this.employees[i].id != this.employee.id) {
          isExist = true;
          break;
        }
      }
      if (isExist) {
        $("#field_20" + par).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_20" + par).html("Số chứng minh nhân dân đã tồn tại!");
        return false;
      }
      else {
        $("#field_20" + par).removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_20" + par).html("");
        return true;
      }
    }
    else {
      $("#field_20" + par).removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_20" + par).html("Vui lòng nhập đúng số chứng minh nhân dân!");
      return false;
    }
  }
  validate_use_email(par: string) {
    var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if ($("#field_7" + par).val() == '') {
      $("#field_7" + par).removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_7").html("Vui lòng nhập địa chỉ email!");
      return true;
    }
    else if ($("#field_7" + par).val() != '' && regex.test($("#field_7" + par).val().toString()) == false) {
      $("#field_7" + par).removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_7" + par).html("Vui lòng nhập đúng địa chỉ email!");
      return false;
    }
    else {
      let isExist = false;
      if (par == '1' || par == "1") {
        for (let i = 0; i < this.students.length; i++) {
          if ($("#field_7" + par).val().toString().trim().toLowerCase() == this.students[i].stU_EMAIL.toLowerCase() && this.students[i].id != this.student.id) {
            isExist = true;
            break;
          }
        }
      }
      else {
        for (let i = 0; i < this.employees.length; i++) {
          if ($("#field_7" + par).val().toString().trim() == this.employees[i].emP_EMAIL && this.employees[i].id != this.employee.id) {
            isExist = true;
            break;
          }
        }
      }
      if (isExist) {
        $("#field_7" + par).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_7" + par).html("Email đã tồn tại!");
        return false;
      }
      $("#field_7" + par).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_7" + par).html("");
      return true;
    }
  }
  validate_use_per(par: string) {
    if (this.list_per == null || this.list_per == '') {
      $("#field_9" + par + ">.ng-select-container").removeAttr("style").attr("style", "border-color:#dc3545;");
      $("#valid_mes_9" + par).html("Vui lòng chọn nhóm quyền!");
      return false;
    }
    else {
      $("#field_9" + par + ">.ng-select-container").removeAttr("style").attr("style", "border-color:#28a745;");
      $("#valid_mes_9" + par).html("");
      return true;
    }
  }
  validate_emp_pos(par: string) {
    if (this.list_pos == null || this.list_pos == '') {
      $("#field_000" + par + ">.ng-select-container").removeAttr("style").attr("style", "border-color:#dc3545;");
      $("#valid_mes_000" + par).html("Vui lòng chọn chức vụ!");
      return false;
    }
    else {
      $("#field_000" + par + ">.ng-select-container").removeAttr("style").attr("style", "border-color:#28a745;");
      $("#valid_mes_000" + par).html("");
      return true;
    }
  }
  // validate_use_per(par: string) {
  //   if (this.list_per[0] == null) {
  //     $("#field_9" + par).removeAttr("style").addClass('is-invalid');
  //     $("#valid_mes_9" + par).html("Vui lòng chọn nhóm quyền!");
  //     return false;
  //   }
  //   else {
  //     $("#field_9" + par).removeAttr("style").addClass('is-valid');
  //     $("#valid_mes_9" + par).html("");
  //     return true;
  //   }
  // }
  // validate_emp_pos(par: string) {
  //   if (this.list_pos[0] == null) {
  //     $("#field_000" + par + ">.ng-select-container").removeAttr("style").attr("style", "border-color:#dc3545;");
  //     $("#valid_mes_000" + par).html("Vui lòng chọn chức vụ!");
  //     return false;
  //   }
  //   else {
  //     $("#field_000" + par + ">.ng-select-container").removeAttr("style").attr("style", "border-color:#28a745;");
  //     $("#valid_mes_000" + par).html("");
  //     return true;
  //   }
  // }
  validate_use_DOB(par: string) {
    if (this.date == null) {
      $("#field_99" + par).addClass('is-valid').removeClass("is-invalid");
      $("#valid_mes_99" + par).html("");
      return true;
    }
    else {
      $("#field_99" + par).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_99" + par).html("");
      return true;
    }
  }
  validate_use_address(par: string) {
    $("#field_98" + par).addClass('is-valid');
  }
  validate_emp_status(par: string) {
    $("#field_77" + par).addClass('is-valid');
  }
  validate(par: string) {
    var check = true;
    if (this.validate_use_username(par) == false)
      check = false;
    if (this.validate_use_password(par) == false)
      check = false;
    if (this.validate_use_cfpassword(par) == false)
      check = false;
    if (this.validate_use_firstname(par) == false)
      check = false;
    if (this.validate_use_lastname(par) == false)
      check = false;
    if (this.validate_use_phone(par) == false)
      check = false;
    if (this.validate_use_email(par) == false)
      check = false;
    if (this.validate_use_per(par) == false)
      check = false;
    if (this.validate_use_DOB(par) == false)
      check = false;

    this.validate_use_address(par);
    this.validate_emp_status(par);
    //validate all
    if (check == false)
      return false;
    else
      return true;
  }
  clearField(par: string) {
    for (let i = 1; i <= 7; i++) {
      var field = "#field_" + i + par;
      var validmes = "#valid_mes_" + i + par;
      $(field).removeClass('is-invalid is-valid')
      $(validmes).html("");
    }
    $("#field_20" + par).removeClass('is-invalid is-valid');
    $("#valid_mes_20" + par).html("");
    $("#field_99" + par).removeClass('is-valid');
    $("#field_98" + par).removeClass('is-valid');

    this.list_per = [];
    $("#field_9" + par + ">.ng-select-container").css({
      "border-color": "#ced4da;"
    });
    // $("#field_9" + par).removeClass('is-valid');
    $("#valid_mes_9" + par).html("");
    this.date = null;
    this.list_per = [];
    this.list_pos = [];
  }
  checkField(par: string) {
    for (let i = 1; i <= 7; i++) {
      var field = "#field_" + i + par;
      var validmes = "#valid_mes_" + i + par;
      $(field).removeClass('is-invalid').addClass('is-valid');
      $(validmes).html("");
    }
    $("#field_20" + par).removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_20" + par).html("");
    $("#field_9" + par + ">.ng-select-container").css({
      "border-color": "#28a745;"
    });
    // $("#field_9" + par).addClass('is-valid');
    $("#valid_mes_9" + par).html("");
  }
  parseDate(dateString: string): Date {
    if (dateString) {
      return new Date(dateString);
    }
    return null;
  }
  // showDetailsemp(event = null, id: Number) {
  //   if (event != null) {
  //     event.preventDefault();
  //   }
  //   this.userSer.get(id).subscribe(res => {
  //     this.user = res.data;
  //     if (res.data.user_Create != null) {
  //       $("#emp_create").html(res.data.user_Create.employees[0].emP_LASTNAME + " " + res.data.user_Create.employees[0].emP_FIRSTNAME)
  //       $("#create_box").show();
  //     }
  //     if (res.data.user_Modified != null) {
  //       $("#emp_modified").html(res.data.user_Modified.employees[0].emP_LASTNAME + " " + res.data.user_Modified.employees[0].emP_FIRSTNAME)
  //       $("#modified_box").show();
  //     }
  //     this.employeeSer.getByUserId(res.data.id).subscribe(resList => {
  //       this.employee = resList.data;
  //       this.userpermissionSer.getlistperbyuserid(res.data.id).subscribe(resList1 => {
  //         this.user_permissions = resList1.data;
  //         this.employeepositionSer.getlistPosbyEmpid(resList.data.id).subscribe(res2 => {
  //           this.employee_positions = res2.data;
  //         });
  //       });
  //     });
  //     this.modalViewemp.show();
  //   });
  // }
  // showModalemp(event = null, id: Number = 0) {
  //   if (event != null) {
  //     event.preventDefault();
  //   }
  //   if (id > 0) {
  //     this.checkField('');
  //     this.file = null;
  //     this.userSer.get(id).subscribe(res => {
  //       this.user = res.data;
  //       $("#field_3").val(this.user.usE_PASSWORD);
  //       this.userpermissionSer.get(res.data.id).subscribe(resList => {
  //         this.list_per = resList.data;
  //         this.employeeSer.getByUserId(res.data.id).subscribe(resList1 => {
  //           this.employee = resList1.data;
  //           if (resList1.data.emP_DATEOFBIRTH != null) {
  //             // this.date = resList1.data.emP_DATEOFBIRTH.toString().substring(0, 10).split('-').reverse().join('/');
  //             // let d = new Date(this.employee.emP_DATEOFBIRTH.toString().substring(0, 10));
  //             // this.date = (d.getDate() + 1) + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
  //             let d = new Date(this.employee.emP_DATEOFBIRTH.toString().substring(0, 10));
  //             d.setDate(d.getUTCDate() + 1);
  //             this.date = d.getUTCDate() + '-' + (d.getUTCMonth() + 1) + '-' + d.getUTCFullYear();
  //           }
  //           this.url = resList1.data.emP_AVATAR;
  //           this.old_filename = resList1.data.fileName;
  //           $("#label_field_7").html(
  //             resList1.data.fileName.length > 20 ?
  //               resList1.data.fileName.substring(0, 5) + "..." + resList1.data.fileName.substring(resList1.data.fileName.length - 9, resList1.data.fileName.length) :
  //               resList1.data.fileName
  //           );
  //           this.employeepositionSer.get(resList1.data.id).subscribe(res2 => {
  //             this.list_pos = res2.data;
  //           });
  //         });
  //       });
  //       $("#modal_title").html("Sửa nhân viên");
  //       this.modalemp.show();
  //     });
  //   }
  //   else {
  //     this.clearField('');
  //     this.user = {
  //       id: 0,
  //       usE_STATUS: 1,
  //     } as User;
  //     this.employee = {
  //       emP_GENDER: true,
  //       emP_STATUS: 1,
  //     } as Employee;
  //     this.url = "";
  //     this.employee.emP_AVATAR = "";
  //     this.file = null;
  //     $("#field_3").val('');
  //     $("#modal_title").html("Thêm nhân viên");
  //     this.modalemp.show();
  //   }
  // }
  // saveemp() {
  //   if (this.validate('') == false) {
  //     return false;
  //   }
  //   if (this.validate_emp_pos('') == false) {
  //     return false;
  //   }
  //   if (this.validate_use_identity('') == false) {
  //     return false;
  //   }
  //   if (this.file != null) {
  //     var crDate = new Date();
  //     this.employee.emP_AVATAR = this.user.usE_USERNAME + "_" + this.file.name;
  //   }
  //   else {
  //     this.employee.emP_AVATAR = "df_avatar.png";
  //   }

  //   // if ($("#field_8").is(":checked")) {
  //   //   this.user.usE_STATUS = 1;
  //   // }
  //   // else {
  //   //   this.user.usE_STATUS = 0;
  //   // }
  //   if (this.employee.emP_STATUS == 0) {
  //     this.user.usE_STATUS = 0;
  //   }
  //   else {
  //     this.user.usE_STATUS = 1;
  //   }
  //   if (this.user.id === 0) {
  //     this.user.usE_ISDELETED = false;
  //     this.user.usE_CREATEDBY = parseInt(this.cookieSer.get('Id'));

  //     this.employee.emP_CREATEBY = parseInt(this.cookieSer.get('Id'));
  //     if (this.date != null) {
  //       let date = new Date(this.date);
  //       this.employee.emP_DATEOFBIRTH = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
  //       // this.employee.emP_DATEOFBIRTH = new Date(this.date);
  //       // this.employee.emP_DATEOFBIRTH.setMinutes(this.employee.emP_DATEOFBIRTH.getMinutes() + this.employee.emP_DATEOFBIRTH.getTimezoneOffset());
  //       // this.employee.emP_DATEOFBIRTH.setDate(this.employee.emP_DATEOFBIRTH.getDate() + 1);
  //     }
  //     this.emp_req = {
  //       user: this.user,
  //       employee: this.employee,
  //       user_permissions: this.list_per,
  //       employee_position: this.list_pos
  //     } as EmployeeRequest
  //     // add
  //     this.userSer.addemp(this.emp_req).subscribe(res => {
  //       if (res.errorCode === 0) {
  //         if (res.data.employees[0].emP_AVATAR != 'df_avatar.png') {
  //           const formData = new FormData();
  //           formData.append('file', this.file, res.data.employees[0].emP_AVATAR);
  //           this.fileSer.add("User", formData).subscribe();
  //         }
  //         this.url = "";
  //         this.employee.emP_AVATAR = "";
  //         this.file = null;
  //         this.userSer.getAll().subscribe(resList => {
  //           this.users = resList.data;
  //           this.rerender();
  //           this.url = "";
  //           this.employee.emP_AVATAR = "";
  //           this.modalemp.hide();
  //           this.date = null;
  //           this.isClick = 0;
  //           this.pnotify.success({
  //             title: 'Thông báo',
  //             text: 'Thêm nhân viên thành công!'
  //           });
  //         });
  //       }
  //     });
  //   }
  //   else {
  //     this.user.usE_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
  //     this.employee.emP_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
  //     this.employee.emP_CREATEBY = parseInt(this.cookieSer.get('Id'));
  //     if (this.isClick == 0) {
  //       this.employee.emP_DATEOFBIRTH = this.employee.emP_DATEOFBIRTH;
  //     }
  //     else {
  //       if (this.date != null) {
  //         let date = new Date(this.date);
  //         this.employee.emP_DATEOFBIRTH = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
  //         // let date = new Date(this.date);
  //         // this.employee.emP_DATEOFBIRTH = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();    
  //         // this.employee.emP_DATEOFBIRTH = new Date(this.date);
  //         // this.employee.emP_DATEOFBIRTH.setMinutes(this.employee.emP_DATEOFBIRTH.getMinutes() + this.employee.emP_DATEOFBIRTH.getTimezoneOffset());
  //         // this.employee.emP_DATEOFBIRTH.setDate(this.employee.emP_DATEOFBIRTH.getDate()+1);
  //         // let d = new Date(this.employee.emP_DATEOFBIRTH.toString().substring(0, 10));
  //         // d.setDate(d.getUTCDate()+1);
  //         // this.date = d.getUTCDate() + '-' + (d.getUTCMonth()+1) + '-' + d.getUTCFullYear();          
  //       }
  //     }
  //     this.emp_req = {
  //       user: this.user,
  //       employee: this.employee,
  //       user_permissions: this.list_per,
  //       employee_position: this.list_pos
  //     } as EmployeeRequest
  //     //update
  //     this.userSer.putemp(this.emp_req).subscribe(res => {
  //       if (res.errorCode === 0) {
  //         if (res.data.employees[0].emP_AVATAR != 'df_avatar.png' && this.file != null) {
  //           const formData = new FormData();
  //           formData.append('file', this.file, res.data.employees[0].emP_AVATAR);
  //           // this.fileSer.put(this.old_filename, "User", formData).subscribe(res2 => {
  //           //   if (this.emp_req.user.id == parseInt(this.cookieSer.get('Id'))) {
  //           //     $("#ava_icon").attr("src", res.data.imageurl);
  //           //   }
  //           // });
  //         }
  //         this.url = "";
  //         this.employee.emP_AVATAR = "";
  //         this.file = null;
  //         this.userSer.getAll().subscribe(resList => {
  //           this.users = resList.data;
  //           this.rerender();
  //           this.url = "";
  //           this.employee.emP_AVATAR = "";
  //           this.modalemp.hide();
  //           this.date = null;
  //           this.isClick = 0;
  //           this.pnotify.success({
  //             title: 'Thông báo',
  //             text: 'Cập nhật nhân viên thành công!'
  //           });
  //         });
  //       }
  //     });
  //   }
  // }

  // showModalstu(event = null, id: Number = 0) {
  //   if (event != null) {
  //     event.preventDefault();
  //   }
  //   if (id > 0) {
  //     this.checkField('1');
  //     this.userSer.get(id).subscribe(res => {
  //       this.user = res.data;
  //       $("#field_31").val(this.user.usE_PASSWORD);
  //       this.userpermissionSer.get(res.data.id).subscribe(resList => {
  //         this.list_per = resList.data;
  //         this.studentSer.getByUserId(res.data.id).subscribe(resList1 => {
  //           this.student = resList1.data;
  //           if (this.student.stU_DATEOFBIRTH != null) {
  //             // this.date = this.student.stU_DATEOFBIRTH.toString().substring(0, 10).split('-').reverse().join('/');
  //             // let d = new Date(this.student.stU_DATEOFBIRTH.toString().substring(0, 10));
  //             // this.date = (d.getDate() + 1) + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
  //             let d = new Date(this.student.stU_DATEOFBIRTH.toString().substring(0, 10));
  //             d.setDate(d.getUTCDate() + 1);
  //             this.date = d.getUTCDate() + '-' + (d.getUTCMonth() + 1) + '-' + d.getUTCFullYear();
  //           }
  //           this.url = resList1.data.stU_IMAGE;
  //           this.old_filename = resList1.data.fileName;
  //           $("#label_field_7").html(
  //             resList1.data.fileName.length > 20 ?
  //               resList1.data.fileName.substring(0, 5) + "..." + resList1.data.fileName.substring(resList1.data.fileName.length - 9, resList1.data.fileName.length) :
  //               resList1.data.fileName
  //           );
  //         });
  //       });
  //       $("#modal_title_stu").html("Sửa học viên");
  //       this.modalstu.show();
  //     });
  //   }
  //   else {
  //     this.clearField('1');
  //     this.user = {
  //       id: 0,
  //       usE_STATUS: 1,
  //     } as User;
  //     this.student = {
  //       stU_GENDER: true,
  //       stU_DATEOFBIRTH: null
  //     } as Student;
  //     this.url = "";
  //     this.student.stU_IMAGE = "";
  //     this.file = null;
  //     $("#field_31").val('');
  //     $("#modal_title_stu").html("Thêm học viên");
  //     this.modalstu.show();
  //   }
  // }
  // savestu() {
  //   if (this.validate('1') == false) {
  //     return false;
  //   }
  //   if (this.file != null) {
  //     var crDate = new Date();
  //     this.student.stU_IMAGE = this.user.usE_USERNAME + "_" + this.file.name;
  //   }
  //   else {
  //     this.student.stU_IMAGE = "df_avatar.png";
  //   }

  //   if ($("#field_81").is(":checked")) {
  //     this.user.usE_STATUS = 1;
  //   }
  //   else {
  //     this.user.usE_STATUS = 0;
  //   }
  //   // if (this.date != null) {
  //   //   this.student.stU_DATEOFBIRTH = new Date(this.date);
  //   // }

  //   if (this.user.id === 0) {
  //     this.user.usE_ISDELETED = false;
  //     this.user.usE_CREATEDBY = parseInt(this.cookieSer.get('Id'));

  //     this.student.stU_CREATEBY = parseInt(this.cookieSer.get('Id'));
  //     if (this.date != null) {
  //       let date = new Date(this.date);
  //       this.student.stU_DATEOFBIRTH = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
  //       // this.student.stU_DATEOFBIRTH = new Date(this.date);
  //       // this.student.stU_DATEOFBIRTH.setMinutes(this.student.stU_DATEOFBIRTH.getMinutes() + this.student.stU_DATEOFBIRTH.getTimezoneOffset());
  //       // this.student.stU_DATEOFBIRTH.setDate(this.student.stU_DATEOFBIRTH.getDate() + 1);
  //     }
  //     this.stu_req = {
  //       user: this.user,
  //       student: this.student,
  //       user_permissions: this.list_per
  //     } as StudentRequest
  //     // add
  //     this.userSer.addstu(this.stu_req).subscribe(res => {
  //       if (res.errorCode === 0) {
  //         if (res.data.students[0].stU_IMAGE != 'df_avatar.png') {
  //           const formData = new FormData();
  //           formData.append('file', this.file, res.data.students[0].stU_IMAGE);
  //           this.fileSer.add("User", formData).subscribe();
  //         }
  //         this.url = "";
  //         this.student.stU_IMAGE = "";
  //         this.file = null;
  //         this.userSer.getAll().subscribe(resList => {
  //           this.users = resList.data;
  //           this.rerender();
  //           this.url = "";
  //           this.student.stU_IMAGE = "";
  //           this.isClick2 = 0;
  //           this.date = null;
  //           this.modalstu.hide();
  //           this.pnotify.success({
  //             title: 'Thông báo',
  //             text: 'Thêm học viên thành công!'
  //           });
  //         });
  //       }
  //     });
  //   }
  //   else {
  //     if (this.isClick2 == 0) {
  //       this.student.stU_DATEOFBIRTH = this.student.stU_DATEOFBIRTH;
  //     }
  //     else {
  //       if (this.date != null) {
  //         let date = new Date(this.date);
  //         this.student.stU_DATEOFBIRTH = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
  //         // this.student.stU_DATEOFBIRTH = new Date(this.date);
  //         // this.student.stU_DATEOFBIRTH.setMinutes(this.student.stU_DATEOFBIRTH.getMinutes() + this.student.stU_DATEOFBIRTH.getTimezoneOffset());
  //         // this.student.stU_DATEOFBIRTH.setDate(this.student.stU_DATEOFBIRTH.getDate()+1);
  //       }
  //     }
  //     this.user.usE_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
  //     this.student.stU_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
  //     this.student.stU_CREATEBY = parseInt(this.cookieSer.get('Id'));
  //     this.stu_req = {
  //       user: this.user,
  //       student: this.student,
  //       user_permissions: this.list_per
  //     } as StudentRequest
  //     //update
  //     this.userSer.putstu(this.stu_req).subscribe(res => {
  //       if (res.errorCode === 0) {
  //         if (res.data.students[0].stU_IMAGE != 'df_avatar.png' && this.file != null) {
  //           const formData = new FormData();
  //           formData.append('file', this.file, res.data.students[0].stU_IMAGE);
  //           // this.fileSer.put(this.old_filename, "User", formData).subscribe();
  //         }
  //         this.url = "";
  //         this.student.stU_IMAGE = "";
  //         this.file = null;
  //         this.userSer.getAll().subscribe(resList => {
  //           this.users = resList.data;
  //           this.rerender();
  //           this.url = "";
  //           this.student.stU_IMAGE = "";
  //           this.isClick2 = 0;
  //           this.date = null;
  //           this.modalstu.hide();
  //           this.pnotify.success({
  //             title: 'Thông báo',
  //             text: 'Cập nhật học viên thành công!'
  //           });
  //         });
  //       }
  //     });
  //   }
  // }
  // showDetailsstu(event = null, id: Number) {
  //   if (event != null) {
  //     event.preventDefault();
  //   }
  //   this.userSer.get(id).subscribe(res => {
  //     this.user = res.data;
  //     if (res.data.user_Create != null) {
  //       $("#emp_create1").html(res.data.user_Create.employees[0].emP_LASTNAME + " " + res.data.user_Create.employees[0].emP_FIRSTNAME)
  //       $("#create_box1").show();
  //     }
  //     if (res.data.user_Modified != null) {
  //       $("#emp_modified1").html(res.data.user_Modified.employees[0].emP_LASTNAME + " " + res.data.user_Modified.employees[0].emP_FIRSTNAME)
  //       $("#modified_box1").show();
  //     }
  //     this.studentSer.getByUserId(res.data.id).subscribe(resList => {
  //       this.student = resList.data;
  //       this.userpermissionSer.getlistperbyuserid(res.data.id).subscribe(resList1 => {
  //         this.user_permissions = resList1.data;
  //       });
  //     });
  //     this.modalViewstu.show();
  //   });
  // }
}
