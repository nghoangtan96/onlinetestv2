import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { SettingtypeService, SettingType } from '../../services/settingtype.service';
import { PnotifyService } from '../../services/pnotify.service';
import { SettingService, Setting } from '../../services/setting.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { settingRequest } from '../../models/setting_request';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { Branch, BranchService } from '../../services/branch.service';
import { SignalrService } from '../../services/signalr.service';
import { RoleService } from '../../services/role.service';
import { UserPermissionService } from '../../services/user-permission.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent implements OnInit {

  settingListSortByType: [SettingType];
  settingList: [Setting];
  branches: [Branch];
  pnotify = undefined;
  isChange: boolean = false;
  settingReqList: settingRequest[] = [];

  set_upd: boolean = true;

  @ViewChild('modalConfirm', { static: false }) modalConfirm: ModalDirective;
  constructor(private settingTypeSer: SettingtypeService, private pNotify: PnotifyService,
    private settingSer: SettingService, private cookieSer: CookieService,
    private branchSer: BranchService, private signalRSer: SignalrService,
    private roleSer: RoleService, private userPermissionSer: UserPermissionService) {
    this.pnotify = this.pNotify.getPNotify();
  }
  ngOnInit() {
    this.settingTypeSer.getAll().subscribe(res => {
      this.settingListSortByType = res.data;
    });
    this.settingSer.getAll().subscribe(res => {
      this.settingList = res.data;
    });
    this.branchSer.getAll().subscribe(res => {
      this.branches = res.data;
    });
    //permission
    this.roleSer.get(75).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'set_upd':
                this.set_upd = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    //realtime
    this.signalRSer.branch.subscribe((res1) => {
      if (res1 === true) {
        this.branchSer.getAll().subscribe(res => {
          this.branches = res.data;
        });
      }
    });
    this.signalRSer.setting.subscribe((res1) => {
      if (res1 === true) {
        this.settingTypeSer.getAll().subscribe(res => {
          this.settingListSortByType = res.data;
        });
        this.settingSer.getAll().subscribe(res => {
          this.settingList = res.data;
        });
      }
    });
  }
  UpdateField() {
    this.settingList.forEach(item => {
      if (item.seT_CHOICE != null || item.seT_MULTICHOICE! + null) {
        $("input[name=field_" + item.id + "]").prop("disabled", false);
      }
      else {
        $("#field_" + item.id).prop("disabled", false);
      }
    });
    this.isChange = true;
  }
  ClearField() {
    this.settingSer.getAll().subscribe(res => {
      this.settingList = res.data;
      this.settingList.forEach(item => {
        if (item.seT_VALUE != null) {
          $("#field_" + item.id).val(item.seT_VALUE);

          $("#field_" + item.id).prop("disabled", true);
          $("#field_" + item.id).removeClass('is-invalid is-valid');
          $("#valid_mes_" + item.id).html("");
        }
        else if (item.seT_ISACTIVE != null) {
          if (item.seT_ISACTIVE == true) {
            $("#field_" + item.id).prop("checked", true);
          }
          else {
            $("#field_" + item.id).prop("checked", false);
          }

          $("#field_" + item.id).prop("disabled", true);
          $("#field_" + item.id).removeClass('is-invalid is-valid');
          $("#valid_mes_" + item.id).html("");
        }
        else if (item.seT_CHOICE != null) {
          for (let i = 0; i < item.seT_CHOICE.split('/').length; i++) {
            if (item.seT_CHOICE.split('/')[i] == item.seT_CHOICEVALUE)
              $("#choice_" + item.id + i).prop("checked", true);
          }
          $("input[name=field_" + item.id + "]").prop("disabled", true);
          $("#valid_mes_" + item.id).html("");
          $("#box_choice_" + item.id).removeClass("text-danger");
        }
        else if (item.seT_MULTICHOICE != null) {
          for (let i = 0; i < item.seT_MULTICHOICE.split('/').length; i++) {
            if (item.seT_MULTICHOICEVALUE.split('/')[i] == '1')
              $("#choice_" + item.id + i).prop("checked", true);
            else
              $("#choice_" + item.id + i).prop("checked", false);
          }
          $("input[name=field_" + item.id + "]").prop("disabled", true);
          $("#valid_mes_" + item.id).html("");
        }
      });
      this.isChange = false;
      this.settingReqList = [];
    });
  }
  validate_field(id: number) {
    if (this.settingList.find(x => x.id == id).seT_VALUE != null) {
      if ($("#field_" + id).val() == '') {
        $("#field_" + id).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_" + id).html("Vui lòng nhập " + this.settingList.find(x => x.id == id).seT_NAME.toLowerCase() + "!");
        return false;
      }
      else if ($("#field_" + id).val() < 6 || $("#field_" + id).val() > 32) {
        $("#field_" + id).removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_" + id).html("Sai định dạng, " + this.settingList.find(x => x.id == id).seT_NAME.toLowerCase() + " phải từ 6-32 ký tự!");
        return false;
      }
      else {
        $("#field_" + id).removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_" + id).html("");
        return true;
      }
    }
    else if (id == 5) {
      let isSmaller = false;
      let newValue = 0;
      for (let i = 0; i < this.settingList.find(x => x.id == 5).seT_CHOICE.split('/').length; i++) {
        if ($("#choice_" + id + i).is(":checked") == true) {
          if ($("#choice_" + id + i).val() < parseInt(this.settingList.find(x => x.id == 5).seT_CHOICEVALUE)) {
            newValue = Number($("#choice_" + id + i).val());
            isSmaller = true;
            break;
          }
          else
            continue;
        }
      }
      if (isSmaller == true && this.branches.find(x => x.brA_LEVEL > newValue) != null) {
        $("#box_choice_" + id).addClass("text-danger");
        $("#valid_mes_" + id).html("Vẫn còn ngành học con ở bậc " + (newValue + 1) + ", vui lòng xóa các ngành học ở bậc " + (newValue + 1) + " trước khi thay đổi!");
        return false;
      }
      else {
        $("#box_choice_" + id).removeClass("text-danger");
        $("#valid_mes_" + id).html("");
        return true;
      }
    }
    else if (id == 10) {
      $("#valid_mes_" + id).html("<i class='fa fa-warning'></i> Tắt một loại câu hỏi sẽ không thể tạo mới hoặc sử dụng các câu hỏi đó!");
      $("#valid_mes_" + id).addClass('text-warning');
    }
  }
  SaveUpdate() {
    if (this.isChange == false) {
      return false;
    }
    else {
      $("#message").html("Bạn có chắc chắn muốn lưu thay đổi?");
      this.modalConfirm.show();
    }
  }
  SaveUpdateConfirm() {
    for (let i = 0; i < this.settingList.length; i++) {
      if (this.validate_field(this.settingList[i].id) == false) {
        this.modalConfirm.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: 'Có lỗi xảy ra, vui lòng kiểm tra lại!'
        });
        return false;
      }
      else
        continue;
    }

    let userId = Number(this.cookieSer.get('Id'));
    this.settingList.forEach(item => {
      if (item.seT_VALUE != null) {
        this.settingReqList.push({ userId: userId, id: item.id, value: Number($("#field_" + item.id).val()), isactive: null, choicevalue: null, multichoicevalue: null });
      }
      else if (item.seT_ISACTIVE != null) {
        this.settingReqList.push({ userId: userId, id: item.id, value: null, isactive: $("#field_" + item.id).is(":checked") ? true : false, choicevalue: null, multichoicevalue: null });
      }
      else if (item.seT_CHOICE != null) {
        let value = "";
        for (let i = 0; i < item.seT_CHOICE.split('/').length; i++) {
          if ($("#choice_" + item.id + i).is(":checked") == true)
            value = item.seT_CHOICE.split('/')[i];
        }
        this.settingReqList.push({ userId: userId, id: item.id, value: null, isactive: null, choicevalue: value, multichoicevalue: null });
      }
      else if (item.seT_MULTICHOICE != null) {
        let value = "";
        for (let i = 0; i < item.seT_MULTICHOICE.split('/').length; i++) {
          if (i == 0) {
            if ($("#choice_" + item.id + i).is(":checked") == true)
              value += "1";
            else
              value += "0";
          }
          else {
            if ($("#choice_" + item.id + i).is(":checked") == true)
              value += "/1";
            else
              value += "/0";
          }
        }
        this.settingReqList.push({ userId: userId, id: item.id, value: null, isactive: null, choicevalue: null, multichoicevalue: value });
      }
    });
    this.settingSer.updateSetting(this.settingReqList).subscribe(res => {
      this.settingListSortByType = res.data;
      this.modalConfirm.hide();
      this.ClearField();
      this.pnotify.success({
        title: 'Thông báo',
        text: 'Chỉnh sửa thiết lập hệ thống thành công!'
      });
    });
  }
  ToggleGroup(id: number) {
    if ($("#contentToggle_" + id).is(":visible")) {
      $("#buttonicon_" + id).removeClass().addClass("fa fa-plus");
      $("#contentToggle_" + id).fadeOut();
    }
    else {
      $("#buttonicon_" + id).removeClass().addClass("fa fa-minus");
      $("#contentToggle_" + id).fadeIn();
    }
  }
}
