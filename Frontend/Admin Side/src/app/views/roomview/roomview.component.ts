import { Component, OnInit, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { ScheduleTest, ScheduleTestService } from '../../services/schedule-test.service';
import { TestshiftService, TestShift } from '../../services/testshift.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PopoverDirective } from 'ngx-bootstrap/popover';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { SignalrService } from '../../services/signalr.service';
import { StudentTestService } from '../../services/student-test.service';

@Component({
  selector: 'app-roomview',
  templateUrl: './roomview.component.html',
  styleUrls: ['./roomview.component.css']
})
export class RoomviewComponent implements OnInit {

  scheduleTests: [ScheduleTest];
  searchText: string;
  testShifts: [TestShift];

  isListItemNull: boolean = true;
  today: Date = new Date();
  listStudentHTML: any;
  keyId: number;
  pnotify = undefined;
  userId: number = Number(this.cookieSer.get('Id'));
  isHaveItem: boolean = true;

  @ViewChildren(PopoverDirective) popovers: QueryList<PopoverDirective>;
  @ViewChild('modalConfirmBox', { static: false }) modalConfirmBox: ModalDirective;
  constructor(private scheduleTestSer: ScheduleTestService, private testShiftSer: TestshiftService,
    private pNotify: PnotifyService, private cookieSer: CookieService,
    private router: Router, private signalRSer: SignalrService,
    private studentTestSer: StudentTestService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    let date = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
    this.scheduleTestSer.getListScheduleTestByDate(date).subscribe(res => {
      this.scheduleTests = res.data;
      for (let i = 0; i < res.data.length; i++) {
        var date = new Date("2020-06-22 " + res.data[i].testShifts.shI_TIMESTART);
        date.setTime(date.getTime() + res.data[i].tests.teS_TIME * 60000);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        res.data[i].testShifts.timeEnd = ((hours > 12) ? (hours - 12 + ':' + minutes + ' PM') : (hours + ':' + minutes + ' AM'));

        var dateUnlock = new Date("2020-06-22 " + res.data[i].testShifts.shI_TIMESTART);
        dateUnlock.setTime(dateUnlock.getTime() - 15 * 60000);
        var datecurrent = new Date();
        // console.log(datecurrent);
        // console.log(dateUnlock);
        // console.log("      ");
        var dateStart = new Date("2020-06-22 " + res.data[i].testShifts.shI_TIMESTART);
        if (dateUnlock.getHours() > datecurrent.getHours() || (datecurrent.getHours() == dateUnlock.getHours() && dateUnlock.getMinutes() > datecurrent.getMinutes())) {
          this.scheduleTests[i].labs.labScheduleStatus = 1;
        }
        else if (datecurrent.getHours() < dateStart.getHours() || (datecurrent.getHours() == dateStart.getHours() && datecurrent.getMinutes() < dateStart.getMinutes())) {
          this.scheduleTests[i].labs.labScheduleStatus = 2;
        }
        else if (datecurrent.getHours() > date.getHours() || (datecurrent.getHours() == date.getHours() && datecurrent.getMinutes() > date.getMinutes())) {
          this.scheduleTests[i].labs.labScheduleStatus = 4;
        }
        else {
          this.scheduleTests[i].labs.labScheduleStatus = 3;
        }
      }
      if (res.data[0] == null) {
        this.isListItemNull = true;
      }
      else {
        this.isListItemNull = false;
      }
    });

    this.testShiftSer.getAll().subscribe(res => {
      this.testShifts = res.data;
    });
    //navigate interupt test
    this.studentTestSer.getAllByUserId(Number(this.cookieSer.get('Id'))).subscribe(res => {
      for (let i = 0; i < res.data.length; i++) {
        if (res.data.find(x => x.sttE_STATUS == 2) != null) {
          this.router.navigate(['/exam'], { queryParams: { scheduleTestId: res.data.find(x => x.sttE_STATUS == 2).sttE_SCTEID } })
        }
      }
    });

    //real time
    this.signalRSer.roomview.subscribe((res1) => {
      if (res1 === true) {
        this.scheduleTestSer.getListScheduleTestByDate(date).subscribe(res => {
          this.scheduleTests = res.data;
          let check = false;
          let testShift = "";
          let count = 0;
          for (let i = 0; i < res.data.length; i++) {
            var date = new Date("2020-06-22 " + res.data[i].testShifts.shI_TIMESTART);
            date.setTime(date.getTime() + res.data[i].tests.teS_TIME * 60000);
            var hours = date.getHours();
            var minutes = date.getMinutes();
            res.data[i].testShifts.timeEnd = ((hours > 12) ? (hours - 12 + ':' + minutes + ' PM') : (hours + ':' + minutes + ' AM'));

            var dateUnlock = new Date("2020-06-22 " + res.data[i].testShifts.shI_TIMESTART);
            dateUnlock.setTime(dateUnlock.getTime() - 15 * 60000);
            var datecurrent = new Date();
            // console.log(datecurrent);
            // console.log(dateUnlock);
            // console.log("      ");
            var dateStart = new Date("2020-06-22 " + res.data[i].testShifts.shI_TIMESTART);
            if (dateUnlock.getHours() > datecurrent.getHours() || (datecurrent.getHours() == dateUnlock.getHours() && dateUnlock.getMinutes() > datecurrent.getMinutes())) {
              this.scheduleTests[i].labs.labScheduleStatus = 1;
            }
            else if (datecurrent.getHours() < dateStart.getHours() || (datecurrent.getHours() == dateStart.getHours() && datecurrent.getMinutes() < dateStart.getMinutes())) {
              this.scheduleTests[i].labs.labScheduleStatus = 2;
              if (count == 0) {
                check = true;
                testShift += this.scheduleTests[i].testShifts.shI_NAME + " (" + date.getHours() + ":" + date.getMinutes() + " " + ((date.getHours() > 12) ? ('PM') : ('AM')) + ")";
                count++;
              }
            }
            else if (datecurrent.getHours() > date.getHours() || (datecurrent.getHours() == date.getHours() && datecurrent.getMinutes() > date.getMinutes())) {
              this.scheduleTests[i].labs.labScheduleStatus = 4;
            }
            else {
              this.scheduleTests[i].labs.labScheduleStatus = 3;
            }
          }

          if (check == true)
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Các phòng thi thuộc ' + testShift + ' đã mở!'
            });

          if (res.data[0] == null) {
            this.isListItemNull = true;
          }
          else {
            this.isListItemNull = false;
          }
        });
      }
    });
    this.signalRSer.testshift.subscribe((res1) => {
      if (res1 === true) {
        this.testShiftSer.getAll().subscribe(res => {
          this.testShifts = res.data;
        });
      }
    });
  }

  searchRoom() {
    if (this.searchText != "" && this.scheduleTests.find(x => x.labs.laB_NAME.includes(this.searchText)) == null) {
      this.isHaveItem = false;
    }
    else {
      this.isHaveItem = true;
    }
  }

  ToggleGroup(id: number) {
    if ($("#contentToggle_roomview_" + id).is(":visible")) {
      $("#buttonicon_roomview_" + id).removeClass().addClass("fa fa-plus");
      $("#contentToggle_roomview_" + id).slideToggle();
    }
    else {
      $("#buttonicon_roomview_" + id).removeClass().addClass("fa fa-minus");
      $("#contentToggle_roomview_" + id).slideToggle();
    }
  }

  loadListStudent(id: number) {
    console.log(this.scheduleTests);
    var listStudent = this.scheduleTests.find(x => x.id == id).student_Tests;
    console.log(this.scheduleTests.find(x => x.id == id));
    this.listStudentHTML = '<div class="col-12"><table class="table table-striped">' +
      '<thead><tr><th><strong>STT</strong></th><th><strong>MSSV</strong></th><th><strong>Họ</strong></th>' +
      '<th><strong>Tên</strong></th><th><strong>Lớp</strong></th></tr></thead><tbody>';
    for (let i = 0; i < listStudent.length; i++) {
      this.listStudentHTML += '<tr>' +
        '<td>' + (i + 1) + '</td>' + '<td>' + listStudent[i].students.stU_CODE + '</td>' +
        '<td>' + listStudent[i].students.stU_LASTNAME + '</td>' +
        '<td>' + listStudent[i].students.stU_FIRSTNAME + '</td>' +
        '<td>' + listStudent[i].students.classes.clA_CODE + '</td>' +
        '</tr>';
    }
    this.listStudentHTML += '</tbody></table></div>';

    this.popovers.forEach((popover: PopoverDirective) => {
      popover.onShown.subscribe(() => {
        this.popovers
          .filter(p => p !== popover)
          .forEach(p => p.hide());
      });
    });
  }

  joinRoom(id: number) {
    this.unlockModal("confirm");
    $("#confirmText").html('<i class="fa fa-warning"></i> Bạn chắc chắn muốn vào phòng thi này!');
    this.keyId = id;
    this.modalConfirmBox.show();
  }

  comfirmJoinRoom() {
    this.lockModal("confirm");
    let scheduleTestItem = this.scheduleTests.find(x => x.id == this.keyId);
    if (scheduleTestItem.labs.labScheduleStatus == 1 || scheduleTestItem.labs.labScheduleStatus == 4) {
      this.modalConfirmBox.hide();
      this.pnotify.error({
        title: 'Thông báo',
        text: 'Phòng thi chưa mở, không thể vào!'
      });
    }
    else if (scheduleTestItem.student_Tests.find(x => x.students.stU_USEID == this.userId) == null) {
      this.modalConfirmBox.hide();
      this.pnotify.error({
        title: 'Thông báo',
        text: 'Bạn không có quyền vào phòng này!'
      });
    }
    else
      if (scheduleTestItem.student_Tests.find(x => x.students.stU_USEID == this.userId).sttE_STATUS == 3) {
        this.modalConfirmBox.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: 'Bạn đã hoàn thành bài thi này!'
        });
      }
      else {
        this.router.navigate(['/exam'], { queryParams: { scheduleTestId: scheduleTestItem.id } });
      }
  }

  lockModal(key: string) {
    $(".close_modal_roomview_" + key).prop("disabled", true);
    $("#save_modal_roomview_" + key).prop("disabled", true);
    $("#save_modal_roomview_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_roomview_" + key).prop("disabled", false);
    $("#save_modal_roomview_" + key).prop("disabled", false);
    $("#save_modal_roomview_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

}
