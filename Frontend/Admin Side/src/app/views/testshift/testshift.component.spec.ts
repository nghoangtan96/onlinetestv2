import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestshiftComponent } from './testshift.component';

describe('TestshiftComponent', () => {
  let component: TestshiftComponent;
  let fixture: ComponentFixture<TestshiftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestshiftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestshiftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
