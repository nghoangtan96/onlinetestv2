import { Component, OnInit, ViewChild } from '@angular/core';
import { TestShift, TestshiftService } from '../../services/testshift.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { SettingService } from '../../services/setting.service';
import { ActivatedRoute } from '@angular/router';
import { SignalrService } from '../../services/signalr.service';
import { RoleService } from '../../services/role.service';
import { UserPermissionService } from '../../services/user-permission.service';

@Component({
  selector: 'app-testshift',
  templateUrl: './testshift.component.html',
  styleUrls: ['./testshift.component.css']
})
export class TestshiftComponent implements OnInit {

  testshiftid: number;
  testshift_Id: number;

  testshifts: [TestShift];
  testshift: TestShift = {} as TestShift;
  testshiftsMultiDel: [TestShift];

  pnotify = undefined;
  isListItemNull: boolean = true;
  tabControl: number = 1;

  tesf_add: boolean = true;
  tesf_upd: boolean = true;
  tesf_del: boolean = true;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild('modalDeletMul', { static: false }) modalDeletMul: ModalDirective;

  constructor(private pNotify: PnotifyService, private cookieSer: CookieService,
    private settingSer: SettingService, private route: ActivatedRoute,
    private signalRSer: SignalrService, private testshiftSer: TestshiftService,
    private roleSer: RoleService, private userPermissionSer: UserPermissionService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.dtOptions = {
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      order: [[1, 'asc']]
    };
    this.testshiftSer.getAll().subscribe(res => {
      this.testshifts = res.data;
      // console.log(res.data);
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
    //permission
    this.roleSer.get(67).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'tesf_add':
                this.tesf_add = false;
                break;
              case 'tesf_upd':
                this.tesf_upd = false;
                break;
              case 'tesf_del':
                this.tesf_del = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    //realtime
    this.signalRSer.testshift.subscribe((res) => {
      if (res === true) {
        this.testshiftSer.getAll().subscribe(res1 => {
          this.testshifts = res1.data;
          this.rerender();
        });
      }
    });
  }

  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.testshiftid = params['id'];
      if (this.testshiftid != null) {
        // console.log(this.braid);
        this.openModalDetail(this.testshiftid);
      }
    });
  }

  Select_Clear_All() {
    $(".toggle-shi-all").closest('tr').toggleClass('selected');
    if ($(".toggle-shi-all").closest('tr').hasClass('selected')) {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.select();
        });
      });
    }
    else {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.deselect();
        });
      });
    }
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
  }

  clearField() {
    for (let i = 1; i <= 3; i++) {
      $("#field_shi_" + i).removeClass('is-invalid is-valid');
      $("#valid_mes_shi_" + i).html("");
    }
  }

  checkField() {
    for (let i = 1; i <= 3; i++) {
      $("#field_shi_" + i).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_shi_" + i).html("");
    }
  }

  validate_shi_name() {
    if ($("#field_shi_1").val() == '') {
      $("#field_shi_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_shi_1").html("Vui lòng nhập tên ca thi!");
      return false;
    }
    else {
      let facultyName = $("#field_shi_1").val().toString().trim().toLowerCase();
      let isExist = false;
      for (let i = 0; i < this.testshifts.length; i++) {
        if (facultyName == this.testshifts[i].shI_NAME.toLowerCase() && this.testshift.id != this.testshifts[i].id && this.testshifts[i].shI_STATUS == 1) {
          isExist = true;
          break;
        }
      }

      if (isExist == true) {
        $("#field_shi_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_shi_1").html("Tên ca thi đã tồn tại!");
        return false;
      }
      else {
        $("#field_shi_1").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_shi_1").html("");
        return true;
      }
    }
  }

  validate_shi_timestart() {
    var date = new Date("2020-06-22 " + this.testshift.shI_TIMESTART);
    var hours = date.getHours();
    var minutes = date.getMinutes();

    if ($("#field_shi_2").val() == '') {
      $("#field_shi_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_shi_2").html("Vui lòng nhập thời gian bắt đầu!");
      return false;
    }
    else if (hours < 6 || hours > 18) {
      $("#field_shi_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_shi_2").html("Vui lòng nhập thời gian trong giờ hành chính (6:00 AM - 6:00 PM)!");
      return false;
    }
    else {
      let isTimeCorrect = true;
      let facultyName = $("#field_shi_2").val().toString();
      let isExist = false;
      for (let i = 0; i < this.testshifts.length; i++) {
        if (facultyName == this.testshifts[i].shI_TIMESTART.toString() && this.testshift.id != this.testshifts[i].id && this.testshifts[i].shI_STATUS == 1) {
          isExist = true;
          break;
        }
        if (this.testshift.id != this.testshifts[i].id && this.testshifts[i].shI_STATUS == 1) {
          let dateString = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
          let dateCurrent = new Date(dateString + " " + this.testshift.shI_TIMESTART);
          let dateInArr = new Date(dateString + " " + this.testshifts[i].shI_TIMESTART);
          // console.log(dateInArr);
          let spaceTime = (dateCurrent.getTime() > dateInArr.getTime() ? (dateCurrent.getTime() - dateInArr.getTime()) : (dateInArr.getTime() - dateCurrent.getTime()));
          if ((spaceTime / 60000) / 60 < 2.5) {
            isTimeCorrect = false;
            break;
          }
        }
      }

      if (isExist == true) {
        $("#field_shi_2").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_shi_2").html("Thời gian bắt đầu đã tồn tại!");
        return false;
      }
      if (isTimeCorrect == false) {
        $("#field_shi_2").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_shi_2").html("Ca thi phải cách nhau ít nhất 2:30 phút!");
        return false;
      }
      $("#field_shi_2").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_shi_2").html("");
      return true;
    }
  }

  validate_shi_status() {
    $("#field_shi_3").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_shi_3").html("");
    return true;
  }

  validate() {
    var check = true;
    if (this.validate_shi_name() == false)
      check = false;
    if (this.validate_shi_timestart() == false)
      check = false;

    this.validate_shi_status();
    //validate all
    if (check == false) {
      $("#valid_shi_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_shi_" + id).is(":visible")) {
      $("#buttonicon_shi_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_shi_" + id).slideToggle();
    }
    else {
      $("#buttonicon_shi_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_shi_" + id).slideToggle();
    }
  }

  ToggleGroupMulti(id: number) {
    if ($("#detailToggle_shi_mul_" + id).is(":visible")) {
      $("#buttonicon_shi_mul_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_shi_mul_" + id).slideToggle();
    }
    else {
      $("#buttonicon_shi_mul_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_shi_mul_" + id).slideToggle();
    }
  }

  openModal(id: number = 0) {
    this.unlockModal("addupd");
    if (id > 0) { // update
      this.checkField();

      this.testshiftSer.get(id).subscribe(res => {
        this.testshift = res.data;
      });

      $("#modal_title_shi").html("Sửa ca thi");
      this.modal.show();
    }
    else { // insert
      this.testshift = {
        id: 0,
        shI_NAME: 'Ca ' + (this.testshifts.length + 1),
        shI_STATUS: 1
      } as TestShift

      this.clearField();
      $("#modal_title_shi").html("Thêm ca thi");
      this.modal.show();
    }
  }

  save() {

    if (this.validate() == false) {
      return false;
    }

    this.lockModal("addupd");
    if (this.testshift.id === 0) { // insert
      this.testshift.shI_CREATEDBY = parseInt(this.cookieSer.get('Id'));

      this.testshiftSer.add(this.testshift).subscribe(res => {
        if (res.errorCode === 0) {
          this.testshiftSer.getAll().subscribe(res1 => {
            this.testshifts = res1.data;
            this.rerender();
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Thêm ca thi thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
    else { // update
      this.testshift.shI_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.testshiftSer.put(this.testshift).subscribe(res => {
        if (res.errorCode === 0) {
          this.testshiftSer.getAll().subscribe(res1 => {
            this.testshifts = res1.data;
            this.rerender();
            this.modal.hide();
            if (this.modalDetail.isShown) {
              this.openModalDetail(res.data.id);
            }
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Sửa ca thi thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
  }
  openModalDelete(id: number) {
    this.unlockModal("del");
    this.testshiftSer.get(id).subscribe(res => {
      this.testshift = res.data;
      this.modalDelete.show();
    });
  }
  delete() {
    this.lockModal("del");
    if (this.testshift.id != 0) {
      this.testshiftSer.delete(this.testshift.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          this.testshiftSer.getAll().subscribe(res1 => {
            this.testshifts = res1.data;
            this.rerender();
          });
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Xóa ca thi thành công!'
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
        this.modalDetail.hide();
      });
    }
  }
  openModalDeleteMul() {
    this.unlockModal("muldel");
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      let arr = [];
      dtInstance.rows({ selected: true }).every(function () {
        if (this.data()[1] != null) {
          arr.push(this.data()[1]);
        }
      });
      if (arr.length == 0) {
        this.isListItemNull = true;
        this.modalDeletMul.show();
      }
      else {
        this.isListItemNull = false;
        this.testshiftSer.getListTestShiftMultiDel(arr).subscribe(res => {
          this.testshiftsMultiDel = res.data;
          // console.log(this.subjectsMultiDel);
          this.modalDeletMul.show();
        });
      }
    });
  }
  deleteMul() {
    this.lockModal("muldel");
    this.testshiftSer.deleteMulti(this.testshiftsMultiDel, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
      if (res.errorCode === 0) {
        this.testshiftSer.getAll().subscribe(res1 => {
          this.testshifts = res1.data;
          this.rerender();
          if ($(".toggle-shi-all").closest('tr').hasClass('selected')) {
            $(".toggle-shi-all").closest('tr').toggleClass('selected');
          }
          this.modalDeletMul.hide();
          this.pnotify.success({
            title: 'Thông báo',
            text: "Xóa danh sách ca thi thành công!"
          });
        });
      }
      else {
        if ($(".toggle-shi-all").closest('tr').hasClass('selected')) {
          $(".toggle-shi-all").closest('tr').toggleClass('selected');
        }
        this.modalDeletMul.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message
        });
      }
    });
  }

  openModalDetail(id: number) {
    this.testshiftSer.get(id).subscribe(res => {
      this.testshift = res.data;
      this.testshift_Id = res.data.id;
      this.modalDetail.show();
    });
  }

  resetDetailModal() {
    this.testshift_Id = null;
    //reset tab
    for (let i = 1; i <= 2; i++) {
      if (i == 1) {
        $("#tab_shi_" + i).addClass("active");
        $("#content_shi_" + i).addClass("active");
      }
      else {
        $("#tab_shi_" + i).removeClass("active");
        $("#content_shi_" + i).removeClass("active").addClass("fade");
      }
    }
    this.loadTab(1);
  }

  lockModal(key: string) {
    $(".close_modal_shi_" + key).prop("disabled", true);
    $("#save_modal_shi_" + key).prop("disabled", true);
    $("#save_modal_shi_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_shi_" + key).prop("disabled", false);
    $("#save_modal_shi_" + key).prop("disabled", false);
    $("#save_modal_shi_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  loadTab(key: number) {
    this.tabControl = key;
  }
}
