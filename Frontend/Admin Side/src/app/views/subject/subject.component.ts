import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { SubjectService, Subjectt } from '../../services/subject.service';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { Branch, BranchService } from '../../services/branch.service';
import { SettingService, Setting } from '../../services/setting.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { SubjectBranchService } from '../../services/subject-branch.service';
import { ActivatedRoute } from '@angular/router';
import { FacultyService, Faculty } from '../../services/faculty.service';
import { UserPermissionService } from '../../services/user-permission.service';
import { RoleService } from '../../services/role.service';
import { SignalrService } from '../../services/signalr.service';
import { UserService } from '../../services/user.service';
import { EmployeeSubjectService, Employee_Subject } from '../../services/employee-subject.service';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css']
})
export class SubjectComponent implements OnInit {
  @Input() public branchId: number; //parse parameter through selector
  @Input() public facultyId: number; //parse parameter through selector
  subid: number;

  pnotify = undefined;
  subjects: [Subjectt];
  subjectsMultiDel: [Subjectt];
  subjectsConstraint: [number];
  subject: Subjectt = {} as Subjectt;
  branch: Branch = {} as Branch;
  branches: [Branch];
  branchParents: [Branch];
  faculties: [Faculty];
  faculty: Faculty = {} as Faculty;
  facultyConstraint: Faculty;
  autoGenCode: Setting = {} as Setting;
  isHasChild: Setting = {} as Setting;
  extendSize: Setting = {} as Setting;
  size: number;
  list_branch: any = [];
  subject_Id: number;
  tabControl: number = 1;

  isListItemNull: boolean = true;
  sub_add: boolean = true;
  sub_upd: boolean = true;
  sub_del: boolean = true;
  sub_con: boolean = true;
  sub_ref: boolean = true;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild('modalDeletMul', { static: false }) modalDeletMul: ModalDirective;

  constructor(private subjectSer: SubjectService, private pNotify: PnotifyService,
    private cookieSer: CookieService, private branchSer: BranchService,
    private settingSer: SettingService, private SubjectBranchSer: SubjectBranchService,
    private route: ActivatedRoute, private facultySer: FacultyService,
    private userPermissionSer: UserPermissionService, private roleSer: RoleService,
    private signalRSer: SignalrService, private userSer: UserService,
    private employeeSubjectSer: EmployeeSubjectService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.dtOptions = {
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      order: [[1, 'asc']]
    };
    if (this.branchId != null) {
      this.SubjectBranchSer.getListByBranchId(this.branchId).subscribe(res => {
        this.subjects = res.data;
        // console.log(res.data);
        this.dtTrigger.next();
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.columns().every(function () {
            const that = this;
            $('input', this.footer()).on('keyup change', function () {
              if (that.search() !== this['value']) {
                that
                  .search(this['value'])
                  .draw();
              }
            });
          });
        });
      });
      this.branchSer.get(this.branchId).subscribe(res => {
        this.branch = res.data;
      });
    }
    else if (this.facultyId != null) {
      this.subjectSer.getListSubjectByFacultyId(this.facultyId).subscribe(res => {
        this.subjects = res.data;
        this.dtTrigger.next();
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.columns().every(function () {
            const that = this;
            $('input', this.footer()).on('keyup change', function () {
              if (that.search() !== this['value']) {
                that
                  .search(this['value'])
                  .draw();
              }
            });
          });
        });
      });
      this.facultySer.get(this.facultyId).subscribe(res => {
        this.faculty = res.data;
      });
    }
    else {
      this.subjectSer.getAll().subscribe(res => {
        this.subjects = res.data;
        // console.log(res.data);
        this.dtTrigger.next();
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.columns().every(function () {
            const that = this;
            $('input', this.footer()).on('keyup change', function () {
              if (that.search() !== this['value']) {
                that
                  .search(this['value'])
                  .draw();
              }
            });
          });
        });
      });
    }
    this.branchSer.getAll().subscribe(res => {
      this.branches = res.data;
    });
    this.branchSer.getAllParentBranch().subscribe(res => {
      this.branchParents = res.data;
    });
    this.facultySer.getAll().subscribe(res => {
      this.faculties = res.data;
    });
    this.settingSer.get(3).subscribe(res => {
      this.autoGenCode = res.data;
    });
    this.settingSer.get(4).subscribe(res => {
      this.isHasChild = res.data;
    });
    this.settingSer.get(5).subscribe(res => {
      this.extendSize = res.data;
      this.size = Number(res.data.seT_CHOICEVALUE);
    });
    //permission
    this.roleSer.get(6).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);s
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'sub_add':
                this.sub_add = false;
                break;
              case 'sub_upd':
                this.sub_upd = false;
                break;
              case 'sub_del':
                this.sub_del = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    this.roleSer.get(11).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        let count = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
          }
        }
        if (!(count > 0)) {
          this.sub_ref = false;
        }
      });
    });
    this.roleSer.get(105).subscribe(res => {
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        let count = 0;
        let countNoConstraint = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == false) != null) {
              countNoConstraint++;
            }
          }
        }
        if (count > 0 && countNoConstraint == 0) {
          this.sub_con = false;
          this.userSer.get(Number(this.cookieSer.get('Id'))).subscribe(res2 => {
            this.employeeSubjectSer.getListSubjectIdByEmployeeId(res2.data.employees[0].id).subscribe(res3 => {
              this.subjectsConstraint = res3.data;
            });
            this.facultySer.get(res2.data.employees[0].emP_FACID).subscribe(res3 => {
              this.facultyConstraint = res3.data;
            });
          });
        }
        else if (countNoConstraint > 0) {
          this.sub_con = true;
        }
      })
    });
    //realtime
    this.signalRSer.subject.subscribe((res) => {
      if (res === true) {
        if (this.branchId != null) {
          this.SubjectBranchSer.getListByBranchId(this.branchId).subscribe(res1 => {
            this.subjects = res1.data;
            this.rerender();
          });
        }
        else if (this.facultyId != null) {
          this.subjectSer.getListSubjectByFacultyId(this.facultyId).subscribe(res1 => {
            this.subjects = res1.data;
            this.rerender();
          });
        }
        else {
          this.subjectSer.getAll().subscribe(res1 => {
            this.subjects = res1.data;
            this.rerender();
          });
        }
      }
    });
    this.signalRSer.branch.subscribe((res) => {
      if (res === true) {
        this.branchSer.getAll().subscribe(res1 => {
          this.branches = res1.data;
        });
        this.branchSer.getAllParentBranch().subscribe(res1 => {
          this.branchParents = res1.data;
        });
      }
    });
    this.signalRSer.faculty.subscribe((res) => {
      if (res === true) {
        this.facultySer.getAll().subscribe(res1 => {
          this.faculties = res1.data;
        });
      }
    });
    this.signalRSer.setting.subscribe((res) => {
      if (res === true) {
        this.settingSer.get(3).subscribe(res1 => {
          this.autoGenCode = res1.data;
        });
        this.settingSer.get(4).subscribe(res1 => {
          this.isHasChild = res1.data;
        });
        this.settingSer.get(5).subscribe(res1 => {
          this.extendSize = res1.data;
          this.size = Number(res1.data.seT_CHOICEVALUE);
        });
      }
    });
  }

  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.subid = params['id'];
      if (this.subid != null) {
        // console.log(this.braid);
        this.openModalDetail(this.subid);
      }
    });
  }

  Select_Clear_All() {
    $(".toggle-all").closest('tr').toggleClass('selected');
    if ($(".toggle-all").closest('tr').hasClass('selected')) {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.select();
        });
      });
    }
    else {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.deselect();
        });
      });
    }
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_sub_" + id).is(":visible")) {
      $("#buttonicon_sub_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_sub_" + id).slideToggle();
    }
    else {
      $("#buttonicon_sub_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_sub_" + id).slideToggle();
    }
  }

  ToggleGroupMulti(id: number) {
    if ($("#detailToggle_sub_mul_" + id).is(":visible")) {
      $("#buttonicon_sub_mul_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_sub_mul_" + id).slideToggle();
    }
    else {
      $("#buttonicon_sub_mul_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_sub_mul_" + id).slideToggle();
    }
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
  }

  clearField() {
    for (let i = 1; i <= 6; i++) {
      $("#field_" + i).removeClass('is-invalid is-valid');
      $("#valid_mes_" + i).html("");
    }
    this.list_branch = [];
    $("#field_4>.ng-select-container").removeAttr("style");

    $("#choice_1").prop("checked", true);
    $("#branch_choice").hide();
  }

  checkField() {
    for (let i = 1; i <= 6; i++) {
      $("#field_" + i).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_" + i).html("");
    }
  }

  generateCode() {
    this.facultySer.get(this.subject.suB_FACID).subscribe(res => {
      this.subject.suB_CODE = this.GenCode();
    });
  }

  GenCode() {
    let code = "XXXXX";
    console.log(this.subject.suB_FACID);
    if (this.subject.suB_FACID != 0 && this.subject.suB_FACID != null) {
      code = this.faculties.find(x => x.id == this.subject.suB_FACID).faC_CODE + code.substr(2, 3);

      let count = 0;
      for (let i = 0; i < this.subjects.length; i++) {
        if (this.subjects[i].suB_FACID == this.subject.suB_FACID) {
          count++;
        }
      }

      do {
        code = code.substr(0, 2) + (count < 9 ? "00" + (count + 1) : (count < 99 ? "0" + (count + 1) : (count + 1)))
        count++;
      } while (this.subjects.find(x => x.suB_CODE == code) != null);
    }

    return code;
  }

  validate_sub_name() {
    if ($("#field_1").val() == '') {
      $("#field_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_1").html("Vui lòng nhập tên môn học!");
      return false;
    }
    else {
      let subjectName = $("#field_1").val().toString().trim().toLowerCase();
      let isExist = false;
      for (let i = 0; i < this.subjects.length; i++) {
        if (subjectName == this.subjects[i].suB_NAME.toLowerCase() && this.subject.id != this.subjects[i].id && this.subjects[i].suB_STATUS == 1) {
          isExist = true;
          break;
        }
      }

      if (isExist == true) {
        $("#field_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_1").html("Tên môn học đã tồn tại!");
        return false;
      }
      else {
        $("#field_1").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_1").html("");
        return true;
      }
    }
  }

  validate_sub_code() {
    if ($("#field_2").val() == '') {
      $("#field_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_2").html("Vui lòng nhập mã môn học!");
      return false;
    }
    else {
      let subjectCode = $("#field_2").val().toString().trim();
      let isExist = false;
      for (let i = 0; i < this.subjects.length; i++) {
        if (subjectCode == this.subjects[i].suB_CODE && this.subject.id != this.subjects[i].id) {
          isExist = true;
          break;
        }
      }

      if (isExist == true) {
        $("#field_2").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_2").html("Mã môn học đã tồn tại!");
        return false;
      }
      else {
        $("#field_2").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_2").html("");
        return true;
      }
    }
  }

  validate_sub_credit() {
    if ($("#field_3").val() == '') {
      $("#field_3").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_3").html("Vui lòng nhập số tín chỉ!");
      return false;
    }
    else if (parseInt($("#field_3").val().toString()) <= 0) {
      $("#field_3").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_3").html("Số tín chỉ phải lớn hơn 0!");
      return false;
    }
    else {
      $("#field_3").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_3").html("");
      return true;
    }
  }

  validate_sub_branch() {
    if ($("#choice_2").is(":checked") == true) {
      if (this.list_branch[0] == null || this.list_branch[0] == 0) {
        $("#field_4>.ng-select-container").removeAttr("style").attr("style", "border-color:#dc3545;");
        $("#valid_mes_4").html("Vui lòng chọn ngành học!");
        return false;
      }
      else {
        $("#field_4>.ng-select-container").removeAttr("style").attr("style", "border-color:#28a745;");
        $("#valid_mes_4").html("");
        return true;
      }
    }
  }

  validate_sub_faculty() {
    if ($("#field_6").val() == '' || $("#field_6").val() == 0 || $("#field_6").val() == null) {
      $("#field_6").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_6").html("Vui lòng chọn khoa!"); ``
      return false;
    }
    else {
      $("#field_6").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_6").html("");
      return true;
    }
  }

  validate() {
    var check = true;
    if (this.validate_sub_name() == false)
      check = false;
    if (this.validate_sub_code() == false)
      check = false;
    if (this.validate_sub_credit() == false)
      check = false;
    if (this.validate_sub_branch() == false)
      check = false;
    if (this.validate_sub_faculty() == false)
      check = false;

    //validate all
    if (check == false) {
      $("#valid_sub_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  toggleDropdown() {
    if ($("#choice_1").is(":checked") == true) {
      $("#branch_choice").hide();
    }
    else {
      $("#branch_choice").show();
    }
  }

  openModal(id: number = 0) {
    this.unlockModal("addupd");
    if (id > 0) { // update
      this.checkField();

      this.subjectSer.get(id).subscribe(res => {
        this.subject = res.data;
        // console.log(res.data);
        if (res.data.subject_Branches[0] != null) {
          this.SubjectBranchSer.getListBySubjectId(res.data.id).subscribe(res1 => {
            this.list_branch = res1.data;
            $("#field_4>.ng-select-container").attr("style", "border-color:#28a745;");
            $("#choice_2").prop("checked", true);
            $("#branch_choice").show();
          });
        }
        else {
          $("#choice_1").prop("checked", true);
          $("#branch_choice").hide();
        }
      });

      $("#modal_title").html("Sửa môn học");
      this.modal.show();
    }
    else { // insert
      this.clearField();

      if (this.autoGenCode.seT_ISACTIVE == true) {
        this.subject = {
          id: 0,
          suB_CODE: "XXXXX",
          suB_FACID: 0
        } as Subjectt
      }
      else {
        this.subject = {
          id: 0,
          suB_FACID: 0
        } as Subjectt
      }

      if (this.facultyId != null) {
        this.subject.suB_FACID = this.facultyId;
        this.subject.suB_CODE = this.GenCode();
      }
      $("#modal_title").html("Thêm môn học");
      this.modal.show();
    }
  }

  save() {

    if (this.validate() == false) {
      return false;
    }
    this.lockModal("addupd");

    if ($("#choice_1").is(":checked") == false) {
      this.subject.listBranch = this.list_branch;
      this.subject.suB_ISALL = false;
    }
    else {
      this.subject.listBranch = null;
      this.subject.suB_ISALL = true;
    }

    // console.log(this.subject);

    if (this.subject.id === 0) { // insert
      this.subject.suB_CREATEDBY = parseInt(this.cookieSer.get('Id'));
      this.subjectSer.add(this.subject).subscribe(res => {
        if (res.errorCode === 0) {
          if (this.branchId != null) {
            this.SubjectBranchSer.getListByBranchId(this.branchId).subscribe(res1 => {
              this.subjects = res1.data;
              this.rerender();
            });
          }
          else if (this.facultyId != null) {
            this.subjectSer.getListSubjectByFacultyId(this.facultyId).subscribe(res1 => {
              this.subjects = res1.data;
              this.rerender();
            });
          }
          else {
            this.subjectSer.getAll().subscribe(res1 => {
              this.subjects = res1.data;
              this.rerender();
            });
          }
          this.list_branch = [];
          this.modal.hide();
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Thêm môn học thành công!'
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
    else { // update
      this.subject.suB_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.subjectSer.put(this.subject).subscribe(res => {
        if (res.errorCode === 0) {
          if (this.branchId != null) {
            this.SubjectBranchSer.getListByBranchId(this.branchId).subscribe(res1 => {
              this.subjects = res1.data;
              this.rerender();
            });
          }
          else if (this.facultyId != null) {
            this.subjectSer.getListSubjectByFacultyId(this.facultyId).subscribe(res1 => {
              this.subjects = res1.data;
              this.rerender();
            });
          }
          else {
            this.subjectSer.getAll().subscribe(res1 => {
              this.subjects = res1.data;
              this.rerender();
            });
          }
          this.list_branch = [];
          this.modal.hide();
          if (this.modalDetail.isShown) {
            this.openModalDetail(res.data.id);
          }
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Sửa môn học thành công!'
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
  }

  openModalDelete(id: number) {
    this.unlockModal("del");
    this.subjectSer.get(id).subscribe(res => {
      this.subject = res.data;
      this.modalDelete.show();
    });
  }
  delete() {
    this.lockModal("del");
    if (this.subject.id != 0) {
      this.subjectSer.delete(this.subject.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          if (this.branchId != null) {
            this.SubjectBranchSer.getListByBranchId(this.branchId).subscribe(res1 => {
              this.subjects = res1.data;
              this.rerender();
            });
          }
          else if (this.facultyId != null) {
            this.subjectSer.getListSubjectByFacultyId(this.facultyId).subscribe(res1 => {
              this.subjects = res1.data;
              this.rerender();
            });
          }
          else {
            this.subjectSer.getAll().subscribe(res1 => {
              this.subjects = res1.data;
              this.rerender();
            });
          }
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Xóa môn học thành công!'
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
        this.modalDetail.hide();
      });
    }
  }
  openModalDeleteMul() {
    this.unlockModal("muldel");
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      let arr = [];
      dtInstance.rows({ selected: true }).every(function () {
        if (this.data()[1] != null) {
          arr.push(this.data()[1]);
        }
      });
      if (arr.length == 0) {
        this.isListItemNull = true;
        this.modalDeletMul.show();
      }
      else {
        this.isListItemNull = false;
        this.subjectSer.getListSubjectMultiDel(arr).subscribe(res => {
          this.subjectsMultiDel = res.data;
          // console.log(this.subjectsMultiDel);
          this.modalDeletMul.show();
        });
      }
    });
  }
  deleteMul() {
    this.lockModal("muldel");
    this.subjectSer.deleteMulti(this.subjectsMultiDel, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
      if (res.errorCode === 0) {
        if (this.branchId != null) {
          this.SubjectBranchSer.getListByBranchId(this.branchId).subscribe(res1 => {
            this.subjects = res1.data;
            this.rerender();
          });
        }
        else if (this.facultyId != null) {
          this.subjectSer.getListSubjectByFacultyId(this.facultyId).subscribe(res1 => {
            this.subjects = res1.data;
            this.rerender();
          });
        }
        else {
          this.subjectSer.getAll().subscribe(res1 => {
            this.subjects = res1.data;
            this.rerender();
          });
        }
        if ($(".toggle-all").closest('tr').hasClass('selected')) {
          $(".toggle-all").closest('tr').toggleClass('selected');
        }
        this.modalDeletMul.hide();
        this.pnotify.success({
          title: 'Thông báo',
          text: "Xóa danh sách môn học thành công!"
        });
      }
      else {
        if ($(".toggle-all").closest('tr').hasClass('selected')) {
          $(".toggle-all").closest('tr').toggleClass('selected');
        }
        this.modalDeletMul.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message
        });
      }
    });
  }

  openModalDetail(id: number) {
    this.subjectSer.get(id).subscribe(res => {
      this.subject = res.data;
      // console.log(res.data);
      this.subject_Id = id;
      this.modalDetail.show();
    });
  }

  resetDetailModal() {
    this.subject_Id = null;
    //reset tab
    for (let i = 1; i <= 2; i++) {
      if (i == 1) {
        $("#tab_sub_" + i).addClass("active");
        $("#content_sub_" + i).addClass("active");
      }
      else {
        $("#tab_sub_" + i).removeClass("active");
        $("#content_sub_" + i).removeClass("active").addClass("fade");
      }
    }
    this.loadTab(1);
  }

  lockModal(key: string) {
    $(".close_modal_sub_" + key).prop("disabled", true);
    $("#save_modal_sub_" + key).prop("disabled", true);
    $("#save_modal_sub_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_sub_" + key).prop("disabled", false);
    $("#save_modal_sub_" + key).prop("disabled", false);
    $("#save_modal_sub_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  loadTab(key: number) {
    this.tabControl = key;
  }
}
