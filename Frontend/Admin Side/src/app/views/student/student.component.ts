import { Component, OnInit, ViewChild, ViewChildren, QueryList, Input } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { StudentService, Student } from '../../services/student.service';
import { DomSanitizer } from '@angular/platform-browser';
import { FileService } from '../../services/file.service';
import { UserService, User } from '../../services/user.service';
import { StudentRequest } from '../../models/student_request';
import { UserPermissionService, User_Permission } from '../../services/user-permission.service';
import { Permission, PermissionService } from '../../services/permission.service';
import { Router } from '@angular/router';
import { Faculty, FacultyService } from '../../services/faculty.service';
import { Branch, BranchService } from '../../services/branch.service';
import { Class, ClassService } from '../../services/class.service';
import { SchoolYear, SchoolyearService } from '../../services/schoolyear.service';
import { Setting, SettingService } from '../../services/setting.service';
import { SignalrService } from '../../services/signalr.service';
import { ValidateService } from '../../services/validate.service';
import { Employee, EmployeeService } from '../../services/employee.service';
import { saveAs } from 'file-saver';
import { RoleService } from '../../services/role.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit {

  @Input() public classId: number; //parse parameter through selector
  @Input() public scheduleTestId: number; //parse parameter through selector
  @Input() public schoolyearId: number; //parse parameter through selector

  students: [Student];
  studentsMultiDel: [Student];
  studentsExport: [Student];
  student: Student = {} as Student;
  emplyees: [Employee];
  user: User = {} as User;
  users: [User];
  faculties: [Faculty];
  facultyConstraint: Faculty;
  branches: [Branch];
  branchParents: [Branch];
  classes: [Class];
  permissions: [Permission];
  schoolYear: SchoolYear;
  userPermission: User_Permission[] = [];
  pnotify = undefined;
  studentRequest: StudentRequest = {} as StudentRequest;

  file: File = null;
  fileImport: File = null;
  url: any = "";
  fileIsExist: boolean;
  fileImportIsExist: boolean;
  isListItemNull: boolean = true;
  autoGenCode: Setting = {} as Setting;
  lengthUsername: Setting = {} as Setting;
  lengthPassword: Setting = {} as Setting;
  isHasChild: Setting = {} as Setting;
  extendSize: Setting = {} as Setting;
  exportType: number = 1;
  list_permission: any = [];
  fac_id: number = 0;
  size: number;
  sampleFileLink: string = "";
  errorImport: boolean = false;
  errorImportLog: string = "";

  stu_add: boolean = true;
  stu_upd: boolean = true;
  stu_del: boolean = true;
  stu_con: boolean = true;
  stu_ref: boolean = true;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  dtTrigger1: Subject<any> = new Subject();
  dtOptions1: any = {};
  @ViewChildren(DataTableDirective)
  dtElements: QueryList<DataTableDirective>;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild('modalDeletMul', { static: false }) modalDeletMul: ModalDirective;
  @ViewChild('modalExport', { static: false }) modalExport: ModalDirective;
  @ViewChild('modalImport', { static: false }) modalImport: ModalDirective;

  constructor(private pNotify: PnotifyService, private cookieSer: CookieService,
    private stuSer: StudentService, private _DomSanitizer: DomSanitizer, //unsafe bug fix
    private fileSer: FileService, private userSer: UserService, private userPermissionSer: UserPermissionService,
    private perSer: PermissionService, private studentSer: StudentService,
    private employeeSer: EmployeeService,
    private schoolYearSer: SchoolyearService, private classSer: ClassService,
    private validateSer: ValidateService, private branchSer: BranchService,
    private signalRService: SignalrService, private facultySer: FacultyService,
    private permissionSer: PermissionService, private settingSer: SettingService,
    private userPerSer: UserPermissionService, private router: Router,
    private roleSer: RoleService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.dtOptions = {
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      order: [[1, 'asc']]
    };
    if (this.classId != null) {
      this.studentSer.getListByClassId(this.classId).subscribe(res => {
        this.students = res.data;
        // console.log(res.data);
        this.dtTrigger.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
        //excel
        this.studentsExport = res.data;
        this.dtTrigger1.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
      });
    }
    else if (this.scheduleTestId != null) {
      this.studentSer.getListByScheduleTestId(this.scheduleTestId).subscribe(res => {
        this.students = res.data;
        // console.log(res.data);
        this.dtTrigger.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
        //excel
        this.studentsExport = res.data;
        this.dtTrigger1.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
      });
    }
    else if (this.schoolyearId != null) {
      this.studentSer.getListBySchoolYearId(this.schoolyearId).subscribe(res => {
        this.students = res.data;
        // console.log(res.data);
        this.dtTrigger.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
        //excel
        this.studentsExport = res.data;
        this.dtTrigger1.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
      });
    }
    else {
      this.studentSer.getAll().subscribe(res => {
        this.students = res.data;
        // console.log(res.data);
        this.dtTrigger.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
        //excel
        this.studentsExport = res.data;
        this.dtTrigger1.next();
        this.dtElements.forEach((dtElement: DataTableDirective) => {
          if (dtElement.dtInstance != null) {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
                dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                      that
                        .search(this['value'])
                        .draw();
                    }
                  });
                });
              }
            });
          }
        });
      });
    }
    this.facultySer.getAll().subscribe(res => {
      this.faculties = res.data;
    });
    this.permissionSer.getAll().subscribe(res => {
      this.permissions = res.data;
    });
    this.schoolYearSer.getSchoolYearByCurrentYear(new Date().getFullYear()).subscribe(res => {
      this.schoolYear = res.data;
    });
    this.branchSer.getAll().subscribe(res => {
      this.branches = res.data;
    });
    this.employeeSer.getAll().subscribe(res => {
      this.emplyees = res.data;
    });
    this.settingSer.get(3).subscribe(res => {
      this.autoGenCode = res.data;
    });
    this.settingSer.get(1).subscribe(res => {
      this.lengthUsername = res.data;
    });
    this.settingSer.get(2).subscribe(res => {
      this.lengthPassword = res.data;
    });
    this.settingSer.get(4).subscribe(res => {
      this.isHasChild = res.data;
    });
    this.settingSer.get(5).subscribe(res => {
      this.extendSize = res.data;
      this.size = Number(res.data.seT_CHOICEVALUE);
    });
    //permission
    this.roleSer.get(92).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);s
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'stu_add':
                this.stu_add = false;
                break;
              case 'stu_upd':
                this.stu_upd = false;
                break;
              case 'stu_del':
                this.stu_del = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    this.roleSer.get(105).subscribe(res => {
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        let count = 0;
        let countNoConstraint = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == false) != null) {
              countNoConstraint++;
            }
          }
        }
        if (count > 0 && countNoConstraint == 0) {
          this.stu_con = false;
          this.userSer.get(Number(this.cookieSer.get('Id'))).subscribe(res2 => {
            this.facultySer.get(res2.data.employees[0].emP_FACID).subscribe(res3 => {
              this.facultyConstraint = res3.data;
            });
          });
        }
        else if (countNoConstraint > 0) {
          this.stu_con = true;
        }
      })
    });
    // realtime
    this.signalRService.student.subscribe((res) => {
      if (res === true) {
        if (this.classId != null) {
          this.studentSer.getListByClassId(this.classId).subscribe(res1 => {
            this.students = res1.data;
            // console.log(res.data);
            this.rerender();
            this.studentsExport = res.data;
            this.rerender1();
          });
        }
        else if (this.scheduleTestId != null) {
          this.studentSer.getListByScheduleTestId(this.scheduleTestId).subscribe(res1 => {
            this.students = res1.data;
            // console.log(res.data);
            this.rerender();
            this.studentsExport = res.data;
            this.rerender1();
          });
        }
        else if (this.schoolyearId != null) {
          this.studentSer.getListBySchoolYearId(this.schoolyearId).subscribe(res1 => {
            this.students = res1.data;
            // console.log(res.data);
            this.rerender();
            this.studentsExport = res.data;
            this.rerender1();
          });
        }
        else {
          this.studentSer.getAll().subscribe(res => {
            this.students = res.data;
            // console.log(res.data);
            this.rerender();
            this.studentsExport = res.data;
            this.rerender1();
          });
        }
      }
    });
    this.signalRService.faculty.subscribe((res1) => {
      if (res1 === true) {
        this.facultySer.getAll().subscribe(res => {
          this.faculties = res.data;
        });
      }
    });
    this.signalRService.permission.subscribe((res1) => {
      if (res1 === true) {
        this.permissionSer.getAll().subscribe(res => {
          this.permissions = res.data;
        });
      }
    });
    this.signalRService.schoolyear.subscribe((res1) => {
      if (res1 === true) {
        this.schoolYearSer.getSchoolYearByCurrentYear(new Date().getFullYear()).subscribe(res => {
          this.schoolYear = res.data;
        });
      }
    });
    this.signalRService.branch.subscribe((res1) => {
      if (res1 === true) {
        this.branchSer.getAll().subscribe(res => {
          this.branches = res.data;
        });
      }
    });
    this.signalRService.employee.subscribe((res1) => {
      if (res1 === true) {
        this.employeeSer.getAll().subscribe(res => {
          this.emplyees = res.data;
        });
      }
    });
    this.signalRService.setting.subscribe((res1) => {
      if (res1 === true) {
        this.settingSer.get(3).subscribe(res => {
          this.autoGenCode = res.data;
        });
        this.settingSer.get(1).subscribe(res => {
          this.lengthUsername = res.data;
        });
        this.settingSer.get(2).subscribe(res => {
          this.lengthPassword = res.data;
        });
        this.settingSer.get(4).subscribe(res => {
          this.isHasChild = res.data;
        });
        this.settingSer.get(5).subscribe(res => {
          this.extendSize = res.data;
          this.size = Number(res.data.seT_CHOICEVALUE);
        });
      }
    });
  }
  onSelectFile(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      this.file = event.target.files[0];

      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      }

      $("#label_field_stu_15").html(
        this.file.name.length > 30 ?
          this.file.name.substring(0, 9) + "..." + this.file.name.substring(this.file.name.length - 9, this.file.name.length) :
          this.file.name
      );
      this.fileIsExist = true;
    }
  }

  onSelectFileImport(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      this.fileImport = event.target.files[0];

      // reader.onload = (event: any) => { // called once readAsDataURL is completed
      //   this.url = event.target.result;
      // }

      $("#label_field_stu_import").html(
        this.fileImport.name.length > 30 ?
          this.fileImport.name.substring(0, 9) + "..." + this.fileImport.name.substring(this.fileImport.name.length - 9, this.fileImport.name.length) :
          this.fileImport.name
      );
      this.fileImportIsExist = true;
    }
  }

  removeFile(event = null) {
    if (event != null) {
      event.preventDefault();
    }
    this.file = null;
    $("#field_stu_15").val("");
    this.fileIsExist = null;
    this.url = "";
    $("#label_field_stu_15").html("Tải ảnh lên...");
    this.validate_stu_avatar();
  }

  removeFileImport(event = null) {
    if (event != null) {
      event.preventDefault();
    }
    this.fileImport = null;
    $("#field_stu_import").val("");
    this.fileImportIsExist = null;
    $("#label_field_stu_import").html("Tải tệp lên...");
    this.validate_stu_importfile();
  }

  Select_Clear_All() {
    $(".toggle-stu-all").closest('tr').toggleClass('selected');
    if ($(".toggle-stu-all").closest('tr').hasClass('selected')) {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_main") {
              dtInstance.rows({ search: 'applied' }).every(function () {
                this.select();
              });
            }
          });
        }
      });
    }
    else {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_main") {
              dtInstance.rows({ search: 'applied' }).every(function () {
                this.deselect();
              });
            }
          });
        }
      });
    }
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    this.dtTrigger1.unsubscribe();
  }

  rerender(): void {
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      if (dtElement.dtInstance != null) {
        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == "tb_main") {
            dtInstance.destroy();
            this.dtTrigger.next();
            this.dtElements.forEach((dtElement: DataTableDirective) => {
              if (dtElement.dtInstance != null) {
                dtElement.dtInstance.then((dtInstance: any) => {
                  if (dtInstance.table().node().id == "tb_main" && dtInstance != null) {
                    dtInstance.columns().every(function () {
                      const that = this;
                      $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this['value']) {
                          that
                            .search(this['value'])
                            .draw();
                        }
                      });
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }

  rerender1(): void {
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      if (dtElement.dtInstance != null) {
        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == "tb_sub") {
            dtInstance.destroy();
            this.dtTrigger1.next();
            this.dtElements.forEach((dtElement: DataTableDirective) => {
              if (dtElement.dtInstance != null) {
                dtElement.dtInstance.then((dtInstance: any) => {
                  if (dtInstance.table().node().id == "tb_sub" && dtInstance != null) {
                    dtInstance.columns().every(function () {
                      const that = this;
                      $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this['value']) {
                          that
                            .search(this['value'])
                            .draw();
                        }
                      });
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }

  genCode() {
    let code = 'XXXXXXXXX';
    if (this.student.stU_SCYEID != null && this.student.stU_SCYEID != 0) {
      code = this.schoolYear.scyE_CODE + code.substr(2, 3) + code.substr(5, 4);
    }

    if (this.student.stU_BRAID != null && this.student.stU_BRAID != 0 && this.branchParents[0] != null) {
      code = code.substr(0, 2) + this.branches.find(x => x.id == this.student.stU_BRAID).brA_CODE + code.substr(5, 4);
    }

    if (this.student.stU_SCYEID != null && this.student.stU_SCYEID != 0 && this.student.stU_BRAID != null && this.student.stU_BRAID != 0) {
      let count = 0;
      for (let i = 0; i < this.students.length; i++) {
        if (this.students[i].stU_BRAID == this.student.stU_BRAID && this.students[i].stU_SCYEID == this.student.stU_SCYEID && this.students[i].id != this.student.id) {
          count++;
        }
      }
      //check duplicate
      do {
        code = code.substr(0, 2) + code.substr(2, 3) + (count < 9 ? ("000" + (count + 1)) : (count < 99 ? ("00" + (count + 1)) : (count < 999 ? ("0" + (count + 1)) : (count + 1))));
        count++;
      } while (this.students.find(x => x.stU_SCYEID == this.student.stU_SCYEID && x.stU_BRAID == this.student.stU_BRAID && x.id != this.student.id && x.stU_CODE == code) != null);
    }
    return code.toUpperCase();
  }

  resetCode() {
    this.student.stU_CODE = this.genCode();
  }

  generateBranches() {
    if (this.fac_id != null && this.fac_id != 0) {
      if (this.isHasChild.seT_ISACTIVE == true) {
        this.branchSer.getAllParentBranch().subscribe(res => {
          this.branchParents = res.data;
          if (res.data.find(x => x.brA_FACID == this.fac_id) != null)
            this.student.stU_BRAID = res.data.find(x => x.brA_FACID == this.fac_id).id;
          else
            this.student.stU_BRAID = 0;
          this.resetCode();
        });
      }
      else {
        this.branchSer.getAll().subscribe(res => {
          this.branchParents = res.data;
          if (res.data.find(x => x.brA_FACID == this.fac_id) != null)
            this.student.stU_BRAID = res.data.find(x => x.brA_FACID == this.fac_id).id;
          else
            this.student.stU_BRAID = 0;
          this.resetCode();
        });
      }
    }
  }

  generateClass() {
    if (this.student.stU_BRAID != null && this.student.stU_BRAID != 0 && this.student.stU_SCYEID != null && this.student.stU_SCYEID != 0) {
      this.classSer.getListByBranchIdAndSchoolYearId(this.student.stU_BRAID, this.student.stU_SCYEID).subscribe(res => {
        this.classes = res.data;
        if (res.data != null)
          this.student.stU_CLAID = res.data[0].id;
        else
          this.student.stU_CLAID = 0;
        this.resetCode();
        // console.log(res.data);
      });
    }
  }

  generateUser() {
    if (this.student.stU_CODE.length >= this.lengthUsername.seT_VALUE) {
      this.user.usE_USERNAME = this.student.stU_CODE;
    }
    else {
      this.user.usE_USERNAME = this.removeAccents(this.student.stU_LASTNAME.trim().replace(' ', '') + this.student.stU_FIRSTNAME.trim().replace(' ', '') + this.student.stU_CODE).toLowerCase();
    }
    this.user.usE_PASSWORD = this.student.stU_DATEOFBIRTH.toString().split('-').reverse().join('/');
    $("#field_stu_10").val(this.student.stU_DATEOFBIRTH.toString().split('-').reverse().join('/'));
  }

  removeAccents(str: string) {
    return str.normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .replace(/đ/g, 'd').replace(/Đ/g, 'D');
  }

  showPassword(id: number) {
    if ($("#field_stu_" + id).attr('type') == 'password') {
      $("#field_stu_" + id).prop('type', 'text');
      $("#button_field_stu_" + id).prop('title', 'Ẩn mật khẩu');
      $("#icon_field_stu_" + id).removeClass('fa-eye').addClass('fa-eye-slash');
    }
    else {
      $("#field_stu_" + id).prop('type', 'password');
      $("#button_field_stu_" + id).prop('title', 'Hiện mật khẩu');
      $("#icon_field_stu_" + id).removeClass('fa-eye-slash').addClass('fa-eye');
    }
  }

  clearField() {
    for (let i = 1; i <= 18; i++) {
      if (i == 14) {
        $("#field_stu_" + i + ">.ng-select-container").removeAttr("style");
        $("#valid_mes_stu_" + i).html("");
      }
      else {
        $("#field_stu_" + i).removeClass('is-invalid is-valid');
        $("#valid_mes_stu_" + i).html("");
      }
    }
    this.fac_id = 0;
    this.list_permission = [];
    this.file = null;
    this.fileIsExist = null;
    $("#label_field_stu_15").html("Tải tệp lên");
    this.url = "";
    $("#field_stu_10").val("");

    $("#field_stu_9").prop('type', 'password');
    $("#button_field_stu_9").prop('title', 'Hiện mật khẩu');
    $("#icon_field_stu_9").removeClass('fa-eye-slash').addClass('fa-eye');
    $("#field_stu_10").prop('type', 'password');
    $("#button_field_stu_10").prop('title', 'Hiện mật khẩu');
    $("#icon_field_stu_10").removeClass('fa-eye-slash').addClass('fa-eye');
  }

  checkField() {
    for (let i = 1; i <= 18; i++) {
      if (i == 14) {
        $("#field_stu_" + i + ">.ng-select-container").removeAttr("style").attr("style", "border-color:#28a745;");
        $("#valid_mes_stu_" + i).html("");
      }
      else {
        $("#field_stu_" + i).removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_stu_" + i).html("");
      }
    }

    this.file = null;
    $("#field_stu_9").prop('type', 'password');
    $("#button_field_stu_9").prop('title', 'Hiện mật khẩu');
    $("#icon_field_stu_9").removeClass('fa-eye-slash').addClass('fa-eye');
    $("#field_stu_10").prop('type', 'password');
    $("#button_field_stu_10").prop('title', 'Hiện mật khẩu');
    $("#icon_field_stu_10").removeClass('fa-eye-slash').addClass('fa-eye');
  }

  validate_stu_firstname() {
    if ($("#field_stu_1").val() == '') {
      $("#field_stu_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_1").html("Vui lòng nhập họ sinh viên!");
      return false;
    }
    else {
      let isHaveNumber = false;
      let arr = $("#field_stu_1").val().toString().split('');
      for (let i = 0; i < arr.length; i++) {
        if ($.isNumeric(arr[i])) {
          isHaveNumber = true;
          break;
        }
      }
      if (isHaveNumber == true) {
        $("#field_stu_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_stu_1").html("Vui lòng nhập đúng định dạng họ tên!");
        return false;
      }
      else {
        $("#field_stu_1").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_stu_1").html("");
        return true;
      }
    }
  }

  validate_stu_lastname() {
    if ($("#field_stu_2").val() == '') {
      $("#field_stu_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_2").html("Vui lòng nhập tên sinh viên!");
      return false;
    }
    else {
      let isHaveNumber = false;
      let arr = $("#field_stu_2").val().toString().split('');
      for (let i = 0; i < arr.length; i++) {
        if ($.isNumeric(arr[i])) {
          isHaveNumber = true;
          break;
        }
      }
      if (isHaveNumber == true) {
        $("#field_stu_2").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_stu_2").html("Vui lòng nhập đúng định dạng họ tên!");
        return false;
      }
      else {
        $("#field_stu_2").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_stu_2").html("");
        return true;
      }
    }
  }

  validate_stu_dateofbirth() {
    if ($("#field_stu_3").val() == '') {
      $("#field_stu_3").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_3").html("Vui lòng nhập ngày sinh!");
      return false;
    }
    else if (parseInt($("#field_stu_3").val().toString().split('-')[0]) > (new Date().getFullYear() - 18)) {
      $("#field_stu_3").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_3").html("Ngày sinh không hợp lệ, ít nhất 18 tuổi trở lên!");
      return false;
    }
    else {
      $("#field_stu_3").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_stu_3").html("");
      return true;
    }
  }

  validate_stu_phone() {
    if (this.validateSer.validate_phonenumber($("#field_stu_4").val().toString()) == '1') {
      $("#field_stu_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_4").removeClass('text-success').html("Vui lòng nhập số điện thoại!");
      return false;
    }
    else if (this.validateSer.validate_phonenumber($("#field_stu_4").val().toString()) == '2') {
      $("#field_stu_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_4").removeClass('text-success').html("Số điện thoại sai định dạng!");
      return false;
    }
    else if (this.students.find(x => this.validateSer.customizePhoneNumber(x.stU_PHONE) == this.validateSer.customizePhoneNumber($("#field_stu_4").val().toString()) && x.id != this.student.id) != null || this.emplyees.find(x => this.validateSer.customizePhoneNumber(x.emP_PHONE) == this.validateSer.customizePhoneNumber($("#field_stu_4").val().toString())) != null) {
      $("#field_stu_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_4").removeClass('text-success').html("Số điện thoại đã được sử dụng!");
      return false;
    }
    else {
      $("#field_stu_4").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_stu_4").addClass('text-success').html("Số điện thoại " + this.validateSer.validate_phonenumber($("#field_stu_4").val().toString()));
      return true;
    }
  }

  validate_stu_identity() {
    if ($("#field_stu_16").val() == '') {
      $("#field_stu_16").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_16").removeClass('text-success').html("Vui lòng nhập CMND!");
      return false;
    }
    else if (!$.isNumeric($("#field_stu_16").val())) {
      $("#field_stu_16").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_16").removeClass('text-success').html("CMND sai định dạng!");
      return false;
    }
    else if ($("#field_stu_16").val().toString().length != 9 && $("#field_stu_16").val().toString().length != 12) {
      $("#field_stu_16").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_16").removeClass('text-success').html("CMND phải đủ 9 hoặc 12 số!");
      return false;
    }
    else if (this.students.find(x => x.stU_IDENTITY == $("#field_stu_16").val().toString() && x.id != this.student.id) != null || this.emplyees.find(x => x.emP_IDENTITY == $("#field_stu_16").val().toString())) {
      $("#field_stu_16").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_16").removeClass('text-success').html("CMND đã được sử dụng!");
      return false;
    }
    else {
      $("#field_stu_16").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_stu_16").html("");
      return true;
    }
  }

  validate_stu_email() {
    var regex = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if ($("#field_stu_6").val() == '') {
      $("#field_stu_6").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_6").html("Vui lòng nhập địa chỉ email!");
      return false;
    }
    else if (!regex.test($("#field_stu_6").val().toString())) {
      $("#field_stu_6").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_6").html("Email sai định dạng!");
      return false;
    }
    else if (this.students.find(x => x.stU_EMAIL == $("#field_stu_6").val() && x.id != this.student.id) != null || this.emplyees.find(x => x.emP_EMAIL == $("#field_stu_6").val()) != null) {
      $("#field_stu_6").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_6").html("Email đã được sử dụng!");
      return false;
    }
    else {
      $("#field_stu_6").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_stu_6").html("");
      return true;
    }
  }

  validate_stu_address() {
    $("#field_stu_5").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_stu_5").html("");
    return true;
  }

  validate_stu_faculty() {
    $("#field_stu_12").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_stu_12").html("");
  }

  validate_stu_braid() {
    if ($("#field_stu_13").val() == '' || $("#field_stu_13").val() == 0 || $("#field_stu_13").val() == null) {
      $("#field_stu_13").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_13").html("Vui lòng chọn ngành học!");
      return false;
    }
    else {
      $("#field_stu_13").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_stu_13").html("");
      return true;
    }
  }

  validate_stu_scyeid() {
    if ($("#field_stu_17").val() == '' || $("#field_stu_17").val() == 0 || $("#field_stu_17").val() == null) {
      $("#field_stu_17").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_17").html("Vui lòng chọn niên khóa!");
      return false;
    }
    else {
      $("#field_stu_17").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_stu_17").html("");
      return true;
    }
  }

  validate_stu_claid() {
    if ($("#field_stu_18").val() == '' || $("#field_stu_18").val() == 0 || $("#field_stu_18").val() == null) {
      $("#field_stu_18").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_18").html("Vui lòng chọn lớp học!");
      return false;
    }
    else {
      $("#field_stu_18").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_stu_18").html("");
      return true;
    }
  }

  validate_stu_code() {
    if ($("#field_stu_7").val() == '') {
      $("#field_stu_7").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_7").html("Vui lòng nhập mã sinh viên!");
      return false;
    }
    else {
      let studentCode = $("#field_stu_7").val().toString().trim();
      let isExist = false;
      for (let i = 0; i < this.students.length; i++) {
        if (studentCode == this.students[i].stU_CODE && this.student.id != this.students[i].id) {
          isExist = true;
          break;
        }
      }

      if (isExist == true) {
        $("#field_stu_7").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_stu_7").html("Mã sinh viên đã tồn tại!");
        return false;
      }
      else {
        $("#field_stu_7").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_stu_7").html("");
        return true;
      }
    }
  }

  validate_stu_username() {
    if ($("#field_stu_8").val() == '') {
      $("#field_stu_8").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_8").html("Vui lòng nhập tên tài khoản!");
      return false;
    }
    else if ($("#field_stu_8").val().toString().length < this.lengthUsername.seT_VALUE) {
      $("#field_stu_8").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_8").html("Tên tài khoản bắt buộc từ " + this.lengthUsername.seT_VALUE + " ký tự!");
      return false;
    }
    else {
      $("#field_stu_8").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_stu_8").html("");
      return true;
    }
  }

  validate_stu_password() {
    if ($("#field_stu_9").val() == '') {
      $("#field_stu_9").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_9").html("Vui lòng nhập mật khẩu!");
      return false;
    }
    else if ($("#field_stu_9").val().toString().length < this.lengthPassword.seT_VALUE) {
      $("#field_stu_9").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_9").html("Mật khẩu bắt buộc từ " + this.lengthPassword.seT_VALUE + " ký tự!");
      return false;
    }
    else {
      $("#field_stu_9").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_stu_9").html("");
      return true;
    }
  }

  validate_stu_confirmpassword() {
    if ($("#field_stu_10").val() == '') {
      $("#field_stu_10").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_10").html("Vui lòng nhập xác nhận mật khẩu!");
      return false;
    }
    else if ($("#field_stu_10").val() != $("#field_stu_9").val()) {
      $("#field_stu_10").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_10").html("Xác nhận mật khẩu không khớp!");
      return false;
    }
    else {
      $("#field_stu_10").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_stu_10").html("");
      return true;
    }
  }

  validate_stu_status() {
    $("#field_stu_11").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_stu_11").html("");
    return true;
  }

  validate_stu_permission() {
    if (this.list_permission[0] == null) {
      $("#field_stu_14>.ng-select-container").removeAttr("style").attr("style", "border-color:#dc3545;");
      $("#valid_mes_stu_14").html("Vui lòng chọn vai trò!");
      return false;
    }
    else {
      $("#field_stu_14>.ng-select-container").removeAttr("style").attr("style", "border-color:#28a745;");
      $("#valid_mes_stu_14").html("");
      return true;
    }
  }

  validate_stu_avatar() {
    $("#field_stu_15").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_stu_15").html("");
  }

  validate_stu_importfile() {
    if (this.fileImport == null) {
      $("#field_stu_import").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_stu_import").html("Vui lòng tải tệp lên!");
      return false;
    }
    else {
      $("#field_stu_import").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_stu_import").html("");
      return true;
    }
  }

  validate() {
    var check = true;
    if (this.validate_stu_firstname() == false)
      check = false;
    if (this.validate_stu_lastname() == false)
      check = false;
    if (this.validate_stu_dateofbirth() == false)
      check = false;
    if (this.validate_stu_phone() == false)
      check = false;
    if (this.validate_stu_email() == false)
      check = false;
    if (this.validate_stu_code() == false)
      check = false;
    if (this.validate_stu_username() == false)
      check = false;
    if (this.validate_stu_password() == false)
      check = false;
    if (this.validate_stu_confirmpassword() == false)
      check = false;
    if (this.validate_stu_permission() == false)
      check = false;
    if (this.validate_stu_identity() == false)
      check = false;
    if (this.validate_stu_braid() == false)
      check = false;
    if (this.validate_stu_claid() == false)
      check = false;
    if (this.validate_stu_scyeid() == false)
      check = false;

    this.validate_stu_address();
    this.validate_stu_avatar();
    this.validate_stu_faculty();
    //validate all
    if (check == false) {
      $("#valid_stu_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_stu_" + id).is(":visible")) {
      $("#buttonicon_stu_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_stu_" + id).slideToggle();
    }
    else {
      $("#buttonicon_stu_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_stu_" + id).slideToggle();
    }
  }

  ToggleGroupMulti(id: number) {
    if ($("#detailToggle_stu_mul_" + id).is(":visible")) {
      $("#buttonicon_stu_mul_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_stu_mul_" + id).slideToggle();
    }
    else {
      $("#buttonicon_stu_mul_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_stu_mul_" + id).slideToggle();
    }
  }

  openModal(id: number = 0) {
    this.unlockModal("addupdate");
    if (id > 0) { // update

      this.studentSer.get(id).subscribe(res => {
        this.student = res.data;
        // console.log(res.data);
        this.student.stU_DATEOFBIRTH = this.student.stU_DATEOFBIRTH.substring(0, 10);
        $("#field_stu_10").val(res.data.users.usE_PASSWORD);
        this.user = res.data.users;
        this.url = this.student.fileUrl;

        this.userPermissionSer.getListPermissionIdByUserId(res.data.users.id).subscribe(res1 => {
          this.list_permission = res1.data;
        });

        this.fac_id = this.faculties.find(x => x.id == this.branches.find(x => x.id == this.student.stU_BRAID).brA_FACID).id;
        this.branchSer.getAllParentBranch().subscribe(res1 => {
          this.branchParents = res1.data;
          this.classSer.getListByBranchIdAndSchoolYearId(this.student.stU_BRAID, this.student.stU_SCYEID).subscribe(res2 => {
            this.classes = res2.data;
          });
        });

        if (res.data.stU_AVATAR != null) {
          $("#label_field_stu_15").html(
            res.data.stU_AVATAR.split('/')[3].length > 30 ?
              res.data.stU_AVATAR.split('/')[3].substring(0, 9) + "..." + res.data.stU_AVATAR.split('/')[3].substring(res.data.stU_AVATAR.split('/')[3].length - 9, res.data.stU_AVATAR.split('/')[3].length) :
              res.data.stU_AVATAR.split('/')[3]
          );
          this.fileIsExist = true;
        }
        else {
          $("#label_field_stu_15").html("Tải ảnh lên...");
          this.fileIsExist = null;
        }
      });

      this.checkField();
      $("#modal_title_stu").html("Sửa thông tin sinh viên");
      this.modal.show();
    }
    else { // insert
      if (this.autoGenCode.seT_ISACTIVE == true) {
        this.student = {
          id: 0,
          stU_GENDER: true,
          stU_CODE: "XXXXXXXXX",
          stU_BRAID: 0,
          stU_CLAID: 0,
          stU_SCYEID: 0
        } as Student;
        this.user = {
          id: 0,
          usE_STATUS: 1
        } as User;
      }
      else {
        this.student = {
          id: 0,
          stU_GENDER: true,
          stU_BRAID: 0,
          stU_CLAID: 0,
          stU_SCYEID: 0
        } as Student;
        this.user = {
          id: 0,
          usE_STATUS: 1
        } as User;
      }

      this.clearField();
      $("#modal_title_stu").html("Thêm sinh viên");
      this.modal.show();
    }
  }

  save() {

    if (this.validate() == false) {
      return false;
    }

    this.lockModal("addupdate");

    this.student.origin = window.location.origin;

    if (this.student.id === 0) { // insert
      this.student.stU_CREATEDBY = parseInt(this.cookieSer.get('Id'));
      this.user.usE_CREATEDBY = parseInt(this.cookieSer.get('Id'));
      this.studentRequest = {
        student: this.student,
        user: this.user,
        user_permissions: this.list_permission
      } as StudentRequest;

      if (this.file != null) {
        const formData = new FormData();
        formData.append('file', this.file);
        this.fileSer.add("Users", formData).subscribe(res1 => {
          this.student.stU_AVATAR = res1.data;
          this.studentSer.add(this.studentRequest).subscribe(res => {
            if (res.errorCode === 0) {
              this.studentSer.getAll().subscribe(res2 => {
                this.students = res2.data;
                this.rerender();
                this.modal.hide();
                this.pnotify.success({
                  title: 'Thông báo',
                  text: 'Thêm sinh viên thành công!'
                });
              });
            }
          });
        });
      }
      else {
        this.studentSer.add(this.studentRequest).subscribe(res => {
          if (res.errorCode === 0) {
            this.studentSer.getAll().subscribe(res1 => {
              this.students = res1.data;
              this.rerender();
              this.modal.hide();
              this.pnotify.success({
                title: 'Thông báo',
                text: 'Thêm sinh viên thành công!'
              });
            });
          }
        });
      }
    }
    else { // update
      this.student.stU_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.user.usE_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.studentRequest = {
        student: this.student,
        user: this.user,
        user_permissions: this.list_permission
      } as StudentRequest;

      if (this.file != null) {
        const formData = new FormData();
        formData.append('file', this.file);
        if (this.student.stU_AVATAR != null) {
          this.fileSer.put(this.student.stU_AVATAR.split('/')[3], "Users", formData).subscribe(res => {
            this.student.stU_AVATAR = res.data;
            this.studentSer.put(this.studentRequest).subscribe(res1 => {
              if (res1.errorCode === 0) {
                this.studentSer.getAll().subscribe(res2 => {
                  this.students = res2.data;
                  this.rerender();
                  this.modal.hide();
                  this.pnotify.success({
                    title: 'Thông báo',
                    text: 'Sửa thông tin sinh viên thành công!'
                  });
                });
              }
              else {
                this.modal.hide();
                this.pnotify.error({
                  title: 'Thông báo',
                  text: res1.message
                });
              }
              if (this.modalDetail.isShown) {
                this.openModalDetail(res1.data.id);
              }
            });
          });
        }
        else {
          this.fileSer.add("Users", formData).subscribe(res => {
            this.student.stU_AVATAR = res.data;
            this.studentSer.put(this.studentRequest).subscribe(res1 => {
              if (res1.errorCode === 0) {
                this.studentSer.getAll().subscribe(res2 => {
                  this.students = res2.data;
                  this.rerender();
                  this.modal.hide();
                  this.pnotify.success({
                    title: 'Thông báo',
                    text: 'Sửa thông tin sinh viên thành công!'
                  });
                });
              }
              else {
                this.modal.hide();
                this.pnotify.error({
                  title: 'Thông báo',
                  text: res1.message
                });
              }
              if (this.modalDetail.isShown) {
                this.openModalDetail(res1.data.id);
              }
            });
          });
        }
      }
      else {
        if (this.fileIsExist == null && this.student.stU_AVATAR != null) {
          this.fileSer.delete("Users", this.student.stU_AVATAR.split('/')[3]).subscribe(res => {
            this.student.stU_AVATAR = null;
            this.studentSer.put(this.studentRequest).subscribe(res1 => {
              if (res1.errorCode === 0) {
                this.studentSer.getAll().subscribe(res2 => {
                  this.students = res2.data;
                  this.rerender();
                  this.modal.hide();
                  this.pnotify.success({
                    title: 'Thông báo',
                    text: 'Sửa thông tin sinh viên thành công!'
                  });
                });
              }
              else {
                this.modal.hide();
                this.pnotify.error({
                  title: 'Thông báo',
                  text: res1.message
                });
              }
              if (this.modalDetail.isShown) {
                this.openModalDetail(res1.data.id);
              }
            });
          });
        }
        else {
          this.studentSer.put(this.studentRequest).subscribe(res1 => {
            if (res1.errorCode === 0) {
              this.studentSer.getAll().subscribe(res2 => {
                this.students = res2.data;
                this.rerender();
                this.modal.hide();
                this.pnotify.success({
                  title: 'Thông báo',
                  text: 'Sửa thông tin sinh viên thành công!'
                });
              });
            }
            else {
              this.modal.hide();
              this.pnotify.error({
                title: 'Thông báo',
                text: res1.message
              });
            }
            if (this.modalDetail.isShown) {
              this.openModalDetail(res1.data.id);
            }
          });
        }
      }
    }
  }

  openModalDelete(id: number) {
    this.unlockModal("delete");
    this.studentSer.get(id).subscribe(res => {
      this.student = res.data;
      // console.log(res.data);
      this.modalDelete.show();
    });
  }

  delete() {
    this.lockModal("delete");
    if (this.student.id != 0) {
      this.studentSer.delete(this.student.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          this.studentSer.getAll().subscribe(res1 => {
            this.students = res1.data;
            this.rerender();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Xóa sinh viên thành công!'
            });
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
        this.modalDetail.hide();
      });
    }
  }

  openModalDeleteMul() {
    this.unlockModal("multidel");
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      if (dtElement.dtInstance != null) {
        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == "tb_main") {
            let arr = [];
            dtInstance.rows({ selected: true }).every(function () {
              if (this.data()[1] != null) {
                arr.push(this.data()[1]);
              }
            });
            if (arr.length == 0) {
              this.isListItemNull = true;
              this.modalDeletMul.show();
            }
            else {
              this.isListItemNull = false;
              this.studentSer.getListStudentMultiDel(arr).subscribe(res => {
                this.studentsMultiDel = res.data;
                this.modalDeletMul.show();
              });
            }
          }
        });
      }
    });
  }

  deleteMul() {
    this.lockModal("multidel");
    this.studentSer.deleteMulti(this.studentsMultiDel, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
      if (res.errorCode === 0) {
        this.studentSer.getAll().subscribe(res1 => {
          this.students = res1.data;
          this.rerender();
          if ($(".toggle-stu-all").closest('tr').hasClass('selected')) {
            $(".toggle-stu-all").closest('tr').toggleClass('selected');
          }
          this.modalDeletMul.hide();
          this.pnotify.success({
            title: 'Thông báo',
            text: "Xóa danh sách sinh viên thành công!"
          });
        });
      }
    });
  }

  openModalDetail(id: number) {
    this.studentSer.get(id).subscribe(res => {
      this.student = res.data;
      this.modalDetail.show();
    });
  }

  lockModal(key: string) {
    $(".close_modal_stu_" + key).prop("disabled", true);
    $("#save_modal_stu_" + key).prop("disabled", true);
    $("#save_modal_stu_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_stu_" + key).prop("disabled", false);
    $("#save_modal_stu_" + key).prop("disabled", false);
    $("#save_modal_stu_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  openModalExport() {
    this.unlockModal("export");
    if (this.exportType == 1) {
      this.studentSer.getAll().subscribe(res => {
        this.studentsExport = res.data;
        this.rerender1();
      });
    }
    else {
      this.dtElements.forEach((dtElement: DataTableDirective) => {
        if (dtElement.dtInstance != null) {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == "tb_main") {
              let arr = [];
              dtInstance.rows({ selected: true }).every(function () {
                if (this.data()[1] != null) {
                  arr.push(this.data()[1]);
                }
              });
              if (arr.length == 0) {
                this.isListItemNull = true;
              }
              else {
                this.isListItemNull = false;
                this.studentSer.getListStudentMultiDel(arr).subscribe(res => {
                  this.studentsExport = res.data;
                  this.rerender1();
                });
              }
            }
          });
        }
      });
    }
    this.modalExport.show();
  }

  exportExcel() {
    this.lockModal("export");
    this.studentSer.exportExcelLinkDownload(this.studentsExport).subscribe(res => {
      if (res.errorCode === 0) {
        //download file
        //file saver getting cors error
        saveAs(res.data);
        // window.open can't get filename
        // var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        // var blob = new Blob([res.data], { type: contentType });
        // var url = window.URL.createObjectURL(blob);
        // window.open(url);
        // a tag child
        // var link = document.createElement('a');
        // if (typeof link.download === 'string') {
        //   document.body.appendChild(link); // Firefox requires the link to be in the body
        //   link.download = res.data.split('/')[res.data.split('/').length - 1];
        //   link.href = res.data;
        //   link.click();
        //   document.body.removeChild(link); // remove the link when done
        // } else {
        //   location.replace(res.data);
        // }

        this.studentSer.getAll().subscribe(res1 => {
          this.students = res1.data;
          this.rerender();
          if ($(".toggle-stu-all").closest('tr').hasClass('selected')) {
            $(".toggle-stu-all").closest('tr').toggleClass('selected');
          }
        });

        this.modalExport.hide();
        this.pnotify.success({
          title: 'Thông báo',
          text: "Xuất excel danh sách sinh viên thành công!"
        });
      }
    });
  }

  downloadFile(event = null) {
    if (event != null) {
      event.preventDefault();
    }
    saveAs(this.sampleFileLink);
  }

  openModalImport() {
    this.unlockModal("import");

    // clear modal
    this.fileImport = null;
    $("#field_stu_import").val("");
    this.fileImportIsExist = null;
    $("#label_field_stu_import").html("Tải tệp lên...");
    $("#field_stu_import").removeClass('is-invalid is-valid');
    $("#field_stu_import").html("");
    $("#valid_mes_stu_import").html("");
    $("#field_import_1").prop("checked", false);
    $("#field_import_2").prop("checked", false);

    this.studentSer.getSampleFileLink().subscribe(res => {
      this.sampleFileLink = res.data;
    });
    this.modalImport.show();
  }

  ImportExcel() {
    if (!this.validate_stu_importfile()) {
      $("#valid_stu_import").show(0).delay(3000).hide(0);
      return false;
    }

    this.lockModal("import");
    const formData = new FormData();
    formData.append('file', this.fileImport);
    let autoGenCode = $("#field_import_1").is(":checked");
    let autoGenAccount = $("#field_import_2").is(":checked");
    this.studentSer.importExcel(Number(this.cookieSer.get('Id')), autoGenCode, autoGenAccount, formData).subscribe(res => {
      if (res.errorCode === 0) {
        this.errorImport = false;
        this.studentSer.getAll().subscribe(res1 => {
          this.students = res1.data;
          this.rerender();
        });
        this.modalImport.hide();
        this.pnotify.success({
          title: 'Thông báo',
          text: "Thêm danh sách sinh viên thành công!"
        });
      }
      else if (res.errorCode == 405) {
        this.unlockModal("import");
        this.errorImport = true;
        this.errorImportLog = res.message.split("<divider>")[1];
        this.removeFileImport();
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message.split("<divider>")[0]
        });
      }
      else {
        this.modalImport.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message
        });
      }
    });
  }
}
