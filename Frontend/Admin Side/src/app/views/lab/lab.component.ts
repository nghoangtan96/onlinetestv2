import { Component, OnInit, ViewChild } from '@angular/core';
import { Lab, LabService } from '../../services/lab.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { SettingService } from '../../services/setting.service';
import { ActivatedRoute } from '@angular/router';
import { SignalrService } from '../../services/signalr.service';
import { RoleService } from '../../services/role.service';
import { UserPermissionService } from '../../services/user-permission.service';

@Component({
  selector: 'app-lab',
  templateUrl: './lab.component.html',
  styleUrls: ['./lab.component.css']
})
export class LabComponent implements OnInit {

  labid: number;
  lab_Id: number;

  labs: [Lab];
  lab: Lab = {} as Lab;
  labsMultiDel: [Lab];

  pnotify = undefined;
  isListItemNull: boolean = true;
  tabControl: number = 1;

  lab_add: boolean = true;
  lab_upd: boolean = true;
  lab_del: boolean = true;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild('modalDeletMul', { static: false }) modalDeletMul: ModalDirective;
  constructor(private pNotify: PnotifyService, private cookieSer: CookieService,
    private settingSer: SettingService, private route: ActivatedRoute,
    private signalRSer: SignalrService, private labSer: LabService,
    private roleSer: RoleService, private userPermissionSer: UserPermissionService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.dtOptions = {
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      order: [[1, 'asc']]
    };
    this.labSer.getAll().subscribe(res => {
      this.labs = res.data;
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
    //permission
    this.roleSer.get(63).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'lab_add':
                this.lab_add = false;
                break;
              case 'lab_upd':
                this.lab_upd = false;
                break;
              case 'lab_del':
                this.lab_del = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    //realtime
    this.signalRSer.lab.subscribe((res) => {
      if (res === true) {
        this.labSer.getAll().subscribe(res1 => {
          this.labs = res1.data;
          this.rerender();
        });
      }
    });
  }
  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.labid = params['id'];
      if (this.labid != null) {
        // console.log(this.braid);
        this.openModalDetail(this.labid);
      }
    });
  }

  Select_Clear_All() {
    $(".toggle-lab-all").closest('tr').toggleClass('selected');
    if ($(".toggle-lab-all").closest('tr').hasClass('selected')) {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.select();
        });
      });
    }
    else {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.deselect();
        });
      });
    }
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
  }

  clearField() {
    for (let i = 1; i <= 2; i++) {
      $("#field_lab_" + i).removeClass('is-invalid is-valid');
      $("#valid_mes_lab_" + i).html("");
    }
  }

  checkField() {
    for (let i = 1; i <= 2; i++) {
      $("#field_lab_" + i).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_lab_" + i).html("");
    }
  }

  validate_lab_name() {
    if ($("#field_lab_1").val() == '') {
      $("#field_lab_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_lab_1").html("Vui lòng nhập tên phòng thi!");
      return false;
    }
    else {
      let facultyName = $("#field_lab_1").val().toString().trim().toLowerCase();
      let isExist = false;
      for (let i = 0; i < this.labs.length; i++) {
        if (facultyName == this.labs[i].laB_NAME.toLowerCase() && this.lab.id != this.labs[i].id && this.labs[i].laB_STATUS == 1) {
          isExist = true;
          break;
        }
      }

      if (isExist == true) {
        $("#field_lab_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_lab_1").html("Tên phòng thi đã tồn tại!");
        return false;
      }
      else {
        $("#field_lab_1").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_lab_1").html("");
        return true;
      }
    }
  }

  validate_lab_status() {
    $("#field_lab_2").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_lab_2").html("");
    return true;
  }

  validate() {
    var check = true;
    if (this.validate_lab_name() == false)
      check = false;

    this.validate_lab_status();
    //validate all
    if (check == false) {
      $("#valid_lab_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_lab_" + id).is(":visible")) {
      $("#buttonicon_lab_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_lab_" + id).slideToggle();
    }
    else {
      $("#buttonicon_lab_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_lab_" + id).slideToggle();
    }
  }

  ToggleGroupMulti(id: number) {
    if ($("#detailToggle_lab_mul_" + id).is(":visible")) {
      $("#buttonicon_lab_mul_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_lab_mul_" + id).slideToggle();
    }
    else {
      $("#buttonicon_lab_mul_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_lab_mul_" + id).slideToggle();
    }
  }

  openModal(id: number = 0) {
    this.unlockModal("addupd");
    if (id > 0) { // update
      this.checkField();

      this.labSer.get(id).subscribe(res => {
        this.lab = res.data;
      });

      $("#modal_title_lab").html("Sửa phòng thi");
      this.modal.show();
    }
    else { // insert
      this.lab = {
        id: 0,
        laB_STATUS: 1
      } as Lab

      this.clearField();
      $("#modal_title_lab").html("Thêm phòng thi");
      this.modal.show();
    }
  }

  save() {

    if (this.validate() == false) {
      return false;
    }

    this.lockModal("addupd");
    if (this.lab.id === 0) { // insert
      this.lab.laB_CREATEDBY = parseInt(this.cookieSer.get('Id'));

      this.labSer.add(this.lab).subscribe(res => {
        if (res.errorCode === 0) {
          this.labSer.getAll().subscribe(res1 => {
            this.labs = res1.data;
            this.rerender();
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Thêm phòng thi thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
    else { // update
      this.lab.laB_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.labSer.put(this.lab).subscribe(res => {
        if (res.errorCode === 0) {
          this.labSer.getAll().subscribe(res1 => {
            this.labs = res1.data;
            this.rerender();
            this.modal.hide();
            if (this.modalDetail.isShown) {
              this.openModalDetail(res.data.id);
            }
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Sửa phòng thi thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
  }
  openModalDelete(id: number) {
    this.unlockModal("del");
    this.labSer.get(id).subscribe(res => {
      this.lab = res.data;
      this.modalDelete.show();
    });
  }
  delete() {
    this.lockModal("del");
    if (this.lab.id != 0) {
      this.labSer.delete(this.lab.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          this.labSer.getAll().subscribe(res1 => {
            this.labs = res1.data;
            this.rerender();
          });
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Xóa phòng thi thành công!'
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
        this.modalDetail.hide();
      });
    }
  }
  openModalDeleteMul() {
    this.unlockModal("muldel");
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      let arr = [];
      dtInstance.rows({ selected: true }).every(function () {
        if (this.data()[1] != null) {
          arr.push(this.data()[1]);
        }
      });
      if (arr.length == 0) {
        this.isListItemNull = true;
        this.modalDeletMul.show();
      }
      else {
        this.isListItemNull = false;
        this.labSer.getListLabMultiDel(arr).subscribe(res => {
          this.labsMultiDel = res.data;
          // console.log(this.subjectsMultiDel);
          this.modalDeletMul.show();
        });
      }
    });
  }
  deleteMul() {
    this.lockModal("muldel");
    this.labSer.deleteMulti(this.labsMultiDel, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
      if (res.errorCode === 0) {
        this.labSer.getAll().subscribe(res1 => {
          this.labs = res1.data;
          this.rerender();
          if ($(".toggle-lab-all").closest('tr').hasClass('selected')) {
            $(".toggle-lab-all").closest('tr').toggleClass('selected');
          }
          this.modalDeletMul.hide();
          this.pnotify.success({
            title: 'Thông báo',
            text: "Xóa danh sách phòng thi thành công!"
          });
        });
      }
      else {
        if ($(".toggle-lab-all").closest('tr').hasClass('selected')) {
          $(".toggle-lab-all").closest('tr').toggleClass('selected');
        }
        this.modalDeletMul.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message
        });
      }
    });
  }

  openModalDetail(id: number) {
    this.labSer.get(id).subscribe(res => {
      this.lab = res.data;
      this.lab_Id = res.data.id;
      this.modalDetail.show();
    });
  }

  resetDetailModal() {
    this.lab_Id = null;
    //reset tab
    for (let i = 1; i <= 4; i++) {
      if (i == 1) {
        $("#tab_lab_" + i).addClass("active");
        $("#content_lab_" + i).addClass("active");
      }
      else {
        $("#tab_lab_" + i).removeClass("active");
        $("#content_lab_" + i).removeClass("active").addClass("fade");
      }
    }
    this.loadTab(1);
  }

  lockModal(key: string) {
    $(".close_modal_lab_" + key).prop("disabled", true);
    $("#save_modal_lab_" + key).prop("disabled", true);
    $("#save_modal_lab_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_lab_" + key).prop("disabled", false);
    $("#save_modal_lab_" + key).prop("disabled", false);
    $("#save_modal_lab_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  loadTab(key: number) {
    this.tabControl = key;
  }
}
