import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { PartService, Part } from '../../services/part.service';
import { Subjectt, SubjectService } from '../../services/subject.service';
import { SettingService, Setting } from '../../services/setting.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { RoleService, Role } from '../../services/role.service';
import { UserPermissionService } from '../../services/user-permission.service';
import { SignalrService } from '../../services/signalr.service';
import { UserService } from '../../services/user.service';
import { EmployeeSubjectService } from '../../services/employee-subject.service';

@Component({
  selector: 'app-part',
  templateUrl: './part.component.html',
  styleUrls: ['./part.component.css']
})
export class PartComponent implements OnInit {

  @Input() public subjectId: number; //parse parameter through selector
  parid: number;

  pnotify = undefined;
  parts: [Part];
  partsMultiDel: [Part];
  part: Part = {} as Part;
  subject: Subjectt = {} as Subjectt;
  subjects: [Subjectt];
  part_Id: number;
  subjectsConstraint: [number];

  tabControl: number = 1;
  isListItemNull: boolean = true;
  isAllowUpdate: boolean = true;
  activeTab: number = 0;
  par_add: boolean = true;
  par_upd: boolean = true;
  par_del: boolean = true;
  par_con: boolean = true;
  par_ref: boolean = true;

  editorConfig = {
    placeholder: 'Hướng dẫn làm bài...',
    language: 'vi'
  };

  editorViewConfig = {
    toolbar: []
  };

  public Editor = ClassicEditor;
  public EditorView = ClassicEditor;

  difficultDisplay: Setting = {} as Setting;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild('modalDeletMul', { static: false }) modalDeletMul: ModalDirective;
  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  constructor(private pNotify: PnotifyService, private partSer: PartService,
    private cookieSer: CookieService, private subjectSer: SubjectService,
    private settingSer: SettingService, private route: ActivatedRoute,
    private _DomSanitizer: DomSanitizer, private roleSer: RoleService,
    private userPermissionSer: UserPermissionService, private signalRSer: SignalrService, 
    private userSer: UserService, private employeeSubjectSer: EmployeeSubjectService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.dtOptions = {
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      order: [[1, 'asc']]
    };
    if (this.subjectId != null) {
      this.partSer.getListBySubjectId(this.subjectId).subscribe(res => {
        this.parts = res.data;
        // console.log(res.data);
        // console.log(this.subjectId);
        this.dtTrigger.next();
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.columns().every(function () {
            const that = this;
            $('input', this.footer()).on('keyup change', function () {
              if (that.search() !== this['value']) {
                that
                  .search(this['value'])
                  .draw();
              }
            });
          });
        });
      });
      this.subjectSer.get(this.subjectId).subscribe(res => {
        this.subject = res.data;
        // console.log(this.subject);
      });
    } else {
      this.partSer.getAll().subscribe(res => {
        this.parts = res.data;
        this.dtTrigger.next();
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.columns().every(function () {
            const that = this;
            $('input', this.footer()).on('keyup change', function () {
              if (that.search() !== this['value']) {
                that
                  .search(this['value'])
                  .draw();
              }
            });
          });
        });
      });
    }
    this.subjectSer.getAll().subscribe(res => {
      this.subjects = res.data;
    });
    this.settingSer.get(9).subscribe(res => {
      this.difficultDisplay = res.data;
      // console.log(res.data);
    });
    //permission
    this.roleSer.get(11).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.peR_STATUS == 1) {
              if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
                count++;
            }
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'par_add':
                this.par_add = false;
                break;
              case 'par_upd':
                this.par_upd = false;
                break;
              case 'par_del':
                this.par_del = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    this.roleSer.get(21).subscribe(res => {
      // console.log(res.data);
      this.roleSer.get(26).subscribe(res2 => {
        this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
          // console.log(res1.data);
          let count = 0;
          for (let i = 0; i < res1.data.length; i++) {
            if (res1.data[i].permissions.peR_STATUS == 1) {
              if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
                count++;
              }
              if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res2.data.id && x.ropE_ISACTIVE == true) != null) {
                count++;
              }
            }
          }
          if (!(count > 0)) {
            this.par_ref = false;
          }
        });
      });
    });
    this.roleSer.get(105).subscribe(res => {
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        let count = 0;
        let countNoConstraint = 0;
        for (let i = 0; i < res1.data.length; i++) {
          if (res1.data[i].permissions.peR_STATUS == 1) {
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == true) != null) {
              count++;
            }
            if (res1.data[i].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.id && x.ropE_ISACTIVE == false) != null) {
              countNoConstraint++;
            }
          }
        }
        if (count > 0 && countNoConstraint == 0) {
          this.par_con = false;
          this.userSer.get(Number(this.cookieSer.get('Id'))).subscribe(res2 => {
            this.employeeSubjectSer.getListSubjectIdByEmployeeId(res2.data.employees[0].id).subscribe(res3 => {
              this.subjectsConstraint = res3.data;
            });
          });
        }
        else if (countNoConstraint > 0) {
          this.par_con = true;
        }
      })
    });
    //realtime
    this.signalRSer.part.subscribe((res) => {
      if (res === true) {
        if (this.subjectId != null) {
          this.partSer.getListBySubjectId(this.subjectId).subscribe(res1 => {
            this.parts = res1.data;
            this.rerender();
          });
        }
        else {
          this.partSer.getAll().subscribe(res1 => {
            this.parts = res1.data;
            this.rerender();
          });
        }
      }
    });
    this.signalRSer.subject.subscribe((res) => {
      if (res === true) {
        this.subjectSer.getAll().subscribe(res1 => {
          this.subjects = res1.data;
        });
      }
    });
    this.signalRSer.setting.subscribe((res) => {
      if (res === true) {
        this.settingSer.get(9).subscribe(res1 => {
          this.difficultDisplay = res1.data;
        });
      }
    });
  }

  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.parid = params['id'];
      if (this.parid != null) {
        // console.log(this.subid);
        this.openModalDetail(this.parid);
      }
    });
  }

  onReadyCKEditor(editor) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
  }

  Select_Clear_All() {
    $(".toggle-ins-all").closest('tr').toggleClass('selected');
    if ($(".toggle-ins-all").closest('tr').hasClass('selected')) {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.select();
        });
      });
    }
    else {
      this.dtElement.dtInstance.then((dtInstance: any) => {
        dtInstance.rows({ search: 'applied' }).every(function () {
          this.deselect();
        });
      });
    }
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
  }

  clearField() {
    for (let i = 1; i <= 8; i++) {
      // $("#field_ins_" + i).val("");
      if (i == 6)
        $("#field_ins_6>.ck-editor").removeAttr("style");
      else {
        $("#field_ins_" + i).removeClass('is-invalid is-valid');
        $("#valid_mes_ins_" + i).html("");
      }
    }
  }

  checkField() {
    for (let i = 1; i <= 8; i++) {
      if (i == 6)
        $("#field_ins_6>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
      else {
        $("#field_ins_" + i).removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_ins_" + i).html("");
      }
    }
  }

  toggleAllowShuffleQuestion() {
    if ($("#field_ins_10").is(":checked")) {
      this.part.paR_ISALLOWSHUFFLEQUESTION = true;
    }
    else {
      this.part.paR_ISALLOWSHUFFLEQUESTION = false;
      $("#field_ins_9").prop("checked", false);
    }
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_ins_" + id).is(":visible")) {
      $("#buttonicon_ins_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_ins_" + id).slideToggle();
    }
    else {
      $("#buttonicon_ins_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_ins_" + id).slideToggle();
    }
  }

  ToggleGroupMulti(id: number) {
    if ($("#detailToggle_ins_mul_" + id).is(":visible")) {
      $("#buttonicon_ins_mul_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_ins_mul_" + id).slideToggle();
    }
    else {
      $("#buttonicon_ins_mul_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_ins_mul_" + id).slideToggle();
    }
  }

  validate_par_name() {
    if ($("#field_ins_1").val() == '') {
      $("#field_ins_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_ins_1").html("Vui lòng nhập tên học phần!");
      return false;
    }
    else {
      let partName = $("#field_ins_1").val().toString().trim().toLowerCase();
      let isExist = false;
      if (this.subjectId == null) {
        for (let i = 0; i < this.parts.length; i++) {
          if (partName == this.parts[i].paR_NAME.toLowerCase() && this.part.id != this.parts[i].id && this.part.paR_SUBID == this.parts[i].paR_SUBID && this.parts[i].paR_STATUS == 1) {
            isExist = true;
            break;
          }
        }
      }
      else {
        for (let i = 0; i < this.parts.length; i++) {
          if (partName == this.parts[i].paR_NAME.toLowerCase() && this.part.id != this.parts[i].id) {
            isExist = true;
            break;
          }
        }
      }

      if (isExist == true) {
        $("#field_ins_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_ins_1").html("Tên học phần đã tồn tại!");
        return false;
      }
      else {
        $("#field_ins_1").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_ins_1").html("");
        return true;
      }
    }
  }

  validate_par_score() {
    if ($("#field_ins_2").val() == '') {
      $("#field_ins_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_ins_2").html("Vui lòng nhập số điểm học phần!");
      return false;
    }
    else if ($("#field_ins_2").val() <= 0) {
      $("#field_ins_2").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_ins_2").html("Số điểm phải lớn hơn 0!");
      return false;
    }
    else {
      $("#field_ins_2").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_ins_2").html("");
      return true;
    }
  }

  validate_par_level() {
    if (Number($("#field_ins_3").val()) == 0) {
      // console.log($("#field_ins_3").val());
      $("#field_ins_3").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_ins_3").html("Vui lòng chọn độ khó học phần!");
      return false;
    }
    else {
      $("#field_ins_3").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_ins_3").html("");
      return true;
    }
  }

  validate_par_status() {
    if (Number($("#field_ins_4").val()) == 0) {
      // console.log($("#field_ins_4").val());
      $("#field_ins_4").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_ins_4").html("Vui lòng chọn trạng thái học phần!");
      return false;
    }
    else {
      $("#field_ins_4").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_ins_4").html("");
      return true;
    }
  }

  validate_par_subid() {
    if (Number($("#field_ins_7").val()) == 0) {
      // console.log($("#field_ins_7").val());
      $("#field_ins_7").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_ins_7").html("Vui lòng chọn môn học!");
      return false;
    }
    else {
      $("#field_ins_7").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_ins_7").html("");
      return true;
    }
  }

  validate_par_note() {
    $("#field_ins_5").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_ins_5").html("");
  }

  validate_par_direction() {
    $("#field_ins_6>.ck-editor").removeAttr("style").attr("style", "border:1px solid #28a745;");
    $("#valid_mes_ins_6").html("");
  }
  
  validate_par_column() {
    if ($("#field_ins_8").val() == '') {
      $("#field_ins_8").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_ins_8").html("Vui lòng nhập số cột!");
      return false;
    }
    else if ($("#field_ins_8").val() <= 0) {
      $("#field_ins_8").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_ins_8").html("Số cột phải lớn hơn 0!");
      return false;
    }
    else {
      $("#field_ins_8").removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_ins_8").html("");
      return true;
    }
  }

  validate() {
    var check = true;
    if (this.validate_par_name() == false)
      check = false;
    if (this.validate_par_score() == false)
      check = false;
    if (this.validate_par_level() == false)
      check = false;
    if (this.validate_par_status() == false)
      check = false;
    if (this.subjectId == null) {
      if (this.validate_par_subid() == false)
        check = false;
    }
    if (this.validate_par_column() == false)
      check = false;

    this.validate_par_note();
    this.validate_par_direction();

    //validate all
    if (check == false) {
      $("#valid_par_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  openModal(id: number = 0) {
    this.unlockModal("addupd");
    if (id > 0) { // update
      this.checkField();

      this.partSer.get(id).subscribe(res => {
        this.part = res.data;
        if ((res.data.passages != null && res.data.passages.find(x => x.paS_ISDELETED == false) != null) || (res.data.questions != null && res.data.questions.find(x => x.quE_ISDELETED == false) != null)) {
          this.isAllowUpdate = false;
        }
        else {
          this.isAllowUpdate = true;
        }
      });

      $("#modal_title_par").html("Sửa học phần");
      this.modal.show();
    }
    else { // insert
      this.clearField();
      this.isAllowUpdate = true;

      this.part = {
        id: 0,
        paR_DEFAULTLEVEL: 0,
        paR_DEFAULTCOLUMN: 1,
        paR_SUBID: 0,
        paR_STATUS: 1,
        paR_DIRECTION: "",
        paR_ISALLOWNULLOPTION: false,
        paR_ISALLOWNULLQUESTION: false,
        paR_ISALLOWSHUFFLEOPTION: false,
        paR_ISALLOWSHUFFLEQUESTION: false
      } as Part

      $("#modal_title_par").html("Thêm học phần");
      this.modal.show();
    }
  }

  save() {

    if (this.validate() == false) {
      return false;
    }
    this.lockModal("addupd");

    if ($("#field_ins_9").is(":checked"))
      this.part.paR_ISSHUFFLE = true;
    else
      this.part.paR_ISSHUFFLE = false;

    if (this.part.id === 0) { // insert
      this.part.paR_CREATEDBY = parseInt(this.cookieSer.get('Id'));
      if (this.subjectId != null)
        this.part.paR_SUBID = this.subjectId;

      if ($("#field_ins_10").is(":checked"))
        this.part.paR_ISALLOWSHUFFLEQUESTION = true;
      else
        this.part.paR_ISALLOWSHUFFLEQUESTION = false;
      if ($("#field_ins_11").is(":checked"))
        this.part.paR_ISALLOWSHUFFLEOPTION = true;
      else
        this.part.paR_ISALLOWSHUFFLEOPTION = false;
      if ($("#field_ins_12").is(":checked"))
        this.part.paR_ISALLOWNULLQUESTION = true;
      else
        this.part.paR_ISALLOWNULLQUESTION = false;
      if ($("#field_ins_13").is(":checked"))
        this.part.paR_ISALLOWNULLOPTION = true;
      else
        this.part.paR_ISALLOWNULLOPTION = false;

      this.partSer.add(this.part).subscribe(res => {
        if (res.errorCode === 0) {
          if (this.subjectId != null) {
            this.partSer.getListBySubjectId(this.subjectId).subscribe(res1 => {
              this.parts = res1.data;
              this.rerender();
            });
          }
          else {
            this.partSer.getAll().subscribe(res1 => {
              this.parts = res1.data;
              this.rerender();
            });
          }
          this.modal.hide();
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Thêm học phần thành công!'
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
    else { // update
      this.part.paR_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.partSer.put(this.part).subscribe(res => {
        if (res.errorCode === 0) {
          if (this.subjectId != null) {
            this.partSer.getListBySubjectId(this.subjectId).subscribe(res1 => {
              this.parts = res1.data;
              this.rerender();
            });
          }
          else {
            this.partSer.getAll().subscribe(res1 => {
              this.parts = res1.data;
              this.rerender();
            });
          }
          this.modal.hide();
          if (this.modalDetail.isShown) {
            this.openModalDetail(res.data.id);
          }
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Sửa học phần thành công!'
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
  }

  openModalDelete(id: number) {
    this.unlockModal("del");
    this.partSer.get(id).subscribe(res => {
      this.part = res.data;
      this.modalDelete.show();
    });
  }

  delete() {
    this.lockModal("del");
    if (this.part.id != 0) {
      this.partSer.delete(this.part.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          if (this.subjectId != null) {
            this.partSer.getListBySubjectId(this.subjectId).subscribe(res1 => {
              this.parts = res1.data;
              this.rerender();
            });
          } else {
            this.partSer.getAll().subscribe(res1 => {
              this.parts = res1.data;
              this.rerender();
            });
          }
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Xóa học phần thành công!'
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
        this.modalDetail.hide();
      });
    }
  }

  openModalDeleteMul() {
    this.unlockModal("muldel");
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      let arr = [];
      dtInstance.rows({ selected: true }).every(function () {
        if (this.data()[1] != null) {
          arr.push(this.data()[1]);
        }
      });
      if (arr.length == 0) {
        this.isListItemNull = true;
        this.modalDeletMul.show();
      }
      else {
        this.isListItemNull = false;
        this.partSer.getListPartMultiDel(arr).subscribe(res => {
          this.partsMultiDel = res.data;
          this.modalDeletMul.show();
        });
      }
    });
  }

  deleteMul() {
    this.lockModal("muldel");
    this.partSer.deleteMulti(this.partsMultiDel, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
      if (res.errorCode === 0) {
        if (this.subjectId != null) {
          this.partSer.getListBySubjectId(this.subjectId).subscribe(res1 => {
            this.parts = res1.data;
            this.rerender();
          });
        } else {
          this.partSer.getAll().subscribe(res1 => {
            this.parts = res1.data;
            this.rerender();
          });
        }
        if ($(".toggle-ins-all").closest('tr').hasClass('selected')) {
          $(".toggle-ins-all").closest('tr').toggleClass('selected');
        }
        this.modalDeletMul.hide();
        this.pnotify.success({
          title: 'Thông báo',
          text: "Xóa danh sách học phần thành công!"
        });
      }
      else {
        if ($(".toggle-ins-all").closest('tr').hasClass('selected')) {
          $(".toggle-ins-all").closest('tr').toggleClass('selected');
        }
        this.modalDeletMul.hide();
        this.pnotify.error({
          title: 'Thông báo',
          text: res.message
        });
      }
    });
  }

  openModalDetail(id: number) {
    this.partSer.get(id).subscribe(res => {
      this.part = res.data;
      // console.log(res.data);
      if (res.data.passages[0] != null && res.data.questions[0] != null) {
        this.activeTab = 0;
      }
      else if (res.data.passages[0] != null) {
        this.activeTab = 1;
      }
      else if (res.data.questions[0] != null) {
        this.activeTab = 2;
      }
      this.part_Id = id;
      this.modalDetail.show();
    });
  }

  resetDetailModal() {
    this.part_Id = null;
    for (let i = 1; i <= 2; i++) {
      if (i == 1) {
        $("#tab_par_" + i).addClass("active");
        $("#content_par_" + i).addClass("active");
      }
      else {
        $("#tab_par_" + i).removeClass("active");
        $("#content_par_" + i).removeClass("active").addClass("fade");
      }
    }
    this.loadTab(1);
  }

  lockModal(key: string) {
    $(".close_modal_par_" + key).prop("disabled", true);
    $("#save_modal_par_" + key).prop("disabled", true);
    $("#save_modal_par_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_par_" + key).prop("disabled", false);
    $("#save_modal_par_" + key).prop("disabled", false);
    $("#save_modal_par_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  loadTab(key: number) {
    this.tabControl = key;
    if (key == 1) {
      if ($("#pas_tab").hasClass("disabled") == true) {
        this.activeTab = 2;
      }
      else if ($("#que_tab").hasClass("disabled") == true) {
        this.activeTab = 1;
      }
      else {
        this.activeTab = 0;
      }
    }
  }

}
