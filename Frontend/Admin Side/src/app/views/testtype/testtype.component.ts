import { Component, OnInit, ViewChild } from '@angular/core';
import { Testtype, TestTypeService } from '../../services/test-type.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PnotifyService } from '../../services/pnotify.service';
import { CookieService } from 'ngx-cookie-service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { UserPermissionService, User_Permission } from '../../services/user-permission.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SignalrService } from '../../services/signalr.service';
import { RoleService } from '../../services/role.service';

@Component({
  selector: 'app-testtype',
  templateUrl: './testtype.component.html',
  styleUrls: ['./testtype.component.scss']
})
export class TesttypeComponent implements OnInit {
  testtypeid: number;
  testtype_Id: number;

  testtypes: [Testtype];
  testtype: Testtype = {} as Testtype;

  pnotify = undefined;
  isListItemNull: boolean = true;
  tabControl: number = 1;

  test_add: boolean = true;
  test_upd: boolean = true;
  test_del: boolean = true;

  dtTrigger: Subject<any> = new Subject();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;

  @ViewChild('modal', { static: false }) modal: ModalDirective;
  @ViewChild('modalDetail', { static: false }) modalDetail: ModalDirective;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalDirective;
  @ViewChild('modalDeletMul', { static: false }) modalDeletMul: ModalDirective;

  constructor(private testtypeSer: TestTypeService, private pNotify: PnotifyService,
    private cookieSer: CookieService, private signalRSer: SignalrService,
    private userPermissionSer: UserPermissionService, private route: ActivatedRoute, 
    private roleSer: RoleService) {
    this.pnotify = this.pNotify.getPNotify();
  }

  ngOnInit() {
    this.testtypeSer.getAll().subscribe(res => {
      this.testtypes = res.data;
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
    //permission
    this.roleSer.get(55).subscribe(res => {
      // console.log(res.data);
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res1 => {
        // console.log(res1.data);
        for (let i = 0; i < res.data.roles.length; i++) {
          let count = 0;
          for (let j = 0; j < res1.data.length; j++) {
            if (res1.data[j].permissions.role_Permissions.find(x => x.ropE_ROLID == res.data.roles[i].id).ropE_ISACTIVE == true)
              count++;
          }
          if (count == 0) {
            switch (res.data.roles[i].roL_TRIGGER) {
              case 'test_add':
                this.test_add = false;
                break;
              case 'test_upd':
                this.test_upd = false;
                break;
              case 'test_del':
                this.test_del = false;
                break;
              default:
                break;
            }
          }
        }
      });
    });
    //realtime
    this.signalRSer.testtype.subscribe((res) => {
      if (res === true) {
        this.testtypeSer.getAll().subscribe(res1 => {
          this.testtypes = res1.data;
          this.rerender();
        });
      }
    });
  }

  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
      this.testtypeid = params['id'];
      if (this.testtypeid != null) {
        // console.log(this.braid);
        this.openModalDetail(this.testtypeid);
      }
    });
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this['value']) {
              that
                .search(this['value'])
                .draw();
            }
          });
        });
      });
    });
  }

  clearField() {
    for (let i = 1; i <= 2; i++) {
      $("#field_tety_" + i).removeClass('is-invalid is-valid');
      $("#valid_mes_tety_" + i).html("");
    }
  }

  checkField() {
    for (let i = 1; i <= 2; i++) {
      $("#field_tety_" + i).removeClass('is-invalid').addClass('is-valid');
      $("#valid_mes_tety_" + i).html("");
    }
  }

  validate_tety_name() {
    if ($("#field_tety_1").val() == '') {
      $("#field_tety_1").removeClass('is-valid').addClass('is-invalid');
      $("#valid_mes_tety_1").html("Vui lòng nhập tên loại bài thi!");
      return false;
    }
    else {
      let testtypeName = $("#field_tety_1").val().toString().trim().toLowerCase();
      let isExist = false;
      for (let i = 0; i < this.testtypes.length; i++) {
        if (testtypeName == this.testtypes[i].tetY_NAME.toLowerCase() && this.testtype.id != this.testtypes[i].id && this.testtypes[i].tetY_STATUS == 1) {
          isExist = true;
          break;
        }
      }

      if (isExist == true) {
        $("#field_tety_1").removeClass('is-valid').addClass('is-invalid');
        $("#valid_mes_tety_1").html("Tên loại bài thi đã tồn tại!");
        return false;
      }
      else {
        $("#field_tety_1").removeClass('is-invalid').addClass('is-valid');
        $("#valid_mes_tety_1").html("");
        return true;
      }
    }
  }

  validate_tety_status() {
    $("#field_tety_2").removeClass('is-invalid').addClass('is-valid');
    $("#valid_mes_tety_2").html("");
    return true;
  }

  validate() {
    var check = true;
    if (this.validate_tety_name() == false)
      check = false;

    this.validate_tety_status();
    //validate all
    if (check == false) {
      $("#valid_tety_all").show(0).delay(3000).hide(0);
      return false;
    }
    else
      return true;
  }

  ToggleGroup(id: number) {
    if ($("#detailToggle_tety_" + id).is(":visible")) {
      $("#buttonicon_tety_" + id).removeClass().addClass("fa fa-plus");
      $("#detailToggle_tety_" + id).slideToggle();
    }
    else {
      $("#buttonicon_tety_" + id).removeClass().addClass("fa fa-minus");
      $("#detailToggle_tety_" + id).slideToggle();
    }
  }

  openModal(id: number = 0) {
    this.unlockModal("addupd");
    if (id > 0) { // update
      this.checkField();

      this.testtypeSer.get(id).subscribe(res => {
        this.testtype = res.data;
      });

      $("#modal_title_tety").html("Sửa loại bài thi");
      this.modal.show();
    }
    else { // insert
      this.testtype = {
        id: 0,
        tetY_STATUS: 1
      } as Testtype

      this.clearField();
      $("#modal_title_tety").html("Thêm loại bài thi");
      this.modal.show();
    }
  }

  save() {

    if (this.validate() == false) {
      return false;
    }

    this.lockModal("addupd");
    if (this.testtype.id === 0) { // insert
      this.testtype.tetY_CREATEDBY = parseInt(this.cookieSer.get('Id'));

      this.testtypeSer.add(this.testtype).subscribe(res => {
        if (res.errorCode === 0) {
          this.testtypeSer.getAll().subscribe(res1 => {
            this.testtypes = res1.data;
            this.rerender();
            this.modal.hide();
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Thêm loại bài thi thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
    else { // update
      this.testtype.tetY_MODIFIEDBY = parseInt(this.cookieSer.get('Id'));
      this.testtypeSer.put(this.testtype).subscribe(res => {
        if (res.errorCode === 0) {
          this.testtypeSer.getAll().subscribe(res1 => {
            this.testtypes = res1.data;
            this.rerender();
            this.modal.hide();
            if (this.modalDetail.isShown) {
              this.openModalDetail(res.data.id);
            }
            this.pnotify.success({
              title: 'Thông báo',
              text: 'Sửa loại bài thi thành công!'
            });
          });
        }
        else {
          this.modal.hide();
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
      });
    }
  }
  openModalDelete(id: number) {
    this.unlockModal("del");
    this.testtypeSer.get(id).subscribe(res => {
      this.testtype = res.data;
      this.modalDelete.show();
    });
  }
  delete() {
    this.lockModal("del");
    if (this.testtype.id != 0) {
      this.testtypeSer.delete(this.testtype.id, parseInt(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.errorCode === 0) {
          this.testtypeSer.getAll().subscribe(res1 => {
            this.testtypes = res1.data;
            this.rerender();
          });
          this.pnotify.success({
            title: 'Thông báo',
            text: 'Xóa loại bài thi thành công!'
          });
        }
        else {
          this.pnotify.error({
            title: 'Thông báo',
            text: res.message
          });
        }
        this.modalDelete.hide();
        this.modalDetail.hide();
      });
    }
  }

  openModalDetail(id: number) {
    this.testtypeSer.get(id).subscribe(res => {
      this.testtype = res.data;
      this.testtype_Id = res.data.id;
      this.modalDetail.show();
    });
  }

  resetDetailModal() {
    this.testtype_Id = null;
    //reset tab
    for (let i = 1; i <= 2; i++) {
      if (i == 1) {
        $("#tab_tety_" + i).addClass("active");
        $("#content_tety_" + i).addClass("active");
      }
      else {
        $("#tab_tety_" + i).removeClass("active");
        $("#content_tety_" + i).removeClass("active").addClass("fade");
      }
    }
    this.loadTab(1);
  }

  lockModal(key: string) {
    $(".close_modal_tety_" + key).prop("disabled", true);
    $("#save_modal_tety_" + key).prop("disabled", true);
    $("#save_modal_tety_" + key).html("").html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Đang xử lý...");
  }

  unlockModal(key: string) {
    $(".close_modal_tety_" + key).prop("disabled", false);
    $("#save_modal_tety_" + key).prop("disabled", false);
    $("#save_modal_tety_" + key).html("").html("<i class='fa fa-check'></i> OK");
  }

  loadTab(key: number) {
    this.tabControl = key;
  }
}
