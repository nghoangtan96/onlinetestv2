import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
// import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';
import { CookieService } from 'ngx-cookie-service';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgSelectModule } from '@ng-select/ng-select';
import { DataTablesModule } from 'angular-datatables';
import { PopoverModule } from 'ngx-bootstrap/popover';

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
  
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FormsModule } from '@angular/forms';
import { PnotifyService } from './services/pnotify.service';
import { TesttypeComponent } from './views/testtype/testtype.component';
import { AppInterceptor } from './app.interceptor';
import { EmployeeComponent } from './views/employee/employee.component';
import { InformationComponent } from './views/information/information.component';
import { UserComponent } from './views/user/user.component';
import { ClassComponent } from './views/class/class.component';
import { StudentComponent } from './views/student/student.component';
import { PositionComponent } from './views/position/position.component';
import { InfoUserComponent } from './views/info-user/info-user.component';
import { ReportsComponent } from './views/reports/reports.component';
import { PermissionComponent } from './views/permission/permission.component';
import { SettingComponent } from './views/setting/setting.component';
import { LogComponent } from './views/log/log.component';
import { BranchComponent } from './views/branch/branch.component';
import { SubjectComponent } from './views/subject/subject.component';
import { PartComponent } from './views/part/part.component';
import { AnswertypeComponent } from './views/answertype/answertype.component';
import { QuestionComponent } from './views/question/question.component';
import { PassageComponent } from './views/passage/passage.component';
import { ScriptHackComponent } from './views/script-hack/script-hack.component';
import { FacultyComponent } from './views/faculty/faculty.component';
import { NoficationComponent } from './views/nofication/nofication.component';
import { SchoolyearComponent } from './views/schoolyear/schoolyear.component';
import { LabComponent } from './views/lab/lab.component';
import { TestshiftComponent } from './views/testshift/testshift.component';
import { TestComponent } from './views/test/test.component';
import { ScheduleTestComponent } from './views/schedule-test/schedule-test.component';
import { ScheduleviewComponent } from './views/scheduleview/scheduleview.component';
import { RoomviewComponent } from './views/roomview/roomview.component';
import { ExamComponent } from './views/exam/exam.component';
import { CountdownModule } from 'ngx-countdown';
import { ResultComponent } from './views/result/result.component';
import { NgxAudioPlayerModule } from 'ngx-audio-player';
// import { FileTeacherComponent } from './views/file-teacher/file-teacher.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    NgSelectModule,
    FormsModule,
    ChartsModule,
    HttpClientModule,
    ModalModule.forRoot(),
    CKEditorModule,
    DataTablesModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    TooltipModule.forRoot(),
    PopoverModule.forRoot(),
    CountdownModule,
    NgxAudioPlayerModule
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
    TesttypeComponent,
    EmployeeComponent,
    InformationComponent,
    UserComponent,
    ClassComponent,
    StudentComponent,
    PositionComponent,
    InfoUserComponent,
    ReportsComponent,
    PermissionComponent,
    SettingComponent,
    LogComponent,
    BranchComponent,
    SubjectComponent,
    PartComponent,
    AnswertypeComponent,
    QuestionComponent,
    PassageComponent,
    ScriptHackComponent,
    FacultyComponent,
    NoficationComponent,
    SchoolyearComponent,
    LabComponent,
    TestshiftComponent,
    TestComponent,
    ScheduleTestComponent,
    ScheduleviewComponent,
    RoomviewComponent,
    ExamComponent,
    ResultComponent
    // FileTeacherComponent,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppInterceptor,
      multi: true
    },
    CookieService,
    PnotifyService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
