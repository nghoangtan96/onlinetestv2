import { Injectable } from '@angular/core';
import { User } from './user.service';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';

export interface ListNoficationResult {
  errorCode: number;
  message: string;
  data: [Nofication];
}
export interface Nofication {
  id: number;
  noF_USEID: number;
  noF_TITLE: string;
  noF_CONTENT: string;
  noF_LINK: string;
  noF_KEYPARAM: string;
  noF_PARAM: number;
  noF_STATUS: number;
  noF_CREATEDBY: number;
  noF_CREATEDDATE: Date;
  noF_MODIFIEDDATE: Date;
  user: User;
  userCreated: User;
  timeSpan: string;
}
export interface NoficationResult {
  errorCode: number;
  message: string;
  data: Nofication;
}

@Injectable({
  providedIn: 'root'
})
export class NoficationService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAllNoficationByUserId(use_id): Observable<ListNoficationResult> {
    return this._http.get<ListNoficationResult>(`${this.baseapi.url.nofication}/getAllNoficationByUserId/${use_id}`);
  }
  getTop5NoficationNewestOrAllNotReadByUserId(use_id): Observable<ListNoficationResult> {
    return this._http.get<ListNoficationResult>(`${this.baseapi.url.nofication}/getTop5NoficationNewestOrAllNotReadByUserId/${use_id}`);
  }
  get(id: number): Observable<NoficationResult> {
    return this._http.get<NoficationResult>(`${this.baseapi.url.nofication}/${id}`);
  }
  messageRead(userId: number): Observable<ListNoficationResult> {
    return this._http.get<ListNoficationResult>(`${this.baseapi.url.nofication}/messageRead/${userId}`);
  }
}
