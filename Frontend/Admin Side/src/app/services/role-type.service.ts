import { Injectable } from '@angular/core';
import { Role } from './role.service';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';

export interface ListRoleTypeResult {
  errorCode: number;
  Message: string;
  data: [RoleType];
}
export interface RoleType {
  id: number;
  rotY_NAME: string;
  rotY_STATUS: number;
  roles: [Role];
}
export interface RoleTypeResult {
  errorCode: number;
  Message: string;
  data: RoleType;
}

@Injectable({
  providedIn: 'root'
})
export class RoleTypeService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListRoleTypeResult> {
    return this._http.get<ListRoleTypeResult>(this.baseapi.url.roletype);
  }
}
