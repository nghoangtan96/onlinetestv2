import { Injectable } from '@angular/core';
import { Employee } from './employee.service';
import { Subjectt } from './subject.service';
import { User } from './user.service';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

export interface ListEmployeeSubjectResult {
  errorCode: number;
  Message: string;
  data: [Employee_Subject];
}
export interface ListSubjectIdResult {
  errorCode: number;
  Message: string;
  data: [number];
}
export interface Employee_Subject {
  id: number;
  emsU_EMPID: number;
  emsU_SUBID: number;
  emsU_CREATEDBY: number;
  emsU_CREATEDDATE: Date;
  employees: Employee;
  subjects: Subjectt;
  userCreated: User;
}
export interface EmployeeSubjectResult {
  errorCode: number;
  Message: string;
  data: Employee_Subject;
}

@Injectable({
  providedIn: 'root'
})
export class EmployeeSubjectService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getListSubjectIdByEmployeeId(id: number): Observable<ListSubjectIdResult> {
    return this._http.get<ListSubjectIdResult>(`${this.baseapi.url.employee_subject}/getListSubjectIdByEmployeeId/${id}`);
  }
}
