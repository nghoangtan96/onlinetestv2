import { Injectable } from '@angular/core';
import { Question } from './question.service';
import { User } from './user.service';

export interface ListOptionResult {
  errorCode: number;
  message: string;
  data: [Option];
}
export interface Option {
  id: number;
  opT_CONTENT: string;
  opT_ISCORRECT: boolean;
  opT_ORDER: number;
  opT_ISDELTED: boolean;
  opT_STATUS: number;
  opT_CREATEDBY: number;
  opT_CREATEDDATE: Date;
  opT_MODIFIEDBY: number;
  opT_MODIFIEDDATE: Date;
  opT_QUEID: number;
  questions: Question;
  userCreated: User;
  userModified: User;
  isChoose: boolean;
}
export interface OptionResult {
  errorCode: number;
  message: string;
  data: Option;
}

@Injectable({
  providedIn: 'root'
})
export class OptionService {

  constructor() { }
}
