import { Injectable } from '@angular/core';
import { User } from './user.service';
import { Employee } from './employee.service';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { Branch } from './branch.service';

export interface ListFacultyResult {
  errorCode: number;
  message: string;
  data: [Faculty];
}
export interface Faculty {
  id: number;
  faC_CODE: string;
  faC_NAME: string;
  faC_ADDRESS: string;
  faC_PHONE: string;
  faC_EMAIL: string;
  faC_WEBSITE: string;
  faC_INFORMATION: string;
  faC_STATUS: number;
  faC_ISDELETED: boolean;
  faC_CREATEDBY: number;
  faC_CREATEDDATE: Date;
  faC_MODIFIEDBY: number;
  faC_MODIFIEDDATE: Date;
  userCreated: User;
  userModified: User;
  employees: [Employee];
  branches: [Branch];
}
export interface FacultyResult {
  errorCode: number;
  message: string;
  data: Faculty;
}

@Injectable({
  providedIn: 'root'
})
export class FacultyService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListFacultyResult> {
    return this._http.get<ListFacultyResult>(`${this.baseapi.url.faculty}`);
  }
  get(id: number): Observable<FacultyResult> {
    return this._http.get<FacultyResult>(`${this.baseapi.url.faculty}/${id}`);
  }
  getListFacultyMultiDel(arrDel: number[]): Observable<ListFacultyResult> {
    return this._http.post<ListFacultyResult>(`${this.baseapi.url.faculty}/getListFacultyMultiDel`, arrDel);
  }
  add(faculty: Faculty): Observable<FacultyResult> {
    return this._http.post<FacultyResult>(`${this.baseapi.url.faculty}`, faculty);
  }
  put(faculty: Faculty): Observable<FacultyResult> {
    return this._http.put<FacultyResult>(`${this.baseapi.url.faculty}/${faculty.id}`, faculty);
  }
  delete(id, userid): Observable<FacultyResult> {
    return this._http.delete<FacultyResult>(`${this.baseapi.url.faculty}/${id}&${userid}`);
  }
  deleteMulti(arrDel: Faculty[], userid): Observable<FacultyResult> {
    return this._http.post<FacultyResult>(`${this.baseapi.url.faculty}/multipleDelete/${userid}`, arrDel);
  }
}
