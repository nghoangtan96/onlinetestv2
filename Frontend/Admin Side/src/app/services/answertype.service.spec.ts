import { TestBed } from '@angular/core/testing';

import { AnswertypeService } from './answertype.service';

describe('AnswertypeService', () => {
  let service: AnswertypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AnswertypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
