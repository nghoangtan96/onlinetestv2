import { Injectable } from '@angular/core';
import { Setting } from './setting.service';
import { Observable } from 'rxjs';
import { BaseapiService } from './baseapi.service';
import { HttpClient } from '@angular/common/http';

export interface ListSettingTypeResult {
  errorCode: number;
  Message: string;
  data: [SettingType];
}
export interface SettingType {
  id: number;
  setY_NAME: string;
  settings: Setting[];
}
export interface SettingTypeResult {
  errorCode: number;
  Message: string;
  data: SettingType;
}

@Injectable({
  providedIn: 'root'
})
export class SettingtypeService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListSettingTypeResult> {
    return this._http.get<ListSettingTypeResult>(this.baseapi.url.settingtype);
  }
  get(id): Observable<SettingTypeResult> {
    return this._http.get<SettingTypeResult>(`${this.baseapi.url.settingtype}/${id}`);
  }
}
