import { TestBed } from '@angular/core/testing';

import { TestingFormatService } from './testing-format.service';

describe('TestingFormatService', () => {
  let service: TestingFormatService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestingFormatService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
