import { Injectable } from '@angular/core';
import { User } from './user.service';
import { Permission } from './permission.service';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';

// export interface ListUserPermissionResult {
//   errorCode: number;
//   Message: string;
//   data: [number];
// }
// export interface ListUserPermissionFullResult {
//   errorCode: number;
//   Message: string;
//   data: [User_Permission];
// }
export interface User_Permission{
  id: number;
  useP_USEID: number;
  useP_PERID: number;
  useP_CREATEBY: number;
  useP_CREATEDATE: Date;
  useP_MODIFIEDBY: number;
  useP_MODIFIEDDATE:Date;
  useP_STATUS: number;
  useP_ISACTIVE: boolean;
  users: User;
  permissions: Permission;
  userCreated: User;
  userModified: User;
}

export interface ListPermissionResult {
  errorCode: number;
  Message: string;
  data: [User_Permission];
}

export interface ListPermissionIdResult {
  errorCode: number;
  Message: string;
  data: [number];
}

@Injectable({
  providedIn: 'root'
})
export class UserPermissionService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  // get(id): Observable<ListUserPermissionResult> {
  //   return this._http.get<ListUserPermissionResult>(`${this.baseapi.url.userpermisson}/${id}`);
  // }
  // getlistperbyuserid(id): Observable<ListUserPermissionFullResult> {
  //   return this._http.get<ListUserPermissionFullResult>(`${this.baseapi.url.userpermisson}/getlistperbyuserid/${id}`);
  // }
  getListPermissionIdByUserId(id: number): Observable<ListPermissionIdResult> {
    return this._http.get<ListPermissionIdResult>(`${this.baseapi.url.user_permisson}/getListPermissionIdByUserId/${id}`);
  }
  getListPermissionByUserId(id: number): Observable<ListPermissionResult> {
    return this._http.get<ListPermissionResult>(`${this.baseapi.url.user_permisson}/getListPermissionByUserId/${id}`);
  }
}
