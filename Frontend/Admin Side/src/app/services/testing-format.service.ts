import { Injectable } from '@angular/core';
import { Test } from './test.service';

export interface ListTestingFormatResult {
  errorCode: number;
  message: string;
  data: [TestingFormat];
}
export interface TestingFormat {
  id: number;
  tesF_TESID: number;
  tesF_ISFREE: boolean;
  tesF_ISSHOWPART: boolean;
  tesF_ISSHOWSCORE: boolean;
  tesF_ISSHOWPROCTOR: boolean;
  tesF_ISSHOWFEEDBACK: boolean;
  tesF_ISSHOWSIGNATURE: boolean;
  tests: Test;
}
export interface TestingFormatResult {
  errorCode: number;
  message: string;
  data: TestingFormat;
}

@Injectable({
  providedIn: 'root'
})
export class TestingFormatService {

  constructor() { }
}
