import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { User } from './user.service';
import { Testtype } from './test-type.service';
import { Subjectt } from './subject.service';
import { TestRequest } from '../models/test_request';
import { TestDetail } from './test-detail.service';
import { TestQuestion } from './test-question.service';
import { TestingFormat } from './testing-format.service';
import { AcceptRequest } from '../models/acceptRequest';

export interface ListTestResult {
  errorCode: number;
  message: string;
  data: [Test];
}
export interface Test {
  id: number;
  teS_CODE: string;
  teS_TITLE: string;
  teS_VERSION: string;
  teS_DATE: Date;
  teS_TIME: number;
  teS_LANGUAGE: string;
  teS_ISSHUFFLE: boolean;
  teS_ISHEADPHONE: boolean;
  teS_ISFRAME: boolean;
  teS_ISACTIVE: boolean;
  teS_ISLOCKED: boolean;
  teS_PASSWORD: string;
  teS_ENCPASSWORD: string;
  teS_MAXSCORE: number;
  teS_NOTE: string;
  teS_CREATEDBY: number;
  teS_CREATEDDATE: Date;
  teS_ACCEPTEDBY: number;
  teS_ACCEPTEDDATE: Date;
  teS_MODIFIEDBY: number;
  teS_MODIFIEDDATE: Date;
  teS_SUBID: number;
  teS_TETYID: number
  teS_STATUS: number;
  teS_ISDELETED: boolean;
  test_Types: Testtype;
  subjects: Subjectt;
  userCreated: User;
  userAccepted: User;
  userModified: User;
  countQuestion: number;
  test_Details: [TestDetail];
  test_Questions: [TestQuestion];
  testingFormats: [TestingFormat];
}
export interface TestResult {
  errorCode: number;
  message: string;
  data: Test;
}

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListTestResult> {
    return this._http.get<ListTestResult>(`${this.baseapi.url.test}`);
  }
  getListById(id: number): Observable<ListTestResult> {
    return this._http.get<ListTestResult>(`${this.baseapi.url.test}/getListById/${id}`);
  }
  get(id: number): Observable<TestResult> {
    return this._http.get<TestResult>(`${this.baseapi.url.test}/${id}`);
  }
  getTestByTesttypeIdAndSubjectId(testtypeid: number, subjectid: number): Observable<ListTestResult> {
    return this._http.get<ListTestResult>(`${this.baseapi.url.test}/getTestByTesttypeIdAndSubjectId/${testtypeid}&${subjectid}`);
  }
  getListTestMultiDel(arrDel: number[]): Observable<ListTestResult> {
    return this._http.post<ListTestResult>(`${this.baseapi.url.test}/getListTestMultiDel`, arrDel);
  }
  add(testRequest: TestRequest): Observable<TestResult> {
    return this._http.post<TestResult>(`${this.baseapi.url.test}`, testRequest);
  }
  put(testRequest: TestRequest): Observable<TestResult> {
    return this._http.put<TestResult>(`${this.baseapi.url.test}/${testRequest.test.id}`, testRequest);
  }
  acceptTest(acceptRequest: AcceptRequest): Observable<TestResult> {
    return this._http.post<TestResult>(`${this.baseapi.url.test}/acceptTest`, acceptRequest);
  }
  delete(id, userid): Observable<TestResult> {
    return this._http.delete<TestResult>(`${this.baseapi.url.test}/${id}&${userid}`);
  }
  deleteMulti(arrDel: Test[], userid): Observable<TestResult> {
    return this._http.post<TestResult>(`${this.baseapi.url.test}/multipleDelete/${userid}`, arrDel);
  }
}
