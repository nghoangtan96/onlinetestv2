import { Injectable } from '@angular/core';
import { BaseapiService } from './baseapi.service';
import { User } from './user.service';
import { AnswerType } from './answertype.service';
import { Part } from './part.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Question } from './question.service';

export interface ListPassageResult {
  errorCode: number;
  message: string;
  data: [Passage];
}
export interface Passage {
  id: number;
  paS_CONTENT: string;
  paS_ISSHUFFLE: boolean;
  paS_ISSHOWALL: boolean;
  paS_MEDIA: string;
  paS_ISDELETED: boolean;
  paS_STATUS: number;
  paS_ORDER: number;
  paS_ANTYID: number;
  paS_PARID: number;
  paS_CREATEDBY: number;
  paS_CREATEDDATE: Date;
  paS_MODIFIEDBY: number;
  paS_MODIFIEDDATE: Date;
  userCreated: User;
  userModified: User;
  answerTypes: AnswerType;
  parts: Part;
  questions: [Question];
  fileUrl: string;
  countTest: number;
  mediaType: string;
}
export interface PassageResult {
  errorCode: number;
  message: string;
  data: Passage;
}

@Injectable({
  providedIn: 'root'
})
export class PassageService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListPassageResult> {
    return this._http.get<ListPassageResult>(`${this.baseapi.url.passage}`);
  }
  get(id: number): Observable<PassageResult> {
    return this._http.get<PassageResult>(`${this.baseapi.url.passage}/${id}`);
  }
  getListByPartId(id: number): Observable<ListPassageResult> {
    return this._http.get<ListPassageResult>(`${this.baseapi.url.passage}/getListByPartId/${id}`);
  }
  getListBySubjectId(id: number): Observable<ListPassageResult> {
    return this._http.get<ListPassageResult>(`${this.baseapi.url.passage}/getListBySubjectId/${id}`);
  }
  getListByPartIdAndAnswerTypeId(partid: number, answertypeid: number): Observable<ListPassageResult> {
    return this._http.get<ListPassageResult>(`${this.baseapi.url.passage}/getListByPartIdAndAnswerTypeId/${partid}&${answertypeid}`);
  }
  getListPassageMultiDel(arrDel: number[]): Observable<ListPassageResult> {
    return this._http.post<ListPassageResult>(`${this.baseapi.url.passage}/getListPassageMultiDel`, arrDel);
  }
  add(passage: Passage): Observable<PassageResult> {
    return this._http.post<PassageResult>(`${this.baseapi.url.passage}`, passage);
  }
  put(passage: Passage): Observable<PassageResult> {
    return this._http.put<PassageResult>(`${this.baseapi.url.passage}/${passage.id}`, passage);
  }
  delete(id, userid): Observable<PassageResult> {
    return this._http.delete<PassageResult>(`${this.baseapi.url.passage}/${id}&${userid}`);
  }
  deleteMulti(arrDel: Passage[], userid): Observable<PassageResult> {
    return this._http.post<PassageResult>(`${this.baseapi.url.passage}/multipleDelete/${userid}`, arrDel);
  }
}
