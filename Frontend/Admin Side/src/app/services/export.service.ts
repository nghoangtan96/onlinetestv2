import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Observable } from 'rxjs';
import { resolve } from 'path';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';

@Injectable({
  providedIn: 'root'
})
export class ExportService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  fileExtension = '.xlsx';

  public exportExcel(jsonData: any[], fileName: string): void {

    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(jsonData);
    const wb: XLSX.WorkBook = { Sheets: { 'data': ws }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
    this.saveExcelFile(excelBuffer, fileName);
  }

  private saveExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: this.fileType });
    FileSaver.saveAs(data, fileName + this.fileExtension);
  }
  
  public exportAsExcelFile(json: any[], excelFileName: string, headersArray: any[]): void {
    //Excel Title, Header, Data
    const header = headersArray;
    const data = json;
    const Excel = require('exceljs');
    //Create workbook and worksheet
    let workbook = new Excel.Workbook();
    let worksheet = workbook.addWorksheet(excelFileName);
    //Add Header Row
    let headerRow = worksheet.addRow(header);
    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'DCE6F1' }
      }
      cell.font = { name: 'Cambria', family: 4, bold: true, size: 13 };
      cell.alignment = { horizontal: 'center' };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    })
    // Add Data and Conditional Formatting
    data.forEach((element) => {
      let eachRow = [];
      headersArray.forEach((headers) => {
        eachRow.push(element[headers])
      })
      if (element.isDeleted === "Y") {
        let deletedRow = worksheet.addRow(eachRow);
        deletedRow.eachCell((cell, number) => {
          cell.font = { name: 'Cambria', family: 4, size: 13, bold: false, strike: true };
          cell.alignment = { wrapText: true, vertical: 'middle' };
        })
      } else {
        let insertRow = worksheet.addRow(eachRow);
        insertRow.eachCell((cell, number) => {
          cell.font = { name: 'Cambria', family: 4, size: 13, bold: false, strike: false };
          cell.alignment = { wrapText: true, vertical: 'middle' };
        })
      }
    })
    worksheet.getColumn(1).width = 6;
    worksheet.getColumn(2).width = 24;
    worksheet.getColumn(3).width = 15;
    worksheet.getColumn(4).width = 16;
    worksheet.getColumn(4).alignment = { horizontal: 'center', wrapText: true, vertical: 'middle' };
    worksheet.getColumn(5).width = 15;
    worksheet.getColumn(5).alignment = { horizontal: 'center', wrapText: true, vertical: 'middle' };
    worksheet.getColumn(6).width = 17;
    worksheet.getColumn(6).alignment = { horizontal: 'center', wrapText: true, vertical: 'middle' };
    worksheet.getColumn(7).width = 47;
    worksheet.getColumn(8).width = 35;
    worksheet.getColumn(9).width = 16;
    worksheet.getColumn(9).alignment = { horizontal: 'center', wrapText: true, vertical: 'middle' };
    worksheet.getColumn(10).width = 25;
    worksheet.getColumn(10).alignment = { horizontal: 'center', wrapText: true, vertical: 'middle' };
    worksheet.getColumn(11).width = 34;
    worksheet.getColumn(12).width = 34;
    worksheet.getColumn(12).alignment = { horizontal: 'center', wrapText: true, vertical: 'middle' };
    worksheet.addRow([]);
    //Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: this.fileType });
      FileSaver.saveAs(blob, excelFileName + this.fileExtension);
    });
  }
}
