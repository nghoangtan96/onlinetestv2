import { Injectable } from '@angular/core';
import { SettingType, ListSettingTypeResult } from './settingtype.service';
import { User } from './user.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { settingRequest } from '../models/setting_request';

export interface ListSettingResult {
  errorCode: number;
  message: string;
  data: [Setting];
}
export interface Setting {
  id: number;
  seT_NAME: string;
  seT_VALUE: number;
  seT_ISACTIVE: boolean;
  seT_CHOICE: string;
  seT_CHOICEVALUE: string;
  seT_MULTICHOICE: string;
  seT_MULTICHOICEVALUE: string;
  seT_MODIFIEDBY: number;
  seT_MODIFIEDDATE: Date;
  seT_SETYID: number;
  seT_UNIT: string;
  seT_NOTE: string;
  setting_type: SettingType;
  user_modified: User;
}
export interface SettingResult {
  errorCode: number;
  message: string;
  data: Setting;
}

@Injectable({
  providedIn: 'root'
})
export class SettingService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListSettingResult> {
    return this._http.get<ListSettingResult>(this.baseapi.url.setting);
  }
  get(id: number): Observable<SettingResult> {
    return this._http.get<SettingResult>(`${this.baseapi.url.setting}/${id}`);
  }
  updateSetting(settingReqList: settingRequest[]): Observable<ListSettingTypeResult>{
    return this._http.post<ListSettingTypeResult>(`${this.baseapi.url.setting}/updatesetting`, settingReqList);
  }
}
