import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { User } from './user.service';
import { permissionRequest } from '../models/permission_request';
import { RolePermission } from './role-permission.service';

export interface ListPermissionResult {
  errorCode: number;
  message: string;
  data: [Permission];
}
export interface Permission {
  id: number;
  peR_NAME: string;
  peR_ISACTIVE: boolean;
  peR_STATUS: number;
  peR_ISDELETED: boolean;
  peR_CREATEDBY: number;
  peR_CREATEDATE: Date;
  peR_MODIFIEDBY: number;
  peR_MODIFIEDDATE: Date;
  role_Permissions: [RolePermission];
  userCreated: User;
  userModified: User;
}
export interface PermissionResult {
  errorCode: number;
  message: string;
  data: Permission;
}

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListPermissionResult> {
    return this._http.get<ListPermissionResult>(this.baseapi.url.permission);
  }
  get(id): Observable<PermissionResult> {
    return this._http.get<PermissionResult>(`${this.baseapi.url.permission}/${id}`);
  }
  getListPermissionMultiDel(arrDel: number[]): Observable<ListPermissionResult> {
    return this._http.post<ListPermissionResult>(`${this.baseapi.url.permission}/getListPermissionMultiDel`, arrDel);
  }
  add(permissionRequest: permissionRequest): Observable<PermissionResult>{
    return this._http.post<PermissionResult>(`${this.baseapi.url.permission}`, permissionRequest);
  }
  put(permissionRequest: permissionRequest): Observable<PermissionResult>{
    return this._http.put<PermissionResult>(`${this.baseapi.url.permission}/${permissionRequest.permission.id}`, permissionRequest);
  }
  delete(id, userid): Observable<PermissionResult>{ 
    return this._http.delete<PermissionResult>(`${this.baseapi.url.permission}/${id}&${userid}`);
  }
  deleteMulti(arrDel: Permission[], userid): Observable<PermissionResult> {
    return this._http.post<PermissionResult>(`${this.baseapi.url.permission}/multipleDelete/${userid}`, arrDel);
  }
}
