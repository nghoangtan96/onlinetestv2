import { TestBed } from '@angular/core/testing';

import { EmployeePositionService } from './employee-position.service';

describe('EmployeePositionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmployeePositionService = TestBed.get(EmployeePositionService);
    expect(service).toBeTruthy();
  });
});
