import { Injectable, EventEmitter } from '@angular/core';
import * as signalR from "@aspnet/signalr";
import { BaseapiService } from './baseapi.service';

@Injectable({
  providedIn: 'root'
})
export class SignalrService {
  private hubConnection: signalR.HubConnection;

  employee = new EventEmitter<boolean>();
  log = new EventEmitter<boolean>();
  nofication = new EventEmitter<boolean>();
  schoolyear = new EventEmitter<boolean>();
  class = new EventEmitter<boolean>();
  branch = new EventEmitter<boolean>();
  student = new EventEmitter<boolean>();
  permission = new EventEmitter<boolean>();
  role = new EventEmitter<boolean>();
  faculty = new EventEmitter<boolean>();
  subject = new EventEmitter<boolean>();
  setting = new EventEmitter<boolean>();
  part = new EventEmitter<boolean>();
  answertype = new EventEmitter<boolean>();
  passage = new EventEmitter<boolean>();
  question = new EventEmitter<boolean>();
  lab = new EventEmitter<boolean>();
  testshift = new EventEmitter<boolean>();
  testtype = new EventEmitter<boolean>();
  test = new EventEmitter<boolean>();
  scheduletest = new EventEmitter<boolean>();
  roomview = new EventEmitter<boolean>();
  information = new EventEmitter<boolean>();

  constructor(private baseapi: BaseapiService) {
    this.buildConnection();
    this.startConnection();
  }

  private buildConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .configureLogging(signalR.LogLevel.Debug)
      .withUrl("http://localhost:50840/signalHub", {
        skipNegotiation: true,
        transport: signalR.HttpTransportType.WebSockets
      })
      .build();
  };

  private startConnection() {
    this.hubConnection
      .start()
      .then(() => {
        console.log("Connection Started...");
        this.employeeUpdate();
        this.logUpdate();
        this.noficationUpdate();
        this.schoolYearUpdate();
        this.classUpdate();
        this.branchUpdate();
        this.studentUpdate();
        this.permissionUpdate();
        this.roleUpdate();
        this.facultyUpdate();
        this.subjectUpdate();
        this.settingUpdate();
        this.partUpdate();
        this.answertypeUpdate();
        this.passageUpdate();
        this.questionUpdate();
        this.labUpdate();
        this.testshiftUpdate();
        this.testtypeUpdate();
        this.testUpdate();
        this.scheduletestUpdate();
        this.roomviewUpdate();
        this.informationUpdate();
      })
      .catch(err => {
        console.log("Error while starting connection: " + err);
        setTimeout(() =>
          this.startConnection()
          , 3000);
      });
  };

  public employeeUpdate() {
    this.hubConnection.on("GetEmployee", (data) => {
      this.employee.emit(data);
    });
  }

  public logUpdate() {
    this.hubConnection.on("GetLog", (data) => {
      this.log.emit(data);
    });
  }

  public noficationUpdate() {
    this.hubConnection.on("GetNofication", (data) => {
      this.nofication.emit(data);
    });
  }

  public schoolYearUpdate() {
    this.hubConnection.on("GetSchoolYear", (data) => {
      this.schoolyear.emit(data);
    });
  }

  public classUpdate() {
    this.hubConnection.on("GetClass", (data) => {
      this.class.emit(data);
    });
  }

  public branchUpdate() {
    this.hubConnection.on("GetBranch", (data) => {
      this.branch.emit(data);
    });
  }

  public studentUpdate() {
    this.hubConnection.on("GetStudent", (data) => {
      this.student.emit(data);
    });
  }

  public permissionUpdate() {
    this.hubConnection.on("GetPermission", (data) => {
      this.permission.emit(data);
    });
  }

  public roleUpdate() {
    this.hubConnection.on("GetRole", (data) => {
      this.role.emit(data);
    });
  }

  public facultyUpdate() {
    this.hubConnection.on("GetFaculty", (data) => {
      this.faculty.emit(data);
    });
  }

  public subjectUpdate() {
    this.hubConnection.on("GetSubject", (data) => {
      this.subject.emit(data);
    });
  }

  public settingUpdate() {
    this.hubConnection.on("GetSetting", (data) => {
      this.setting.emit(data);
    });
  }

  public partUpdate() {
    this.hubConnection.on("GetPart", (data) => {
      this.part.emit(data);
    });
  }

  public answertypeUpdate() {
    this.hubConnection.on("GetAnswerType", (data) => {
      this.answertype.emit(data);
    });
  }

  public passageUpdate() {
    this.hubConnection.on("GetPassage", (data) => {
      this.passage.emit(data);
    });
  }

  public questionUpdate() {
    this.hubConnection.on("GetQuestion", (data) => {
      this.question.emit(data);
    });
  }

  public labUpdate() {
    this.hubConnection.on("GetLab", (data) => {
      this.lab.emit(data);
    });
  }

  public testshiftUpdate() {
    this.hubConnection.on("GetTestShift", (data) => {
      this.testshift.emit(data);
    });
  }

  public testtypeUpdate() {
    this.hubConnection.on("GetTestType", (data) => {
      this.testtype.emit(data);
    });
  }

  public testUpdate() {
    this.hubConnection.on("GetTest", (data) => {
      this.test.emit(data);
    });
  }

  public scheduletestUpdate() {
    this.hubConnection.on("GetScheduleTest", (data) => {
      this.scheduletest.emit(data);
    });
  }

  public roomviewUpdate() {
    this.hubConnection.on("GetRoomView", (data) => {
      this.roomview.emit(data);
    });
  }

  public informationUpdate() {
    this.hubConnection.on("GetInformation", (data) => {
      this.information.emit(data);
    });
  }
}
