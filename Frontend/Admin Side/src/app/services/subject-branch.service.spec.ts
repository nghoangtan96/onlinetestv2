import { TestBed } from '@angular/core/testing';

import { SubjectBranchService } from './subject-branch.service';

describe('SubjectBranchService', () => {
  let service: SubjectBranchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubjectBranchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
