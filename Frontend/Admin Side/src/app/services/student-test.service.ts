import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { ScheduleTest } from './schedule-test.service';
import { Student } from './student.service';
import { ExamRequest } from '../models/examRequest';

export interface ListStudentTestResult {
  errorCode: number;
  message: string;
  data: [StudentTest];
}

export interface StudentTest {
  id: number;
  sttE_TESTCODE: string;
  sttE_SCTEID: number;
  sttE_STUID: number;
  sttE_QUESTION: string;
  sttE_OPTION: string;
  sttE_PASSWORD: string;
  sttE_REMAININGTIME: number;
  sttE_SCORE: number;
  sttE_AUTONUMBER: number;
  sttE_HASH: string;
  sttE_CREATEDDATE: Date;
  sttE_MODIFIEDDATE: Date;
  sttE_STATUS: number;
  sttE_CURRENTPART: number;
  sttE_CURRENTTIMEAUDIO: number;
  schedule_Tests: ScheduleTest;
  students: Student;
}

export interface StudentTestResult {
  errorCode: number;
  message: string;
  data: StudentTest;
}

@Injectable({
  providedIn: 'root'
})
export class StudentTestService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListStudentTestResult> {
    return this._http.get<ListStudentTestResult>(`${this.baseapi.url.studenttest}`);
  }
  getAllByUserId(id: number): Observable<ListStudentTestResult> {
    return this._http.get<ListStudentTestResult>(`${this.baseapi.url.studenttest}/getAllByUserId/${id}`);
  }
  getAllByUserIdForResult(id: string): Observable<ListStudentTestResult> {
    return this._http.get<ListStudentTestResult>(`${this.baseapi.url.studenttest}/getAllByUserIdForResult/${id}`);
  }
  get(id: number): Observable<StudentTestResult> {
    return this._http.get<StudentTestResult>(`${this.baseapi.url.studenttest}/${id}`);
  }
  getByScheduleTestIdAndStudentId(scteid: number, useid: number): Observable<StudentTestResult> {
    return this._http.get<StudentTestResult>(`${this.baseapi.url.studenttest}/getByScheduleTestIdAndStudentId/${scteid}&${useid}`);
  }
  getAllByScheduleTestId(id: number): Observable<ListStudentTestResult> {
    return this._http.get<ListStudentTestResult>(`${this.baseapi.url.studenttest}/getAllByScheduleTestId/${id}`);
  }
  testSubmit(examRequest: ExamRequest): Observable<StudentTestResult> {
    return this._http.put<StudentTestResult>(`${this.baseapi.url.studenttest}/testSubmit/${examRequest.scheduleTestId}&${examRequest.userId}`, examRequest);
  }
  doingExam(examRequest: ExamRequest): Observable<StudentTestResult> {
    return this._http.put<StudentTestResult>(`${this.baseapi.url.studenttest}/doingExam/${examRequest.scheduleTestId}&${examRequest.userId}`, examRequest);
  }
  updateExam(examRequest: ExamRequest): Observable<StudentTestResult> {
    return this._http.put<StudentTestResult>(`${this.baseapi.url.studenttest}/updateExam/${examRequest.scheduleTestId}&${examRequest.userId}`, examRequest);
  }
}
