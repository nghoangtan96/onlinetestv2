import { Injectable } from '@angular/core';
import { Action } from './action.service';
import { Table } from './table.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { RoleType } from './role-type.service';

export interface ListRoleResult {
  errorCode: number;
  Message: string;
  data: [Role];
}
export interface Role {
  id: number;
  roL_ACTID: number;
  roL_TABID: number;
  roL_URL: string;
  roL_TRIGGER: string;
  roL_ISACTIVE: boolean;
  roL_STATUS: number;
  roL_ISPARENT: boolean;
  roL_PARENT: number;
  roles: [Role];
  role_types: RoleType;
  actionns: Action;
  tables: Table;
}
export interface RoleResult {
  errorCode: number;
  Message: string;
  data: Role;
}

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListRoleResult> {
    return this._http.get<ListRoleResult>(this.baseapi.url.role);
  }
  get(id): Observable<RoleResult> {
    return this._http.get<RoleResult>(`${this.baseapi.url.role}/${id}`);
  }
}
