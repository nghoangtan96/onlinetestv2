import { Injectable } from '@angular/core';
import { BaseapiService } from './baseapi.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { changePasswordRequest } from '../models/changePassword_request';

export interface LoginResult {
  errorCode: Number;
  message: string;
  data: {
    id: number;
    username: string;
    firstname: string;
    lastname: string;
    token: string;
    isLogin: number;
    role: number;
  };
}
export interface LoginRequest {
  username: string;
  password: string;
}
export interface ResetPasswordRequest {
  email: string;
}
export interface ChangePasswordResult{
  errorCode: number;
  message: string;
}
export interface ResetPasswordResult{
  errorCode: number;
  message: string;
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private api: BaseapiService, private _http: HttpClient, private cookieSer: CookieService) { 
  }
  login(login: LoginRequest): Observable<LoginResult> {
    return this._http.post<LoginResult>(this.api.url.login, login);
  }
  changePassword(req: changePasswordRequest): Observable<ChangePasswordResult> {
    return this._http.post<ChangePasswordResult>(this.api.url.changepassword, req);
  }
  resetPassword(resetPasswordRequest: ResetPasswordRequest): Observable<ResetPasswordResult> {
    return this._http.post<ResetPasswordResult>(this.api.url.resetpassword, resetPasswordRequest);
  }
}