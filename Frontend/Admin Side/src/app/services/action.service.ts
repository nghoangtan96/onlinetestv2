import { Injectable } from '@angular/core';

export interface Action {
  id: number;
  acT_NAME: string;
  acT_STATUS: number;
  acT_CODE: string;
}

@Injectable({
  providedIn: 'root'
})
export class ActionService {

  constructor() { }
}
