import { TestBed } from '@angular/core/testing';

import { SettingtypeService } from './settingtype.service';

describe('SettingtypeService', () => {
  let service: SettingtypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SettingtypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
