import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { User } from './user.service';
import { StudentRequest } from '../models/student_request';
import { Branch } from './branch.service';
import { Class } from './class.service';
import { SchoolYear } from './schoolyear.service';

export interface ListStudentResult {
  errorCode: number;
  message: string;
  data: [Student];
}
export interface Student {
  id: number;
  stU_CODE: string;
  stU_IDENTITY: string;
  stU_FIRSTNAME: string;
  stU_LASTNAME: string;
  stU_GENDER: boolean;
  stU_DATEOFBIRTH: any;
  stU_PHONE: string;
  stU_ADDRESS: string;
  stU_EMAIL: string;
  stU_AVATAR: string;
  stU_CREATEDBY: number;
  stU_CREATEDDATE: Date;
  stU_MODIFIEDBY: number;
  stU_MODIFIEDDATE: Date;
  stU_USEID: number;
  stU_STATUS: number;
  stU_ISDELETED: boolean;
  stU_BRAID: number;
  stU_CLAID: number;
  stU_SCYEID: number;
  origin: string;
  fileUrl: string;
  users: User;
  userCreated: User;
  userModified: User;
  branches: Branch;
  classes: Class;
  schoolYears: SchoolYear;
}
export interface StudentResult {
  errorCode: number;
  message: string;
  data: Student;
}
export interface StudentDataExportResult {
  errorCode: number;
  message: string;
  data: string;
}

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListStudentResult> {
    return this._http.get<ListStudentResult>(this.baseapi.url.student);
  }
  getListByClassId(id: number): Observable<ListStudentResult> {
    return this._http.get<ListStudentResult>(`${this.baseapi.url.student}/getListByClassId/${id}`);
  }
  getListByScheduleTestId(id: number): Observable<ListStudentResult> {
    return this._http.get<ListStudentResult>(`${this.baseapi.url.student}/getListByScheduleTestId/${id}`);
  }
  getListBySchoolYearId(id: number): Observable<ListStudentResult> {
    return this._http.get<ListStudentResult>(`${this.baseapi.url.student}/getListBySchoolYearId/${id}`);
  }
  getListBySubjectId(id: number): Observable<ListStudentResult> {
    return this._http.get<ListStudentResult>(`${this.baseapi.url.student}/getListBySubjectId/${id}`);
  }
  exportExcelLinkDownload(listStudentRequest: Student[]): Observable<StudentDataExportResult> {
    return this._http.post<StudentDataExportResult>(`${this.baseapi.url.student}/exportExcelLinkDownload`, listStudentRequest);
  }
  importExcel(user: number, autogencode: boolean, autogenaccount: boolean, file: FormData): Observable<ListStudentResult> {
    return this._http.post<ListStudentResult>(`${this.baseapi.url.student}/ImportExcel/${user}&${autogencode}&${autogenaccount}`, file);
  }
  getSampleFileLink(): Observable<StudentDataExportResult> {
    return this._http.get<StudentDataExportResult>(`${this.baseapi.url.student}/getSampleFileLink`);
  }
  get(id): Observable<StudentResult> {
    return this._http.get<StudentResult>(`${this.baseapi.url.student}/${id}`);
  }
  getListStudentMultiDel(arrDel: number[]): Observable<ListStudentResult> {
    return this._http.post<ListStudentResult>(`${this.baseapi.url.student}/getListStudentMultiDel`, arrDel);
  }
  add(studentRequest: StudentRequest): Observable<StudentResult> {
    return this._http.post<StudentResult>(`${this.baseapi.url.student}`, studentRequest);
  }
  put(studentRequest: StudentRequest): Observable<StudentResult> {
    return this._http.put<StudentResult>(`${this.baseapi.url.student}/${studentRequest.student.id}`, studentRequest);
  }
  delete(id, userid): Observable<StudentResult> {
    return this._http.delete<StudentResult>(`${this.baseapi.url.student}/${id}&${userid}`);
  }
  deleteMulti(arrDel: Student[], userid): Observable<StudentResult> {
    return this._http.post<StudentResult>(`${this.baseapi.url.student}/multipleDelete/${userid}`, arrDel);
  }
}
