import { Injectable } from '@angular/core';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { User } from './user.service';

export interface ListSchoolYearResult {
  errorCode: number;
  message: string;
  data: [SchoolYear];
}
export interface SchoolYear {
  id: number;
  scyE_CODE: string;
  scyE_STARTYEAR: number;
  scyE_CREATEDBY: number;
  scyE_CREATEDDATE: Date;
  scyE_MODIFIEDBY: number;
  scyE_MODIFIEDDATE: Date;
  scyE_STATUS: number;
  scyE_ISDELETED: boolean;
  userCreated: User;
  userModified: User;
}
export interface SchoolYearResult {
  errorCode: number;
  message: string;
  data: SchoolYear;
}

@Injectable({
  providedIn: 'root'
})
export class SchoolyearService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListSchoolYearResult> {
    return this._http.get<ListSchoolYearResult>(this.baseapi.url.schoolyear);
  }
  get(id): Observable<SchoolYearResult> {
    return this._http.get<SchoolYearResult>(`${this.baseapi.url.schoolyear}/${id}`);
  }
  getSchoolYearByCurrentYear(year): Observable<SchoolYearResult> {
    return this._http.get<SchoolYearResult>(`${this.baseapi.url.schoolyear}/getSchoolYearByCurrentYear/${year}`);
  }
  add(schoolYear: SchoolYear): Observable<SchoolYearResult> {
    return this._http.post<SchoolYearResult>(`${this.baseapi.url.schoolyear}`, schoolYear);
  }
  put(schoolYear: SchoolYear): Observable<SchoolYearResult> {
    return this._http.put<SchoolYearResult>(`${this.baseapi.url.schoolyear}/${schoolYear.id}`, schoolYear);
  }
  delete(id: number, userid): Observable<SchoolYearResult> {
    return this._http.delete<SchoolYearResult>(`${this.baseapi.url.schoolyear}/${id}&${userid}`);
  }
}
