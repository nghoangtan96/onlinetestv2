import { Injectable } from '@angular/core';
import { Question } from './question.service';
import { Passage } from './passage.service';
import { User } from './user.service';
import { Test } from './test.service';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';

export interface ListTestDetailResult {
  errorCode: number;
  message: string;
  data: [TestDetail];
}
export interface TestDetail {
  id: number;
  tedE_CODE: string;
  tedE_QUEID: number;
  tedE_PASID: number;
  tedE_TESID: number;
  tedE_QUEOPTION: string;
  tedE_PASQUESTION: string;
  tedE_PASOPTION: string;
  tedE_CREATEDBY: number;
  tedE_CREATEDDATE: Date;
  tedE_ISDELETED: boolean;
  questions: Question;
  passages: Passage;
  tests: Test;
  userCreated: User;
}
export interface TestDetailResult {
  errorCode: number;
  message: string;
  data: TestDetail;
}

@Injectable({
  providedIn: 'root'
})
export class TestDetailService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListTestDetailResult> {
    return this._http.get<ListTestDetailResult>(`${this.baseapi.url.testdetail}`);
  }
  get(id: number): Observable<TestDetailResult> {
    return this._http.get<TestDetailResult>(`${this.baseapi.url.testdetail}/${id}`);
  }
  getListByTestId(id: number, code: string): Observable<ListTestDetailResult> {
    return this._http.get<ListTestDetailResult>(`${this.baseapi.url.testdetail}/getListByTestId/${id}&${code}`);
  }
}
