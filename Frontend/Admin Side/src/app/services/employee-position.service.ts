import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { User } from './user.service';
import { Position } from './position.service';

export interface ListEmployeePositionResult {
  errorCode: number;
  Message: string;
  data: [EmployeePosition];
}
export interface EmployeePosition {
  id: number;
  empO_EMPID: number;
  empO_POSID: number;
  empO_STATUS: number;
  empO_ISDELETED: boolean;
  empO_CREATEBY: number;
  empO_CREATEDATE: Date;
  empO_MODIFIEDBY: number;
  empO_MODIFIEDDATE: Date;
  user_Create: User;
  user_Modified: User;
  positions: Position;
}
export interface EmployeePositionResult {
  errorCode: number;
  Message: string;
  data: EmployeePosition;
}

@Injectable({
  providedIn: 'root'
})
export class EmployeePositionService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListEmployeePositionResult> {
    return this._http.get<ListEmployeePositionResult>(this.baseapi.url.employee_position);
  }
  get(id): Observable<EmployeePositionResult> {
    return this._http.get<EmployeePositionResult>(`${this.baseapi.url.employee_position}/${id}`);
  }
  getlistPosbyEmpid(id): Observable<ListEmployeePositionResult> {
    return this._http.get<ListEmployeePositionResult>(`${this.baseapi.url.employee_position}/getlistposbyempid/${id}`);
  }
  add(employee_position: EmployeePosition): Observable<EmployeePositionResult>{
    return this._http.post<EmployeePositionResult>(`${this.baseapi.url.employee_position}`, employee_position);
  }
  put(employee_position: EmployeePosition): Observable<EmployeePositionResult>{
    return this._http.put<EmployeePositionResult>(`${this.baseapi.url.employee_position}/${employee_position.id}`, employee_position);
  }
  delete(id, userid): Observable<EmployeePositionResult>{ 
    return this._http.delete<EmployeePositionResult>(`${this.baseapi.url.employee_position}/${id}&${userid}`);
  }
}
