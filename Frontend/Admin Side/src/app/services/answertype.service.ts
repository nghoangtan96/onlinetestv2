import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { User } from './user.service';

export interface ListAnswerTypeResult {
  errorCode: number;
  message: string;
  data: [AnswerType];
}
export interface AnswerType {
  id: number;
  antY_NAME: string;
  antY_ORDER: number;
  antY_SAMPLE: string;
  antY_ISDELETED: boolean;
  antY_STATUS: number;
  antY_CREATEDBY: number;
  antY_CREATEDDATE: Date;
  antY_MODIFIEDBY: number;
  antY_MODIFIEDDATE: Date;
  userCreated: User;
  userModified: User;
}
export interface AnswerTypeResult {
  errorCode: number;
  message: string;
  data: AnswerType;
}

@Injectable({
  providedIn: 'root'
})
export class AnswertypeService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListAnswerTypeResult> {
    return this._http.get<ListAnswerTypeResult>(`${this.baseapi.url.answertype}`);
  }
  get(id: number): Observable<AnswerTypeResult> {
    return this._http.get<AnswerTypeResult>(`${this.baseapi.url.answertype}/${id}`);
  }
  put(answertype : AnswerType):Observable<AnswerTypeResult> {
    return this._http.put<AnswerTypeResult>(`${this.baseapi.url.answertype}/${answertype.id}`, answertype);
  }
}
