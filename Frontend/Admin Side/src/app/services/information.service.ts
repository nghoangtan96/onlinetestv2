import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { User } from './user.service';

export interface ListInformationResult {
  errorCode: number;
  message: string;
  data: [Information];
}

export interface Information {
  id: number;
  inF_KEYNAME: string;
  inF_VALUE: string;
  inF_ORDER: number;
  inF_STATUS: number;
  inF_ISMULTI: boolean;
  inF_INFID: number;
  inF_ISDELETED: boolean;
  inF_CREATEDBY: number;
  inF_CREATEDDATE: Date;
  inF_MODIFIEDBY: number;
  inF_MODIFIEDDATE: Date;
  fileUrl: string;
  informations: [Information];
  userCreated: User;
  userModified: User;
}
export interface InformationResult {
  errorCode: number;
  message: string;
  data: Information;
}

@Injectable({
  providedIn: 'root'
})
export class InformationService {
  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListInformationResult> {
    return this._http.get<ListInformationResult>(this.baseapi.url.information);
  }
  get(id): Observable<InformationResult> {
    return this._http.get<InformationResult>(`${this.baseapi.url.information}/${id}`);
  }
  add(information: Information): Observable<InformationResult>{
    return this._http.post<InformationResult>(`${this.baseapi.url.information}`, information);
  }
  put(userid: number, listInformation: [Information]): Observable<ListInformationResult>{
    return this._http.put<ListInformationResult>(`${this.baseapi.url.information}/${userid}`, listInformation);
  }
  delete(id, userid): Observable<InformationResult>{ 
    return this._http.delete<InformationResult>(`${this.baseapi.url.information}/${id}&${userid}`);
  }
}
