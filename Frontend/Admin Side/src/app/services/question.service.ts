import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { AnswerType } from './answertype.service';
import { Passage } from './passage.service';
import { Part } from './part.service';
import { User } from './user.service';
import { Option } from './option.service';
import { questionRequest } from '../models/question_request';

export interface ListQuestionResult {
  errorCode: number;
  message: string;
  data: [Question];
}
export interface Question {
  id: number;
  quE_CONTENT: string;
  quE_ISSHUFFLE: boolean;
  quE_SCORE: number;
  quE_OPTCOLUMN: number;
  quE_ISBANK: boolean;
  quE_LEVEL: number;
  quE_MEDIA: string;
  quE_REFERENCE: string;
  quE_ORDER: number;
  quE_ISDELETED: boolean;
  quE_STATUS: number;
  quE_CREATEDBY: number;
  quE_CREATEDDATE: Date;
  quE_MODIFIEDBY: number;
  quE_MODIFIEDDATE: Date;
  quE_ANTYID: number;
  quE_PASID: number;
  quE_PARID: number;
  fileUrl: string;
  fileType: string;
  countTest: number;
  answerTypes: AnswerType;
  passages: Passage;
  parts: Part;
  options: [Option];
  userCreated: User;
  userModified: User;
  index: number;
  isDone: boolean;
  mediaType: string;
}
export interface QuestionResult {
  errorCode: number;
  message: string;
  data: Question;
}

export interface QuestionDataExportResult {
  errorCode: number;
  message: string;
  data: string;
}

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }

  getAll(): Observable<ListQuestionResult> {
    return this._http.get<ListQuestionResult>(`${this.baseapi.url.question}`);
  }
  get(id: number): Observable<QuestionResult> {
    return this._http.get<QuestionResult>(`${this.baseapi.url.question}/${id}`);
  }
  getListByPartId(id: number): Observable<ListQuestionResult> {
    return this._http.get<ListQuestionResult>(`${this.baseapi.url.question}/getListByPartId/${id}`);
  }
  getSampleFileLink(): Observable<QuestionDataExportResult> {
    return this._http.get<QuestionDataExportResult>(`${this.baseapi.url.question}/getSampleFileLink`);
  }
  getListByPassageId(id: number): Observable<ListQuestionResult> {
    return this._http.get<ListQuestionResult>(`${this.baseapi.url.question}/getListByPassageId/${id}`);
  }
  getListBySubjectId(id: number): Observable<ListQuestionResult> {
    return this._http.get<ListQuestionResult>(`${this.baseapi.url.question}/getListBySubjectId/${id}`);
  }
  getListBySubjectIdForExam(id: number): Observable<ListQuestionResult> {
    return this._http.get<ListQuestionResult>(`${this.baseapi.url.question}/getListBySubjectIdForExam/${id}`);
  }
  getListQuestionMultiDel(arrDel: number[]): Observable<ListQuestionResult> {
    return this._http.post<ListQuestionResult>(`${this.baseapi.url.question}/getListQuestionMultiDel`, arrDel);
  }
  importExcel(user: number, passageId: number, partId: number, file: FormData): Observable<ListQuestionResult> {
    return this._http.post<ListQuestionResult>(`${this.baseapi.url.question}/ImportExcel/${user}&${passageId}&${partId}`, file);
  }
  add(questionRequest: questionRequest): Observable<QuestionResult> {
    return this._http.post<QuestionResult>(`${this.baseapi.url.question}`, questionRequest);
  }
  put(questionRequest: questionRequest): Observable<QuestionResult> {
    return this._http.put<QuestionResult>(`${this.baseapi.url.question}/${questionRequest.question.id}`, questionRequest);
  }
  delete(id, userid): Observable<QuestionResult> {
    return this._http.delete<QuestionResult>(`${this.baseapi.url.question}/${id}&${userid}`);
  }
  deleteMulti(arrDel: Question[], userid): Observable<QuestionResult> {
    return this._http.post<QuestionResult>(`${this.baseapi.url.question}/multipleDelete/${userid}`, arrDel);
  }
}
