import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseapiService {

  constructor() { }
  baseUrl = 'http://localhost:50840/api/';
  url = {
    login: `${this.baseUrl}user/login`,
    employee: `${this.baseUrl}employee`,
    student: `${this.baseUrl}student`,
    user: `${this.baseUrl}user`,
    log: `${this.baseUrl}log`,
    permission: `${this.baseUrl}permission`,
    nofication: `${this.baseUrl}nofication`,
    changepassword: `${this.baseUrl}user/changepassword`,
    resetpassword: `${this.baseUrl}user/resetpassword`,
    settingtype: `${this.baseUrl}settingtype`,
    setting: `${this.baseUrl}setting`,
    branch: `${this.baseUrl}branch`,
    subject: `${this.baseUrl}subject`,
    subjectbranch: `${this.baseUrl}subjectbranch`,
    part: `${this.baseUrl}part`,
    question: `${this.baseUrl}question`,
    option: `${this.baseUrl}option`,
    answertype: `${this.baseUrl}answertype`,
    passage: `${this.baseUrl}passage`,
    faculty: `${this.baseUrl}faculty`,
    role: `${this.baseUrl}role`,
    roletype: `${this.baseUrl}roletype`,
    information: `${this.baseUrl}information`,
    user_permisson: `${this.baseUrl}userpermission`,
    employee_subject: `${this.baseUrl}employeesubject`,
    schoolyear: `${this.baseUrl}schoolyear`,
    class: `${this.baseUrl}class`,
    lab: `${this.baseUrl}lab`,
    testshift: `${this.baseUrl}testshift`,
    testtype: `${this.baseUrl}testtype`,
    test: `${this.baseUrl}test`,
    test_question: `${this.baseUrl}testquestion`,
    testdetail: `${this.baseUrl}testdetail`,
    scheduletest: `${this.baseUrl}scheduletest`,
    studenttest: `${this.baseUrl}studenttest`,

    logout: `${this.baseUrl}user/logout`,
    file: `${this.baseUrl}file`,
    position: `${this.baseUrl}position`,
    employee_position: `${this.baseUrl}employee_position`,

  }
}
