import { TestBed } from '@angular/core/testing';

import { TestDetailService } from './test-detail.service';

describe('TestDetailService', () => {
  let service: TestDetailService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestDetailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
