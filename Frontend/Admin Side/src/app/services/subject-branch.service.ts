import { Injectable } from '@angular/core';
import { Branch } from './branch.service';
import { Subjectt, ListSubjectResult } from './subject.service';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';

export interface ListSubjectBranchResult {
  errorCode: number;
  message: string;
  data: [SubjectBranch];
}

export interface SubjectBranch {
  id: number;
  subR_SUBID: number;
  subR_BRAID: number;
  subR_STATUS: number;
  subR_CREATEDBY: number;
  subR_CREATEDDATE: Date;
  branches: Branch;
  subjects: Subjectt;
}

export interface SubjectBranchResult {
  errorCode: number;
  message: string;
  data: SubjectBranch;
}

@Injectable({
  providedIn: 'root'
})
export class SubjectBranchService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }

  getListBySubjectId(id: number): Observable<ListSubjectBranchResult> {
    return this._http.get<ListSubjectBranchResult>(`${this.baseapi.url.subjectbranch}/getListBySubjectId/${id}`);
  }
  getListByBranchId(id: number): Observable<ListSubjectResult> {
    return this._http.get<ListSubjectResult>(`${this.baseapi.url.subjectbranch}/getListByBranchId/${id}`);
  }
}
