import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { User } from './user.service';

export interface ListPositionResult {
  errorCode: number;
  Message: string;
  data: [Position];
}
export interface Position {
  id: number;
  poS_NAME: string;
  poS_SALARYRATE: number;
  poS_STATUS: number;
  poS_ISALLOWUSER: boolean;
  poS_ISDELETED: boolean;
  poS_CREATEBY: number;
  poS_CREATEDATE: Date;
  poS_MODIFIEDBY: number;
  poS_MODIFIEDDATE: Date;
  user_Create: User;
  user_Modified: User;
}
export interface PositionResult {
  errorCode: number;
  Message: string;
  data: Position;
}

@Injectable({
  providedIn: 'root'
})
export class PositionService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListPositionResult> {
    return this._http.get<ListPositionResult>(this.baseapi.url.position);
  }
  getAllDataExport(id_ex): Observable<ListPositionResult> {
    return this._http.get<ListPositionResult>(`${this.baseapi.url.position}/dataexport/${id_ex}`);
  }
  get(id): Observable<PositionResult> {
    return this._http.get<PositionResult>(`${this.baseapi.url.position}/${id}`);
  }
  add(position: Position): Observable<PositionResult>{
    return this._http.post<PositionResult>(`${this.baseapi.url.position}`, position);
  }
  put(position: Position): Observable<PositionResult>{
    return this._http.put<PositionResult>(`${this.baseapi.url.position}/${position.id}`, position);
  }
  delete(id, userid): Observable<PositionResult>{ 
    return this._http.delete<PositionResult>(`${this.baseapi.url.position}/${id}&${userid}`);
  }
}
