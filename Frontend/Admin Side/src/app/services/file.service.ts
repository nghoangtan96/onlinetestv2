import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';

export interface FileResult {
  errorCode: number;
  Message: string;
  data: string;
}

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }

  add(directory: string, file: FormData): Observable<FileResult> {
    return this._http.post<FileResult>(`${this.baseapi.url.file}/${directory}`, file);
  }
  put(oldfile: string, directory: string, file: FormData): Observable<FileResult> {
    return this._http.put<FileResult>(`${this.baseapi.url.file}/${oldfile}&${directory}`, file);
  }
  delete(directory: string, filename: string): Observable<FileResult> {
    return this._http.delete<FileResult>(`${this.baseapi.url.file}/${directory}&${filename}`);
  }
}
