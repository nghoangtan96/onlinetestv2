import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidateService {

  carriersNumber: { oldNumber: string, newNumber: string, mobileNetOper: string }[] = [
    { 'oldNumber': '086', 'newNumber': '086', 'mobileNetOper': 'Viettel' },
    { 'oldNumber': '096', 'newNumber': '096', 'mobileNetOper': 'Viettel' },
    { 'oldNumber': '097', 'newNumber': '097', 'mobileNetOper': 'Viettel' },
    { 'oldNumber': '098', 'newNumber': '098', 'mobileNetOper': 'Viettel' },
    { 'oldNumber': '0162', 'newNumber': '032', 'mobileNetOper': 'Viettel' },
    { 'oldNumber': '0163', 'newNumber': '033', 'mobileNetOper': 'Viettel' },
    { 'oldNumber': '0164', 'newNumber': '034', 'mobileNetOper': 'Viettel' },
    { 'oldNumber': '0165', 'newNumber': '035', 'mobileNetOper': 'Viettel' },
    { 'oldNumber': '0166', 'newNumber': '036', 'mobileNetOper': 'Viettel' },
    { 'oldNumber': '0167', 'newNumber': '037', 'mobileNetOper': 'Viettel' },
    { 'oldNumber': '0168', 'newNumber': '038', 'mobileNetOper': 'Viettel' },
    { 'oldNumber': '0169', 'newNumber': '039', 'mobileNetOper': 'Viettel' },

    { 'oldNumber': '089', 'newNumber': '089', 'mobileNetOper': 'MobiFone' },
    { 'oldNumber': '090', 'newNumber': '090', 'mobileNetOper': 'MobiFone' },
    { 'oldNumber': '093', 'newNumber': '093', 'mobileNetOper': 'MobiFone' },
    { 'oldNumber': '0120', 'newNumber': '070', 'mobileNetOper': 'MobiFone' },
    { 'oldNumber': '0121', 'newNumber': '079', 'mobileNetOper': 'MobiFone' },
    { 'oldNumber': '0122', 'newNumber': '077', 'mobileNetOper': 'MobiFone' },
    { 'oldNumber': '0126', 'newNumber': '076', 'mobileNetOper': 'MobiFone' },
    { 'oldNumber': '0128', 'newNumber': '078', 'mobileNetOper': 'MobiFone' },

    { 'oldNumber': '088', 'newNumber': '088', 'mobileNetOper': 'VinaPhone' },
    { 'oldNumber': '091', 'newNumber': '091', 'mobileNetOper': 'VinaPhone' },
    { 'oldNumber': '094', 'newNumber': '094', 'mobileNetOper': 'VinaPhone' },
    { 'oldNumber': '0123', 'newNumber': '083', 'mobileNetOper': 'VinaPhone' },
    { 'oldNumber': '0124', 'newNumber': '084', 'mobileNetOper': 'VinaPhone' },
    { 'oldNumber': '0125', 'newNumber': '085', 'mobileNetOper': 'VinaPhone' },
    { 'oldNumber': '0127', 'newNumber': '081', 'mobileNetOper': 'VinaPhone' },
    { 'oldNumber': '0129', 'newNumber': '082', 'mobileNetOper': 'VinaPhone' },

    { 'oldNumber': '092', 'newNumber': '092', 'mobileNetOper': 'Vietnamobile' },
    { 'oldNumber': '056', 'newNumber': '056', 'mobileNetOper': 'Vietnamobile' },
    { 'oldNumber': '058', 'newNumber': '058', 'mobileNetOper': 'Vietnamobile' },

    { 'oldNumber': '099', 'newNumber': '099', 'mobileNetOper': 'Gmobile' },
    { 'oldNumber': '0199', 'newNumber': '059', 'mobileNetOper': 'Gmobile' },
  ];

  constructor() { }

  customizeFaxNumber(faxnumber: string) {
    faxnumber = faxnumber.replace('.', '');
    return faxnumber;
  }

  customizePhoneNumber(phonenumber: string) {
    phonenumber = phonenumber.trim();
    phonenumber = phonenumber.replace('(+84)', '0');
    phonenumber = phonenumber.replace('+84', '0');
    phonenumber = phonenumber.replace('0084', '0');
    phonenumber = phonenumber.replace(/ /g, '');
    phonenumber = phonenumber.replace('-', '');
    phonenumber = phonenumber.replace('.', '');
    return phonenumber;
  }
  validate_phonenumber(phonenumber: string) {
    phonenumber = phonenumber.trim();
    phonenumber = phonenumber.replace('(+84)', '0');
    phonenumber = phonenumber.replace('+84', '0');
    phonenumber = phonenumber.replace('0084', '0');
    phonenumber = phonenumber.replace(/ /g, '');
    phonenumber = phonenumber.replace('-', '');
    phonenumber = phonenumber.replace('.', '');

    if (phonenumber == '')
      return '1';
    else {
      //check have character
      let arr = phonenumber.split('');
      let isHaveChar = false;
      for (let i = 0; i < arr.length; i++) {
        if ($.isNumeric(arr[i]) == false) {
          isHaveChar = true;
        }
      }

      if (isHaveChar == true)
        return '2';
      else {
        for (let i = 0; i < this.carriersNumber.length; i++) {
          if (this.carriersNumber[i].oldNumber.length == 3) {
            if (phonenumber.substring(0, this.carriersNumber[i].oldNumber.length) == this.carriersNumber[i].oldNumber && phonenumber.match(/^\d{10}/)) {
              return phonenumber.replace(this.carriersNumber[i].oldNumber, this.carriersNumber[i].newNumber) + " - " + this.carriersNumber[i].mobileNetOper;
            }
          }
          else {
            if (phonenumber.substring(0, this.carriersNumber[i].oldNumber.length) == this.carriersNumber[i].oldNumber && phonenumber.match(/^\d{11}/)) {
              return phonenumber.replace(this.carriersNumber[i].oldNumber, this.carriersNumber[i].newNumber) + " - " + this.carriersNumber[i].mobileNetOper;
            }
          }
        }
        for (let i = 0; i < this.carriersNumber.length; i++) {
          if (phonenumber.substring(0, this.carriersNumber[i].newNumber.length) == this.carriersNumber[i].newNumber && phonenumber.match(/^\d{10}/)) {
            return phonenumber + " - " + this.carriersNumber[i].mobileNetOper;
          }
        }
        return '2';
      }
    }
  }
}
