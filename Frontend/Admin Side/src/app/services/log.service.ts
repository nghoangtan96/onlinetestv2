import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { User } from './user.service';
import { Employee } from './employee.service';
import { Student } from './student.service';

export interface ListLogResult {
  errorCode: number;
  message: string;
  data: [Log];
}
export interface Log {
  id: number;
  loG_MODULE: number;
  loG_USEID: number;
  loG_NOTE: string;
  loG_DATE: Date;
  loG_ACTID: number;
  loG_CONTENT: string;
  users: User;
}
export interface LogResult {
  errorCode: number;
  message: string;
  data: Log;
}

export interface UpdateLog {
  oldValue: string;
  newValue: string;
}

export interface InsertMultiLog{
  arrData: string[];
}

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }

  getAll(): Observable<ListLogResult> {
    return this._http.get<ListLogResult>(`${this.baseapi.url.log}`);
  }
  get(id: number): Observable<LogResult> {
    return this._http.get<LogResult>(`${this.baseapi.url.log}/${id}`);
  }
  // get1(date): Observable<ListLogResult> {
  //   return this._http.get<ListLogResult>(`${this.baseapi.url.log}/${date}`);
  // }
  // get2(datefrom, dateto): Observable<ListLogResult> {
  //   return this._http.get<ListLogResult>(`${this.baseapi.url.log}/${datefrom}&${dateto}`);
  // }
  // getExport1(date, id): Observable<ListLogResult> {
  //   return this._http.get<ListLogResult>(`${this.baseapi.url.log}/dataexport/${date}&${id}`);
  // }
  // getExport2(datefrom, dateto, id): Observable<ListLogResult> {
  //   return this._http.get<ListLogResult>(`${this.baseapi.url.log}/dataexport/${datefrom}&${dateto}&${id}`);
  // }
}
