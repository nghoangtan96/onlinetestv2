import { Injectable } from '@angular/core';
import { User } from './user.service';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';

export interface ListTestShiftResult {
  errorCode: number;
  message: string;
  data: [TestShift];
}
export interface TestShift {
  id: number;
  shI_NAME: string;
  shI_TIMESTART: Date;
  shI_ISDELETED: boolean;
  shI_STATUS: number;
  shI_CREATEDBY: number;
  shI_CREATEDDATE: Date;
  shI_MODIFIEDBY: number;
  shI_MODIFIEDDATE: Date;
  userCreated: User;
  userModified: User;
  timeEnd: string;
}
export interface TestShiftResult {
  errorCode: number;
  message: string;
  data: TestShift;
}

@Injectable({
  providedIn: 'root'
})
export class TestshiftService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListTestShiftResult> {
    return this._http.get<ListTestShiftResult>(`${this.baseapi.url.testshift}`);
  }
  get(id: number): Observable<TestShiftResult> {
    return this._http.get<TestShiftResult>(`${this.baseapi.url.testshift}/${id}`);
  }
  getListTestShiftMultiDel(arrDel: number[]): Observable<ListTestShiftResult> {
    return this._http.post<ListTestShiftResult>(`${this.baseapi.url.testshift}/getListTestShiftMultiDel`, arrDel);
  }
  add(testshift: TestShift): Observable<TestShiftResult> {
    return this._http.post<TestShiftResult>(`${this.baseapi.url.testshift}`, testshift);
  }
  put(testshift: TestShift): Observable<TestShiftResult> {
    return this._http.put<TestShiftResult>(`${this.baseapi.url.testshift}/${testshift.id}`, testshift);
  }
  delete(id, userid): Observable<TestShiftResult> {
    return this._http.delete<TestShiftResult>(`${this.baseapi.url.testshift}/${id}&${userid}`);
  }
  deleteMulti(arrDel: TestShift[], userid): Observable<TestShiftResult> {
    return this._http.post<TestShiftResult>(`${this.baseapi.url.testshift}/multipleDelete/${userid}`, arrDel);
  }
}
