import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { SubjectBranch } from './subject-branch.service';
import { Part } from './part.service';
import { User } from './user.service';
import { Faculty } from './faculty.service';

export interface ListSubjectResult {
  errorCode: number;
  message: string;
  data: [Subjectt];
}
export interface Subjectt {
  id: number;
  suB_CODE: string;
  suB_NAME: string;
  suB_CREDIT: number;
  suB_STATUS: number;
  suB_ISDELETED: boolean;
  suB_ISALL: boolean;
  suB_CREATEDBY: number;
  suB_CREATEDDATE: Date;
  suB_MODIFIEDBY: number;
  suB_MODIFIEDDATE: Date;
  suB_FACID: number;
  subject_Branches: [SubjectBranch];
  userCreated: User;
  userModified: User;
  listBranch: number[];
  parts: [Part];
  faculties: Faculty;
}
export interface SubjectResult {
  errorCode: number;
  message: string;
  data: Subjectt;
}

@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListSubjectResult> {
    return this._http.get<ListSubjectResult>(`${this.baseapi.url.subject}`);
  }
  get(id: number): Observable<SubjectResult> {
    return this._http.get<SubjectResult>(`${this.baseapi.url.subject}/${id}`);
  }
  getListSubjectMultiDel(arrDel: number[]): Observable<ListSubjectResult> {
    return this._http.post<ListSubjectResult>(`${this.baseapi.url.subject}/getListSubjectMultiDel`, arrDel);
  }
  getListSubjectByFacultyId(id: number): Observable<ListSubjectResult> {
    return this._http.get<ListSubjectResult>(`${this.baseapi.url.subject}/getListSubjectByFacultyId/${id}`);
  }
  add(subject: Subjectt): Observable<SubjectResult> {
    return this._http.post<SubjectResult>(`${this.baseapi.url.subject}`, subject);
  }
  put(subject: Subjectt): Observable<SubjectResult> {
    return this._http.put<SubjectResult>(`${this.baseapi.url.subject}/${subject.id}`, subject);
  }
  delete(id, userid): Observable<SubjectResult> {
    return this._http.delete<SubjectResult>(`${this.baseapi.url.subject}/${id}&${userid}`);
  }
  deleteMulti(arrDel: Subjectt[], userid): Observable<SubjectResult> {
    return this._http.post<SubjectResult>(`${this.baseapi.url.subject}/multipleDelete/${userid}`, arrDel);
  }
}
