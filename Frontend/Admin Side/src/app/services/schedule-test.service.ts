import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { Test } from './test.service';
import { TestShift } from './testshift.service';
import { Lab } from './lab.service';
import { User } from './user.service';
import { StudentTest } from './student-test.service';
import { ScheduleTestRequest } from '../models/scheduleTestRequest';
import { ExamRequest } from '../models/examRequest';

export interface ListScheduleTestResult {
  errorCode: number;
  message: string;
  data: [ScheduleTest];
}
export interface ScheduleTest {
  id: number;
  sctE_TESID: number;
  sctE_SHIID: number;
  sctE_DATE: any;
  sctE_LABID: number;
  sctE_CREATEDBY: number;
  sctE_CREATEDDATE: Date;
  sctE_MODIFIEDBY: number;
  sctE_MODIFIEDDATE: Date;
  sctE_ISDELETED: boolean;
  sctE_STATUS: number;
  tests: Test;
  testShifts: TestShift;
  labs: Lab;
  userCreated: User;
  userModified: User;
  student_Tests: [StudentTest];
}
export interface ScheduleTestResult {
  errorCode: number;
  message: string;
  data: ScheduleTest;
}

@Injectable({
  providedIn: 'root'
})
export class ScheduleTestService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListScheduleTestResult> {
    return this._http.get<ListScheduleTestResult>(`${this.baseapi.url.scheduletest}`);
  }
  getAllIncoming(): Observable<ListScheduleTestResult> {
    return this._http.get<ListScheduleTestResult>(`${this.baseapi.url.scheduletest}/getAllIncoming`);
  }
  get(id: number): Observable<ScheduleTestResult> {
    return this._http.get<ScheduleTestResult>(`${this.baseapi.url.scheduletest}/${id}`);
  }
  getListScheduleTestMultiDel(arrDel: number[]): Observable<ListScheduleTestResult> {
    return this._http.post<ListScheduleTestResult>(`${this.baseapi.url.scheduletest}/getListScheduleTestMultiDel`, arrDel);
  }
  getListScheduleTestByDate(date: string): Observable<ListScheduleTestResult> {
    return this._http.get<ListScheduleTestResult>(`${this.baseapi.url.scheduletest}/getListScheduleTestByDate/${date}`);
  }
  add(scheduleTestRequest: ScheduleTestRequest): Observable<ScheduleTestResult> {
    return this._http.post<ScheduleTestResult>(`${this.baseapi.url.scheduletest}`, scheduleTestRequest);
  }
  put(scheduleTestRequest: ScheduleTestRequest): Observable<ScheduleTestResult> {
    return this._http.put<ScheduleTestResult>(`${this.baseapi.url.scheduletest}/${scheduleTestRequest.scheduleTest.id}`, scheduleTestRequest);
  }
  delete(id, userid): Observable<ScheduleTestResult> {
    return this._http.delete<ScheduleTestResult>(`${this.baseapi.url.scheduletest}/${id}&${userid}`);
  }
  deleteMulti(arrDel: ScheduleTest[], userid): Observable<ScheduleTestResult> {
    return this._http.post<ScheduleTestResult>(`${this.baseapi.url.scheduletest}/multipleDelete/${userid}`, arrDel);
  }
}
