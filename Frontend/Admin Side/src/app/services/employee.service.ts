import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { User } from './user.service';
import { EmployeePosition } from './employee-position.service';
import { EmployeeRequest } from '../models/employee_request';
import { Employee_Subject } from './employee-subject.service';

export interface ListEmployeeResult {
  errorCode: number;
  message: string;
  data: [Employee];
}
export interface Employee {
  id: number;
  emP_CODE: string;
  emP_FIRSTNAME: string;
  emP_LASTNAME: string;
  emP_GENDER: boolean;
  emP_DATEOFBIRTH: any;
  emP_PHONE: string;
  emP_IDENTITY: string;
  emP_ADDRESS: string;
  emP_EMAIL: string;
  emP_AVATAR: string;
  emP_CREATEDBY: number;
  emP_CREATEDATE: Date;
  emP_MODIFIEDBY: number;
  emP_MODIFIEDDATE: Date;
  emP_USEID: number;
  emP_STATUS: number;
  emP_FACID: number;
  origin: string;
  fileUrl: string;
  users: User;
  employee_Positions: [EmployeePosition];
  fileName: string;
  userCreated: User;
  userModified: User;
  employee_Subjects: [Employee_Subject];
}
export interface EmployeeResult {
  errorCode: number;
  message: string;
  data: Employee;
}
export interface EmployeeDataExportResult {
  errorCode: number;
  message: string;
  data: string;
}

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListEmployeeResult> {
    return this._http.get<ListEmployeeResult>(this.baseapi.url.employee);
  }
  GetAllExceptSelf(id: number): Observable<ListEmployeeResult> {
    return this._http.get<ListEmployeeResult>(`${this.baseapi.url.employee}/GetAllExceptSelf/${id}`);
  }
  getListByPermissionId(id: number): Observable<ListEmployeeResult> {
    return this._http.get<ListEmployeeResult>(`${this.baseapi.url.employee}/getListByPermissionId/${id}`);
  }
  getListByFacultyId(id: number): Observable<ListEmployeeResult> {
    return this._http.get<ListEmployeeResult>(`${this.baseapi.url.employee}/getListByFacultyId/${id}`);
  }
  exportExcelLinkDownload(listEmployeeRequest: Employee[]): Observable<EmployeeDataExportResult> {
    return this._http.post<EmployeeDataExportResult>(`${this.baseapi.url.employee}/exportExcelLinkDownload`, listEmployeeRequest);
  }
  get(id): Observable<EmployeeResult> {
    return this._http.get<EmployeeResult>(`${this.baseapi.url.employee}/${id}`);
  }
  getListEmployeeMultiDel(arrDel: number[]): Observable<ListEmployeeResult> {
    return this._http.post<ListEmployeeResult>(`${this.baseapi.url.employee}/getListEmployeeMultiDel`, arrDel);
  }
  add(emp_req: EmployeeRequest): Observable<EmployeeResult> {
    return this._http.post<EmployeeResult>(`${this.baseapi.url.employee}`, emp_req);
  }
  put(emp_req: EmployeeRequest): Observable<EmployeeResult> {
    return this._http.put<EmployeeResult>(`${this.baseapi.url.employee}/${emp_req.employee.id}`, emp_req);
  }
  delete(id: number, userid): Observable<EmployeeResult> {
    return this._http.delete<EmployeeResult>(`${this.baseapi.url.employee}/${id}&${userid}`);
  }
  deleteMulti(arrDel: Employee[], userid): Observable<EmployeeResult> {
    return this._http.post<EmployeeResult>(`${this.baseapi.url.employee}/multipleDelete/${userid}`, arrDel);
  }
}
