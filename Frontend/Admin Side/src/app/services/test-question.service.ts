import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Test } from './test.service';
import { Question } from './question.service';
import { Passage } from './passage.service';
import { User } from './user.service';

export interface ListTestQuestionResult {
  errorCode: number;
  message: string;
  data: [TestQuestion];
}
export interface TestQuestion {
  id: number;
  teqU_TESID: number;
  teqU_QUEID: number;
  teqU_PASID: number;
  teqU_CREATEDBY: number;
  teqU_CREATEDDATE: number;
  tests: Test;
  questions: Question;
  passages: Passage;
  userCreated: User;
}
export interface TestQuestionResult {
  errorCode: number;
  message: string;
  data: TestQuestion;
}

@Injectable({
  providedIn: 'root'
})
export class TestQuestionService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
}
