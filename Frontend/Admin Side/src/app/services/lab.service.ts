import { Injectable } from '@angular/core';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { User } from './user.service';

export interface ListLabResult {
  errorCode: number;
  message: string;
  data: [Lab];
}
export interface Lab {
  id: number;
  laB_NAME: string;
  laB_ADDRESS: string;
  laB_ISDELETED: boolean;
  laB_STATUS: number;
  laB_CREATEDBY: number;
  laB_CREATEDDATE: Date;
  laB_MODIFIEDBY: number;
  laB_MODIFIEDDATE: Date;
  userCreated: User;
  userModified: User;
  labScheduleStatus: number;
}
export interface LabResult {
  errorCode: number;
  message: string;
  data: Lab;
}

@Injectable({
  providedIn: 'root'
})
export class LabService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListLabResult> {
    return this._http.get<ListLabResult>(`${this.baseapi.url.lab}`);
  }
  get(id: number): Observable<LabResult> {
    return this._http.get<LabResult>(`${this.baseapi.url.lab}/${id}`);
  }
  getListLabMultiDel(arrDel: number[]): Observable<ListLabResult> {
    return this._http.post<ListLabResult>(`${this.baseapi.url.lab}/getListLabMultiDel`, arrDel);
  }
  add(lab: Lab): Observable<LabResult> {
    return this._http.post<LabResult>(`${this.baseapi.url.lab}`, lab);
  }
  put(lab: Lab): Observable<LabResult> {
    return this._http.put<LabResult>(`${this.baseapi.url.lab}/${lab.id}`, lab);
  }
  delete(id, userid): Observable<LabResult> {
    return this._http.delete<LabResult>(`${this.baseapi.url.lab}/${id}&${userid}`);
  }
  deleteMulti(arrDel: Lab[], userid): Observable<LabResult> {
    return this._http.post<LabResult>(`${this.baseapi.url.lab}/multipleDelete/${userid}`, arrDel);
  }
}
