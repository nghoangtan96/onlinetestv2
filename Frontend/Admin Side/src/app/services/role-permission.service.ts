import { Injectable } from '@angular/core';
import { User } from './user.service';
import { Role } from './role.service';
import { Permission } from './permission.service';

export interface RolePermission {
  id: number;
  ropE_ROLID: number;
  ropE_PERID: number;
  ropE_CREATEDBY: number;
  ropE_CREATEDDATE: Date;
  ropE_MODIFIEDBY: number;
  ropE_MODIFIEDDATE: Date;
  ropE_ISACTIVE: boolean;
  ropE_STATUS: number;
  roles: Role;
  permissions: Permission;
  userCreated: User;
  userModified: User;
}

@Injectable({
  providedIn: 'root'
})
export class RolePermissionService {

  constructor() { }
}
