import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { User } from './user.service';

export interface ListTesttypeResult {
  errorCode: number;
  message: string;
  data: [Testtype];
}
export interface Testtype {
  id: number;
  tetY_NAME: string;
  tetY_STATUS: number;
  tetY_CREATEDBY: number;
  tetY_CREATEDATE: Date;
  tetY_MODIFIEDBY: number;
  tetY_MODIFIEDDATE: Date;
  tetY_ISDELETED: boolean;
  userCreated: User;
  userModified: User;
}
export interface TesttypeResult {
  errorCode: number;
  message: string;
  data: Testtype;
}

@Injectable({
  providedIn: 'root'
})
export class TestTypeService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListTesttypeResult> {
    return this._http.get<ListTesttypeResult>(`${this.baseapi.url.testtype}`);
  }
  get(id: number): Observable<TesttypeResult> {
    return this._http.get<TesttypeResult>(`${this.baseapi.url.testtype}/${id}`);
  }
  add(testtype: Testtype): Observable<TesttypeResult> {
    return this._http.post<TesttypeResult>(`${this.baseapi.url.testtype}`, testtype);
  }
  put(testtype: Testtype): Observable<TesttypeResult> {
    return this._http.put<TesttypeResult>(`${this.baseapi.url.testtype}/${testtype.id}`, testtype);
  }
  delete(id, userid): Observable<TesttypeResult> {
    return this._http.delete<TesttypeResult>(`${this.baseapi.url.testtype}/${id}&${userid}`);
  }
}
