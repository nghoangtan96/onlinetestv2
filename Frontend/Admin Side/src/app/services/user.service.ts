import { Injectable } from '@angular/core';
import { Employee } from './employee.service';
import { Student } from './student.service';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { EmployeeRequest } from '../models/employee_request';
import { User_Permission } from './user-permission.service';
import { StudentRequest } from '../models/student_request';

export interface ListUserResult {
  errorCode: number;
  message: string;
  data: [User];
}
export interface User {
  id: number;
  usE_USERNAME: string;
  usE_PASSWORD: string;
  usE_STATUS: number;
  usE_CREATEDBY: number;
  usE_CREATEDDATE: Date;
  usE_MODIFIEDBY: number;
  usE_MODIFIEDDATE: Date;
  usE_ISDELETED: boolean;
  imageurl: string;
  usercreated: User;
  usermodified: User;
  employees: [Employee];
  students: [Student];
  user_Permissions: [User_Permission];
}
export interface UserResult {
  errorCode: number;
  message: string;
  data: User;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListUserResult> {
    return this._http.get<ListUserResult>(this.baseapi.url.user);
  }
  get(id): Observable<UserResult> {
    return this._http.get<UserResult>(`${this.baseapi.url.user}/${id}`);
  }
  addemp(emp: EmployeeRequest): Observable<UserResult>{
    return this._http.post<UserResult>(`${this.baseapi.url.user}`, emp);
  }
  addstu(stu: StudentRequest): Observable<UserResult>{
    return this._http.post<UserResult>(`${this.baseapi.url.user}/student/`, stu);
  }
  putemp(emp: EmployeeRequest): Observable<UserResult>{
    return this._http.put<UserResult>(`${this.baseapi.url.user}/${emp.user.id}`, emp);
  }
  putstu(stu: StudentRequest): Observable<UserResult>{
    return this._http.put<UserResult>(`${this.baseapi.url.user}/student/${stu.user.id}`, stu);
  }
  delete(id, userid): Observable<UserResult>{ 
    return this._http.delete<UserResult>(`${this.baseapi.url.user}/${id}&${userid}`);
  }
}
