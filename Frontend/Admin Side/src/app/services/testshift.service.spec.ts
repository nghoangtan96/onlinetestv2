import { TestBed } from '@angular/core/testing';

import { TestshiftService } from './testshift.service';

describe('TestshiftService', () => {
  let service: TestshiftService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestshiftService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
