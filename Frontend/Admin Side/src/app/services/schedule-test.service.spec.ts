import { TestBed } from '@angular/core/testing';

import { ScheduleTestService } from './schedule-test.service';

describe('ScheduleTestService', () => {
  let service: ScheduleTestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScheduleTestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
