import { Injectable } from '@angular/core';
import { User } from './user.service';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { SubjectBranch } from './subject-branch.service';
import { Faculty } from './faculty.service';

export interface ListBranchResult {
  errorCode: number;
  message: string;
  data: [Branch];
}
export interface Branch {
  id: number;
  brA_CODE: string;
  brA_NAME: string;
  brA_ISDELETED: boolean;
  brA_ISPARENT: boolean;
  brA_STATUS: number;
  brA_PARENT: number;
  brA_DURATION: number;
  brA_LEVEL: number;
  brA_CREATEDBY: number;
  brA_CREATEDDATE: Date;
  brA_MODIFIEDBY: number;
  brA_MODIFIEDDATE: Date;
  brA_FACID: number;
  branches: [Branch];
  branchesParent: Branch;
  userCreated: User;
  userModified: User;
  subject_Branches: [SubjectBranch];
  faculties: Faculty;
}
export interface BranchResult {
  errorCode: number;
  message: string;
  data: Branch;
}

@Injectable({
  providedIn: 'root'
})
export class BranchService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }

  getAll(): Observable<ListBranchResult> {
    return this._http.get<ListBranchResult>(`${this.baseapi.url.branch}`);
  }
  getAllParentBranch(): Observable<ListBranchResult> {
    return this._http.get<ListBranchResult>(`${this.baseapi.url.branch}/getAllParentBranch`);
  }
  getAllAllowChildBranch(): Observable<ListBranchResult> {
    return this._http.get<ListBranchResult>(`${this.baseapi.url.branch}/getAllAllowChildBranch`);
  }
  getListBranchByFacultyId(id: number): Observable<ListBranchResult> {
    return this._http.get<ListBranchResult>(`${this.baseapi.url.branch}/getListBranchByFacultyId/${id}`);
  }
  getListParentBranchByFacultyId(id: number): Observable<ListBranchResult> {
    return this._http.get<ListBranchResult>(`${this.baseapi.url.branch}/getListParentBranchByFacultyId/${id}`);
  }
  getListAllowChildBranchByFacultyId(id: number): Observable<ListBranchResult> {
    return this._http.get<ListBranchResult>(`${this.baseapi.url.branch}/getListAllowChildBranchByFacultyId/${id}`);
  }
  get(id: number): Observable<BranchResult> {
    return this._http.get<BranchResult>(`${this.baseapi.url.branch}/${id}`);
  }
  add(branch: Branch): Observable<BranchResult> {
    return this._http.post<BranchResult>(`${this.baseapi.url.branch}`, branch);
  }
  put(branch: Branch): Observable<BranchResult> {
    return this._http.put<BranchResult>(`${this.baseapi.url.branch}/${branch.id}`, branch);
  }
  delete(id, userid): Observable<BranchResult>{ 
    return this._http.delete<BranchResult>(`${this.baseapi.url.branch}/${id}&${userid}`);
  }
}
