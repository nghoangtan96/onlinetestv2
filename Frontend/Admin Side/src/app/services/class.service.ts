import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Observable } from 'rxjs';
import { User } from './user.service';
import { Branch } from './branch.service';
import { SchoolYear } from './schoolyear.service';

export interface ListClassResult {
  errorCode: number;
  message: string;
  data: [Class];
}

export interface Class {
  id: number;
  clA_CODE: string;
  clA_NAME: string;
  clA_CREATEDBY: number;
  clA_CREATEDDATE: Date;
  clA_MODIFIEDBY: number;
  clA_MODIFIEDDATE: Date;
  clA_STATUS: number;
  clA_ISDELETED: boolean;
  clA_BRAID: number;
  clA_SCYEID: number;
  userCreated: User;
  userModified: User;
  branches: Branch;
  schoolYears: SchoolYear;
}
export interface ClassResult {
  errorCode: number;
  message: string;
  data: Class;
}

@Injectable({
  providedIn: 'root'
})
export class ClassService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListClassResult> {
    return this._http.get<ListClassResult>(this.baseapi.url.class);
  }
  get(id): Observable<ClassResult> {
    return this._http.get<ClassResult>(`${this.baseapi.url.class}/${id}`);
  }
  getListClassMultiDel(arrDel: number[]): Observable<ListClassResult> {
    return this._http.post<ListClassResult>(`${this.baseapi.url.class}/getListClassMultiDel`, arrDel);
  }
  getListByBranchIdAndSchoolYearId(braid: number, scyeid: number): Observable<ListClassResult> {
    return this._http.get<ListClassResult>(`${this.baseapi.url.class}/getListByBranchIdAndSchoolYearId/${braid}&${scyeid}`);
  }
  add(classs: Class): Observable<ClassResult>{
    return this._http.post<ClassResult>(`${this.baseapi.url.class}`, classs);
  }
  put(classs: Class): Observable<ClassResult>{
    return this._http.put<ClassResult>(`${this.baseapi.url.class}/${classs.id}`, classs);
  }
  delete(id, userid): Observable<ClassResult>{ 
    return this._http.delete<ClassResult>(`${this.baseapi.url.class}/${id}&${userid}`);
  }
  deleteMulti(arrDel: Class[], userid): Observable<ClassResult> {
    return this._http.post<ClassResult>(`${this.baseapi.url.class}/multipleDelete/${userid}`, arrDel);
  }
}
