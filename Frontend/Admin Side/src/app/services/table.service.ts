import { Injectable } from '@angular/core';

export interface Table {
  id: number;
  taB_NAME: string;
  taB_STATUS: number;
  taB_CODE: string;
}

@Injectable({
  providedIn: 'root'
})
export class TableService {

  constructor() { }
}
