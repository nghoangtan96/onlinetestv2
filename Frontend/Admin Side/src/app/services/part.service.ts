import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseapiService } from './baseapi.service';
import { Subjectt } from './subject.service';
import { User } from './user.service';
import { Observable } from 'rxjs';
import { Passage } from './passage.service';
import { Question } from './question.service';

export interface ListPartResult {
  errorCode: number;
  message: string;
  data: [Part];
}
export interface Part {
  id: number;
  paR_CODE: string;
  paR_NAME: string;
  paR_ISSHUFFLE: boolean;
  paR_ISALLOWSHUFFLEQUESTION: boolean;
  paR_ISALLOWSHUFFLEOPTION: boolean;
  paR_ISALLOWNULLQUESTION: boolean;
  paR_ISALLOWNULLOPTION: boolean;
  paR_DIRECTION: string;
  paR_DEFAULTSCORE: number;
  paR_DEFAULTCOLUMN: number;
  paR_DEFAULTLEVEL: number;
  paR_ORDER: number;
  paR_NOTE: string;
  paR_ISDELETED: boolean;
  paR_STATUS: number;
  paR_SUBID: number;
  paR_CREATEDBY: number;
  paR_CREATEDDATE: Date;
  paR_MODIFIEDBY: number;
  paR_MODIFIEDDATE: Date;
  subjects: Subjectt;
  userCreated: User;
  userModified: User;
  passages: [Passage];
  questions: [Question];
  countQuestion: number;
  countPassage: number;
  numberOfQuestion: number;
  noquestion: number;
}
export interface PartResult {
  errorCode: number;
  message: string;
  data: Part;
}

@Injectable({
  providedIn: 'root'
})
export class PartService {

  constructor(private _http: HttpClient, private baseapi: BaseapiService) { }
  getAll(): Observable<ListPartResult> {
    return this._http.get<ListPartResult>(`${this.baseapi.url.part}`);
  }
  get(id: number): Observable<PartResult> {
    return this._http.get<PartResult>(`${this.baseapi.url.part}/${id}`);
  }
  getListBySubjectId(id: number): Observable<ListPartResult> {
    return this._http.get<ListPartResult>(`${this.baseapi.url.part}/getListBySubjectId/${id}`);
  }
  // getListBySubjectIdAndQuestion(id: number): Observable<ListPartResult> {
  //   return this._http.get<ListPartResult>(`${this.baseapi.url.part}/getListBySubjectIdAndQuestion/${id}`);
  // }
  // getListBySubjectIdAndPassage(id: number): Observable<ListPartResult> {
  //   return this._http.get<ListPartResult>(`${this.baseapi.url.part}/getListBySubjectIdAndPassage/${id}`);
  // }
  getListPartMultiDel(arrDel: number[]): Observable<ListPartResult> {
    return this._http.post<ListPartResult>(`${this.baseapi.url.part}/getListPartMultiDel`, arrDel);
  }
  add(part: Part): Observable<PartResult> {
    return this._http.post<PartResult>(`${this.baseapi.url.part}`, part);
  }
  put(part: Part): Observable<PartResult> {
    return this._http.put<PartResult>(`${this.baseapi.url.part}/${part.id}`, part);
  }
  delete(id, userid): Observable<PartResult> {
    return this._http.delete<PartResult>(`${this.baseapi.url.part}/${id}&${userid}`);
  }
  deleteMulti(arrDel: Part[], userid): Observable<PartResult> {
    return this._http.post<PartResult>(`${this.baseapi.url.part}/multipleDelete/${userid}`, arrDel);
  }
}
