import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '../services/auth.service';
import { UserPermissionService } from '../services/user-permission.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private cookieSer: CookieService,
    private authSer: AuthService, private userPermissionSer: UserPermissionService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.cookieSer.check('Id') == true) {
      this.userPermissionSer.getListPermissionByUserId(Number(this.cookieSer.get('Id'))).subscribe(res => {
        if (res.data[0].permissions.role_Permissions.find(x => x.roles.roL_URL == this.router.url) == null) {
          return true;
        }
        else {
          let count = 0;
          for (let i = 0; i < res.data.length; i++) {
            if (res.data[i].permissions.peR_STATUS == 1) {
              if (res.data[i].permissions.role_Permissions.find(x => x.roles.roL_URL == this.router.url && x.ropE_ISACTIVE == true) != null) {
                count++;
              }
            }
          }
          if (count == 0) {
            this.router.navigate(['/404']);
            return false;
          }
        }
      });
      return true;
    }
    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }

}
